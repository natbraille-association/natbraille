package org.natbraille.editor.server.unoconvertwrapper;

import com.sun.star.beans.XPropertySet;
import com.sun.star.bridge.UnoUrlResolver;
import com.sun.star.bridge.XUnoUrlResolver;
import com.sun.star.comp.helper.Bootstrap;
import com.sun.star.lang.XMultiComponentFactory;
import com.sun.star.uno.UnoRuntime;
import com.sun.star.uno.XComponentContext;
import org.apache.jena.base.Sys;

/*

    // https://mvnrepository.com/artifact/org.libreoffice/libreoffice
    implementation("org.libreoffice:libreoffice:24.2.3")

    implementation(files("/usr/lib/libreoffice/program/classes/unoil.jar"))
    implementation(files("/usr/lib/libreoffice/program/classes/jurt.jar"))
    implementation(files("/usr/lib/libreoffice/program/classes/ridl.jar"))

    implementation(files("/usr/lib/libreoffice/program/classes/java_uno.jar"))
    implementation(files("/usr/lib/libreoffice/program/classes/juh.jar"))
    implementation(files("/usr/lib/libreoffice/program/classes/unoloader.jar"))

    implementation(files("/usr/lib/libreoffice/program/classes/libreoffice.jar"))
    // java_uno.jar  juh.jar  jurt.jar  libreoffice.jar  ridl.jar  unoil.jar  unoloader.jar

 */

//import com.sun.star.document.XFilterFactory;
import java.util.Arrays;

// https://wiki.documentfoundation.org/Documentation/DevGuide/First_Steps#Getting_Started
public class JUNOConverter {
    void getIt() throws Exception {

        XComponentContext xcomponentcontext = Bootstrap.createInitialComponentContext(null);

// create a connector, so that it can contact the office
        XUnoUrlResolver urlResolver = UnoUrlResolver.create(xcomponentcontext);

        Object initialObject = urlResolver.resolve(
                "uno:socket,host=localhost,port=2002;urp;StarOffice.ServiceManager");

        XMultiComponentFactory xOfficeFactory = UnoRuntime.queryInterface(
                XMultiComponentFactory.class, initialObject);

// retrieve the component context as property (it is not yet exported from the office)
// Query for the XPropertySet interface.
        XPropertySet xProperySet = UnoRuntime.queryInterface(
                XPropertySet.class, xOfficeFactory);

// Get the default context from the office server.
        Object oDefaultContext = xProperySet.getPropertyValue("DefaultContext");

// Query for the interface XComponentContext.
        XComponentContext xOfficeComponentContext = UnoRuntime.queryInterface(
                XComponentContext.class, oDefaultContext);

        //System.out.println(Arrays.toString(xOfficeComponentContext.getServiceManager().getAvailableServiceNames()));


// now create the desktop service
// NOTE: use the office component context here!
        Object oDesktop = xOfficeFactory.createInstanceWithContext(
                "com.sun.star.frame.Desktop", xOfficeComponentContext);
        System.out.println("oDesktop");
        System.out.println(oDesktop);


        Object filterFactoryObject = xOfficeComponentContext.getServiceManager().createInstanceWithContext(
                "com.sun.star.document.FilterFactory",
                xcomponentcontext
        );
        System.out.println("filterFactoryObject");
        System.out.println(filterFactoryObject);

        //    XFilterFactory filterService = UnoRuntime.queryInterface(XFilterFactory.class, filterFactoryObject);


        //System.out.println(filterService.get)

        /*
        Object typeDetectionService = xOfficeComponentContext.getServiceManager().createInstanceWithContext(
                "com.sun.star.document.TypeDetection",
                xcomponentcontext
        );

        System.out.println("typeDetectionService");
        System.out.println(typeDetectionService);
*/


    }

    public static void main(String[] args) throws Exception {
        JUNOConverter junoConverter = new JUNOConverter();
        junoConverter.getIt();
        System.out.println("zou");
    }
}
