# A. Après la transcription

- *on crée une paire d'élements noir/braille au plus interne de la structure.*
- les élements ajoutés sont dans le braille
- on conserve les id
- on ajoute des id pour les nouveaux éléments (ça, ça va pas être facile en xsl sans variable incrémentable globalement ; ça peut être
  fait en 1. utilisant un attribut qui dit "ajouté pendant la transcription", et en englobant le traitement à la toute fin de la passe, 
  une fois que tout le xml est transcrit qui "ajoute" (au sens recopie en ajoutant) l'attribut id en concaténant un préfixe genre 
  "tr-" et le numéro du noeud "ajouté  pendant la transcription". 2. en faisant ça dans une passe séparée comme celle faite en java.

## exemple phrase/mot/ponctuation/majuscule

    <phrase id="">
        <phrase-noire id="">
                <mot id="">Bla</mot><mot id="">blu</mot><ponctuation id="">.<ponctuation>
            </phrase-noire>
            <phrase-braille id="">
                <majsucule id="">:</majuscule><mot id="">bla</mot><mot id="">blu</mot><ponctuation id="">.<ponctuation>
        </phrase>
    </phrase>

note : on peut mettre noir au lieu de phrase-noire ; braille à la place de
phrase-braille, vu que ça peut être déduit du tag phrase englobant. Il faut que ce soit 
le plus pratique à faire

## exemple pour un li 

Ça illustre "au plus interne de la structure"

(je ne mets pas les id pour li/ul mais ils y sont)
(ici, je mets pas li-noir li-braille mais noir, braille)

        <ul>
            <li>... </li>
            <li>
                    <ul>
                        <li>
                                <noir id="">
                                <braille id="">
                        </li>
                    <ul>
            </li>
            <li>... </li>
        <ul>

## B. après la mise en page

il faudrait ajouter un élément qui dit où commence et s'arrête le bloc braille (page/ligne, 
ça c'est facile + offset dans la ligne ?) comme ça on peut refaire la mise en page seulement 
à partir de l'endroit modifié.  Note que c'est un plus : vu que ça n'améliore que la vitesse 
des changements faits  à la fin du document, et que si on modifie le début, on doit tout 
recalculer de toutes façons, on peut dire en première approche qu'on ajoute pas cet attribut.

Dans cette passe, je pense, sauf preuve du contraire qu'il n y'a a *pas besoin de dupliquer les élement* :
il suffit d'ajouter les élements de mise en page. On peut soit le préfixer par "layout-" soit mettre un nom avec "qualification", 
du genre "layout:page-number".

ÇA peut être bien de leur donner des id aussi pour le cas de la détranscription et pour l'homogénétié.

y'aurait donc :

    <layout:spacing>
    <layout:page-number>
    <layout:line-break>
    <layout:page-break>
    <layout:bullet>  (ou bien c'est déjà dans "transcription" ?)
    ... ?

## exemple 

        <phrase id="">
            <phrase-noire id="">
                    <mot id="">Bla</mot><mot id="">blu</mot><ponctuation id="">.<ponctuation>
                </phrase-noire>
                <phrase-braille id="" layout:page-start="" layout:line-start="" layout:page-end="" layout:line-end="">
                    <layout:spacing id="">                 <layout:spacing><layout:page-number>#1</layout:page-number id=""><layout:line-break id=""/>
                    <majsucule id="">:</majuscule><mot id="">bla</mot><mot id="">blu</mot><ponctuation id="">.<ponctuation>
            </phrase>
        </phrase>