(doc existant 01.2023 + prop préliminaire)

# Editeur

L'éditeur se présente sous la forme de deux fenêtres côte à côté : 
- la première contient une vue de type "document strucuré"
- la seconde contient le document en braille mis en page

## attributs de fragments du document

Un menu/barre d'outil/raccourcis permet les manipulations usuelles d'un éditeur de texte riche et structuré, mais en limitant les fonctionalités d'édition aux aspects qui ont un sens dans un document Braille. Par exemple : formattage ; style de titres et de paragraphes ; mise en évidence.

En plus de ces fonctionnalités d'édition de texte riche, l'éditeur doit permettre l'interaction avec les fonctionalités spécfiques du braille / de Natbraille. Par exemple : il doit être possible, pour un mot ou une expression, de lui attribuer une transcription prédéterminée ; de choisir, en cas d'ambiguité, la version correcte du braille transcrit ; de préciser les attributs d'un mot ou d'une expression qui conditionne sa transcription (par ex. nom propre).

Outre la définition d'attributs propres à des fragments d'un texte, l'éditeur doit permettre l'écriture de règles d'abrégé.

## attributs de la transcription globale à tout le document

L'éditeur doit permettre la sélection des options de nat (voir le fichier listant les options) qui n'ont de sens que pour l'ensemble d'une transcription. Par exemple, la correspondance des titres du document structuré avec celle du document braille ; le choix des règles du braille abrégé ; le choix de la transcription ou non des mathématiques, numérotation des pages etc.

## interaction

Les deux vues (document strucuré et braille mis en page) doivent pouvoir être parcourues de manière synchrone : le curseur et la portion visible du document des deux vues doivent correspondre. 

Un document braille ne correpondant pas à une translittération d'un document noir (et d'autant moins un document noir structuré et un document braille mis en page), la synchronisation du curseur ne peut pas être faite au niveau de chaque caractère. 

Nous choisissons donc de faire cette synchronisation au niveau du mot ou groupe de mot, entendu au sens où une expression de plusieurs mots en noir qui s'abrège en un mot en noir sera considérée comme un seul mot. 

Dans le cas où ce niveau de précision ne serait pas réalisable du fait de difficultés liées à la conception initiale du moteur natbraille (destiné au traitement pas lot), la synchronisation portera sur le paragraphe.

## action sur le document mis en page. 

Il reste à déterminer dans quelle mesure un utilisateur peut agir sur la vue braille pour modifier la mise en page. Si l'utilisateur veut ajouter un saut de ligne/saut de page à la fin d'un mot/groupe de mot ou d'un paragraphe, il est possible d'ajouter ce saut de ligne dans le document structuré noir ; cependant si ce même saut de ligne/saut de page est ajouté au milieu d'un mot, la synchronisation étant faite au niveau du mot, il n'est plus possible de la reporter sur le document noir/structuré. De ce fait, si une modification dans le noir du même paragraphe est opérée ultérieurement, le saut de ligne ajouté sera supprimé.

Dans le même cas, les informations relatives à la césure n'ont de sens qu'ajoutées au braille, surtout dans le cas de l'abrégé, il n'est donc pas possible de les reporter dans le noir, donc une modification du noir entrainant une retranscription du fragment aurait pour conséquence de faire perdre à l'utilisateur l'information de mise en page.

## transcription inverse.

Dans une certaine mesure à définir, l'éditeur doit permettre de réaliser une transcription inverse, c'est à dire la "dé-transcription" dans le vocabulaire de natbraille, doit la transformation du braille vers le noir structuré.

## explicabilité

Dans un but pédagogique, l'éditeur doit idéalement être en mesure de fournir, pour chaque transcription, l'explication de la transcription effectuée (règles utilisées), lorsque le moteur natbraille le permet.

# Rappel : formats XML/braille actuels

En partant d'un document libreoffice/word, sortie des étapes successives :
- 1. "Convertisseur openoffice" : odt -> xhtml+mathml
- 2. "Convertisseur XML" : mathml+xhtml -> phrase/lit/mot/ponctuation/math/tableau...
- 3. "Transcodeur normal"  :  phrase/lit/mot/ponctuation/math/tableau -> phrase @modBraille + braille / braille abrégé+info césure
- 4. "AmbiguityFilter" : résolution des ambiguités 
- 5. "PrésentateurMEP" : braille mise en page
- 6. "BrailleTable Converter" : table braille


## 1. "Convertisseur openoffice"

    <body dir="ltr">
        <p class="Title">Jean-Marc</p>
        <p class="Subtitle">Un type</p>
        <p>&nbsp;</p>
        <p>Qui est Jean-Marc&nbsp;? On verra bien&nbsp;!</p>
        <h1 id="toc0">Les elements</h1>
        <p>Est-ce que Jean-Marc est un tableau&nbsp;?</p>
        <h2 id="toc1">Un tableau 2x2&nbsp;?</h2>
        <table 

## 2. "Convertisseur XML"        

    <doc:doc
        <phrase styleOrig="Title" center="true">
            <lit>
                <mot mev="1">Jean-Marc</mot>
            </lit>
        </phrase>
        <phrase styleOrig="Subtitle" center="true">
            <lit>
                <mot>Un</mot>
                <mot>type</mot>
            </lit>
        </phrase>
        <phrase/>
        <phrase>
            <lit>
                <mot>Qui</mot>
                <mot>est</mot>
                <mot>Jean-Marc</mot>
                <ponctuation>?</ponctuation>                
            </lit>
        </phrase>
        <titre niveauOrig="1">
            <lit>
                <mot>Les</mot>
                <mot>elements</mot>
            </lit>
        </titre>
        <tableau type="lit">
            <tr>
                <th ... >													
                    <phrase xmlns="" styleOrig="TableContents"><lit><mot>Oui</mot></lit></phrase>
                </th>
            </tr>
            <tr>
                <td ...>
                     <phrase xmlns="" styleOrig="TableContents"><lit><mot>Peut-être</mot></lit></phrase>
                </td>
            </tr>
        </tableau>
        <phrase>
            <lit>
                <mot>En</mot>
                <mot>mettre</mot>
                <mot>une</mot>
                <mot>sur</mot>
                <mot>la</mot>
                <mot>ligne</mot>
            </lit>
            <math xmlns="http://www.w3.org/1998/Math/MathML"
                xmlns:pref="http://www.w3.org/2002/Math/preference"
                xmlns:xlink="http://www.w3.org/1999/xlink"
                overflow="scroll">
                <mfrac>
                    <mrow>
                    <mrow>
                        <mn>2</mn>
                        <mo stretchy="false">∗</mo>
                        <mn>2</mn>
                </mrow>
                <mo stretchy="false">=</mo>
                <mn>78</mn>
                </mrow>
                <mn>23</mn>
                </mfrac>
            </math>
        </phrase>
        <phrase>
            <lit>
            <mot>Il</mot>
            <mot>y</mot>
            <mot>a</mot>
            <mot>aussi</mot>
            <mot>un</mot>
            <mot mev="2">mot</mot>
            <mot>en</mot>
            <mot>italiques</mot>
            <ponctuation>.</ponctuation>
            <mot>Ou</mot>
            <mot>alors</mot>
            <mot mev="2">tout</mot>
            <mot mev="2">un</mot>
            <mot mev="2">bout</mot>
            <mot mev="2">de</mot>
            <mot mev="2">phrase</mot>
            <mot mev="2">qui</mot>
            <mot mev="2">est</mot>
            <mot mev="2">en</mot>
            <mot mev="2">italiques</mot>
        </phrase>
        
        <doc:ul list-style-type="disc">
            <doc:li>
            <lit>
                <mot/>
            </lit>
            <lit>
                <mot>item</mot>
                <mot doSpace="false">A</mot>
            </lit>
            <doc:ul list-style-type="circle">
                <doc:li>
                <lit>
                    <mot/>
                </lit>
                <lit>
                    <mot>sous</mot>
                    <mot>item</mot>
                    <mot doSpace="false">A.1</mot>

                    ...
   </phrase>

## 3. "Transcodeur normal"        

    <doc:doc ...>
        <phrase center="true" styleOrig="Title" modeBraille="3-1">⠸⠨⠚⠑⠁⠝⠤⠨⠍⠈⠉</phrase>
        <phrase center="true" styleOrig="Subtitle" modeBraille="3-1">⠨ⴹ⠥⠀ⴹ⠞⠽</phrase>
        <phrase modeBraille="3-1"/>
        <phrase modeBraille="3-1">⠨ⴹ⠛⠀ⴹ⠱⠀⠨⠚⠑⠁⠝⠤⠨⠍⠈⠉⠢⠀⠨ⴹ⠬⠀⠧⠦⠗⠁⠀ⴹ⠃⠖</phrase>
        <titre niveauOrig="1" niveauBraille="1">⠨⠇⠑⠎⠀⠑⠇⠑⠍⠑⠝⠞⠎</titre>
        <phrase modeBraille="3-1">⠨ⴹ⠱⠤ⴹ⠉⠀ⴹ⠟⠀⠨⠚⠑⠁⠝⠤⠨⠍⠈⠉⠀ⴹ⠱⠀ⴹ⠥⠀⠞⠁⠘⠑⠅⠢</phrase>
        <titre niveauOrig="2" niveauBraille="2">⠨⠥⠝⠀⠞⠁⠃⠇⠑⠁⠥⠀⠠⠣⠭⠣⠢</titre>
        <doc:ul style="tableau">
            <doc:li/>
            <doc:li/>
        </doc:ul>
        <titre niveauOrig="2" niveauBraille="2">⠨⠕⠥⠀⠑⠝⠉⠕⠗⠑⠀⠥⠝⠀⠞⠁⠃⠇⠑⠁⠥⠀⠋⠥⠎⠊⠕⠝</titre>
        <doc:ul style="tableau">
            <doc:li/>
            <doc:li/>
        </doc:ul>
        <titre niveauOrig="2" niveauBraille="2">⠨⠕⠥⠀⠑⠝⠉⠕⠗⠑⠀⠥⠝⠑⠀⠋⠕⠗⠍⠥⠇⠑⠀⠙⠑⠀⠍⠁⠞⠓⠎⠢</titre>
        <titre niveauOrig="3" niveauBraille="3">⠨ⴹ⠉⠇⠤⠉⠊⠀ⴹ⠱⠀ⴹ⠓⠀⠎⠁⠀ⴹ⠇⠶⠂⠀ⴹ⠎⠇⠑</titre>
        <phrase modeBraille="3-1">ⴲ⠣ⴰ⠐⠔⠣ⴱ⠶⠻⠳ⴰ⠌⠣⠩ⴳ</phrase>
        <titre niveauOrig="3" niveauBraille="3">⠨ⴹ⠅⠴⠀ⴹ⠷⠭</titre>
        <phrase modeBraille="3-1">⠨ⴹ⠢⠀ⴹ⠍⠴⠀ⴹ⠥⠝⠀ⴹ⠓⠀ⴹ⠄⠀ⴹ⠇⠶⠀ⴲ⠠⠣ⴰ⠐⠔⠣ⴱ⠶⠻⠳ⴰ⠌⠣⠩ⴳ</phrase>
        <phrase modeBraille="3-1">⠨ⴹ⠢⠎⠂⠀ⴹ⠊⠀⠽⠀⠁⠀⠒⠨ⴹ⠞⠞⠎⠀ⴹ⠜⠀ⴹ⠓⠞⠎⠀ⴹ⠙⠀⠍⠁⠚⠥⠎⠉⠥⠇⠱⠀⠷⠀ⴹ⠄⠀⠨⠎⠆⠞⠑⠀ⴹ⠛⠀ⴹ⠬⠞⠀ⴹ⠥⠀⠴⠌⠞⠍⠀⠎⠏⠿⠉⠊⠁⠇</phrase>
        <phrase modeBraille="3-1">⠨ⴹ⠊⠀⠽⠀⠁⠀ⴹ⠅⠎⠀ⴹ⠥⠀⠸⠍⠕⠞⠀ⴹ⠢⠀⠊⠞⠁⠇⠊⠟⠎⠲⠀⠨ⴹ⠳⠀ⴹ⠁⠇⠀⠒⠸ⴹ⠡⠀ⴹ⠥⠀⠃⠳⠞⠀ⴹ⠙⠀⠏⠓⠗⠁⠎⠑⠀ⴹ⠛⠀ⴹ⠱⠀ⴹ⠢⠀⠸⠊⠞⠁⠇⠊⠟⠎⠀ⴹ⠾⠀⠉⠄ⴹ⠱⠀ⴹ⠃⠀ⴹ⠪⠀⠹⠾⠞⠲⠀⠨ⴹ⠢⠀ⴹ⠫⠀ⴹ⠙⠎⠀⠊⠞⠁⠇⠊⠟⠎⠂⠀ⴹ⠊⠀⠽⠀⠁⠀ⴹ⠜⠀⠍⠕⠞⠎⠀ⴹ⠢⠀⠸⠛⠗⠁⠎⠲⠀⠨ⴹ⠳⠀ⴹ⠁⠇⠀⠒⠸ⴹ⠡⠀ⴹ⠥⠀⠃⠳⠞⠀ⴹ⠙⠀⠏⠓⠗⠁⠎⠑⠀ⴹ⠛⠀ⴹ⠱⠀ⴹ⠢⠀⠸⠻⠁⠎⠀ⴹ⠾⠀⠉⠄ⴹ⠱⠀ⴹ⠃⠀ⴹ⠪⠀⠹⠾⠞⠲</phrase>
        <phrase modeBraille="3-1">⠨ⴹ⠱⠤ⴹ⠉⠀ⴹ⠟⠀⠉⠄ⴹ⠱⠀ⴹ⠡⠢</phrase>
        <phrase modeBraille="3-1">⠨⠝⠬⠂⠀⠉⠈⠀ⴹ⠊⠀⠽⠀⠁⠀ⴹ⠅⠎⠀ⴹ⠜⠀⠋⠁⠍⠐⠭⠀⠎⠅⠞⠎⠀ⴹ⠙⠀⠏⠁⠛⠑⠂⠀ⴹ⠏⠸⠑⠀ⴹ⠁⠖⠀ⴹ⠇⠀⠏⠁⠗⠁⠻⠁⠏⠓⠑⠀⠎⠆⠧⠹</phrase>
        <phrase modeBraille="3-1">⠨ⴹ⠁⠖⠀⠉⠑⠇⠥⠊⠤⠉⠊⠀ⴹ⠢⠀ⴹ⠏⠇</phrase>
        <phrase modeBraille="3-1">⠨ⴹ⠬⠀ⴹ⠱⠀⠏⠁⠮⠿⠀⠷⠀ⴹ⠄⠀⠏⠁⠛⠑⠀⠎⠆⠧⠂⠞⠑⠲</phrase>
        <phrase modeBraille="3-1">⠨ⴹ⠭⠀ⴹ⠊⠀⠽⠀ⴹ⠅⠎⠀ⴹ⠥⠀ⴹ⠅⠴⠀⠛⠢⠗⠑⠀ⴹ⠙⠀⠎⠅⠞⠀ⴹ⠙⠀⠏⠁⠛⠑</phrase>
        <phrase modeBraille="3-1">⠨ⴹ⠉⠍⠀ⴹ⠇⠀⠎⠅⠞⠀ⴹ⠙⠀⠏⠁⠛⠑⠀ⴹ⠁⠖⠀ⴹ⠉⠀⠏⠁⠗⠁⠻⠁⠏⠓⠑</phrase>
        <phrase modeBraille="3-1">⠨ⴹ⠕⠀ⴹ⠢⠀ⴹ⠎⠺⠎⠀⠷⠀ⴹ⠄⠀⠏⠁⠛⠑⠀⠴⠾⠎⠂⠀ⴹ⠕⠀⠁⠸⠬⠎⠀⠎⠅⠞⠦⠀ⴹ⠟⠟⠎⠀ⴹ⠇⠶⠎</phrase>
        <phrase modeBraille="3-1"/>
        <phrase modeBraille="3-1"/>
        <phrase modeBraille="3-1"/>
        <phrase modeBraille="3-1">⠨ⴹ⠾⠀ⴹ⠕⠀ⴹ⠧⠇⠀ⴹ⠥⠀⠏⠐⠀ⴹ⠅⠀⠍⠊⠇⠠⠀ⴹ⠙⠀ⴹ⠄⠀ⴹ⠇⠶⠲</phrase>
        <phrase center="true" modeBraille="3-1">⠨ⴹ⠟⠀ⴹ⠎⠀⠏⠁⠮⠑⠤⠞⠤ⴹ⠊⠀ⴹ⠇⠟⠀ⴹ⠇⠀⠞⠭⠞⠑⠀ⴹ⠱⠀⠉⠢⠴⠿⠢</phrase>
        <phrase modeBraille="3-1">⠨ⴹ⠳⠀ⴹ⠢⠉⠀⠚⠥⠎⠞⠊⠋⠊⠿⠀⠷⠀⠹⠾⠞⠑⠢</phrase>
        <phrase modeBraille="3-1">⠨ⴹ⠊⠀ⴹ⠱⠀ⴹ⠞⠏⠀⠙⠄⠁⠃⠰⠙⠦⠀ⴹ⠍⠞⠀ⴹ⠜⠀⠇⠊⠎⠞⠱⠀⠷⠀⠏⠥⠉⠱⠲</phrase>
        <doc:ul list-style-type="disc">
            <doc:li>⠊⠞⠜⠀⠨⠁<doc:ul list-style-type="circle">
                    <doc:li>ⴹ⠴⠀⠊⠞⠜⠀⠨⠁⠠⠲⠡</doc:li>
                    <doc:li>ⴹ⠴⠀⠊⠞⠜⠀⠨⠁⠠⠲⠣</doc:li>
                </doc:ul>
            </doc:li>
            <doc:li>⠊⠞⠜⠀⠨⠃<doc:ul list-style-type="circle">
                    <doc:li>ⴹ⠴⠀⠊⠞⠜⠀⠨⠃⠠⠲⠡</doc:li>
                    <doc:li>ⴹ⠴⠀⠊⠞⠜⠀⠨⠃⠠⠲⠣<doc:ul list-style-type="square">
                        <doc:li>ⴹ⠴⠤ⴹ⠴⠀⠊⠞⠜⠀⠨⠃⠠⠲⠡⠲⠁</doc:li>
                        <doc:li>ⴹ⠴⠤ⴹ⠴⠀⠊⠞⠜⠀⠨⠃⠠⠲⠡⠲⠃</doc:li>
                    </doc:ul>
                    </doc:li>
                </doc:ul>
            </doc:li>
            <doc:li>⠊⠞⠜⠀⠨⠉</doc:li>
        </doc:ul>
        <phrase modeBraille="3-1"/>
        <phrase modeBraille="3-1">⠨ⴹ⠊⠀⠽⠀⠁⠀ⴹ⠜⠀⠇⠊⠎⠞⠱⠀⠝⠥⠍⠿⠗⠕⠞⠿⠱⠂⠀ⴹ⠿⠛⠍⠒</phrase>
        <doc:ol list-style-type="decimal">
            <doc:li>⠠⠡⠲ ⴹ⠖⠍</doc:li>
            <doc:li>⠠⠣⠲ ⠙⠐⠭⠊⠮⠍⠑</doc:li>
            <doc:li>⠠⠩⠲ ⠴⠾⠎⠊⠮⠍⠑</doc:li>
        </doc:ol>
        <phrase modeBraille="3-1"/>
        <phrase modeBraille="3-1">⠨ⴹ⠜⠀ⴹ⠙⠢⠱⠀⠏⠥⠉⠱⠢⠀⠨⠝⠬⠂⠀ⴹ⠏⠎⠀ⴹ⠯⠀ⴹ⠇⠀ⴹ⠍⠢⠲</phrase>
        <phrase modeBraille="3-1">⠨ⴹ⠕⠀⠁⠸⠬⠎⠀ⴹ⠍⠞⠀⠧⠾⠗⠀ⴹ⠟⠀ⴹ⠇⠀⠏⠁⠗⠁⠻⠁⠏⠓⠑⠀⠎⠆⠧⠂⠞⠑⠀ⴹ⠱⠀ⴹ⠡⠀ⴹ⠎⠇⠀ⴹ⠓⠀⠎⠁⠀⠏⠁⠛⠑⠂⠀⠉⠈⠀ⴹ⠊⠀⠽⠀⠁⠀ⴹ⠥⠀⠎⠅⠞⠀ⴹ⠙⠀⠏⠁⠛⠑⠀ⴹ⠁⠧⠂⠀ⴹ⠾⠀ⴹ⠥⠀ⴹ⠁⠖</phrase>
        <phrase modeBraille="3-1">⠨ⴹ⠡⠀ⴹ⠎⠇⠀ⴹ⠓⠀ⴹ⠄⠀⠏⠁⠛⠑</phrase>
        <phrase modeBraille="3-1">⠨ⴹ⠾⠀ⴹ⠕⠀ⴹ⠧⠇⠀⠷⠀ⴹ⠄⠀⠋⠔⠀ⴹ⠦⠀⠙⠕⠉⠥⠍⠣⠲</phrase>
        <phrase modeBraille="3-1">⠨⠝⠬⠀ⴹ⠏⠎⠀ⴹ⠡⠤⠷⠤⠋⠩⠖</phrase>
        <phrase modeBraille="3-1">⠨ⴹ⠊⠀ⴹ⠕⠀⠄⠎⠞⠑⠀ⴹ⠢⠉⠀⠷⠀⠧⠾⠗⠀ⴹ⠥⠀⠎⠞⠽⠇⠑⠀ⴹ⠏⠝⠇⠞⠀ⴹ⠙⠀⠏⠁⠗⠁⠻⠁⠓⠑</phrase>
        <phrase modeBraille="3-1"/>
        <phrase styleOrig="poesie" modeBraille="1-3">⠨⠉⠄ⴹ⠱⠀ⴹ⠇⠀⠎⠞⠽⠇⠑⠀⠏⠕⠑⠎⠊⠑</phrase>
        <phrase styleOrig="poesie" modeBraille="1-3">ⴹ⠇⠀⠎⠞⠽⠇⠑⠀⠌⠍⠿⠀ⴹ⠏⠀ⴹ⠜⠀⠏⠕⠮⠞⠱</phrase>
        <phrase styleOrig="poesie" modeBraille="1-3">ⴹ⠉⠭⠀ⴹ⠛⠀⠚⠳⠣⠀ⴹ⠁⠉⠀ⴹ⠜⠀⠍⠕⠞⠎</phrase>
        <phrase styleOrig="poesie" modeBraille="1-3">ⴹ⠮⠀ⴹ⠎⠀⠋⠁⠞⠊⠛⠥⠦⠀⠎⠅⠋⠀ⴹ⠉⠭⠀ⴹ⠛⠀⠈⠗⠣⠞⠣⠲</phrase>
        <titre niveauOrig="4" niveauBraille="4">⠨ⴹ⠾⠀ⴹ⠖⠀ⴹ⠅⠎⠀ⴹ⠥⠀ⴹ⠞⠴⠀⠠⠹</titre>
        <phrase modeBraille="3-1"/>
        <phrase modeBraille="3-1">⠨⠉⠄ⴹ⠱⠀⠞⠦⠍⠊⠝⠿⠲</phrase>
        <phrase modeBraille="3-1">⠨ⴹ⠱⠤ⴹ⠉⠀ⴹ⠟⠀ⴹ⠉⠁⠀⠒⠧[Amb:Verbe convier (1) ou convenir (2)?|Est - ce que cela |convient|? |⠊⠣|⠲⠞]⠢</phrase>
        <phrase modeBraille="3-1">⠨ⴹ⠍⠉⠖</phrase>
        <doc:ol list-style-type="decimal">
            <doc:li>⠠⠡⠲ </doc:li>
        </doc:ol>
        <phrase modeBraille="3-1"/>
        <phrase modeBraille="3-1"/>
        <phrase modeBraille="3-1"/>
        <phrase modeBraille="3-1"/>
   </doc:doc>

## 4. Ambiguity Filter

    ...
        <phrase styleOrig="poesie" modeBraille="1-3">ⴹ⠇⠀⠎⠞⠽⠇⠑⠀⠌⠍⠿⠀ⴹ⠏⠀ⴹ⠜⠀⠏⠕⠮⠞⠱</phrase>
        <phrase styleOrig="poesie" modeBraille="1-3">ⴹ⠉⠭⠀ⴹ⠛⠀⠚⠳⠣⠀ⴹ⠁⠉⠀ⴹ⠜⠀⠍⠕⠞⠎</phrase>
        <phrase styleOrig="poesie" modeBraille="1-3">ⴹ⠮⠀ⴹ⠎⠀⠋⠁⠞⠊⠛⠥⠦⠀⠎⠅⠋⠀ⴹ⠉⠭⠀ⴹ⠛⠀⠈⠗⠣⠞⠣⠲</phrase>
        <titre niveauOrig="4" niveauBraille="4">⠨ⴹ⠾⠀ⴹ⠖⠀ⴹ⠅⠎⠀ⴹ⠥⠀ⴹ⠞⠴⠀⠠⠹</titre>
        <phrase modeBraille="3-1"/>
        <phrase modeBraille="3-1">⠨⠉⠄ⴹ⠱⠀⠞⠦⠍⠊⠝⠿⠲</phrase>
        <phrase modeBraille="3-1">⠨ⴹ⠱⠤ⴹ⠉⠀ⴹ⠟⠀ⴹ⠉⠁⠀⠒⠧⠊⠣⠢</phrase>
        <phrase modeBraille="3-1">⠨ⴹ⠍⠉⠖</phrase>
        <doc:ol list-style-type="decimal">
            <doc:li>⠠⠡⠲ </doc:li>
        </doc:ol>
        <phrase modeBraille="3-1"/>
        <phrase modeBraille="3-1"/>
        <phrase modeBraille="3-1"/>
        <phrase modeBraille="3-1"/>
    </doc:doc>

## 5. PresentateurMEP

    ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠸⠨⠚⠑⠁⠝⠤⠨⠍⠈⠉
    ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠨⠥⠀⠞⠽

    ⠀⠀⠨⠛⠀⠱⠀⠨⠚⠑⠁⠝⠤⠨⠍⠈⠉⠢⠀⠨⠬⠀⠧⠦⠗⠁⠀⠃⠖
    ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠨⠇⠑⠎⠀⠑⠇⠑⠍⠑⠝⠞⠎
    ⠀⠀⠨⠱⠤⠉⠀⠟⠀⠨⠚⠑⠁⠝⠤⠨⠍⠈⠉⠀⠱⠀⠥⠀⠞⠁⠘⠑⠅⠢
    ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠨⠥⠝⠀⠞⠁⠃⠇⠑⠁⠥⠀⠠⠣⠭⠣⠢
    ⠀⠀⠀⠀⠀⠀⠨⠕⠥⠀⠑⠝⠉⠕⠗⠑⠀⠥⠝⠀⠞⠁⠃⠇⠑⠁⠥⠀⠋⠥⠎⠊⠕⠝
    ⠀⠀⠀⠀⠨⠕⠥⠀⠑⠝⠉⠕⠗⠑⠀⠥⠝⠑⠀⠋⠕⠗⠍⠥⠇⠑⠀⠙⠑⠀⠍⠁⠞⠓⠎⠢
    ⠀⠀⠀⠀⠨⠉⠇⠤⠉⠊⠀⠱⠀⠓⠀⠎⠁⠀⠇⠶⠂⠀⠎⠇⠑
    ⠣⠐⠔⠣⠶⠻⠳⠌⠣⠩
    ⠀⠀⠀⠀⠨⠅⠴⠀⠷⠭
    ⠀⠀⠨⠢⠀⠍⠴⠀⠥⠝⠀⠓⠀⠄⠀⠇⠶⠀⠠⠣⠐⠔⠣⠶⠻⠳⠌⠣⠩
    ⠀⠀⠨⠢⠎⠂⠀⠊⠀⠽⠀⠁⠀⠒⠨⠞⠞⠎⠀⠜⠀⠓⠞⠎⠀⠙⠀⠍⠁⠚⠥⠎⠉⠥⠇⠱⠀⠷⠀⠄
    ⠨⠎⠆⠞⠑⠀⠛⠀⠬⠞⠀⠥⠀⠴⠌⠞⠍⠀⠎⠏⠿⠉⠊⠁⠇
    ⠀⠀⠨⠊⠀⠽⠀⠁⠀⠅⠎⠀⠥⠀⠸⠍⠕⠞⠀⠢⠀⠊⠞⠁⠇⠊⠟⠎⠲⠀⠨⠳⠀⠁⠇⠀⠒⠸⠡
    ⠥⠀⠃⠳⠞⠀⠙⠀⠏⠓⠗⠁⠎⠑⠀⠛⠀⠱⠀⠢⠀⠸⠊⠞⠁⠇⠊⠟⠎⠀⠾⠀⠉⠄⠱⠀⠃⠀⠪
    ⠹⠾⠞⠲⠀⠨⠢⠀⠫⠀⠙⠎⠀⠊⠞⠁⠇⠊⠟⠎⠂⠀⠊⠀⠽⠀⠁⠀⠜⠀⠍⠕⠞⠎⠀⠢
    ⠸⠛⠗⠁⠎⠲⠀⠨⠳⠀⠁⠇⠀⠒⠸⠡⠀⠥⠀⠃⠳⠞⠀⠙⠀⠏⠓⠗⠁⠎⠑⠀⠛⠀⠱⠀⠢
    ⠸⠻⠁⠎⠀⠾⠀⠉⠄⠱⠀⠃⠀⠪⠀⠹⠾⠞⠲
    ⠀⠀⠨⠱⠤⠉⠀⠟⠀⠉⠄⠱⠀⠡⠢
    ⠀⠀⠨⠝⠬⠂⠀⠉⠈⠀⠊⠀⠽⠀⠁⠀⠅⠎⠀⠜⠀⠋⠁⠍⠐⠭⠀⠎⠅⠞⠎⠀⠙⠀⠏⠁⠛⠑⠂
    ⠏⠸⠑⠀⠁⠖⠀⠇⠀⠏⠁⠗⠁⠻⠁⠏⠓⠑⠀⠎⠆⠧⠹
    ⠀⠀⠨⠁⠖⠀⠉⠑⠇⠥⠊⠤⠉⠊⠀⠢⠀⠏⠇
    ⠀⠀⠨⠬⠀⠱⠀⠏⠁⠮⠿⠀⠷⠀⠄⠀⠏⠁⠛⠑⠀⠎⠆⠧⠂⠞⠑⠲
    ⠀⠀⠨⠭⠀⠊⠀⠽⠀⠅⠎⠀⠥⠀⠅⠴⠀⠛⠢⠗⠑⠀⠙⠀⠎⠅⠞⠀⠙⠀⠏⠁⠛⠑
    ⠀⠀⠨⠉⠍⠀⠇⠀⠎⠅⠞⠀⠙⠀⠏⠁⠛⠑⠀⠁⠖⠀⠉⠀⠏⠁⠗⠁⠻⠁⠏⠓⠑
    ⠀⠀⠨⠕⠀⠢⠀⠎⠺⠎⠀⠷⠀⠄⠀⠏⠁⠛⠑⠀⠴⠾⠎⠂⠀⠕⠀⠁⠸⠬⠎⠀⠎⠅⠞⠦⠀⠟⠟⠎
    ⠇⠶⠎



    ⠀⠀⠨⠾⠀⠕⠀⠧⠇⠀⠥⠀⠏⠐⠀⠅⠀⠍⠊⠇⠠⠀⠙⠀⠄⠀⠇⠶⠲
    ⠀⠀⠀⠀⠨⠟⠀⠎⠀⠏⠁⠮⠑⠤⠞⠤⠊⠀⠇⠟⠀⠇⠀⠞⠭⠞⠑⠀⠱⠀⠉⠢⠴⠿⠢
    ⠀⠀⠨⠳⠀⠢⠉⠀⠚⠥⠎⠞⠊⠋⠊⠿⠀⠷⠀⠹⠾⠞⠑⠢
    ⠀⠀⠨⠊⠀⠱⠀⠞⠏⠀⠙⠄⠁⠃⠰⠙⠦⠀⠍⠞⠀⠜⠀⠇⠊⠎⠞⠱⠀⠷⠀⠏⠥⠉⠱⠲
    ⠀⠀⠪⠕⠀⠊⠞⠜⠀⠨⠁
    ⠀⠀⠀⠀⠪⠕⠀⠴⠀⠊⠞⠜⠀⠨⠁⠠⠲⠡
    ⠀⠀⠀⠀⠪⠕⠀⠴⠀⠊⠞⠜⠀⠨⠁⠠⠲⠣

    ⠀⠀⠪⠕⠀⠊⠞⠜⠀⠨⠃
    ⠀⠀⠀⠀⠪⠕⠀⠴⠀⠊⠞⠜⠀⠨⠃⠠⠲⠡
    ⠀⠀⠀⠀⠪⠕⠀⠴⠀⠊⠞⠜⠀⠨⠃⠠⠲⠣
    ⠀⠀⠀⠀⠀⠀⠪⠕⠀⠴⠤⠴⠀⠊⠞⠜⠀⠨⠃⠠⠲⠡⠲⠁
    ⠀⠀⠀⠀⠀⠀⠪⠕⠀⠴⠤⠴⠀⠊⠞⠜⠀⠨⠃⠠⠲⠡⠲⠃
    ⠀⠀⠪⠕⠀⠊⠞⠜⠀⠨⠉

    ⠀⠀⠨⠊⠀⠽⠀⠁⠀⠜⠀⠇⠊⠎⠞⠱⠀⠝⠥⠍⠿⠗⠕⠞⠿⠱⠂⠀⠿⠛⠍⠒
    ⠀⠀⠠⠡⠲ ⠖⠍
    ⠀⠀⠠⠣⠲ ⠙⠐⠭⠊⠮⠍⠑
    ⠀⠀⠠⠩⠲ ⠴⠾⠎⠊⠮⠍⠑

    ⠀⠀⠨⠜⠀⠙⠢⠱⠀⠏⠥⠉⠱⠢⠀⠨⠝⠬⠂⠀⠏⠎⠀⠯⠀⠇⠀⠍⠢⠲
    ⠀⠀⠨⠕⠀⠁⠸⠬⠎⠀⠍⠞⠀⠧⠾⠗⠀⠟⠀⠇⠀⠏⠁⠗⠁⠻⠁⠏⠓⠑⠀⠎⠆⠧⠂⠞⠑⠀⠱
    ⠡⠀⠎⠇⠀⠓⠀⠎⠁⠀⠏⠁⠛⠑⠂⠀⠉⠈⠀⠊⠀⠽⠀⠁⠀⠥⠀⠎⠅⠞⠀⠙⠀⠏⠁⠛⠑
    ⠁⠧⠂⠀⠾⠀⠥⠀⠁⠖
    ⠀⠀⠨⠡⠀⠎⠇⠀⠓⠀⠄⠀⠏⠁⠛⠑
    ⠀⠀⠨⠾⠀⠕⠀⠧⠇⠀⠷⠀⠄⠀⠋⠔⠀⠦⠀⠙⠕⠉⠥⠍⠣⠲
    ⠀⠀⠨⠝⠬⠀⠏⠎⠀⠡⠤⠷⠤⠋⠩⠖
    ⠀⠀⠨⠊⠀⠕⠀⠄⠎⠞⠑⠀⠢⠉⠀⠷⠀⠧⠾⠗⠀⠥⠀⠎⠞⠽⠇⠑⠀⠏⠝⠇⠞⠀⠙⠀⠏⠁⠤
    ⠗⠁⠻⠁⠓⠑

    ⠨⠉⠄⠱⠀⠇⠀⠎⠞⠽⠇⠑⠀⠏⠕⠑⠎⠊⠑
    ⠇⠀⠎⠞⠽⠇⠑⠀⠌⠍⠿⠀⠏⠀⠜⠀⠏⠕⠮⠞⠱
    ⠉⠭⠀⠛⠀⠚⠳⠣⠀⠁⠉⠀⠜⠀⠍⠕⠞⠎
    ⠮⠀⠎⠀⠋⠁⠞⠊⠛⠥⠦⠀⠎⠅⠋⠀⠉⠭⠀⠛⠀⠈⠗⠣⠞⠣⠲
    ⠨⠾⠀⠖⠀⠅⠎⠀⠥⠀⠞⠴⠀⠠⠹

    ⠀⠀⠨⠉⠄⠱⠀⠞⠦⠍⠊⠝⠿⠲
    ⠀⠀⠨⠱⠤⠉⠀⠟⠀⠉⠁⠀⠒⠧⠊⠣⠢
    ⠀⠀⠨⠍⠉⠖
    ⠀⠀⠠⠡⠲ 

## 5. BrailleTable Converter

                ¸¨jean-¨m^c
                    ¨u ty

    ¨g 5 ¨jean-¨m^c? ¨ó v(ra b!
                ¨les elements
    ¨5-c q ¨jean-¨m^c 5 u ta¤ek?
            ¨un tableau `2x2?
        ¨ou encore un tableau fusion
        ¨ou encore une formule de maths?
        ¨cl-ci 5 h sa l", sle
    2´*2"78/23
        ¨k) àx
    ¨? m) un h ' l" `2´*2"78/23
    ¨?s, i y a :¨tts @ hts d majuscul5 à '
    ¨s;te g ót u )/tm spécial
    ¨i y a ks u ¸mot ? italiqs. ¨8 al :¸1
    u b8t d phrase g 5 ? ¸italiqs ù c'5 b 9
    4ùt. ¨? 6 ds italiqs, i y a @ mots ?
    ¸gras. ¨8 al :¸1 u b8t d phrase g 5 ?
    ¸7as ù c'5 b 9 4ùt.
    ¨5-c q c'5 1?
    ¨nó, c^ i y a ks @ fam´x skts d page,
    p¸e a! l para7aphe s;v4
    ¨a! celui-ci ? pl
    ¨ó 5 paèé à ' page s;v,te.
    ¨x i y ks u k) g?re d skt d page
    ¨cm l skt d page a! c para7aphe
    ¨o ? sws à ' page )ùs, o a¸ós skt( qqs
    l"s



    ¨ù o vl u p´ k mil` d ' l".
        ¨q s paèe-t-i lq l txte 5 c?)é?
    ¨8 ?c justifié à 4ùte?
    ¨i 5 tp d'ab„d( mt @ list5 à puc5.
    9o it@ ¨a
        9o ) it@ ¨a`.1
        9o ) it@ ¨a`.2

      9o it@ ¨b
        9o ) it@ ¨b`.1
        9o ) it@ ¨b`.2
        9o )-) it@ ¨b`.1.a
        9o )-) it@ ¨b`.1.b
    9o it@ ¨c

    ¨i y a @ list5 numéroté5, égm:
    `1. !m
    `2. d´xième
    `3. )ùsième

    ¨@ d?5 puc5? ¨nó, ps ç l m?.
    ¨o a¸ós mt vùr q l para7aphe s;v,te 5
    1 sl h sa page, c^ i y a u skt d page
    av, ù u a!
    ¨1 sl h ' page
    ¨ù o vl à ' f* ( docum2.
    ¨nó ps 1-à-f3!
    ¨i o 'ste ?c à vùr u style pnlt d pa-
    ra7ahe

    ¨c'5 l style poesie
    l style /mé p @ poèt5
    cx g j82 ac @ mots
    è s fatigu( skf cx g ^r2t2.
    ¨ù ! ks u t) `4

    ¨c'5 t(miné.
    ¨5-c q ca :vi2?
    ¨mc!
    `1. 


# Format combiné idéal

## Exemple pour le littéraire

Un cas simple (au niveau structure) est le cas d'une expression littéraire, sans ambiguité

### découpage par mot avec frag-noir frag-brl, hors mise en page

    <doc:doc>
        <phrase styleOrig="Title" center="true" modeBraille="3-1">
            <lit>
                <frag-noir><mot mev="1">Jean-Marc</mot></frag-noir>
                <frag-brl>⠸⠨⠚⠑⠁⠝⠤⠨⠍⠈⠉</frag-brl>
            </lit>
        </phrase modeBraille="3-1>           
        <phrase>
            <lit>
                <frag-noir><mot>Qui</mot></frag-noir>
                <frag-brl>⠨ⴹ⠛</frag-brl>
                <frag-noir><mot>ⴹ⠱</mot>
                <frag-brl></frag-brl>
                <frag-noir><mot>Jean-Marc</mot>
                <frag-brl>⠨⠚⠑⠁⠝⠤⠨⠍⠈⠉</frag-brl>
                <frag-noir><ponctuation>?</ponctuation></frag-noir>
                <frag-brl>⠢</frag-brl>
            </lit>
        </phrase>    
    </doc:doc>

note: 
- les "ⴹ" sont des caractères de coupure destinés à l'étape ultérieure de mise en page
- Ce format contient les informations des formats 2 (convertisseurXML) et 3 (transcodeur normal), mais pas celui de la mise en page.

### découpage par mot avec frag-noir frag-brl, avec info de mise en page

On peut envisager d'ajouter les informations de mise en page par un attribut de début et de fin "loc:page:ligne:offset" sur frag-mep

    <doc:doc>
        <phrase styleOrig="Title" center="true" modeBraille="3-1">
            <lit>
                <frag-noir><mot mev="1">Jean-Marc</mot></frag-noir>
                <frag-brl début="1:1:3" fin="">⠸⠨⠚⠑⠁⠝⠤⠨⠍⠈⠉</frag-brl>
            </lit>
        </phrase modeBraille="3-1>           
        <phrase>
            <lit>
                <frag-noir><mot>Qui</mot></frag-noir>
                <frag-brl >⠨ⴹ⠛</frag-brl>
                <frag-mep début="1:2:1" fin="1:2:4">⠨ⴹ⠛</frag-mep>
                <frag-noir><mot>ⴹ⠱</mot>
                <frag-brl ></frag-brl>
                <frag-mep début="1:2:5" fin="1:2:8"></frag-mep>
                <frag-noir><mot>Jean-Marc</mot>
                <frag-brl>⠨⠚⠑⠁⠝⠤⠨⠍⠈⠉</frag-brl>
                <frag-mep début="1:2:8" fin="1:3:10">⠨⠚⠑⠁⠝⠤\n⠨⠍⠈⠉</frag-mep>
                <frag-noir><ponctuation>?</ponctuation></frag-noir>
                <frag-brl >⠢</frag-brl>
                <frag-mep début="1:3:10 fin="1:3:11">⠢</frag-mep>
            </lit>
        </phrase>    
    </doc:doc>

notes :
- les localisations sont arbitraires (et destinées à faire figurer un changement de ligne)
- difficulté : les sauts de pages/lignes tombent au milieu de mots, il n'est donc pas toujours possible de savoir quelle page afficher (et sur quelle page synchroniser le curseur) du document braille.
- Ce format contient des informations de mise en page, mais pas
- Ce format n'inclue pas la numérotation des pages. La numérotation des pages n'est pas dans le document d'origine. 
===> Comment l'inclure ?

### réalisation de ce format

Obtenir un tel document à partir de natbraille demande de prédécouper le document au niveau du mot/ groupe de mot. Or il est délicat de déterminer dans quels cas précisément la transcription d'un mot par natbraille dépend de ce qui suit (par exemple pour la transcription : expression composée de plusieurs mots ; suivi d'espace ou de ponctuation, de changement de code ; ou pour la mise en page : aliénéa de début de ligne, fin de ligne, fin de page). 

Tous ces cas sont donc à étudier très finement et à valider un par un : il ne semble pas possible de faire une règle générale. Le cas général est par contre possible à déterminer en découpant non pas par mot mais par paragrahe.

### découpage par mot avec ambiguités

    <phrase modeBraille="3-1>
      <lit>
         <frag-noir><mot>Est-ce</mot></frag-noir>
         <frag-brl>⠨ⴹ⠱⠤ⴹ⠉</frag-brl>
         <frag-mep début="x:y:z" fin="x:y:z">⠨⠱⠤⠉</frag-brl>         

         <frag-noir><mot>que</mot></frag-noir>
         <frag-brl>ⴹ⠟</frag-brl>
         <frag-mep début="x:y:z" fin="x:y:z">⠟</frag-brl>         

         <frag-noir><mot>cela</mot></frag-noir>
         <frag-brl>ⴹ⠉⠁</frag-brl>
         <frag-mep début="x:y:z" fin="x:y:z">⠉⠁</frag-brl>         

         <frag-noir><mot>convient</mot></frag-noir>
         <frag-brl type="amb" choix="1">
            <q>Verbe convier (1) ou convenir (2)</q>
            <choix>⠊⠣</choix>
            <choix>⠲⠞</choix>
         </frag-brl>
         <frag-mep début="x:y:z" fin="x:y:z">⠊⠣</frag-brl>         

        <frag-noir><ponctuation>?</ponctuation></frag-noir>
        <frag-brl>⠢</frag-brl>
        <frag-mep début="x:y:z" fin="x:y:z">⠢</frag-brl>         
   </lit>
   </phrase>
   
remarques
- les espaces entre les mots n'apparaissent qu'implicitement dans les tags début et fin.
==> les faire apparaitre explicitement en changeant MEP ?

## tableaux

Traiter le cas particulier des tableaux de variation

## Maths

Les maths sont traités en première approche expression par expression. Le traitement est donc totalement identique à un mot/groupe de mot

    <phrase styleOrig="Standard" modeBraille="3-1">
        <frag-noir>
            <math ...>
                <mrow>
                <mrow>
                    <mn>2</mn>
                    <mo stretchy="false">+</mo>
                    <mn>2</mn>
                </mrow>
                <mo stretchy="false">=</mo>
                <mn>4</mn>
                </mrow>
            </math>
        </frag-noir>
        <frag-brl>ⴲ⠣ⴰ⠖⠣ⴱ⠶⠹ⴳ</frag-brl>
        <frag-mep début="1:1:1" fin="1:1:6">⠣⠖⠣⠶⠹</frag-brl>
    </phrase>

# Document

Le document peut être sauvegardé par l'utilisateur.

Afin d'inclure à ce document de potentielles metadonnées ainsi que les différents fichiers de configuration (règles d'abrégé, options de document) simplement, il pourra être fait usage d'un format similaire à libreoffice : chacun des fichiers est sauvegardé à côté du document proprement dit et enveloppé dans un zip (par ex avec l'extension .natbraille)

De cette manière, 

# Web / Poste

# Editeurs Web

## textarea 

utiliser prosemirror

https://letsken.com/michael/how-to-implement-a-web-based-rich-text-editor-in-2023

