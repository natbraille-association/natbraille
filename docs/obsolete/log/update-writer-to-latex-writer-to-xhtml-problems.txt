= svn server

https://sourceforge.net/p/writer2latex/code/HEAD/tree/branches/w2x-stable1.7/

w2x-stable1.7 	2022-09-14 	henrikjust 

svn checkout -r 441  https://svn.code.sf.net/p/writer2latex/code/trunk writer2latex-code-r441

= json lib version

Json (as a jar) 2014010

/writer2latex-code-r441/source/lib/json-20140107.jar

Natbraille (server api) uses

  <dependency>
      <groupId>org.json</groupId>
      <artifactId>json</artifactId>
      <version>20170516</version>
      <scope>compile</scope>
  </dependency>
        
= bibtext lib version

/writer2latex-code-r441/source/lib/jbibtex-1.0.14.jar

avail on maven central 

  <dependency>
      <groupId>org.jbibtex</groupId>
      <artifactId>jbibtex</artifactId>
      <version>1.0.20</version>
  </dependency>
svn co https://svn.code.sf.net/p/writer2latex/code/branches/w2x-stable1.7
