# extract messages

xgettext  --from-code='UTF-8' --language=Java --output="xxxxxxxxxxxx.pot" /home/vivien/src/natbraille/natbraille-brailletable/src/main/java/org/natbraille/brailletable/ui/BrailleTableCli.java  -ktrc:1c,2 -ktrnc:1c,2,3 -ktr -kmarktr -ktrn:1,2 -k


# echo "== brailletable"
# msgfmt --java2 -d /home/vivien/src/natbraille/natbraille-brailletable/target/classes -r org.natbraille.brailletable.Messages -l fr_FR /home/vivien/src/natbraille/natbraille-brailletable/src/main/po/org/natbraille/brailletable/fr_FR.po

msgfmt --java2 -d /home/vivien/src/natbraille/natbraille-brailletable/build/classes/java/main -r org.natbraille.brailletable.Messages -l fr_FR /home/vivien/src/natbraille/natbraille-brailletable/src/main/po/org/natbraille/brailletable/fr_FR.po
touch /home/vivien/src/natbraille/natbraille-brailletable/build/classes/java/main/org/natbraille/brailletable/Messages.properties

# echo "== core1"
# #msgfmt --java2 -d /home/vivien/src/natbraille/natbraille-core/target/classes -r org.natbraille.core.i18n.Messages -l fr_FR /home/vivien/src/natbraille/natbraille-core/src/main/java/../resources/org/natbraille/core/i18n/fr_FR.po
# echo "== core2"


 msgfmt --java2 -d /home/vivien/src/natbraille/natbraille-core/build/classes/java/main -r org.natbraille.core.i18n.Messages -l fr_FR /home/vivien/src/natbraille/natbraille-core/src/main/java/../resources/org/natbraille/core/i18n/fr_FR.po

# echo "== font"
# msgfmt --java2 -d /home/vivien/src/natbraille/natbraille-font/target/classes -r org.natbraille.font.Messages -l fr_FR /home/vivien/src/natbraille/natbraille-font/src/main/po/org/natbraille/font/fr_FR.po

# echo "== ocr"
# msgfmt --java2 -d /home/vivien/src/natbraille/natbraille-ocr/target/classes -r org.natbraille.ocr.Messages -l fr_FR /home/vivien/src/natbraille/natbraille-ocr/src/main/po/org/natbraille/ocr/fr_FR.po

 msgfmt --java2 -d /home/vivien/src/natbraille/natbraille-brailletable/target/classes -r org.natbraille.brailletable.Messages -l fr_FR /home/vivien/src/natbraille/natbraille-brailletable/src/main/po/org/natbraille/brailletable/fr_FR.po


for i in $(find . -name 'i18n.properties' | grep  '/build/') ;
do
    echo $i ;
    cat $i;
    echo ;
    BASENAME=` cat "$i" | sed s/basename=// `;
    echo "->" $BASENAME
    echo "\n"
done
