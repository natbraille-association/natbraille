parser grammar demathParser ;
// ⠠⠄⠦⠩⠩⠩⠖⠦⠁⠁⠖⠩⠴⠴
// ⠠⠄⠜⠩⠩⠩⠖⠁⠆             /// ????
// ⠠⠄⠜⠰⠩⠩⠩⠖⠁⠆
// ⠠⠄⠩⠌⠩⠌⠦⠁⠖⠩⠴
// ⠠⠄⠖⠩
// ⠠⠄⠨⠁⠘⠔⠨⠃
// ⠠⠄⠨⠲⠨⠲⠨⠁ ??
// ⠠⠄⠜⠁
// ⠠⠄⠁⠜⠁⠖⠩⠶⠁
// compare ⠠⠄⠜⠁⠖⠩ to ⠠⠄⠜⠁⠖⠩⠶⠜⠁⠁⠁⠖⠩
options { tokenVocab = demathLexer; }


prefixedExpression
    : mathPrefix equation EOF
    ;

mathPrefix : D6 D3 ;

// blockBarreMultiligne :   barreMultilignes expression ;
// blockDoubleBarreMultiligne :   doubleBarreMultilignes expression ;

bloc : blocOuvrant equation blocFermant ;

bloc1 : parentheseOuvrante equation parentheseFermante ;
bloc2 : grandeParentheseOuvrante equation grandeParentheseFermante ;
bloc3 : parentheseOuvranteMultilignes equation parentheseFermanteMultilignes ;
bloc4 : accoladeOuvrante equation accoladeFermante;
bloc5 : accoladeOuvranteMultilignes equation accoladeFermanteMultilignes ;
bloc6 : crochetOuvrant equation crochetFermant ;
bloc7 : grandCrochetOuvrant equation grandCrochetFermant;
bloc8 : crochetOuvrantMultilignes equation crochetFermantMultilignes ;
bloc9 : crochetDoubleOuvrant equation crochetDoubleFermant;
bloc10 : barre equation barre;
bloc11 : barreMultilignes  equation barreMultilignes ;
bloc12 : doubleBarre equation doubleBarre ;
bloc13 : doubleBarreMultilignes equation doubleBarreMultilignes ;

balancedSBloc
   : bloc1 | bloc2 | bloc3 | bloc4 | bloc5
   | bloc6 | bloc7 | bloc8 | bloc9 | bloc10
   | bloc11 | bloc12 | bloc13
   ;

unbalancedSBlocSymbol
    : parentheseOuvrante | parentheseFermante
    | grandeParentheseOuvrante | grandeParentheseFermante
    | parentheseOuvranteMultilignes | parentheseFermanteMultilignes
    | accoladeOuvrante | accoladeFermante
    | accoladeOuvranteMultilignes | accoladeFermanteMultilignes
    | crochetOuvrant | crochetFermant
    | grandCrochetOuvrant | grandCrochetFermant
    | crochetOuvrantMultilignes | crochetFermantMultilignes
    | crochetDoubleOuvrant | crochetDoubleFermant
    | barre
    | barreMultilignes
    | doubleBarre
    doubleBarreMultilignes
;

relop
    : plusGrandOuEgal | egal
    ;

equation
   : expression (relop? expression)*
   ;

expression
   : multiplyingExpression ((plus | moins) multiplyingExpression)*
   ;

fraction
   : powExpression ( barreFraction powExpression)*
   ;

mutliplyLevelSymbol
    : multiplie
    // | grpOperatorPt45 // should go in relop ?
    ;

multiplyingExpression
   : fraction
   | ( powExpression ( mutliplyLevelSymbol powExpression)* )
   ;

powSign
   : exposant | indice
   ;

powExpression
   : signedAtom (powSign signedAtom)*
;

signedAtom
   : ( plus signedAtom )
   | ( moins signedAtom )
   | func
   | atom
   ;

func :
    sqrt;

sqrt :
    (racine number)
    | (racine lowercaseLetters)
    | (racine bloc)
    | (racine balancedSBloc) ;


number
    : grpChiffres+
    ;

lowercaseLetters
    : grpAlphabet+
    ;

greekLetter
    : D45 grpAlphabet
    ;

uppercaseLetter
    : majuscule grpAlphabet
    ;

letters
    : (greekLetter | uppercaseLetter | lowercaseLetters )
    ;

trailing
    : relop
    | plus
    | powSign
    | mutliplyLevelSymbol
    ;
    // ...
    // all "unbalanced" = "trailing" ops

atom
    :     bloc
    | balancedSBloc
    | unbalancedSBlocSymbol
    // | sqrt
    | number
    | letters
    | trailing
    ;



plus : D235 ;
grandPlus : D5 D235 ;
plusDansRond : D46 D235 ;
grandPlusDansRond : D46 D46 D235 ;
pourcent : D5 D346 ;
pourmille : D5 D346 D346 ;
produitTensoriel : D46 D35 ;
grandProduitTensoriel : D46 D46 D35 ;
differentDe : D46 D2356 ;
nonCongru : D46 D2356 D2356 ;
posterieur : D46 D46 D345 ;
anterieur : D46 D46 D126 ;
inclus : D46 D16 ;
appartientSymetrique : D46 D45 D16 ;
nonAppartientSymetrique : D46 D45 D34 ;
complementaire : D46 D146 ;
nabla : D46 D1456 ;
nonExiste : D46 D456 D16 ;
union : D456 D235 ;
grandUnion : D456 D456 D235 ;
doubleMultiplication : D456 D456 D35 ;
parallele : D456 D1256 ;
pointExclamation : D456 D35 ;
existe : D456 D16 ;
existeUnique : D456 D16 D235 ;
pourTous : D456 D34 ;

//  Bruno
pourTousNeg : D46 D456 D34 ;

//  pas trouvé en mathml...)
operateurRond : D456 D3456 ;
flecheEst : D456 D156 ;
flecheDoubleEstOuest : D456 D12456 ;
flecheOuest : D456 D246 ;
egalTilde : D456 D2356 ;
carre : D456 D1456 ;
sousEnsemble : D456 D46 D16 ;
nonSousEnsemble : D456 D46 D34 ;
doubleTildeSouligne : D456 D5 D2356 ;
nonInclus : D46 D34 ;
asterisque : D5 D35 ;
grandMultiplie : D5 D5 D35 ;
moins : D36 ;
multiplie : D35 ;
virgule : D2 ;
point : D256 ;
pointVirgule : D23 ;

grpOperatorSimple : plus | moins | multiplie | signeDivision | egal ;
// grpOperatorSimpleMono : plus | plus | plus | plus ;
// chaineChiffres : D3456 D16 D126 D146 D1456 D156 D1246 D12456 D1256 D246 ;
grpChiffres : un | deux | trois | quatre | cinq | six | sept | huit | neuf | zero ;

a : D1 ;
b : D12 ;
c : D14 ;
d : D145 ;
e : D15 ;
f : D124 ;
g : D1245 ;
h : D125 ;
i : D24 ;
j : D245 ;
k : D13 ;
l : D123 ;
m : D134 ;
n : D1345 ;
o : D135 ;
p : D1234 ;
q : D12345 ;
r : D1235 ;
s : D234 ;
t : D2345 ;
u : D136 ;
v : D1236 ;
w : D2456 ;
x : D1346 ;
y : D13456 ;
z : D1356 ;

integrale : D12346 ;
integraleCurviligne : D46 D12346 ;
integraleDouble : D12346 D12346 ;
integraleSurface : D46 D12346 D12346 ;
integraleTriple : D12346 D12346 D12346 ;
integraleVolume : D46 D12346 D12346 D12346 ;
barre : D123456 ;
barreMultilignes : D456 D123456 ;
doubleBarre : D45 D123456 ;
doubleBarreMultilignes : D46 D123456 ;

// <xsl:variable name="aAccentAigu">
//   <xsl:text>&pt12356;</xsl:text>
// </xsl:variable>
//
// <xsl:variable name="eAccentAigu">
//   <xsl:text>&pt2346;</xsl:text>
// </xsl:variable>
//
// <xsl:variable name="uAccentAigu">
//   <xsl:text>&pt23456;</xsl:text>
// </xsl:variable>
zero : D3456 ;
un : D16 ;
deux : D126 ;
trois : D146 ;
quatre : D1456 ;
cinq : D156 ;
six : D1246 ;
sept : D12456 ;
huit : D1256 ;
neuf : D246 ;

grpAlphabet : a | b | c | d | e | f | g | h | i | j | k | l | m | n | o | p | q | r | s | t | u | v | w | x | y | z ;
point46 : D46 ;
point5 : D5 ;
blocOuvrant : D56 ;

//  Le bdfgdfgdloc braille n'a pas de sens en noir, on ne le remplace donc par aucun caractère
blocFermant : D23 ;

//  Le bloc braille n'a pas de sens en noir, on le remplace donc par aucun caractère
parentheseOuvrante : D236 ;
parentheseFermante : D356 ;
grandeParentheseOuvrante : D5 D236 ;
grandeParentheseFermante : D5 D356 ;
parentheseOuvranteMultilignes : D45 D236 ;
parentheseFermanteMultilignes : D45 D356 ;
accoladeOuvrante : D46 D236 ;
accoladeFermante : D46 D356 ;
accoladeOuvranteMultilignes : D456 D236 ;
accoladeFermanteMultilignes : D456 D356 ;
crochetOuvrant : D12356 ;
crochetFermant : D23456 ;
grandCrochetOuvrant : D5 D12356 ;
grandCrochetFermant : D5 D23456 ;
crochetOuvrantMultilignes : D45 D12356 ;
crochetFermantMultilignes : D45 D23456 ;
crochetDoubleOuvrant : D46 D12356 ;
crochetDoubleFermant : D46 D23456 ;
surEnsemble : D5 D16 ;
nonSurEnsemble : D5 D34 ;
flecheSudEst : D46 D156 ;
flecheSud : D46 D12456 ;
flecheSudOuest : D46 D246 ;
intersection : D45 D235 ;
grandeIntersection : D45 D45 D235 ;
produitVectoriel : D45 D35 ;
grandProduitVectoriel : D45 D45 D35 ;
precedeOuEgal : D45 D45 D126 ;
suitOuEgal : D45 D45 D345 ;
ou : D45 D26 ;
tilde : D45 D2356 ;
plusGrandOuEgal : D45 D345 ;
plusPetitOuEgal : D45 D126 ;
flecheNordEst : D45 D156 ;
flecheNord : D45 D12456 ;
flecheNordOuest : D45 D246 ;
perpendiculaire : D45 D1256 ;
appartient : D45 D16 ;
nonAppartient : D45 D34 ;
diametre : D45 D3456 ;
aleph : D45 D45 D1 ;
beth : D45 D45 D12 ;
daleth : D45 D45 D145 ;
gimel : D45 D45 D1245 ;
infini : D45 D14 ;
flecheTaquetDroite : D5 D156 ;
flecheBilaterale : D5 D12456 ;
doubleFlecheGauche : D5 D25 ;
doubleFlecheDroite : D25 D2 ;
doubleFlecheGaucheBarree : D46 D5 D25 ;
doubleFlecheDroiteBarree : D46 D25 D2 ;
doubleFlecheBilaterale : D5 D25 D2 ;
doubleFlecheBilateraleBarree : D46 D5 D25 D2 ;
inferieur : D5 D126 ;
apostrophe : D3 ;
superieur : D5 D345 ;
superieurInferieur : D5 D345 D5 D126 ;
tresInferieur : D5 D5 D126 ;
tresSuperieur : D5 D5 D345 ;
egal : D2356 ;
plusOuMoins : D235 D36 ;
moinsOuPlus : D36 D235 ;
produitScalaire : D35 D35 ;
congru : D2356 D2356 ;
egalDelta : D25 D2356 ;
environEgal : D5 D2356 ;
exposant : D4 ;
indice : D26 ;
racine : D345 ;
barreFraction : D34 ;
signeDivision : D25 ;
majuscule : D46 ;
point45 : D45 ;
point36 : D36 ;
point456 : D456 ;
arc : D4 D25 ;
angleSaillant : D45 D25 ;
vecteur : D46 D25 ;

// Il s'agit de la fleche est
tenseur : D46 D25 D4 D1345 ;
barreHorizontale : D456 D25 ;
demiRondConvexeBas : D4 D4 D25 ;
angleRentrant : D456 D45 D25 ;
vecteurAxial : D5 D46 D25 ;
tildeSurscrit : D5 D456 D25 ;
flecheCirculaireNegatif : D45 D4 D25 ;
tRenverse : D45 D45 D25 ;
dague : D45 D46 D25 ;
doubleBarreHorizontale : D45 D456 D25 ;
flecheOuestSurscrit : D456 D46 D25 ;

// Regroupe les signes surscrits
grpSurscrit : arc | angleSaillant | vecteur | tenseur | barreHorizontale | demiRondConvexeBas | angleRentrant | vecteurAxial | tildeSurscrit | flecheCirculaireNegatif | tRenverse | doubleBarreHorizontale | flecheOuestSurscrit | dague ;

// Équivalent MathML de l'entité ci-dessus

// Bruno: Regroupe les signes non surscrits commençant comme un signe suscrit
grpNonSurscrit : doubleFlecheDroiteBarree ;

// Regroupe les préfixes classiques qui débutent les opérateurs
grpPrefixe : point5 | point45 | point46 | point456 ;

// Regroupe les débuts possible des opérateurs et identifier qui ne commencent pas par un préfixe classique
grpDebutSansPrefixe : plus | moins | multiplie | signeDivision | barreFraction | egal | pointVirgule ;

//  Sequence des symboles braille de deux caractères signifiant le début d'un bloc SUR PLUSIEURS LIGNES (utile pour tableau, matrices, systèmes...)
grpOuvrantMultilignes : parentheseOuvranteMultilignes | accoladeOuvranteMultilignes | crochetOuvrantMultilignes | barreMultilignes | doubleBarreMultilignes ;

//  Équivalent MathML de la séquence précédente

//  Sequence des symboles braille de deux caractères signifiant le début d'un bloc
grpOuvrantDouble : grandeParentheseOuvrante | parentheseOuvranteMultilignes | accoladeOuvrante | accoladeOuvranteMultilignes | grandCrochetOuvrant | crochetOuvrantMultilignes | crochetDoubleOuvrant ;

//  Équivalent MathML de la séquence précédente

//  Sequence des symboles braille de deux caractères signifiant la fin d'un bloc
grpFermantDouble : grandeParentheseFermante | parentheseFermanteMultilignes | accoladeFermante | accoladeFermanteMultilignes | grandCrochetFermant | crochetFermantMultilignes | crochetDoubleFermant ;

//  Équivalent MathML de la séquence précédente

//  Sequence des symboles braille de deux caractères signifiant la fin d'un bloc SUR PLUSIEURS LIGNES (utile pour tableau, matrices, systèmes...)
grpFermantMultilignes : parentheseFermanteMultilignes | accoladeFermanteMultilignes | crochetFermantMultilignes | barreMultilignes | doubleBarreMultilignes ;

//  Équivalent MathML de la séquence précédente

//  Sequence des symboles braille d'un caractère signifiant le début d'un bloc
grpOuvrantSimple : blocOuvrant | parentheseOuvrante | crochetOuvrant ;

//  Équivalent MathML de la séquence précédente

//  Sequence des symboles braille d'un caractère signifiant la fin d'un bloc
grpFermantSimple : blocFermant | parentheseFermante | crochetFermant ;

//  Équivalent MathML de la séquence précédente

// Ensemble des caractères braille composés qui peuvent poser problème lors des détection, car mélangeant les caractères débutant les caractères spéciaux.
grpOperatorSpeciaux : doubleTildeSouligne | doubleFlecheGaucheBarree | doubleFlecheBilateraleBarree | sousEnsemble | nonSousEnsemble | appartientSymetrique | nonAppartientSymetrique ;

//  Traduction MathMl de l'élément précédent

// Sequence de même longueur que les deux d'en-dessus, utile pour effectuer des remplacements
grpOperatorSpeciauxMono : doubleTildeSouligne | doubleTildeSouligne | doubleTildeSouligne | doubleTildeSouligne | doubleTildeSouligne | doubleTildeSouligne | doubleTildeSouligne ;

// Ensemble des caractères braille composés qui ne commencent pas par un préfixe classique et sont traduits par un operator MathML.
grpOperatorSansPrefixe : plusOuMoins | moinsOuPlus | produitScalaire | integraleDouble | integraleTriple | congru | egalDelta | doubleFlecheDroite ;

//  Traduction MathMl de l'élément précédent

// Ensemble des caractères hébreux disponibles en MathML
grpHebraique : aleph | beth | daleth | gimel ;

// Traduction MathML de seqHebraique".

// Ensemble des caractères braille composés qui débutent par le point45 et qui sont traduits par des OPERATOR MathMl. Note : remplacement des grands crochet par des crochets simples
grpOperatorPt45 : grandProduitVectoriel | produitVectoriel | ou | intersection | grandeIntersection | precedeOuEgal | suitOuEgal | tilde | plusGrandOuEgal | plusPetitOuEgal | flecheNordEst | flecheNord | flecheNordOuest | perpendiculaire | appartient | nonAppartient | diametre | doubleBarre ;

// Traduction MathML de seqIdentifierPt45.

// Ensemble des caractères braille composés qui débutent par le point456 et qui sont traduits par des OPERATOR MathMl
grpOperatorPt456 : operateurRond | carre | doubleMultiplication | union | grandUnion | parallele | pointExclamation | existe | existeUnique | pourTous | flecheEst | flecheDoubleEstOuest | flecheOuest | egalTilde | carre | grpBarreLetters ;

// Traduction MathML de seqIdentifierPt456.

// Ensemble des caractères braille composés qui débutent par le point5 et qui sont traduits par des OPERATOR MathMl
grpOperatorPt5 : pourmille | pourcent | grandPlus | grandMultiplie | asterisque | environEgal | tresInferieur | tresSuperieur | superieurInferieur | inferieur | superieur | surEnsemble | nonSurEnsemble | flecheTaquetDroite | flecheBilaterale | doubleFlecheGauche | doubleFlecheBilaterale ;

// Traduction MathML de seqIdentifierPT5. Note : il n'existe pas d'équivalent MathML au grandPlus et grandMultiplie, ils sont remplacés par des simple plus et multipliés."

// Ensemble des caractères braille composés qui débutent par le point46 et qui sont traduits par des OPERATOR MathMl

//  Bruno: j'ajoute aussi vecteur, nonexiste, n'implique pas
grpOperatorPt46 : vecteur | nonExiste | pourTousNeg | plusDansRond | grandPlusDansRond | produitTensoriel | grandProduitTensoriel | nabla | integraleCurviligne | integraleSurface | integraleVolume | anterieur | posterieur | differentDe | nonCongru | inclus | nonInclus | appartientSymetrique | nonAppartientSymetrique | complementaire | doubleFlecheDroiteBarree | doubleFlecheGaucheBarree | doubleFlecheBilateraleBarree | flecheSudEst | flecheSud | flecheSudOuest ;

// Traduction MathML de seqIdentifierPT46.

//  lettres "bar"
grpBarreLetters : ( D456 D36 D125  ) | ( D456 D36 D123 ) ;

