//import { DOMParser } from '@xmldom/xmldom'
import { readFileSync } from "fs"
import { createSaxParser } from 'tag-soup';
console.log('@')
//new DOMParser().parseFromString(xml, mimeType) => Document
const files = [
    //'../after/ConvertisseurOpenOffice14144660826672285042/output_17874195862854064207',
    '../after/ConvertisseurXML7560777501623907058/output_12905319393757371577',
 //   '../after/TranscodeurNormal17381888184621005378/output_1452638066750217881',
    //'../after/AmbiguityFilter18194081090103919441/output_3692682870645656502',
 
    //'../after/BrailleTableModifierJava5311132718569690745/output_18214171040864001970',
    //'../after/PresentateurMEP4068980750015497891/output_7936216079172171916.utf8_TbFr2007',
]

files.forEach(file => {
    const xmlText = readFileSync(file, 'utf8')//.replaceAll("/>"," />")
    //console.log(file)
    //console.log(xmlText)
    const stack = []
    const saxParser = createSaxParser({
        startTag(token) {
            stack.push(token.name)
            console.log(stack.join(" > "))//, token.selfClosing)
            if (token.selfClosing) {
                stack.pop()
            }

            
            if (token.attributes) {
                for ( let i = 0 ; i < token.attributes.length ; i++) {
                    const attribute = token.attributes[i]
                    console.log(stack.join(" - "), '@', attribute.name, '=', attribute.value)                    
                }

            }

            //console.log(token)
        },
        endTag(token) {
            stack.pop()
        },
    }, {
        selfClosingEnabled: true,

    });
    saxParser.parse(xmlText);

})
//../after/PresentateurMEP4068980750015497891/output_7936216079172171916
