# writer2latex / writer2xhtml / w2x

Conversion to `xhtml` from `OpenDocument` format. 

by Henrik Just <writer2latex@gmail.com>

## License

Licensed under The GNU General Public License (GPL) version 2

See `LICENSE` and `̀ADDITIONAL_LICENSE_INFO` files in this directory

## Source

Retrieved on SVN, the branch `w2x-stable1.7`, 29 nov 2022

    svn co https://svn.code.sf.net/p/writer2latex/code/branches/w2x-stable1.7

## Adaptations

The following adaptations for usage in natbraille: 

- keep only `writer2xhtml.*` package (in dir. `src/main/java/writer2xhtml/` )
- move `XHtmlStrings` resource bundle to `src/main/resources/writer2xhtml/xhtml/l10n`
- add a `build.gradle.kts` for building with gradle file 
- this `README.md`