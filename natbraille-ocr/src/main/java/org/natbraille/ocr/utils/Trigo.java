/*
 * NatBraille - Ocr - An universal Translator
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.natbraille.ocr.utils;



/**
 *
 * @author vivien
 */
public class Trigo {

    /* ========================================
     * trigo utils
     *
     */
    public static double hypo(double a, double b) {
        return Math.pow(
                Math.pow(a, 2) + Math.pow(b, 2),
                0.5);
    }

    public static double minrotationangle(double w, double h) {
        double hy = hypo(w, h);
        double cosa = (1.0 / hy) * (1 - (1 / (2 * hy)));
        return (Math.PI / 2.0) - Math.acos(cosa);

    }

    public static double[] getSteps(double min, double max, int steps) {
        double d = max - min;
        double stepsize = d / (double) (steps - 1);
        double angles[] = new double[steps];
        for (int step = 0; step < steps; step++) {
            angles[step] = min + (stepsize * step);
        }
        return angles;
    }

}
