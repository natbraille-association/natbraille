/*
 * NatBraille - Ocr - An universal Translator
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.natbraille.ocr.utils;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.color.ColorSpace;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 *
 * @author vivien
 */
public class G2D {

    /* ========================================
     * g2d utils
     *
     */
    public static void save(BufferedImage img, String filename) {
        try {
            ImageIO.write(img, "png", new File(filename));
        } catch (IOException ex) {
            System.err.println("could not write :" + filename);

        }

    }

    public static BufferedImage applyTransform(BufferedImage img, AffineTransform transform) {

        Rectangle2D b = transform
                .createTransformedShape(img.getData().getBounds())
                .getBounds2D();
        /*
         // obtain the current system graphical settings
         GraphicsConfiguration gfx_config = GraphicsEnvironment.
         getLocalGraphicsEnvironment().getDefaultScreenDevice().
         getDefaultConfiguration();

        
         /*BufferedImage resImage = gfx_config.createCompatibleImage(
         Math.round(Math.round(b.getWidth())),
         Math.round(Math.round(b.getHeight())),
         img.getTransparency()
         );
         */
        BufferedImage resImage = new BufferedImage(
                Math.round(Math.round(b.getWidth())),
                Math.round(Math.round(b.getHeight())),
                img.getType());
        Graphics gfx = resImage.getGraphics();
/*
        gfx.setColor(Color.WHITE);
        gfx.fillRect(
                resImage.getMinX(),
                resImage.getMinY(),
                resImage.getWidth(),
                resImage.getHeight()
        );
*/
        AffineTransform translation = new AffineTransform();

        translation.translate(-b.getMinX(), -b.getMinY());
        translation.concatenate(transform);

        AffineTransformOp atop = new AffineTransformOp(translation, AffineTransformOp.TYPE_BICUBIC);

        Graphics2D g2d = (Graphics2D) resImage.createGraphics();
        g2d.drawImage(img, atop, 0, 0);

        return resImage;
    }

    public static BufferedImage applyCentredRotation(BufferedImage originalImage, double angle) {

        AffineTransform t1 = new AffineTransform();
        t1.translate(-originalImage.getWidth() / 2, -originalImage.getHeight() / 2);

        AffineTransform t2 = new AffineTransform();
        t2.rotate(angle);
        t2.concatenate(t1);

        return applyTransform(originalImage, t2);

    }

    public static BufferedImage ByteGreyScale(BufferedImage img) {
        BufferedImage resImage = new BufferedImage(
                img.getWidth(),
                img.getHeight(),
                BufferedImage.TYPE_BYTE_GRAY);

        ColorConvertOp greyscaleop = new ColorConvertOp(
                ColorSpace.getInstance(ColorSpace.CS_GRAY),
                null);

        Graphics2D g2d = (Graphics2D) resImage.createGraphics();

        g2d.drawImage(img, greyscaleop, 0, 0);

        return resImage;
    }
}
