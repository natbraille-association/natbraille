/*
 * NatBraille - Ocr - An universal Translator
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.natbraille.ocr.utils;

/**
 *
 * @author vivien
 */
public class VE {

    /* ========================================
     * vector utils
     *
     */
    public static double[] derivate(double v[]) {
        double resu[] = new double[v.length - 1];
        for (int i = 0; i < resu.length; i++) {
            resu[i] = v[i + 1] - v[i];
        }
        return resu;
    }

    public static double[] abs(double v[]) {
        double resu[] = new double[v.length];
        for (int i = 0; i < resu.length; i++) {
            resu[i] = Math.abs(v[i]);
        }
        return resu;
    }

    public static double sum(double v[]) {
        double resu = 0;
        for (int i = 0; i < v.length; i++) {
            resu += v[i];
        }
        return resu;
    }

    public static int minIdx(double v[]) {
        double resu = Double.MAX_VALUE;
        int idx = 0;
        for (int i = 0; i < v.length; i++) {
            if (v[i] < resu) {
                resu = v[i];
                idx = i;
            }
        }
        return idx;
    }

    public static int maxIdx(double v[]) {
        double resu = Double.MIN_VALUE;
        int idx = 0;
        for (int i = 0; i < v.length; i++) {
            if (v[i] > resu) {
                resu = v[i];
                idx = i;
            }
        }
        return idx;
    }

    public static double min(double v[]) {
        double resu = Double.MAX_VALUE;
        for (int i = 0; i < v.length; i++) {
            if (v[i] < resu) {
                resu = v[i];
            }
        }
        return resu;
    }

    public static double max(double v[]) {
        double resu = Double.MIN_VALUE;
        for (int i = 0; i < v.length; i++) {
            if (v[i] > resu) {
                resu = v[i];
            }
        }
        return resu;
    }

    public static double[] multt(double v1[], double v2[]) {

        double resu[] = new double[Math.min(v1.length, v2.length)];

        for (int i = 0; i < resu.length; i++) {
            resu[i] = v1[i] * v2[i];
        }
        return resu;
    }

    public static double[] diff(double v1[], double v2[]) {

        double resu[] = new double[Math.min(v1.length, v2.length)];

        for (int i = 0; i < resu.length; i++) {
            resu[i] = v1[i] - v2[i];
        }
        return resu;
    }

    public static double[] normalize(double nmin, double nmax, double[] v) {

        double min = min(v);
        double max = max(v);

        double resu[] = new double[v.length];
        for (int i = 0; i < resu.length; i++) {
            resu[i] = nmin + (nmax * (v[i] - min) / (max - min));
        }
        return resu;

    }

    public static double[] autocorrelation(double[] v) {
        double offsetCorrelation[] = new double[v.length];

        double moy = VE.sum(v) / (double) v.length;

        for (int offset = 0; offset < offsetCorrelation.length; offset++) {

            int ntest = v.length - offset;
            for (int i1 = 0; i1 < (ntest); i1++) {
                int i2 = i1 + offset;
                offsetCorrelation[offset] += (v[i2] - moy) * (v[i1] * moy);
            }
        }
        return VE.normalize(0, 1, offsetCorrelation);
    }

}
