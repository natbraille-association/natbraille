/*
 * NatBraille - Ocr - An universal Translator
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.natbraille.ocr.calculators;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import org.natbraille.ocr.structures.CellDisposition;
import org.natbraille.ocr.structures.PageDisposition;
import org.natbraille.ocr.utils.Misc;

/**
 *
 * @author vivien
 */
public class PageTextCalculator {

    private final double[][] simPix;
    public final int maxCol;
    public final int maxLine;

    public PageTextCalculator(BufferedImage img, PageDisposition pd) {
        DataBuffer pixels = img.getRaster().getDataBuffer();
        Rectangle b = img.getRaster().getBounds();

        this.maxCol = maxCellPos(pd.cellDispositionX, b.width);
        this.maxLine = maxCellPos(pd.cellDispositionY, b.height);
        this.simPix = extractPointsLuminosity(pixels, b,
                pd.cellDispositionX,
                pd.cellDispositionY);

    }

    private static int maxCellPos(CellDisposition cd, int max) {
        return Misc.d2i(cd.cellNum(max));
    }

    private static double[][] extractPointsLuminosity(DataBuffer pixels, Rectangle bounds, CellDisposition cdX, CellDisposition cdY) {

        int maxCol = maxCellPos(cdX, bounds.width);
        int maxLine = maxCellPos(cdY, bounds.height);

        double simPix[][] = new double[(maxCol + 1) * (maxLine + 1)][6];
        double maxPointLum = ((cdX.radius) * (cdY.radius) * 4.0 * 255.0);

        // get luminosity for each dot of each cell
        int nPixels = pixels.getSize();
        for (int pos = 0; pos < nPixels; pos++) {

            int x = pos % bounds.width;
            int y = pos / bounds.width;

            int col = Misc.d2i(cdX.cellNum(x));
            int line = Misc.d2i(cdY.cellNum(y));

            int pX = cdX.pointInCell(x, false);
            int pY = cdY.pointInCell(y, true);

            int pointNum = -1;
            if (pX == 1) {
                if (pY == 1) {
                    pointNum = 1;
                } else if (pY == 2) {
                    pointNum = 2;
                } else if (pY == 3) {
                    pointNum = 3;
                }
            } else if (pX == 2) {
                if (pY == 1) {
                    pointNum = 4;
                } else if (pY == 2) {
                    pointNum = 5;
                } else if (pY == 3) {
                    pointNum = 6;
                }
            }
            int lum = pixels.getElem(pos);
            if (pointNum != -1) {
                simPix[ line * maxCol + col][pointNum - 1] += (double) lum / maxPointLum;
            }

        }
        return simPix;
    }

    private static String extractText(
            double[][] simPix, int maxCol, int maxLine,
            double floor) {

        StringBuilder sb = new StringBuilder();

        // get luminosity for each dot of each cell        
        for (int line = 0; line < maxLine; line++) {
            for (int col = 0; col < maxCol; col++) {
                int pos = (maxCol * line + col);
                char code = 10240;
                for (int ptnum = 0; ptnum < 6; ptnum++) {
                    int val = (simPix[pos][ptnum] < floor) ? 1 : 0;
                    code += val << ptnum;
                }
                Character c = code;
                sb.append(c);
            }
            sb.append("\n");
        }
        return sb.toString();
    }

    public String getText(double whiteFloor) {
        return extractText(simPix, maxCol, maxLine, whiteFloor);
    }


}
