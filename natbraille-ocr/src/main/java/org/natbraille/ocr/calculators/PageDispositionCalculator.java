/*
 * NatBraille - Ocr - An universal Translator
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.ocr.calculators;

import org.natbraille.ocr.structures.CellDisposition;
import org.natbraille.ocr.structures.PageDisposition;
import org.natbraille.ocr.utils.Misc;
import org.natbraille.ocr.utils.VE;

/**
 *
 * @author vivien
 */
public class PageDispositionCalculator {

    public final double[] distOccurencesX;
    public final double[] distOccurencesY;
    public final double[] acDistOccurencesX;
    public final double[] acDistOccurencesY;
    public final PageDisposition pageDisposition;

    public PageDispositionCalculator(PageRotationCalculator prc) {

        double mX[] = prc.mX;
        double mY[] = prc.mY;
        double dmX[] = prc.dmX;
        double dmY[] = prc.dmY;

        /// determine magically weighted usage of distances between each 
        // pair of row of interest, and cols each pair of interest
        double doX[] = getDistancesOccurences(dmX);
        double doY[] = getDistancesOccurences(dmY);

        // autocorrelate usage distances to make cell repetition
        // pattern easily visible
        double acdoX[] = VE.autocorrelation(doX);
        double acdoY[] = VE.autocorrelation(doY);

        // extract cell informations
        CellDisposition cdX;
        CellDisposition cdY;
        {
            CellDisposition cda = extractCellPointData(acdoX);
            CellDisposition cdb = extractCellPointData(acdoY);

            cdX = cda;
            cdY = cdb;

            CellDisposition corrCdX = adaptCellDisposition(mX, cdX, false);
            CellDisposition corrCdY = adaptCellDisposition(mY, cdY, true);
// TODO invert X / Y
            //if (cda.interCell < cdb.interCell) {

//                } else {
//                    cdX = cdb;
//                    cdY = cda;
//                }
            cdX = corrCdX;
            cdY = corrCdY;
        }
        this.pageDisposition = new PageDisposition(cdX, cdY);
        this.distOccurencesX = doX;
        this.distOccurencesY = doY;
        this.acDistOccurencesX = acdoX;
        this.acDistOccurencesY = acdoY;

    }
 
    private static double[] getDistancesOccurences(double[] v) {
        double occurences[] = new double[v.length];

        for (int i1 = 0; i1 < v.length; i1++) {
            for (int i2 = i1 + 1; i2 < v.length; i2++) {
                if ((v[i2] != 1.0) && (v[i1] != 1.0)) {
                    int dist = i2 - i1;
                    occurences[dist] += (v[i1] * v[i2]);
                } else {

                }
            }
        }
        occurences = VE.normalize(0, 1, occurences);

        return occurences;
    }

    /* netbean auto format destroys schemas
     for X axis :
     *                     *
     *                   * *
     *     *     *     *
     *   * *   * *   *
     * *   * *   * *
     *     *     * 
     0    1  2  3  4  5    6
     for Y axis : 
     *                              *
     *                            * *
     *     *             *      *  
     *   * *   *   *   * *    *
     * *   * * * * * *   * *
     *     *   *   *     * 
     0    1  2  3 4 5 6 7  8  9    10
            
     if low res :
     *                          *
     *                        * *
     *     *         *      * 
     *   * *   *   * *    *
     * *   * * * *   * *
     *     *   *     *             
     0    1  2  3 4 5   6 7      8     
     */
    private static CellDisposition extractCellPointData(double[] ac) {

        // extract first minimum, corresponding to braille
        // point radius
        double radius = 0;
        {
            double min = Double.MAX_VALUE;
            for (int i = 1; i < ac.length / 2; i++) {
                if (ac[i] < min) {
                    min = ac[i];
                    radius = (double) i / 2.0;
                }

            }
        }

        // extract intercell
        //  maximum (after 0 which is always the autocorrel max)        
        // min distance is x.x..x
        int intercel = 0;
        {
            double max = Double.MIN_VALUE;
            for (int i = Misc.d2i(6.0 * radius); i < ac.length / 2; i++) {
                if (ac[i] > max) {
                    max = ac[i];
                    intercel = i;
                }

            }
        }

        // get idx of maximum value
        // which could be a or (minDac -a);
        int interpoint = 0;
        {
            double max = Double.MIN_VALUE;
            for (int i = Misc.d2i(radius + 1); i < (intercel - radius - 1); i++) {
                if (ac[i] > max) {
                    max = ac[i];
                    interpoint = i;
                }
            }
            if (interpoint > (intercel / 2.0)) {
                interpoint = intercel - interpoint;
            }
        }

        CellDisposition cd = new CellDisposition(radius, interpoint, intercel, 0);

        return cd;
    }

    private static double gridScore(double ns[], double grid[]) {
        double score = 0;
        for (int i = 0; i < ns.length; i++) {
            if (grid[i] != 0) {
                double matching = (1 - ns[i]) / ns.length;
                if (grid[i] != 1) {
                    matching = matching / 2;
                }
                score += matching;
            } else {
                score += (ns[i]) / ns.length;
            }

        }
        return score;
    }

    private static CellDisposition adaptCellDisposition(double[] ns, CellDisposition cd, boolean y) {
        double errors[] = new double[]{-0.75, -0.5, -0.25, 0.0, 0.25, 0.5, 0.75};

        double bestScore = 0;
        CellDisposition newCd = null;

        for (double off = 0; off <= cd.interCell; off++) {

            for (double icErr : errors) {
                for (double ipErr : errors) {

                    double interCell = cd.interCell + icErr;
                    double interPoint = cd.interPoint + ipErr;
                    double radius = cd.radius;
                    double offset = off;

                    CellDisposition lcd = new CellDisposition(radius, interPoint, interCell, offset);

                    if (((lcd.interCell > 0) && (lcd.interCell < ns.length))
                            && ((lcd.interPoint > 0) && (lcd.interCell < ns.length))
                            && ((lcd.radius > 0) && (lcd.radius < (ns.length / 2)))) {

                        double[] grid = lcd.grid(ns.length, y);
                        double score = gridScore(ns, grid);
                        if (score > bestScore) {
                            bestScore = score;
                            newCd = lcd;
                        }
                    }

                }
            }
        }

        return newCd;
    }

}
