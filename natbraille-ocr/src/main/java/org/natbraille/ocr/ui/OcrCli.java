/*
 * NatBraille - Ocr - An universal Translator
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.ocr.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.apache.commons.cli.UnrecognizedOptionException;
import org.fusesource.jansi.Ansi;
import org.fusesource.jansi.AnsiConsole;
import org.natbraille.ocr.Ocr;
import org.natbraille.ocr.Ocr.OcrResult;
import org.natbraille.ocr.calculators.PageBestRotationCalculator;
import org.natbraille.ocr.structures.CellDisposition;
import org.natbraille.ocr.structures.PageDisposition;
import org.natbraille.ocr.utils.Misc;
import org.natbraille.ocr.utils.Utf8ConsolePrinter;
import org.xnap.commons.i18n.I18n;
import org.xnap.commons.i18n.I18nFactory;

/**
 *
 * @author vivien
 */
public class OcrCli {

    private static final Logger l = Logger.getLogger(OcrCli.class.getName());
    private static final Utf8ConsolePrinter c = new Utf8ConsolePrinter();
    private static final I18n i18n = I18nFactory.getI18n(Ocr.class);


    /* 
     --rotation 12.5 
     -r 12.6 
     --xdisposition 0.5,1,2,5.2 
     -y 0.5,1,2,5.2 -f 0.55 
     --floor 0.55 
     -sample 20,3.14,20,3.14,20,1.25,20,1.25 
     /home/vivien/Bureau/ocr/ideal33.jpg  /home/vivien/Bureau/ocr/ideal44.jpg /home/vivien/Bureau/ocr/ideal55.jpg
     */
    private static final String O_ROTATION = "r";
    private static final String O_XDISPOSITION = "x";
    private static final String O_YDISPOSITION = "y";
    private static final String O_FLOOR = "f";
    private static final String O_SAMPLE = "s";
    private static final String O_HELP = "h";
    private static final String O_GUI = "g";
    private static final String O_OUTPUT = "o";

    private static final Options options = new Options()
            .addOption(O_GUI, "gui", false,
                    i18n.tr("launch gui. this options discards all other options"))
            .addOption(O_ROTATION, "rotation", true,
                    i18n.tr("force page rotation to angle : <arg>"))
            .addOption(O_XDISPOSITION, "xdisposition", true,
                    i18n.tr("force x cell disposition (floating pixel size) : <point radius>,<inter point>,<inter cell>,<offset>"))
            .addOption(O_YDISPOSITION, "ydisposition", true,
                    i18n.tr("force y cell disposition (floating pixel size) : <point radius>,<inter point>,<inter cell>,<offset>"))
            .addOption(O_FLOOR, "floor", true,
                    i18n.tr("minimal intensity of a white point (0.0 .. 1.0)"))
            .addOption(O_SAMPLE, "sample", true,
                    i18n.tr("rotation sampling parameters (params for rotation finder)"))
            .addOption(O_OUTPUT, "output", true,
                    i18n.tr("output result to files using <arg> as suffix"))
            .addOption(O_HELP, "help", false,
                    i18n.tr("display help"));

    public static CellDisposition decodeDisposition(String arg) {
        String[] dispo = arg.split(",");
        CellDisposition cd = null;
        if (dispo.length == 4) {
            double radius = Double.valueOf(dispo[0]);
            double interPoint = Double.valueOf(dispo[1]);
            double interCell = Double.valueOf(dispo[2]);
            double offset = Double.valueOf(dispo[3]);
            cd = new CellDisposition(radius, interPoint, interCell, offset);
        }
        return cd;
    }

    public static PageBestRotationCalculator.Config decodeSample(String arg) {
        String[] dispo = arg.split(",");
        PageBestRotationCalculator.Config config = null;
        if (dispo.length == 8) {

            int nTest1a = Misc.d2i(Double.valueOf(dispo[0]));
            double searchAngle1a = Double.valueOf(dispo[1]);
            int nTest1b = Misc.d2i(Double.valueOf(dispo[2]));
            double searchAngle1b = Double.valueOf(dispo[3]);
            int nTest1c = Misc.d2i(Double.valueOf(dispo[4]));
            double searchAngle1c = Double.valueOf(dispo[5]);
            int nTest2 = Misc.d2i(Double.valueOf(dispo[6]));
            double searchAngle2 = Double.valueOf(dispo[7]);
            config = new PageBestRotationCalculator.Config(nTest1a, searchAngle1a, nTest1b, searchAngle1b, nTest1c, searchAngle1c, nTest2, searchAngle2);
        }
        return config;
    }

    public static void analyseFiles(Ocr ocr, String filenames[], double whitefloor, String extension) {
        for (String filename : filenames) {

            c.out(i18n.tr("file : ''{0}''", filename));
            try {
                OcrResult ocrResult = ocr.analyze(new FileInputStream(new File(filename)));
                c.out(i18n.tr("rotation : {0}", ocrResult.getAngle()));
                c.out(i18n.tr("disposition : {0}", ocrResult.getPageDisposition()));
                String text = ocrResult.getText(whitefloor);
                if (extension != null) {
                    File outputFile = new File(filename + "." + extension);
                    FileOutputStream fos = new FileOutputStream(outputFile);
                    OutputStreamWriter ofw = new OutputStreamWriter(fos, "utf-8");
                    ofw.write(text);
                    ofw.close();
                    c.out(i18n.tr("wrote file : ''{0}''", outputFile));
                } else {
                    c.out(text);
                }
            } catch (IOException ex) {
                l.log(Level.SEVERE, ex.getLocalizedMessage());
            }
        }

    }

    private static void printHelpAndExit() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp(OcrCli.class.getCanonicalName() + " [options] file1 file2 ... filen", options);
        System.exit(0);
    }

    public static void main(String[] args) {

        l.setLevel(Level.CONFIG);
        TheNatbrailleBannerDrawIt();

        try {
            Ocr ocr = new Ocr();
            double whiteFloor = 0.5;

            CommandLine cl = new PosixParser().parse(options, args);

            // rotator parameters
            if (cl.hasOption(O_GUI)) {
                c.out(i18n.tr("launching gui... "));
                OcrGui.main(args);
            } else {
                {
                    // rotator parameters
                    if (cl.hasOption(O_HELP)) {
                        printHelpAndExit();
                    }
                }

                {
                    // rotator parameters
                    if (cl.hasOption(O_SAMPLE)) {
                        PageBestRotationCalculator.Config conf = decodeSample(cl.getOptionValue(O_SAMPLE));
                        ocr.setPageBestRotationCalculatorConfig(conf);
                        l.log(Level.CONFIG, i18n.tr("setting rotator config to {0} ", conf));
                    }
                }
                {
                    // rotation
                    if (cl.hasOption(O_ROTATION)) {
                        Double angle = Double.valueOf(cl.getOptionValue(O_ROTATION));
                        ocr.setUserAngle(angle);
                        l.log(Level.CONFIG, i18n.tr("forcing rotation to : {0}", angle));
                    }
                }
                {
                    // page disposition
                    if (cl.hasOption(O_XDISPOSITION) && (cl.hasOption(O_YDISPOSITION))) {
                        CellDisposition cdx = decodeDisposition(cl.getOptionValue("x"));
                        CellDisposition cdy = decodeDisposition(cl.getOptionValue(O_YDISPOSITION));
                        if ((cdx != null) && (cdy != null)) {
                            PageDisposition pd = new PageDisposition(cdy, cdy);
                            l.log(Level.CONFIG, i18n.tr("forcing page disposition to {0}", pd.toString()));
                            ocr.setUserPageDisposition(pd);
                        }
                    }
                }
                {
                    // white floor
                    if (cl.hasOption(O_FLOOR)) {
                        whiteFloor = Double.valueOf(cl.getOptionValue(O_FLOOR));
                        l.log(Level.CONFIG, i18n.tr("setting white floor to {0} ", whiteFloor));
                    }
                }

                String extension = null;
                {
                    if (cl.hasOption(O_OUTPUT)) {
                        extension = cl.getOptionValue(O_OUTPUT);
                    }
                }
                if (cl.getArgs().length > 0) {
                    // analysis
                    analyseFiles(ocr, cl.getArgs(), whiteFloor, extension);
                } else {
                    printHelpAndExit();
                }
            }

        } catch (UnrecognizedOptionException e) {
            l.log(Level.SEVERE, e.getMessage());
        } catch (ParseException e) {
            l.log(Level.SEVERE, e.getMessage());
        }
    }

    public static void TheNatbrailleBannerDrawIt() {

        AnsiConsole.systemInstall();
        Ansi a = new Ansi();

        a.bold().fg(Ansi.Color.YELLOW)
                .a("N").fg(Ansi.Color.GREEN).a("at")
                .fg(Ansi.Color.YELLOW)
                .a("B").fg(Ansi.Color.GREEN).a("raille")
                .fg(Ansi.Color.YELLOW)
                .a(" - ")
                .fg(Ansi.Color.RED)
                .a("ocr");
        a.reset();
        c.out(a.toString());
    }
}
