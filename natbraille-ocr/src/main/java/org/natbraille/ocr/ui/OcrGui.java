/*
 * NatBraille - Ocr - An universal Translator
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.ocr.ui;

import java.awt.Cursor;
import java.awt.Font;
import java.awt.FontFormatException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.UIManager;
import org.natbraille.ocr.Ocr;
import org.natbraille.ocr.Ocr.OcrResult;
import org.natbraille.ocr.calculators.PageBestRotationCalculator;
import org.natbraille.ocr.calculators.PageBestRotationCalculator.Config;
import org.natbraille.ocr.calculators.PageRotationCalculator;
import org.natbraille.ocr.calculators.PageTextCalculator;
import org.natbraille.ocr.structures.CellDisposition;
import org.natbraille.ocr.structures.PageDisposition;
import org.natbraille.ocr.utils.Misc;
import org.xnap.commons.i18n.I18n;
import org.xnap.commons.i18n.I18nFactory;

/**
 *
 * @author vivien
 */
public class OcrGui extends javax.swing.JFrame implements Ocr.OcrResultListener {

    private static I18n i18n = I18nFactory.getI18n(Ocr.class);

    private final Ocr ocr = new Ocr();
    private volatile OcrResult ocrResultCache;

    public synchronized void newOcrResult(OcrResult oldOcrResult, OcrResult newOcrResult) {
        ocrResultCache = newOcrResult;
        ocr2gui();
        ocrResultCache2gui();
        setCursor(Cursor.getDefaultCursor());
    }

    /**
     * Creates new form NewJFrame
     */
    public void analyse() {
        try {
            //ocrResultCache = ocr.analyzeThread(ocrResultCache);
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            ocr.analyzeThread(ocrResultCache, this);

        } catch (Exception e) {
            setCursor(Cursor.getDefaultCursor());
            //e.printStackTrace();
        }
    }

    public void ocr2gui() {

        jToggleButtonRotationAuto.setSelected(ocr.getAngleIsAuto());
        jToggleButtonDispositionAuto.setSelected(ocr.getPageDispositionIsAuto());
        Config c = ocr.getPageBestRotationCalculatorConfig();
        jTextFieldNTest1a.setText(Integer.toString(c.nTest1a));
        jTextFieldSearchAngle1a.setText(Double.toString(c.searchAngle1a));
        jTextFieldNTest1b.setText(Integer.toString(c.nTest1b));
        jTextFieldSearchAngle1b.setText(Double.toString(c.searchAngle1b));
        jTextFieldNTest1c.setText(Integer.toString(c.nTest1c));
        jTextFieldSearchAngle1c.setText(Double.toString(c.searchAngle1c));
        jTextFieldNTest2.setText(Integer.toString(c.nTest2));
        jTextFieldSearchAngle2.setText(Double.toString(c.searchAngle2));
    }

    Object imageCached = null;

    public void ocrResultCache_RotationPart_2ui(PageRotationCalculator prc) {
        if (prc.image != null) {
            if (imageCached != prc.image) {
                jLabelImageLabel.setIcon(new ImageIcon(prc.image));
                imageCached = prc.image;
            }
        }

    }

    public void ocrResultCache_TextPart_2ui(PageTextCalculator ptc) {
        if (ptc != null) {
            double value = ((double) jSliderWhiteFloor.getValue()) / 100.0;
            jTextAreaResuText.setText(ptc.getText(value));
        }
    }

    public void ocrResultCache2gui() {
        if (ocrResultCache != null) {
            PageRotationCalculator prc = ocrResultCache.prc;
            if (prc != null) {
                ocrResultCache_RotationPart_2ui(prc);
                jTextFieldRotation.setText(Double.toString(prc.angle));
            }
            PageDisposition pd = ocrResultCache.pd;
            if (pd != null) {
                CellDisposition cdX = pd.cellDispositionX;
                if (cdX != null) {
                    jTextFieldXIntercell.setText(Double.toString(cdX.interCell));
                    jTextFieldXInterpoint.setText(Double.toString(cdX.interPoint));
                    jTextFieldXRadius.setText(Double.toString(cdX.radius));
                    jTextFieldXOffset.setText(Double.toString(cdX.offset));
                }
                CellDisposition cdY = pd.cellDispositionY;
                if (cdY != null) {
                    jTextFieldYIntercell.setText(Double.toString(cdY.interCell));
                    jTextFieldYInterpoint.setText(Double.toString(cdY.interPoint));
                    jTextFieldYRadius.setText(Double.toString(cdY.radius));
                    jTextFieldYOffset.setText(Double.toString(cdY.offset));
                }

            }
            PageTextCalculator ptc = ocrResultCache.ptc;
            ocrResultCache_TextPart_2ui(ptc);
        }
    }

    public OcrGui() {
        initComponents();
        translateComponents();
        ocr.setRotationListener(new PageBestRotationCalculator.RotationListener() {

            public void rotationEvent(PageRotationCalculator prc, int rotNum, int maxRot) {
                if (maxRot != 0) {
                    jProgressBarRotation.setValue(100 * rotNum / maxRot);
                    if (jCheckBoxPreviewRotation.isSelected()) {
                        ocrResultCache_RotationPart_2ui(prc);
                    }
                }
            }
        });

        try {
            Font font = Font.createFont(Font.PLAIN, getClass().getResourceAsStream("DejaVuSans.ttf")).deriveFont(Font.PLAIN, 12);
            jTextAreaResuText.setFont(font);
        } catch (FontFormatException ex) {
            Logger.getLogger(OcrGui.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(OcrGui.class.getName()).log(Level.SEVERE, null, ex);
        }
        ocr2gui();
        ocrResultCache2gui();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jFileChooser1 = new javax.swing.JFileChooser();
        jFrameBestRotationCalculatorConfig = new javax.swing.JFrame();
        jLabel2 = new javax.swing.JLabel();
        jTextFieldSearchAngle1a = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jTextFieldNTest1a = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jTextFieldNTest1b = new javax.swing.JTextField();
        jTextFieldSearchAngle1b = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jLabel1c = new javax.swing.JLabel();
        jTextFieldNTest1c = new javax.swing.JTextField();
        jTextFieldSearchAngle1c = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jTextFieldNTest2 = new javax.swing.JTextField();
        jTextFieldSearchAngle2 = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jButton1SetBestRotationCalculatorConfig = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jTextFieldXRadius = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jTextFieldXInterpoint = new javax.swing.JTextField();
        jTextFieldXIntercell = new javax.swing.JTextField();
        jTextFieldXOffset = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jTextFieldYRadius = new javax.swing.JTextField();
        jTextFieldYIntercell = new javax.swing.JTextField();
        jTextFieldYOffset = new javax.swing.JTextField();
        jTextFieldYInterpoint = new javax.swing.JTextField();
        jButtonEditDisposition = new javax.swing.JButton();
        jToggleButtonDispositionAuto = new javax.swing.JToggleButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jTextFieldRotation = new javax.swing.JTextField();
        jButtonSetRotation = new javax.swing.JButton();
        jToggleButtonRotationAuto = new javax.swing.JToggleButton();
        jProgressBarRotation = new javax.swing.JProgressBar();
        jCheckBoxPreviewRotation = new javax.swing.JCheckBox();
        jButton1 = new javax.swing.JButton();
        jSplitPane2 = new javax.swing.JSplitPane();
        jPanel3 = new javax.swing.JPanel();
        jLabelImageLabel = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jSliderWhiteFloor = new javax.swing.JSlider();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextAreaResuText = new javax.swing.JTextArea();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItemOpen = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenu7 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenuItem8 = new javax.swing.JMenuItem();
        jMenuItem9 = new javax.swing.JMenuItem();
        jMenuItem10 = new javax.swing.JMenuItem();
        jMenuItem11 = new javax.swing.JMenuItem();
        jMenuItem12 = new javax.swing.JMenuItem();
        jMenuItem13 = new javax.swing.JMenuItem();
        jMenuItem14 = new javax.swing.JMenuItem();
        jMenuItem15 = new javax.swing.JMenuItem();
        jMenuItem16 = new javax.swing.JMenuItem();

        jLabel2.setText("searchangle1a");

        jTextFieldSearchAngle1a.setText("jTextField1");

        jLabel11.setText("ntest1a");

        jTextFieldNTest1a.setText("jTextField1");

        jLabel12.setText("searchangle1b");

        jLabel13.setText("ntest1b");

        jTextFieldNTest1b.setText("jTextField1");

        jTextFieldSearchAngle1b.setText("jTextField1");

        jLabel14.setText("searchangle1c");

        jLabel1c.setText("ntest1c");

        jTextFieldNTest1c.setText("jTextField1");

        jTextFieldSearchAngle1c.setText("jTextField1");

        jLabel16.setText("searchangle");

        jLabel17.setText("ntest2");

        jTextFieldNTest2.setText("jTextField1");

        jTextFieldSearchAngle2.setText("jTextField1");

        jLabel15.setText("maximum search");

        jLabel18.setText("maximum and minimum d search");

        jButton1SetBestRotationCalculatorConfig.setText("set");
        jButton1SetBestRotationCalculatorConfig.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1SetBestRotationCalculatorConfigActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jFrameBestRotationCalculatorConfigLayout = new javax.swing.GroupLayout(jFrameBestRotationCalculatorConfig.getContentPane());
        jFrameBestRotationCalculatorConfig.getContentPane().setLayout(jFrameBestRotationCalculatorConfigLayout);
        jFrameBestRotationCalculatorConfigLayout.setHorizontalGroup(
            jFrameBestRotationCalculatorConfigLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jFrameBestRotationCalculatorConfigLayout.createSequentialGroup()
                .addGap(0, 81, Short.MAX_VALUE)
                .addGroup(jFrameBestRotationCalculatorConfigLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel15, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel18, javax.swing.GroupLayout.Alignment.TRAILING)))
            .addGroup(jFrameBestRotationCalculatorConfigLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jFrameBestRotationCalculatorConfigLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jFrameBestRotationCalculatorConfigLayout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jTextFieldSearchAngle1a, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jFrameBestRotationCalculatorConfigLayout.createSequentialGroup()
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jTextFieldSearchAngle1b, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jFrameBestRotationCalculatorConfigLayout.createSequentialGroup()
                        .addComponent(jLabel14)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jTextFieldSearchAngle1c, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jFrameBestRotationCalculatorConfigLayout.createSequentialGroup()
                        .addComponent(jLabel17)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jTextFieldNTest2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jFrameBestRotationCalculatorConfigLayout.createSequentialGroup()
                        .addComponent(jLabel1c)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jTextFieldNTest1c, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jFrameBestRotationCalculatorConfigLayout.createSequentialGroup()
                        .addComponent(jLabel13)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jTextFieldNTest1b, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jFrameBestRotationCalculatorConfigLayout.createSequentialGroup()
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jTextFieldNTest1a, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jFrameBestRotationCalculatorConfigLayout.createSequentialGroup()
                        .addComponent(jLabel16)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jTextFieldSearchAngle2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jFrameBestRotationCalculatorConfigLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButton1SetBestRotationCalculatorConfig)))
                .addContainerGap())
        );
        jFrameBestRotationCalculatorConfigLayout.setVerticalGroup(
            jFrameBestRotationCalculatorConfigLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jFrameBestRotationCalculatorConfigLayout.createSequentialGroup()
                .addComponent(jLabel15)
                .addGap(9, 9, 9)
                .addGroup(jFrameBestRotationCalculatorConfigLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jTextFieldSearchAngle1a, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jFrameBestRotationCalculatorConfigLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(jTextFieldNTest1a, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jFrameBestRotationCalculatorConfigLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(jTextFieldSearchAngle1b, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jFrameBestRotationCalculatorConfigLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(jTextFieldNTest1b, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jFrameBestRotationCalculatorConfigLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(jTextFieldSearchAngle1c, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jFrameBestRotationCalculatorConfigLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1c)
                    .addComponent(jTextFieldNTest1c, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel18)
                .addGap(15, 15, 15)
                .addGroup(jFrameBestRotationCalculatorConfigLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(jTextFieldSearchAngle2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jFrameBestRotationCalculatorConfigLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17)
                    .addComponent(jTextFieldNTest2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 21, Short.MAX_VALUE)
                .addComponent(jButton1SetBestRotationCalculatorConfig)
                .addContainerGap())
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel3.setText("xradius");

        jLabel4.setText("xinterpoint");

        jLabel5.setText("xintercell");

        jLabel6.setText("xoffset");

        jLabel7.setText("yradius");

        jLabel8.setText("yinterpoint");

        jLabel9.setText("yintercell");

        jLabel10.setText("yoffset");

        jButtonEditDisposition.setText("set");
        jButtonEditDisposition.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonEditDispositionActionPerformed(evt);
            }
        });

        jToggleButtonDispositionAuto.setText("Auto");
        jToggleButtonDispositionAuto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleButtonDispositionAutoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jToggleButtonDispositionAuto, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel5)
                                            .addComponent(jLabel6))
                                        .addGap(143, 143, 143))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(jLabel4)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jTextFieldXInterpoint, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addGap(581, 581, 581))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel9)
                                    .addComponent(jLabel10))
                                .addGap(25, 25, 25))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel7)
                                    .addComponent(jLabel3))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jTextFieldYInterpoint)
                            .addComponent(jTextFieldYIntercell)
                            .addComponent(jButtonEditDisposition, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jTextFieldYRadius, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jTextFieldYOffset, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jTextFieldXOffset, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextFieldXIntercell, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextFieldXRadius, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldXRadius, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldXInterpoint, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldXIntercell, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldXOffset, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldYRadius, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldYInterpoint, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldYIntercell, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldYOffset, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonEditDisposition)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jToggleButtonDispositionAuto)
                .addContainerGap(61, Short.MAX_VALUE))
        );

        jLabel1.setText("rotation");

        jButtonSetRotation.setText("set");
        jButtonSetRotation.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSetRotationActionPerformed(evt);
            }
        });

        jToggleButtonRotationAuto.setText("Auto");
        jToggleButtonRotationAuto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleButtonRotationAutoActionPerformed(evt);
            }
        });

        jCheckBoxPreviewRotation.setSelected(true);
        jCheckBoxPreviewRotation.setText("preview");

        jButton1.setText("...");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jProgressBarRotation, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jTextFieldRotation)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButtonSetRotation))
                    .addComponent(jCheckBoxPreviewRotation)
                    .addComponent(jLabel1)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jToggleButtonRotationAuto, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldRotation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonSetRotation))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jToggleButtonRotationAuto)
                    .addComponent(jButton1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jProgressBarRotation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 5, Short.MAX_VALUE)
                .addComponent(jCheckBoxPreviewRotation)
                .addContainerGap())
        );

        jSplitPane2.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);

        jPanel3.setMinimumSize(new java.awt.Dimension(200, 200));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jLabelImageLabel)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabelImageLabel)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jSplitPane2.setTopComponent(jPanel3);

        jSliderWhiteFloor.setOrientation(javax.swing.JSlider.VERTICAL);
        jSliderWhiteFloor.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jSliderWhiteFloorStateChanged(evt);
            }
        });

        jTextAreaResuText.setColumns(20);
        jTextAreaResuText.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        jTextAreaResuText.setRows(5);
        jScrollPane2.setViewportView(jTextAreaResuText);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(jSliderWhiteFloor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 396, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSliderWhiteFloor, javax.swing.GroupLayout.DEFAULT_SIZE, 36, Short.MAX_VALUE)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 36, Short.MAX_VALUE)
        );

        jSplitPane2.setRightComponent(jPanel4);

        jMenu1.setText("File");

        jMenuItemOpen.setText("open...");
        jMenuItemOpen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemOpenActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItemOpen);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edit");
        jMenuBar1.add(jMenu2);

        jMenu7.setText("Tests");

        jMenuItem1.setText("ideal33.jpg");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu7.add(jMenuItem1);

        jMenuItem2.setText("ideal44.jpg");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu7.add(jMenuItem2);

        jMenuItem3.setText("ideal44.jpg");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu7.add(jMenuItem3);

        jMenuItem4.setText("ideal55.jpg");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu7.add(jMenuItem4);

        jMenuItem6.setText("oneline.jpg");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        jMenu7.add(jMenuItem6);

        jMenuItem7.setText("oneline.jpg");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        jMenu7.add(jMenuItem7);

        jMenuItem8.setText("sentense1.jpg");
        jMenuItem8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem8ActionPerformed(evt);
            }
        });
        jMenu7.add(jMenuItem8);

        jMenuItem9.setText("sentensec.jpg");
        jMenuItem9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem9ActionPerformed(evt);
            }
        });
        jMenu7.add(jMenuItem9);

        jMenuItem10.setText("sentenset.jpg");
        jMenuItem10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem10ActionPerformed(evt);
            }
        });
        jMenu7.add(jMenuItem10);

        jMenuItem11.setText("sentensett.jpg");
        jMenuItem11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem11ActionPerformed(evt);
            }
        });
        jMenu7.add(jMenuItem11);

        jMenuItem12.setText("BrailleDin16.jpg");
        jMenuItem12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem12ActionPerformed(evt);
            }
        });
        jMenu7.add(jMenuItem12);

        jMenuItem13.setText("BrailleDin16r.jpg");
        jMenuItem13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem13ActionPerformed(evt);
            }
        });
        jMenu7.add(jMenuItem13);

        jMenuItem14.setText("BrailleDin16x2.jpg");
        jMenuItem14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem14ActionPerformed(evt);
            }
        });
        jMenu7.add(jMenuItem14);

        jMenuItem15.setText("BrailleDin16x2r.jpg");
        jMenuItem15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem15ActionPerformed(evt);
            }
        });
        jMenu7.add(jMenuItem15);

        jMenuItem16.setText("ideal1010.jpg");
        jMenuItem16.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem16ActionPerformed(evt);
            }
        });
        jMenu7.add(jMenuItem16);

        jMenuBar1.add(jMenu7);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 226, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSplitPane2)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addComponent(jSplitPane2)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void translateComponents() {

        jLabel2.setText(i18n.tr("searchangle1a"));
        jTextFieldSearchAngle1a.setText(i18n.tr("jTextField1"));
        jLabel11.setText(i18n.tr("ntest1a"));
        jTextFieldNTest1a.setText(i18n.tr("jTextField1"));
        jLabel12.setText(i18n.tr("searchangle1b"));
        jLabel13.setText(i18n.tr("ntest1b"));
        jTextFieldNTest1b.setText(i18n.tr("jTextField1"));
        jTextFieldSearchAngle1b.setText(i18n.tr("jTextField1"));
        jLabel14.setText(i18n.tr("searchangle1c"));
        jLabel1c.setText(i18n.tr("ntest1c"));
        jTextFieldNTest1c.setText(i18n.tr("jTextField1"));
        jTextFieldSearchAngle1c.setText(i18n.tr("jTextField1"));
        jLabel16.setText(i18n.tr("searchangle"));
        jLabel17.setText(i18n.tr("ntest2"));
        jTextFieldNTest2.setText(i18n.tr("jTextField1"));
        jTextFieldSearchAngle2.setText(i18n.tr("jTextField1"));
        jLabel15.setText(i18n.tr("maximum search"));
        jLabel18.setText(i18n.tr("maximum and minimum d search"));
        jLabel3.setText(i18n.tr("xradius"));
        jLabel4.setText(i18n.tr("xinterpoint"));
        jLabel5.setText(i18n.tr("xintercell"));
        jLabel6.setText(i18n.tr("xoffset"));
        jLabel7.setText(i18n.tr("yradius"));
        jLabel8.setText(i18n.tr("yinterpoint"));
        jLabel9.setText(i18n.tr("yintercell"));
        jLabel10.setText(i18n.tr("yoffset"));
        jButtonEditDisposition.setText(i18n.tr("set"));
        jToggleButtonDispositionAuto.setText(i18n.tr("Auto"));
        jLabel1.setText(i18n.tr("rotation"));
        jButtonSetRotation.setText(i18n.tr("set"));
        jToggleButtonRotationAuto.setText(i18n.tr("Auto"));
        jCheckBoxPreviewRotation.setText(i18n.tr("preview"));
        jButton1.setText(i18n.tr("..."));
        jMenu1.setText(i18n.tr("File"));
        jMenuItemOpen.setText(i18n.tr("open..."));
        jMenu2.setText(i18n.tr("Edit"));
    }
    private void jToggleButtonRotationAutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleButtonRotationAutoActionPerformed
        ocr.setAngleIsAuto(jToggleButtonRotationAuto.isSelected());
        analyse();
    }//GEN-LAST:event_jToggleButtonRotationAutoActionPerformed

    private void setImage(File file) {
        try {
            setImage(file.getAbsolutePath(),new FileInputStream(file));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(OcrGui.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void setImage(String resourceName) {
        setImage(resourceName,getClass().getResourceAsStream(resourceName));
    }
    
    private void setImage(String name, InputStream inputStream) {
        try {
            this.setTitle(i18n.tr("NatBraille - ocr - {0}",name));
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

            ocr.analyzeThread(inputStream, this);
            //OcrResult result = ocr.analyze(new FileInputStream(file));
            //ocrResultCache = result;
            //ocrResultCache2gui();
        } catch (Exception ex) {
            Logger.getLogger(OcrGui.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    private void jMenuItemOpenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemOpenActionPerformed
        jFileChooser1.setFileSelectionMode(JFileChooser.FILES_ONLY);

        int returnVal = jFileChooser1.showOpenDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = jFileChooser1.getSelectedFile();
            setImage(file);
        } else {
            System.out.println("File access cancelled by user.");
        }
    }//GEN-LAST:event_jMenuItemOpenActionPerformed

    private void jToggleButtonDispositionAutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleButtonDispositionAutoActionPerformed

        ocr.setPageDispositionIsAuto(jToggleButtonDispositionAuto.isSelected());
        analyse();
    }//GEN-LAST:event_jToggleButtonDispositionAutoActionPerformed

    private void jButtonSetRotationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSetRotationActionPerformed
        try {
            ocr.setUserAngle(Double.valueOf(jTextFieldRotation.getText()));
            analyse();
        } catch (Exception ex) {
            Logger.getLogger(OcrGui.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButtonSetRotationActionPerformed

    private void jSliderWhiteFloorStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jSliderWhiteFloorStateChanged
//        analyse();
        if (ocrResultCache != null) {
            PageTextCalculator ptc = ocrResultCache.ptc;
            ocrResultCache_TextPart_2ui(ptc);
        }

    }//GEN-LAST:event_jSliderWhiteFloorStateChanged

    private void jButtonEditDispositionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonEditDispositionActionPerformed
        try {
            CellDisposition cdX = new CellDisposition(
                    Double.valueOf(jTextFieldXRadius.getText()),
                    Double.valueOf(jTextFieldXInterpoint.getText()),
                    Double.valueOf(jTextFieldXIntercell.getText()),
                    Double.valueOf(jTextFieldXOffset.getText())
            );
            CellDisposition cdY = new CellDisposition(
                    Double.valueOf(jTextFieldYRadius.getText()),
                    Double.valueOf(jTextFieldYInterpoint.getText()),
                    Double.valueOf(jTextFieldYIntercell.getText()),
                    Double.valueOf(jTextFieldYOffset.getText())
            );
            ocr.setUserPageDisposition(new PageDisposition(cdX, cdY));
            analyse();
        } catch (Exception ex) {
            Logger.getLogger(OcrGui.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButtonEditDispositionActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        jFrameBestRotationCalculatorConfig.pack();
        jFrameBestRotationCalculatorConfig.setVisible(true);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton1SetBestRotationCalculatorConfigActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1SetBestRotationCalculatorConfigActionPerformed
        try {
            Config c = new Config(
                    Misc.d2i(Double.valueOf(jTextFieldNTest1a.getText())),
                    Double.valueOf(jTextFieldSearchAngle1a.getText()),
                    Misc.d2i(Double.valueOf(jTextFieldNTest1b.getText())),
                    Double.valueOf(jTextFieldSearchAngle1b.getText()),
                    Misc.d2i(Double.valueOf(jTextFieldNTest1c.getText())),
                    Double.valueOf(jTextFieldSearchAngle1c.getText()),
                    Misc.d2i(Double.valueOf(jTextFieldNTest2.getText())),
                    Double.valueOf(jTextFieldSearchAngle2.getText())
            );
            ocr.setPageBestRotationCalculatorConfig(c);
            analyse();
        } catch (Exception e) {
            ocr2gui();
            //e.printStackTrace();
        }

    }//GEN-LAST:event_jButton1SetBestRotationCalculatorConfigActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        setImage(evt.getActionCommand());
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        setImage(evt.getActionCommand());
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        setImage(evt.getActionCommand());
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        setImage(evt.getActionCommand());
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed
        setImage(evt.getActionCommand());
    }//GEN-LAST:event_jMenuItem6ActionPerformed

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
        setImage(evt.getActionCommand());
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    private void jMenuItem8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem8ActionPerformed
        setImage(evt.getActionCommand());
    }//GEN-LAST:event_jMenuItem8ActionPerformed

    private void jMenuItem9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem9ActionPerformed
        setImage(evt.getActionCommand());
    }//GEN-LAST:event_jMenuItem9ActionPerformed

    private void jMenuItem10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem10ActionPerformed
        setImage(evt.getActionCommand());
    }//GEN-LAST:event_jMenuItem10ActionPerformed

    private void jMenuItem11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem11ActionPerformed
        setImage(evt.getActionCommand());
    }//GEN-LAST:event_jMenuItem11ActionPerformed

    private void jMenuItem12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem12ActionPerformed
        setImage(evt.getActionCommand());
    }//GEN-LAST:event_jMenuItem12ActionPerformed

    private void jMenuItem13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem13ActionPerformed
        setImage(evt.getActionCommand());
    }//GEN-LAST:event_jMenuItem13ActionPerformed

    private void jMenuItem14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem14ActionPerformed
        setImage(evt.getActionCommand());
    }//GEN-LAST:event_jMenuItem14ActionPerformed

    private void jMenuItem15ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem15ActionPerformed
        setImage(evt.getActionCommand());
    }//GEN-LAST:event_jMenuItem15ActionPerformed

    private void jMenuItem16ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem16ActionPerformed
        setImage(evt.getActionCommand());
    }//GEN-LAST:event_jMenuItem16ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {

            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                //   System.out.println(info.getName());
           /*     if ("Nimbus".equals(info.getName())) {
                 javax.swing.UIManager.setLookAndFeel(info.getClassName());
                 break;
                 }
                 */
            }
            UIManager.setLookAndFeel(
                    UIManager.getSystemLookAndFeelClassName());

            //    javax.swing.UIManager.setLookAndFeel("com.sun.java.swing.plaf.SystemLookAndFeel");//com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(OcrGui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(OcrGui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(OcrGui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(OcrGui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new OcrGui().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton1SetBestRotationCalculatorConfig;
    private javax.swing.JButton jButtonEditDisposition;
    private javax.swing.JButton jButtonSetRotation;
    private javax.swing.JCheckBox jCheckBoxPreviewRotation;
    private javax.swing.JFileChooser jFileChooser1;
    private javax.swing.JFrame jFrameBestRotationCalculatorConfig;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel1c;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabelImageLabel;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu7;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem10;
    private javax.swing.JMenuItem jMenuItem11;
    private javax.swing.JMenuItem jMenuItem12;
    private javax.swing.JMenuItem jMenuItem13;
    private javax.swing.JMenuItem jMenuItem14;
    private javax.swing.JMenuItem jMenuItem15;
    private javax.swing.JMenuItem jMenuItem16;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JMenuItem jMenuItem8;
    private javax.swing.JMenuItem jMenuItem9;
    private javax.swing.JMenuItem jMenuItemOpen;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JProgressBar jProgressBarRotation;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSlider jSliderWhiteFloor;
    private javax.swing.JSplitPane jSplitPane2;
    private javax.swing.JTextArea jTextAreaResuText;
    private javax.swing.JTextField jTextFieldNTest1a;
    private javax.swing.JTextField jTextFieldNTest1b;
    private javax.swing.JTextField jTextFieldNTest1c;
    private javax.swing.JTextField jTextFieldNTest2;
    private javax.swing.JTextField jTextFieldRotation;
    private javax.swing.JTextField jTextFieldSearchAngle1a;
    private javax.swing.JTextField jTextFieldSearchAngle1b;
    private javax.swing.JTextField jTextFieldSearchAngle1c;
    private javax.swing.JTextField jTextFieldSearchAngle2;
    private javax.swing.JTextField jTextFieldXIntercell;
    private javax.swing.JTextField jTextFieldXInterpoint;
    private javax.swing.JTextField jTextFieldXOffset;
    private javax.swing.JTextField jTextFieldXRadius;
    private javax.swing.JTextField jTextFieldYIntercell;
    private javax.swing.JTextField jTextFieldYInterpoint;
    private javax.swing.JTextField jTextFieldYOffset;
    private javax.swing.JTextField jTextFieldYRadius;
    private javax.swing.JToggleButton jToggleButtonDispositionAuto;
    private javax.swing.JToggleButton jToggleButtonRotationAuto;
    // End of variables declaration//GEN-END:variables

}
