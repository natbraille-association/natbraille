/*
 * NatBraille - Ocr - An universal Translator
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


package unused;

import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;

/**
 *
 * @author vivien
 */

/**
 * ==========> p2 is WAY faster than p1
 * 
 */ 
public class Speed {

    /* ========================================
     * GD2 speed
     *
     */
    private static BufferedImage toCompatibleImage(BufferedImage image) {
        // obtain the current system graphical settings
        GraphicsConfiguration gfx_config = GraphicsEnvironment.
                getLocalGraphicsEnvironment().getDefaultScreenDevice().
                getDefaultConfiguration();

        /*
         * if image is already compatible and optimized for current system 
         * settings, simply return it
         */
        if (image.getColorModel().equals(gfx_config.getColorModel())) {
            return image;
        }

        // image is not optimized, so create a new image that is
        BufferedImage new_image = gfx_config.createCompatibleImage(
                image.getWidth(), image.getHeight(), image.getTransparency());

        // get the graphics context of the new image to draw the old image on
        Graphics2D g2d = (Graphics2D) new_image.getGraphics();

        // actually draw the image and dispose of context no longer needed
        g2d.drawImage(image, 0, 0, null);
        g2d.dispose();

        // return the new optimized image
        return new_image;
    }

    public static int p1(BufferedImage img) {
        Rectangle b = img.getRaster().getBounds();

        int sum = 0;
        for (int x = 0; x < b.width; x++) {
            for (int y = 0; y < b.height; y++) {
                int rx = x + b.x;
                int ry = y + b.y;

                int pix[] = {0, 0, 0, 0};
                img.getRaster().getPixel(rx, ry, pix);
                sum += pix[0];
                pix[0] = 0;
                pix[1] = 0;
                pix[2] = pix[2] / 2;
                img.getRaster().setPixel(rx, ry, pix);
            }
        }
        //save(i, "resu/ra.png");
        return sum;
    }

    public static int p2(BufferedImage img) {

        DataBuffer db = img.getRaster().getDataBuffer();
        
        int size = db.getSize();
        int sum = 0;
        for (int x = 0; x < size; x++) {
            int v = db.getElem(x);
            int r = (v << 24) & 0xff;
            int g = (v << 16) & 0xff;
            int b = (v << 8) & 0xff;
            int a = (v << 0) & 0xff;

            r = 0;
            g = 0;
            b = b;

            sum += v;
            v = (r >> 24) | (g >> 16) | (b >> 8) | a;
            db.setElem(x, v);

        }
        //save(i, "resu/rb.png");
        return sum;
    }

    /*
     p2 is WAY faster than p1
     =1
     13.418 s
     =2
     0.523 s
     =1
     12.738 s
     =2
     0.465 s
     */    
    
    public static void testp1p2(BufferedImage img) {
        long time;

        System.err.println("=1");

        time = System.currentTimeMillis();
        for (int i = 0; i < 100; i++) {
            p1(img);
        }
        System.err.println((System.currentTimeMillis() - time) / 1000.0);

        System.err.println("=2");
        time = System.currentTimeMillis();

        for (int i = 0; i < 100; i++) {
            p2(img);
        }
        System.err.println((System.currentTimeMillis() - time) / 1000.0);

        System.err.println("=1");
        time = System.currentTimeMillis();
        for (int i = 0; i < 100; i++) {
            p1(img);
        }
        System.err.println((System.currentTimeMillis() - time) / 1000.0);

        System.err.println("=2");
        time = System.currentTimeMillis();
        for (int i = 0; i < 100; i++) {
            p2(img);
        }
        System.err.println((System.currentTimeMillis() - time) / 1000.0);

        System.exit(0);
    }
}
