/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unused;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.DataBuffer;
import org.natbraille.ocr.calculators.PageDispositionCalculator;
import org.natbraille.ocr.calculators.PageRotationCalculator;
import org.natbraille.ocr.structures.CellDisposition;
import org.natbraille.ocr.utils.Misc;
import org.natbraille.ocr.utils.VE;

/**
 *
 * @author vivien
 */
public class ImageDebug {

    public static boolean pointOnIntermediateCalculus(
            int x, int y, Rectangle b,
            double[] nsX, double[] nsY,
            double[] ndsX, double[] ndsY,
            double[] distOccurencesX, double[] distOccurencesY,
            double[] acDistOccurencesX, double[] acDistOccurencesY,
            double[] gridX, double[] gridY) {

        boolean yes = false;
        int dgw = Misc.d2i(b.width / (2.0 * 5.0));
        if (dgw > 100) {
            dgw = 100;
        }

        if (x < dgw) {
            double array[] = nsY;

            if (y < array.length) {
                //int val = d2i(0* dgw + dgw * array[y]);
                int val = Misc.d2i(dgw * (0.0 + array[y]));
                yes = (x <= val);
            }
        } else if (x < 2 * dgw) {
            double array[] = ndsY;
            if (y < array.length) {
                int val = Misc.d2i(dgw * (1.0 + array[y]));
                yes = (x <= val);

            }
        } else if (x < 3 * dgw) {
            double array[] = VE.normalize(0, 1, distOccurencesY);
            if (y < array.length) {
                int val = Misc.d2i(dgw * (2.0 + array[y]));
                yes = (x <= val);
            }
        } else if (x < 4 * dgw) {
            double array[] = acDistOccurencesY;
            if (y < array.length) {
                int val = Misc.d2i(dgw * (3.0 + array[y]));
                yes = (x <= val);
            }
        } else if (x < 5 * dgw) {
            double array[] = gridY;
            if (y < array.length) {
                int val = Misc.d2i(dgw * (4.0 + array[y]));
                yes = (x <= val);
            }
        }

        if ((b.width - x) < dgw) {
            double array[] = nsX;
            if (y < array.length) {
                int val = b.width - Misc.d2i(dgw * (0.0 + array[y]));
                yes = (x >= val);
            }
        } else if ((b.width - x) < 2 * dgw) {
            double array[] = ndsX;
            if (y < array.length) {
                int val = b.width - Misc.d2i(dgw * (1.0 + array[y]));
                yes = (x >= val);
            }
        } else if ((b.width - x) < 3 * dgw) {
            double array[] = VE.normalize(0, 1, distOccurencesX);
            if (y < array.length) {
                int val = b.width - Misc.d2i(dgw * (2.0 + array[y]));
                yes = (x >= val);
            }
        } else if ((b.width - x) < 4 * dgw) {
            double array[] = acDistOccurencesX;
            if (y < array.length) {
                int val = b.width - Misc.d2i(dgw * (3.0 + array[y]));
                yes = (x >= val);
            }
        } else if ((b.width - x) < 5 * dgw) {
            double array[] = gridX;
            if (y < array.length) {
                int val = b.width - Misc.d2i(dgw * (4.0 + array[y]));
                yes = (x >= val);
            }
        }
        return yes;
    }

    public static void overlayIntermediateCalculus(PageRotationCalculator prc, PageDispositionCalculator pdc, String name) {
        // render image

        CellDisposition cdX = pdc.pageDisposition.cellDispositionX;
        CellDisposition cdY = pdc.pageDisposition.cellDispositionY;

        DataBuffer pixels = prc.image.getRaster().getDataBuffer();
        int npixels = pixels.getSize();
        Rectangle b = prc.image.getRaster().getBounds();

        double[] sX = prc.mX;
        double[] sY = prc.mY;
        double[] dsX = prc.dmX;
        double[] dsY = prc.dmY;
        double[] distOccurencesX = pdc.distOccurencesX;
        double[] distOccurencesY = pdc.distOccurencesX;
        double[] acDistOccurencesX = pdc.acDistOccurencesX;
        double[] acDistOccurencesY = pdc.acDistOccurencesX;

        double[] gridX = VE.normalize(0, 1, cdX.grid(b.width, false));
        double[] gridY = VE.normalize(0, 1, cdY.grid(b.height, true));

        double ndsX[] = VE.normalize(0, 1, dsX);
        double ndsY[] = VE.normalize(0, 1, dsY);

        double nsX[] = VE.normalize(0, 1, sX);
        double nsY[] = VE.normalize(0, 1, sY);

        for (int pos = 0; pos < npixels; pos++) {
            int x = (pos) % (b.width);
            int y = (pos) / (b.width);

            int lum = pixels.getElem(pos);
            int m;

            if ((gridX[x] != 0) && (gridY[y] != 0)) {
                m = lum;
            } else {
                m = lum / 8;
            }

            if (true && pointOnIntermediateCalculus(x, y, b, nsX, nsY,
                    ndsX, ndsY, distOccurencesX, distOccurencesY,
                    acDistOccurencesX, acDistOccurencesY,
                    gridX, gridY)) {
                m = m / 2 + 128;

            }
            pixels.setElem(pos, m);
        }

        // paint text infos
        {
            Graphics thegraph = prc.image.getGraphics();
            for (int ptx : new int[]{0, 2, 1}) {
                for (int pty : new int[]{0, 2, 1}) {
                    int fh = 12;
                    if ((ptx == 1) && (pty == 1)) {
                        thegraph.setColor(Color.WHITE);
                    } else {
                        thegraph.setColor(Color.BLACK);
                    }

                    thegraph.setFont(new Font("Arial", Font.BOLD, Misc.d2i(fh * 1.4) - 1));

                    thegraph.drawString("score:" + prc.score, ptx, b.height + pty);
                    thegraph.drawString("angle:" + prc.angle, ptx, b.height - fh + pty);
                    thegraph.drawString("y cell:" + cdX.toString(), ptx, b.height - 2 * fh + pty);
                    thegraph.drawString("x cell:" + cdY.toString(), ptx, b.height - 3 * fh + pty);
                    thegraph.drawString(name, ptx, b.height - 4 * fh + pty);
                }
            }
        }

    }
}
