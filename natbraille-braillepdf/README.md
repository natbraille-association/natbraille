# natbraille-braillepdf  braille file to pdf file

## usage

    org.natbraille.braillepdf.BraillePdfCli [options] file1 file2 ... filen
       
    -f,--font-file <arg>     utilise le fichier de police <arg>
    -H,--page-height <arg>   fixe la hauteur de page à <arg>
    -h,--help                affiche l'aide
    -l,--margin-left <arg>   fixe la marge gauche à <arg>
    -P,--page-format <arg>   fixe le format de page à <arg>
    -s,--font-size <arg>     fixe la taille de police à <arg>
    -t,--margin-top <arg>    fixe la taille de la marge à <arg>
    -W,--page-width <arg>    fixe la largeur de la page à <arg>

## example

 