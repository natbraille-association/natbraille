/*
 * NatBraille Music 
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.braillepdf;

import java.io.IOException;
import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDFontFactory;
import org.apache.pdfbox.pdmodel.font.PDTrueTypeFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

/**
 *
 */
/**
 * Produces a pdf file using a page configuraiton @{see PageConfig}.
 * <p>
 * input Braille format
 * <ul>
 * <li>\n for line separation</li>
 * <li>\f for page separation</li>
 * </ul>
 * <p>
 * @author vivien
 */
public class PdfBoxPdfFormatter {

    private final PageConfig pageConfig;

    public PdfBoxPdfFormatter(PageConfig pageConfig) {
        this.pageConfig = pageConfig;
    }

    /**
     * Writes a pdf file from given Braille CodeUS String.
     *
     * @param brlString the brailleString in CodeUS Braille Table
     * @param pdfName
     * @throws IOException
     * @throws COSVisitorException
     */
    public void brailleToPdf(String brlString, String pdfName) throws IOException, COSVisitorException {

        //System.err.println(pageConfig.toString());
        PDDocument document = new PDDocument();
        if (pdfName != null) {
            document.getDocumentInformation().setTitle(pdfName);
        }

        //document.getDocumentInformation().getModificationDate();
        //document.getDocumentInformation().getProducer();
        document.getDocumentInformation().setProducer(getClass().getCanonicalName());

        PDFont font;

        if (pageConfig.fontFileName == null) {
            font = PDTrueTypeFont.loadTTF(document, getClass().getResourceAsStream("LouisLouis.ttf"));
            System.err.println("using default louislouis.ttf");

            //font = PDTrueTypeFont.loadTTF(document, getClass().getResourceAsStream("DejaVuSans.ttf"));
        } else {
            font = PDTrueTypeFont.loadTTF(document, pageConfig.fontFileName);
        }
        System.err.println("got\n" + brlString);
        /**
         * foreach page
         */
        for (String brlPage : brlString.split("\f")) {
            PDPage page = new PDPage();
            if (pageConfig.pageWidth != null) {
                page.getMediaBox().setUpperRightX(pageConfig.pageWidth);
            }

            if (pageConfig.pageHeight != null) {
                page.getMediaBox().setUpperRightY(pageConfig.pageHeight);
            }

            PDPageContentStream contentStream = new PDPageContentStream(document, page);
            /*                contentStream.drawLine(
             page.getMediaBox().getLowerLeftX(),
             page.getMediaBox().getLowerLeftY(),
             page.getMediaBox().getUpperRightX(),
             page.getMediaBox().getUpperRightY();
             */
            contentStream.beginText();
            contentStream.setFont(font, pageConfig.fontSize);

            contentStream.moveTextPositionByAmount(
                    pageConfig.marginLeft,
                    page.getMediaBox().getHeight() - (font.getFontDescriptor().getAscent() / 1000f * pageConfig.fontSize) - pageConfig.marginTop
            );
            //font.setFontEncoding(null);
            /**
             * foreach line
             */
            System.out.println("WITH THAHT FONT " + font.getFontDescriptor().getAscent()+ " "
                    + font.getFontDescriptor().getDescent()+ " "
                    + font.getFontDescriptor().getCapHeight() + " "
                    + font.getFontDescriptor().getFontFamily() + " "
                    + font.getFontDescriptor().getFontName() + " "
                    + font.getFontDescriptor().getLeading());

            for (String lineOfText : brlPage.split("\n")) {
                //font.getCOSObject();

                contentStream.drawString(lineOfText);

                float leadingPage = font.getFontDescriptor().getLeading() / 1000f * pageConfig.fontSize;
                leadingPage = 1.5f * font.getFontDescriptor().getFontBoundingBox().getHeight() / 1000f * pageConfig.fontSize;
                //leadingPage = font.getFontDescriptor().getDescent() / 1000f * pageConfig.fontSize;
                contentStream.moveTextPositionByAmount(0, -leadingPage);

            }
            contentStream.endText();
            contentStream.close();
            document.addPage(page);

        }
        document.save(pdfName);
        document.close();

    }

}
