/*
 * works with the AGPL version of iText.
 */
package org.natbraille.braillepdf;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfWriter;
import java.awt.Canvas;
import java.awt.FontFormatException;
import java.awt.FontMetrics;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class LowagiePdfFormatter {

    private final PageConfig pageConfig;

    public LowagiePdfFormatter(PageConfig pageConfig) {
        this.pageConfig = pageConfig;
    }

    /**
     * Writes a pdf file from given Braille String.
     * <p>The user must ensure that font Braille table and text Braille table are matching.
     *
     * @param brlString the brailleString 
     * @param pdfName
     * @throws com.lowagie.text.DocumentException
     * @throws java.io.IOException
     * @throws java.awt.FontFormatException
     *
     */
    public void brailleToPdf(String brlString, String pdfName) throws DocumentException, IOException, FontFormatException {

        // get lowagie font
        BaseFont baseFont = BaseFont.createFont(pageConfig.fontFileName, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
        Font font = new Font(baseFont, pageConfig.fontSize);

        // we rather use awt font metrics that knows about Leading
        java.awt.Font awtFont = java.awt.Font.createFont(java.awt.Font.TRUETYPE_FONT, new File(pageConfig.fontFileName));
        java.awt.Font awtFont2 = awtFont.deriveFont(pageConfig.fontSize);
        Canvas c = new Canvas();
        FontMetrics metrics = c.getFontMetrics(awtFont2);
        
        // configure pdf document
        Document document = new Document();
        if ((pageConfig.pageHeight != null) && (pageConfig.pageWidth != null)) {
            document.setPageSize(new Rectangle(0, 0, pageConfig.pageWidth, pageConfig.pageHeight));
        } else {
            document.setPageSize(PageSize.A4);
        }
        document.setMargins(pageConfig.marginLeft, 0, pageConfig.marginTop, 0);
        
        
        PdfWriter.getInstance(document, new FileOutputStream(pdfName));
        document.open();

        // foreach braille page
        for (String brlPage : brlString.split("\f")) {            
            boolean first = true;
            // foreach braille line
            for (String lineOfText : brlPage.split("\n")) {
                Paragraph paragraph = new Paragraph(lineOfText, font);
                if (first) {
                    paragraph.setLeading(metrics.getAscent());
                } else {
                    //?
                    //paragraph.setLeading(metrics.getLeading()+50);
                    paragraph.setLeading(metrics.getAscent());
                }
                document.add(paragraph);
                first = false;
            }
            document.newPage();
        }

        ///
        if (false){
        String fts = brlString.substring(0, 10);
        Paragraph p = new Paragraph(
                font.getFamilyname() + "\n"
                + " - lw ascent:" + baseFont.getAscent(fts) + "\n"
                + " - lw descent:" + baseFont.getDescent(fts) + "\n"
                + " - aw ascent:" + metrics.getAscent() + "\n"
                + " - aw descent:" + metrics.getDescent() + "\n"
                + " - aw leading:" + metrics.getLeading()
        );
        document.add(p);
        document.newPage();
        }
        //document.add(Chunk.NEWLINE);
        document.close();
    }
}
