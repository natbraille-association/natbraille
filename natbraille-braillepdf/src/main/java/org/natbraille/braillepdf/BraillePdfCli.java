package org.natbraille.braillepdf;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.PosixParser;
import org.apache.commons.cli.UnrecognizedOptionException;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.xnap.commons.i18n.I18n;
import org.xnap.commons.i18n.I18nFactory;

/*
 * @author vivien
 */
public class BraillePdfCli {

    private static final Logger l = Logger.getLogger(BraillePdfCli.class.getName());

    private static final I18n i18n = I18nFactory.getI18n(BraillePdfCli.class);

    private static final String PAGE_FORMAT = "P";
    private static final String PAGE_WIDTH = "W";
    private static final String PAGE_HEIGHT = "H";
    private static final String FONT_SIZE = "s";
    private static final String FONT_FILE = "f";
    private static final String MARGIN_LEFT = "l";
    private static final String MARGIN_TOP = "t";
    private static final String O_HELP = "h";

    private static final Options options = new Options()
            .addOption(PAGE_FORMAT, "page-format", true,
                    i18n.tr("set page format to <arg> (A1,A2,A3,A4,A5,A6,letter)"))
            .addOption(PAGE_WIDTH, "page-width", true,
                    i18n.tr("set page width to <arg> (overrides page-format)"))
            .addOption(PAGE_HEIGHT, "page-height", true,
                    i18n.tr("set page height to <arg> (overrides page-format)"))
            .addOption(FONT_SIZE, "font-size", true,
                    i18n.tr("set font size to <arg>"))
            .addOption(MARGIN_TOP, "margin-top", true,
                    i18n.tr("set margin top to <arg>"))
            .addOption(FONT_FILE, "font-file", true,
                    i18n.tr("set font file to <arg>"))
            .addOption(MARGIN_LEFT, "margin-left", true,
                    i18n.tr("set margin left to <arg>"))
            .addOption(O_HELP, "help", false,
                    i18n.tr("display help"));

    
    public static void print(String s) {
        System.out.println(s);
    }

    public static void printerr(String s) {
        System.err.println(s);
    }

    static String convertFileToString(File file, String charset) throws FileNotFoundException {
        InputStream is = new FileInputStream(file);
        java.util.Scanner s = new java.util.Scanner(is, charset).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    static PDRectangle decodePageSize(String paramValue) {
        String p = paramValue.toLowerCase();
        
        if ("a0".equals(p)) {
            return PDPage.PAGE_SIZE_A0;
        } else if ("a1".equals(p)) {
            return PDPage.PAGE_SIZE_A1;
        } else if ("a2".equals(p)) {
            return PDPage.PAGE_SIZE_A2;
        } else if ("a3".equals(p)) {
            return PDPage.PAGE_SIZE_A3;
        } else if ("a4".equals(p)) {
            return PDPage.PAGE_SIZE_A4;
        } else if ("a5".equals(p)) {
            return PDPage.PAGE_SIZE_A5;
        } else if ("a6".equals(p)) {
            return PDPage.PAGE_SIZE_A6;
        } else if ("letter".equals(p)) {
            return PDPage.PAGE_SIZE_LETTER;
        }

        return null;
    }

    public static void main(String[] args) throws Exception {
        l.setLevel(Level.CONFIG);
        TheNatbrailleBannerDrawIt();
        try {

            CommandLine cl = new PosixParser().parse(options, args);
            if (cl.hasOption(O_HELP)) {
                HelpFormatter formatter = new HelpFormatter();
                formatter.printHelp(BraillePdfCli.class.getCanonicalName() + " [options] file1 file2 ... filen", options);
            } else {
                PageConfig pageConfig = new PageConfig();

                if (cl.hasOption(PAGE_FORMAT)) {
                    PDRectangle a = decodePageSize(cl.getOptionValue(PAGE_FORMAT));
                    pageConfig.pageWidth = a.getWidth();
                    pageConfig.pageHeight = a.getHeight();
                }

                if (cl.hasOption(PAGE_WIDTH)) {
                    pageConfig.pageWidth = Float.parseFloat(cl.getOptionValue(PAGE_WIDTH));
                }

                if (cl.hasOption(PAGE_HEIGHT)) {
                    pageConfig.pageHeight = Float.parseFloat(cl.getOptionValue(PAGE_HEIGHT));

                }

                if (cl.hasOption(FONT_SIZE)) {
                    pageConfig.fontSize = Float.parseFloat(cl.getOptionValue(FONT_SIZE));
                }
                if (cl.hasOption(FONT_FILE)) {
                    pageConfig.fontFileName = cl.getOptionValue(FONT_FILE);
                }
                if (cl.hasOption(MARGIN_LEFT)) {
                    pageConfig.marginLeft = Float.parseFloat(cl.getOptionValue(MARGIN_LEFT));
                }
                if (cl.hasOption(MARGIN_TOP)) {
                    pageConfig.marginTop = Float.parseFloat(cl.getOptionValue(MARGIN_TOP));
                }
              //  PdfBoxPdfFormatter pdfFormatter = new PdfBoxPdfFormatter(pageConfig);
                  LowagiePdfFormatter pdfFormatter = new LowagiePdfFormatter(pageConfig);

                if (cl.getArgs().length == 0) {
                    HelpFormatter formatter = new HelpFormatter();
                    formatter.printHelp(BraillePdfCli.class.getCanonicalName() + " [options] file1 file2 ... filen", options);
                }
                for (String arg : cl.getArgs()) {
                    try {
                        String s = convertFileToString(new File(arg), "UTF8");
                        pdfFormatter.brailleToPdf(s, arg + ".pdf");
                    } catch (FileNotFoundException e) {
                        l.log(Level.SEVERE, e.getLocalizedMessage());
                    }
                }

            }
        } catch (UnrecognizedOptionException e) {
            l.log(Level.SEVERE, e.getMessage());
        }

    }

    public static void TheNatbrailleBannerDrawIt() {

        System.err.println("natbraille-pdf : braille file to pdf file");
    }
}
