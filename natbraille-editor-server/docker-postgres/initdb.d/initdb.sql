create database user_documents;

\c user_documents ;

create table documents
(
    username varchar(255) not null,
    kind varchar(255) not null,
    name varchar(255) not null,
    document bytea not null,
    lastModified timestamp with time zone not null,
    UNIQUE (username, kind, name)
);

create table users
(
    username varchar(255) PRIMARY KEY,
    encodedPassword varchar(255) not null,
    email varchar(255) not null
);

create table user_registration
(
    username varchar(255) PRIMARY KEY,
    encodedPassword varchar(255) not null,
    email varchar(255) not null,
    created timestamp with time zone not null,
    encodedChallenge varchar(255),
    sent timestamp with time zone
);

create table tokens
(
    token varchar(255),
    username varchar(255),
    created timestamp with time zone not null,
    lastUsed timestamp with time zone not null,
    UNIQUE (username)
);

create table user_password_reset_request
(
    username varchar(255) PRIMARY KEY,
    encodedChallenge varchar(255),
    sent timestamp with time zone not null
);

create table user_account_removal_request
(
    username varchar(255) PRIMARY KEY,
    encodedChallenge varchar(255),
    sent timestamp with time zone not null
);

-- INSERT INTO users (username,user_pass) VALUES
--    ('user0','passhash0'),
--    ('user1','passhash1')
--   ;
