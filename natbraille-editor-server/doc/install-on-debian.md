# Installation steps for a debian server

This procedure installs natbraille server in a home directory 
(in this case, username is `debian`). 

- Binaries not in distro are installed in `~/bin`
  - gradle
  - node
- The placeholder smptp server is installed in `~/go/bin/MailHog`
- server source are compiled in `~/src`
- server is installed in `~/server`

Server config file is `~/server.config/server.conf.json`

## git

   	sudo apt install git -y

---

# backend

## install jdk

	sudo apt install openjdk-17-jdk-headless -y

## install unzip

	sudo apt install unzip -y

## install gradle

    mkdir -p ~/bin
    cd ~/bin
    curl https://downloads.gradle.org/distributions/gradle-7.6.2-bin.zip --output gradle.zip
    unzip gradle.zip
    rm gradle.zip
    echo 'export PATH="$PATH:/home/debian/bin/gradle-7.6.2/bin"' >> ~/.bashrc
    source ~/.bashrc
    gradle --version

## install msgfmt

    sudo apt install gettext

## get backend sources / compile backend

    mkdir -p ~/src
    cd ~/src
    git	https://framagit.org/natbraille-association/natbraille.git
    git clone -b update-saxon-he --single-branch https://framagit.org/natbraille-association/natbraille.git
    gradle build
    gradle shadowJar

## (update sources)

    cd ~/src/natbraille
    git pull  	
    gradle shadowJar

## edit server backend config

    mkdir -p ~/server/.config
	cd ~/server
    touch .config/server.conf.json

## add libreoffice + unoserver conversion server

-> see install-on-debian-unoserver.md
    
Example content for `.config/server.conf.json` (replace *.*.*.* by actual ip/domain)

    {
        "version": 1,
        "documentsRoot": "./SERVER_DOCUMENTS_ROOT",
        "smtp": {
            "mail.smtp.host": "0.0.0.0",
            "mail.smtp.port": "1025"
        },
        "useDb" : false,
        "host" : "",
        "port" : "",
        "staticFilesDirectory" : "/home/debian/server/dist/",
        "devURL" : "/",
        "publicServerURLForExternalLinks" : "http://*.*.*.*:4567"
    }

## launch server

	cd ~/server
    java -jar ~/src/natbraille/natbraille-editor-server/build/libs/natbraille-editor-server-1.0-SNAPSHOT-all.jar

An optional logging config file (sl4j) can be provided

    java -Djava.util.logging.config.file=logging.properties -jar ...

## open port (exemple)

    # sudo iptables -A INPUT -p tcp --dport 4567 -j ACCEPT

## test from outside

    nmap 51.254.38.63 -p 4567
    curl http://51.254.38.63:4567/api/ping        # -> pong
    curl http://51.254.38.63:4567/api/hello/Joe   # -> Hello World Joe

    curl --request POST --data 'a great document content' http://51.254.38.63:4567/api/users/vivien/fr-g2-rules/greatdocument.txt
    curl http://51.254.38.63:4567/api/users/vivien/archive.zip --output archive.zip

---

# frontend

## install node

	mkdir -p ~/bin
	cd ~/bin
	curl https://nodejs.org/dist/v20.4.0/node-v20.4.0-linux-x64.tar.xz --output node.xz
	tar -xf node.xz
	rm node.xz
	node-v20.4.0-linux-x64/bin/
	echo 'export PATH="$PATH:/home/debian/bin/node-v20.4.0-linux-x64/bin/"' >> ~/.bashrc
	source ~/.bashrc
	node --version


## get frontend sources

	mkdir -p ~/src
	cd ~/src
	git clone https://framagit.org/natbraille-association/natbraille-editor.git

## update sources

  	 cd ~/src/natbraille-editor
	 git reset --hard            # !! deletes all local modifications
	 git pull

## build

	cd ~/src/natbraille-editor
	npm install
	npm audit fix
	npm run build

# move dist/ to backend static files (to be served from backend)

    rm -rf ~/server/dist
    mv ~/src/natbraille-editor/dist ~/server/

# FAKE ssmtp server for TESTS

https://github.com/mailhog/MailHog

## install

	sudo apt-get -y install golang-go
	go install github.com/mailhog/MailHog@latest

## start

	~/go/bin/MailHog

## stop

	killall MailHog