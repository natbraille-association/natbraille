### systemd unit

```
[Unit]
Description=Natbraille server editor
After=network-online.target

[Service]
Type=simple

# https://superuser.com/questions/710253/allow-non-root-process-to-bind-to-port-80-and-443
AmbientCapabilities=CAP_NET_BIND_SERVICE

User=natbraille
Group=natbraille
# UMask=007

ExecStart=/datas/server/launch-server.sh

Restart=on-failure

# Configures the time to wait before service is stopped forcefully.
TimeoutStopSec=300

[Install]
WantedBy=multi-user.target
```

-------
sudo cp natbraille.service /etc/systemd/system/natbraille.service

sudo systemctl enable natbraille.service
sudo systemctl start natbraille.service
sudo systemctl stop natbraille.service
sudo systemctl status natbraille.service
sudo systemctl restart natbraille.service

journalctl --since "10 minutes ago"


sudo journalctl -u  natbraille

    // follow
    sudo journalctl -u -f  natbraille


https://unix.stackexchange.com/questions/139513/how-to-clear-journalctl

