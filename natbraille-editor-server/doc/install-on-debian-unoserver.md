# used for starmath -> mathml

repo : https://github.com/unoconv/unoserver

## Installation

    sudo apt-install libreoffice
    
    sudo apt install pip
    
    # non --break-system-packages requires setting a python env    
    sudo pip install unoserver --break-system-packages

    # (using proxy)
    sudo pip --proxy proxy.univ-lyon1.fr:3128 install unoserver --break-system-packages

## Start

    # installed system wide, so
    
    unoserver

    # everything (ports/client) of server and loserver are set to default.
    # natbraille-editor-server is configured for default (= has no configuration)

## Service

/etc/systemd/system/unoserver.service

----sof
```
[Unit]
Description=Unoserver converter
After=network-online.target
 
[Service]
Type=simple

# https://superuser.com/questions/710253/allow-non-root-process-to-bind-to-port-80-and-443
# AmbientCapabilities=CAP_NET_BIND_SERVICE

User=natbraille
Group=natbraille
# UMask=007
 
ExecStart=/usr/local/bin/unoserver

Restart=on-failure
 
# Configures the time to wait before service is stopped forcefully.
TimeoutStopSec=300
 
[Install]
WantedBy=multi-user.target
    
```    
----eof

sudo cp unoserver.service /etc/systemd/system/unoserver.service

sudo systemctl enable unoserver.service
sudo systemctl start unoserver.service
sudo systemctl stop unoserver.service
sudo systemctl status unoserver.service
sudo systemctl restart unoserver.service