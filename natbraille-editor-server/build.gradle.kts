/*
 * This file was generated by the Gradle 'init' task.
 *
 * This project uses @Incubating APIs which are subject to change.
 */

plugins {
    id("org.natbraille.java-conventions")
    id("com.github.johnrengelman.shadow") version  "7.1.2"
    id("com.gorylenko.gradle-git-properties") version "2.4.1"
    //id("io.github.lhotari.gradle-nar-plugin") version "0.5.1"
}

/*
 * SLF4J: Class path contains multiple SLF4J bindings.
 * SLF4J: Found binding in [jar:file:/home/vivien/.gradle/caches/modules-2/files-2.1/org.slf4j/slf4j-simple/1.7.36/a41f9cfe6faafb2eb83a1c7dd2d0dfd844e2a936/slf4j-simple-1.7.36.jar!/org/slf4j/impl/StaticLoggerBinder.class]
 *  SLF4J: Found binding in [jar:file:/home/vivien/.gradle/caches/modules-2/files-2.1/org.slf4j/slf4j-nop/1.7.7/6cca9a3b999ff28b7a35ca762b3197cd7e4c2ad1/slf4j-nop-1.7.7.jar!/org/slf4j/impl/StaticLoggerBinder.class]
 *
 *  implementation("...") {
 *        //exclude(group = "org.slf4j", module = "slf4j-nop")
 *  }
 */

dependencies {
    api(project(":natbraille-core"))
    // https://mvnrepository.com/artifact/com.sparkjava/spark-core
    implementation("com.sparkjava:spark-core:2.9.4")
    // https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-databind
    //implementation("com.fasterxml.jackson.core:jackson-databind:2.15.2")
    implementation(libs.com.fasterxml.jackson.core.jackson.databind)

    // https://mvnrepository.com/artifact/org.slf4j/slf4j-simple
    implementation("org.slf4j:slf4j-simple:1.7.36")/*    api("net.oneandone.reflections8:reflections8:0.11.4") */
    // https://mvnrepository.com/artifact/org.postgresql/postgresql
    implementation("org.postgresql:postgresql:42.6.0")
    // https://mvnrepository.com/artifact/org.springframework.security/spring-security-crypto
    implementation("org.springframework.security:spring-security-crypto:6.1.1")
    // https://mvnrepository.com/artifact/org.bouncycastle/bcprov-jdk15on
    implementation("org.bouncycastle:bcprov-jdk15on:1.70")
    // https://mvnrepository.com/artifact/jakarta.mail/jakarta.mail-api
    implementation("jakarta.mail:jakarta.mail-api:2.1.2")
    // https://mvnrepository.com/artifact/org.eclipse.angus/jakarta.mail
    implementation("org.eclipse.angus:jakarta.mail:2.0.0")

    // https://mvnrepository.com/artifact/org.junit.jupiter/junit-jupiter-api
    // testImplementation("org.junit.jupiter:junit-jupiter-api:5.10.0-RC1")
    // https://mvnrepository.com/artifact/org.junit.jupiter/junit-jupiter-engine
    // testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.10.0-RC1")
    implementation("org.odftoolkit:odfdom-java:0.11.0")
    implementation("com.opencsv:opencsv:5.9")

    //implementation("org.liblouis:louis:3.27.0-p1") // liblouis NAR
    implementation("org.liblouis:liblouis-java:5.0.2")
    //implementation(files("natbraille-editor-server/include-jar/liblouis-java-5.0.2-resources.jar"))
    implementation("org.liblouis:liblouis-java:5.0.2:resources")
    // https://repo1.maven.org/maven2/org/liblouis/liblouis-java/5.0.2/liblouis-java-5.0.2-resources.jar
    // implementation(files("include-jar/liblouis-java-5.0.2-resources.jar"))
    // implementation("org.liblouis:liblouis-java:5.0.2-resources")

    // https://mvnrepository.com/artifact/com.zaxxer/HikariCP
    implementation("com.zaxxer:HikariCP:6.2.1")

    // test

    // https://mvnrepository.com/artifact/org.junit.jupiter/junit-jupiter-api
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.12.0")

    // https://mvnrepository.com/artifact/org.junit.jupiter/junit-jupiter-engine
    testImplementation("org.junit.jupiter:junit-jupiter-engine:5.12.0")

    // https://mvnrepository.com/artifact/org.junit.platform/junit-platform-launcher
    testImplementation("org.junit.platform:junit-platform-launcher:1.12.0")

}

// tasks.test { useJUnitPlatform() }
description = "natbraille-editor-server"

tasks.jar {
    manifest.attributes["Main-Class"] = "org.natbraille.editor.server.Server"
}
tasks.test {
    useJUnitPlatform()
}

tasks.register<Copy>("createSymlink") {
    // it's a copy, but a symlink would be better.
    val versionedJar = tasks.shadowJar.get().archiveFile.get().asFile
    from(versionedJar)
    into(file(versionedJar).parentFile)
    rename { _ -> "natbraille-editor-server-all.jar" }
}

tasks.named("shadowJar") {
    finalizedBy("createSymlink")
}

