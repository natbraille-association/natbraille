# Server for natbraille-editor

The web server for natbraille-editor (server part)

# SMTP

## MailHog (optional) 

Used to simulate smtp server (not need on a production server)

see https://github.com/mailhog/MailHog

    ~/go/bin/MailHog

The default ui can be found at : **http://0.0.0.0:8025/#**

# Unoconverter

`Unoconverter` / `Unoserver` are used for libreoffice related conversions. See https://github.com/unoconv/unoserver 

A pool of unoservers can be used to improve libreoffice speed conversions. `Unoservers` must be launched prior to editor-server execution. Each server must 
specify a different uno port and port

    unoserver --port 2010 --uno-port 2011 
    unoserver --port 2012 --uno-port 2013 
    unoserver --port 2014 --uno-port 2015
    ...

Note that each `unoserver` will spawn a libreoffice instance with a tmp profile. Their usage must be reserved to natbraille-editor-server as concurrent
accesses can have unpredictable behavior.

Each of these `unoserver`'s port must be specified in the "unoServers" of `server.conf.json`:

    ...
    "unoServers": [{"port": "2010"},{"port": "2012"},{"port": "2014"}]
    ...

Calls to `unoconverter`s (`unoserver` clients) are wrapped in the server, using path found in `server.conf.json`

    ...
    "unoconvertExecutablePath": "/usr/local/bin/unoconvert"
    ...

by default, a single `unoserver` listening on port `2003` is excepted.

# Database

## Using postgres from docker

db initialisation is all sql files in directory:

    ./docker-postgres/initdb.d/

db env (user/password) is in docker env file:

    `./docker-postgres/.env`

### 1. install docker

https://docs.docker.com/engine/install/debian/

### 2. docker commands

    cd docker-postgres/

#### start

    sudo docker compose build

    sudo docker compose up

#### stop and remove all

    # Stop the container(s) using the following command:

    sudo docker compose down

    # Delete all containers using the following command:

    sudo  docker rm -f $(sudo  docker ps -a -q)

    # Delete all volumes using the following command:

    sudo docker volume rm $(sudo docker volume ls -q)

    # Delete persistent volume

    sudo rm postgres-data -rf

one line

    sudo docker compose down ; sudo  docker rm -f $(sudo  docker ps -a -q) ; sudo docker volume rm $(sudo docker volume ls -q) ; sudo rm postgres-data -rf

down / remove all / up one line

    sudo docker compose down ; sudo  docker rm -f $(sudo  docker ps -a -q) ; sudo docker volume rm $(sudo docker volume ls -q) ; sudo rm postgres-data -rf ; sudo docker compose up


# run command in the running docker container

    sudo docker ps

    CONTAINER ID   IMAGE      COMMAND                  CREATED       STATUS       PORTS                                       NAMES
    85596d3ebcac   postgres   "docker-entrypoint.s…"   12 days ago   Up 12 days   0.0.0.0:5432->5432/tcp, :::5432->5432/tcp   postgres_container

    sudo docker exec 85596d3ebcac psql -U postgres -d user_documents -c 'select * from users'
    sudo docker exec 85596d3ebcac psql -U postgres -d user_documents -c 'select * from users'

# ajout column email (NOT NULL) à user

 email varchar(255) not null,

ajout column

    -c 'ALTER TABLE users ADD COLUMN email varchar(255);'

ajout emails

    -c "UPDATE users SET email='joe2@zzo.com' where username='joe2'"
    ...

ajout constraint NOT NULL

    -c "ALTER TABLE users ALTER COLUMN email SET NOT NULL;"

# ajout column last_modified (NOT NULL) à user

lastModified timestamp with time zone not null
# DROP COL
#    -c 'ALTER TABLE documents DROP COLUMN last_modified;'

ajout column

    -c 'ALTER TABLE documents ADD COLUMN lastModified timestamp with time zone;'

ajout emails

    -c "UPDATE documents SET lastModified=NOW() where lastModified is null"
    ...

ajout constraint NOT NULL

    -c "ALTER TABLE documents ALTER COLUMN lastModified SET NOT NULL;"


# misc. doc

## auth

the definitive guide to form based website authentication

https://stackoverflow.com/questions/549/the-definitive-guide-to-form-based-website-authentication

owasp / password hashing

https://cheatsheetseries.owasp.org/cheatsheets/Password_Storage_Cheat_Sheet.html#password-hashing-algorithms

## cookie path (/api vs /)

https://stackoverflow.com/questions/576535/cookie-path-and-its-accessibility-to-subfolder-pages
