package org.natbraille.editor.server.faketranscriptor.layout;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.*;

class TestFakeHyphenateTrie {

    record HyphenationTestPair(String word, int[] hyphenationScore) {
    }

    record HyphenationResourceTests(String hyphenationDictionaryResourcePath, HyphenationTestPair[] pairs) {
    }

    @Test
    void testHyphenate() throws IOException {

        HyphenationResourceTests tests = new HyphenationResourceTests(
                "org/natbraille/core/xsl/dicts/hyph_fr.dic",
                new HyphenationTestPair[]{
                        new HyphenationTestPair("automatisation", new int[]{4, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0}),
                        new HyphenationTestPair("sempiternels", new int[]{0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0}),
                        new HyphenationTestPair("cantonnement", new int[]{0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0}),
                        new HyphenationTestPair("camion", new int[]{0, 1, 0, 0, 0})
                });

        InputStream is = TestFakeHyphenateTrie.class.getClassLoader().getResourceAsStream(tests.hyphenationDictionaryResourcePath());
        assertNotNull(is, "Hyphenation dictionary not found");

        Function<String, int[]> hyphenate = FakeHyphenateTrie.hyphenate1(is, 0, 0);

        for (HyphenationTestPair test : tests.pairs()) {
            String word = test.word();
            int[] expected = test.hyphenationScore();
            int[] actual = hyphenate.apply(word);
            assertArrayEquals(expected, actual, "Hyphenation points mismatch for word: " + word);
        }
    }

}
