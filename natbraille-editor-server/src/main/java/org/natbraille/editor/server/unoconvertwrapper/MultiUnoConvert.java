package org.natbraille.editor.server.unoconvertwrapper;

import org.natbraille.editor.server.ServerConfiguration;
import org.natbraille.editor.server.unoconvertwrapper.measurements.SpamTestMultiUnoConvert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.LinkedBlockingQueue;

import static org.natbraille.editor.server.Server.serverConfiguration;

public class MultiUnoConvert {
    private static final Logger LOGGER = LoggerFactory.getLogger(MultiUnoConvert.class);

    public static class ConversionRequest {
        final byte[] source;
        final Common.UnoDestinationFormats destinationFormat;
        final CompletableFuture<byte[]> resultFuture;

        // error ?
        public ConversionRequest(byte[] source, Common.UnoDestinationFormats destinationFormat) {
            this.source = source;
            this.destinationFormat = destinationFormat;
            this.resultFuture = new CompletableFuture<>();
        }
    }

    /**
     * A common blocking queue for all servers wrappers
     */
    private final BlockingQueue<ConversionRequest> requestQueue = new LinkedBlockingQueue<>();

    /**
     * Wraps a unoserver instance (started from outside of server)
     */
    public class UnoServerWrapperRunnable implements Runnable {
        String port;

        public UnoServerWrapperRunnable(String port) {
            this.port = port;
        }

        public enum UnoServerState {Down, Up}

        private UnoServerState testServerIsUp() {
            String starmath5 = "%alpha <> %beta";
            byte[] input = Common.mmlSourceDocument(starmath5).getBytes();
            byte[] output = Common.convertTo(input, Common.UnoDestinationFormats.MML, port);
            return (new String(output, StandardCharsets.UTF_8).contains("α"))
                    ? UnoServerState.Up
                    : UnoServerState.Down;
        }

        @Override
        public void run() {
            LOGGER.info("start a unoserver wrapper on port: {}", port);
            try {
                UnoServerState serverState = testServerIsUp();
                if (serverState.equals(UnoServerState.Down)) {
                    LOGGER.error("✗ unoserver server on port {} is {}", port, serverState);
                } else {
                    LOGGER.info("✓ unoserver server on port: {} is: {}", port, serverState);
                    while (true) {
                        ConversionRequest request = requestQueue.take();
                        LOGGER.debug("start a unoconvert task on port: {}", port);
                        try {
                            byte[] result = Common.convertTo(request.source, request.destinationFormat, port);
                            request.resultFuture.complete(result);
                        } catch (Exception e) {
                            request.resultFuture.completeExceptionally(e);
                        }
                        LOGGER.debug("completed a unoconvert task on port: {}", port);
                    }
                }
            } catch (InterruptedException e) {
                LOGGER.info("interrupted unoserver wrapper on port: {}", port);
                Thread.currentThread().interrupt(); // Restore interrupt status
            }
            LOGGER.info("exit unoserver wrapper on port: {}", port);
        }
    }

    public final List<Thread> unoServerWrapperRunnableThreads = new ArrayList<>();

    /**
     * start all wrappers threads according to global server configuration
     */
    public void start() {
        for (ServerConfiguration.UnoServerConfig unoServerConfig : serverConfiguration.unoServers) {
            UnoServerWrapperRunnable unoServerWrapperRunnable = new UnoServerWrapperRunnable(unoServerConfig.port);
            Thread t = new Thread(unoServerWrapperRunnable);
            unoServerWrapperRunnableThreads.add(t);
            t.start();
        }
    }

    public byte[] convertTo(byte[] source, Common.UnoDestinationFormats destinationFormat) {
        try {
            ConversionRequest conversionRequest = new ConversionRequest(source, destinationFormat);
            requestQueue.put(conversionRequest);
            return conversionRequest.resultFuture.get();
        } catch (InterruptedException | ExecutionException e) {
            // TODO : error handling
            return new byte[]{};
        }
    }

    /**
     * specialisations
     */
    public byte[] starmathToMathML(String starMath5) {
        LOGGER.debug("Conversion with starmathToMathML : {}", starMath5);
        String sourceXmlString = Common.mmlSourceDocument(starMath5);
        return mathMLToMathML(sourceXmlString);
    }

    public byte[] mathMLToMathML(String sourceXmlString) {
        return convertTo(sourceXmlString.getBytes(), Common.UnoDestinationFormats.MML);
    }

    public byte[] anyToOdt(byte[] msWord) {
        return convertTo(msWord, Common.UnoDestinationFormats.ODT);
    }

    public boolean isUnoStarMathToMathMLOk() {
        String starmath5 = "%alpha <> %beta";
        byte[] xmlBytes = null;
        try {
            xmlBytes = starmathToMathML(starmath5);
        } catch (Exception e) {
            LOGGER.error(e.toString());
        }
        if ((xmlBytes != null)) {
            String s = new String(xmlBytes);
            return (s.contains("α") && s.contains("≠") && s.contains("β"));
        }
        return false;
    }

}
