package org.natbraille.editor.server.faketranscriptor.steps;

import org.natbraille.core.filter.NatFilterException;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.editor.server.faketranscriptor.FakeTranscription;
import org.natbraille.editor.server.faketranscriptor.Tag;
import org.natbraille.editor.server.faketranscriptor.layout.BlockNavigator;
import org.natbraille.editor.server.faketranscriptor.math.MathTranslator;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.xml.xpath.XPathExpressionException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.natbraille.editor.server.faketranscriptor.FakeTranscription.*;

public class Step3Layout {
    //static String INDENTATION_PADDING_CHAR = "P";
    static String INDENTATION_PADDING_CHAR = " ";
    //static String CENTERING_PADDING_CHAR = "C";
    static String CENTERING_PADDING_CHAR = " ";
    //static String PADDING_TO_PAGE_NUMBERING = "N";
    static String PADDING_TO_PAGE_NUMBERING = " ";

    // static int mepFirstNumberedPageNumber = 1;
    static String UL_BULLET_SYMBOL = "⠪⠕";


    enum PageNumberingLineMode {FIRST_LINE, NONE, LAST_LINE}

    enum PageNumberingInLineMode {ALONE, INCLUDED, NONE;}

    static boolean getDoPageNumbering(NatFilterOptions nfo) throws NatFilterException {
        return !nfo.getValue(NatFilterOption.LAYOUT_NUMBERING_MODE).equals("'nn'");
    }

    static PageNumberingLineMode getPageNumberingLineMode(NatFilterOptions nfo) throws NatFilterException {
        String mode = nfo.getValue(NatFilterOption.LAYOUT_NUMBERING_MODE);
        return switch (mode) {
            case "'hs'", "'hb'" -> PageNumberingLineMode.FIRST_LINE;
            case "'bs'", "'bb'" -> PageNumberingLineMode.LAST_LINE;
            default -> PageNumberingLineMode.NONE; // + case "'nn'"
        };
    }

    static PageNumberingInLineMode getPageNumberingInLineMode(NatFilterOptions nfo) throws NatFilterException {
        String mode = nfo.getValue(NatFilterOption.LAYOUT_NUMBERING_MODE);
        return switch (mode) {
            case "'hs'", "'bs'" -> PageNumberingInLineMode.ALONE;
            case "'hb'", "'bb'" -> PageNumberingInLineMode.INCLUDED;
            default -> PageNumberingInLineMode.NONE; // + case "'nn'"
        };
    }

    static class LayoutState {
        int currentPage;
        int currentLine;
        int currentOffset;
        int currentBlockLine;
        public Element currentLineLeftPadding;
        public int consecutiveEmptyLines;

        public LayoutState() {
            this.currentPage = 0;
            this.currentLine = 0;
            this.currentOffset = 0;
            this.currentBlockLine = 0;
            this.currentLineLeftPadding = null;
            this.consecutiveEmptyLines = 0;
        }
    }


    static class LayoutConfig {

        boolean doLayout;
        int pageHeight;
        int pageWidth;
        boolean doPageNumbering;
        PageNumberingLineMode pageNumberingLineMode;
        PageNumberingInLineMode pageNumberingInLineMode;
        int firstNumberedPage; // zero based
        boolean alwaysLineBreak = true; // put line breaks even if the line is full.
        boolean alwaysPageBreak = true; // put page breaks even if the page is full.
        boolean doHyphenateWords;
        int minimumSpacingBeforePageNumber = 1;
        public static String hyphenationSymbol = "-";
        int emptyLinesMode;

        LayoutConfig(NatFilterOptions filterOptions) throws NatFilterException {
            this.doLayout = filterOptions.getValueAsBoolean(NatFilterOption.LAYOUT);
            this.pageHeight = filterOptions.getValueAsInteger(NatFilterOption.FORMAT_PAGE_LENGTH);
            this.pageWidth = filterOptions.getValueAsInteger(NatFilterOption.FORMAT_LINE_LENGTH);
            this.doPageNumbering = getDoPageNumbering(filterOptions);
            this.pageNumberingLineMode = getPageNumberingLineMode(filterOptions);
            this.pageNumberingInLineMode = getPageNumberingInLineMode(filterOptions);
            this.firstNumberedPage = filterOptions.getValueAsInteger(NatFilterOption.LAYOUT_NUMBERING_START_PAGE) - 1;
            this.doHyphenateWords = filterOptions.getValueAsBoolean(NatFilterOption.LAYOUT_LIT_HYPHENATION);
            this.emptyLinesMode = filterOptions.getValueAsInteger(NatFilterOption.LAYOUT_PAGE_EMPTY_LINES_MODE) ;
        }
    }

    static boolean DEBUG_POSITION = false;

    static void addOutput(Element $layoutRoot, Element $element, String elementTextContent, LayoutState layoutState) {
        layoutState.currentOffset += elementTextContent.length();
//        if (DEBUG_POSITION) {
        $element.setAttribute("position", String.format("%d:%d:%d %d", layoutState.currentPage, layoutState.currentLine, layoutState.currentOffset, layoutState.currentBlockLine));
        //      }
        $element.setTextContent(elementTextContent);
        $layoutRoot.appendChild($element);
    }

    static void putEndOfPage(Element $layoutRoot, LayoutConfig layoutConfig, LayoutState layoutState) {
        Document doc = $layoutRoot.getOwnerDocument();
        {
            Element $lineBreak = doc.createElement("page-break");
            addOutput($layoutRoot, $lineBreak, "", layoutState);
        }
        layoutState.currentLine = 0;
        layoutState.currentPage++;
    }

    static void putEndOfLine(Element $layoutRoot, LayoutConfig layoutConfig, LayoutState layoutState) {
        Document doc = $layoutRoot.getOwnerDocument();
        {
            Element $lineBreak = doc.createElement("line-break");
            //$lineBreak.setAttribute("next-line-number", String.valueOf(layoutState.currentLine + 1));
            addOutput($layoutRoot, $lineBreak, "", layoutState);
            layoutState.currentOffset = 0;
            layoutState.currentLine++;
            if ((layoutState.currentLine) >= layoutConfig.pageHeight) {
                putEndOfPage($layoutRoot, layoutConfig, layoutState);
            }
        }
    }

    static void putPageNumber(Element $layoutRoot, LayoutConfig layoutConfig, LayoutState layoutState) {

        Document doc = $layoutRoot.getOwnerDocument();
        String prefixedBrailleNumber = String.format("%s%d", NumberPrefix, layoutState.currentPage + 1);

        int paddingSize = layoutConfig.pageWidth - prefixedBrailleNumber.length() - layoutState.currentOffset;
        String padding = PADDING_TO_PAGE_NUMBERING.repeat(paddingSize);
        {
            Element $paddingToPageNumbering = doc.createElement("padding-to-page-numbering");
            addOutput($layoutRoot, $paddingToPageNumbering, padding, layoutState);
        }
        {
            Element $pageNumbering = doc.createElement("page-numbering");
            addOutput($layoutRoot, $pageNumbering, prefixedBrailleNumber, layoutState);
        }
        putEndOfLine($layoutRoot, layoutConfig, layoutState);
    }

    static boolean lineHasPageNumber(LayoutConfig layoutConfig, LayoutState layoutState) {
        if (!layoutConfig.doLayout) return false;
        if (!layoutConfig.doPageNumbering) return false;
        if (layoutConfig.firstNumberedPage > layoutState.currentPage) return false;
        boolean matchFirst = isFirstPageLine(layoutState.currentLine) && (layoutConfig.pageNumberingLineMode == PageNumberingLineMode.FIRST_LINE);
        if (matchFirst) return true;
        boolean matchLast = isLastPageLine(layoutConfig.pageHeight, layoutState.currentLine) && (layoutConfig.pageNumberingLineMode == PageNumberingLineMode.LAST_LINE);
        if (matchLast) return true;
        return false;
    }

    static int getRemainingContentLineLength(LayoutConfig layoutConfig, LayoutState layoutState, Node block) {

        CenterMode centerMode = getCenterMode(block);
        int minPaddingRight = (centerMode != null) ? (centerMode.minPaddingRight) : (0);

        if (lineHasPageNumber(layoutConfig, layoutState)) {
            int pageNumberLength = String.format("%d", layoutState.currentPage + 1).length();
            int numberPrefixSize = 1;
            return layoutConfig.pageWidth - layoutState.currentOffset - (layoutConfig.minimumSpacingBeforePageNumber + numberPrefixSize + pageNumberLength) - minPaddingRight;
        } else {
            return layoutConfig.pageWidth - layoutState.currentOffset - minPaddingRight;
        }
    }

    static void maybePutPageNumber(Element $layoutRoot, LayoutConfig layoutConfig, LayoutState layoutState) {
        if (lineHasPageNumber(layoutConfig, layoutState)) {
            putPageNumber($layoutRoot, layoutConfig, layoutState);
        }
        /*
        if (!layoutConfig.doLayout) return;
        if (!layoutConfig.doPageNumbering) return;
        if (layoutConfig.firstNumberedPage > layoutState.currentPage) return;
        boolean matchFirst = isFirstPageLine(layoutState.currentLine) && (layoutConfig.pageNumberingLineMode == PageNumberingLineMode.FIRST_LINE);
        boolean matchLast = isLastPageLine(layoutConfig.pageHeight, layoutState.currentLine) && (layoutConfig.pageNumberingLineMode == PageNumberingLineMode.LAST_LINE);
        if (!(matchFirst || matchLast)) {
            return;
        }
        */
    }

    record ParagraphIndentationMode(int first, int subsequent) {
    }

    static Map<String, ParagraphIndentationMode> elementsDefaultIndentations = Map.of(
            "p", new ParagraphIndentationMode(2, 0),
            "poetry", new ParagraphIndentationMode(0, 1)
    );
    static ParagraphIndentationMode fallbackIndentation = new ParagraphIndentationMode(2, 1);

    static String INDENTATION_PADDING_CHAR2 = " ";

    static void putStartOfLineIndentation(Element $layoutRoot, LayoutConfig layoutConfig, LayoutState layoutState, Node block, ListParagraphMode listParagraphMode, CenterMode centerMode) {
        Document doc = $layoutRoot.getOwnerDocument();
        if (centerMode != null) {
            String padding = INDENTATION_PADDING_CHAR2.repeat(centerMode.minPaddingLeft);
            Element $paddingToPageNumbering = doc.createElement("centered-left-padding");
            addOutput($layoutRoot, $paddingToPageNumbering, padding, layoutState);
            layoutState.currentLineLeftPadding = $paddingToPageNumbering;
        } else if (listParagraphMode != null) {
            if (layoutState.currentBlockLine == 0) {
                {
                    String padding = INDENTATION_PADDING_CHAR2.repeat(listParagraphMode.firstLineBeforeSymbol);
                    Element $paddingToPageNumbering = doc.createElement("block-first-line-padding");
                    addOutput($layoutRoot, $paddingToPageNumbering, padding, layoutState);
                }
                {
                    Element $paddingToPageNumbering = doc.createElement("bullet-list-symbol");
                    addOutput($layoutRoot, $paddingToPageNumbering, listParagraphMode.symbol, layoutState);
                }
                {
                    String padding = INDENTATION_PADDING_CHAR2.repeat(listParagraphMode.firstLineAfterSymbol);
                    Element $paddingToPageNumbering = doc.createElement("after-list-symbol-padding");
                    addOutput($layoutRoot, $paddingToPageNumbering, padding, layoutState);
                }
            } else {
                {
                    String padding = INDENTATION_PADDING_CHAR2.repeat(listParagraphMode.otherLinesOffset);
                    Element $paddingToPageNumbering = doc.createElement("block-next-line-padding");
                    addOutput($layoutRoot, $paddingToPageNumbering, padding, layoutState);
                }
            }
        } else {
            ParagraphIndentationMode indentationMode = elementsDefaultIndentations.getOrDefault(block.getNodeName(), fallbackIndentation);
            if (layoutState.currentBlockLine == 0) {
                String padding = INDENTATION_PADDING_CHAR2.repeat(indentationMode.first());
                Element $paddingToPageNumbering = doc.createElement("block-first-line-padding");
                addOutput($layoutRoot, $paddingToPageNumbering, padding, layoutState);
            } else {
                String padding = INDENTATION_PADDING_CHAR2.repeat(indentationMode.subsequent());
                Element $paddingToPageNumbering = doc.createElement("block-next-line-padding");
                addOutput($layoutRoot, $paddingToPageNumbering, padding, layoutState);
            }
        }
    }

    private record LineCutResult(int cutAt, int cutAtQ, boolean useHyphenationSymbol) {
    }

    static LineCutResult getLineCut(LayoutConfig layoutConfig, LayoutState layoutState, BlockNavigator bn, int i) throws XPathExpressionException {
        Map<BlockNavigator.Position, Integer> hyphenationQualityAfter = getBlockHyphenation(bn, layoutConfig.doHyphenateWords);
        int remainingContentLineLength = getRemainingContentLineLength(layoutConfig, layoutState, bn.block);
        int cutAt = -1;
        int cutQuality = 0;
        boolean useHyphenationSymbol = false;
        {
            for (int lookahead = 0; lookahead < remainingContentLineLength; lookahead++) {
                if ((i + lookahead) >= bn.elements.size()) {
                    // the paragraph end fits on the line
                    cutAt = i + lookahead - 1;
                    useHyphenationSymbol = false;
                    cutQuality = 666;
                    break;
                }
                int q = hyphenationQualityAfter.get(bn.elements.get(i + lookahead));
                if (q > 0) {
                    // TODO : account for quality ?
                    String hyphenationSymbol = getHyphenationSymbol(bn.elements.get(i + lookahead), bn.elements.get(i + lookahead + 1));
                    if (hyphenationSymbol.isEmpty()) {
                        // no need for a hyphenation symbol
                        cutAt = i + lookahead;
                        useHyphenationSymbol = false;
                        cutQuality = q;
                    } else {
                        if (lookahead < (remainingContentLineLength - 1)) {
                            // account for the hyphenation symbol
                            cutAt = i + lookahead;
                            useHyphenationSymbol = true;
                            cutQuality = q;
                        }
                    }
                }
            }
        }
        if (cutAt == -1) {
            // no possible hyphenation found so make one (last char, at hyphen)
            if (bn.elements.size() <= remainingContentLineLength) {
                cutAt = i + remainingContentLineLength - 1;
                useHyphenationSymbol = false;
                cutQuality = 666;
            } else {
                cutAt = i + remainingContentLineLength - 2;
                useHyphenationSymbol = true;
                cutQuality = -1;
            }
        }
        return new LineCutResult(cutAt, cutQuality, useHyphenationSymbol);
    }

    private record CenterMode(int minPaddingLeft, int minPaddingRight) {
    }

    static CenterMode getCenterMode(Node block) {
        return elementsDefaultCenterModes.get(block.getNodeName());
    }

    static Map<String, CenterMode> elementsDefaultCenterModes = Map.of(
            "h1", new CenterMode(3, 3),
            "h2", new CenterMode(3, 3)
    );

    private static void centerLine(Element $layoutRoot, LayoutConfig layoutConfig, LayoutState layoutState, CenterMode centerMode, Node block) {

        // replace the left padding text content by the same left padding plus half of unused space.

        // note : if this happens on a line with a page number, the centering will be done
        //        relative to the available space before the page number

        // TODO : a refinement could be : if the *page* centered title fits with the page numbering (with the correct right padding), do that instead

        int remainingContentLength = getRemainingContentLineLength(layoutConfig, layoutState, block);
        int addedPadding = remainingContentLength / 2;
        String paddingLeft = INDENTATION_PADDING_CHAR.repeat(centerMode.minPaddingLeft + addedPadding);
        layoutState.currentLineLeftPadding.setTextContent(paddingLeft);
        layoutState.currentLineLeftPadding = null;
        layoutState.currentOffset += addedPadding;
    }

    private record EmptyLineAfterMode(int count, boolean notOverPageBreak) {
    }

    static EmptyLineAfterMode getEmptyLineAfterMode(Node block) {
        return elementsDefaultEmptyLineAfterMode.get(block.getNodeName());
    }

    static Map<String, EmptyLineAfterMode> elementsDefaultEmptyLineAfterMode = Map.of(
            "h1", new EmptyLineAfterMode(2, true),
            "h2", new EmptyLineAfterMode(1, true)
    );

    public static void putEmptyLines(Element $layoutRoot, LayoutConfig layoutConfig, LayoutState layoutState, int count, boolean notOverPageBreak) {
        for (int i = 0; i < count; i++) {
            if (notOverPageBreak && layoutState.currentLine == 0) {
                break;
            }
            if (lineHasPageNumber(layoutConfig, layoutState)) {
                putPageNumber($layoutRoot, layoutConfig, layoutState);
            } else {
                putEndOfLine($layoutRoot, layoutConfig, layoutState);
            }
        }
    }

    public static void maybePutEmptyLinesAfter(Element $layoutRoot, LayoutConfig layoutConfig, LayoutState layoutState, Node block) {
        EmptyLineAfterMode emptyLineAfterMode = getEmptyLineAfterMode(block);
        if (emptyLineAfterMode == null)
            return;
        putEmptyLines($layoutRoot, layoutConfig, layoutState, emptyLineAfterMode.count, emptyLineAfterMode.notOverPageBreak);
    }

    public static boolean isEmptyBlock(Node block) {
        Node child = block.getFirstChild();
        while (child != null) {
            if (!(Tag.spacing.tagName.equals(child.getNodeName()))) return false;
            child = child.getNextSibling();
        }
        return true;
    }
    public static void maybePutEmptyBlock(Element $layoutRoot, LayoutConfig layoutConfig, LayoutState layoutState, boolean isEmptyBlock) {
        // TODO : cover all natbraille values
        // TODO : check if (layoutConfig.emptyLinesMode == 3) is correct

        if (isEmptyBlock){
            layoutState.consecutiveEmptyLines++;
            boolean doIt = true;
            if (layoutConfig.emptyLinesMode == 0){
                // as in source document
                doIt = true;
            } else if (layoutConfig.emptyLinesMode == 1 ){
                // custom
            } else if (layoutConfig.emptyLinesMode == 2){
                // no empty line
                doIt = false;
            } else if (layoutConfig.emptyLinesMode == 3){
                // Conforme à la norme Braille
                doIt = layoutState.consecutiveEmptyLines < 2;
            } else if (layoutConfig.emptyLinesMode == 4){
                // Conforme à la norme Braille "aérée"'
            } else if (layoutConfig.emptyLinesMode == 5){
                // Interligne double
            }
            if (doIt) {
                   putEmptyLines($layoutRoot, layoutConfig, layoutState, 1, false);
             }
        } else {
            layoutState.consecutiveEmptyLines = 0;
        }
    }


    public static Document step3(FakeTranscription ft, NatFilterOptions filterOptions, Document inputDoc) throws Exception {
        // TODO: no layout mode.

        Document $layoutDocument = ft.createDOM();
        Element $layoutRoot;
        {
            $layoutRoot = $layoutDocument.createElement("body");
            $layoutDocument.appendChild($layoutRoot);
        }

        LayoutConfig layoutConfig = new LayoutConfig(filterOptions);
        LayoutState layoutState = new LayoutState();


        for (Node block : ft.xpathNodesIt(inputDoc, "//p|//h1|//h2|//h3|//h4|//h5|//h6|//pre")) {
            String tc = ((Element) block).getTextContent();
            // System.out.println(tc);
            ListParagraphMode listParagraphMode = getListParagraphMode(block);
            CenterMode centerMode = getCenterMode(block);
            boolean isEmptyBlock = isEmptyBlock(block);
            {
                // Start Of Paragraph
                layoutState.currentBlockLine = 0;
            }
            BlockNavigator bn = new BlockNavigator((Element) block, ft);

            maybePutEmptyBlock($layoutRoot, layoutConfig, layoutState,isEmptyBlock);

            if (!isEmptyBlock) {

                for (int i = 0; i < bn.elements.size(); i++) {
                    if (layoutConfig.pageNumberingInLineMode.equals(PageNumberingInLineMode.ALONE)) {
                        maybePutPageNumber($layoutRoot, layoutConfig, layoutState);
                    }
                    if (layoutState.currentOffset == 0) {
                        // remove spacing at start of line.
                        while ((i < bn.elements.size()) && (bn.elements.get(i).type.equals(Tag.spacing))) {
                            i++;
                        }
                        if (i >= bn.elements.size()) {
                            break;
                        }
                    }
                    if (layoutState.currentOffset == 0) {
                        //System.out.println("start "+bn.elements.get(i).character);
                        putStartOfLineIndentation($layoutRoot, layoutConfig, layoutState, block, listParagraphMode, centerMode);
                    }
                    {
                        LineCutResult lineCutResult = getLineCut(layoutConfig, layoutState, bn, i);

                        while (i <= lineCutResult.cutAt) {
                            // write content until cut position
                            if (i >= bn.elements.size()) {
                                System.out.println("error!");
                                break;
                            } else {
                                BlockNavigator.Position bni2 = bn.elements.get(i);
                                Element $c = $layoutDocument.createElement("character");
                                setAttributesFromBlockNavigatorPosition($c, bn, i);
                                addOutput($layoutRoot, $c, bni2.character, layoutState);
                                i++;
                            }
                        }
                        if (lineCutResult.useHyphenationSymbol) {
                            // write hyphenation symbol
                            Element $character = $layoutDocument.createElement("character");
                            setAttributesFromBlockNavigatorPosition($character, bn, i - 1);
                            $character.setAttribute("cut-quality", String.valueOf(lineCutResult.cutAtQ));
                            addOutput($layoutRoot, $character, LayoutConfig.hyphenationSymbol, layoutState);
                        }
                        {
                            if (centerMode != null) {
                                centerLine($layoutRoot, layoutConfig, layoutState, centerMode, block);
                            }
                        }
                        {
                            if (layoutConfig.pageNumberingInLineMode.equals(PageNumberingInLineMode.INCLUDED)) {
                                // includes end of line
                                maybePutPageNumber($layoutRoot, layoutConfig, layoutState);
                            }
                            if (layoutState.currentOffset > 0) {
                                putEndOfLine($layoutRoot, layoutConfig, layoutState);
                            }
                        }
                        {
                            i--;
                            layoutState.currentBlockLine++;
                        }

                    }
                }
            }

            {
                // end of paragraph
                maybePutEmptyLinesAfter($layoutRoot, layoutConfig, layoutState, block);
            }
        }
        {
            // fill last page with line breaks and possible page numbering.
            if (!(layoutState.currentLine == 0)) {
                while (layoutState.currentLine < (layoutConfig.pageHeight - 1)) {
                    // add last line;
                    putEndOfLine($layoutRoot, layoutConfig, layoutState);
                }
                if (layoutConfig.doLayout && layoutConfig.doPageNumbering && layoutConfig.pageNumberingLineMode.equals(PageNumberingLineMode.LAST_LINE)) {
                    maybePutPageNumber($layoutRoot, layoutConfig, layoutState);
                } else {
                    // if we wanted to end layout by a page break:
                    // putEndOfLine($layoutRoot, layoutConfig, layoutState);
                }
            }
        }
        return $layoutDocument;
    }

    public static Document step3old(FakeTranscription ft, NatFilterOptions filterOptions, Document inputDoc) throws Exception {

//Document inputDoc = ft.parseDOM(inputFilename);
        int firstNumberedPageNumber = 1;
        int mepOptionPageHeight = filterOptions.getValueAsInteger(NatFilterOption.FORMAT_PAGE_LENGTH);
        int mepOptionPageWidth = filterOptions.getValueAsInteger(NatFilterOption.FORMAT_LINE_LENGTH);

        boolean mepOptionDoPageNumbering = getDoPageNumbering(filterOptions);
        PageNumberingLineMode mepOptionPageNumberingLineMode = getPageNumberingLineMode(filterOptions);
        PageNumberingInLineMode mepOptionPageNumberingInLineMode = getPageNumberingInLineMode(filterOptions);

        int currentPage = 0;
        int currentLine = 0;
        int currentOffset = 0;

        // StringBuilder sb = new StringBuilder();

        Document $layoutDocument = ft.createDOM();
        Element $layoutDocumentRoot;
        {
            $layoutDocumentRoot = $layoutDocument.createElement("body");
            $layoutDocument.appendChild($layoutDocumentRoot);
        }

        for (Node block : ft.xpathNodesIt(inputDoc, "//p|//h1|//h2|//h3|//h4|//h5|//h6|//pre")) {
            currentOffset = 0;

            /*
            Element $layoutBlock = $layoutDocument.createElement(block.getNodeName());
            $layoutDocumentRoot.appendChild($layoutBlock);
            copyAttributes(block, $layoutBlock);
            */
            Element $layoutBlock = $layoutDocumentRoot;

            boolean blockIsTitle12 = (block.getNodeName().equals("h1") || block.getNodeName().equals("h2"));
            boolean blockIsTitle3456 = (block.getNodeName().equals("h3") || block.getNodeName().equals("h4")
                    || block.getNodeName().equals("h5")
                    || block.getNodeName().equals("h6")
            );

            if (block.getNodeName().equals("p") || blockIsTitle12 || blockIsTitle3456) {

                int FIRST_LINE_INDENT;
                int OTHER_LINES_INDENT;
                int LEFT_MARGIN;
                int RIGHT_MARGIN;
                ListParagraphMode listParagraphMode = getListParagraphMode(block);

                boolean center = false;
                {
                    if (block.getNodeName().equals("p") || blockIsTitle3456) {
                        center = false;
                        LEFT_MARGIN = 0;
                        RIGHT_MARGIN = 0;
                        if (listParagraphMode != null) {
                            // list
                            FIRST_LINE_INDENT = listParagraphMode.firstLineOffsetTotal;
                            OTHER_LINES_INDENT = listParagraphMode.otherLinesOffset;
                        } else {
                            // normal paragraph
                            FIRST_LINE_INDENT = 2;
                            OTHER_LINES_INDENT = 0;
                        }
                    } else /*if (block.getNodeName().equals("h1"))*/ {
                        FIRST_LINE_INDENT = 0;
                        OTHER_LINES_INDENT = 0;
                        LEFT_MARGIN = 3;
                        RIGHT_MARGIN = 3;
                        center = true;
                    }
                }
                //StringBuilder blockSb = new StringBuilder();

                BlockNavigator bn = new BlockNavigator((Element) block, ft);

                //currentOffset += 2;
                if (false) {
                    String brailleIndentation = INDENTATION_PADDING_CHAR.repeat(FIRST_LINE_INDENT);
                    //  blockSb.append(brailleIndentation);
                    {
                        Element $indent = $layoutDocument.createElement("block-first-line-indentation");
                        $indent.setAttribute("size", String.valueOf(FIRST_LINE_INDENT));
                        $indent.setTextContent(brailleIndentation);
                        $layoutBlock.appendChild($indent);
                    }
                }

                int currentIndentation = FIRST_LINE_INDENT;

                boolean doHypenateWord = !blockIsTitle12;
                Map<BlockNavigator.Position, Integer> hyphenationQualityAfter = getBlockHyphenation(bn, doHypenateWord);

                int bni = 0; // block navigator index

                int currentBlockLine = 0;
                while (bni < bn.elements.size()) {
                    // while elements of the block navigator remain

                    if (bn.elements.get(bni).type == Tag.spacing) {
                        // skip space at beginning
                        bni++;
                    }

                    if (currentBlockLine == 0) {
                        if (center) {

                        } else {
                            if (listParagraphMode != null) {
                                String before = INDENTATION_PADDING_CHAR.repeat(listParagraphMode.firstLineBeforeSymbol);
                                //blockSb.append(before);
                                {
                                    Element $indent = $layoutDocument.createElement("list-first-line-indentation");
                                    $indent.setAttribute("size", String.valueOf(listParagraphMode.firstLineBeforeSymbol));
                                    $indent.setTextContent(before);
                                    $layoutBlock.appendChild($indent);
                                }
                                //blockSb.append(listParagraphMode.symbol);
                                {
                                    {
                                        Element $indent = $layoutDocument.createElement("list-bullet");
                                        $indent.setTextContent(listParagraphMode.symbol);
                                        $layoutBlock.appendChild($indent);
                                    }
                                }
                                //blockSb.append(INDENTATION_PADDING_CHAR.repeat(listParagraphMode.firstLineAfterSymbol));
                                {
                                    Element $indent = $layoutDocument.createElement("spacing-after-bullet");
                                    $indent.setAttribute("size", String.valueOf(listParagraphMode.firstLineAfterSymbol));
                                    $indent.setTextContent(INDENTATION_PADDING_CHAR);
                                    $layoutBlock.appendChild($indent);
                                }
                            } else {
                                String brailleIndentation = INDENTATION_PADDING_CHAR.repeat(FIRST_LINE_INDENT);
                                //blockSb.append(brailleIndentation);
                                {
                                    Element $indent = $layoutDocument.createElement("block-first-line-indentation");
                                    $indent.setAttribute("size", String.valueOf(FIRST_LINE_INDENT));
                                    $indent.setTextContent(brailleIndentation);
                                    $layoutBlock.appendChild($indent);
                                }
                            }
                        }
                    } else {
                        // currentBlockLine > 0
                        if (center) {

                        } else {
                            //blockSb.append(INDENTATION_PADDING_CHAR.repeat(OTHER_LINES_INDENT));
                            {
                                Element $indent = $layoutDocument.createElement("block-other-lines-indentation");
                                $indent.setAttribute("size", String.valueOf(OTHER_LINES_INDENT));
                                $indent.setTextContent(INDENTATION_PADDING_CHAR.repeat(OTHER_LINES_INDENT));
                                $layoutBlock.appendChild($indent);
                            }
                        }
                    }


                    int availableLineWidth = computeContentAvailableLineWidth(mepOptionDoPageNumbering, mepOptionPageNumberingLineMode, mepOptionPageNumberingInLineMode, firstNumberedPageNumber, mepOptionPageWidth, mepOptionPageHeight, currentPage, currentLine);
                    // last considered block navigator index (considered available line length and indentation)
                    int lastBni = Math.min(bn.elements.size() - 1, (bni + (availableLineWidth - currentIndentation - LEFT_MARGIN - RIGHT_MARGIN) - 1));

                    // find hyphenation position and use of break symbol.
                    int cutIdx = lastBni;
                    int cutQuality = 0;
                    String cutSymbol = "";
                    if (lastBni != (bn.elements.size() - 1))
                        for (int ci = lastBni; ci > (bni + availableLineWidth / 2); ci--) {
                            // search the best hyphenation position from last possible character
                            // in line to the middle of the line
                            int distanceToLineEnd = lastBni - ci;
                            BlockNavigator.Position position = bn.elements.get(ci);
                            String cs = "";
                            // boolean inWord = false;
                            if (ci < (bn.elements.size() - 1)) {
                                // ?
                                cs = getHyphenationSymbol(position, bn.elements.get(ci + 1));
                                if (distanceToLineEnd < cs.length()) {
                                    continue;
                                }
                                // inWord = true;
                            }
                            int hq = hyphenationQualityAfter.get(position);
                            //int hq = inWord?(1):hyphenationQualityAfter.get(position);

                            // evaluate hyphenation quality taking account of the position in line
                            // (the furthest, the best)
                            int fhq = hq - distanceToLineEnd / 4;
                            if (fhq > cutQuality) {
                                cutQuality = fhq;
                                cutIdx = ci;
                                cutSymbol = cs;
                            }
                        }

                    // length of chars to write not including mode indentation
                    int centerLeftPadding = 0;
                    if (center) {
                        // center paragraph line
                        int writeLineLength = (1 + cutIdx - bni) + cutSymbol.length();
                        // TODO? round up for left>right?
                        centerLeftPadding = (availableLineWidth - writeLineLength) / 2;
                    }

                    // write line
                    if (center) {
                        //blockSb.append(CENTERING_PADDING_CHAR.repeat(centerLeftPadding));
                        {
                            Element centerPadding = $layoutDocument.createElement("center-left-padding");
                            centerPadding.setAttribute("size", Integer.toString(centerLeftPadding));
                            centerPadding.setAttribute("cut-symbol", cutSymbol);
                            centerPadding.setTextContent(" ".repeat(centerLeftPadding));
                            $layoutBlock.appendChild(centerPadding);
                            setAttributesFromBlockNavigatorPosition(centerPadding, bn, 0);
                        }
                    }
                    int lineOffset = currentIndentation + centerLeftPadding;
                    for (int lbni = bni; lbni <= cutIdx; lbni++) {
                        BlockNavigator.Position position = bn.elements.get(lbni);
                        //blockSb.append("{"+lineOffset+"}"+position.character);
                        //blockSb.append(position.character);
                        {
                            Element $character = $layoutDocument.createElement("character");
                            $character.setTextContent(position.character);
                            $layoutBlock.appendChild($character);
                            $character.setAttribute("cut-quality", String.valueOf(cutQuality));
                            setAttributesFromBlockNavigatorPosition($character, bn, lbni);
                        }
                        bni++;
                        lineOffset++;
                    }
                    if (cutSymbol.length() > 0) {
                        //blockSb.append(cutSymbol);
                        {
                            Element $lineBreakHyphen = $layoutDocument.createElement("line-break-symbol");
                            $lineBreakHyphen.setAttribute("break-symbol", cutSymbol);
                            $lineBreakHyphen.setTextContent(cutSymbol);
                            $layoutBlock.appendChild($lineBreakHyphen);
                        }
                        lineOffset++;
                    }
                    // blockSb.append("[:"+writeLineLength+":]");
                    //
                    {
                        if (doLineHaveNumbering(mepOptionDoPageNumbering, mepOptionPageNumberingLineMode, firstNumberedPageNumber, mepOptionPageHeight, currentPage, currentLine)) {


                            int lineFullLength = mepOptionPageWidth;
                            String brailleNumber = String.format("⠠%d", currentPage + 1);
                            int paddingSize = lineFullLength - brailleNumber.length() - lineOffset;
                            String padding = PADDING_TO_PAGE_NUMBERING.repeat(paddingSize);
                            //
                            //blockSb.append(padding);
                            lineOffset += padding.length();
                            {
                                Element $paddingToPageNumbering = $layoutDocument.createElement("padding-to-page-numbering");
                                $paddingToPageNumbering.setAttribute("size", String.valueOf(paddingSize));
                                $paddingToPageNumbering.setAttribute("padding", padding);
                                $paddingToPageNumbering.setTextContent(padding);
                                $layoutBlock.appendChild($paddingToPageNumbering);
                            }
                            //
                            //
                            //  blockSb.append(brailleNumber);
                            {
                                Element $pageNumbering = $layoutDocument.createElement("page-numbering");
                                $pageNumbering.setAttribute("text", brailleNumber);
                                $pageNumbering.setTextContent(brailleNumber);
                                $layoutBlock.appendChild($pageNumbering);
                            }
                            lineOffset += brailleNumber.length();
                        }
                    }

                    //
                    //blockSb.append("\n");
                    currentLine++;
                    {
                        Element $lineBreak = $layoutDocument.createElement("line-break");
                        $lineBreak.setAttribute("next-line-number", String.valueOf(currentLine + 1));
                        $layoutBlock.appendChild($lineBreak);
                    }
                    currentIndentation = OTHER_LINES_INDENT;
                    currentBlockLine++;

                    if (currentLine == mepOptionPageHeight) {
                        currentLine = 0;
                        //currentOffset = 0;
                        currentPage++;
                        //  blockSb.append(String.format("------------------- %d \n", currentPage + 1));
                        {
                            Element $pageBreak = $layoutDocument.createElement("page-break");
                            $pageBreak.setAttribute("next-page-number", String.valueOf(currentPage));
                            $layoutBlock.appendChild($pageBreak);
                        }

                    }
                }

                //sb.append(blockSb.toString());
            } else if (block.getNodeName().equals("pre")) {
                String[] linesArray = block.getTextContent().split("\n");
                for (String line : linesArray) {
                    {
                        Element $indent = $layoutDocument.createElement("pre");
                        //$indent.setAttribute("size", String.valueOf(listParagraphMode.firstLineBeforeSymbol));
                        $indent.setTextContent(line);
                        $layoutBlock.appendChild($indent);
                    }
                    currentLine++;
                    if (currentLine == mepOptionPageHeight) {
                        currentLine = 0;
                        currentPage++;
                    }
                }
            }

        }

        //Files.writeString(Path.of(outputFilename + ".txt"), sb.toString());
        //ft.serializeDOM(outputFilename, $layoutDocument);
        return $layoutDocument;
    }

    static boolean isLastPageLine(int mepPageHeight, int currentLine) {
        return currentLine == (mepPageHeight - 1);
    }

    static boolean isFirstPageLine(int currentLine) {
        return currentLine == 0;
    }

    static boolean doLineHaveNumbering(boolean doPageNumbering, PageNumberingLineMode pageNumberingLineMode, int firstNumberedPageNumber, int pageHeight, int currentPage, int currentLine) {
        if (doPageNumbering && (currentPage >= firstNumberedPageNumber)) {
            boolean matchFirst = (pageNumberingLineMode == PageNumberingLineMode.FIRST_LINE) && isFirstPageLine(currentLine);
            boolean matchLast = (pageNumberingLineMode == PageNumberingLineMode.LAST_LINE) && isLastPageLine(pageHeight, currentLine);
            return (matchLast || matchFirst);
        }
        return false;
    }

    static int computeContentAvailableLineWidth(boolean doPageNumbering, PageNumberingLineMode pageNumberingLineMode, PageNumberingInLineMode pageNumberingInLineMode, int firstNumberedPageNumber, int pageWidth, int pageHeight, int currentPage, int currentLine) {
        if (doPageNumbering && (currentPage >= firstNumberedPageNumber)) {
            boolean matchFirst = (pageNumberingLineMode == PageNumberingLineMode.FIRST_LINE) && isFirstPageLine(currentLine);
            boolean matchLast = (pageNumberingLineMode == PageNumberingLineMode.LAST_LINE) && isLastPageLine(pageHeight, currentLine);
            if (matchFirst || matchLast) {
                if (pageNumberingInLineMode == PageNumberingInLineMode.ALONE) {
                    return 0;
                } else {
                    int digitsLength = String.valueOf(currentPage + 1).length();
                    int numberPrefixLength = 1;
                    int minSpacesBefore = 3;
                    return pageWidth - digitsLength - numberPrefixLength - minSpacesBefore;
                }
            }
        }
        return pageWidth;
    }

    /*
            public static String buildXpathPathOfNode(Node node) {
                List<String> path = new ArrayList<>();
                for (; ; ) {
                    if (node.hasAttributes()) {
                        String fragmentId = ((Element) node).getAttribute("fragmentId");
                        if (fragmentId.length() > 0) {
                            path.add(0, "//*[@fragmentId=\"" + fragmentId + "\"");
                            break;
                        }
                    }
                    Node parent = node.getParentNode();
                    if (parent == null) {
                        path.add(0, "/");
                        break;
                    }

                    int indexInParent = getNodeIndexInParent(node);
                    path.add(0, "/*[" + indexInParent + "]");
                    node = parent;
                }
                return String.join("", path);
            }
        */
    public static void setAttributesFromBlockNavigatorPosition(Element $layoutElement, BlockNavigator blockNavigator, int blockNavigatorIndex) {
        BlockNavigator.Position position = blockNavigator.elements.get(blockNavigatorIndex);
        //$layoutElement.setAttribute("ref-character", position.character);
        $layoutElement.setAttribute("dbg-ref-type", String.valueOf(position.type));
        $layoutElement.setAttribute("ref-inner-offset", String.valueOf(position.innerOffset));
        //$layoutElement.setAttribute("ref-global-offset", String.valueOf(position.globalOffset));



        String refFragmentId = ((Element) position.typeNode).getAttribute("fragmentId");

        // for math nodes, always put the outermost ("math" fragmentId ref)
        Node z = position.typeNode.getParentNode();
        if (z != null){
            Node zz = z.getParentNode();
            if (zz != null){
                if (zz.getNodeName().equals("math")){
                    refFragmentId = ((Element)zz).getAttribute("fragmentId");
                }
            }
        }


        $layoutElement.setAttribute("ref-fragmentId", refFragmentId);
        //$layoutElement.setAttribute("ref-type-node-name", ((Element) position.typeNode).getTagName());
        //$layoutElement.setAttribute("node-fragmentId",position.node.getAttributes("fragmentId"));
        //$layoutElement.setAttribute("ref-node-index", String.valueOf(position.nodeIndex));
        //$layoutElement.setAttribute("ref-path", buildXpathPathOfNode(position.typeNode));

    }

    public static ListType getEnclosingListType(Node paragraph) {
        Node li = paragraph.getParentNode();
        if ((li != null) && li.getNodeName().equals("li")) {
            Node list = li.getParentNode();
            if (list != null) {
                return switch (list.getNodeName()) {
                    case "ul" -> ListType.UL;
                    case "ol" -> ListType.OL;
                    default -> null;
                };
            }
        }
        return null;
    }

    /**
     * count the index number of a list item containing the paragraph
     *
     * @param paragraph
     * @return the 1-based index number or 0 if it does not apply
     */
    public static int getListItemParagraphIndex(Node paragraph) {
        Node li = paragraph.getParentNode();
        int count = 0;
        while (li != null) {
            count++;
            li = li.getPreviousSibling();
        }
        return count;
    }

    public static int getListLevel(Node paragraph) {
        int level = 0;
        Node p = paragraph.getParentNode();
        while (p != null) {
            String tag = p.getNodeName();
            if (tag.equals("ul") || tag.equals("ol")) {
                level++;
            }
            p = p.getParentNode();
        }
        return level;
    }

    public static int getFollowingListItemLevel(Node paragraph) {
        // <li><p>txt</p><li>
        Node parentLi = paragraph.getParentNode();
        Node nextLi = parentLi.getNextSibling();
        /*if (nextLi) {

        }
        String nextLiName =
        if (nextLi.getNodeName())
*/
        return 0;
    }

    public static ListParagraphMode getListParagraphMode(Node paragraph) {
        int level = getListLevel(paragraph);
        if (level > 0) {
            ListType listType = getEnclosingListType(paragraph);
            int index = getListItemParagraphIndex(paragraph);
            return new ListParagraphMode(level, listType, index);
        } else {
            return null;
        }

    }


    public static enum ListType {
        UL, OL
    }

    public static class ElementHyphenationTriplet {
        final String from;
        final String to;
        final int quality;

        public ElementHyphenationTriplet(String from, String to, int quality) {
            this.from = from;
            this.to = to;
            this.quality = quality;
        }
    }

    static ElementHyphenationTriplet[] elementHyphenationTriplets = new ElementHyphenationTriplet[]{
            new ElementHyphenationTriplet(Tag.math_prefix.tagName, Tag.pre_typeset_math.tagName, -1),
            new ElementHyphenationTriplet("*", Tag.spacing.tagName, 10),
    };

    static int getElementHyphenation(Tag from, Tag to) {
        // System.out.println(from.tagName + " -> " + to.tagName);
        for (ElementHyphenationTriplet elementHyphenationTriplet : elementHyphenationTriplets) {
            if (elementHyphenationTriplet.from.equals(from.tagName) && elementHyphenationTriplet.to.equals((to.tagName))) {
                return elementHyphenationTriplet.quality;
            } else if (elementHyphenationTriplet.from.equals("*") && elementHyphenationTriplet.to.equals((to.tagName))) {
                return elementHyphenationTriplet.quality;
            } else if (elementHyphenationTriplet.from.equals(from.tagName) && elementHyphenationTriplet.to.equals("*")) {
                return elementHyphenationTriplet.quality;
            }
        }
        return 0;
    }

    public static Map<BlockNavigator.Position, Integer> getBlockHyphenation(BlockNavigator blockNavigator, boolean doHypenateWords) throws XPathExpressionException {
        Element aP = blockNavigator.block;

        // hyphenation quality for words
        Map<Node, int[]> lettersHyphenations = new HashMap<>();

        Node maybeWord = aP.getFirstChild();
        while (maybeWord != null) {
            if ("word".equals(maybeWord.getNodeName())) {
                Element word = (Element) maybeWord;
                // should be not null
                Element braille = getBraille(word);
                // should be not null
                Element letters = getFirstElementByTagName(braille, "letters");
                lettersHyphenations.put(letters, hyphenate.apply(letters.getTextContent()));
            } else if ("math".equals(maybeWord.getNodeName())) {
                Element math = (Element) maybeWord;
                // braille element
                Element braille = getBraille(math);
                Element preTypesetMath = getFirstElementByTagName(braille, Tag.pre_typeset_math.toString());

                String rawNatbraillePreTypeset = preTypesetMath.getAttribute("rawNatbraillePreTypeset");
                int[] hyphenation = MathTranslator.NatbraillePreTypesetBraille.getHyphenationArray(rawNatbraillePreTypeset, 8, 5, 0);

        //        System.out.println(rawNatbraillePreTypeset + " " + preTypesetMath.getTextContent() + " " + Arrays.toString(hyphenation));

                lettersHyphenations.put(preTypesetMath, hyphenation);

            }
            maybeWord = maybeWord.getNextSibling();
        }


        Map<BlockNavigator.Position, Integer> hyphenationQualityAfter = new HashMap<>();

        for (BlockNavigator.Position position : blockNavigator.elements) {
            int hyphenationQuality;
            if (lettersHyphenations.containsKey(position.typeNode) && (position.innerOffset < (position.innerLength - 1))) {
                // word
                if (doHypenateWords) {
                    int[] lettersHyphenation = lettersHyphenations.get(position.typeNode);
                    int lliangValue = lettersHyphenation[position.innerOffset];
                    boolean canHyphenate = ((lliangValue % 2) > 0); // lliang odd numbers indicate an acceptable location
                    hyphenationQuality = canHyphenate ? (lliangValue) : (-1);
                } else {
                    hyphenationQuality = -1;
                }
            } else if (position.globalOffset < (blockNavigator.elements.size() - 1)) {
                // inter-elements
                BlockNavigator.Position nextPosition = blockNavigator.elements.get(position.globalOffset + 1);
                hyphenationQuality = getElementHyphenation(position.type, nextPosition.type);
            } else {
                hyphenationQuality = -1;
            }
            // System.out.println(String.format("%2d | %s | %s[%d]", position.globalOffset, position.character, position.type.tagName, position.innerOffset));
            // System.out.println(String.format("✂ %d", hyphenationQuality));
            hyphenationQualityAfter.put(position, hyphenationQuality);
            // if ((hyphenationQuality%2)>0){
            // System.out.println(String.format("%d @ %d[%d/%d] %s (%s)",hyphenationQuality,position.globalOffset,position.innerOffset,position.innerLength,position.character,position.type.tagName));
            // }
        }
        return hyphenationQualityAfter;
    }

    public static String getHyphenationSymbol(BlockNavigator.Position before, BlockNavigator.Position after) {
        if (before.typeNode.equals(after.typeNode)) {
            if (before.type == Tag.letters) {
                return "-";
            }
        }
        return "";
    }

    public static class ListParagraphMode {

        /**
         * CBFU p.60
         * Lorsqu’une liste comporte plus d’un niveau hiérarchique, le retrait appliqué à la
         * portion de texte qui excède la première ligne d’un élément de cette liste est de
         * quatre cellules. Ainsi, les éléments du premier niveau hiérarchique sont disposés en
         * 1-5, les éléments du deuxième niveau en 3-7, ceux du troisième niveau en 5-9, et
         * ainsi de suite.
         * <p>
         * Cependant, afin de minimiser les pertes d’espace, le transcripteur
         * réduit le retrait à deux cellules seulement lorsqu’un élément est immédiatement suivi
         * d’un autre élément de même niveau ou de niveau supérieur.
         * <p>
         * Le retrait est également réduit à deux cellules pour tout élément appartenant au
         * dernier niveau hiérarchique
         * de même que pour le dernier élément de la liste.
         */
        public int level;
        public int firstLineBeforeSymbol;
        public String symbol = UL_BULLET_SYMBOL;
        public int firstLineAfterSymbol = 1;
        public int firstLineOffsetTotal;
        public int otherLineExtraOffset = 4;
        public int otherLinesOffset;

        public ListParagraphMode(int level, ListType listType, int index) {
            this.level = level;
            this.firstLineBeforeSymbol = 2 * (level - 1);
            if (listType == ListType.OL) {
                symbol = String.format("%s%d.", NumberPrefix, index);
            } else if (listType == ListType.UL) {
                symbol = UL_BULLET_SYMBOL;
            }
            this.firstLineOffsetTotal = this.firstLineBeforeSymbol + symbol.length() + this.firstLineAfterSymbol;
            this.otherLinesOffset = this.firstLineBeforeSymbol + otherLineExtraOffset;
        }
    }
}
