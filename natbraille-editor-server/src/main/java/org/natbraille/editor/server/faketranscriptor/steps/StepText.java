package org.natbraille.editor.server.faketranscriptor.steps;


import org.natbraille.core.filter.NatFilterException;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.filter.options.NatFilterOptions;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

public class StepText {
    public static String renderToText(Document $layout, NatFilterOptions natFilterOptions) throws NatFilterException {

        boolean insertPageBreaks = natFilterOptions.getValueAsBoolean(NatFilterOption.LAYOUT_PAGE_BREAKS);

        StringBuilder sb = new StringBuilder();
        Node $body = $layout.getFirstChild();
        Node $n = $body.getFirstChild();
        while ($n != null) {
            String nodeName =$n.getNodeName();
            if ("line-break".equals(nodeName)){
                sb.append("\n");
            } else if ("page-break".equals(nodeName)){
                if (insertPageBreaks) {
                    sb.append("\f");
                }
            } else {
                sb.append($n.getTextContent());
            }
            $n = $n.getNextSibling();
        }
        return sb.toString();
    }
}
