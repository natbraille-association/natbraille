package org.natbraille.editor.server;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * write a full default configuration to stdout
 */
public class WriteSampleConfiguration {
    public static void main(String[] args) throws JsonProcessingException {
        System.out.println((new ObjectMapper()).writeValueAsString(new ServerConfiguration()));
    }
}
