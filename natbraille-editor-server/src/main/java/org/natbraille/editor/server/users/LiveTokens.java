package org.natbraille.editor.server.users;

import org.natbraille.editor.server.tools.RandomString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.time.OffsetDateTime;
import java.util.HashMap;
import java.util.Map;

public class LiveTokens {
    private static Logger LOGGER = LoggerFactory.getLogger(LiveTokens.class);

    public class TokenInfo {
        public String username;
        public OffsetDateTime created;
        public OffsetDateTime lastUsed;

        public void markUsed() {
            this.lastUsed = OffsetDateTime.now();
        }

        public Duration getAge() {
            return Duration.between(created, OffsetDateTime.now());
        }

        public Duration getDurationSinceLastUsed() {
            return Duration.between(lastUsed, OffsetDateTime.now());
        }

        public TokenInfo(String username) {
            this.username = username;
            this.created = OffsetDateTime.now();
            this.lastUsed = OffsetDateTime.now();
        }
        public TokenInfo(String username, OffsetDateTime created, OffsetDateTime lastUsed) {
            this.username = username;
            this.created = created;
            this.lastUsed = lastUsed;
        }
    }

    protected Map<String, TokenInfo> tokens = new HashMap<>();
    private RandomString randomString = new RandomString();

    public void load(String token, String username, OffsetDateTime created, OffsetDateTime lastUsed){
        tokens.put(token,new TokenInfo(username,created,lastUsed));
    }


    public Map.Entry<String, TokenInfo> create(String username) {
        String token = randomString.nextString();
        TokenInfo tokenInfo = new TokenInfo(username);
        LOGGER.info("create token {} {}", token, tokenInfo);
        tokens.put(token,tokenInfo);
        return Map.entry(token, tokenInfo);
    }

    public TokenInfo getInfo(String token) {
        return tokens.get(token);
    }

    public boolean checkUsername(String token, String username) {
        TokenInfo tokenInfo = tokens.get(token);
        if (tokenInfo != null) {
            tokenInfo.markUsed();
            return tokenInfo.username.equals(username);
        } else {
            return false;
        }
    }

    public TokenInfo remove(String token) {
        return tokens.remove(token);
    }

}
