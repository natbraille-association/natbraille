package org.natbraille.editor.server.performance;


public class Estimator {
    long startTimeNs;
    long doneTimeNs;
    int done = 0;
    final int total;

    public void start() {
        startTimeNs = System.nanoTime();
    }

    public void setDone(int done) {
        this.done = done;
        this.doneTimeNs = System.nanoTime();
    }

    public String getPercentDone() {
        return Double.toString(100.0 * ((double) done) / ((double) total)) + "%";
    }

    public double getNsPerUnit() {
        long nsElapsed = doneTimeNs - startTimeNs;
        if (done == 0) {
            return 0;
        } else {
            return ((double) nsElapsed) / ((double) done);
        }
    }

    public String getRemainingSeconds() {
        double nsPerUnit = getNsPerUnit();
        double nsRemaining = ((double) (total - done)) * nsPerUnit;
        double sRemaining = nsRemaining * 1.0e-9;
        return sRemaining + "s";
    }


    public Estimator(int total) {
        this.total = total;
    }

}
