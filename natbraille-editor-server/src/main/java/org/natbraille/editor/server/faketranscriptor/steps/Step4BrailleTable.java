package org.natbraille.editor.server.faketranscriptor.steps;

import org.natbraille.brailletable.BrailleTable;
import org.natbraille.brailletable.BrailleTables;
import org.natbraille.brailletable.Converter;
import org.natbraille.brailletable.system.SystemBrailleTables;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.editor.server.faketranscriptor.FakeTranscription;
import org.w3c.dom.Document;

public class Step4BrailleTable {

    public static void step4(FakeTranscription ft, /*String inputFilename, String outputFilename, */ NatFilterOptions natFilterOptions, Document inputDoc) throws Exception {

        // Document inputDoc = ft.parseDOM(inputFilename);
        //BrailleTable inputBrailleTable = BrailleTables.forName(SystemBrailleTables.MixedFrUnicode);
        BrailleTable inputBrailleTable = BrailleTables.forName(SystemBrailleTables.BrailleUTF8);
        BrailleTable outputBrailleTable = BrailleTables.forName(natFilterOptions.getValue(NatFilterOption.FORMAT_OUTPUT_BRAILLE_TABLE));
        // System.out.println("table"+outputBrailleTable.getName());
        //Converter brailleTableConverter = new Converter(BrailleTables.unicodeBrailleTable(),outputBrailleTable);
        Converter brailleTableConverter = new Converter(inputBrailleTable, outputBrailleTable);
        brailleTableConverter.getConfig().setCopyMissingSource(true);
        brailleTableConverter.getConfig().setCopyMissingDest(true);
        brailleTableConverter.getConfig().setSpaceIsEmptyBrailleCell(false);
        FakeTranscription.forEachNode(inputDoc, "//*[not(local-name(.)='pre')]/text()", textNode -> {

            String sourceString = textNode.getTextContent();
            // System.out.println("sourceString:"+sourceString);
            String brailleUtf8String = FakeTranscription.transliteration.transliterateString(sourceString);
            // System.out.println("brailleUtf8String:"+brailleUtf8String);
            String outputTableString = brailleTableConverter.convert(brailleUtf8String);
            // System.out.println("outputTableString:"+outputBrailleTable.getName()+":"+outputTableString);
            textNode.setTextContent(outputTableString);
        });
   //     return inputDoc;
        //ft.serializeDOM(outputFilename, inputDoc);
    }
}
