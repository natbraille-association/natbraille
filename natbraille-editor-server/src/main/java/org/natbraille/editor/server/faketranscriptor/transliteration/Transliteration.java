package org.natbraille.editor.server.faketranscriptor.transliteration;

import org.natbraille.editor.server.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.InputSource;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Transliteration {
    private static final Logger LOGGER = LoggerFactory.getLogger(Transliteration.class);

    public static class TransliterationMap extends HashMap<Character, String> {
    }

    public static class Loader {
        private static char pointsStringToUnicodeBraille(String points) {
            char code = 0x2800;
            for (int i = 0; i < points.length(); i++) {
                int number = Integer.parseInt(points.substring(i, i + 1));
                code |= (char) (1 << (number - 1));
            }
            return code;
        }

        public static TransliterationMap loadTransliterationFromResource(String resourceName) throws IOException {
            InputStream inputSource = Transliteration.class.getResourceAsStream(resourceName);
            if (inputSource == null) {
                throw new IOException("Transliteration map resource not found");
            }
            return loadTransliteration(inputSource);
        }

        private static TransliterationMap loadTransliteration(InputStream inputStream) throws IOException {
            String text = new String(inputStream.readAllBytes(), StandardCharsets.UTF_8);
            Pattern linePattern = Pattern.compile("^(.+?)\\s+([^#]+)(?:#.*)?");
            TransliterationMap transliterationMap = new TransliterationMap();
            for (String line : text.split("\n+")) {
                Matcher m = linePattern.matcher(line);
                if (m.find() && (m.groupCount() == 2)) {
                    // System.out.println("# line : " + m.group(1) + "->" + "[" + m.group(2) + "]");
                    String sourceText = m.group(1);
                    char source;
                    if (sourceText.length() == 1) {
                        // a single character source character is read as-is
                        source = sourceText.charAt(0);
                    } else if (sourceText.startsWith("\\")) {
                        // a source character starting with \ is parsed as a codepoint
                        int code = Integer.parseInt(sourceText.substring(1));
                        source = Character.toChars(code)[0];
                    } else {
                        LOGGER.error("cannot parse transliteration table line '{}' ; source '{}'", line, sourceText);
                        throw new IOException("error while reading transliteration table");
                    }

                    String[] destinationPointStrings = m.group(2).trim().split("[ , ]+");
                    StringBuilder sb = new StringBuilder();
                    for (String destinationFragment : destinationPointStrings) {
                        // a destination character starting with p denotes a braille p12346 representation
                        if (destinationFragment.startsWith("p")) {
                            sb.append(pointsStringToUnicodeBraille(destinationFragment.substring(1)));
                        } else if (destinationFragment.startsWith("\\")) {
                            // a destination character starting with \ is parsed as a codepoint
                            int code = Integer.parseInt(sourceText.substring(1));
                            sb.append(Character.toChars(code));
                        } else {
                            LOGGER.error("cannot parse transliteration table line '{}' ; destination '{}'", line, destinationFragment);
                            throw new IOException("error while reading transliteration table");
                        }
                    }
                    // System.out.println(" > " + source + " " + sb.toString());
                    transliterationMap.put(source, sb.toString());
                }
            }
            return transliterationMap;
        }
    }

    private final TransliterationMap map;

    private final String unknownMatch;

    /**
     * If unknownMatch member is set to a non-null value, it is used as a replacement for unmatched characters. If set to null, the source character is kept.
     */
    public String transliterateCharacter(char sourceCharacter) {
        if (this.unknownMatch == null) {
            String s = map.get(sourceCharacter);
            return (s == null) ? (String.valueOf((sourceCharacter))) : s;
        } else {
            String s = map.getOrDefault(sourceCharacter, unknownMatch);
            if (s.equals(unknownMatch)) {
                LOGGER.debug("character '{}' is not not in transliteration table", sourceCharacter);
            }
            return s;
        }
    }

    public String transliterateCharacter(String sourceCharacter) {
        return transliterateCharacter(sourceCharacter.charAt(0));
    }

    public String transliterateString(String sourceString) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < sourceString.length(); i++) {
            sb.append(transliterateCharacter(sourceString.charAt(i)));
        }
        return sb.toString();
    }

    public Transliteration(TransliterationMap map) {
        this.map = map;
        this.unknownMatch = null;
    }

    public Transliteration(TransliterationMap map, String unknownMatch) {
        this.map = map;
        this.unknownMatch = unknownMatch;
    }


    public static void main(String[] args) throws IOException {
        Transliteration transliteration = new Transliteration(Loader.loadTransliterationFromResource("transliterate-fr.txt"));
        System.out.println(transliteration.transliterateString("bonjour! 0123456"));
    }
}
