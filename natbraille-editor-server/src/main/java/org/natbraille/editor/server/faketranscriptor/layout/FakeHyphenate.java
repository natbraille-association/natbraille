package org.natbraille.editor.server.faketranscriptor.layout;

import org.natbraille.editor.server.faketranscriptor.FakeTranscription;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * UNUSED - REMOVE ME
 */

/**
 * Implements word hyphenation using (something close to) Liang algorithm
 * see https://tug.org/tugboat/tb27-1/tb86nemeth.pdf
 * see hyphenation.xsl
 */
public class FakeHyphenate {
    public static final class HyphenationPattern {
        public final String matchPattern;
        public final int level[];

        public HyphenationPattern(String rawPattern) {
            List<Integer> weights = new ArrayList<>();
            StringBuilder sb = new StringBuilder();
            boolean previousIsSet = false;
            for (int i = 0; i < rawPattern.length(); i++) {
                char c = rawPattern.charAt(i);
                int digit = "0123456789".indexOf(c);
                if (digit != -1) {
                    weights.add(digit);
                    previousIsSet = true;
                } else {
                    if (!previousIsSet) {
                        weights.add(0);
                    }
                    sb.append(c);
                    previousIsSet = false;
                }
            }
            matchPattern = sb.toString();
            level = weights.stream().mapToInt(Integer::intValue).toArray();
        }
    }

    public static List<HyphenationPattern> parseHyphens(InputStream is) throws IOException, NullPointerException {
        String s = new String(is.readAllBytes(), StandardCharsets.UTF_8);
        String[] a = s.replaceAll("^UTF-8\n", "").trim().split("\n");
        return Arrays.stream(a).map(HyphenationPattern::new).collect(Collectors.toList());
    }

    public static int[] hyphenateWord(List<HyphenationPattern> hyphenationPatterns, String word) {
        String boundedWord = "." + word.toLowerCase() + ".";
        int levels[] = new int[boundedWord.length()];
        for (HyphenationPattern p : hyphenationPatterns) {
            int foundAtIndex = -1;
            do {
                foundAtIndex = boundedWord.indexOf(p.matchPattern, foundAtIndex + 1);
                if (foundAtIndex != -1) {
                    for (int i = 0; i < p.level.length; i++)
                        levels[foundAtIndex + i] += p.level[i];
                }
            } while (foundAtIndex != -1);
        }
        return Arrays.copyOfRange(levels, 2, levels.length - 1);
    }

    public static int[] hyphenateWord(List<HyphenationPattern> hyphenationPatterns, String word, int minLetterCountBeforeBreak, int minLetterCountAfterBreak) {
        int[] levels = hyphenateWord(hyphenationPatterns, word);
        Arrays.fill(levels, 0, Math.max(0, Math.min(minLetterCountBeforeBreak - 1, levels.length)), 0);
        Arrays.fill(levels, Math.max(0, Math.min(levels.length-1,levels.length - (minLetterCountAfterBreak - 1))), levels.length, 0);
        return levels;
    }

    public static String dbgWord(String word, int levels[]) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < word.length(); i++) {
            sb.append(word.charAt(i));
            if ((i < levels.length) && (levels[i] > 0)) sb.append(levels[i]);
        }
        return sb.toString();
    }

    public static void main(String[] args) throws IOException {
        InputStream is = FakeTranscription.class.getClassLoader().getResourceAsStream("org/natbraille/core/xsl/dicts/hyph_fr.dic");
        List<HyphenationPattern> hyphenationPatterns = parseHyphens(is);


        String word = "solution";
        int minLettersBeforeBreak = 2;
        int minLettersAfterBreak = 2;
        int levels[] = hyphenateWord(hyphenationPatterns, word, minLettersBeforeBreak, minLettersAfterBreak);


        System.out.println(dbgWord(word, levels));
        String[] words = Files.readString(Path.of("./natbraille-editor-server/test/fr-wordlist.txt")).split("\n");

        System.out.println(words.length + " words");
        {
            String[] words0 = Files.readString(Path.of("./natbraille-editor-server/test/fr-wordlist.txt")).split("\n");
            StringBuilder sb0 = new StringBuilder();
            for (String word0 : words0) {
                int levels0[] = hyphenateWord(hyphenationPatterns, word0, 2, 2);
                sb0.append(dbgWord(word0, levels0));
                sb0.append("\n");
            }
            Files.writeString(Path.of("./natbraille-editor-server/test/r/hyph-resu-array.txt"), sb0.toString());
        }
        {
            String[] wordList = Files.readString(Path.of("./natbraille-editor-server/test/fr-wordlist.txt")).split("\n");
            int nRepeat = 100;
            long beginTime = System.currentTimeMillis();
            for (int i = 0; i < nRepeat; i++) {
                for (String word2 : wordList){
                    hyphenateWord(hyphenationPatterns, word2, 2, 2);
                }
            }
            long endTime = System.currentTimeMillis();
            long totalTime = endTime - beginTime;
            float oneWordTime = ((float) totalTime) / (((float) nRepeat) * ((float) (wordList.length)));
            System.out.println("totalTime " + totalTime);
            System.out.println("oneWordTime " + (1000.0 * oneWordTime) + "ns");
        }
        // oneWordTime 25.591520592570305ns+
    }
}
