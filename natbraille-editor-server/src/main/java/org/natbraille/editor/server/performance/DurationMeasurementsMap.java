package org.natbraille.editor.server.performance;

import java.util.AbstractMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class DurationMeasurementsMap {
    private final Map<String, DurationMeasurements> map = new HashMap<>();

    private DurationMeasurements createOrGetDurationMeasurement(String key) {
        if (!map.containsKey(key)) {
            DurationMeasurements durationMeasurements = new DurationMeasurements();
            map.put(key, durationMeasurements);
            return durationMeasurements;
        } else {
            return map.get(key);
        }
    }
    public static class StartedMapMeasurement {
        private final DurationMeasurements durationMeasurements;
        private final DurationMeasurement durationMeasurement;

        public StartedMapMeasurement(DurationMeasurements durationMeasurements, DurationMeasurement durationMeasurement) {
            this.durationMeasurements = durationMeasurements;
            this.durationMeasurement = durationMeasurement;
        }
    }

    public StartedMapMeasurement startMeasure(String key) {
        DurationMeasurements durationMeasurements = createOrGetDurationMeasurement(key);
        DurationMeasurement durationMeasurement = durationMeasurements.startMeasure();
        return new StartedMapMeasurement(durationMeasurements,durationMeasurement);
    }

    public void endMeasure(StartedMapMeasurement startedMapMeasurement) {
        startedMapMeasurement.durationMeasurements.endMeasure(startedMapMeasurement.durationMeasurement);
    }
    public Map<String, List<double[]>> getMeasurements() {
        Map<String, List<double[]>> o = new HashMap<>();
        for (String key : map.keySet()){
            o.put(key,map.get(key).getMeasurements());
        }
        return o;
    }

}
