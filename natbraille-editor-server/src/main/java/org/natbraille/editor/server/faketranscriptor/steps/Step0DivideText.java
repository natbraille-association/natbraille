package org.natbraille.editor.server.faketranscriptor.steps;

import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.editor.server.faketranscriptor.FakeTranscription;
import org.natbraille.editor.server.faketranscriptor.Tag;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.natbraille.editor.server.faketranscriptor.FakeTranscription.forEachElement;
import static org.natbraille.editor.server.faketranscriptor.FakeTranscription.forEachNode;

public class Step0DivideText {
    public static List<Node> step0_divideText(String text, Document doc) {
        List<Node> nodes = new ArrayList<>();
        String state = "";
        int start = 0;
        Element element = null;
        for (int i = 0; i < text.length(); i++) {
            String c = text.substring(i, i + 1);
            String type;
            if (c.equals(" ")) type = Tag.spacing.tagName;
            else if ((",?;.:!-—'".indexOf(c)) >= 0) type = Tag.punctuation.tagName;
            else if (("0132456789".indexOf(c)) >= 0) type = Tag.number.tagName;
            else type = Tag.word.tagName;
            if (type != state) {
                if (element != null) {
                    element.setTextContent(text.substring(start, i));
                }
                element = doc.createElement(type);
                nodes.add(element);
                start = i;
                state = type;
            }
        }
        if (element != null) {
            element.setTextContent(text.substring(start, text.length()));
        }

        return nodes;
    }



/*
    public class TranslationAttributes {
        public enum Grade {            g0,            g1,            g2        }
        Grade grade;
        boolean strong;
    }

 */

    public static final String BRAILLE_EMPHASIS_ATTRIBUTE = "emphasis";
    public static final String BRAILLE_EMPHASIS_VALUE_STANDARD = "1";

    public static final String NOIR_GRADE_ATTRIBUTE = "data-grade";
    public static final String BRAILLE_GRADE_ATTRIBUTE = "grade";
    public static final String BRAILLE_GRADE_VALUE_G0 = "g0";
    public static final String BRAILLE_GRADE_VALUE_G1 = "g1";
    public static final String BRAILLE_GRADE_VALUE_G2 = "g2";

    public static void putIfAbsent(Map<String, String> map, String key, String value) {
        if (!map.containsKey(key)) {
            map.put(key, value);
        }
    }

    /**
     * Collect attributes given in parent element of a text (strong, data-grade)
     *
     * @param textNode
     * @param topMostParent
     * @throws Exception
     */
    public static Map<String, String> getAttributedTextAttributes(Node textNode, Element topMostParent) throws Exception {
        Map<String, String> attributes = new HashMap<>();
        Node currentNode = textNode;
        Node upperLimit = topMostParent.getParentNode();
        while (currentNode != upperLimit) {
            if (currentNode.getNodeType() == Node.ELEMENT_NODE) {
                Element currentElement = (Element) currentNode;
                if (currentElement.getNodeName().equals("strong")) {
                    putIfAbsent(attributes, BRAILLE_EMPHASIS_ATTRIBUTE, BRAILLE_EMPHASIS_VALUE_STANDARD);
                } else {
                    NamedNodeMap attrs = currentElement.getAttributes();
                    for (int i = 0; i < attrs.getLength(); i++) {
                        Node attributeNode = attrs.item(i);
                        if (attributeNode.getNodeType() == Node.ATTRIBUTE_NODE) {
                            Attr attribute = (Attr) attributeNode;
                            String name = attribute.getName();
                            String value = attribute.getValue();
                            if (name.equals(NOIR_GRADE_ATTRIBUTE)) {
                                if (value.equals(BRAILLE_GRADE_VALUE_G0)) {
                                    putIfAbsent(attributes, BRAILLE_GRADE_ATTRIBUTE, BRAILLE_GRADE_VALUE_G0);
                                }
                                if (value.equals(BRAILLE_GRADE_VALUE_G1)) {
                                    putIfAbsent(attributes, BRAILLE_GRADE_ATTRIBUTE, BRAILLE_GRADE_VALUE_G1);
                                }
                                if (value.equals(BRAILLE_GRADE_VALUE_G2)) {
                                    putIfAbsent(attributes, BRAILLE_GRADE_ATTRIBUTE, BRAILLE_GRADE_VALUE_G2);
                                }
                            }
                        }
                    }
                }
            }
            currentNode = currentNode.getParentNode();
        }
        return attributes;
    }

    public static List<Node> attributedText(Element element) throws Exception {
        Document doc = element.getOwnerDocument();
        List<Node> divided = new ArrayList<>();
        forEachNode(element, ".//text()", textNode -> {
            Map<String, String> attributes = getAttributedTextAttributes(textNode, element);
            List<Node> dividedGroup = step0_divideText(textNode.getTextContent(), doc);
            for (Node n : dividedGroup) {
                if (n.getNodeType() == Node.ELEMENT_NODE) {
                    // TODO : this should be the case always and divideText should return Element ?
                    Element e = (Element) n;
                    for (var attr : attributes.entrySet()) {
                        e.setAttribute(attr.getKey(), attr.getValue());
                    }
                }
            }
            /*
            System.out.println("---");
            System.out.println(textNode);
            System.out.println(attributes);
            System.out.println("---");
             */
            divided.addAll(dividedGroup);
        });
        return divided;
    }

    public static void step0(FakeTranscription ft, /*String inputFilename, String outputFilename,*/ NatFilterOptions natFilterOptions, Document inputDoc) throws ParserConfigurationException, IOException, SAXException, XPathExpressionException, TransformerException, Exception {

        // TODO fix xml / html5 case-sensible/insensible attribute
        //Document inputDoc = ft.parseDOM(inputFilename);

        forEachElement(inputDoc, "//*", block -> {
            String foundId = block.getAttribute("fragmentid");
            if (foundId.length() != 0) {
                block.setAttribute("fragmentId", foundId);
                block.removeAttribute("fragmentid");
            }
        });

        forEachNode(inputDoc, "//p|//h1|//h2|//h3|//h4|//h5|//h6", node -> {
            Node childNode = node.getFirstChild();
            while (childNode != null) {
                Node nextChildNode = childNode.getNextSibling();

                if (childNode.getNodeType() == Node.TEXT_NODE) {
                    String textContent = childNode.getTextContent();
                    for (Node subTextChildNode : step0_divideText(textContent, inputDoc)) {
                        node.insertBefore(subTextChildNode, childNode);
                    }
                    node.removeChild(childNode);
                } else if (childNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element childElement = (Element) childNode;
                    if (childElement.getNodeName().equals("math")) {
                        // noop
                    } else {
                        for (Node sub : attributedText(childElement) ) {
                            node.insertBefore(sub, childNode);
                        }
                        node.removeChild(childNode);
                    }
                }
                childNode = nextChildNode;
            }
        });
    }

}
