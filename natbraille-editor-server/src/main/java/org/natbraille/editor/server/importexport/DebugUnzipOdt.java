package org.natbraille.editor.server.importexport;

import com.googlecode.mp4parser.util.Path;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * extract content.xml file from odt zip (useful for debug)
 */
public class DebugUnzipOdt {
    public static void readContentXml(InputStream inputStream, OutputStream outputStream) throws IOException {
        try (ZipInputStream zis = new ZipInputStream(inputStream)) {
            ZipEntry entry;
            while (((entry = zis.getNextEntry()) != null)) {
                if ((!entry.isDirectory()) && entry.getName().equals("content.xml")) {
                    zis.transferTo(outputStream);
                    zis.closeEntry();
                }
            }
        }
    }
    public static void readStylesXml(InputStream inputStream, OutputStream outputStream) throws IOException {
        try (ZipInputStream zis = new ZipInputStream(inputStream)) {
            ZipEntry entry;
            while (((entry = zis.getNextEntry()) != null)) {
                if ((!entry.isDirectory()) && entry.getName().equals("styles.xml")) {
                    zis.transferTo(outputStream);
                    zis.closeEntry();
                }
            }
        }
    }
}
