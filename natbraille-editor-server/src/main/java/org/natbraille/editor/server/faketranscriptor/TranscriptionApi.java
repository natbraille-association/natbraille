package org.natbraille.editor.server.faketranscriptor;

import org.natbraille.editor.server.formats.AllTranscriptionSteps;
import org.natbraille.editor.server.formats.TranscriptionRequest;

import java.util.Properties;

public interface TranscriptionApi {
    public AllTranscriptionSteps transcribe(TranscriptionRequest transcriptionRequest);
}
