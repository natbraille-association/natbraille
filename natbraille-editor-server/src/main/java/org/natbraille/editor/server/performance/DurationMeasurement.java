package org.natbraille.editor.server.performance;

public class DurationMeasurement {
    long startTimeNs;
    long endTimeNs;
    long durationNs;

    public void start() {
        startTimeNs = System.nanoTime();
    }

    public void end() {
        endTimeNs = System.nanoTime();
        durationNs = endTimeNs - startTimeNs;
    }
    public long getDurationNs() {
        return durationNs;
    }

    public double getDurationSecond() {
        return ((double) (durationNs)) * 1.0e-9;
    }

}
