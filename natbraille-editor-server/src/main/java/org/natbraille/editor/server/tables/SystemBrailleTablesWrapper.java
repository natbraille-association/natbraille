package org.natbraille.editor.server.tables;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.natbraille.brailletable.BrailleTable;
import org.natbraille.brailletable.BrailleTableException;
import org.natbraille.brailletable.system.SystemBrailleTables;

public class SystemBrailleTablesWrapper {
    public static ObjectNode SystemBrailleTablesJsonCache = null;

    public static ObjectNode getSystemBrailleTablesJsonCache() throws BrailleTableException {
        if (SystemBrailleTablesJsonCache == null) {
            SystemBrailleTablesJsonCache = buildSystemBrailleTablesJsonCache();
        }
        return SystemBrailleTablesJsonCache;
    }

    private static ObjectNode buildSystemBrailleTablesJsonCache() throws BrailleTableException {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jsonRoot = mapper.createObjectNode();
        for (String tableName : SystemBrailleTables.getNames()) {
            BrailleTable brailleTable = SystemBrailleTables.forName(tableName);
            ArrayNode jsonTableArray = mapper.createArrayNode();
            for (int i = 0x2800; i <= 0x283f; i++) {
                char unicodeCharacter = (char) i;
                char tableCharacter = brailleTable.getChar(unicodeCharacter);
                jsonTableArray.add(tableCharacter);
            }
            jsonRoot.set(tableName, jsonTableArray);
        }
        return jsonRoot;
    }
}
