package org.natbraille.editor.server.userDocuments;

import org.natbraille.core.UserDocumentLoader;

public interface UserDocuments extends UserDocumentLoader {
    /**
     * Save a document
     * @param username username name
     * @param kind document kind
     * @param name file name
     * @param document document data
     * @return true if saved, false otherwise
     */
    boolean saveDocument(String username, String kind, String name, byte[] document);

    /**
     * Delete a document
     * @param username user name
     * @param kind document kind
     * @param name file name
     */
    boolean deleteDocument(String username, String kind, String name);
    /**
     * Load a document
     * @param user user name
     * @param kind document kind
     * @param name file name
     * @return the document data if succeeded, null otherwise
     */
    // See org.natbraille.core.UserDocumentLoader
    // public byte[] loadDocument(String user, String kind, String name);
    /**
     * List username documents of a given kind
     * @param username username name
     * @param kind document kind
     * @return the document filenames if succeeded, the empty list otherwise
     */
    String[] listDocuments(String username, String kind);

    /**
     * Load a zip archive of all user documents, with the following structure
     * /{user}/{kind}/name
     * @param user user name
     * @return the zip data if succeeded, null otherwise
     */
    byte[] loadArchive(String user);

    void deleteUserDocuments(String username);
}
