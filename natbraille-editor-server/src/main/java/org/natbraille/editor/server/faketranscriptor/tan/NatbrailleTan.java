package org.natbraille.editor.server.faketranscriptor.tan;

import org.natbraille.brailletable.BrailleTable;
import org.natbraille.brailletable.BrailleTableException;
import org.natbraille.brailletable.BrailleTables;
import org.natbraille.brailletable.Converter;
import org.natbraille.brailletable.system.SystemBrailleTables;
import org.natbraille.core.document.NatDocument;
import org.natbraille.core.filter.NatFilterChain;
import org.natbraille.core.filter.NatFilterConfigurator;
import org.natbraille.core.filter.convertisseur.ConvertisseurTan;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.core.filter.transcodeur.TranscodeurNormal;
import org.natbraille.core.gestionnaires.GestionnaireErreur;
import org.natbraille.editor.server.faketranscriptor.FakeTranscription;
import org.natbraille.editor.server.importexport.DebugUnzipOdt;
import org.natbraille.editor.server.importexport.NoirToOpenDocument;
import org.natbraille.editor.server.importexport.StylistWrapper;
import org.w3c.dom.Document;

import java.nio.file.Files;
import java.nio.file.Path;

public class NatbrailleTan {

    public static String QuirkyXHtmlToHtml(String xhtmlString) {
        int bodyStart = xhtmlString.indexOf("<body");
        return "<html>\n"
                + xhtmlString.substring(bodyStart)
                .replaceAll("<m:math", "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"")
                .replaceAll("<m:", "<")
                .replaceAll("</m:", "</");
    }

    private static final BrailleTable unicodeBrailleTable;

    static {
        try {
            unicodeBrailleTable = BrailleTables.forName(SystemBrailleTables.BrailleUTF8);
        } catch (BrailleTableException e) {
            throw new RuntimeException(e);
        }
    }

    public static String toUnicodeBraille(BrailleTable inputBrailleTable, String text) throws Exception {
        Converter brailleTableConverter = new Converter(inputBrailleTable, unicodeBrailleTable);
        brailleTableConverter.getConfig().setCopyMissingSource(true);
        brailleTableConverter.getConfig().setCopyMissingDest(true);
        brailleTableConverter.getConfig().setSpaceIsEmptyBrailleCell(false);
        // replace all 0x2800 braille empty cell by x020 space
        return brailleTableConverter.convert(text).replaceAll("\u2800", " ");
    }

    public static String natbrailleBrailleTextToBlackHtml(String brailleString, NatFilterOptions nfo) throws Exception {

        BrailleTable inputBrailleTable = BrailleTables.forName(nfo.getValue(NatFilterOption.FORMAT_OUTPUT_BRAILLE_TABLE));
        String unicodeBrailleString = toUnicodeBraille(inputBrailleTable, brailleString);

        GestionnaireErreur ge = new GestionnaireErreur();
        // do not add any afficheur for gestionnaire erreur.

        // ensures detrans mode (useful ?)
        nfo.setOption(NatFilterOption.MODE_DETRANS, "true");
        nfo.setOption(NatFilterOption.FORMAT_OUTPUT_BRAILLE_TABLE,SystemBrailleTables.BrailleUTF8);

        // NatFilterOption used by TranscodeurNormal:
        // nfo.setOption(NatFilterOption.MODE_G2,"true");
        // nfo.setOption(NatFilterOption.MODE_MATH, "true");

        // NatFilterOption used by ConvertisseurTan:
        // NatFilterOption.MODE_DETRANS_KEEP_HYPHEN
        // NatFilterOption.LAYOUT_PAGE_BREAKS_KEEP_ORIGINAL
        // NatFilterOption.MODE_DETRANS_MATH_USE_BRAILLE_SEMANTICS_ANNOTATION
        // NatFilterOption.MODE_DETRANS_KEEP_SOURCE_OFFSETS

        NatFilterConfigurator configurator = new NatFilterConfigurator(nfo, ge);
        NatFilterChain trans = new NatFilterChain(configurator);
        NatDocument natDocument = new NatDocument();
        natDocument.setString(unicodeBrailleString);
        trans.addNewFilter(ConvertisseurTan.class);
        trans.addNewFilter(TranscodeurNormal.class);
        return QuirkyXHtmlToHtml(trans.run(natDocument).getString());

    }

    public static byte[] natbrailleBrailleTextToBlackOpenDocument(String brailleString, NatFilterOptions nfo) throws Exception {

        Document stylistDocument = StylistWrapper.buildStylistDocumentFromNatFilterOptions(nfo);
        String chemistryStyleName = StylistWrapper.getChemistryStyleName(stylistDocument);

        return NoirToOpenDocument.toOpenDocument(natbrailleBrailleTextToBlackHtml(brailleString, nfo),chemistryStyleName);
    }


    public static void main(String[] args) throws Exception {

        //AfficheurAnsiConsole afficheurConsole = new AfficheurAnsiConsole(LogLevel.ULTRA);
        //afficheurConsole.setLogLevel(LogLevel.NONE);
        //afficheurConsole.setLogLevel(LogLevel.DEBUG);
        //

        GestionnaireErreur ge = new GestionnaireErreur();

        NatFilterOptions nfo = new NatFilterOptions();
        nfo.setOption(NatFilterOption.MODE_DETRANS, "true");
        nfo.setOption(NatFilterOption.MODE_G2, "true");
        nfo.setOption(NatFilterOption.MODE_MATH, "true");
        nfo.setOption(NatFilterOption.MODE_DETRANS_MATH_USE_BRAILLE_SEMANTICS_ANNOTATION, "true");
        nfo.setOption(NatFilterOption.MODE_DETRANS_KEEP_SOURCE_OFFSETS, "true");

        //      String brailleString = "⠨⠇ ⠉⠁⠍⠼\n⠨⠏⠰⠞⠑\n⠨⠷⠌⠎⠑\n⠠⠄⠘⠁⠶⠩\n⠠⠄⠁⠌⠃⠶⠜⠰⠦⠉⠴⠆\n";
//        String brailleString = Files.readString(Path.of("/home/vivien/Bureau/tan/braille_src.txt"));
        String brailleString = "⠠⠄⠘⠁⠶⠩";
        System.out.println(brailleString);
        String htmlString = natbrailleBrailleTextToBlackHtml(brailleString, nfo);

        System.out.println(htmlString);

        Path debugDir = Path.of("/home/vivien/Bureau/tan/");
        Path destinationHtml = debugDir.resolve("detrans.html");
        Path destinationOdt = debugDir.resolve("detrans.odt");

        Files.writeString(destinationHtml, natbrailleBrailleTextToBlackHtml(brailleString, nfo));
        Files.write(destinationOdt, natbrailleBrailleTextToBlackOpenDocument(brailleString, nfo));

        Path destinationOdtContentXml = debugDir.resolve("detrans.odt.content.xml");

        DebugUnzipOdt.readContentXml(Files.newInputStream(destinationOdt), Files.newOutputStream(destinationOdtContentXml));

    }

}