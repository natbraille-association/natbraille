package org.natbraille.editor.server.users;

import org.natbraille.editor.server.DatabaseManager;
import org.natbraille.editor.server.Server;
import org.natbraille.editor.server.userDocuments.UserDocumentsDB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;

import java.sql.*;
import java.time.OffsetDateTime;
import java.util.Optional;

public class UsersDB implements Users {
    private static Logger LOGGER = LoggerFactory.getLogger(UsersDB.class);
    /**
     * https://cheatsheetseries.owasp.org/cheatsheets/Password_Storage_Cheat_Sheet.html#password-hashing-algorithms
     * Password Hashing Algorithms / Argon2id
     */
    private final Argon2PasswordEncoder arg2SpringSecurity;
    private final DatabaseManager databaseManager;

    public UsersDB() throws SQLException {
        // String connectionString = Server.serverConfiguration.dbConnectionString;
        // conn = DriverManager.getConnection(connectionString);
        this.arg2SpringSecurity = new Argon2PasswordEncoder(16, 32, 1, 12288, 10);
        this.databaseManager = DatabaseManager.getInstance();
    }

    /*
        @Override
        public boolean addUser(String username, String rawPassword) {
            String encodedPassword = arg2SpringSecurity.encode(rawPassword);
            try (PreparedStatement ps = conn.prepareStatement(
                    "INSERT INTO users (username,encodedPassword) VALUES (?, ?) "
            )) {
                ps.setString(1, username);
                ps.setString(2, encodedPassword);
                ps.executeUpdate();
                return true;
            } catch (SQLException e) {
                LOGGER.error("cannot add user {}", username, e);
                return false;
            }
        }
    */
    @Override
    public boolean removeUser(String username) {
        String[] sqlStatements = {
                "DELETE FROM users WHERE username = ?",
                "DELETE FROM user_registration WHERE username = ?",
                "DELETE FROM tokens WHERE username = ?",
                "DELETE FROM user_password_reset_request WHERE username = ?",
                "DELETE FROM user_account_removal_request WHERE username = ?"
        };

        try (Connection connection = databaseManager.getConnection()) {
            for (String sql : sqlStatements) {
                try (PreparedStatement ps = connection.prepareStatement(sql)) {
                    ps.setString(1, username);
                    ps.executeUpdate();
                }
            }
            return true;
        } catch (SQLException e) {
            LOGGER.error("Cannot remove some of user {}", username, e);
            return false;
        }
    }


    @Override
    public boolean userPasswordMatch(String username, String rawPassword) {
        try (Connection connection = databaseManager.getConnection(); PreparedStatement ps = connection.prepareStatement("SELECT encodedPassword FROM users WHERE username=?")) {
            ps.setMaxRows(1);
            ps.setString(1, username);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    String encodedPassword = rs.getString("encodedPassword");
                    return arg2SpringSecurity.matches(rawPassword, encodedPassword);
                }
            } catch (SQLException e) {
                LOGGER.error("cannot load document", e);
            }
        } catch (SQLException e) {
            LOGGER.error("cannot load document", e);
        }
        return false;
    }

    @Override
    public boolean addUserRegistration(String username, String rawPassword, String email) {
        String encodedPassword = arg2SpringSecurity.encode(rawPassword);
        try (Connection connection = databaseManager.getConnection(); PreparedStatement ps = connection.prepareStatement(
                "INSERT INTO user_registration (username,encodedPassword,email,created) VALUES (?, ?, ?, ?)"
        )) {
            ps.setString(1, username);
            ps.setString(2, encodedPassword);
            ps.setString(3, email);
            OffsetDateTime now = OffsetDateTime.now();
            ps.setObject(4, now);
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            LOGGER.error("cannot add user registration {} {}", username, email, e);
            return false;
        }
    }

    @Override
    public boolean setUserRegistrationChallenge(String username, String rawChallenge) {
        String encodedChallenge = arg2SpringSecurity.encode(rawChallenge);
        try (Connection connection = databaseManager.getConnection(); PreparedStatement ps = connection.prepareStatement(
                "UPDATE user_registration SET (encodedChallenge,sent) = (?,?) WHERE username = ?"
        )) {
            ps.setString(1, encodedChallenge);
            ps.setObject(2, OffsetDateTime.now());
            ps.setObject(3, username);
            int modifiedCount = ps.executeUpdate();
            return modifiedCount == 1;
        } catch (SQLException e) {
            LOGGER.error("cannot set user registration challenge {}", username);
            return false;
        }
    }

    @Override
    public boolean removeRegistration(String username) {
        try (Connection connection = databaseManager.getConnection(); PreparedStatement ps = connection.prepareStatement(
                "DELETE FROM user_registration WHERE username = ?"
        )) {
            ps.setString(1, username);
            int deletedRowsCount = ps.executeUpdate();
            return (deletedRowsCount == 1);
        } catch (SQLException e) {
            LOGGER.error("cannot remove user registration {}", username, e);
            return false;
        }

    }

    public boolean setAccountRemovalRequestChallenge(String username, String rawChallenge){
        String insertOrUpdateRequest = """                 
                INSERT INTO user_account_removal_request (username, encodedChallenge, sent) VALUES (?, ?, now())
                ON CONFLICT (username) DO UPDATE SET encodedChallenge = EXCLUDED.encodedChallenge, sent = now();
                """;
        String encodedChallenge = arg2SpringSecurity.encode(rawChallenge);
        try (Connection connection = databaseManager.getConnection(); PreparedStatement ps = connection.prepareStatement(
              insertOrUpdateRequest
        )) {
            ps.setObject(1, username);
            ps.setString(2, encodedChallenge);
            int modifiedCount = ps.executeUpdate();
            return modifiedCount == 1;
        } catch (SQLException e) {
            LOGGER.error("cannot set user removal challenge {}", username);
            return false;
        }
    }

    @Override
    public boolean addRegisteredUser(String username) {
        try (Connection connection = databaseManager.getConnection(); PreparedStatement ps = connection.prepareStatement(
                "INSERT INTO users (username,encodedpassword,email) SELECT username,encodedPassword,email from user_registration where username=?"
        )) {
            ps.setString(1, username);
            int insertedRowsCount = ps.executeUpdate();
            return (insertedRowsCount == 1);
        } catch (SQLException e) {
            LOGGER.error("cannot add registered user {}", username, e);
            return false;
        }
    }

    @Override
    public boolean registrationChallengeMatch(String username, String rawChallenge) {
        try (Connection connection = databaseManager.getConnection(); PreparedStatement ps = connection.prepareStatement("SELECT encodedChallenge FROM user_registration WHERE username=?")) {
            ps.setMaxRows(1);
            ps.setString(1, username);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    String encodedChallenge = rs.getString("encodedChallenge");
                    return arg2SpringSecurity.matches(rawChallenge, encodedChallenge);
                }
            } catch (SQLException e) {
                LOGGER.error("cannot check challenge", e);
            }
        } catch (SQLException e) {
            LOGGER.error("cannot check challenge", e);
        }
        return false;
    }

    /// ////////////////////////////
    /// //////////////////////////////
    @Override
    public boolean setUserPasswordResetRequestChallenge(String username, String rawChallenge) {
        String insertOrUpdateRequest = """                 
                INSERT INTO user_password_reset_request (username, encodedChallenge, sent) VALUES (?, ?, now())
                ON CONFLICT (username) DO UPDATE SET encodedChallenge = EXCLUDED.encodedChallenge, sent = now();
                """;
        String encodedChallenge = arg2SpringSecurity.encode(rawChallenge);
        try (Connection connection = databaseManager.getConnection(); PreparedStatement ps = connection.prepareStatement(insertOrUpdateRequest)) {
            ps.setString(1, username);
            ps.setString(2, encodedChallenge);
            int insertedRowsCount = ps.executeUpdate();
            return (insertedRowsCount == 1);
        } catch (Exception e) {
            LOGGER.error("cannot set password reset challenge for {}", username);
            return false;
        }
    }

    public boolean resetUserPassword(String username, String rawChallenge, String rawPassword) {
        String selectQuery = "SELECT encodedChallenge FROM user_password_reset_request WHERE username = ?";
        String deleteQuery = "DELETE FROM user_password_reset_request WHERE username = ?";
        String updateQuery = "UPDATE users SET encodedPassword = ? WHERE username = ?";

        try (Connection connection = databaseManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(selectQuery);
             PreparedStatement deleteStmt = connection.prepareStatement(deleteQuery);
             PreparedStatement updateStmt = connection.prepareStatement(updateQuery)) {

            // Step 1: Fetch encodedChallenge
            selectStmt.setString(1, username);
            ResultSet rs = selectStmt.executeQuery();
            if (!rs.next()) {
                return false; // No reset request found
            }
            String encodedChallenge = rs.getString("encodedChallenge");

            // Step 2: Verify the challenge
            if (!arg2SpringSecurity.matches(rawChallenge, encodedChallenge)) {
                return false; // Challenge does not match
            }

            // Step 3: Update password and delete reset request
            connection.setAutoCommit(false); // Start transaction

            deleteStmt.setString(1, username);
            deleteStmt.executeUpdate();

            String encodedPassword = arg2SpringSecurity.encode(rawPassword);
            updateStmt.setString(1, encodedPassword);
            updateStmt.setString(2, username);
            updateStmt.executeUpdate();

            connection.commit(); // Commit transaction
            return true;
        } catch (SQLException e) {
            LOGGER.error("Could not reset password for user : {}", username);
            return false;
        }
    }

    /// ///////////////////
    /// ////////////////////

    @Override
    public Optional<String> getEmailForUsername(String username) {
        String email = null;
        try (Connection connection = databaseManager.getConnection();
             PreparedStatement ps = connection.prepareStatement("SELECT email FROM users WHERE username=?")) {
            ps.setString(1, username);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    email = rs.getString("email");
                }
            }
        } catch (SQLException e) {
            LOGGER.error("cannot get email for user {}", username);
        }
        return Optional.ofNullable(email);
    }

    @Override
    public boolean accountRemovalChallengeMatch(String username, String rawChallenge) {
        try (Connection connection = databaseManager.getConnection(); PreparedStatement ps = connection.prepareStatement("SELECT encodedChallenge FROM user_account_removal_request WHERE username=?")) {
            ps.setMaxRows(1);
            ps.setString(1, username);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    String encodedChallenge = rs.getString("encodedChallenge");
                    return arg2SpringSecurity.matches(rawChallenge, encodedChallenge);
                }
            } catch (SQLException e) {
                LOGGER.error("cannot check challenge", e);
            }
        } catch (SQLException e) {
            LOGGER.error("cannot check challenge", e);
        }
        return false;
    }

    public static class Test {
        public static void main(String[] args) {
            UsersDB usersDB = null;
            try {
                usersDB = new UsersDB();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }

//        boolean added0 = usersDB.addUser("Claudine", "Tiercelin");
//        System.out.println("added true ? " + added0);
//
//        boolean added1 = usersDB.addUser("Claudine", "Tiercelin");
//        System.out.println("added twice false ? " + added1);

            boolean doesCheck0 = usersDB.userPasswordMatch("Claudine", "Tiercelin");
            System.out.println("does match correct password true ? " + doesCheck0);

            boolean doesCheck1 = usersDB.userPasswordMatch("Claudine", "Pierce");
            System.out.println("does match incorrect password  false ? " + doesCheck1);

            boolean removed0 = usersDB.removeUser("Claudine");
            System.out.println("removed true ? " + removed0);

            boolean removed1 = usersDB.removeUser("Claudine");
            System.out.println("removed twice false ? " + removed1);

            boolean addedRegistration0 = usersDB.addUserRegistration("Charles", "Sanders123", "peirce@peirce.com");
            System.out.println("added challenge true ? " + addedRegistration0);

            boolean setChallenge0 = usersDB.setUserRegistrationChallenge("Charles", "HowtoMakeOurIdeasClear");
            System.out.println("set challenge true ? " + setChallenge0);

            boolean doesChallengeMatch0 = usersDB.registrationChallengeMatch("Charles", "HowtoMakeOurIdeasClear");
            System.out.println("does match correct challenge true ? " + doesChallengeMatch0);

            boolean doesChallengeMatch1 = usersDB.registrationChallengeMatch("Charles", "HowtoNotMakeOurIdeasClear");
            System.out.println("does match incorrect challenge false ? " + doesChallengeMatch1);

            boolean removedChallenge0 = usersDB.removeRegistration("Charles");
            System.out.println("removed challenge true ? " + removedChallenge0);

            boolean removedChallenge1 = usersDB.removeRegistration("Charles");
            System.out.println("removed challenge twice false ? " + removedChallenge1);

        }

    }
}
