package org.natbraille.editor.server.unoconvertwrapper;

import org.natbraille.editor.server.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

import static org.natbraille.editor.server.unoconvertwrapper.Common.convertTo;

/**
 * Unoserver conversion using the new (2024) unoserver.
 *
 * <p>Conversion from StarMath5 idiom used by LibreOffice to MathML + StarMath annotation
 *
 * <p>Conversion from * document (esp. word) format to odt
 * <p>
 * This requires an installation of LibreOffice + installation of unoserver
 * <p>
 * unoserver repository + documentation : https://github.com/unoconv/unoserver
 */

public class UnoConvert {

    private static final Logger LOGGER = LoggerFactory.getLogger(UnoConvert.class);

    public static byte[] starmathToMathML(String starMath5) {
        LOGGER.debug("Conversion with starmathToMathML : {}", starMath5);
        String sourceXmlString = Common.mmlSourceDocument(starMath5);
        return mathMLToMathML(sourceXmlString);
    }

    public static byte[] mathMLToMathML(String sourceXmlString) {
        return synchronizedDefaultConvertTo(sourceXmlString.getBytes(), Common.UnoDestinationFormats.MML);
    }

    public static byte[] anyToOdt(byte[] msWord) {
        return synchronizedDefaultConvertTo(msWord, Common.UnoDestinationFormats.ODT);
    }


    // private static final String unoconvertExecutablePath = "/usr/local/bin/unoconvert";

    synchronized public static byte[] synchronizedDefaultConvertTo(byte[] source, Common.UnoDestinationFormats destinationFormat) {
        return convertTo(source,destinationFormat,null);
    }

    public static boolean isUnoStarMathToMathMLOk() {
        String starmath5 = "%alpha <> %beta";
        byte[] xmlBytes = null;
        try {
            xmlBytes = starmathToMathML(starmath5);
        } catch (Exception e) {
            LOGGER.error(e.toString());
        }
        if ((xmlBytes != null)) {
            String s = new String(xmlBytes);
            return (s.contains("α") && s.contains("≠") && s.contains("β"));
        }
        return false;
    }

    public static void main(String[] args) throws IOException {
        System.out.println("isUnoStarMathToMathMLOk:" + isUnoStarMathToMathMLOk());
        /*
        String starmath5 = "5 + sqrt { %alpha over %beta } <> 25";
        byte[] xmlBytes = starmathToMathML(starmath5);
        System.out.println(new String(xmlBytes));

         */
    }
}
