package org.natbraille.editor.server.faketranscriptor;

import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.editor.server.formats.AllTranscriptionSteps;
import org.natbraille.editor.server.formats.TranscriptionRequest;

import java.util.Properties;

public class FakeApiTranscriptor implements TranscriptionApi {

    @Override
    public AllTranscriptionSteps transcribe(TranscriptionRequest transcriptionRequest) {
        try {
            NatFilterOptions filterOptions = new NatFilterOptions(transcriptionRequest.filterOptions);
            String xmlString = transcriptionRequest.xmlString;
            return FakeTranscription.translate(xmlString,filterOptions);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
