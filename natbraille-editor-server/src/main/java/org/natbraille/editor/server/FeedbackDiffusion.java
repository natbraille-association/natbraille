package org.natbraille.editor.server;

import jakarta.mail.Message;
import jakarta.mail.MessagingException;
import jakarta.mail.Session;
import jakarta.mail.Transport;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;

public class FeedbackDiffusion {
    private static final Logger LOGGER = LoggerFactory.getLogger(FeedbackDiffusion.class);
    private final Session session;

    public FeedbackDiffusion() {
        HashMap<String, String> smtpConfiguration = Server.serverConfiguration.smtp;
        Properties props = new Properties();
        if (smtpConfiguration != null) {
            for (String propName : smtpConfiguration.keySet()) {
                props.put(propName, smtpConfiguration.get(propName));
            }
        }
        session = Session.getInstance(props, null);
    }

    public void sendFeedback(String jsonString) throws MessagingException, UnsupportedEncodingException {

        String feedBackRecipientEmail = Server.serverConfiguration.feedbackRecipientEmail;
        String fromAddress = Server.serverConfiguration.email.fromAddress;
        String fromName = Server.serverConfiguration.email.fromName;
        String replyTo = Server.serverConfiguration.email.replyTo;

        MimeMessage msg = new MimeMessage(session);
        //set message headers
        msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
        msg.addHeader("format", "flowed");
        msg.addHeader("Content-Transfer-Encoding", "8bit");

        msg.setFrom(new InternetAddress(fromAddress, fromName));
        msg.setReplyTo(InternetAddress.parse(replyTo, false));

        msg.setSubject("feedback", "UTF-8");
        msg.setText(jsonString, "UTF-8");

        msg.setSentDate(new Date());
        msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(feedBackRecipientEmail, false));
        Transport.send(msg);
        LOGGER.debug("feedback email Sent to {}", feedBackRecipientEmail);
    }
}
