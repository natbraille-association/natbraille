package org.natbraille.editor.server.userDocuments;

import org.natbraille.editor.server.DatabaseManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class UserDocumentsDB implements UserDocuments {
    private static Logger LOGGER = LoggerFactory.getLogger(UserDocumentsDB.class);
    private final DatabaseManager databaseManager;

    public UserDocumentsDB() throws SQLException {
        this.databaseManager = DatabaseManager.getInstance();
    }

    public boolean saveDocument(String username, String kind, String name, byte[] document) {
        try (Connection connection = databaseManager.getConnection(); PreparedStatement ps = connection.prepareStatement(
                "INSERT INTO documents (username,kind,name,document,lastModified) VALUES (?, ?, ?, ?,NOW()) "
                        + " ON CONFLICT (username,kind,name) DO UPDATE SET document = EXCLUDED.document, lastModified = NOW()"
        )) {
            ps.setString(1, username);
            ps.setString(2, kind);
            ps.setString(3, name);
            ps.setBytes(4, document);
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            LOGGER.error("cannot save document", e);
            return false;
        }
    }

    public byte[] loadDocument(String username, String kind, String name) {
        try (Connection connection = databaseManager.getConnection(); PreparedStatement ps = connection.prepareStatement("SELECT document FROM documents WHERE username=? AND kind=? AND name=?")) {
            ps.setString(1, username);
            ps.setString(2, kind);
            ps.setString(3, name);
            try (ResultSet rs = ps.executeQuery()) {
                byte[] document = null;
                if (rs.next()) {
                    document = rs.getBytes(1);
                }
                return document;
            } catch (SQLException e) {
                LOGGER.error("cannot load document", e);
            }
        } catch (SQLException e) {
            LOGGER.error("cannot load document", e);
        }
        return null;
    }

    public boolean deleteDocument(String username, String kind, String name) {
        try (Connection connection = databaseManager.getConnection(); PreparedStatement ps = connection.prepareStatement("DELETE FROM documents WHERE username=? AND kind=? AND name=?")) {
            ps.setString(1, username);
            ps.setString(2, kind);
            ps.setString(3, name);
            try (ResultSet rs = ps.executeQuery()) {
                return true;
            } catch (SQLException e) {
                LOGGER.error("cannot delete document", e);
            }
        } catch (SQLException e) {
            LOGGER.error("cannot delete document", e);
        }
        return false;
    }

    public String[] listDocuments(String username, String kind) {
        try (Connection connection = databaseManager.getConnection(); PreparedStatement ps = connection.prepareStatement("SELECT name FROM documents WHERE username=? AND kind=? ORDER BY lastModified DESC")) {
            ps.setString(1, username);
            ps.setString(2, kind);
            try (ResultSet rs = ps.executeQuery()) {
                List<String> list = new ArrayList<>();
                while (rs.next()) {
                    list.add(rs.getString("name"));
                }
                return list.toArray(new String[0]);
            } catch (SQLException e) {
                LOGGER.error("cannot list documents", e);
            }
        } catch (SQLException e) {
            LOGGER.error("cannot list documents", e);
        }
        return new String[]{};
    }

    public byte[] loadArchive(String username) {
        try (ByteArrayOutputStream fos = new ByteArrayOutputStream()) {
            try (ZipOutputStream zipOut = new ZipOutputStream(fos)) {
                try (Connection connection = databaseManager.getConnection(); PreparedStatement ps = connection.prepareStatement("SELECT username, kind, name, document FROM documents WHERE username=? ORDER BY kind")) {
                    ps.setString(1, username);
                    try (ResultSet rs = ps.executeQuery()) {
                        String previousKind = "";
                        while (rs.next()) {
                            String kind = rs.getString("kind");
                            String name = rs.getString("name");
                            byte[] document = rs.getBytes("document");
                            String path = username + "/" + kind + "/";
                            if (!previousKind.equals(kind)) {
                                zipOut.putNextEntry(new ZipEntry(path));
                                zipOut.closeEntry();
                                previousKind = kind;
                            }
                            ZipEntry e = new ZipEntry(path + name);
                            zipOut.putNextEntry(e);
                            zipOut.write(document);
                            zipOut.closeEntry();
                        }
                    } catch (SQLException e) {
                        LOGGER.error("cannot load archive", e);
                    }
                } catch (SQLException e) {
                    LOGGER.error("cannot load archive", e);
                }
            } catch (IOException e) {
                LOGGER.error("cannot load archive", e);
            }
            return fos.toByteArray();
        } catch (IOException e) {
            LOGGER.error("cannot load archive", e);
        }
        return null;
    }

    @Override
    public void deleteUserDocuments(String username) {
        try (Connection connection = databaseManager.getConnection(); PreparedStatement ps = connection.prepareStatement("DELETE FROM documents WHERE username = ?")) {
            ps.setString(1, username);
            ps.executeQuery();
        } catch (SQLException e){
            LOGGER.error("cannot delete user", e);
        }
    }

    private class Test {
        public static void main(String[] args) {
            try {
                test();
            } catch (SQLException e) {
                LOGGER.error("error opening DB", e);
            }
        }

        public static void test() throws SQLException {
            UserDocumentsDB userDocumentsDB = new UserDocumentsDB();
            byte[] byteString;
            try {
                byteString = "MES 6000 BYTES WORDLIST".getBytes("UTF-8");
            } catch (UnsupportedEncodingException e) {
                throw new RuntimeException(e);
            }
            userDocumentsDB.saveDocument("joe", "ivb-wordlist", "wl-666.txt", byteString);
            // String[] list = userDocumentDB.listDocuments("joe", "fr-g2-rules");
            String[] list = userDocumentsDB.listDocuments("joe", "ivb-wordlist");
            for (int i = 0; i < list.length; i++) {
                System.out.println("found " + i + " " + list[i]);
            }
            byte[] loadedDocument = userDocumentsDB.loadDocument("joe", "ivb-wordlist", "wl-666.txt");
            System.out.println(new String(loadedDocument, StandardCharsets.UTF_8));
            byte[] archive = userDocumentsDB.loadArchive("joe");
            try {
                Files.write(Path.of("zzzzzzzzz.zip"), archive);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
