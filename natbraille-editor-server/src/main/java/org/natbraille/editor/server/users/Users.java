package org.natbraille.editor.server.users;

import java.util.Optional;

public interface Users {
    //boolean addUser(String username, String plainTextPassword);

    boolean removeUser(String username);

    boolean userPasswordMatch(String username, String plainTextPassword);

    boolean addUserRegistration(String username, String rawPassword, String email);

    boolean setUserRegistrationChallenge(String username, String rawChallenge);

    boolean removeRegistration(String username);

    boolean addRegisteredUser(String username);

    boolean registrationChallengeMatch(String username, String rawChallenge);

    Optional<String> getEmailForUsername(String username);

    boolean setUserPasswordResetRequestChallenge(String username, String rawChallenge);

    boolean resetUserPassword(String username, String rawChallenge, String rawPassword);

    boolean setAccountRemovalRequestChallenge(String username, String rawChallenge);

    boolean accountRemovalChallengeMatch(String username, String rawChallenge);
}
