package org.natbraille.editor.server.faketranscriptor.g2.OneTime;

import com.opencsv.exceptions.CsvValidationException;
import org.natbraille.core.document.NatDocument;
import org.natbraille.core.document.NatDocumentException;
import org.natbraille.core.filter.NatFilterChain;
import org.natbraille.core.filter.NatFilterConfigurator;
import org.natbraille.core.filter.NatFilterException;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.core.filter.transcodeur.TranscodeurNormal;
import org.natbraille.core.gestionnaires.AfficheurAnsiConsole;
import org.natbraille.core.gestionnaires.GestionnaireErreur;
import org.natbraille.core.gestionnaires.LogLevel;
import org.natbraille.editor.server.faketranscriptor.g2.G2AdHocDoc;
import org.natbraille.editor.server.faketranscriptor.g2.Morphalou;
import org.odftoolkit.odfdom.doc.OdfTextDocument;
import org.odftoolkit.odfdom.incubator.doc.text.OdfTextParagraph;

import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.util.*;

/*

960768:;;;;;;;;;"R &amp; D";1036442;singular;-;-;-;-;;lefff

 */
/*

Caused by: net.sf.saxon.trans.XPathException: Regex backtracking limit exceeded processing ^((?:((((?:ftp|http|https))://)?([....]+[....])+[....]{2,2147483647})|(([....]+(([....]?[....]+)+)?)@(([....]+[....])+[....]{2,4}))))$\Z. Simplify the regular expression, or set Feature.REGEX_BACKTRACKING_LIMIT to -1 to remove this limit.

 */
public class G2Cache {



    public static void main(String[] args) throws IOException, NatFilterException, NatDocumentException, TransformerException, CsvValidationException {
        // 45 min 47 s
        //  54m 15s > 22
        //  56m 15s > 23
        // > 24 -> err
        final String path = "/home/vivien/pr-src/doc-g2/morphalou/morphalou/5/Morphalou3.1_formatCSV_toutEnUn/Morphalou3.1_CSV.csv";
        List<Morphalou.Word> words = Morphalou.parseCsv(path);


        System.setProperty(
                "javax.xml.parsers.DocumentBuilderFactory",
                "org.apache.xerces.jaxp.DocumentBuilderFactoryImpl"
        );
        System.setProperty(
                "javax.xml.xpath.XPathFactory:http://java.sun.com/jaxp/xpath/dom",
                "com.sun.org.apache.xpath.internal.jaxp.XPathFactoryImpl"
        );
        GestionnaireErreur ge = new GestionnaireErreur();

        AfficheurAnsiConsole afficheurConsole = new AfficheurAnsiConsole(LogLevel.ULTRA);

        afficheurConsole.setLogLevel(LogLevel.NONE);
        //      afficheurConsole.setLogLevel(LogLevel.DEBUG);
//        ge.addAfficheur(afficheurConsole);


        String xsl_g2_dict = "nat://system/xsl/dicts/fr-g2.xml";

        NatFilterOptions nfo = new NatFilterOptions();
        //nfo.setOption(NatFilterOption.debug_dyn_res_show,"true");
        nfo.setOption(NatFilterOption.MODE_G2, "true");
        nfo.setOption(NatFilterOption.XSL_G2_DICT, xsl_g2_dict);
        nfo.setOption(NatFilterOption.debug_xsl_processing, "false");
        //    nfo.setOption(NatFilterOption.debug_xsl_processing, "true");


        NatFilterConfigurator configurator = new NatFilterConfigurator(nfo, ge);//, adhocUserDocumentLoader);
        NatFilterChain trans = new NatFilterChain(configurator);

        //trans.addNewFilter(ConvertisseurTexte.class);
        //trans.addNewFilter(FragmentIdentifier.class);
        trans.addNewFilter(TranscodeurNormal.class);

        String what = "each";
        if (what.equals("all")) {
            NatDocument inDoc = new NatDocument();
            //inDoc.setString(docDocFromWord("c'est-à-dire"));
            //inDoc.setString(docDocFromWords(new String[]{"c'est-à-dire", "maintenant", "neige"}));

            //inDoc.setString(docDocFromMorphalou(words.subList(12000, 13000)));
            inDoc.setString(G2AdHocDoc.docDocFromMorphalou(words));
            NatDocument outDoc = trans.run(inDoc);
            String outString = outDoc.getString();
            System.out.println(outString);
        } else if (what.equals("each")) {


            for (Morphalou.Word word : words/*.subList(0, 1)*/) {
                try {
                    //System.out.println("flexion:" + word.flexion.id + " ------------------------ [" + word.getGraphie() + "]");


/*                    if (word.getGraphie().contains("&")) {
                        System.out.println("wrong word:" + word.getGraphie());
                        continue;
                    }*/
                    String docString = G2AdHocDoc.docDocFromWord(word.getGraphie());
                    // System.out.println(docString);

                    NatDocument inDoc = new NatDocument();
                    inDoc.setString(docString);
                    NatDocument outDoc = trans.run(inDoc);
                    String outString = outDoc.getString();
                    //System.out.println("<!>" + word.getGraphie() + "<!>" + outString);
                } catch (Exception e) {
                    System.out.println("err flexion:" + word.flexion.id + " ------------------------ [" + word.getGraphie() + "]" + " " + word.getGraphie().length());
                }
            }
        }


    }

    private static void buildBigOdt(List<Morphalou.Word> words) throws Exception {
        OdfTextDocument odt = OdfTextDocument.newTextDocument();
        for (Morphalou.Word word : words) {
            OdfTextParagraph p = odt.newParagraph();
            odt.addText(word.getGraphie());
        }
        odt.save("all-words.odt");

    }

    public static void main2(String[] args) throws Exception {
        final String path = "/home/vivien/pr-src/doc-g2/morphalou/morphalou/5/Morphalou3.1_formatCSV_toutEnUn/Morphalou3.1_CSV.csv";
        List<Morphalou.Word> words = Morphalou.parseCsv(path);
        buildBigOdt(words);

    }
}
