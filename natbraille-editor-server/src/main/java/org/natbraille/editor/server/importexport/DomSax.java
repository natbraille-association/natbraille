package org.natbraille.editor.server.importexport;

import org.w3c.dom.Element;
import org.w3c.dom.Node;


/**
 * Mimics Sax Parser on a org.w3c.dom  Document
 */
public class DomSax {
    /**
     * Mimics Sax Handler on a org.w3c.dom Nodes
     */
    public static interface DomSaxHandler {
        void startElement(Element e);

        void endElement(Element e);

        void characters(String s);
    }

    /**
     * Recursively parse element and visit using the DomSaxHandler
     * @param node root element to parse
     * @param handler object visiting elements and textContent
     */
    public static void parse(Node node, DomSaxHandler handler) {
        if (node.getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) node;
            handler.startElement(element);
            Node childNode = element.getFirstChild();
            while (childNode != null) {
                parse(childNode, handler);
                childNode = childNode.getNextSibling();
            }
            handler.endElement(element);
        } else if (node.getNodeType() == Node.TEXT_NODE) {
            handler.characters(node.getTextContent());
        }
    }

}
