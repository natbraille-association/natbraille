package org.natbraille.editor.server.faketranscriptor.g2;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvValidationException;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/*
 * Parsing and labelling of Morpalou lexicon.
 * (from csv)
 * https://repository.ortolang.fr/api/content/morphalou/2/LISEZ_MOI.html#idp37270384
 */
public class Morphalou {
    private static String getStringAtCol(String[] cols, int num) {
        if (num < cols.length) {
            return cols[num];
        } else {
            return "";
        }
    }

    public static class Lemme {
        public final String graphie;
        public final String id;
        public final String categorie;
        public final String sous_categorie;
        public final String locution;
        public final String genre;
        public final String autres_lemmes_lies;
        public final String phonetique;
        public final String origines;

        public Lemme(String graphie, String id, String categorie, String sous_categorie, String locution, String genre, String autres_lemmes_lies, String phonetique, String origines) {
            this.graphie = graphie;
            this.id = id;
            this.categorie = categorie;
            this.sous_categorie = sous_categorie;
            this.locution = locution;
            this.genre = genre;
            this.autres_lemmes_lies = autres_lemmes_lies;
            this.phonetique = phonetique;
            this.origines = origines;
        }

        public static Lemme fromCsvColumns(String[] cols) {
            return new Lemme(
                    getStringAtCol(cols, 0), getStringAtCol(cols, 1), getStringAtCol(cols, 2),
                    getStringAtCol(cols, 3), getStringAtCol(cols, 4), getStringAtCol(cols, 5),
                    getStringAtCol(cols, 6), getStringAtCol(cols, 7), getStringAtCol(cols, 8)
            );
        }
    }

    public static class Flexion {
        public final String graphie;
        public final String id;
        public final String nombre;
        public final String mode;
        public final String genre;
        public final String temps;
        public final String personne;
        public final String phonetique;
        public final String origines;

        public Flexion(String graphie, String id, String nombre, String mode, String genre, String temps, String personne, String phonetique, String origines) {
            this.graphie = graphie;
            this.id = id;
            this.nombre = nombre;
            this.mode = mode;
            this.genre = genre;
            this.temps = temps;
            this.personne = personne;
            this.phonetique = phonetique;
            this.origines = origines;
        }

        public static Flexion fromCsvColumns(String[] cols) {
            return new Flexion(
                    getStringAtCol(cols, 9), getStringAtCol(cols, 10), getStringAtCol(cols, 11),
                    getStringAtCol(cols, 12), getStringAtCol(cols, 13), getStringAtCol(cols, 14),
                    getStringAtCol(cols, 15), getStringAtCol(cols, 16), getStringAtCol(cols, 17)
            );
        }
    }

    public static class Word {
        public final Lemme lemme;
        public final Flexion flexion;
        final boolean isLemme;

        public Word(Lemme lemme, Flexion flexion, boolean isLemme) {
            this.lemme = lemme;
            this.flexion = flexion;
            this.isLemme = isLemme;
        }

        /*
            this.graphie = graphie;
            this.id = id;
            this.categorie = categorie;
            this.sous_categorie = sous_categorie;
            this.locution = locution;
            this.genre = genre;
            this.autres_lemmes_lies = autres_lemmes_lies;
            this.phonetique = phonetique;
            this.origines = origines;
         */
        /*
            this.graphie = graphie;
            this.id = id;
            this.nombre = nombre;
            this.mode = mode;
            this.genre = genre;
            this.temps = temps;
            this.personne = personne;
            this.phonetique = phonetique;
            this.origines = origines;

         */
        public String getGraphie() {
            if (isLemme) {
                return lemme.graphie;
            } else {
                return flexion.graphie;
            }
        }

        public void check() {
            if (flexion.graphie.isEmpty() && (!isLemme)) {
                // graphie flexion is empty <=> isLemme
                System.out.println(lemme.graphie);
            }
            if (flexion.graphie.isEmpty() && (isLemme)) {
                // graphie flexion is empty <=> isLemme
                System.out.println(lemme.graphie);
            }
            if (flexion.id.isEmpty()) {
                // flexion id never empty
                System.out.println(flexion.graphie);
            }
            if (lemme.id.isEmpty()) {
                // lemme id never empty
                System.out.println(lemme.graphie);
            }
            if (isLemme && (flexion.id.isEmpty())) {
                // even lemmes have a flexion id
            }
            if (lemme.graphie.equals(flexion.graphie)) {
                if (!isLemme) {
                    // revoilà
                    //   System.out.println(lemme.graphie+" "+lemme.id+" "+flexion.id);
                }
            }
        }
    }

    public static List<Word> parseCsv(String path) throws IOException, CsvValidationException {
        List<Word> words = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            Lemme lemme = null;
            CSVParser parser = new CSVParserBuilder().withSeparator(';').build();
            try (CSVReader csvReader = new CSVReaderBuilder(br).withSkipLines(16).withCSVParser(parser).build()) {
                String[] columns;
                while ((columns = csvReader.readNext()) != null) {
                    boolean isLemme = !(columns[0].isEmpty());
                    if (isLemme) {
                        lemme = Lemme.fromCsvColumns(columns);
                    }
                    Flexion flexion = Flexion.fromCsvColumns(columns);
                    Word word = new Word(lemme, flexion, isLemme);
                    words.add(word);
                }
            }
        }
        return words;
    }

    public static void main(String[] args) throws IOException, CsvValidationException {
        final String path = "/home/vivien/pr-src/doc-g2/morphalou/morphalou/5/Morphalou3.1_formatCSV_toutEnUn/Morphalou3.1_CSV.csv";
        List<Word> words = parseCsv(path);

        // export word list to file
        //FileOutputStream fos = new FileOutputStream("/home/vivien/pr-src/doc-g2/morphalou/morphalou/5/Morphalou3.1_formatCSV_toutEnUn/Morphalou3.1-wordlist.txt");
        FileOutputStream fos = new FileOutputStream("/home/vivien/pr-src/doc-g2/morphalou/morphalou/5/Morphalou3.1_formatCSV_toutEnUn/Morphalou3.1-wordlist-with-opencsv.txt");
        for (Word word : words) {
            fos.write(word.getGraphie().getBytes(StandardCharsets.UTF_8));
            fos.write("\n".getBytes(StandardCharsets.UTF_8));
        }
        fos.close();

    }
}
