package org.natbraille.editor.server.userDocuments;

import org.natbraille.editor.server.Server;
import org.natbraille.editor.server.tools.ZipDirectory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class UserDocumentsFilesystem implements UserDocuments {
    private static Logger LOGGER = LoggerFactory.getLogger(UserDocumentsFilesystem.class);

    private static Set<String> Kinds = new HashSet<>(Arrays.asList(new String[]{
            "configurations",
            "fr-g2-rules",
            "stylist",
            "hyphenation-dictionary"
    }));

    static Path userDocumentRoot = Paths.get(Server.serverConfiguration.documentsRoot, "users");

    public static Path getValidDocumentDirectory(String username, String kind) {
        // check if kind is valid
        if (Kinds.contains(kind) == false) {
            return null;
        }
        Path directory = userDocumentRoot.resolve(Paths.get(username,kind));
        return directory;
    }

    public static Path getValidDocumentPath(String username, String kind, String name) {
        Path directory = getValidDocumentDirectory(username, kind);
        if (directory == null) {
            return null;
        }
        // check filename (TODO)
        Path filename = Paths.get(name).getFileName();
        return directory.resolve(filename);
    }

    public boolean saveDocument(String username, String kind, String name, byte[] document) {
        try {
            Path path = getValidDocumentPath(username, kind, name);
            if (path == null) {
                return false;
            }
            // recursively create user document directory
            Files.createDirectories(path.getParent());
            // write data
            Files.write(path, document);
            LOGGER.debug("wrote document user : {} kind : {} name : {}", username, kind, name);
            return true;
        } catch (IOException e) {
            LOGGER.error("could not save document user : {} kind : {} name : {}", username, kind, name, e);
            return false;
        }
    }

    public byte[] loadDocument(String username, String kind, String name) {
        try {
            Path path = getValidDocumentPath(username, kind, name);
            if (path == null) {
                return null;
            }
            LOGGER.debug("read document username : {} kind : {} name : {}", username, kind, name);
            return Files.readAllBytes(path);
        } catch (IOException e) {
            LOGGER.error("could not load document user : {} kind : {} name : {}", username, kind, name, e);
            return null;
        }
    }
    public boolean deleteDocument(String username, String kind, String name) {
        try {
            Path path = getValidDocumentPath(username, kind, name);
            if (path == null) {
                return false;
            }
            LOGGER.debug("delete document username : {} kind : {} name : {}", username, kind, name);
            return Files.deleteIfExists(path);
        } catch (IOException e) {
            LOGGER.error("could not delete document user : {} kind : {} name : {}", username, kind, name, e);
            return false;
        }
    }
    public String[] listDocuments(String username, String kind) {
        Path path = getValidDocumentDirectory(username, kind);
        if (path != null) {
            String list[] = path.toFile().list();
            if (list != null) {
                return list;
            }
        }
        return new String[]{};
    }

    @Override
    public byte[] loadArchive(String username) {
        try {
            Path directory = userDocumentRoot.resolve(Paths.get(username));
            return ZipDirectory.zipDirectory(directory);
        } catch (IOException e) {
            LOGGER.error("could not load archive user : {}", username, e);
            return null;
        }
    }

    @Override
    public void deleteUserDocuments(String username) {
        LOGGER.error("deleteUserDocuments is NOT IMPLEMENTED for filesystem");
    }
}
