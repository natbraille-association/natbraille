package org.natbraille.editor.server.faketranscriptor.steps;

import org.natbraille.editor.server.faketranscriptor.FakeTranscription;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

import static org.natbraille.editor.server.faketranscriptor.FakeTranscription.forEachElement;

public class Step1EnsureFragmentId {
    public static void step1(FakeTranscription ft, Document inputDoc/*String inputFilename, String outputFilename*/) throws ParserConfigurationException, IOException, SAXException, TransformerException, Exception {
        //Document inputDoc = ft.parseDOM(inputFilename);
        AtomicInteger fragmentIdCounter = new AtomicInteger(1);
        forEachElement(inputDoc, "(//p|//h1|//h2|//h3|//h4|//h5|//h6|//word|//punctuation|//spacing|//number|//math|//span)/self::node()[not(@fragmentId)]", inline -> {
            String fragmentId = "f" + fragmentIdCounter;
            inline.setAttribute("fragmentId", fragmentId);
            fragmentIdCounter.getAndIncrement();
        });
        //ft.serializeDOM(outputFilename, inputDoc);
        //return inputDoc;
    }
}
