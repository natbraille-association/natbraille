/*
 * NatBraille Http Server - An universal Translator over http
 * Copyright (C) 2013 Vivien Guillet
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.natbraille.editor.server.formats.nfometa;

import org.natbraille.core.filter.options.NatFilterOptionDetail;

/**
 * copied from http-server
 */
public class FoNatFilterOptionDetail {
	
	public String comment;
	public String defaultValue;
	public String hidden;
	public String[] possibleValues;
	public String[] categories;
	public String type;
	
	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}
	/**
	 * @param comment the comment to set
	 */
	private void setComment(String comment) {
		this.comment = comment;
	}
	/**
	 * @return the defaultValue
	 */
	public String getDefaultValue() {
		return defaultValue;
	}
	/**
	 * @param defaultValue the defaultValue to set
	 */
	private void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
	/**
	 * @return the hidden
	 */
	public String getHidden() {
		return hidden;
	}
	/**
	 * @param hidden the hidden to set
	 */
	private void setHidden(String hidden) {
		this.hidden = hidden;
	}
	/**
	 * @return the possibleValues
	 */
	public String[] getPossibleValues() {
		return possibleValues;
	}
	/**
	 * @param possibleValues the possibleValues to set
	 */
	private void setPossibleValues(String[] possibleValues) {
		this.possibleValues = possibleValues;
	}
	/**
	 * @return the categories
	 */
	public String[] getCategories() {
		return categories;
	}
	/**
	 * @param categories the categories to set
	 */
	private void setCategories(String[] categories) {
		this.categories = categories;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	private void setType(String type) {
		this.type = type;
	}

	public FoNatFilterOptionDetail(NatFilterOptionDetail detail) {
		
		String strCategs[] = new String[detail.categories().length];
		for (int i=0;i<detail.categories().length;i++){
			strCategs[i] = detail.categories()[i].name();
		}
		setCategories(strCategs);
		String strPossVal[] = new String[detail.possibleValues().length];
		for (int i=0;i<detail.possibleValues().length;i++){
			strPossVal[i] = detail.possibleValues()[i];
		}
		setPossibleValues(strPossVal);
		setComment(detail.comment());
		setHidden(detail.hidden());
		setType(detail.type().name());
		setDefaultValue(detail.defaultValue());
	}
	
}
