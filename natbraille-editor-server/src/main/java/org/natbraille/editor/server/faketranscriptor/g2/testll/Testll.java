package org.natbraille.editor.server.faketranscriptor.g2.testll;

//https://github.com/liblouis/liblouis-java/blob/master/src/test/java/org/liblouis/TranslatorTest.java

import org.liblouis.*;

import java.util.Arrays;

/*import org.liblouis.DisplayTable.StandardDisplayTables;
import static org.liblouis.Louis.asFile;
import static org.liblouis.Utilities.Hyphenation.insertHyphens;

 */
public class Testll {

    static Typeform getTypeformByName(Translator translator, String name) {
        Typeform typeform = null;
        for (Typeform tt : translator.getSupportedTypeforms()) {
            System.out.println(tt.getName() + " " + tt);
            if (tt.getName().equals(name)) {
                typeform = tt;
            }
        }
        return typeform;
    }

    static Typeform[] makeTypeformArray(Typeform emphasis, String string) {
        Typeform[] typeforms = new Typeform[string.length()];
        for (int i = 0; i < string.length(); i++) {
            typeforms[i] = (string.charAt(i) == '^') ? emphasis : Typeform.PLAIN_TEXT;
        }
        return typeforms;
    }

    public static void forwardIntegral() throws CompilationException, TranslationException, DisplayException {

        /**
         * ⠨T
         * ⠨⠨TOUT
         * ⠨Une
         * u⠨Ne
         * p⠨⠨LU⠠⠄sieurs
         * p⠨⠨LU⠠⠄sieu⠨Rs
         * p⠨⠨LU⠠⠄sieu⠨⠨RE⠠⠄s
         * p⠨⠨LU⠠⠄sieu⠨⠨RES
         *
         * (mot d'une lettre -> prefixe simple ⠨)
         * une seule lettre en majuscule -> préfixe simple ⠨
         * plusieurs lettres en majuscules -> préfixe double⠨
         * retour en minuscules : ⠠ ⠄
         *
         ⠨ ⠍ ⠁ ⠚ ⠥ ⠎ ⠉ ⠥ ⠇ ⠑ ⠎
         mAjuscules ⠍ ⠨ ⠁ ⠚ ⠥ ⠎ ⠉ ⠥ ⠇ ⠑ ⠎
         mAJuscules ⠍ ⠨ ⠨ ⠁ ⠚ ⠠ ⠄ ⠥ ⠎ ⠉ ⠥ ⠇ ⠑⠎
         mAJuscuLes ⠍ ⠨ ⠨ ⠁ ⠚ ⠠ ⠄ ⠥ ⠎ ⠉ ⠥ ⠨ ⠇ ⠑ ⠎
         mAJuscuLEs ⠍ ⠨ ⠨ ⠁ ⠚ ⠠ ⠄ ⠥ ⠎ ⠉ ⠥ ⠨ ⠨ ⠇ ⠑ ⠠ ⠄ ⠎
         mAJuscuLES ⠍ ⠨ ⠨ ⠁ ⠚ ⠠ ⠄ ⠥ ⠎ ⠉ ⠥ ⠨ ⠨ ⠇  ⠑ ⠎
         *
         */
        /*
         * ⠸iiiii
         * ⠸i⠠⠄nn
         *
         * Idem mais pas de signe pour plusieurs lettres (implicite dans une lettre)
         *
         * mev à l'intéieur d'un mot -> intégral.
         *
         * /> quatre consecutifs italiques
         * /> quatre consecutifs italiques
         *
         *
         *
         * ' aopstropphe => Pas ponctuation ?
         */
        String table = "unicode.dis,fr-bfu-comp6.utb";
        //String table = "fr-bfu-comp6.utb";
        Translator translator = new Translator(table);
        Typeform emphasis = getTypeformByName(translator, "italic");
        {
            String text = "UNE LONGUE SUITE DE MOTS EN MAJUSCULES";
            TranslationResult result = translator.translate(text, null, null, null);
            System.out.println(text);
            System.out.println(result.getBraille());
        }
        {
            String text = "M";
            TranslationResult result = translator.translate(text, null, null, null);
            System.out.println(text);
            System.out.println(result.getBraille());
        }
        {
            String text = "Majuscules";
            TranslationResult result = translator.translate(text, null, null, null);
            System.out.println(text);
            System.out.println(result.getBraille());
        }
        {
            String text = "mAjuscules";
            TranslationResult result = translator.translate(text, null, null, null);
            System.out.println(text);
            System.out.println(result.getBraille());
        }
        {
            String text = "mAJuscules";
            TranslationResult result = translator.translate(text, null, null, null);
            System.out.println(text);
            System.out.println(result.getBraille());
        }
        {
            String text = "mAJuscuLes";
            TranslationResult result = translator.translate(text, null, null, null);
            System.out.println(text);
            System.out.println(result.getBraille());
        }
        {
            String text = "mAJuscuLEs";
            TranslationResult result = translator.translate(text, null, null, null);
            System.out.println(text);
            System.out.println(result.getBraille());
        }
        {
            String text = "mAJuscuLES";
            TranslationResult result = translator.translate(text, null, null, null);
            System.out.println(text);
            System.out.println(result.getBraille());
        }

        {
            String text = "MAJUSCULES";
            TranslationResult result = translator.translate(text, null, null, null);
            System.out.println(text);
            System.out.println(result.getBraille());
        }
        {
            String text = "MAJUSCules";
            TranslationResult result = translator.translate(text, null, null, null);
            System.out.println(text);
            System.out.println(result.getBraille());
        }
        {
            String text = "majUSCules";
            TranslationResult result = translator.translate(text, null, null, null);
            System.out.println(text);
            System.out.println(result.getBraille());
        }
        {
            String text = "majUSCuLEs";
            TranslationResult result = translator.translate(text, null, null, null);
            System.out.println(text);
            System.out.println(result.getBraille());
        }
        {
            String text = "majUSCuLes";
            TranslationResult result = translator.translate(text, null, null, null);
            System.out.println(text);
            System.out.println(result.getBraille());
        }
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        {
            String text = "abcde";
            String emph = "^^^^^";
            TranslationResult result = translator.translate(text, makeTypeformArray(emphasis, emph), null, null);
            System.out.println(text);
            System.out.println(emph);
            System.out.println(result.getBraille());
        }
        {
            String text = "abcde";
            String emph = " ^^^ ";
            TranslationResult result = translator.translate(text, makeTypeformArray(emphasis, emph), null, null);
            System.out.println(text);
            System.out.println(emph);
            System.out.println(result.getBraille());
        }
        {
            String text = "abcde";
            String emph = "^    ";
            TranslationResult result = translator.translate(text, makeTypeformArray(emphasis, emph), null, null);
            System.out.println(text);
            System.out.println(emph);
            System.out.println(result.getBraille());
        }
        {
            String text = "abcde";
            String emph = " ^   ";
            TranslationResult result = translator.translate(text, makeTypeformArray(emphasis, emph), null, null);
            System.out.println(text);
            System.out.println(emph);
            System.out.println(result.getBraille());
        }
        {
            String text = "abcde";
            String emph = " ^^  ";
            TranslationResult result = translator.translate(text, makeTypeformArray(emphasis, emph), null, null);
            System.out.println(text);
            System.out.println(emph);
            System.out.println(result.getBraille());
        }
        {
            String text = "abcde";
            String emph = " ^^^^";
            TranslationResult result = translator.translate(text, makeTypeformArray(emphasis, emph), null, null);
            System.out.println(text);
            System.out.println(emph);
            System.out.println(result.getBraille());
        }
        {
            String text = "ABCde";
            String emph = " ^^^^";
            TranslationResult result = translator.translate(text, makeTypeformArray(emphasis, emph), null, null);
            System.out.println(text);
            System.out.println(emph);
            System.out.println(result.getBraille());
        }
        {
            String text = "ABCde";
            String emph = " ^^^ ";
            TranslationResult result = translator.translate(text, makeTypeformArray(emphasis, emph), null, null);
            System.out.println(text);
            System.out.println(emph);
            System.out.println(result.getBraille());
        }


    }

    public static void forward() throws CompilationException, TranslationException, DisplayException {
        //String table = "/usr/share/liblouis/tables/fr-bfu-g2.ctb";
        String table = "fr-bfu-g2.ctb";


        Translator translator = new Translator(table);

        Typeform emphTypeForm = null;
        for (Typeform tt : translator.getSupportedTypeforms()) {
            System.out.println(tt.getName() + " " + tt);
            if (tt.getName().equals("italic")) {
                emphTypeForm = tt;
            }
        }

        String text = "Camion JE CAMIONNE CAMIONNE DANS LE CAMION oui";

        Typeform[] typeforms = new Typeform[text.length()];
        {
            for (int i = 0; i < typeforms.length; i++) {
                if (i == 1) {
                    typeforms[i] = emphTypeForm;
                }
            }
        }

        int[] characterAttributes = new int[text.length()];
        for (int i = 0; i < characterAttributes.length; i++) {
            characterAttributes[i] = i;
        }

        int[] interCharacterAttributes = new int[text.length() - 1];
        for (int i = 0; i < interCharacterAttributes.length; i++) {
            interCharacterAttributes[i] = i;
        }

        TranslationResult result = translator.translate(text, typeforms, characterAttributes, interCharacterAttributes);
        System.out.println(Arrays.toString(typeforms));
        System.out.println(text + " -> " + result.getBraille());
        System.out.println(Arrays.toString(characterAttributes) + " -> " + Arrays.toString(result.getCharacterAttributes()));
        System.out.println(Arrays.toString(interCharacterAttributes) + " -> " + Arrays.toString(result.getInterCharacterAttributes()));


    }

    public static void backwards(String text) throws CompilationException, TranslationException, DisplayException {
        //String table = "/usr/share/liblouis/tables/fr-bfu-g2.ctb";
        String table = "fr-bfu-g2.ctb";
        Translator translator = new Translator(table);


        String result = translator.backTranslate(text);

        //System.out.println(Arrays.toString(typeforms));
        System.out.println(text + " -> " + result);


    }

    public static void main(String[] args) throws CompilationException, TranslationException, DisplayException {

        forward();
        backwards("¨c|a`'m0");
        backwards("¨cam0");
        // Louis.setLibraryPath();
        // for (Table table : Louis.listTables())    {
        // System.out.println(table);
        // }
        forwardIntegral();


    }
}
