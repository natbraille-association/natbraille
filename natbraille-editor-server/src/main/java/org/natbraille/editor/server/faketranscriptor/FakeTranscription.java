package org.natbraille.editor.server.faketranscriptor;

import net.sf.saxon.s9api.SaxonApiException;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.editor.server.Server;
import org.natbraille.editor.server.faketranscriptor.steps.*;
import org.natbraille.editor.server.faketranscriptor.transliteration.Transliteration;
import org.natbraille.editor.server.formats.AllTranscriptionSteps;
import org.natbraille.editor.server.performance.DurationMeasurement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.*;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.function.Function;

import static org.natbraille.editor.server.faketranscriptor.layout.FakeHyphenateTrie.hyphenate1;

public class FakeTranscription {
    private static final Logger LOGGER = LoggerFactory.getLogger(FakeTranscription.class);

    public static void main(String[] args) throws Exception {

        // String input = "./natbraille-editor-server/test/00-source-long.html";
        // String input = "/home/vivien/Bureau/imports/to-braille-sav.xml";
        //String input = "./natbraille-editor-server/test/00-source-g2.xml";
//        String input = "/home/vivien/Bureau/nat/listes-tmp/input-document.xml";
        //String input = "/home/vivien/Bureau/nat/math-prefix/input-document.xml";
        //String input = "/home/vivien/Bureau/nat/chimie/input-document.xml";
        //  String input = "/home/vivien/Bureau/nat/braille-bloc/input-document.xml";
        //String input = "/home/vivien/Bureau/nba-layout/document0.xml";
        //String input = "/home/vivien/Bureau/nba-layout/0-source.html";
        //String input = "/home/vivien/Bureau/nba-layout/documentgsb.xml";
//        String input = "/home/vivien/Bureau/nba-layout/w0-source.html";
        //String input = "/home/vivien/Bureau/nba-layout/melange.html";
        //String input = "/home/vivien/Bureau/nba-layout/simple-layout.html";
        String input = "/home/vivien/Bureau/nba-layout/document-bug-loop.html";


        FakeTranscription ft = new FakeTranscription();

        DurationMeasurement d = new DurationMeasurement();
        d.start();
        int count = 1;
        //int count = 1;
        for (int i = 0; i < count; i++) {
            NatFilterOptions filterOptions = new NatFilterOptions();
            // filterOptions.setOption(NatFilterOption.MODE_G2,"true");
            filterOptions.setOption(NatFilterOption.MODE_G2,"false");
            filterOptions.setOption(NatFilterOption.LAYOUT,"true");
            filterOptions.setOption(NatFilterOption.FORMAT_LINE_LENGTH,"25");
            filterOptions.setOption(NatFilterOption.FORMAT_PAGE_LENGTH,"10");
            filterOptions.setOption(NatFilterOption.LAYOUT_NUMBERING_START_PAGE,"1");
            filterOptions.setOption(NatFilterOption.LAYOUT_NUMBERING_MODE,"'hb'"); // haut inclus
            //filterOptions.setOption(NatFilterOption.LAYOUT_NUMBERING_MODE,"'hs'"); // haut seul
            //filterOptions.setOption(NatFilterOption.LAYOUT_NUMBERING_MODE,"'bb'"); // bas inclus
            //filterOptions.setOption(NatFilterOption.LAYOUT_NUMBERING_MODE,"'bs'"); // bas seul
            //filterOptions.setOption(NatFilterOption.LAYOUT_NUMBERING_MODE,"'nn'"); // aucune
            filterOptions.setOption(NatFilterOption.LAYOUT_PAGE_BREAKS,"true");

            Document xmlDocument = ft.parseDOM(input);
            step00(ft, xmlDocument);//, "././natbraille-editor-server/test/0-source.html");
            ft.serializeDOM("././natbraille-editor-server/test/0-source.html", xmlDocument);
            Step0DivideText.step0(ft/*, "./natbraille-editor-server/test/0-source.html", "./natbraille-editor-server/test/1-pre.html"*/, filterOptions, xmlDocument);
            ft.serializeDOM("./natbraille-editor-server/test/1-pre.html", xmlDocument);
            Step1EnsureFragmentId.step1(ft/*,"./natbraille-editor-server/test/1-pre.html", "./natbraille-editor-server/test/2-fragmentId.html"*/, xmlDocument);
            ft.serializeDOM("./natbraille-editor-server/test/2-fragmentId.html", xmlDocument);
            Step2Transcribe.step2(ft/*, "./natbraille-editor-server/test/2-fragmentId.html", "./natbraille-editor-server/test/3-trans.html",*/, filterOptions, xmlDocument);
            ft.serializeDOM("./natbraille-editor-server/test/3-trans.html", xmlDocument);
            xmlDocument = Step3Layout.step3(ft /*,"./natbraille-editor-server/test/3-trans.html", "./natbraille-editor-server/test/4-mep.html"*/, filterOptions, xmlDocument);
            ft.serializeDOM("./natbraille-editor-server/test/4-mep.html", xmlDocument);
            Files.writeString(Path.of("./natbraille-editor-server/test/4b-mep-text.txt"), StepText.renderToText(xmlDocument,filterOptions));
            ft.serializeDOM("./natbraille-editor-server/test/5-table.html", xmlDocument);
            Files.writeString(Path.of("./natbraille-editor-server/test/5b-mep-text.txt"), StepText.renderToText(xmlDocument,filterOptions));
            ft.serializeDOM("./natbraille-editor-server/test/6-table-text.html", xmlDocument);
        }
        d.end();
        System.out.println("total: " + d.getDurationSecond() + "s");
        System.out.println("mean: " + d.getDurationSecond() / (double) count + "s");

    }

    public static AllTranscriptionSteps translate(String source, NatFilterOptions natFilterOptions) throws Exception {

        FakeTranscription ft = new FakeTranscription();
        AllTranscriptionSteps allTranscriptionSteps = new AllTranscriptionSteps();

        try {
            String rSource = source.replace("<html>", "<html><body>").replace("</html>", "</body></html>");

            allTranscriptionSteps._0_source = rSource;

            Document xmlDocument = ft.parseDOMString(rSource);

            Step0DivideText.step0(ft, /*fn0Source, fn1Pre,*/ natFilterOptions, xmlDocument);
            allTranscriptionSteps._1_pre = ft.serializeDOM(xmlDocument);

            Step1EnsureFragmentId.step1(ft, /*fn1Pre, fn2FragmentId,*/xmlDocument);
            allTranscriptionSteps._2_fragmentId = ft.serializeDOM(xmlDocument);

            Step2Transcribe.step2(ft, /*fn2FragmentId, fn3Trans,*/ natFilterOptions, xmlDocument);
            allTranscriptionSteps._3_trans = ft.serializeDOM(xmlDocument);

            xmlDocument = Step3Layout.step3(ft, /*fn3Trans, fn4mepXml,*/ natFilterOptions, xmlDocument);
            allTranscriptionSteps._4_mep_xml = ft.serializeDOM(xmlDocument);

            Step4BrailleTable.step4(ft, /*fn4mepXml, fn5mepTable,*/ natFilterOptions, xmlDocument);
            allTranscriptionSteps._5_table = ft.serializeDOM(xmlDocument);

            allTranscriptionSteps._5b_table_text_only =StepText.renderToText(xmlDocument,natFilterOptions);

        } catch (Exception e) {
            LOGGER.error("error in transcription steps", e);
        }

        if (Server.serverConfiguration.writeServerTranscriptionStepsFiles) {
            // String baseDirectory = "./natbraille-editor-server/test/";
            Path baseDirectory;
            if (Server.serverConfiguration.useFixedSingleTempDirectory) {
                baseDirectory = Path.of("./natbraille-editor-server/test/");
            } else {
                baseDirectory = Files.createTempDirectory("natbraille-transcription");
            }

            Path fn0Source = Path.of(baseDirectory.toString(), "w0-source.html");
            Path fn1Pre = Path.of(baseDirectory.toString(), "w1-pre.html");
            Path fn2FragmentId = Path.of(baseDirectory.toString(), "w2-fragmentId.html");
            Path fn3Trans = Path.of(baseDirectory.toString(), "w3-trans.html");
            Path fn4mepXml = Path.of(baseDirectory.toString(), "w4-mep.html");
//            String fn4mepTxt = fn4mepXml + ".txt";
            Path fn5mepTable = Path.of(baseDirectory.toString(), "w5-table.html");
            Path fn5mepTableTextOnly = Path.of(baseDirectory.toString(), "w5b-table-text-only.html");

            if (!Server.serverConfiguration.useFixedSingleTempDirectory) {
                Files.deleteIfExists((fn0Source));
                Files.deleteIfExists((fn1Pre));
                Files.deleteIfExists((fn2FragmentId));
                Files.deleteIfExists((fn3Trans));
                Files.deleteIfExists((fn4mepXml));
  //              Files.deleteIfExists(Path.of(fn4mepTxt));
                Files.deleteIfExists((fn5mepTable));
                Files.deleteIfExists((fn5mepTableTextOnly));
                Files.deleteIfExists(baseDirectory);
            }

            if (allTranscriptionSteps._0_source != null) {
                Files.writeString(fn0Source, allTranscriptionSteps._0_source);
            }
            if (allTranscriptionSteps._1_pre != null) {
                Files.writeString(fn1Pre, allTranscriptionSteps._1_pre, StandardCharsets.UTF_8);
            }
            if (allTranscriptionSteps._2_fragmentId != null) {
                Files.writeString(fn2FragmentId, allTranscriptionSteps._2_fragmentId, StandardCharsets.UTF_8);
            }
            if (allTranscriptionSteps._3_trans != null) {
                Files.writeString(fn3Trans, allTranscriptionSteps._3_trans, StandardCharsets.UTF_8);
            }
            if (allTranscriptionSteps._4_mep_xml != null) {
                Files.writeString(fn4mepXml, allTranscriptionSteps._4_mep_xml, StandardCharsets.UTF_8);
            }
            if (allTranscriptionSteps._5_table != null) {
                Files.writeString(fn5mepTable, allTranscriptionSteps._5_table, StandardCharsets.UTF_8);
            }
            if (allTranscriptionSteps._5b_table_text_only != null) {
                Files.writeString(fn5mepTableTextOnly, allTranscriptionSteps._5b_table_text_only, StandardCharsets.UTF_8);
            }
        }

        return allTranscriptionSteps;
        //return Files.readString(Path.of("./natbraille-editor-server/test/w4-mep.html"));
    }

    public static Transliteration transliteration;

    static {
        try {
            transliteration = new Transliteration(Transliteration.Loader.loadTransliterationFromResource("transliterate-fr.txt"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static void step00(FakeTranscription ft, Document inputDoc /*String inputFilename, String outputFilename*/) throws Exception {
        //Document inputDoc = ft.parseDOM(inputFilename);

        // remove text nodes at wrong places
        String[] wrongPlaces = new String[]{"body", "ul"};

        List<Node> toRemove = new ArrayList<>();
        Set<String> elementWithoutTextChildNode = new HashSet<>(Arrays.asList(wrongPlaces));
        forEachNode(inputDoc, "//text()", block -> {
            Node parent = block.getParentNode();
            if ((parent != null) && elementWithoutTextChildNode.contains(parent.getNodeName())) {
                toRemove.add(block);
            }
        });
        for (Node removeMe : toRemove) {
            removeMe.getParentNode().removeChild(removeMe);
        }
        //return inputDoc;
        //ft.serializeDOM(outputFilename, inputDoc);

    }

    // static int mepPageHeight = 10;
    //static int mepPageWidth = 15;


    public static String padLeft(String s, int n) {
        return String.format("%" + n + "s", s);
    }


    public static Function<String, int[]> hyphenate;

    static {
        try {
            hyphenate = hyphenate1(FakeTranscription.class.getClassLoader().getResourceAsStream("org/natbraille/core/xsl/dicts/hyph_fr.dic"), 2, 2);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
/*
    public static int getNodeIndexInParent(Node node) {
        int count = 0;
        for (; ; ) {
            node = node.getPreviousSibling();
            if (node == null) {
                return count;
            }
            count++;
        }
    }
*/

    /// ////////////////////////////////////////////////////
    /// //////////////////////////////////////////////////// 2
    /// ////////////////////////////////////////////////////
    public static final String oneLetterUcPrefix = "⠨";
    public static final String oneUcWordPrefix = "⠨⠨";
    public static final String fourSuccessiveUcWordsFirstWordPrefix = "⠒⠨";
    public static final String fourSuccessiveUcWordsLastWordPrefix = oneLetterUcPrefix;

    static final String EndPrefix = "⠠⠄";

    public static final String wholeWordEmphasisPrefix = "⠸";
    public static final String partialWordEmphasisStartPrefix = wholeWordEmphasisPrefix;
    public static final String partialWordEmphasisEndPrefix = EndPrefix;
    public static final String fourSuccessiveEmphasisWordsFirstWordPrefix = "⠒⠸";
    public static final String fourSuccessiveEmphasisWordsLastWordPrefix = wholeWordEmphasisPrefix;

    public static final String NumberPrefix = "⠠";
    public static final String NumberSeparator = "⠄";

    //public static final String BrailleBlank = "⠀"; // this is not a space (U+2800)
    //public static final String BrailleBlank = "⎵";
    public static final String BrailleBlank = " ";



/*
    public static String replaceLastOccurrence(String originalString, String searchString, String replacement) {
        int lastIndex = originalString.lastIndexOf(searchString);

        if (lastIndex == -1) {
            // If searchString is not found, return original string
            return originalString;
        }

        String beforeLastOccurrence = originalString.substring(0, lastIndex);
        String afterLastOccurrence = originalString.substring(lastIndex + searchString.length());

        return beforeLastOccurrence + replacement + afterLastOccurrence;
    }
*/
    ///////////////////////////////////////////////////////
    /////////////////////////////////////////////////////// 1
    ///////////////////////////////////////////////////////


    ///////////////////////////////////////////////////////
    /////////////////////////////////////////////////////// 0
    ///////////////////////////////////////////////////////

    /// ////////////////////////////////////////////////////
    /// //////////////////////////////////////////////////// utils
    /// ////////////////////////////////////////////////////

    public static Element getNoir(Element t) {
        return getFirstElementByTagName(t, "noir");
    }

    public static Element getBraille(Element t) {
        return getFirstElementByTagName(t, "braille");
    }

    public static Element replaceByTranslationPair(Element src, boolean noirIsSource) {
        if (src.hasAttribute("source")) {
            throw new Error("ALREADY TRANSFORMED");
        }
        Element translationPair = createTranslationPair(src, noirIsSource);
        Node block = src.getParentNode();
        block.insertBefore(translationPair, src);
        block.removeChild(src);
        return translationPair;
    }

    public static Element createTranslationPair(Element src, boolean noirIsSource) {
        Document doc = src.getOwnerDocument();
        Element container = doc.createElement(src.getTagName());
        copyAttributes(src, container);
        container.setAttribute("source", noirIsSource ? "noir" : "braille");
        Element noir = (Element) container.appendChild(doc.createElement(Tag.noir.tagName));
        Element braille = (Element) container.appendChild(doc.createElement(Tag.braille.tagName));
        Element copyTo = noirIsSource ? noir : braille;
        for (Node child : iterable(src.getChildNodes())) {
            Node childClone = child.cloneNode(true);
            copyTo.appendChild(childClone);
        }
        return container;
    }

    /// ////////////////////////////////////////////////////
    /// //////////////////////////////////////////////////// DOM utils
    /// ////////////////////////////////////////////////////
    public static Element getFirstElementByTagName(Element t, String tagName) {
        return (Element) t.getElementsByTagName(tagName).item(0);
    }

    public static Iterable<Node> iterable(final NodeList nodeList) {
        return () -> new Iterator<Node>() {

            private int index = 0;

            @Override
            public boolean hasNext() {
                return index < nodeList.getLength();
            }

            @Override
            public Node next() {
                if (!hasNext()) throw new NoSuchElementException();
                return nodeList.item(index++);
            }
        };
    }

    public static Iterable<Node> iterable(final NamedNodeMap nodeList) {
        return () -> new Iterator<Node>() {

            private int index = 0;

            @Override
            public boolean hasNext() {
                return index < nodeList.getLength();
            }

            @Override
            public Node next() {
                if (!hasNext()) throw new NoSuchElementException();
                return nodeList.item(index++);
            }
        };
    }

    public static void copyAttributes(Node src, Element dest) {
        for (Node nn : iterable(src.getAttributes())) {
            dest.setAttribute(nn.getNodeName(), nn.getNodeValue());
        }
    }

    public interface ElementFunction {
        void run(Element element) throws Exception;

    }

    public interface NodeFunction {
        void run(Node node) throws Exception;

    }

    public static void forEachElement(Node root, String xpath, ElementFunction elementFunction) throws Exception {
        XPath xPath = XPathFactory.newInstance().newXPath();
        NodeList nodeList = (NodeList) xPath.compile(xpath).evaluate(root, XPathConstants.NODESET);
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;
                elementFunction.run(element);
            }
        }
    }

    public static void forEachNode(Node root, String xpath, NodeFunction nodeFunction) throws Exception {
        XPath xPath = XPathFactory.newInstance().newXPath();
        NodeList nodeList = (NodeList) xPath.compile(xpath).evaluate(root, XPathConstants.NODESET);
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            nodeFunction.run(node);
        }
    }

    private DocumentBuilderFactory dbf;
    private DocumentBuilder db;

    public FakeTranscription() throws ParserConfigurationException, IOException {
        System.setProperty("javax.xml.parsers.DocumentBuilderFactory", "org.apache.xerces.jaxp.DocumentBuilderFactoryImpl");
        System.setProperty("javax.xml.xpath.XPathFactory:http://java.sun.com/jaxp/xpath/dom", "com.sun.org.apache.xpath.internal.jaxp.XPathFactoryImpl");

        dbf = DocumentBuilderFactory.newInstance();

        // https://cheatsheetseries.owasp.org/cheatsheets/XML_External_Entity_Prevention_Cheat_Sheet.html
        dbf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
        dbf.setXIncludeAware(false);
        db = dbf.newDocumentBuilder();
    }

    public Document parseDOM(String filename) throws IOException, SAXException {
        return db.parse(filename);
    }

    public Document parseDOMString(String xmlString) throws IOException, SAXException {
        return db.parse(new InputSource(new StringReader(xmlString)));
    }

    public Document createDOM() {
        return db.newDocument();
    }

    public Iterable<Node> xpathNodesIt(Node root, String xpath) throws XPathExpressionException {
        XPath xPath = XPathFactory.newInstance().newXPath();
        return iterable((NodeList) xPath.compile(xpath).evaluate(root, XPathConstants.NODESET));
    }

    public NodeList xpathNodes(Node root, String xpath) throws XPathExpressionException {
        XPath xPath = XPathFactory.newInstance().newXPath();
        return (NodeList) xPath.compile(xpath).evaluate(root, XPathConstants.NODESET);
    }

    public Node xpathNode(Node root, String xpath) throws XPathExpressionException {
        XPath xPath = XPathFactory.newInstance().newXPath();
        return (Node) xPath.compile(xpath).evaluate(root, XPathConstants.NODE);
    }

    public void serializeDOM(String filename, Document doc) throws IOException, SaxonApiException {
        Files.writeString(Path.of(filename), XmlUtils.toString(doc));
    }

    public String serializeDOM(Document doc) throws IOException, SaxonApiException {
        return XmlUtils.toString(doc);
    }

    public static void mergeTextNodes(Element element) {
        Document doc = element.getOwnerDocument();
        NodeList children = element.getChildNodes();
        StringBuilder textContent = new StringBuilder();
        boolean onlyTextNodes = true;

        // Collect text content and check for non-text nodes
        for (int i = 0; i < children.getLength(); i++) {
            Node node = children.item(i);
            if (node.getNodeType() == Node.TEXT_NODE) {
                textContent.append(node.getTextContent());
            } else {
                onlyTextNodes = false;
                break; // Found a non-text node, stop processing
            }
        }

        // If all nodes are text, merge them into one
        if (onlyTextNodes) {
            // Remove existing nodes
            while (element.hasChildNodes()) {
                element.removeChild(element.getFirstChild());
            }
            // Add a single merged text node
            element.appendChild(doc.createTextNode(textContent.toString()));
        }
    }
}
