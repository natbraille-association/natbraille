package org.natbraille.editor.server.importexport;

import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.editor.server.faketranscriptor.XmlUtils;
import org.odftoolkit.odfdom.doc.OdfDocument;
import org.odftoolkit.odfdom.doc.OdfTextDocument;
import org.odftoolkit.odfdom.dom.element.OdfStylePropertiesBase;
import org.odftoolkit.odfdom.dom.element.draw.DrawFrameElement;
import org.odftoolkit.odfdom.dom.element.draw.DrawObjectElement;
import org.odftoolkit.odfdom.dom.element.office.OfficeTextElement;
import org.odftoolkit.odfdom.dom.element.text.*;
import org.odftoolkit.odfdom.dom.style.OdfStyleFamily;
import org.odftoolkit.odfdom.dom.style.props.OdfStylePropertiesSet;
import org.odftoolkit.odfdom.incubator.doc.office.OdfOfficeStyles;
import org.odftoolkit.odfdom.incubator.doc.style.OdfStyle;
import org.odftoolkit.odfdom.incubator.doc.text.OdfTextParagraph;
import org.odftoolkit.odfdom.pkg.OdfElement;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Only used for dev/testing
 */
public class TestOdt {

    /**
     * Only used for dev/testing
     *
     * @throws Exception
     */
    private static void testSourceToOpenDocument(String inputFilename, String outputFilename) throws Exception {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser saxParser = factory.newSAXParser();
        final NoirToOpenDocument.ProseMirrorFormatHandler proseMirrorFormatHandler = new NoirToOpenDocument.ProseMirrorFormatHandler("chimie");
        saxParser.parse(inputFilename, proseMirrorFormatHandler);
        proseMirrorFormatHandler.odt.save(outputFilename);
    }

    /**
     * Only used for dev/testing
     *
     * @throws Exception
     */
    public static void testOpenDocumentToSource(String inputFilename, String outputFilename) throws Exception {


        // Load file
        OdfTextDocument odt = (OdfTextDocument) OdfDocument.loadDocument(inputFilename);
        OpenDocumentToNoir.OpenDocumentToNoirStyle openDocumentToNoirStyle = new OpenDocumentToNoir.OpenDocumentToNoirStyle(odt);

        NatFilterOptions nfo = new NatFilterOptions();
        // nfo.getValue(NatFilterOption.Stylist);

        // GestionnaireErreur ge = new GestionnaireErreur();
        // NatDynamicResolver ndr = new NatDynamicResolver(nfo,ge);
        // Document stylistDocument = SecureXmlParsing.byteToDocument(ndr.readByteArrayAtURI(URI.create(nfo.getValue(NatFilterOption.Stylist))));

        Document stylistDocument = StylistWrapper.buildStylistDocumentFromNatFilterOptions(nfo);
        String chemistryStyleName = StylistWrapper.getChemistryStyleName(stylistDocument);


        // parse
        OpenDocumentToNoir.OdtHandler odtHandler = new OpenDocumentToNoir.OdtHandler(openDocumentToNoirStyle, odt.getPackage(), chemistryStyleName);
        DomSax.parse(odt.getContentRoot(), odtHandler);
        Document doc = odtHandler.doc;

        // save output
        Files.writeString(Path.of(outputFilename), XmlUtils.toString(doc));


    }

    static OdfElement createMathElement(OdfTextDocument odt, String elementName) throws IOException, SAXException {
        boolean namespaced = true;
        if (namespaced) {
            return odt.getContentDom().createElementNS("http://www.w3.org/1998/Math/MathML", "math:" + elementName);
            //return odt.getContentDom().createElement( "math:" + elementName);
        } else {
            return odt.getContentDom().createElement(elementName);
        }
    }

    /**
     * Only used for dev/testing
     *
     * @throws Exception
     */
    private static void testCreateExampleDocument(String odtFilename) throws Exception {
        // Create a text document from a standard template
        // (empty documents within the JAR)

        OdfTextDocument odt = OdfTextDocument.newTextDocument();

        if (false) {
            // Append text to the end of the document.
            String defaultParagraphStyleName;
            {
                OdfTextParagraph p = odt.newParagraph();
                defaultParagraphStyleName = p.getStyleName();
                p.setStyleName("Heading 1");
                odt.addText("This is a heading");
            }
            {
                OdfTextParagraph p = odt.newParagraph();
                p.setStyleName("Heading 2");
                odt.addText("This is a heading 2");
            }
            {
                OdfTextParagraph p = odt.newParagraph();
                p.setStyleName("Heading 3");
                odt.addText("This is a heading 3");
            }
        }
        {
            OdfTextParagraph p = odt.newParagraph();
            odt.addText("This is my very first ODF test");
        }
        {
            TextListElement ol0 = odt.getContentRoot().newTextListElement();
            ol0.setTextStyleNameAttribute("Numbering_20_1");
            {
                TextListItemElement li = ol0.newTextListItemElement();
                final TextPElement p = li.newTextPElement();
                p.setTextContent("contenu de l'item 0 de la liste ordonnée");
            }
            {
                TextListItemElement li = ol0.newTextListItemElement();
                final TextPElement p = li.newTextPElement();
                p.setTextContent("contenu de l'item 1 de la liste ordonnée");
            }
        }
        {
            TextListElement ul0 = odt.getContentRoot().newTextListElement();
            {
                TextListItemElement li = ul0.newTextListItemElement();
                final TextPElement p = li.newTextPElement();
                p.setTextContent("contenu de l'item 0 de la liste");
            }
            {
                TextListItemElement li = ul0.newTextListItemElement();
                final TextPElement p = li.newTextPElement();
                p.setTextContent("contenu de l'item 1 de la liste");
            }
            {
                TextListItemElement li = ul0.newTextListItemElement();
                TextListElement ul1 = li.newTextListElement();
                {
                    TextListItemElement li1 = ul1.newTextListItemElement();
                    final TextPElement p = li1.newTextPElement();
                    p.setTextContent("contenu de l'item a0 de la liste");
                }
                {
                    TextListItemElement li1 = ul1.newTextListItemElement();
                    final TextPElement p = li1.newTextPElement();
                    p.setTextContent("contenu de l'item a1 de la liste");
                }

            }
            {
                TextListItemElement li = ul0.newTextListItemElement();
                final TextPElement p = li.newTextPElement();
                p.setTextContent("contenu de l'item 2 de la liste");
            }
        }
        {
            TextPElement p = odt.getContentRoot().newTextPElement();
            TextSpanElement span0 = p.newTextSpanElement();
            span0.setTextContent("span0");
            TextSpanElement span1 = p.newTextSpanElement();
            span1.setTextContent("span1");

        }
        {
            TextHElement textHElement1 = odt.getContentRoot().newTextHElement(1);
            textHElement1.setTextContent("je suis un titre 1");
        }
        {
            TextHElement textHElement1 = odt.getContentRoot().newTextHElement(2);
            textHElement1.setTextContent("je suis un titre 2");
        }
        {
            TextHElement textHElement1 = odt.getContentRoot().newTextHElement(3);
            textHElement1.setTextContent("je suis un titre 3");
        }
        {
            TextPElement p = odt.getContentRoot().newTextPElement();

            /*String xlinkHrefValue = "";
            String xlinkTypeValue = "";
            //OdfDrawFrame odfDrawFrame =
*/
            //p.newDrawAElement(xlinkHrefValue,xlinkTypeValue);
            if (false) {
                DrawFrameElement drawFrameElement = p.newDrawFrameElement();
                DrawObjectElement drawObjectElement = drawFrameElement.newDrawObjectElement();
                drawObjectElement.setXlinkHrefAttribute("./Object 2");
                drawObjectElement.setXlinkTypeAttribute("simple");
                drawObjectElement.setXlinkShowAttribute("embed");
                drawObjectElement.setXlinkActuateAttribute("onLoad");
                //   OdfPackageDocument odfPackageDocument = new
                //         odt.getPackage().insertDocument(odfPackageDocument, "./Object 2/content.xml");
            } else {
                DrawFrameElement drawFrameElement = p.newDrawFrameElement();
                drawFrameElement.setDrawNameAttribute("Object1");

                DrawObjectElement drawObjectElement = drawFrameElement.newDrawObjectElement();

                {

                    // https://stackoverflow.com/questions/1492428/javadom-how-do-i-set-the-base-namespace-of-an-already-created-document
                    // https://stackoverflow.com/questions/28496282/creating-namespace-prefixed-xml-nodes-in-java-dom

                    //OdfElement math = odt.getContentDom().createElementNS("http://www.w3.org/1998/Math/MathML",MathMathElement.ELEMENT_NAME);
                    //OdfElement math = odt.getContentDom().createElementNS(MathMathElement.ELEMENT_NAME);
                    //OdfElement math = odt.getContentDom().createElementNS("http://www.w3.org/1998/Math/MathML","math:math");
                    //OdfElement math = odt.getContentDom().createElementNS("http://www.w3.org/1998/Math/MathML","math");
                    //   OdfElement math = odt.getContentDom().createElement("math");

                    // OdfElement math = odt.getContentDom().createElementNS("http://www.w3.org/1998/Math/MathML","math:math");
                    //math.setPrefix();
                    //OdfElement math = odt.getContentDom().createElementNS(MathMathElement.ELEMENT_NAME);
                    Element math = odt.getContentDom().createElementNS("http://www.w3.org/1998/Math/MathML", "math:math", "math");
                    //OdfElement math = odt.getContentDom().createElementNS("http://www.w3.org/1998/Math/MathML","math");
                    //math.setPrefix("math");


                    drawObjectElement.appendChild(math);
                    //MathMathElement math = drawObjectElement.newMathMathElement();

                    {
                        OdfElement semantics = createMathElement(odt, "semantics");
                        math.appendChild(semantics);
                        {
                            OdfElement mrow = createMathElement(odt, "mrow");
                            semantics.appendChild(mrow);
                            {
                                OdfElement mi = createMathElement(odt, "mi");
                                mrow.appendChild(mi);
                                mi.setTextContent("a");
                            }
                            {
                                OdfElement mo = createMathElement(odt, "mo");
                                mrow.appendChild(mo);
                                mo.setTextContent("+");
                            }
                            {
                                OdfElement msqrt = createMathElement(odt, "msqrt");
                                mrow.appendChild(msqrt);
                                {
                                    OdfElement mi = createMathElement(odt, "mi");
                                    msqrt.appendChild(mi);
                                    mi.setTextContent("b");
                                }
                            }
                            if (false) {
                                OdfElement annotation = createMathElement(odt, "annotation");
                                semantics.appendChild(annotation);
                                {
                                    annotation.setAttribute("encoding", "StarMath 5.0");
                                    annotation.setTextContent("a=sqrt(b)");
                                }
                            }
                        }
                    }
                }
            }


        }
        // Save document
        odt.save(odtFilename);

    }

    public static void main2(String[] args) throws Exception {

        {
            String outputFilename = "./natbraille-editor-server/to-lo-test/lo-test.odt";
            testCreateExampleDocument(outputFilename);
        }
        {
            String inputFilename = "./natbraille-editor-server/to-lo-test/to-lo-test.xml";
            String outputFilename = inputFilename + ".odt";
            testSourceToOpenDocument(inputFilename, outputFilename);
        }
        {
            String inputFilename = "./natbraille-editor-server/to-lo-test/to-lo-test.xml.odt";
            String outputFilename = inputFilename + ".xml";
            testOpenDocumentToSource(inputFilename, outputFilename);
        }
        {
            String inputFilename = "./natbraille-editor-server/to-lo-test/made-using-lowriter.odt";
            String outputFilename = inputFilename + ".xml";
            testOpenDocumentToSource(inputFilename, outputFilename);
        }
        /*
        {
            string inputfilename = "./natbraille-editor-server/to-lo-test/problems/problem.xml";
            string outputfilename = inputfilename + ".out.odt";
            testsourcetoopendocument(inputfilename, outputfilename);
        }
        */

    }

    public static void main3(String[] args) throws Exception {
        {
            String inputFilename = "./natbraille-editor-server/to-lo-test/mml.odt";
            String outputFilename = inputFilename + ".xml";
            testOpenDocumentToSource(inputFilename, outputFilename);
        }
        {
            String inputFilename = "./natbraille-editor-server/to-lo-test/lo-test-math-output.odt";
            String outputFilename = inputFilename + ".xml";
            testOpenDocumentToSource(inputFilename, outputFilename);
        }


    }

    public static void testCreateStylesInDocument(String odtFilename) throws Exception {

        OdfTextDocument odt = OdfTextDocument.newTextDocument();
        OfficeTextElement contentRoot = odt.getContentRoot();

        // remove all childs;
        while (contentRoot.hasChildNodes()) {
            contentRoot.removeChild(contentRoot.getFirstChild());
        }
        {

            OdfTextParagraph p = odt.newParagraph();
            System.out.println(p.getParentNode());
            System.out.println(odt.getContentRoot());
            TextSpanElement span = p.newTextSpanElement();
            span.setStyleName("chimix");
            span.setTextContent("la chimie");
        }

        OdfOfficeStyles stylesDocument = odt.getDocumentStyles();
        String xmlns_loext = "urn:org:documentfoundation:names:experimental:office:xmlns:loext:1.0";
        {

            OdfStyle chimix = stylesDocument.newStyle("chimix", OdfStyleFamily.Text);
            OdfStylePropertiesBase chimix_textProperties = chimix.getOrCreatePropertiesElement(OdfStylePropertiesSet.TextProperties);
            chimix_textProperties.setAttributeNS("fo", "fo:background-color", "#e8f2a1");
            chimix_textProperties.setAttributeNS(xmlns_loext, "loext:padding", "0.049cm");
            chimix_textProperties.setAttributeNS(xmlns_loext, "loext:border", "0.06pt solid #bf0041");

            /*
            chimix_textProperties.setAttributeNS(xmlns_loext, "loext:padding", "0.049cm");
            chimix_textProperties.setAttributeNS(xmlns_loext, "loext:border-left", "0.06pt solid #bf0041");
            chimix_textProperties.setAttributeNS(xmlns_loext, "loext:border-right", "0.06pt solid #bf0041");
            chimix_textProperties.setAttributeNS(xmlns_loext, "loext:border-top", "none");
            chimix_textProperties.setAttributeNS(xmlns_loext, "loext:border-bottom", "none");
            */

        }

        //chimix_textProperties.setAttribute("fo:background-color", "#e8f2a1");

        /*
        chimix.set
            <style:text-properties fo:background-color="#e8f2a1" loext:padding="0.049cm"
        loext:border="0.06pt solid #bf0041"/>
*/
        {
            odt.save(odtFilename);
            String odtXmlContentFilename = odtFilename + ".content.xml";
            String odtXmlStylesFilename = odtFilename + ".styles.xml";
            DebugUnzipOdt.readContentXml(Files.newInputStream(Path.of(odtFilename)), Files.newOutputStream(Path.of(odtXmlContentFilename)));
            DebugUnzipOdt.readStylesXml(Files.newInputStream(Path.of(odtFilename)), Files.newOutputStream(Path.of(odtXmlStylesFilename)));
        }
    }

    public static void testPre() throws Exception {

        String sourceXmlFilename = "./natbraille-editor-server/to-lo-test/pre/pre-input.xml";
        String destinationOdtFilename = sourceXmlFilename + ".odt";

        String noirXML = Files.readString(Path.of(sourceXmlFilename));
        byte[] odt = NoirToOpenDocument.toOpenDocument(noirXML, "chimie");
        {
            Files.write(Path.of(destinationOdtFilename), odt);
            String odtContentXmlFilename = destinationOdtFilename + ".content.xml";
            String odtXmlStylesFilename = destinationOdtFilename + ".styles.xml";
            DebugUnzipOdt.readContentXml(Files.newInputStream(Path.of(destinationOdtFilename)), Files.newOutputStream(Path.of(odtContentXmlFilename)));
            DebugUnzipOdt.readStylesXml(Files.newInputStream(Path.of(destinationOdtFilename)), Files.newOutputStream(Path.of(odtXmlStylesFilename)));
        }

        String renoirXmlFilename = destinationOdtFilename + ".xml";
        Document reXml = OpenDocumentToNoir.fromOdt(odt, "chimie");
        {
            Files.writeString(Path.of(renoirXmlFilename), XmlUtils.toString(reXml));
        }
    }

    public static void main(String[] args) throws Exception {
        /*{
            String outputFilename = "./natbraille-editor-server/to-lo-test/lo-test-math-output.odt";
            testCreateExampleDocument(outputFilename);
        }
         */
        if (false) {
            //   String inputFilename = "./natbraille-editor-server/to-lo-test/lo-test-math-output.odt";
            String inputFilename = "./natbraille-editor-server/to-lo-test/chimie/testChimie.odt";
            String outputFilename = inputFilename + ".xml";
            testOpenDocumentToSource(inputFilename, outputFilename);

            String noirXML = Files.readString(Path.of(outputFilename));
            String reOdtFilename = outputFilename + ".odt";
            byte[] odt = NoirToOpenDocument.toOpenDocument(noirXML, "chimie");
            Files.write(Path.of(reOdtFilename), odt);
            String reOdtContentXmlFilename = reOdtFilename + ".content.xml";
            DebugUnzipOdt.readContentXml(Files.newInputStream(Path.of(reOdtFilename)), Files.newOutputStream(Path.of(reOdtContentXmlFilename)));

        }
        //  testCreateStylesInDocument("/home/vivien/Bureau/nat/styles/add-styles.odt");
        testPre();
    }
}
