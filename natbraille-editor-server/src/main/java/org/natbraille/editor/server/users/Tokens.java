package org.natbraille.editor.server.users;

public interface Tokens {
    /**
     * persist livetokens in data source
     *
     * @param liveTokens liveTokens to persist
     */
    void persistTokens(LiveTokens liveTokens);

    /**
     * load live tokens in data source
     *
     * @param liveTokens liveTokens to write
     */
    void loadTokens(LiveTokens liveTokens);
}
