package org.natbraille.editor.server;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.eclipse.jetty.websocket.api.*;
import org.eclipse.jetty.websocket.api.annotations.*;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;

@WebSocket
public class TranscriptionSocket {

    private static class TranscriptionTask {
        public String transcriptionId;

        private static class Transcription {
            public String text;
        }

        public Transcription transcription;
    }

    private static class TranscriptionTaskResult {
        public TranscriptionTask transcriptionTask;
        public String result;
    }

    // Store sessions if you want to, for example, broadcast a message to all users
    private static final Queue<Session> sessions = new ConcurrentLinkedQueue<>();

    @OnWebSocketConnect
    public void connected(Session session) {
        sessions.add(session);
    }

    @OnWebSocketClose
    public void closed(Session session, int statusCode, String reason) {
        sessions.remove(session);
    }

    @OnWebSocketMessage
    public void message(Session session, String message) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        TranscriptionTask transcriptionTask = mapper.readValue(message, TranscriptionTask.class);

        System.out.println("Got: " + message);   // Print message
        System.out.println("Got: " + transcriptionTask.transcriptionId);   // Print message
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        TranscriptionTaskResult r = new TranscriptionTaskResult();
        r.result = transcriptionTask.transcription.text.toUpperCase();
        r.transcriptionTask = transcriptionTask;
        ;
        session.getRemote().sendString(mapper.writeValueAsString(r)); // and send it back


    }

}
