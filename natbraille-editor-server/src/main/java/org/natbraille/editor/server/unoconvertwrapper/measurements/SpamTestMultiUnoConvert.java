package org.natbraille.editor.server.unoconvertwrapper.measurements;

import org.natbraille.editor.server.unoconvertwrapper.Common;
import org.natbraille.editor.server.unoconvertwrapper.MultiUnoConvert;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutionException;

import static org.natbraille.editor.server.Server.serverConfiguration;

public class SpamTestMultiUnoConvert {

    // 1x unoserver -> overall avg:0,082050
    // 2x unoserver -> overall avg:0,042085
    // 3x unoserver -> overall avg:0,029253
    // 6x unoserver -> overall avg:0,018383

    public static void main(String[] args) throws InterruptedException, ExecutionException {

        System.out.println(serverConfiguration.unoServers);

        MultiUnoConvert multiUnoConvert = new MultiUnoConvert();
        multiUnoConvert.start();
        List<Thread> threads = new ArrayList<>();
        ConcurrentLinkedQueue<Double> durations = new ConcurrentLinkedQueue<>();
        ConcurrentLinkedQueue<Double> badDurations = new ConcurrentLinkedQueue<>();

        int tasksCount = 10000;
        long totalStartTime = System.nanoTime();
        for (int i = 0; i < tasksCount; i++) {
            final int ii = i;

            Thread t = new Thread(() -> {
                //try {
                String starmath5 = "%alpha <> %beta = " + ii;
                //public List<Double> successTimes = new ArrayList<>();
                long startTime = System.nanoTime();
                byte[] mmlBytes = multiUnoConvert.convertTo(Common.mmlSourceDocument(starmath5).getBytes(), Common.UnoDestinationFormats.MML);
                long endTime = System.nanoTime();
                double duration_s = (endTime - startTime) / 1_000_000_000.0;
                durations.add(duration_s);

                String mml = new String(mmlBytes, StandardCharsets.UTF_8);
                boolean ok = mml.contains("α") && mml.contains("≠") && mml.contains("β");
                if (!ok) {
                    badDurations.add(duration_s);
                    System.out.println("bad" + ii);
                }
                /*} catch (InterruptedException e) {
                    throw new RuntimeException(e);
                } catch (ExecutionException e) {
                    throw new RuntimeException(e);
                }*/
            });
            threads.add(t);
            t.start();
        }
        for (var t : threads) {
            t.join();
        }
        long totalEndTime = System.nanoTime();
        double total_duration_s = (totalEndTime - totalStartTime) / 1_000_000_000.0;


        System.out.println("done");
        for (var t : multiUnoConvert.unoServerWrapperRunnableThreads) {
            t.interrupt();
        }
        if (!badDurations.isEmpty()) {
            System.out.printf("BAD : %d\n", badDurations.size());
        } else {
            BasicStats basicStats = new BasicStats(durations.stream().toList());
            System.out.printf("min:%f max:%f avg:%f med:%f\n", basicStats.min, basicStats.max, basicStats.avg, basicStats.med);
            System.out.printf("task count:%d total time:%f\n", tasksCount, total_duration_s);
            double rate = total_duration_s / (double) tasksCount;
            System.out.printf("overall avg:%f \n", rate);
        }
    }
}
