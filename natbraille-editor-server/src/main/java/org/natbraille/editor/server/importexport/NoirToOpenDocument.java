package org.natbraille.editor.server.importexport;

import org.odftoolkit.odfdom.doc.OdfTextDocument;
import org.odftoolkit.odfdom.dom.element.OdfStylePropertiesBase;
import org.odftoolkit.odfdom.dom.element.draw.DrawFrameElement;
import org.odftoolkit.odfdom.dom.element.draw.DrawObjectElement;
import org.odftoolkit.odfdom.dom.element.office.OfficeTextElement;
import org.odftoolkit.odfdom.dom.element.text.*;
import org.odftoolkit.odfdom.dom.style.OdfStyleFamily;
import org.odftoolkit.odfdom.dom.style.props.OdfStylePropertiesSet;
import org.odftoolkit.odfdom.incubator.doc.office.OdfOfficeStyles;
import org.odftoolkit.odfdom.incubator.doc.style.OdfStyle;
import org.w3c.dom.Element;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Stack;

public class NoirToOpenDocument {
    /**
     * <html>
     * <body><p fragmentid="s159">Paragraphe normal 1</p>
     * <p fragmentid="s169">Paragraphe normal 2</p>
     * <h1 fragmentid="s161">Titre 1</h1>
     * <h2 fragmentid="s163">Titre 2</h2>
     * <ul>
     *     <li><p fragmentid="s164">liste a puce 1.a</p>
     *         <ul>
     *             <li><p fragmentid="s166">liste a puce 2.a</p></li>
     *             <li><p fragmentid="s167">liste a puce 2.b</p></li>
     *         </ul>
     *     </li>
     *     <li><p fragmentid="s168">liste a puce 1.b</p></li>
     * </ul>
     * </body>
     * </html>
     */
    protected static class ProseMirrorFormatHandler extends DefaultHandler {

        //
        // DefaultHandler non overloaded method are all noop;
        // (i.e.) no automatic DTDHandler or EntityResolver
        //
        private void newPElement() {
            newPElement("");
        }

        private void newPElement(String paragraphStyleName) {
            Object stackTop = stack.peek();
            if (stackTop instanceof OfficeTextElement) {
                stack.push(((OfficeTextElement) stackTop).newTextPElement());
            } else if (stackTop instanceof TextListItemElement) {
                stack.push(((TextListItemElement) stackTop).newTextPElement());
            } else {
                System.err.println("newTextPElement" + " inside " + stackTop.getClass().getName());
            }
            if (!paragraphStyleName.isEmpty()) {
                Object maybePElement = stack.peek();
                if (maybePElement instanceof TextPElement) {
                    ((TextPElement) maybePElement).setStyleName(paragraphStyleName);
                }
            }
        }

        private void newTextHElement(int level) {
            Object stackTop = stack.peek();
            if (stackTop instanceof OfficeTextElement) {
                stack.push(((OfficeTextElement) stackTop).newTextHElement(level));
            } else if (stackTop instanceof TextListItemElement) {
                stack.push(((TextListItemElement) stackTop).newTextHElement(level));
            } else {
                System.err.println("newTextHElement" + " inside " + stackTop.getClass().getName());
            }
        }

        private void newTextListElement(String ulol) {
            String styleName = switch (ulol) {
                case "ul" -> "List_20_1";
                case "ol" -> "Numbering_20_1";
                default -> "";
            };
            Object stackTop = stack.peek();
            if (stackTop instanceof OfficeTextElement) {
                TextListElement textListElement = ((OfficeTextElement) stackTop).newTextListElement();
                if (!styleName.isEmpty()) textListElement.setTextStyleNameAttribute(styleName);
                stack.push(textListElement);
            } else if (stackTop instanceof TextListItemElement) {
                TextListElement textListElement = ((TextListItemElement) stackTop).newTextListElement();
                if (!styleName.isEmpty()) textListElement.setTextStyleNameAttribute(styleName);
                stack.push(textListElement);
            } else {
                System.err.println("newTextListElement" + " inside " + stackTop.getClass().getName());
            }
        }

        private void newTextListItemElement() {
            Object stackTop = stack.peek();
            if (stackTop instanceof TextListElement) {
                stack.push(((TextListElement) stackTop).newTextListItemElement());
            } else {
                System.err.println("newTextListItemElement" + " inside " + stackTop.getClass().getName());
            }
        }

        private void newTextSpanElement() {
            Object stackTop = stack.peek();
            if (stackTop instanceof TextPElement) {
                stack.push(((TextPElement) stackTop).newTextSpanElement());
            } else if (stackTop instanceof TextHElement) {
                stack.push(((TextHElement) stackTop).newTextSpanElement());
            } else {
                System.err.println("newTextSpanElement" + " inside " + stackTop.getClass().getName());
            }
        }

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            // System.err.println("[" + stack.size() + "]" + "class of top: " + stack.peek().getClass().getName());
            // System.err.println("- ".repeat(stack.size() + 1) + "<" + qName + ">");
            switch (qName.toLowerCase()) {
                case "pre":
                    newPElement("pre-typeset-braille");
                    break;
                case "p":
                    newPElement();
                    break;
                case "h1":
                    newTextHElement(1);
                    break;
                case "h2":
                    newTextHElement(2);
                    break;
                case "h3":
                    newTextHElement(3);
                    break;
                case "h4":
                    newTextHElement(4);
                    break;
                case "h5":
                    newTextHElement(5);
                    break;
                case "h6":
                    newTextHElement(6);
                    break;
                case "ul":
                case "ol":
                    newTextListElement(qName.toLowerCase());
                    break;
                case "li":
                    newTextListItemElement();
                    break;
                case "math": {
                    // is it a chemistry equation
                    boolean isChemistry = ("true".equals(attributes.getValue("chemistry")));

                    // 0. create a span to be able to specify chimie style
                    // 1. create drawFrameElement/drawObject container
                    // 2. create namespaced math root and push to stack ; copy attributes
                    // 3. set inMathML flag to true
                    Object stackTop = stack.peek();
                    if ((stackTop instanceof TextPElement) || (stackTop instanceof TextHElement)) {

                        TextSpanElement textSpanElement;
                        if (stackTop instanceof TextPElement) {
                            textSpanElement = ((TextPElement) stackTop).newTextSpanElement();
                        } else {
                            textSpanElement = ((TextHElement) stackTop).newTextSpanElement();
                        }
                        if (isChemistry) {
                            textSpanElement.setTextStyleNameAttribute(this.chemistryStyleName);
                        }
                        DrawFrameElement drawFrameElement;
                        drawFrameElement = textSpanElement.newDrawFrameElement();
                        /*
                        if (stackTop instanceof TextPElement) {
                            drawFrameElement = ((TextPElement) stackTop).newDrawFrameElement();
                        } else {
                            drawFrameElement = ((TextHElement) stackTop).newDrawFrameElement();
                        }

 */
                        // if not  text:anchor-type="as-char", anchor would be paragraph so equation would appear at start of line
                        drawFrameElement.setTextAnchorTypeAttribute("as-char");
                        DrawObjectElement drawObjectElement = drawFrameElement.newDrawObjectElement();
                        try {
                            Element math = odt.getContentDom().createElementNS("http://www.w3.org/1998/Math/MathML", "math:math", "math");
                            // todo attributes for math + following elements
                            math.setAttribute("display", "block");
                            //math.setAttributeNS("http://www.w3.org/1998/Math/MathML","math:display","block");
                            //for (int i = 0 ; i < attributes.getLength() ; i++){
                            //       math.setAttributeNS("http://www.w3.org/1998/Math/MathML","math:"+attributes.getLocalName(i),attributes.getValue(i));
                            //}
                            drawObjectElement.appendChild(math);
                            stack.push(math);
                            inMathML = true;
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    } else {
                        System.err.println("new Math Element" + " inside " + stackTop.getClass().getName());
                    }

                }
                break;
                default: {
                    if (inMathML) {
                        // if inMathML flag is true, it means we are in a math expression, so we pass
                        // all incoming elements (mrow,mo,mi,etc.) as-is, keeping attributes, with a namespace
                        Object stackTop = stack.peek();
                        if (stackTop instanceof Element) {
                            try {
                                Element mathElement = odt.getContentDom().createElementNS("http://www.w3.org/1998/Math/MathML", "math:" + qName.toLowerCase());
                                for (int i = 0; i < attributes.getLength(); i++) {
                                    String attributeName = attributes.getQName(i);
                                    String attributeValue = attributes.getValue(i);
                                    mathElement.setAttributeNS("http://www.w3.org/1998/Math/MathML", "math:" + attributeName, attributeValue);
                                }
                                ((Element) stackTop).appendChild(mathElement);
                                stack.push(mathElement);
                            } catch (IOException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    } else {
                        // System.out.println("passing element" + qName.toLowerCase());
                    }
                }

            }
            // System.out.println("...");
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            // System.err.println("</" + qName + ">");
            switch (qName.toLowerCase()) {
                case "pre":
                case "p":
                case "h1":
                case "h2":
                case "h3":
                case "h4":
                case "h5":
                case "h6":
                case "ul":
                case "ol":
                case "li":
                    stack.pop();
                    break;
                case "math":
                    inMathML = false;
                    stack.pop();
                    break;
                default: {
                    if (inMathML) {
                        stack.pop();
                    }
                }
            }
        }

        @Override
        public void characters(char[] ch, int start, int length) throws SAXException {
            String text = String.valueOf(ch, start, length);
            // System.err.println("characters[" + text + ']');
            if (inMathML) {
                // directly write text content
                Element stackTop = (Element) (stack.peek());
                String trimmed = text.trim();
                if (!trimmed.isEmpty()) {
                    stackTop.setTextContent(trimmed);
                }
            } else {
                // create a text span around text content
                boolean parentIsTextPOrTextH = (stack.peek() instanceof TextPElement) || (stack.peek() instanceof TextHElement);
                if (parentIsTextPOrTextH) {
                    newTextSpanElement();
                    TextSpanElement stackTop = (TextSpanElement) (stack.peek());
                    stackTop.setTextContent(text);
                    stack.pop();
                }
            }
        }

        public final OdfTextDocument odt;

        private final Stack<Object> stack = new Stack<>();
        private boolean inMathML = false;
        private final String chemistryStyleName;

        public ProseMirrorFormatHandler(String chemistryStyleName) throws Exception {
            this.chemistryStyleName = chemistryStyleName;
            odt = createEmptyDocument();
            stack.push(odt.getContentRoot());
        }
    }

    public static OdfTextDocument createEmptyDocument() throws Exception {
        OdfTextDocument odt = OdfTextDocument.newTextDocument();
        OfficeTextElement contentRoot = odt.getContentRoot();

        // remove all childs;
        while (contentRoot.hasChildNodes()) {
            contentRoot.removeChild(contentRoot.getFirstChild());
        }

        // create styles
        OdfOfficeStyles stylesDocument = odt.getDocumentStyles();
        String xmlns_loext = "urn:org:documentfoundation:names:experimental:office:xmlns:loext:1.0";
        String xmlns_style = "urn:oasis:names:tc:opendocument:xmlns:style:1.0";
        {
            OdfStyle style = stylesDocument.newStyle("pre-typeset-braille", OdfStyleFamily.Paragraph);
            {
                OdfStylePropertiesBase paragraphProperties = style.getOrCreatePropertiesElement(OdfStylePropertiesSet.ParagraphProperties);
                {
                    paragraphProperties.setAttributeNS(xmlns_style, "style:contextual-spacing", "false");
                }
            }
            {
                OdfStylePropertiesBase textProperties = style.getOrCreatePropertiesElement(OdfStylePropertiesSet.TextProperties);
                {
                    textProperties.setAttributeNS("fo", "fo:background-color", "#e8f2a1");
                    textProperties.setAttributeNS(xmlns_loext, "loext:padding", "0.049cm");
                    textProperties.setAttributeNS(xmlns_loext, "loext:border", "0.06pt solid #bf0041");
                    textProperties.setAttributeNS(xmlns_style, "style:font-pitch", "fixed");
                }
            }


            return odt;
        }
    }

    public static byte[] toOpenDocument(String xmlString, String chemistryStyleName) throws Exception {
        // build sax parser
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser saxParser = factory.newSAXParser();
        final ProseMirrorFormatHandler proseMirrorFormatHandler = new ProseMirrorFormatHandler(chemistryStyleName);
        // build input source
        InputSource inputSource = new InputSource(new java.io.StringReader(xmlString));
        // parse
        saxParser.parse(inputSource, proseMirrorFormatHandler);
        // save to byte array
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        proseMirrorFormatHandler.odt.save(byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }


}
