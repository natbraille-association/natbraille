package org.natbraille.editor.server.tools;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

import static org.natbraille.editor.server.tools.URIEncoder.decodeURIComponent;
import static org.natbraille.editor.server.tools.URIEncoder.encodeURIComponent;

/**
 * Encode And Decode any utf-8 string to be passed as url part
 */
public class B64URIComponent {
    public static String encode(String value) {
        if (value == null) return null;
        String urlEncoded = encodeURIComponent(value);
        return Base64.getEncoder().encodeToString(urlEncoded.getBytes());
    }
    public static String decode(String value) {
        if (value == null) return null;
        byte[] b64decoded = Base64.getDecoder().decode(value);
        String s = new String(b64decoded, StandardCharsets.UTF_8);
        return decodeURIComponent(s);
    }
}
