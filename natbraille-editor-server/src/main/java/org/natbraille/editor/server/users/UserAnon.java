package org.natbraille.editor.server.users;

import java.util.Optional;

public class UserAnon implements Users {

    public boolean addUser(String username, String plainTextPassword) {
        return true;
    }

    @Override
    public boolean removeUser(String username) {
        return false;
    }

    @Override
    public boolean userPasswordMatch(String username, String plainTextPassword) {
        return true;
    }

    @Override
    public boolean addUserRegistration(String username, String rawPassword, String email) {
        return true;
    }

    @Override
    public boolean setUserRegistrationChallenge(String username, String rawChallenge) {
        return true;
    }

    @Override
    public boolean removeRegistration(String username) {
        return true;
    }

    @Override
    public boolean addRegisteredUser(String username) {
        return true;
    }

    @Override
    public boolean registrationChallengeMatch(String username, String rawChallenge) {
        return true;
    }

    @Override
    public Optional<String> getEmailForUsername(String username) {
        return Optional.of("example@example.com");
    }

    @Override
    public boolean setUserPasswordResetRequestChallenge(String username, String rawChallenge) {
        return true;
    }
    @Override
    public boolean resetUserPassword(String username, String rawChallenge, String rawPassword) {
        return true;
    }

    @Override
    public boolean setAccountRemovalRequestChallenge(String username, String rawChallenge) {
        return true;
    }

    @Override
    public boolean accountRemovalChallengeMatch(String username, String rawChallenge) {
        return true;
    }
}
