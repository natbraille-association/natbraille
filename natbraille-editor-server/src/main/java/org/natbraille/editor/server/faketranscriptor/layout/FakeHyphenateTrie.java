package org.natbraille.editor.server.faketranscriptor.layout;

import org.natbraille.editor.server.faketranscriptor.FakeTranscription;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.natbraille.editor.server.faketranscriptor.layout.FakeHyphenate.dbgWord;

/**
 * Implements word hyphenation using (something close to) Liang algorithm
 * see https://tug.org/tugboat/tb27-1/tb86nemeth.pdf
 * see hyphenation.xsl
 */
public class FakeHyphenateTrie {

    /**
     * Trie structure to store hyphenation dictionary
     * @param <ValueType>
     */
    static class Trie<ValueType> {
        Map<Character, Trie<ValueType>> children = new HashMap<>();
        List<ValueType> leafs = new ArrayList<>();

        void insert(String pattern, ValueType value) {
            insert(pattern, 0, value);
        }

        void insert(String pattern, int index, ValueType value) {
            if (index == pattern.length()) {
                leafs.add(value);
            } else {
                Character head = pattern.charAt(index);
                Trie<ValueType> child;
                if (children.containsKey(head)) {
                    child = children.get(head);
                } else {
                    child = new Trie<>();
                    children.put(head, child);
                }
                child.insert(pattern, index + 1, value);
            }
        }

        void find(String pattern, int index, List<ValueType> accumulator) {
            accumulator.addAll(leafs);
            if (index < pattern.length()) {
                Character head = pattern.charAt(index);
                if (children.containsKey(head)) {
                    children.get(head).find(pattern, index + 1, accumulator);
                }
            }
        }

        List<ValueType> find(String pattern) {
            List<ValueType> accumulator = new ArrayList<>();
            find(pattern, 0, accumulator);
            return accumulator;
        }
    }

    /**
     * Compute hyphenation score as in Lliang algorithm
     * @param hyphenationPatterns the hyphenation dictionary
     * @param word the word to hyphenate
     * @return hyphenation score for each position
     */
    static int[] hyphenateWord(Trie<FakeHyphenate.HyphenationPattern> hyphenationPatterns, String word) {
        String boundedWord = "." + word.toLowerCase() + ".";
        int levels[] = new int[boundedWord.length()];

        for (int i = 0; i < boundedWord.length(); i++) {
            for (FakeHyphenate.HyphenationPattern hyphenationPattern : hyphenationPatterns.find(boundedWord.substring(i))) {
                for (int j = 0; j < hyphenationPattern.level.length; j++)
                    levels[i + j] = Math.max(levels[i + j], hyphenationPattern.level[j]);
            }
        }
        return Arrays.copyOfRange(levels, 2, levels.length - 1);
    }

    /**
     *
     * @param hyphenationPatterns the hyphenation dictionary
     * @param word  the word to hyphenate
     * @param minLetterCountBeforeBreak minimum letter count limit before a hyphenation
     * @param minLetterCountAfterBreak minimum letter count limit after a hyphenation
     * @return hyphenation score for each position inside the limit range
     */
    static int[] hyphenateWord(Trie<FakeHyphenate.HyphenationPattern> hyphenationPatterns, String word, int minLetterCountBeforeBreak, int minLetterCountAfterBreak) {
        int[] levels = hyphenateWord(hyphenationPatterns, word);
        Arrays.fill(levels, 0, Math.max(0, Math.min(minLetterCountBeforeBreak - 1, levels.length)), 0);
        Arrays.fill(levels, Math.max(0, Math.min(levels.length - 1, levels.length - (minLetterCountAfterBreak - 1))), levels.length, 0);
        return levels;
    }

    /**
     * Parse hyphenation file data in libreoffice format
     * @param is a stream to the data
     * @return a list of parsed hyphenation patterns
     * @throws IOException
     * @throws NullPointerException
     */
    static List<FakeHyphenate.HyphenationPattern> parseHyphens(InputStream is) throws IOException, NullPointerException {
        String s = new String(is.readAllBytes(), StandardCharsets.UTF_8);
        String[] a = s.replaceAll("^UTF-8\n", "").trim().split("\n");
        return Arrays.stream(a).map(FakeHyphenate.HyphenationPattern::new).collect(Collectors.toList());
    }

    /**
     * Fill a Trie structure with hyphenation patterns
     * @param patterns list of parsed hyphenation patterns
     * @return Trie structure storing the hyphenation dictionary
     */
    static Trie<FakeHyphenate.HyphenationPattern> buildTrie(List<FakeHyphenate.HyphenationPattern> patterns) {
        Trie<FakeHyphenate.HyphenationPattern> trie = new Trie();
        for (FakeHyphenate.HyphenationPattern hyphenationPattern : patterns) {
            trie.insert(hyphenationPattern.matchPattern, hyphenationPattern);
        }
        return trie;
    }

    /**
     * Build a word hyphenation function from hyphenation file data in libreoffice format
     * @param is a stream to the data
     * @param minLetterCountBeforeBreak minimum letter count limit before a hyphenation
     * @param minLetterCountAfterBreak minimum letter count limit after a hyphenation
     * @return a word hyphenation function
     * @throws IOException
     */
    public static Function<String, int[]> hyphenate1(InputStream is, int minLetterCountBeforeBreak, int minLetterCountAfterBreak) throws IOException {
        Trie<FakeHyphenate.HyphenationPattern> trie = buildTrie(parseHyphens(is));
        return (String word) -> hyphenateWord(trie, word, minLetterCountBeforeBreak, minLetterCountAfterBreak);
    }

    /**
     * Used only for execution time measurement purpose
     */
    private static class Measurement {
        public static void main(String[] args) throws IOException {

            // without prefix tree
            // oneWordTime 26.856804266572ns

            // String.subString
            //t0.444
            //totalTime 3676
            //oneWordTime 1.616676920093596ns

            // index
            //t0.299
            //totalTime 3115
            //oneWordTime 1.3699533883482218ns

            InputStream is = FakeTranscription.class.getClassLoader().getResourceAsStream("org/natbraille/core/xsl/dicts/hyph_fr.dic");
            List<FakeHyphenate.HyphenationPattern> a = parseHyphens(is);
            long beginTime0 = System.currentTimeMillis();
            Trie<FakeHyphenate.HyphenationPattern> trie = null;
            for (int i = 0; i < 1000; i++) {
                trie = buildTrie(a);
            }
            long endTime0 = System.currentTimeMillis();
            System.out.println("t" + (((float) (endTime0 - beginTime0)) / 1000.0));
            String word = "conversationnel";
            hyphenateWord(trie, word, 2, 2);

            {
                String[] words0 = Files.readString(Path.of("./natbraille-editor-server/test/fr-wordlist.txt")).split("\n");
                StringBuilder sb0 = new StringBuilder();
                for (String word0 : words0) {
                    int levels0[] = hyphenateWord(trie, word0, 2, 2);
                    sb0.append(dbgWord(word0, levels0));
                    sb0.append("\n");
                }
                Files.writeString(Path.of("./natbraille-editor-server/test/r/hyph-resu-trie.txt"), sb0.toString());
            }

            {
                String[] wordList = Files.readString(Path.of("./natbraille-editor-server/test/fr-wordlist.txt")).split("\n");
                int nRepeat = 100;
                long beginTime = System.currentTimeMillis();
                for (int i = 0; i < nRepeat; i++) {
                    for (String word2 : wordList) {
                        hyphenateWord(trie, word2, 2, 2);
                    }
                }
                long endTime = System.currentTimeMillis();
                long totalTime = endTime - beginTime;
                float oneWordTime = ((float) totalTime) / (((float) nRepeat) * ((float) (wordList.length)));
                System.out.println("totalTime " + totalTime);
                System.out.println("oneWordTime " + (1000.0 * oneWordTime) + "ns");
            }


        /*
        Trie<HyphenationPattern> trie = new Trie();
        for (HyphenationPattern hyphenationPattern : Arrays.stream(a).map(HyphenationPattern::new).collect(Collectors.toList())) {
            trie.insert(hyphenationPattern.matchPattern, hyphenationPattern);
        }
        {
            String word = "conversationnel";
            hyphenateWord(trie,word,2,2);
        }*/
        }
    }
}
