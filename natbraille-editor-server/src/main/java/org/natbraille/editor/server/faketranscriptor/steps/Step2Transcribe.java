package org.natbraille.editor.server.faketranscriptor.steps;

import org.natbraille.core.filter.NatFilterException;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.editor.server.faketranscriptor.FakeTranscription;
import org.natbraille.editor.server.faketranscriptor.Tag;
import org.natbraille.editor.server.faketranscriptor.XmlUtils;
import org.natbraille.editor.server.faketranscriptor.g2.G2;
import org.natbraille.editor.server.faketranscriptor.math.MathTranslator;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.xpath.XPathExpressionException;
import java.util.*;
import java.util.stream.Collectors;

import static org.natbraille.editor.server.faketranscriptor.FakeTranscription.*;
import static org.natbraille.editor.server.faketranscriptor.steps.Step0DivideText.*;

public class Step2Transcribe {

    static MathTranslator.NatbrailleMathTranslators natbrailleMathTranslators = new MathTranslator.NatbrailleMathTranslators();

    static boolean nodeIsElementNamed(Node node, String name) {
        return ((node != null) && (node.getNodeType() == Node.ELEMENT_NODE) && node.getNodeName().equals(name));
    }

    static boolean nodeIsElementNamedOneOf(Node node, String... names) {
        if (node != null && node.getNodeType() == Node.ELEMENT_NODE) {
            String nodeName = node.getNodeName();
            for (String name : names) {
                if (nodeName.equals(name)) {
                    return true;
                }
            }
        }
        return false;
    }


    public static void step2_translate_remaining_spaces(Element block, FakeTranscription ft) throws XPathExpressionException {
        Document inputDoc = block.getOwnerDocument();

        Set<Element> spacings = new HashSet<>();
        Set<Element> spacingAtEnd = new HashSet<>();
        Set<Element> spacingAtStart = new HashSet<>();
        // ft.xpathNodesIt : 12.100ms
        // 10ms -> 17%
        Node maybeSpacing = block.getFirstChild();
        while (maybeSpacing != null) {
            if ("spacing".equals(maybeSpacing.getNodeName())) {
                Element spacing = (Element) maybeSpacing;
                if (!spacing.hasAttribute("source")) {
                    //for (Node spacing : ft.xpathNodesIt(block, "spacing[not(@source)]")) {
                    spacings.add((Element) spacing);
                    if (spacing.getPreviousSibling() == null) {
                        spacingAtStart.add((Element) spacing);
                    } else if (spacing.getNextSibling() == null) {
                        spacingAtEnd.add((Element) spacing);
                    }
                }
            }
            maybeSpacing = maybeSpacing.getNextSibling();
        }
        for (Element spacing : spacings) {
            Element translationPair = FakeTranscription.replaceByTranslationPair((Element) spacing, true);
            Element braille = FakeTranscription.getBraille(translationPair); // empty
            if (spacingAtStart.contains(spacing)) {
                braille.setAttribute("extraneous-space-cause", "at-start");
            } else if (spacingAtEnd.contains(spacing)) {
                braille.setAttribute("extraneous-space-cause", "at-end");
            } else {
                braille.setTextContent(FakeTranscription.BrailleBlank);
            }
        }
    }

    public static void step2_translate_punctuation(Element block, FakeTranscription ft) throws XPathExpressionException {
        Document inputDoc = block.getOwnerDocument();
        Set<Element> spacingBeforePunctuation = new HashSet<>();

        // get all <space>s having a <punctuation> after
        Node maybeSpacing = block.getFirstChild();
        while (maybeSpacing != null) {
            if ("spacing".equals(maybeSpacing.getNodeName())) {
                Element spacing = (Element) maybeSpacing;
                Node maybePunctuation = spacing.getNextSibling();
                if ((maybePunctuation != null) && ("punctuation".equals(maybePunctuation.getNodeName()))) {
                    spacingBeforePunctuation.add((Element) spacing);
                }
            }
            maybeSpacing = maybeSpacing.getNextSibling();
        }

        // set translation pair of such <space>s as empty, because braille admits no space before punctuation.
        for (Element spacing : spacingBeforePunctuation) {
            Element translationPair = FakeTranscription.replaceByTranslationPair((Element) spacing, true);
            Element braille = FakeTranscription.getBraille(translationPair); // empty
            braille.setAttribute("extraneous-space-cause", "before-punctuation");
        }

        Node maybePunctuation = block.getFirstChild();
        while (maybePunctuation != null) {
            if ("punctuation".equals(maybePunctuation.getNodeName())) {
                Element punctuation = (Element) maybePunctuation;
                String punctuationString = punctuation.getTextContent();

                // check if punctuation is a dialog start mark
                boolean isDialogStartMark = ((punctuation.getPreviousSibling() == null) && ("-".equals(punctuationString) || "—".equals(punctuationString)));

                Element translationPair = FakeTranscription.replaceByTranslationPair(punctuation, true);
                Element braille = FakeTranscription.getBraille(translationPair);

                if (isDialogStartMark) {
                    braille.setTextContent("--");
                    braille.setAttribute("cadratin-cause", "start-of-block");
                } else {
                    braille.setTextContent(punctuationString);
                }
                // current element has been substituted
                maybePunctuation = translationPair;
            }
            maybePunctuation = maybePunctuation.getNextSibling();
        }

    }

    public static void step2_translate_numbers(Element block, FakeTranscription ft) throws XPathExpressionException {
        Document inputDoc = block.getOwnerDocument();

        // get spaces between numbers
        Map<Element, String> spacingBetweenNumbers = new HashMap<>();
        Set<Element> tailNumbersInASpaceSeparatedSequence = new HashSet<>();
        Set<Element> headNumberInASpaceSeparatedSequence = new HashSet<>();
        //String spacingBetweenNumberXpath = "spacing" + "[name(preceding-sibling::*[1])='number']" + "[name(following-sibling::*[1])='number']";
        // xpathNodesIt 13.160
        // for (Node spacing : ft.xpathNodesIt(block, spacingBetweenNumberXpath)) {
        // sans 10
        // gain 25%
        Node n = block.getFirstChild();
        while (n != null) {
            if (n.getNodeName().equals("spacing")) {
                Element spacing = (Element) n;
                // gains 2%
                Element precedingNumber = (Element) spacing.getPreviousSibling();
                Element followingNumber = (Element) spacing.getNextSibling();
                if ((precedingNumber != null)
                        && (followingNumber != null)
                        && ("number".equals(precedingNumber.getTagName()))
                        && ("number".equals(followingNumber.getTagName()))) {
                    //Element followingNumber = (Element) ft.xpathNode(spacing, "following-sibling::*[1]");
                    //Element precedingNumber = (Element) ft.xpathNode(spacing, "preceding-sibling::*[1]");
                    String fragmentIdList = precedingNumber.getAttribute("fragmentId") + "," + followingNumber.getAttribute("fragmentId");
                    spacingBetweenNumbers.put((Element) spacing, fragmentIdList);
                    tailNumbersInASpaceSeparatedSequence.add((Element) followingNumber);
                }
            }
            n = n.getNextSibling();
        }

        // ft.xpathNodesIt 12.290
        // gain 20%
        n = block.getFirstChild();

        //if (false)
        while (n != null) {
            //for (Node number : ft.xpathNodesIt(block, "number")) {
            if ("number".equals(n.getNodeName())) {

                Element number = (Element) n;
                Element translationPair = FakeTranscription.replaceByTranslationPair((Element) number, true);
                Element braille = FakeTranscription.getBraille(translationPair);
                if (tailNumbersInASpaceSeparatedSequence.contains(number)) {
                } else {
                    Element prefix = inputDoc.createElement(Tag.number_prefix.tagName);
                    prefix.setTextContent(FakeTranscription.NumberPrefix);
                    braille.appendChild(prefix);
                }
                Element digits = inputDoc.createElement(Tag.digits.tagName);
                digits.setTextContent(transliterateToBraille(number.getTextContent()));
                braille.appendChild(digits);

                // current element has been substituted
                n = translationPair;
            }
            n = n.getNextSibling();
        }

        for (Element spacing : spacingBetweenNumbers.keySet()) {
            Element translationPair = FakeTranscription.replaceByTranslationPair((Element) spacing, true);
            Element braille = FakeTranscription.getBraille(translationPair);
            Element prefix = inputDoc.createElement(Tag.number_separator.tagName);
            prefix.setAttribute("number-separated-list", spacingBetweenNumbers.get(spacing));
            prefix.setTextContent(FakeTranscription.NumberSeparator);
            braille.appendChild(prefix);
        }

    }

    public static void step2_translate_words_emphasis(Element block, FakeTranscription ft) throws XPathExpressionException {

        Document inputDoc = block.getOwnerDocument();

        // build groups of sub-words (named realWords) and determine if they are
        // composed of sub-words all being emphasized.

        List<List<Element>> realWords = new ArrayList<>();
        List<Boolean> realWordsFullyEmphasized = new ArrayList<>();
        Element lastRealWordStart = null;
        boolean lastRealWordFullyEmphasized = false;
        Node maybeWord = block.getFirstChild();
        while (maybeWord != null) {
            if (maybeWord.getNodeType() != Node.ELEMENT_NODE) continue;
            Element element = (Element) maybeWord;
            // TODO : punctuation and numbers in emphasis
            // will be if tag is the same than the previous one with no separator in between.
            if (element.getNodeName().equals(Tag.word.tagName)) {
                Element word = element;
                String text = word.getTextContent();
                boolean startOfRealWord = false;
                if (lastRealWordStart == null) {
                    startOfRealWord = true;
                    lastRealWordStart = word;
                    realWords.add(new ArrayList<>());
                    realWordsFullyEmphasized.add(false);
                }
                realWords.get(realWords.size() - 1).add(word);
                //if (element.hasAttribute(BRAILLE_EMPHASIS_ATTRIBUTE)) {
                String emphasisLevel = word.getAttribute(BRAILLE_EMPHASIS_ATTRIBUTE);
                if (startOfRealWord) {
                    // the first element has emphasis
                    lastRealWordFullyEmphasized = BRAILLE_EMPHASIS_VALUE_STANDARD.equals(emphasisLevel);
                } else {
                    // this element and all previous have emphasis
                    lastRealWordFullyEmphasized = lastRealWordFullyEmphasized && BRAILLE_EMPHASIS_VALUE_STANDARD.equals(emphasisLevel);
                }
                //}
                realWordsFullyEmphasized.set(realWords.size() - 1, lastRealWordFullyEmphasized);
            } else {
                // TODO: it cuts words on everything for now
                lastRealWordStart = null;
                lastRealWordFullyEmphasized = false;
            }
            maybeWord = maybeWord.getNextSibling();
        }
        // System.out.println("===============================");
        // count sequence of fully emphasized words

        //System.out.println("===============================");
        List<List<List<Element>>> realWordsEmphasizedSequences = new ArrayList<>();
        for (int i = 1; i < realWords.size(); i++) {
            //  System.out.println(">> "+i);
            List<Element> realWord0 = realWords.get(i - 1);
            List<Element> realWord1 = realWords.get(i);
            boolean realWordIsFullyEmphasized0 = realWordsFullyEmphasized.get(i - 1);
            boolean realWordIsFullyEmphasized1 = realWordsFullyEmphasized.get(i);
            // System.out.println("realWordIsFullyEmphasized0 "+realWordIsFullyEmphasized0+" "+realWord0.stream().map(Node::getTextContent).collect(Collectors.joining("|")));
            // System.out.println("realWordIsFullyEmphasized1 "+realWordIsFullyEmphasized1+" "+realWord0.stream().map(Node::getTextContent).collect(Collectors.joining("|")));
            boolean isASequence = false;
            if (realWordIsFullyEmphasized0 && realWordIsFullyEmphasized1) {
                // what is between those fully emphasized word parts
                Element from = realWord0.get(realWord0.size() - 1);
                Element to = realWord1.get(0);
                //System.out.println(i+"* from "+from.getTextContent());
                Node n = from.getNextSibling();
                boolean notASequence = false;
                while ((n != null) && (n != to) && (!notASequence)) {
                    if (n.getNodeType() == Node.ELEMENT_NODE) {
                        Element between = (Element) n;
                        //System.out.println(">"+between.getTagName());
                        // not a sequence if separated by a punctuation
                        notASequence = between.getTagName().equals(Tag.punctuation.tagName);
                    }
                    n = n.getNextSibling();
                }
                isASequence = !notASequence;
                //System.out.println(i+"* to   "+to.getTextContent());
            }
            if (isASequence) {
                if (realWordsEmphasizedSequences.isEmpty()) {
                    realWordsEmphasizedSequences.add(new ArrayList<>());
                }
                List<List<Element>> sequence = realWordsEmphasizedSequences.get(realWordsEmphasizedSequences.size() - 1);
                if (sequence.isEmpty()) {
                    sequence.add(realWord0);
                }
                sequence.add(realWord1);
            } else {
                realWordsEmphasizedSequences.add(new ArrayList<>());
            }
        }
        Set<List<Element>> realWordInFullyEmphasizedSequence = new HashSet<>();
        //System.out.println("<<<<<<<<<<<<<<<<<<<<START>>>>>>>>>>>>>>>>>>>>");
        for (List<List<Element>> sequence : realWordsEmphasizedSequences) {
            int sequenceLength = sequence.size();
            if (sequenceLength >= 4) {
                // add to the set in order to mark the word parts as already treated.
                realWordInFullyEmphasizedSequence.addAll(sequence);
                // prefix the first word part
                {
                    Element firstWordPart = sequence.get(0).get(0);
                    Element braille = getBraille(firstWordPart);
                    {
                        Element emphasisPrefix = inputDoc.createElement(Tag.emphasisPrefix.tagName);
                        emphasisPrefix.setAttribute("em-type", "sequence-start");
                        emphasisPrefix.setTextContent(fourSuccessiveEmphasisWordsFirstWordPrefix);
                        braille.insertBefore(emphasisPrefix, braille.getFirstChild());
                    }
                }
                // prefix before the last word part
                {
                    List<Element> lastRealWord = sequence.get(sequence.size() - 1);
                    Element firstWordPart = lastRealWord.get(0);
                    Element braille = getBraille(firstWordPart);
                    {
                        Element emphasisPrefix = inputDoc.createElement(Tag.emphasisPrefix.tagName);
                        emphasisPrefix.setAttribute("em-type", "sequence-end");
                        emphasisPrefix.setTextContent(fourSuccessiveEmphasisWordsLastWordPrefix);
                        braille.insertBefore(emphasisPrefix, braille.getFirstChild());
                    }
                }
            }
            /*
            System.out.println("sequence length (in full word) is "+sequenceLength);
            String result = sequence.stream()
                    .flatMap(List::stream) // Flatten the nested list
                    .map(Element::getTextContent) // Extract textContent
                    .collect(Collectors.joining("|")); // Join with "|"
            System.out.println(result);

             */
        }
        //System.out.println("<<<<<<<<<<<<<<<<<<<<FIN>>>>>>>>>>>>>>>>>>>>");
        //System.out.println("===============================");
        //
        // add the emphasis prefix and suffix.
        //
        for (int i = 0; i < realWords.size(); i++) {


            List<Element> realWord = realWords.get(i);

            // the real word is already in a sequence ?
            if (realWordInFullyEmphasizedSequence.contains(realWord))
                continue;

            boolean realWordIsAllEmphasized = realWordsFullyEmphasized.get(i);

            //
            //  System.out.println("\n<----------->" + "all word emphasis?" + realWordIsAllEmphasized);
            //for (var w : realWord) {
//                System.out.println(w.getTextContent() + "---word part emphasis?" + w.getAttribute(BRAILLE_EMPHASIS_ATTRIBUTE));
            //          }
            //
            if (realWordIsAllEmphasized) {
                // a whole word emphasized
                Element firstWordPart = realWord.get(0);
                Element braille = getBraille(firstWordPart);
                {
                    Element emphasisPrefix = inputDoc.createElement(Tag.emphasisPrefix.tagName);
                    emphasisPrefix.setAttribute("em-type", "word");
                    emphasisPrefix.setTextContent(wholeWordEmphasisPrefix);
                    braille.insertBefore(emphasisPrefix, braille.getFirstChild());

                }
            } else {
                // part of word emphasized (or none)
                String previousEmphasisLevel = "";
                //System.out.println("<PARTIALLY>");
                for (int j = 0; j < realWord.size(); j++) {
                    Element subWord = realWord.get(j);
                    String emphasisLevel = subWord.getAttribute(BRAILLE_EMPHASIS_ATTRIBUTE);
                    //System.out.println("- "+j+"["+emphasisLevel+"]"+subWord.getTextContent());
                    if (BRAILLE_EMPHASIS_VALUE_STANDARD.equals(emphasisLevel)) {
                        //System.out.println("word-part-start?");
                        if (previousEmphasisLevel.isEmpty()) {
                            // System.out.println("word-part-start");
                            // start of subword emphasis
                            Element braille = getBraille(subWord);
                            Element emphasisPrefix = inputDoc.createElement(Tag.emphasisPrefix.tagName);
                            emphasisPrefix.setAttribute("em-type", "word-part-start");
                            emphasisPrefix.setTextContent(partialWordEmphasisStartPrefix);
                            braille.insertBefore(emphasisPrefix, braille.getFirstChild());
                        }
                    } else if (BRAILLE_EMPHASIS_VALUE_STANDARD.equals(previousEmphasisLevel)) {
                        // System.out.println("word-part-end");
                        // subword is not emphasized and previous was
                        Element braille = getBraille(subWord);
                        Element emphasisPrefix = inputDoc.createElement(Tag.emphasisPrefix.tagName);
                        emphasisPrefix.setAttribute("em-type", "word-part-end");
                        emphasisPrefix.setTextContent(partialWordEmphasisEndPrefix);
                        braille.insertBefore(emphasisPrefix, braille.getFirstChild());
                    }
                    previousEmphasisLevel = emphasisLevel;
                }
                //System.out.println("</PARTIALLY>");
            }


        }


    }

    public static boolean isBicaseUppercase(String letters){
        // the letter is uppercase and a lowercase variant do exist.
        return letters.equals(letters.toUpperCase()) && (!letters.toUpperCase().equals(letters.toLowerCase()));
    }
    public static void step2_translate_words(Element block, FakeTranscription ft) throws XPathExpressionException {

        Document inputDoc = block.getOwnerDocument();

        //
        // build list of successive word (1+ length) fully in uppercase
        //
        List<List<Element>> successiveUcWordsLists = new ArrayList<>();
        List<Element> partialOrNoUcWords = new ArrayList<>();

        boolean previousIsUc = false;
        Node maybeWord = block.getFirstChild();
        while (maybeWord != null) {
            //for (Node word : ft.xpathNodesIt(block, "word")) {
            String nodeName = maybeWord.getNodeName();
            if ("word".equals(nodeName)) {
                Element word = (Element) maybeWord;
                String letters = word.getTextContent();
                boolean isAllUc = isBicaseUppercase(letters);
                if (isAllUc) {
                    if (previousIsUc == false) {
                        successiveUcWordsLists.add(new ArrayList<>());
                    }
                    successiveUcWordsLists.get(successiveUcWordsLists.size() - 1).add(word);
                } else {
                    partialOrNoUcWords.add(word);
                }
                previousIsUc = isAllUc;
            } else {
                if ("spacing".equals(nodeName)) {
                    // noop
                } else {
                    // punctuation, numbers, etc. cut succession of uppercase words.
                    previousIsUc = false;
                }
            }
            maybeWord = maybeWord.getNextSibling();
        }

        //
        // build properties sets of all uc words
        //
        Set<Element> firstOfLongUcWordList = new HashSet<>();
        Set<Element> lastOfLongUcWordList = new HashSet<>();
        Set<Element> partOfLongUcWordList = new HashSet<>();
        Set<Element> isolatedUcWord = new HashSet<>();
        Map<Element, String> fragmentIdsLists = new HashMap<>();
        for (List<Element> successiveUcWords : successiveUcWordsLists) {
            int successiveUcWordsCount = successiveUcWords.size();
            Element lastIfSuccessive = null;
            for (int i = 1; i < successiveUcWords.size(); i++) {
                if (nodeIsElementNamed(successiveUcWords.get(i).getPreviousSibling(), Tag.word.tagName)) {
                    // when a word is split into multiple words because of different character attributes (ex:g0) the multiple parts must not be interpreted as different words
                    successiveUcWordsCount--;
                } else {
                    lastIfSuccessive = successiveUcWords.get(i);
                }
            }
            boolean fourOrMoreUcWords = (successiveUcWordsCount >= 4);

            if (fourOrMoreUcWords) {
                String fragmentIdsList = successiveUcWords.stream().map(e -> e.getAttribute("fragmentId")).collect(Collectors.joining(","));
                firstOfLongUcWordList.add(successiveUcWords.get(0));
                lastOfLongUcWordList.add(lastIfSuccessive);
                for (Element word : successiveUcWords) {
                    partOfLongUcWordList.add(word);
                    fragmentIdsLists.put(word, fragmentIdsList);
                }
            } else for (Element word : successiveUcWords) {
                if (word.getTextContent().length() > 1) {
                    // 1 word letter in 4+ sequence is treated as uc word, but not if alone
                    isolatedUcWord.add(word);
                }
            }
        }

        //
        // replace nodes
        //
        maybeWord = block.getFirstChild();
        while (maybeWord != null) {
            if ("word".equals(maybeWord.getNodeName())) {
                Element word = (Element) maybeWord;
                //for (Node word : ft.xpathNodesIt(block, "word")) {
                String letters = word.getTextContent();
                Element translationPair = FakeTranscription.replaceByTranslationPair(word, true);
                Element braille = FakeTranscription.getBraille(translationPair);
                if (firstOfLongUcWordList.contains(word)) {
                    Element prefix = inputDoc.createElement(Tag.uppercase_prefix.tagName);
                    prefix.setAttribute("uppercase-prefix-type", "uc-word-sequence-first-word");
                    prefix.setAttribute("uppercase-word-list", fragmentIdsLists.get(word));
                    prefix.setTextContent(FakeTranscription.fourSuccessiveUcWordsFirstWordPrefix);
                    braille.appendChild(prefix);
                } else if (lastOfLongUcWordList.contains(word)) {
                    Element prefix = inputDoc.createElement(Tag.uppercase_prefix.tagName);
                    prefix.setAttribute("uppercase-prefix-type", "uc-word-sequence-last-word");
                    prefix.setAttribute("uppercase-word-list", fragmentIdsLists.get(word));
                    prefix.setTextContent(FakeTranscription.fourSuccessiveUcWordsLastWordPrefix);
                    braille.appendChild(prefix);
                } else if (partOfLongUcWordList.contains(word)) {
                    /*Element prefix = inputDoc.createElement("uppercase-prefix");
                    prefix.setAttribute("uppercase-prefix-type", "uc-word-sequence-word");
                    prefix.setAttribute("uppercase-word-list", fragmentIdsLists.get(word));
                    prefix.setTextContent(fourSuccessiveUcWordsLastWordPrefix);
                    braille.appendChild(prefix);*/
                } else if (isolatedUcWord.contains(word)) {
                    Element prefix = inputDoc.createElement(Tag.uppercase_prefix.tagName);
                    prefix.setAttribute("uppercase-prefix-type", "uc-word");
                    prefix.setTextContent(FakeTranscription.oneUcWordPrefix);
                    braille.appendChild(prefix);
                }

                if (partOfLongUcWordList.contains(word) || isolatedUcWord.contains(word)) {
                    Element $letters = inputDoc.createElement(Tag.letters.tagName);
                    $letters.setTextContent(transliterateToBraille(letters));
                    braille.appendChild($letters);
                } else {
                    // TODO ? : CBFU 2.1.c méthode 2 : groupe de lettres en majuscules
                    Element $letters = null;
                    for (int i = 0; i < letters.length(); i++) {
                        String letter = letters.substring(i, i + 1);
                        if (isBicaseUppercase(letter)) {
                            // letter is uppercase
                            Element prefix = inputDoc.createElement(Tag.uppercase_prefix.tagName);
                            prefix.setAttribute("uppercase-prefix-type", "uc-letter");
                            prefix.setTextContent(FakeTranscription.oneLetterUcPrefix);
                            braille.appendChild(prefix);
                            $letters = inputDoc.createElement(Tag.letters.tagName);
                            braille.appendChild($letters);
                        }
                        if ($letters == null) {
                            $letters = inputDoc.createElement(Tag.letters.tagName);
                            braille.appendChild($letters);
                        }
                        Node node = inputDoc.createTextNode(transliterateToBraille(letter));
                        $letters.appendChild(node);

                    }
                }
                // current element has been substituted
                maybeWord = translationPair;
            }
            maybeWord = maybeWord.getNextSibling();
        }
    }

    public static void step2_add_fragmentIds(Node block, FakeTranscription ft) throws XPathExpressionException {
        int counter = 0;
        for (Node n : FakeTranscription.iterable(ft.xpathNodes(block, "//*[not(@fragmentId)]"))) {
            ((Element) n).setAttribute("fragmentId", "t" + counter);
            counter++;
        }
    }

    private static String xPathExpressionPartOfG2Headers(NatFilterOptions natFilterOptions) throws NatFilterException {
        final int min = 1;
        final int max = 9;
        final String fullChoice = "//h1|//h2|//h3|//h4|//h5|//h6|//h7|//h8|//h9";
        int firstG2HeaderLevel = Math.min(Math.max(natFilterOptions.getValueAsInteger(NatFilterOption.MODE_G2_HEADING_LEVEL), min), 1 + max);
        if (firstG2HeaderLevel > max) {
            return "";
        } else {
            int index = (firstG2HeaderLevel - 1) * 5;
            return fullChoice.substring(index);
        }
    }

    // Math prefix
    private static boolean mathEquationIsNotAloneInItsParagraph(Element mathElement) {
        // Check if node is alone in paragraph, or rest of paragraph content is empty string or whitespaces
        {
            Node before = mathElement.getPreviousSibling();
            while (before != null) {
                String textContent = before.getTextContent();
                if ((textContent != null) && (!textContent.isBlank())) {
                    return true;
                }
                before = before.getPreviousSibling();
            }
        }
        {
            Node after = mathElement.getNextSibling();
            while (after != null) {
                String textContent = after.getTextContent();
                if ((textContent != null) && (!textContent.isBlank())) {
                    return true;
                }
                after = after.getNextSibling();
            }
        }
        return false;
    }

    private static String getMathEquationPrefix(boolean mathIsAloneInItsParagraph, boolean optionModeMathAlwaysPrefix, String possiblePrefix) {
        if (optionModeMathAlwaysPrefix) {
            // possiblePrefix is always double prefix because if optionModeMathAlwaysPrefix,
            // matching NatFilterOption is true, so translation returns the double prefix
            return possiblePrefix;
        } else if (mathIsAloneInItsParagraph) {
            return "";
        } else {
            return possiblePrefix;
        }
    }

    private static Element createSpacingPair(Document inputDoc) {
        // <spacing fragmentId="f12" source="noir">
        // <noir fragmentId="t38"></noir>
        // <braille fragmentId="t39"></braille>
        // </spacing>
        Element spacing = inputDoc.createElement((Tag.spacing.tagName));
        spacing.setAttribute("source", "noir");
        Element spacing_noir = inputDoc.createElement((Tag.noir.tagName));
        spacing.appendChild(spacing_noir);
        spacing_noir.setTextContent(" ");
        Element spacing_braille = inputDoc.createElement((Tag.braille.tagName));
        spacing.appendChild(spacing_braille);
        spacing_braille.setTextContent(FakeTranscription.BrailleBlank);
        return spacing;
    }

    public static boolean mathMLIsChemistry(Element math) {
        return "true".equals(math.getAttribute("chemistry"));
    }

    public static String transliterateToBraille(String noir) {
        return noir.toLowerCase();
    }

    public static void step2(FakeTranscription ft, /*String inputFilename, String outputFilename,*/ NatFilterOptions natFilterOptions, Document inputDoc) throws Exception {

//        Document inputDoc = ft.parseDOM(inputFilename);

        step2_add_fragmentIds(inputDoc, ft);

        // All math done in one pass

        MathTranslator.NatbrailleMathTranslator natbrailleMathTranslator = natbrailleMathTranslators.getNatbrailleMathTranslator(natFilterOptions);
        Map<String, String> mathmlStrings = new HashMap<>();
        Map<String, Element> mathmlElements = new HashMap<>();
        forEachElement(inputDoc, "(//math)", block -> {

            // remove annotation
            NodeList annotations = block.getElementsByTagName("annotation");
            for (int i = 0; i < annotations.getLength(); i++) {
                Node annotation = annotations.item(i);
                annotation.getParentNode().removeChild(annotation);
            }
            // add in map
            String fragmentId = block.getAttribute("fragmentId");
            String mathmlString = XmlUtils.toString(block, true).replaceAll("xmlns=\"\"", "");

            mathmlStrings.put(fragmentId, mathmlString);
            mathmlElements.put(fragmentId, block);
        });

        boolean optionModeMathAlwaysPrefix = natFilterOptions.getValueAsBoolean(NatFilterOption.MODE_MATH_ALWAYS_PREFIX);

        Map<String, String> brailleEquations = natbrailleMathTranslator.translate(mathmlStrings);
        for (var mathml : mathmlElements.entrySet()) {

            Element mathmlEquation = mathml.getValue();
            //Element pair = createTranslationPair(mathml.getValue(),true);
            //mathml.getValue().getParentNode().insertBefore(pair,mathml.getValue());


            // todo should use same replacebypair function as other elements
            Element pair = inputDoc.createElement("math");
            pair.setAttribute("fragmentId", mathml.getKey());
            mathmlEquation.getParentNode().insertBefore(pair, mathmlEquation);

            boolean mathIsAloneInItsParagraph = !mathEquationIsNotAloneInItsParagraph(mathmlEquation);


            // noir
            Element noir = inputDoc.createElement("noir");
            noir.setAttribute("source", "noir");
            pair.appendChild(noir);
            Node mathmlClone = mathmlEquation.cloneNode(true);
            noir.appendChild(mathmlClone);

            //

            // remove unconverted
            mathmlEquation.getParentNode().removeChild(mathmlEquation);

            // braille
            Element braille = inputDoc.createElement("braille");
            pair.appendChild(braille);

            Element brailleWithTypesetCharacters = inputDoc.createElement(Tag.pre_typeset_math.tagName);
            String brailleEquation = brailleEquations.get(mathml.getKey());

            {
                // cleanup Chemistry
                // TODO : fix, and why ?
                String dirt = "\n" +
                        "      \n" +
                        "         X\n" +
                        "         X\n" +
                        "         \n" +
                        "            ⠨⠭\n" +
                        "      ";
                brailleEquation = brailleEquation.replace(dirt,"");
            }

            String cleanBrailleEquation = MathTranslator.NatbraillePreTypesetBraille.getCleanString(brailleEquation);
            //brailleWithTypesetCharacters.setTextContent(brailleEquation);

            String prefixString = MathTranslator.NatbraillePreTypesetBraille.getPrefix(brailleEquation);

            String maybeMathPrefix = getMathEquationPrefix(mathIsAloneInItsParagraph, optionModeMathAlwaysPrefix, prefixString);


            brailleWithTypesetCharacters.setTextContent(cleanBrailleEquation);
            brailleWithTypesetCharacters.setAttribute("rawNatbraillePreTypeset", brailleEquation);
            braille.appendChild(brailleWithTypesetCharacters);
/*
            System.out.println("EQ");
            System.out.println("brailleEquation      :"+brailleEquation);
            System.out.println("alone in paragraph   :"+mathIsAloneInItsParagraph);
            System.out.println("cleanBrailleEquation :"+cleanBrailleEquation);
            System.out.println("prefixString         :"+prefixString);
            System.out.println("maybeMathPrefix      :"+maybeMathPrefix);
            System.out.println("/EQ");
*/
            if (!maybeMathPrefix.isEmpty()) {
                Element mathPrefix = inputDoc.createElement(Tag.math_prefix.tagName);
                mathPrefix.setTextContent(maybeMathPrefix);
                brailleWithTypesetCharacters.getParentNode().insertBefore(mathPrefix, brailleWithTypesetCharacters);
            }
            {
                // mandatory spacing after math block (will be removed by layout if eol)
                Element spacing = createSpacingPair(inputDoc);
                pair.getParentNode().insertBefore(spacing, pair.getNextSibling()); // If [the second parameter] is null, insert newChild at the end of the list of children.
            }
            {
                // maybe spacing before
                Node beforeMath = pair.getPreviousSibling();
                boolean isAtStartOfLine = (beforeMath == null);
                boolean hasSpaceBefore = (beforeMath != null) && (Tag.spacing.tagName.equals(beforeMath.getNodeName()));
                if (!isAtStartOfLine && !hasSpaceBefore) {
                    Element spacing = createSpacingPair(inputDoc);
                    pair.getParentNode().insertBefore(spacing, pair);
                }
            }
        }


        forEachElement(inputDoc, "(//p|//h1|//h2|//h3|//h4|//h5|//h6|//title)", block -> {
            // step2_translate_spans(block, ft);
            step2_translate_words(block, ft);
            step2_translate_words_emphasis(block, ft);
            step2_translate_numbers(block, ft);
            step2_translate_punctuation(block, ft);
            step2_translate_remaining_spaces(block, ft);
        });

        // merge text node of letters
        forEachElement(inputDoc, "//letters", FakeTranscription::mergeTextNodes);

        // natFilterOptions.setOption(NatFilterOption.MODE_G2,"true");
        {
            boolean hasG2FilterOption =natFilterOptions.getValueAsBoolean(NatFilterOption.MODE_G2);
            //if (natFilterOptions.getValueAsBoolean(NatFilterOption.MODE_G2)) {
                // g2
                G2.G2Translator cachedG2 = G2.G2Translators.getG2Translator(
                        G2.Origin.Natbraille,
                        natFilterOptions.getValue(NatFilterOption.XSL_G2_DICT),  // i.e. "nat://system/xsl/dicts/fr-g2.xml",
                        true
                );

                String headerChoice = xPathExpressionPartOfG2Headers(natFilterOptions);
                String blockChoice = "(//p" + (headerChoice.isEmpty() ? "" : "|") + headerChoice + ")";

                forEachElement(inputDoc, blockChoice, block -> {
                    forEachElement(block, ".//letters", letters -> {
                        Element brailleElement = (Element) letters.getParentNode();
                        Element wordElement = (Element) brailleElement.getParentNode();
                        Element maybePunctuationElementAfter = getMaybePunctuationElementAfter(wordElement);
                        if (BRAILLE_GRADE_VALUE_G1.equals(wordElement.getAttribute(BRAILLE_GRADE_ATTRIBUTE)) || BRAILLE_GRADE_VALUE_G0.equals(wordElement.getAttribute(BRAILLE_GRADE_ATTRIBUTE))) {
                            return;
                        }
                        if (!(hasG2FilterOption || BRAILLE_GRADE_VALUE_G2.equals(wordElement.getAttribute(BRAILLE_GRADE_ATTRIBUTE)))){
                            return;
                        }

                        if (maybePunctuationElementAfter != null) {
                            // System.out.println("after noir:" + FakeTranscription.getNoir(maybePunctuationElementAfter).getTextContent());
                            // System.out.println("after braille:" + FakeTranscription.getBraille(maybePunctuationElementAfter).getTextContent());
                        }
                        // do not pass mixed uppercase & lowercase wOrDs
                        // this is the case when there are multiple letters elements in brailleParent
                        // TODO check what happens when this is the case.
                        Node brailleElementChild = brailleElement.getFirstChild();
                        int lettersElementCount = 0;
                        while ((brailleElementChild != null) && (lettersElementCount < 2)) {
                            if (brailleElementChild.getNodeName().equals(Tag.letters.tagName)) {
                                lettersElementCount++;
                            }
                            brailleElementChild = brailleElementChild.getNextSibling();
                        }

                        if (lettersElementCount < 2) {
                            String g0 = letters.getTextContent();
                            String followingPunctuation = (maybePunctuationElementAfter == null) ? null : (FakeTranscription.getNoir(maybePunctuationElementAfter).getTextContent());
                            G2.G2Result g2Result = cachedG2.getContractedBraille(new G2.G2SourceWord(g0, followingPunctuation, null));
                            String g2BrailleString = g2Result.braille.replaceAll("ⴹ", ""); // TODO: fix hack hack hack
                            letters.setTextContent(g2BrailleString);
                            letters.setAttribute("g2", "true");
                        }

                    });
                });
            //}
        }
        step2_add_fragmentIds(inputDoc, ft);
        forEachElement(inputDoc, "(//word|//number|//punctuation|//spacing)[not(@source)]", el -> {
            System.out.println("== untranscribed" + " @" + el.getAttribute("fragmentId") + " " + el.getTagName() + " " + el.getTextContent());
            //    throw new Error("something has not been transcribed");
        });


        // ft.serializeDOM(outputFilename, inputDoc);
        // return inputDoc;
    }

    /**
     * Get following punctuation Element or null if none or not a punctuation
     *
     * @param letters
     * @return
     */
    public static Element getMaybePunctuationElementAfter(Element letters) {
        Node nextNode = letters.getNextSibling();
        if ((nextNode != null) && (Tag.punctuation.tagName.equals(nextNode.getNodeName()))) {
            return ((Element) nextNode);
        }
        return null;
    }
}
