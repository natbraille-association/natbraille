package org.natbraille.editor.server.importexport;

import org.natbraille.core.NatDynamicResolver;
import org.natbraille.core.document.NatDocumentException;
import org.natbraille.core.filter.NatFilterException;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.core.gestionnaires.GestionnaireErreur;
import org.natbraille.editor.server.tools.LoggerAfficheur;
import org.natbraille.editor.server.tools.SecureXmlParsing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.StandardCharsets;

public class StylistWrapper {

    private static final Logger LOGGER = LoggerFactory.getLogger(StylistWrapper.class);

    private static final String ATTRIBUTE_NAME = "name";
    private static final String ATTRIBUTE_NAME_CHEMISTRY = "chimie";
    private static final String ATTRIBUTE_SPECIAL = "special";

    public static String getChemistryStyleName(Document stylistDocument) {
        Element styleChemistry = getStyleElementByName(stylistDocument,ATTRIBUTE_NAME_CHEMISTRY);
        if (styleChemistry != null) {
            return styleChemistry.getAttribute(ATTRIBUTE_SPECIAL);
        }
        return null;
    }

    public static Element getStyleElementByName(Document stylistDocument, String styleName) {
        final NodeList styles = stylistDocument.getElementsByTagName("style");
        for (int i = 0; i < styles.getLength(); i++) {
            Element style = (Element) (styles.item(i));
            if (styleName.equalsIgnoreCase(style.getAttribute(ATTRIBUTE_NAME))) {
                return style;
            }
        }
        return null;
    }

    public static Document buildStylistDocumentFromNatFilterOptions(NatFilterOptions nfo) throws NatFilterException, IOException, NatDocumentException, ParserConfigurationException, SAXException {
        URI uri = URI.create(nfo.getValue(NatFilterOption.Stylist));
        GestionnaireErreur ge = new GestionnaireErreur();
        ge.addAfficheur(new LoggerAfficheur(LOGGER));
        NatDynamicResolver ndr = new NatDynamicResolver(nfo, ge);
        byte[] bytes = ndr.readByteArrayAtURI(uri);
        System.out.println(new String(bytes, StandardCharsets.UTF_8));
        return SecureXmlParsing.byteToDocument(bytes);
    }


}
