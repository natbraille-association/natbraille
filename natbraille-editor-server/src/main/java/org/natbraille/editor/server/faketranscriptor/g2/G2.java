package org.natbraille.editor.server.faketranscriptor.g2;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.opencsv.exceptions.CsvValidationException;
import org.liblouis.*;
import org.natbraille.core.document.NatDocument;
import org.natbraille.core.document.NatDocumentException;
import org.natbraille.core.filter.NatFilterChain;
import org.natbraille.core.filter.NatFilterConfigurator;
import org.natbraille.core.filter.NatFilterException;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.core.filter.transcodeur.TranscodeurNormal;
import org.natbraille.core.gestionnaires.*;
import org.natbraille.editor.server.Server;
import org.natbraille.editor.server.performance.Estimator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import javax.xml.transform.TransformerException;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * TODO : emphasis input + cache
 */
public class G2 {
    public enum Origin {
        Natbraille,
        Liblouis

    }

    /**
     * A Word with possible emphasis to be translated by a G2Translator
     */
    public static class G2SourceWord {
        public final String letters;
        public final String followingPunctuation;
        public final List<Boolean> emphasis;

        public G2SourceWord(@JsonProperty("letters") String letters, @JsonProperty("followingPunctuation") String followingPunctuation, @JsonProperty("emphasis") List<Boolean> emphasis) {
            this.letters = letters;
            this.emphasis = emphasis;
            this.followingPunctuation = followingPunctuation;
        }

        public String toKey() {
            StringBuilder sb = new StringBuilder();
            sb.append(letters);
            sb.append("\0");
            if (followingPunctuation != null) {
                sb.append(followingPunctuation);
            }
            sb.append("\0");
            if (emphasis != null) {
                for (var e : emphasis) {
                    sb.append(e ? "1" : "0");
                }
            }
            return sb.toString();
        }

        public static G2SourceWord fromKey(String key) {
            int separator0Index = key.indexOf("\0");
            String letters = key.substring(0, separator0Index);
            String tail = key.substring(separator0Index + 1);
            int separator1Index = tail.indexOf("\0");
            String punctuationAfter = tail.substring(0, separator1Index);
            String emphasisString = tail.substring(separator1Index + 1);
            if (emphasisString.isEmpty()) {
                return new G2SourceWord(letters, punctuationAfter.isEmpty() ? null : punctuationAfter, null);
            } else {
                List<Boolean> emphasis = new ArrayList<Boolean>();
                for (int i = 0; i < emphasisString.length(); i++) {
                    emphasis.add(key.charAt(i) == '1');
                }
                return new G2SourceWord(letters, punctuationAfter.isEmpty() ? null : punctuationAfter, emphasis);
            }
        }
    }


    /**
     * G2Translator G2 Transcription Result
     */
    public static class G2Result {
        public final String braille;
        public final String explanation;
        public final String ambiguity = "";
        public final String error = "";

        public G2Result(String braille) {
            this.braille = braille;
            this.explanation = "";
        }

        public G2Result(@JsonProperty("braille") String braille, @JsonProperty("explanation") String explanation) {
            this.braille = braille;
            this.explanation = explanation;
        }

    }

    /**
     * A Pair of G2SourceWord, G2Result
     */
    public static class G2SourceResultPair {
        public final G2SourceWord g2SourceWord;
        public final G2Result g2Result;

        public G2SourceResultPair(@JsonProperty("g2SourceWord") G2SourceWord g2SourceWord, @JsonProperty("g2Result") G2Result g2Result) {
            this.g2SourceWord = g2SourceWord;
            this.g2Result = g2Result;
        }
    }

    /**
     * G2 transcription interface transcribes words
     */
    public interface G2Translator {
        public G2Result getContractedBraille(G2SourceWord g2SourceWord) throws Exception;

        public Origin getOrigin();

        public String getId();
    }

    /**
     * G2 transcription using liblouis
     */
    private static class LiblouisG2 implements G2Translator {
        private final String liblouisTables;
        private final Translator liblouisTranslator;
        private final Typeform liblouisEmpasisTypeform;

        public Origin getOrigin() {
            return Origin.Liblouis;
        }

        public String getId() {
            return getOrigin().name() + " - " + liblouisTables;
        }

        public synchronized G2Result getContractedBraille(G2SourceWord g2SourceWord) throws TranslationException, DisplayException {

            String letters = g2SourceWord.letters;
            List<Boolean> emphasis = g2SourceWord.emphasis;

            Typeform[] typeforms = new Typeform[letters.length()];
            for (int i = 0; i < typeforms.length; i++) {
                typeforms[i] = ((emphasis != null) && emphasis.get(i)) ? liblouisEmpasisTypeform : Typeform.PLAIN_TEXT;
            }

            int[] characterAttributes = new int[letters.length()];
            for (int i = 0; i < characterAttributes.length; i++) {
                characterAttributes[i] = i;
            }

            int[] interCharacterAttributes = new int[letters.length() - 1];
            for (int i = 0; i < interCharacterAttributes.length; i++) {
                interCharacterAttributes[i] = i;
            }

            TranslationResult translationResult = liblouisTranslator.translate(letters, typeforms, characterAttributes, interCharacterAttributes);
            return new G2Result(translationResult.getBraille());
        }

        private LiblouisG2(String liblouisTables, Translator liblouisTranslator, Typeform liblouisEmpasisTypeform) {
            this.liblouisTables = liblouisTables;
            this.liblouisTranslator = liblouisTranslator;
            this.liblouisEmpasisTypeform = liblouisEmpasisTypeform;
        }

        public static LiblouisG2 build(String table, String emphasisName) throws CompilationException {
            Translator liblouisTranslator = new Translator(table);
            Typeform liblouisEmpasisTypeform = getEmphasisTypeformByName(liblouisTranslator, emphasisName);
            return new LiblouisG2(table, liblouisTranslator, liblouisEmpasisTypeform);
        }
    }

    static Typeform getEmphasisTypeformByName(Translator translator, String typeformName) {
        Typeform emphasisTypeForm = null;
        for (Typeform typeform : translator.getSupportedTypeforms()) {
            if (typeform.getName().equals(typeformName)) {
                emphasisTypeForm = typeform;
            }
        }
        return emphasisTypeForm;
    }

    /**
     * G2 transcription using Natbraille
     */
    private static class NatbrailleG2 implements G2Translator {

        /**
         * Split messages between xslt messages / other
         * in order to get a raw message trace that can be used to provide explanation for the Braille contraction
         */
        private static class ReusableAfficheur extends Afficheur {
            public final List<LogMessage> messages = new ArrayList<>();
            public final List<String> xsltMessages = new ArrayList<>();

            @Override
            public void afficheMessage(LogMessage logMessage) {
                MessageContents contents = logMessage.getContents();
                if (contents.getIsXsltProcessingMessage()) {
                    String rawText = logMessage.getContents().getRawText();
                    xsltMessages.add(rawText);
                } else {
                    messages.add(logMessage);
                }
            }

            public void clear() {
                xsltMessages.clear();
                messages.clear();
            }

            public ReusableAfficheur() {
                setLogLevel(LogLevel.ULTRA);
            }
        }

        public Origin getOrigin() {
            return Origin.Natbraille;
        }

        public String getId() {
            return getOrigin().name() + " - " + xsl_g2_dict;
        }

        final String xsl_g2_dict;
        final NatFilterChain trans;
        final ReusableAfficheur reusableAfficheur;
        final NatFilterConfigurator configurator;

        public NatbrailleG2(String xslG2Dict, NatFilterChain trans, ReusableAfficheur reusableAfficheur, NatFilterConfigurator configurator) {
            this.xsl_g2_dict = xslG2Dict;
            this.trans = trans;
            this.reusableAfficheur = reusableAfficheur;
            this.configurator = configurator;
        }

        public static NatbrailleG2 build(String resourceName) throws NatFilterException {
            //String resourceName = "nat://user/$explain$/fr-g2-rules/fr-g2.xml";

            GestionnaireErreur ge = new GestionnaireErreur();

            AfficheurAnsiConsole afficheurConsole = new AfficheurAnsiConsole(LogLevel.ULTRA);
            afficheurConsole.setLogLevel(LogLevel.NONE);
            //afficheurConsole.setLogLevel(LogLevel.DEBUG);
            afficheurConsole.setLogLevel(LogLevel.DEBUG);
            //ge.addAfficheur(afficheurConsole);


            ReusableAfficheur reusableAfficheur = new ReusableAfficheur();
            ge.addAfficheur(reusableAfficheur);

            NatFilterOptions nfo = new NatFilterOptions();
            nfo.setOption(NatFilterOption.MODE_G2, "true");
            nfo.setOption(NatFilterOption.XSL_G2_DICT, resourceName);
            //nfo.setOption(NatFilterOption.debug_xsl_processing, "false");
            nfo.setOption(NatFilterOption.debug_xsl_processing, "true");
            /*
            UserDocumentLoader adhocUserDocumentLoader = (UserDocumentLoader) (username, kind, name) -> {
                if (username.equals("$explain$") && kind.equals("fr-g2-rules") && name.equals("fr-g2.xml")) {
                    //return RulesToolKit.getRuleFileBytes(ruleFileVariants.rules);
                    return
                } else {
                    throw new Error("could not load UserDocument");
                }
            };

            NatFilterConfigurator configurator = new NatFilterConfigurator(nfo, ge, adhocUserDocumentLoader);

             */

            NatFilterConfigurator configurator = new NatFilterConfigurator(nfo, ge);//, adhocUserDocumentLoader);
            NatFilterChain trans = new NatFilterChain(configurator);
            trans.addNewFilter(TranscodeurNormal.class);

            return new NatbrailleG2(resourceName, trans, reusableAfficheur, configurator);
        }

        public synchronized G2Result getContractedBraille(G2SourceWord g2SourceWord) throws NatDocumentException, NatFilterException, IOException, TransformerException {

            String letters = g2SourceWord.letters;
            List<Boolean> emphasis = g2SourceWord.emphasis;

            // reset error messsages
            reusableAfficheur.clear();

            // transcription
            NatDocument inDoc = new NatDocument();
            String wordId = "1";
            //inDoc.setString(G2AdHocDoc.docDocFromWord(letters, wordId));
            inDoc.setString(G2AdHocDoc.docDocFromWordAndFollowingPunctuation(letters, wordId,g2SourceWord.followingPunctuation));
            NatDocument outDoc = trans.run(inDoc);
            // System.out.println("----------------------------");
            // System.out.println(outDoc.getString());
            // System.out.println("----------------------------");

            Document outDomDoc = outDoc.getDocument(configurator.getNatDynamicResolver().getXMLReader());
            Map<String, String> extracted = G2AdHocDoc.extractBrailleStringsFromOutputDocument(outDomDoc);
            String outWord = extracted.get(wordId);

            StringBuilder xsltTrace = new StringBuilder();
            for (var message : reusableAfficheur.xsltMessages) {
                // TODO : further refine explanation from trace
                xsltTrace.append(message);
                xsltTrace.append("\n");
            }

            if (outWord != null) {
                return new G2Result(outWord.trim(), xsltTrace.toString());
            } else {
                return new G2Result(null);
            }
        }
    }


    /**
     * A G2Translator that encapsulates another G2Translator behind a cache
     * consisting of a ConcurrentHashMap whose keys are build with G2SourceWord.toKey
     */
    public static class CachedG2 implements G2Translator {
        final G2Translator g2Translator;

        public CachedG2(G2Translator g2Translator) {
            this.g2Translator = g2Translator;
        }

        ConcurrentHashMap<String, G2Result> cache = new ConcurrentHashMap<>();

        public void feedCache(G2SourceWord sourceWord, G2Result result) {
            cache.put(sourceWord.toKey(), result);
        }

        public interface ExportCallback {
            void onCacheLine(G2SourceWord sourceWord, G2Result result);
        }

        public void exportCache(ExportCallback callback) {
            for (var entry : cache.entrySet()) {
                G2SourceWord sourceWord = G2SourceWord.fromKey(entry.getKey());
                G2Result result = entry.getValue();
                callback.onCacheLine(sourceWord, result);
            }
        }

        public void importCache(List<G2SourceResultPair> pairs) {
            for (var pair : pairs) {
                feedCache(pair.g2SourceWord, pair.g2Result);
            }
        }

        public List<G2SourceResultPair> getCachePairs() {
            List<G2SourceResultPair> pairs = new ArrayList<>();
            exportCache((sourceWord, result) -> {
                G2SourceResultPair pair = new G2SourceResultPair(sourceWord, result);
                pairs.add(pair);
            });
            return pairs;
        }

        @Override
        public synchronized G2Result getContractedBraille(G2SourceWord g2SourceWord) throws Exception {

            String g2SourceWordKey = g2SourceWord.toKey();
            G2Result maybeResult = cache.get(g2SourceWordKey);
            if (maybeResult != null) {
                return maybeResult;
            } else {
                G2Result result = g2Translator.getContractedBraille(g2SourceWord);
                cache.put(g2SourceWordKey, result);
                return result;
            }
        }

        @Override
        public Origin getOrigin() {
            return g2Translator.getOrigin();
        }

        @Override
        public String getId() {
            return g2Translator.getId();
        }

    }

    public static void importJSONCacheFile(CachedG2 cachedG2, InputStream inputStream) throws IOException {
        // load and parse json
        JsonFactory jsonFactory = new JsonFactory();
        JsonParser jsonParser = jsonFactory.createParser(inputStream);
        jsonParser.setCodec(new ObjectMapper());
        G2SourceResultPair[] pairs = jsonParser.readValueAs(G2SourceResultPair[].class);
        // add pairs to cache
        cachedG2.importCache(Arrays.asList(pairs));
    }

    public static void importJSONZIPCacheFile(CachedG2 cachedG2, InputStream inputStream) throws IOException {
        try (ZipInputStream zis = new ZipInputStream(inputStream)) {
            ZipEntry entry;
            while (((entry = zis.getNextEntry()) != null)) {
                if ((!entry.isDirectory()) && entry.getName().endsWith(".json")) {
                    ObjectMapper objectMapper = new ObjectMapper();
                    JsonFactory jsonFactory = objectMapper.getFactory();
                    JsonParser jsonParser = jsonFactory.createParser(zis);
                    jsonParser.setCodec(objectMapper);
                    G2SourceResultPair[] pairs = jsonParser.readValueAs(G2SourceResultPair[].class);
                    cachedG2.importCache(Arrays.asList(pairs));
                    zis.closeEntry();
                }
            }
        }
    }

    public static void exportJSONCacheFile(CachedG2 cachedG2, OutputStream outputStream) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(outputStream, cachedG2.getCachePairs());
    }


    /**
     * Contains all instances of G2 translators
     * todo : never cleaned up, clean up sometime
     */
    public static class G2Translators {

        private static final Logger LOGGER = LoggerFactory.getLogger(G2Translators.class);
        private static final Map<String, G2Translator> liblouisG2Map = new HashMap<>();
        private static final Map<String, G2Translator> natbrailleG2Map = new HashMap<>();

        private static String getG2TranslatorCacheJsonFilename(Origin origin, String configPath) {
            // the filename is base64 encoded to avoid forbidden characters in filename
            // and no possible collision of code
            String code = origin.name() + " " + configPath;
            return Base64.getEncoder().encodeToString(code.getBytes()) + ".json";
        }

        private static String getG2TranslatorCacheJsonZipFilename(Origin origin, String configPath) {
            return getG2TranslatorCacheJsonFilename(origin, configPath) + ".zip";
        }

        private static Path getG2TranslatorCacheJsonPath(Origin origin, String configPath) {
            return Path.of(Server.serverConfiguration.g2CacheRoot, getG2TranslatorCacheJsonFilename(origin, configPath));
        }

        private static Path getG2TranslatorCacheJsonZipPath(Origin origin, String configPath) {
            return Path.of(Server.serverConfiguration.g2CacheRoot, getG2TranslatorCacheJsonZipFilename(origin, configPath));
        }

        private static void writeFileSystemCache(Origin origin, String configPath, G2Translator g2Translator) {
            if (g2Translator instanceof CachedG2) {
                Path pathOfFilesystemCache = getG2TranslatorCacheJsonPath(origin, configPath);
                try {
                    exportJSONCacheFile((CachedG2) g2Translator, Files.newOutputStream(pathOfFilesystemCache));
                } catch (IOException e) {
                    LOGGER.error("error while saving cache for " + origin.name() + " " + configPath + " at " + pathOfFilesystemCache);
                    LOGGER.error(e.toString());
                }
            }
        }

        public static void writeAllFileSystemCaches() {

            for (var entry : liblouisG2Map.entrySet()) {
                writeFileSystemCache(Origin.Liblouis, entry.getKey(), entry.getValue());
            }
            for (var entry : natbrailleG2Map.entrySet()) {
                writeFileSystemCache(Origin.Natbraille, entry.getKey(), entry.getValue());
            }

        }

        /**
         * Create a translator using given config. If cached is true, a cached g2 translator will be created and if a cache file is present on the filesystem, it will imported
         *
         * @param origin
         * @param configPath
         * @param cached
         * @return
         * @throws NatFilterException
         * @throws CompilationException
         */
        private static G2Translator createTranslator(Origin origin, String configPath, boolean cached) throws NatFilterException, CompilationException {
            G2Translator g2Translator;
            G2Translator rawTranslator = switch (origin) {
                case Natbraille -> NatbrailleG2.build(configPath);
                case Liblouis -> LiblouisG2.build(configPath, "italic");
            };
            if (cached) {
                // create a cache around the raw translator
                CachedG2 cachedG2 = new CachedG2(rawTranslator);

                // maybe there is a .json or .json.zip cache file on the filesystem, if so we want to import it
                Path pathOfMaybeFileSystemJsonCache = getG2TranslatorCacheJsonPath(origin, configPath);
                Path pathOfMaybeFileSystemJsonZipCache = getG2TranslatorCacheJsonZipPath(origin, configPath);
                List<File> files = Arrays.asList(pathOfMaybeFileSystemJsonCache.toFile(), pathOfMaybeFileSystemJsonZipCache.toFile());

                // sort most recent first
                files.sort(Comparator.comparingLong(File::lastModified).reversed());
                for (File f : files) {
                    if (f.exists()) {
                        try {
                            if (f.getName().endsWith("json")) {
                                importJSONCacheFile(cachedG2, new FileInputStream(f));
                            } else if (f.getName().endsWith("zip")) {
                                importJSONZIPCacheFile(cachedG2, new FileInputStream(f));
                            }
                            LOGGER.info("loaded cache found for " + origin.name() + " " + configPath + " at " + f.getName());
                            break;
                        } catch (IOException e) {
                            LOGGER.error("error loading cache found for " + origin.name() + " " + configPath + " at " + f.getName());
                            LOGGER.error(e.toString());
                        }
                    }
                }
                g2Translator = cachedG2;
            } else {
                // we don't want a cache so pass directly the translator
                g2Translator = rawTranslator;
            }
            return g2Translator;
        }

        public static synchronized G2Translator getG2Translator(Origin origin, String configPath, boolean cachedIfCreated) throws NatFilterException, CompilationException, IOException {
            Map<String, G2Translator> translatorMap = switch (origin) {
                case Liblouis -> liblouisG2Map;
                case Natbraille -> natbrailleG2Map;
            };
            // get from map
            G2Translator g2Translator = translatorMap.get(configPath);
            if (g2Translator == null) {
                // create cached or raw translator
                g2Translator = createTranslator(origin, configPath, cachedIfCreated);
                // add to map
                translatorMap.put(configPath, g2Translator);
            }
            return g2Translator;
        }
    }

    public static void main(String[] args) throws Exception {
        // testLiblouisAndNatbrailleG2();
        //testCache();
        //testG2SourceWordKey();
        //   testNatbrailleG2Explanation();
        // testCacheImportExport();

//        buildACache();

        //readACache();
        //buildACacheThreaded();


        if (true) {

            G2Translator cachedG2 = G2Translators.getG2Translator(Origin.Natbraille, "nat://system/xsl/dicts/fr-g2.xml", true);
            {
                G2SourceWord word = new G2SourceWord("camion", null, null);
                G2Result contracted = cachedG2.getContractedBraille(word);
                System.out.println(contracted.braille);
            }
            {
                G2SourceWord word = new G2SourceWord("passion", null, null);
                G2Result contracted = cachedG2.getContractedBraille(word);
                System.out.println(contracted.braille);
            }
            {
                G2SourceWord word = new G2SourceWord("poisson", null, null);
                G2Result contracted = cachedG2.getContractedBraille(word);
                System.out.println(contracted.braille);
            }
            G2Translators.writeAllFileSystemCaches();
        }

        if (false) {
            CachedG2 cachedG2 = new CachedG2(NatbrailleG2.build("nat://system/xsl/dicts/fr-g2.xml"));

            final InputStream inputStream = Files.newInputStream(Path.of("g2-cache-natbraille-fr-g2-multi-all.json"));


            importJSONCacheFile(cachedG2, inputStream);

            G2SourceWord word = new G2SourceWord("camion", null, null);

            G2Result contracted = cachedG2.getContractedBraille(word);

            System.out.println(contracted.braille);
            /*            System.out.println(contracted.explanation);

             */

        }


    }


    /*
        public static void readACache() throws NatFilterException, CsvValidationException, IOException {
            CachedG2 cachedG2 = new CachedG2(NatbrailleG2.build("nat://system/xsl/dicts/fr-g2.xml"));
            String filename = "g2-cache-natbraille-fr-g2.xml-2.json";
            //ObjectMapper mapper = new ObjectMapper();
            {
                // read pairs
                JsonFactory jsonFactory = new JsonFactory();
                JsonParser jsonParser = jsonFactory.createParser(new File(filename));
                ObjectMapper mapper = new ObjectMapper();
                jsonParser.setCodec(mapper);
                G2SourceResultPair[] pairs = jsonParser.readValueAs(G2SourceResultPair[].class);
                System.out.println("count" + pairs.length);



            }

        }
    */
    public static void buildACacheThreaded() throws NatFilterException, CsvValidationException, IOException, InterruptedException {

        final String path = "/home/vivien/pr-src/doc-g2/morphalou/morphalou/5/Morphalou3.1_formatCSV_toutEnUn/Morphalou3.1_CSV.csv";
        final List<Morphalou.Word> words = MorphalouG2Utils.keepOnlyOneInstanceOfAGraphie(Morphalou.parseCsv(path));//.subList(0,50000);


        //final int threadCount = 6;
        final int threadCount = Runtime.getRuntime().availableProcessors() - 1;
        System.out.println("using " + threadCount + " threads");


        // split wordlist in <threadCount> lists
        List<List<Morphalou.Word>> threadsWords = new ArrayList<>();
        for (int i = 0; i < threadCount; i++) {
            threadsWords.add(new ArrayList<>());
        }
        int listIndex = 0;
        for (var word : words) {
            threadsWords.get(listIndex).add(word);
            listIndex = (listIndex + 1) % threadCount;
        }

        class CachePartRunnable implements Runnable {

            final int threadIndex;
            final List<Morphalou.Word> words;
            List<G2SourceResultPair> pairs = null;

            final
            @Override
            public void run() {
                try {
                    CachedG2 cachedG2 = new CachedG2(NatbrailleG2.build("nat://system/xsl/dicts/fr-g2.xml"));
                    for (int i = 0; i < words.size(); i++) {
                        Morphalou.Word word = words.get(i);
                        try {
                            G2Result b = cachedG2.getContractedBraille(new G2SourceWord(word.getGraphie(), null, null));
                        } catch (Exception e) {
                            System.err.println("threadIndex:" + threadIndex + " graphie error " + word.getGraphie());
                        }
                        if ((i % 1000) == 0) {
                            System.out.println("threadIndex:" + threadIndex + " " + i + "/" + words.size() + " " + (100.0 * i / (words.size())) + "%");
                        }
                    }
                    pairs = cachedG2.getCachePairs();
                    exportJSONCacheFile(cachedG2, new FileOutputStream("g2-cache-natbraille-fr-g2-multi-" + threadIndex + ".json"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            public CachePartRunnable(int threadIndex, List<Morphalou.Word> words) {
                this.threadIndex = threadIndex;
                this.words = words;
            }
        }
        ;

        CachePartRunnable[] cachePartRunnables = new CachePartRunnable[threadCount];
        Thread[] threads = new Thread[threadCount];

        for (int i = 0; i < threads.length; i++) {
            cachePartRunnables[i] = new CachePartRunnable(i, threadsWords.get(i));
            threads[i] = new Thread(cachePartRunnables[i]);
            threads[i].start();
        }
        for (int i = 0; i < threads.length; i++) {
            threads[i].join();
        }
        CachedG2 cachedG2 = new CachedG2(NatbrailleG2.build("nat://system/xsl/dicts/fr-g2.xml"));
        for (int i = 0; i < threads.length; i++) {
            if (cachePartRunnables[i].pairs != null) {
                cachedG2.importCache(cachePartRunnables[i].pairs);
            }
        }
        exportJSONCacheFile(cachedG2, new FileOutputStream("g2-cache-natbraille-fr-g2-multi-all.json"));

    }

    public static void buildACache() throws NatFilterException, CsvValidationException, IOException {

        CachedG2 cachedG2 = new CachedG2(NatbrailleG2.build("nat://system/xsl/dicts/fr-g2.xml"));

        final String path = "/home/vivien/pr-src/doc-g2/morphalou/morphalou/5/Morphalou3.1_formatCSV_toutEnUn/Morphalou3.1_CSV.csv";
        List<Morphalou.Word> words = MorphalouG2Utils.keepOnlyOneInstanceOfAGraphie(Morphalou.parseCsv(path));


        int count = 0;
        int countOffset = 1000;
        int nextCount = 0;

        //for (Morphalou.Word word : words.subList(words.size() / 2, words.size())) {
        //for (Morphalou.Word word : words.subList(0,10)){
        for (Morphalou.Word word : words) {
            String graphie = word.getGraphie();
            try {
                if (!MorphalouG2Utils.blacklist.contains(graphie)) {
                    G2Result b = cachedG2.getContractedBraille(new G2SourceWord(graphie, null, null));
                }
            } catch (Exception e) {
                System.err.println("error on " + graphie + " word.flexion.id:" + word.flexion.id);
                e.printStackTrace();
            }
            count++;
            if (count >= nextCount) {
                nextCount += countOffset;
                System.out.println(count + "\t" + words.size() + "\t" + (100.0 * count / (words.size())) + "%" + "\t" + (new Date()));
            }
        }
        String filename = "g2-cache-natbraille-fr-g2.xml-all.json";
        exportJSONCacheFile(cachedG2, new FileOutputStream(filename));
        /*
        ObjectMapper mapper = new ObjectMapper();
        var outputStream = new FileOutputStream(filename);
        mapper.writeValue(outputStream,cachedG2.getCachePairs());
*/
        //Files.writeString(Path.of(filename), mapper.writeValueAsString(cachedG2.getCachePairs()));

    }

    public static void testCacheImportExport() throws Exception {
        {
            CachedG2 cachedG2 = new CachedG2(NatbrailleG2.build("nat://system/xsl/dicts/fr-g2.xml"));
            {
                G2Result b = cachedG2.getContractedBraille(new G2SourceWord("absolutisme", null, Arrays.asList(new Boolean[]{false, true, true})));
                System.out.println("[" + b.braille + "]");
                // System.out.println("[" + b.explanation + "]");
            }
            {
                G2Result b = cachedG2.getContractedBraille(new G2SourceWord("peu-à-peu", null, null));
                System.out.println("[" + b.braille + "]");
                // System.out.println("[" + b.explanation + "]");
            }
            {
                G2Result b = cachedG2.getContractedBraille(new G2SourceWord("coordination", null, null));
                System.out.println("[" + b.braille + "]");
                // System.out.println("[" + b.explanation + "]");
            }

            String filename = "g2-cache.json";
            ObjectMapper mapper = new ObjectMapper();
            Files.writeString(Path.of(filename), mapper.writeValueAsString(cachedG2.getCachePairs()));
        }
        {


            String filename = "g2-cache.json";
            String json = Files.readString(Path.of(filename));
            System.out.println(json);

            ObjectMapper mapper = new ObjectMapper();
            G2SourceResultPair[] pairs = mapper.readValue(json, G2SourceResultPair[].class);

            CachedG2 cachedG2 = new CachedG2(NatbrailleG2.build("nat://system/xsl/dicts/fr-g2.xml"));
            cachedG2.importCache(Arrays.asList(pairs));

            {
                G2Result b = cachedG2.getContractedBraille(new G2SourceWord("coordination", null, null));
                System.out.println("[" + b.braille + "]");
                // System.out.println("[" + b.explanation + "]");
            }
        }
    }

    public static void testNatbrailleG2Explanation() throws Exception {
        NatbrailleG2 natbrailleG2 = NatbrailleG2.build("nat://system/xsl/dicts/fr-g2.xml");
        G2Result b = natbrailleG2.getContractedBraille(new G2SourceWord("aalénien", null, null));
        System.out.println("[" + b.braille + "]");
        System.out.println("[" + b.explanation + "]");
    }

    /*

    Exception in thread "main" java.lang.OutOfMemoryError: UTF16 String size is 1324555741, should be less than 1073741823
        at java.base/java.lang.StringUTF16.newBytesFor(StringUTF16.java:50)

        at java.base/java.lang.StringUTF16.newBytesFor(StringUTF16.java:50)
        at java.base/java.lang.AbstractStringBuilder.inflate(AbstractStringBuilder.java:272)
        at java.base/java.lang.AbstractStringBuilder.appendChars(AbstractStringBuilder.java:1737)
        at java.base/java.lang.AbstractStringBuilder.append(AbstractStringBuilder.java:741)
        at java.base/java.lang.StringBuilder.append(StringBuilder.java:233)
        at com.fasterxml.jackson.core.util.TextBuffer.contentsAsString(TextBuffer.java:493)
        at com.fasterxml.jackson.core.io.SegmentedStringWriter.getAndClear(SegmentedStringWriter.java:99)
        at com.fasterxml.jackson.databind.ObjectMapper.writeValueAsString(ObjectMapper.java:3965)
        at org.natbraille.editor.server.faketranscriptor.g2.G2.buildACache(G2.java:431)
        at org.natbraille.editor.server.faketranscriptor.g2.G2.main(G2.java:388)
     */
    public static void testG2SourceWordKey() throws Exception {
        {
            Boolean[] emph = {true, false, true};

            G2SourceWord a = new G2SourceWord("bob", null, Arrays.asList(emph));
            System.out.println(a.toKey());

            G2SourceWord b = G2SourceWord.fromKey(a.toKey());
            System.out.println(b.letters);
            System.out.println(b.emphasis);
            System.out.println(b.toKey());
        }
        {
            G2SourceWord a = new G2SourceWord("bob", null, null);
            System.out.println(a.toKey());

            G2SourceWord b = G2SourceWord.fromKey(a.toKey());
            System.out.println(b.letters);
            System.out.println(b.emphasis);
            System.out.println(b.toKey());
        }

    }

    public static void testCache() throws Exception {
        {
            String table = "unicode.dis,fr-bfu-g2.ctb";
            String emphasisName = "italic";
            G2Translator liblouisG2 = new CachedG2(LiblouisG2.build(table, emphasisName));

            {
                G2Result a = liblouisG2.getContractedBraille(new G2SourceWord("Camion", null, null));
                System.out.println("[" + a.braille + "]");
            }
            {
                G2Result a = liblouisG2.getContractedBraille(new G2SourceWord("Camion", null, null));
                System.out.println("[" + a.braille + "]");
            }
        }
        {
            CachedG2 natbrailleG2 = new CachedG2(NatbrailleG2.build("nat://system/xsl/dicts/fr-g2.xml"));
            {
                G2Result b = natbrailleG2.getContractedBraille(new G2SourceWord("Camion", null, null));
                System.out.println("[" + b.braille + "]");
            }
            {
                G2Result b = natbrailleG2.getContractedBraille(new G2SourceWord("Camions", null, null));
                System.out.println("[" + b.braille + "]");
            }

            natbrailleG2.exportCache((g2SourceWord, result) -> {
                System.out.println(g2SourceWord + " " + result);
            });
        }
    }


    public static void testLiblouisAndNatbrailleG2() throws CompilationException, NatFilterException, TranslationException, DisplayException, NatDocumentException, IOException, TransformerException, CsvValidationException {


        // liblouis
        String table = "unicode.dis,fr-bfu-g2.ctb";
        String emphasisName = "italic";
        LiblouisG2 liblouisG2 = LiblouisG2.build(table, emphasisName);

        G2Result a = liblouisG2.getContractedBraille(new G2SourceWord("Camion", null, null));
        System.out.println("[" + a.braille + "]");

        // natbraille

        NatbrailleG2 natbrailleG2 = NatbrailleG2.build("nat://system/xsl/dicts/fr-g2.xml");
        G2Result b = natbrailleG2.getContractedBraille(new G2SourceWord("Camion", null, null));
        System.out.println("[" + b.braille + "]");

        G2Result b0 = natbrailleG2.getContractedBraille(new G2SourceWord("Canon", null, null));
        System.out.println("[" + b0.braille + "]");

        final String path = "/home/vivien/pr-src/doc-g2/morphalou/morphalou/5/Morphalou3.1_formatCSV_toutEnUn/Morphalou3.1_CSV.csv";
        List<Morphalou.Word> words = MorphalouG2Utils.keepOnlyOneInstanceOfAGraphie(Morphalou.parseCsv(path));
        int count = 0;
        Estimator estimator = new Estimator(words.size());
        for (Morphalou.Word word : words) {
            String graphie = word.getGraphie();
            if (!MorphalouG2Utils.blacklist.contains(graphie)) {
                String liblouisResult = liblouisG2.getContractedBraille(new G2SourceWord(graphie, null, null)).braille;
                String natbrailleResult = natbrailleG2.getContractedBraille(new G2SourceWord(graphie, null, null)).braille;
                estimator.setDone(count);
                StringBuilder sb = new StringBuilder();
                sb.append("[").append(count).append("/").append(words.size()).append("]");
                sb.append("\t");
                sb.append(estimator.getPercentDone());
                sb.append("\t");
                sb.append(estimator.getRemainingSeconds());
                sb.append("\t");
                sb.append(graphie);
                sb.append("\t");
                sb.append(liblouisResult);
                sb.append("\t");
                sb.append(natbrailleResult);
                sb.append("\n");
                System.out.print(sb);
            }
            count++;

        }


        //    return new G2(liblouisG2);
    }
}
/*
[0/726622]	0.0%	Infinitys	aalénien	aalén.	⠁⠁⠇⠿⠝⠊⠢
[1/726622]	1.376231383029966E-4%	1.8207656669730212E11s	aaléniens	aalén.s	⠁⠁⠇⠿⠝⠲⠎
[2/726622]	2.752462766059932E-4%	9.103817238155011E10s	aalénienne	aalén.ne	⠁⠁⠇⠿⠝⠲⠝⠑
[3/726622]	4.128694149089898E-4%	6.069204110699999E10s	aaléniennes	aalén.nû	⠁⠁⠇⠿⠝⠲⠝⠱
[4/726622]	5.504925532119864E-4%	4.551897461914383E10s	aba	aba	⠁⠃⠁
[5/726622]	6.881156915149831E-4%	3.6415134726814E10s	abas	abas	⠁⠃⠁⠎
[6/726622]	8.257388298179796E-4%	3.0345908267833538E10s	abaca	abaca	⠁⠃⠁⠉⠁
[7/726622]	9.633619681209762E-4%	2.601074630941957E10s	abacas	abacas	⠁⠃⠁⠉⠁⠎
[8/726622]	0.0011009851064239728%	2.2759375064699192E10s	abaddir	abaddir	⠁⠃⠁⠙⠙⠊⠗
*/

// [726621/726622]	99.9998623768617%	0.3675764593780775s	à-tout-va	⠷⠤⠡⠤⠧⠁	⠧⠁
// BUILD SUCCESSFUL in 4h 32m 50s