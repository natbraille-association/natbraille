package org.natbraille.editor.server.formats;

import java.util.Properties;

public class DetranscriptionRequest {
    public String brailleString;
    public Properties filterOptions;
}
