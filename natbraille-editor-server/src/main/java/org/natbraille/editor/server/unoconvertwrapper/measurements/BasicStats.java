package org.natbraille.editor.server.unoconvertwrapper.measurements;

import java.util.List;

public class BasicStats {
    final Double max;
    final Double min;
    final Double avg;
    final Double med;

    public static double getMedian(List<Double> values) {
        List<Double> sortedNumbers = values.stream().sorted().toList();
        int size = sortedNumbers.size();
        if (size % 2 == 1) {
            // Odd number of elements, return the middle one
            return sortedNumbers.get(size / 2);
        } else {
            // Even number of elements, return the average of the two middle ones
            double middle1 = sortedNumbers.get(size / 2 - 1);
            double middle2 = sortedNumbers.get(size / 2);
            return (middle1 + middle2) / 2.0;
        }
    }

    public BasicStats(List<Double> values) {
        if (values.isEmpty()) {
            max = null;
            min = null;
            avg = null;
            med = null;
        } else {
            this.max = values.stream().max(Double::compareTo).get();
            this.min = values.stream().min(Double::compareTo).get();
            this.avg = values.stream().mapToDouble(Double::doubleValue).average().getAsDouble();
            this.med = getMedian(values);
        }
    }
}
