package org.natbraille.editor.server.users;

import org.natbraille.editor.server.DatabaseManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.OffsetDateTime;

public class TokensDB implements Tokens {
    private static Logger LOGGER = LoggerFactory.getLogger(TokensDB.class);
    private final DatabaseManager databaseManager;

    public TokensDB() throws SQLException {
        this.databaseManager = DatabaseManager.getInstance();
    }

    @Override
    public void persistTokens(LiveTokens liveTokens) {
        {
            String sql = "DELETE FROM tokens";
            try (Connection connection = databaseManager.getConnection(); PreparedStatement ps = connection.prepareStatement(sql)) {
                ps.executeUpdate();
            } catch (SQLException e) {
                LOGGER.error("cannot delete tokens ", e);
            }
        }
        {
            String sql = "INSERT INTO tokens (token, username, created, lastUsed) VALUES (?, ?, ?, ?)";
            try (Connection connection = databaseManager.getConnection(); PreparedStatement ps = connection.prepareStatement(sql)) {
                for (var entry : liveTokens.tokens.entrySet()) {
                    ps.setString(1, entry.getKey());
                    ps.setString(2, entry.getValue().username);
                    ps.setObject(3, entry.getValue().created);
                    ps.setObject(4, entry.getValue().lastUsed);
                    ps.addBatch();
                }
                ps.executeBatch();
            } catch (SQLException e) {
                LOGGER.error("cannot save tokens ", e);
            }
        }
    }

    @Override
    public void loadTokens(LiveTokens liveTokens) {
        String sql = "SELECT token, username, created, lastUsed FROM tokens";
        int loadedCount = 0;
        try (Connection connection = databaseManager.getConnection(); PreparedStatement ps = connection.prepareStatement(sql)) {
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    String token = rs.getString("token");
                    String username = rs.getString("username");
                    OffsetDateTime created = rs.getObject("created", OffsetDateTime.class);
                    OffsetDateTime lastModified = rs.getObject("created", OffsetDateTime.class);
                    liveTokens.load(token, username, created, lastModified);
                    loadedCount++;
                }
            } catch (SQLException e) {
                LOGGER.error("cannot execute tokens restore", e);
            }
        } catch (SQLException e) {
            LOGGER.error("cannot restore tokens", e);
        }
        LOGGER.info("restored {} tokens", loadedCount);
    }
}
