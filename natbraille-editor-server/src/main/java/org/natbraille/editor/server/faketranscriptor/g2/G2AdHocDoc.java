package org.natbraille.editor.server.faketranscriptor.g2;

import org.natbraille.editor.server.tools.SimpleXMLString;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class G2AdHocDoc {
    private static final String MotFragmentIdPrefix = "mot_";

    public static String docDocFromWord(String word) {
        return docDocFromWord(word, "1");
    }

    public static String docDocFromWord(String word, String wordId) {
        String motId = MotFragmentIdPrefix + wordId;
        return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                + "<!DOCTYPE doc:doc SYSTEM \"nat://system/xsl/mmlents/windob.dtd\">"
                + "<doc:doc xmlns:doc=\"espaceDoc\">"
                + "<phrase fragmentId=\"" + 1 + "\">"
                + "<lit>"
                + "<mot fragmentId=\"" + motId + "\">" + SimpleXMLString.encodeXML(word) + "</mot>"
                + "</lit>"
                + "</phrase>"
                + "</doc:doc>";
    }

    public static String docDocFromWordAndFollowingPunctuation(String word, String wordId, String punctuation) {
        String motId = MotFragmentIdPrefix + wordId;
        String punctuationElement = ((punctuation != null) && (!punctuation.isEmpty()))
                ? ("<ponctuation>" + SimpleXMLString.encodeXML(punctuation) + "</ponctuation>")
                : "";

        return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                + "<!DOCTYPE doc:doc SYSTEM \"nat://system/xsl/mmlents/windob.dtd\">"
                + "<doc:doc xmlns:doc=\"espaceDoc\">"
                + "<phrase fragmentId=\"" + 1 + "\">"
                + "<lit>"
                + "<mot fragmentId=\"" + motId + "\">" + SimpleXMLString.encodeXML(word) + "</mot>"
                + punctuationElement
                + "</lit>"
                + "</phrase>"
                + "</doc:doc>";
    }

    public static String docDocFromWords(String[] words) {
        // TODO : escape forbidden xml chars in word
        StringBuilder sb = new StringBuilder();
        sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                + "<!DOCTYPE doc:doc SYSTEM \"nat://system/xsl/mmlents/windob.dtd\">"
                + "<doc:doc xmlns:doc=\"espaceDoc\">");

        int idCounter = 0;
        for (String word : words) {
            String phraseId = "phrase_" + Integer.toString(idCounter);
            String motId = MotFragmentIdPrefix + Integer.toString(idCounter);
            sb.append("<phrase fragmentId=\"").append(phraseId).append("\">")
                    .append("<lit>")
                    .append("<mot fragmentId=\"").append(motId).append("\">").append(SimpleXMLString.encodeXML(word)).append("</mot>")
                    .append("</lit>")
                    .append("</phrase>");
            idCounter++;
        }
        sb.append("</doc:doc>");
        return sb.toString();
    }

    public static String docDocFromMorphalou(List<Morphalou.Word> words) {
        // TODO : escape forbidden xml chars in word
        StringBuilder sb = new StringBuilder();
        sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                + "<!DOCTYPE doc:doc SYSTEM \"nat://system/xsl/mmlents/windob.dtd\">"
                + "<doc:doc xmlns:doc=\"espaceDoc\">");

        int idCounter = 0;
        for (Morphalou.Word word : words) {
            // String phraseId = "phrase_" + Integer.toString(idCounter);
            // String motId = phraseId + "_" + "mot_" + word.flexion.id;  // Integer.toString(idCounter);
            String phraseId = "phrase_" + word.flexion.id;
            String motId = MotFragmentIdPrefix + word.flexion.id;
//            idCounter++;
         /*   if (word.getGraphie().contains("&")) {
                continue;
            }
            if (blacklist.contains(word.getGraphie())) {
                continue;
            }
            if (word.getGraphie().length() > 24) {
                continue;
            }*/
            String graphie = word.getGraphie();
            sb.append("<phrase fragmentId=\"").append(phraseId).append("\">")
                    .append("<lit>")
                    .append("<mot fragmentId=\"").append(motId).append("\">")
                    .append(SimpleXMLString.encodeXML(graphie))
                    .append("</mot>")
                    .append("</lit>")
                    .append("</phrase>")
                    .append("\n");
        }
        sb.append("</doc:doc>");
        return sb.toString();
    }

    /*
     * <par fragmentId="phrase_1" modeBraille="3-1">
     *  <word fragmentId="mot_1" source="black">
     *    <black>une belle histoire</black>
     *     <info fragmentId="mot_1">une belle histoire</info>
     *      <braille fragmentId="mot_1">⠥⠝⠑ ⠃⠑⠸⠑ ⴹ⠓⠞</braille>
     *    </word>
     *     <spacing/>
     *   </par>
     */
    public static Map<String, String> extractBrailleStringsFromOutputDocument(Document outDomDoc) {
        Map<String, String> idBraille = new HashMap<>();
        NodeList brailleNodes = outDomDoc.getElementsByTagName("braille");
        for (int i = 0; i < brailleNodes.getLength(); i++) {
            Element brailleNode = (Element) brailleNodes.item(i);
            String fragmentId = brailleNode.getAttribute("fragmentId");
            if (!fragmentId.isEmpty()) {
                String id = fragmentId.substring(MotFragmentIdPrefix.length());
                String brailleString = brailleNode.getTextContent();
                idBraille.put(id, brailleString);
            }
//            System.out.println(fragmentId + "|" + id + "|" + brailleString);
        }
        return idBraille;
    }
}
