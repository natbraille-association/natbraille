package org.natbraille.editor.server;

import jakarta.mail.Message;
import jakarta.mail.MessagingException;
import jakarta.mail.Session;
import jakarta.mail.Transport;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class RegistrationEmailSender {
    private static final Logger LOGGER = LoggerFactory.getLogger(RegistrationEmailSender.class);
    private final Session session;

    public RegistrationEmailSender() {
        HashMap<String, String> smtpConfiguration = Server.serverConfiguration.smtp;
        Properties props = new Properties();
        if (smtpConfiguration != null) {
            for (String propName : smtpConfiguration.keySet()) {
                props.put(propName, smtpConfiguration.get(propName));
            }
        }
        session = Session.getInstance(props, null);
    }

    public interface MessageTextBuilder {
        String buildSubject();

        String buildBody();
    }

    static class RegistrationMessageTextBuilder implements MessageTextBuilder {
        private final String url;

        RegistrationMessageTextBuilder(String url) {
            this.url = url;
        }

        public String buildSubject() {
            return "Confirmation de la creation d'un compte Natbraille";
        }

        public String buildBody() {
            String[] lines = {
                    "Vous avez demandé la création d'un compte pour le serveur Natbraille.",
                    "Si vous n'êtes pas à l'origine de cette demande, veuillez ignorer ce message.",
                    "Pour confirmer la creation de votre compte, veuillez suivre le lien suivant: " + url
            };
            return String.join("\n", lines);
        }
    }

    static class PasswordResetMessageTextBuilder implements MessageTextBuilder {
        private final String url;

        PasswordResetMessageTextBuilder(String url) {
            this.url = url;
        }

        public String buildSubject() {
            return "Confirmation de la réinitialisation de votre mot de passe Natbraille";
        }

        public String buildBody() {
            String[] lines = {
                    "Vous avez demandé la réinitialisation du mot de passe de votre compte sur le serveur Natbraille.",
                    "Si vous n'êtes pas à l'origine de cette demande, veuillez ignorer ce message.",
                    "Pour confirmer la réinitialisation du mot de passe, veuillez suivre le lien suivant: " + url
            };
            return String.join("\n", lines);
        }
    }

    static class AccountRemovalMessageTextBuilder implements MessageTextBuilder {
        private final String url;
        private final String username;
        public AccountRemovalMessageTextBuilder(String url,String username) {
            this.url = url;
            this.username = username;
        }
        public String buildSubject(){ return "Confirmation de suppression de votre compte Natbraille";}
        public String buildBody() {
            String[] lines = {
                    "Vous avez demandé la suppression de votre compte '"+username+"' sur le serveur Natbraille.",
                    "Si vous n'êtes pas à l'origine de cette demande, veuillez ignorer ce message.",
                    "Pour confirmer la suppression de votre compte, veuillez suivre le lien suivant: " + url,
                    "ATTENTION ! En suivant ce lien, votre compte sera supprimé définitivement."
            };
            return String.join("\n", lines);
        }
    }

    private static String buildRecipient(String username, String email) {
        return email;
    }

    public void sendChallengeURL(String username, String email, MessageTextBuilder messageTextBuilder) throws MessagingException, UnsupportedEncodingException {

        String fromAddress = Server.serverConfiguration.email.fromAddress;
        String fromName = Server.serverConfiguration.email.fromName;
        String replyTo = Server.serverConfiguration.email.replyTo;

        MimeMessage msg = new MimeMessage(session);
        //set message headers
        msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
        msg.addHeader("format", "flowed");
        msg.addHeader("Content-Transfer-Encoding", "8bit");

        msg.setFrom(new InternetAddress(fromAddress, fromName));
        msg.setReplyTo(InternetAddress.parse(replyTo, false));

        msg.setSubject(messageTextBuilder.buildSubject(), "UTF-8");
        msg.setText(messageTextBuilder.buildBody(), "UTF-8");
        msg.setSentDate(new Date());
        msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(buildRecipient(username, email), false));
        LOGGER.info("Message is ready {}, {}", username, email);
        Transport.send(msg);
        LOGGER.debug("EMail Sent to {}, {}, from {} <{}>, reply-to {}", username, email, fromName, fromAddress, replyTo);
    }


}
