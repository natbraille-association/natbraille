package org.natbraille.editor.server;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.liblouis.CompilationException;
import org.natbraille.core.filter.NatFilterException;
import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.editor.server.faketranscriptor.FakeApiTranscriptor;
import org.natbraille.editor.server.faketranscriptor.XmlUtils;
import org.natbraille.editor.server.faketranscriptor.g2.G2;
import org.natbraille.editor.server.faketranscriptor.tan.NatbrailleTan;
import org.natbraille.editor.server.formats.*;
import org.natbraille.editor.server.formats.nfometa.FoNatFilterOptionsDetail;
import org.natbraille.editor.server.importexport.NoirToOpenDocument;
import org.natbraille.editor.server.importexport.OpenDocumentToNoir;
import org.natbraille.editor.server.importexport.StylistWrapper;
import org.natbraille.editor.server.performance.DurationMeasurementsMap;
import org.natbraille.editor.server.tables.SystemBrailleTablesWrapper;
import org.natbraille.editor.server.tools.B64URIComponent;
import org.natbraille.editor.server.tools.RandomString;
import org.natbraille.editor.server.unoconvertwrapper.MultiUnoConvert;
import org.natbraille.editor.server.userDocuments.UserDocuments;
import org.natbraille.editor.server.userDocuments.UserDocumentsDB;
import org.natbraille.editor.server.userDocuments.UserDocumentsFilesystem;
import org.natbraille.editor.server.users.*;
import org.natbraille.editor.server.version.GitVersion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import spark.Request;
import spark.Response;
import spark.Spark;

import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.natbraille.editor.server.ServerConfigurationReader.getServerConfiguration;
import static spark.Spark.*;

// https://stackoverflow.com/questions/16558869/getting-ip-address-of-client
public class Server {

    private static final Logger LOGGER = LoggerFactory.getLogger(Server.class);
    public static ServerConfiguration serverConfiguration = getServerConfiguration();
    // public static final DurationMeasurements executionTimes = new DurationMeasurements("whole_pass");
    public static final DurationMeasurementsMap perUserExecutionTimes = new DurationMeasurementsMap();

    private static String getClientIp(Request request) {
        String xForwardedFor = request.headers("X-Forwarded-For");
        if (xForwardedFor != null) {
            return xForwardedFor;
        }
        return request.ip();
    }

    public static void startup() {

        // natbraille specifics
        System.setProperty("javax.xml.parsers.DocumentBuilderFactory",
                "org.apache.xerces.jaxp.DocumentBuilderFactoryImpl");
        System.setProperty("javax.xml.xpath.XPathFactory:http://java.sun.com/jaxp/xpath/dom",
                "com.sun.org.apache.xpath.internal.jaxp.XPathFactoryImpl");

        // network config
        if ((serverConfiguration.host != null) && (!serverConfiguration.host.isEmpty())) {
            Spark.ipAddress(serverConfiguration.host);
        }
        if ((serverConfiguration.port != null) && (!serverConfiguration.port.isEmpty())) {
            Spark.port(Integer.parseInt(serverConfiguration.port));
        }

        // CorsFilter.apply(); // Call this before mapping thy routes

        // CORS config (usefull when the ui webserver is elsewhere
        Spark.options("/*",
                (request, response) -> {

                    String accessControlRequestHeaders = request.headers("Access-Control-Request-Headers");
                    if (accessControlRequestHeaders != null) {
                        response.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
                    }

                    String accessControlRequestMethod = request.headers("Access-Control-Request-Method");
                    if (accessControlRequestMethod != null) {
                        response.header("Access-Control-Allow-Methods", accessControlRequestMethod);
                    }

                    return "OK";
                });

        Spark.exception(Exception.class, (exception, request, response) -> {
            LOGGER.error("Error in service", exception);
        });
        Spark.before((request, response) -> {
            // System.out.println("## "+String.format("%s %s", request.requestMethod(), request.url()));
            response.header("Access-Control-Allow-Origin", "*");
        });
        Spark.after((request, response) -> {
            String clientIp = getClientIp(request);
            LOGGER.info(String.format("%s - %s %s", clientIp, request.requestMethod(), request.url()));
        });
    }


    private static UserDocuments getUserDocuments() throws SQLException {
        if (serverConfiguration.useDb) {
            LOGGER.info("use db for UserDocuments");
            return new UserDocumentsDB();
        } else {
            LOGGER.info("use filesystem for UserDocuments");
            return new UserDocumentsFilesystem();
        }
    }

    private static Users getUsers() throws SQLException {
        if (serverConfiguration.useDb) {
            LOGGER.info("use db for Users");
            return new UsersDB();
        } else {
            LOGGER.info("use anon for Users");
            return new UserAnon();
        }
    }

    private static void removeAllCookies(Response res) {
        res.removeCookie("/", "natbraille-token");
        res.removeCookie("/", "natbraille-username");
        res.removeCookie("/", "natbraille-register-email-send");
        res.removeCookie("/", "natbraille-bad-login");
    }

    public static String encodeCookie(String value) {
        return B64URIComponent.encode(value);
        // if (value == null) return null;
        // String urlEncoded = encodeURIComponent(value);
        // return Base64.getEncoder().encodeToString(urlEncoded.getBytes());
    }

    public static String decodeCookie(String value) {
        return B64URIComponent.decode(value);
        // if (value == null) return null;
        // byte[] b64decoded = Base64.getDecoder().decode(value);
        // String s = new String(b64decoded, StandardCharsets.UTF_8);
        // return decodeURIComponent(s);
    }

    public static String getCookie(Request req, String cookieName) {
        return decodeCookie(req.cookie(cookieName));
    }

    private static void setDomainWideCookie(Response res, String name, String value) {
        // maxAge -1 : non-persistent
        // secured false : do not restrict to secured connections
        String encodedValue = encodeCookie(value);
        res.cookie("/", name, encodedValue, -1, false);
    }

    private static String getHeaderUsername(LiveTokens liveTokens, Request req) {
        return req.headers("X-Natbraille-Api-Username");
    }

    private static boolean checkRequestLiveToken(LiveTokens liveTokens, Request req) {
        if (serverConfiguration.useUniversalApiToken) {
            return true;
        }
        String headerToken = req.headers("X-Natbraille-Api-Token");
        String headerUsername = req.headers("X-Natbraille-Api-Username");
        LOGGER.info("headerToken {} headerUsername {}", headerToken, headerUsername);
        return liveTokens.checkUsername(headerToken, headerUsername);
        // String username = req.cookie("natbraille-username");
        // String token = req.cookie("natbraille-token");
        // LOGGER.info("token check {} for {}", token, username);
        // return liveTokens.checkUsername(token, username);
    }

    private static String apiTokenError(Response res) throws JsonProcessingException {
        res.type("application/json");
        Map<String, String> errorMap = new HashMap<>();
        errorMap.put("error", "wrong or missing api token");
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(errorMap);
    }

    private static void ensureDirectoryCreated(Path directory) throws IOException {
        if (!Files.exists(directory)) {
            Files.createDirectories(directory);
            LOGGER.info("created directory" + directory.toAbsolutePath());
        }
    }
    private static String buildMailChallengeUrl(String path,String username, String rawChallenge){
        return serverConfiguration.publicServerURLForExternalLinks
                + path
                +"?"
                + "username=" + URLEncoder.encode(username, StandardCharsets.UTF_8)
                + "&"
                + "challenge=" + URLEncoder.encode(rawChallenge, StandardCharsets.UTF_8);
    }
    public static void main(String[] args) throws IOException, SQLException {

        String encoded = encodeCookie("✓ à la mode");
        System.out.println("encode cookie " + encoded);
        String decoded = decodeCookie(encoded);
        System.out.println("decode cookie " + decoded);

        MultiUnoConvert multiUnoConvert = new MultiUnoConvert();
        multiUnoConvert.start();

        UserDocuments userDocuments = getUserDocuments();
        RandomString rawChallengeGenerator = new RandomString();
        Users users = getUsers();

        // tokens
        Tokens tokens = null;
        LiveTokens liveTokens = new LiveTokens();
        {
            if (serverConfiguration.useDb) {
                tokens = new TokensDB();
                tokens.loadTokens(liveTokens);
            }
        }

        RegistrationEmailSender registrationEmailSender = new RegistrationEmailSender();

        if (multiUnoConvert.isUnoStarMathToMathMLOk()) {
            //if (UnoConvert.isUnoStarMathToMathMLOk()) {
            LOGGER.info("✓ Uno StarMath -> MathML conversion ok");
        } else {
            LOGGER.error("✗ Uno StarMath -> MathML conversion NOT working");
        }

        // write g2 cache on shutdown
        ensureDirectoryCreated(Path.of(serverConfiguration.g2CacheRoot));

        // create g2 cache optionally using available filesystem cache
        {
            G2.G2Translator cachedG2 = null;
            try {
                cachedG2 = G2.G2Translators.getG2Translator(G2.Origin.Natbraille, "nat://system/xsl/dicts/fr-g2.xml", true);
            } catch (NatFilterException | CompilationException e) {
                throw new RuntimeException(e);
            }
            /*
            {
                G2.G2SourceWord word = new G2.G2SourceWord("c'est-à-dire", null, null);
                // ⠅ ⴹ⠉⠴⠗  // au contraire
                // ⴹ⠅ // au
                // ⴹ⠉⠴⠗ // contraire
                //G2.G2SourceWord word = new G2.G2SourceWord("c'est-à-dire", null);
                G2.G2Result contracted = null;
                try {
                    contracted = cachedG2.getContractedBraille(word);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
                System.out.println(contracted.braille);
            }
             */
        }
        LOGGER.info(GitVersion.getDisplayVersion());
        LOGGER.info("config {}", ServerConfiguration.getAsJsonString(serverConfiguration));
        webSocket("/api/transcription", TranscriptionSocket.class);

        if (!serverConfiguration.staticFilesDirectory.isEmpty()) {
            LOGGER.info("use external static files directory {}", serverConfiguration.staticFilesDirectory);
            staticFiles.externalLocation(serverConfiguration.staticFilesDirectory);
        }

        startup();

        //      awaitInitialization();

        get("/api/ping", (req, res) -> "pong");


        get("/api/version", (req, res) -> {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(GitVersion.getStructuredGitVersion());
//            return GitVersion.getDisplayVersion();
        });
        /*
        // it would return db password
        get("/api/server-configuration", (req, res) -> {
            ObjectMapper mapper = new ObjectMapper();
            String configString = mapper.writeValueAsString(serverConfiguration);
            res.type("application/json");
            return configString;
        });
        */
        path("/api/auth", () -> {
            get("/check-live-token", (req, res) -> {
                if (checkRequestLiveToken(liveTokens, req)) {
                    ObjectMapper mapper = new ObjectMapper();
                    return mapper.writeValueAsString(new HashMap<>());
                } else {
                    removeAllCookies(res);
                    return apiTokenError(res);
                }
            });
            post("/login", (req, res) -> {
                String username = req.queryParams("username");
                String password = req.queryParams("password");
                LOGGER.info("user login {}", username /*, "<password>"*/);
                removeAllCookies(res);
                boolean passwordMatch = users.userPasswordMatch(username, password);
                if (passwordMatch) {
                    Map.Entry<String, LiveTokens.TokenInfo> token = liveTokens.create(username);
                    setDomainWideCookie(res, "natbraille-token", token.getKey());
                    setDomainWideCookie(res, "natbraille-username", username);
                    res.redirect(serverConfiguration.devURL + "/editor");
                } else {
                    setDomainWideCookie(res, "natbraille-bad-login", "error");
                    LOGGER.info("password failed {}", username);
                    res.redirect(serverConfiguration.devURL + "/login");
                }
                return "noop";
            });
            post("/register", (req, res) -> {
                String username = req.queryParams("username");
                String email = req.queryParams("email");
                String password = req.queryParams("password");
                users.addUserRegistration(username, password, email);
                LOGGER.info("user register {} {} {}", username, email, "<password>");
                removeAllCookies(res);
                setDomainWideCookie(res, "natbraille-register-email-send", email);
                setDomainWideCookie(res, "natbraille-username", username);
                res.redirect(serverConfiguration.devURL + "/login");
                return "noop";
            });
            post("/register-send-email", (req, res) -> {
                String username = getCookie(req, "natbraille-username");
                String email = getCookie(req, "natbraille-register-email-send");
                String rawChallenge = rawChallengeGenerator.nextString();
                /*
                String url = serverConfiguration.publicServerURLForExternalLinks
                        + "/login?"
                        + "username=" + URLEncoder.encode(username, StandardCharsets.UTF_8)
                        + "&"
                        + "challenge=" + URLEncoder.encode(rawChallenge, StandardCharsets.UTF_8);
                */
                String url = buildMailChallengeUrl("/login",username,rawChallenge);
                registrationEmailSender.sendChallengeURL(username, email, new RegistrationEmailSender.RegistrationMessageTextBuilder(url));
                users.setUserRegistrationChallenge(username, rawChallenge);
                LOGGER.info("user register send email {} {}", email, rawChallenge);
                removeAllCookies(res);
                res.redirect(serverConfiguration.devURL + "/login");
                return "noop";
            });
            post("/challenge", (req, res) -> {
                String username = req.queryParams("username");
                String rawChallenge = req.queryParams("challenge");
                boolean match = users.registrationChallengeMatch(username, rawChallenge);
                LOGGER.info("user register send confirmation challenge match? {} {} {}", username, rawChallenge, match);
                if (match) {
                    if (users.addRegisteredUser(username)) {
                        users.removeRegistration(username);
                    }
                }
                removeAllCookies(res);
                res.redirect(serverConfiguration.devURL + "/login");
                return "noop";
            });
            get("/reset", (req, res) -> {
                // just to clear cookies after failed registration
                LOGGER.info("user rest");
                removeAllCookies(res);
                res.redirect(serverConfiguration.devURL + "/register");
                return "noop";
            });

            post("/logout", (req, res) -> {
                String token = getCookie(req, "natbraille-token");
                LOGGER.info("user logout {} {}", getCookie(req, "natbraille-token"), getCookie(req, "natbraille-username"));
                liveTokens.remove(token);
                removeAllCookies(res);
                res.redirect(serverConfiguration.devURL + "/login");
                return "noop";
            });

            post("/request-password-reset", (req, res) -> {
                removeAllCookies(res);
                String username = req.queryParams("username");
                Optional<String> email = users.getEmailForUsername(username);
                LOGGER.info("user {} request password reset with email ?{} : {} ", username, email.isPresent(), email.orElse(""));

                if (email.isPresent()) {
                    String rawChallenge = rawChallengeGenerator.nextString();
                    /*
                    String url = serverConfiguration.publicServerURLForExternalLinks
                            + "/password-reset?"
                            + "username=" + URLEncoder.encode(username, StandardCharsets.UTF_8)
                            + "&"
                            + "challenge=" + URLEncoder.encode(rawChallenge, StandardCharsets.UTF_8);
                    */
                    String url = buildMailChallengeUrl("/password-reset",username,rawChallenge);
                    users.setUserPasswordResetRequestChallenge(username, rawChallenge);
                    registrationEmailSender.sendChallengeURL(username, email.get(), new RegistrationEmailSender.PasswordResetMessageTextBuilder(url));
                }

                res.redirect(serverConfiguration.devURL + "/request-password-reset?"
                        + "email=" + URLEncoder.encode(MaskEmailAddress.mask(email.orElse("")), StandardCharsets.UTF_8)
                        + "&"
                        + "username=" + URLEncoder.encode((username == null) ? "" : username, StandardCharsets.UTF_8)
                );
                return "noop";
            });
            post("/password-reset", (req, res) -> {
                removeAllCookies(res);
                String username = req.queryParams("username");
                String rawChallenge = req.queryParams("challenge");
                String rawPassword = req.queryParams("password");
                boolean success = users.resetUserPassword(username, rawChallenge, rawPassword);
                if (success) {
                    res.redirect(serverConfiguration.devURL + "/login");
                } else {
                    res.redirect(serverConfiguration.devURL + "/request-password-request");
                }
                return "noop";
            });

            get("/user-email", (req, res) -> {
                if (checkRequestLiveToken(liveTokens, req)) {
                    String username = getHeaderUsername(liveTokens, req);
                    Optional<String> maybeEmail = users.getEmailForUsername(username);
                    Map<String, String> map = new HashMap<>();
                    map.put("email", maybeEmail.orElse(""));
                    ObjectMapper mapper = new ObjectMapper();
                    return mapper.writeValueAsString(map);
                } else {
                    return apiTokenError(res);
                }
            });
            post("/account-removal-request", (req, res) -> {
                if (checkRequestLiveToken(liveTokens, req)) {
                    String username = getHeaderUsername(liveTokens, req);
                    Optional<String> email = users.getEmailForUsername(username);
                    LOGGER.info("user {} request account removal with email ?{} : {} ", username, email.isPresent(), email.orElse(""));
                    Map<String, String> map = new HashMap<>();
                    try {
                        // write email to user
                        {
                            // create challenge
                            String rawChallenge = rawChallengeGenerator.nextString();
                            /*String url = serverConfiguration.publicServerURLForExternalLinks
                                    + "/account-removal?"
                                    + "username=" + URLEncoder.encode(username, StandardCharsets.UTF_8)
                                    + "&"
                                    + "challenge=" + URLEncoder.encode(rawChallenge, StandardCharsets.UTF_8);

                             */
                            String url = buildMailChallengeUrl("/account-removal",username,rawChallenge);
                            // write challenge in db
                            users.setAccountRemovalRequestChallenge(username, rawChallenge);
                            // write registration email
                            registrationEmailSender.sendChallengeURL(username, email.get(), new RegistrationEmailSender.AccountRemovalMessageTextBuilder(url, username));
                        }
                        {
                            map.put("username", username);
                            map.put("email", email.orElse(""));
                            // return {username,email}
                        }
                    } catch (Exception e) {
                        LOGGER.error("cannot account-removal-request", e);
                        map.put("error", "error");
                    }
                    ObjectMapper mapper = new ObjectMapper();
                    return mapper.writeValueAsString(map);
                } else {
                    return apiTokenError(res);
                }
            });
            post("/account-removal", (req, res) -> {
//                if (checkRequestLiveToken(liveTokens, req)) {
                    try {
                        String username = req.queryParams("username");
                        String rawChallenge = req.queryParams("challenge");
                        boolean match = users.accountRemovalChallengeMatch(username, rawChallenge);
                        LOGGER.info("account removal confirmation challenge match? {} {} {}", username, rawChallenge, match);
                        if (match){
                            users.removeUser(username);
                            userDocuments.deleteUserDocuments(username);
                        }
                        removeAllCookies(res);
                    } catch (Exception e) {
                        LOGGER.error("account-removal-request", e);
                    }
  //              }
                res.redirect(serverConfiguration.devURL + "/");
                return "";
            });
        });

        path("/api/users/:username", () ->
        {
            get("/archive.zip", (req, res) -> {
                String username = B64URIComponent.decode(req.params(":username"));
                return userDocuments.loadArchive(username);
            });
            get("/:kind", (req, res) -> {
                String username = B64URIComponent.decode(req.params(":username"));
                String kind = B64URIComponent.decode(req.params(":kind"));
                ObjectMapper mapper = new ObjectMapper();
                return mapper.writeValueAsString(userDocuments.listDocuments(username, kind));
            });
            post("/:kind/:filename", (req, res) -> {
                String username = B64URIComponent.decode(req.params(":username"));
                String kind = B64URIComponent.decode(req.params(":kind"));
                String filename = B64URIComponent.decode(req.params(":filename"));
                return userDocuments.saveDocument(username, kind, filename, req.bodyAsBytes());
            });
            get("/:kind/:filename", (req, res) -> {
                String username = B64URIComponent.decode(req.params(":username"));
                String kind = B64URIComponent.decode(req.params(":kind"));
                String filename = B64URIComponent.decode(req.params(":filename"));
                res.type("application/zip");
                return userDocuments.loadDocument(username, kind, filename);
            });
            delete("/:kind/:filename", (req, res) -> {
                String username = B64URIComponent.decode(req.params(":username"));
                String kind = B64URIComponent.decode(req.params(":kind"));
                String filename = B64URIComponent.decode(req.params(":filename"));
                ObjectMapper mapper = new ObjectMapper();
                return userDocuments.deleteDocument(username, kind, filename);
            });
        });

        get("/api/hello/:username", (req, res) ->

        {
            String username = req.params(":username");
            return "Hello World " + username;
        });
        get("/api/braille-tables", (req, res) ->
                SystemBrailleTablesWrapper.getSystemBrailleTablesJsonCache().toString()
        );
        post("/api/to-braille", (req, res) -> {
            if (checkRequestLiveToken(liveTokens, req)) {
                ObjectMapper mapper = new ObjectMapper();
                TranscriptionRequest transcriptionRequest = mapper.readValue(req.body(), TranscriptionRequest.class);
                //          System.out.println("got source document" + transcrip.xmlString);
//            System.out.println("got options" + transcrip.filterOptions);

                //Files.writeString(Path.of("/home/vivien/Bureau/imports/to-braille.xml"), transcriptionRequest.xmlString);

                FakeApiTranscriptor t = new FakeApiTranscriptor();
                //DurationMeasurement measure0 = executionTimes.startMeasure();
                DurationMeasurementsMap.StartedMapMeasurement measure1 = perUserExecutionTimes.startMeasure(getHeaderUsername(liveTokens, req));
                AllTranscriptionSteps result = t.transcribe(transcriptionRequest);
                //executionTimes.endMeasure(measure0);
                perUserExecutionTimes.endMeasure(measure1);

                return mapper.writeValueAsString(result);
            } else {
                return apiTokenError(res);
            }
        });

        post("/api/to-odt", (req, res) -> {
            if (checkRequestLiveToken(liveTokens, req)) {
                ObjectMapper mapper = new ObjectMapper();
                OdtExportRequest odtExportRequest = mapper.readValue(req.body(), OdtExportRequest.class);
                //   Files.writeString(Path.of("/home/vivien/Bureau/imports/odt-export-request.xml"), odtExportRequest.xmlString);

                // TODO: use USER NatFilterOptions
                NatFilterOptions nfo = new NatFilterOptions();
                Document stylistDocument = StylistWrapper.buildStylistDocumentFromNatFilterOptions(nfo);
                String chemistryStyleName = StylistWrapper.getChemistryStyleName(stylistDocument);

                byte[] odt = NoirToOpenDocument.toOpenDocument(odtExportRequest.xmlString, chemistryStyleName);
                // Files.write(Path.of("/home/vivien/Bureau/imports/odt-export-request-done.odt"), odt);
                res.type("application/vnd.oasis.opendocument.text");
                return odt;
            } else {
                return apiTokenError(res);
            }
        });
        post("/api/any-to-odt", (req, res) -> {
            if (checkRequestLiveToken(liveTokens, req)) {
                //byte[] odt = UnoConvert.anyToOdt(req.bodyAsBytes());
                byte[] odt = multiUnoConvert.anyToOdt(req.bodyAsBytes());
                return odt;
            } else {
                return apiTokenError(res);
            }
        });
        post("/api/odt-to-noir", (req, res) -> {
            // curl -X POST http://localhost:4567/api/odt-to-noir -H "Content-Type: application/octet-stream" --data-binary @to-lo-test.xml.odt
            if (checkRequestLiveToken(liveTokens, req)) {

                // TODO: use USER NatFilterOptions
                NatFilterOptions nfo = new NatFilterOptions();
                Document stylistDocument = StylistWrapper.buildStylistDocumentFromNatFilterOptions(nfo);
                String chemistryStyleName = StylistWrapper.getChemistryStyleName(stylistDocument);

                Document doc = OpenDocumentToNoir.fromOdt(req.bodyAsBytes(), chemistryStyleName);
                String xmlString = XmlUtils.toString(doc);
                // TODO debug, removeme!
                // Files.writeString(Path.of("/home/vivien/Bureau/imports/import.xml"), xmlString);
                res.type("application/xml");
                return xmlString;
            } else {
                return apiTokenError(res);
            }
        });

        post("/api/starmath-to-mathml", (req, res) -> {
            if (checkRequestLiveToken(liveTokens, req)) {
                String starMathString = req.body();
                //Files.writeString(Path.of("/home/vivien/Bureau/imports/eqn.starmath5.in.txt"), starMathString);
                //byte[] mml = UnoConvert.starmathToMathML(starMathString);
                byte[] mml = multiUnoConvert.starmathToMathML(starMathString);
                //Files.writeString(Path.of("/home/vivien/Bureau/imports/eqn.starmath5.out.mml"), new String(mml));
                return mml;
            } else {
                return apiTokenError(res);
            }
        });
        post("/api/braille-to-html", (req, res) -> {
            if (checkRequestLiveToken(liveTokens, req)) {
                ObjectMapper mapper = new ObjectMapper();
                DetranscriptionRequest detranscriptionRequest = mapper.readValue(req.body(), DetranscriptionRequest.class);
                String htmlString = NatbrailleTan.natbrailleBrailleTextToBlackHtml(detranscriptionRequest.brailleString, new NatFilterOptions(detranscriptionRequest.filterOptions));
                DetranscriptionResult result = new DetranscriptionResult();
                result.htmlString = htmlString;
                return mapper.writeValueAsString(result);
            } else {
                return apiTokenError(res);
            }
        });
        get("/api/nat-filter-option-meta", (req, res) -> {
            // curl localhost:4567/api/nat-filter-option-meta > [...]/natbraille-editor/src/formats/filterOptionsMetadata.json
            FoNatFilterOptionsDetail nfod = new FoNatFilterOptionsDetail();
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(nfod);
        });
//        get("/api/performance", (req, res) ->{
//            res.type("application/json");
//            List<double[]> measurements = executionTimes.getMeasurements();
//            ObjectMapper mapper = new ObjectMapper();
//            return mapper.writeValueAsString(measurements);
//        });
        get("/api/user-performance", (req, res) -> {
            res.type("application/json");
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(perUserExecutionTimes.getMeasurements());
        });

        post("/api/feedback-form", (req, res) -> {
            if (checkRequestLiveToken(liveTokens, req)) {
                new FeedbackDiffusion().sendFeedback(req.body());
                return "noop";
            } else {
                return apiTokenError(res);
            }
        });

        if (!serverConfiguration.staticFilesDirectory.isEmpty()) {
            // serve index.html for any subpage not defined above ("api/")
            // this is needed because the web page routing url subpaths are not #-prefixed
            String indexHtml = Files.readString(Path.of(serverConfiguration.staticFilesDirectory, "index.html"));
            get("/:subpage", (req, res) -> {
                return indexHtml;
            });
        }

        Tokens finalTokens = tokens;
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            LOGGER.info("Server is shutting down...");
            if (serverConfiguration.writeG2CacheAtShutdown) {
                G2.G2Translators.writeAllFileSystemCaches();
            }
            if (serverConfiguration.useDb) {
                if (finalTokens != null) {
                    finalTokens.persistTokens(liveTokens);
                }
            }
            DatabaseManager.getInstance().close();
            stop();
        }));

    }

}