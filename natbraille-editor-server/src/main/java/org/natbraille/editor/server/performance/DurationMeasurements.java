package org.natbraille.editor.server.performance;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DurationMeasurements {
    private static final Logger LOGGER = LoggerFactory.getLogger(DurationMeasurements.class);

    private final List<DurationMeasurement> durationMeasurements = new ArrayList<>();


    public DurationMeasurements() {
    }

    public DurationMeasurements(String measurementName) {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                LOGGER.info("Measure {} mean is {}s", measurementName, getMeanSecond());
                long date = System.currentTimeMillis();
                String filename = String.format("measurements_%s_%d_mean_%fs.csv",measurementName,date,getMeanSecond());
                try {
                    BufferedWriter writer = new BufferedWriter(new FileWriter(filename));
                    for (long l : getDurationsNs()) {
                        writer.write(l + "\n");
                    }
                    writer.close();
                } catch (IOException e) {
                    LOGGER.error("Could not write measurement {} to {}", measurementName, filename);
                }

            }
        });
    }

    public DurationMeasurement startMeasure() {
        DurationMeasurement durationMeasurement = new DurationMeasurement();
        durationMeasurement.start();
        return durationMeasurement;
    }

    public void endMeasure(DurationMeasurement durationMeasurement) {
        durationMeasurement.end();
        durationMeasurements.add(durationMeasurement);
    }

    public List<Long> getDurationsNs() {
        List<Long> durations = new ArrayList<>();
        for (DurationMeasurement m : durationMeasurements) {
            durations.add(m.getDurationNs());
        }
        return durations;
    }
    public List<double[]> getMeasurements(){
        List<double[]> pairs = new ArrayList<>();
        double delta = ((double)System.nanoTime())/1.0e6 - (double)System.currentTimeMillis();
        for (DurationMeasurement m : durationMeasurements) {
            double startMs = ((double)m.startTimeNs)/1.0e6 - delta;
            double durationMs = ((double)m.durationNs)/1.0e6;
            double[] pair = {startMs,durationMs};
            pairs.add(pair);
        }
        return pairs;
    }
    public double getMeanSecond() {
        if (durationMeasurements.isEmpty()){
            return 0;
        }
        double sum = 0;
        for (DurationMeasurement m : durationMeasurements) {
            sum += m.getDurationSecond();
        }
        return sum / durationMeasurements.size();
    }
}
