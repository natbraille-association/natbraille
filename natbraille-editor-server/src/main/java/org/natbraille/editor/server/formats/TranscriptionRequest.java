package org.natbraille.editor.server.formats;

import java.util.Properties;

public class TranscriptionRequest {
    public String xmlString;
    public Properties filterOptions;
}
