package org.natbraille.editor.server.faketranscriptor.g2;

import org.natbraille.brailletable.BrailleTables;
import org.natbraille.brailletable.Converter;
import org.natbraille.brailletable.system.SystemBrailleTables;
import org.natbraille.core.document.NatDocument;
import org.natbraille.core.document.NatDocumentException;
import org.natbraille.core.filter.NatFilterChain;
import org.natbraille.core.filter.NatFilterConfigurator;
import org.natbraille.core.filter.NatFilterException;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.core.filter.transcodeur.TranscodeurNormal;
import org.natbraille.core.gestionnaires.AfficheurAnsiConsole;
import org.natbraille.core.gestionnaires.GestionnaireErreur;
import org.natbraille.core.gestionnaires.LogLevel;

import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.util.Map;



public class TestPonctuation {


    private static record TT(NatFilterConfigurator configurator, NatFilterChain natFilterChain) {
    }

    static TT build() throws NatFilterException {
        GestionnaireErreur ge = new GestionnaireErreur();

        AfficheurAnsiConsole afficheurConsole = new AfficheurAnsiConsole(LogLevel.ULTRA);
        //afficheurConsole.setLogLevel(LogLevel.NONE);
        afficheurConsole.setLogLevel(LogLevel.DEBUG);
        ge.addAfficheur(afficheurConsole);
        NatFilterOptions nfo = new NatFilterOptions();
        nfo.setOption(NatFilterOption.MODE_G2, "true");
        // nfo.setOption(NatFilterOption.XSL_G2_DICT, resourceName);
        //nfo.setOption(NatFilterOption.debug_xsl_processing, "false");
        nfo.setOption(NatFilterOption.debug_xsl_processing, "true");
        NatFilterConfigurator configurator = new NatFilterConfigurator(nfo, ge);//, adhocUserDocumentLoader);
        NatFilterChain trans = new NatFilterChain(configurator);
        trans.addNewFilter(TranscodeurNormal.class);
        return new TT(configurator, trans);
    }

    public static void main(String[] args) throws Exception {

        Converter toTable = new Converter(BrailleTables.forName(SystemBrailleTables.BrailleUTF8), BrailleTables.forName(SystemBrailleTables.DuxTbFr2007));
        
        TT tt = build();
        NatFilterChain natFilterChain = tt.natFilterChain;
        NatFilterConfigurator configurator = tt.configurator;


        String word = "musicien";
        String punctuation = "";
        //String punctuation = ".";
        //String word = "examen";
        //String punctuation = "?";
        //String punctuation = "";
        String id = "666";
        String inDocString = G2AdHocDoc.docDocFromWordAndFollowingPunctuation(word, id, punctuation);
        System.out.println(inDocString);

        NatDocument inDoc = new NatDocument();
        inDoc.setString(inDocString);
        NatDocument outDoc = natFilterChain.run(inDoc);
        System.out.println(outDoc.getString());

        Map<String, String> brailleStrings = G2AdHocDoc.extractBrailleStringsFromOutputDocument(outDoc.getDocument(configurator.getNatDynamicResolver().getXMLReader()));

        System.out.println(brailleStrings);
        String brailleString = brailleStrings.get(id);
        String duxtbfr2007BrailleString = toTable.convert(brailleString);
        System.out.println(brailleString);
        System.out.println(duxtbfr2007BrailleString);

    }
}
