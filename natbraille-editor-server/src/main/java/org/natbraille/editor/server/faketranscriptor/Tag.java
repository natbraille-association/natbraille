package org.natbraille.editor.server.faketranscriptor;

import java.util.Objects;

public enum Tag {

    word,
    number,
    punctuation,
    spacing,
    noir,
    braille,
    uppercase_prefix("uppercase-prefix"),
    letters,
    number_prefix("number-prefix"),
    number_separator("number-separator"),
    digits,
    pre_typeset_math("pre-typeset-math"), // result of natbraille math transcription : see MathTranslator.NatbraillePreTypesetBraille
    math_prefix("math-prefix"),
    span("span"),
    emphasisPrefix("emphasis-prefix");
    public final String tagName;

    Tag() {
        this.tagName = this.name();
    }

    Tag(String name) {
        this.tagName = name;
    }

    public String toString() {
        return (this.tagName != null) ? (this.tagName) : (this.name());
    }

    public static Tag byTagName(String tagName) {
        for (Tag tag : Tag.values()) if (Objects.equals(tag.tagName, tagName)) return tag;
        return null;
    }
}
