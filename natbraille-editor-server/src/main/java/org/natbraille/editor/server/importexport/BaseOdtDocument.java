package org.natbraille.editor.server.importexport;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.odftoolkit.odfdom.doc.OdfTextDocument;
import org.odftoolkit.odfdom.dom.element.OdfStylePropertiesBase;
import org.odftoolkit.odfdom.dom.element.office.OfficeTextElement;
import org.odftoolkit.odfdom.dom.style.OdfStyleFamily;
import org.odftoolkit.odfdom.dom.style.props.OdfStylePropertiesSet;
import org.odftoolkit.odfdom.incubator.doc.office.OdfOfficeStyles;
import org.odftoolkit.odfdom.incubator.doc.style.OdfStyle;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class BaseOdtDocument {
    public static class CustomStyles {
        public static class CustomStyle {
            public static class StyleAttribute {
                public String namespaceURI;
                public String qualifiedName;
                public String value;
            }

            public String name;
            public String type;
            public List<StyleAttribute> attributes;
        }

        public CustomStyle chemistry;

    }

    static OdfTextDocument newTextDocument() throws Exception {

        OdfTextDocument odt = OdfTextDocument.newTextDocument();
        OfficeTextElement contentRoot = odt.getContentRoot();

        // remove contentRoot childs (i.e. a rogue paragraph)
        while (contentRoot.hasChildNodes()) {
            contentRoot.removeChild(contentRoot.getFirstChild());
        }

        OdfOfficeStyles stylesDocument = odt.getDocumentStyles();
        String xmlns_loext = "urn:org:documentfoundation:names:experimental:office:xmlns:loext:1.0";

        // add character styles
        OdfStyle chimix = stylesDocument.newStyle("chimix", OdfStyleFamily.Text);
        OdfStylePropertiesBase chimix_textProperties = chimix.getOrCreatePropertiesElement(OdfStylePropertiesSet.TextProperties);
        chimix_textProperties.setAttributeNS("fo", "fo:background-color", "#e8f2a1");
        chimix_textProperties.setAttributeNS(xmlns_loext, "loext:padding", "0.049cm");
        chimix_textProperties.setAttributeNS(xmlns_loext, "loext:border", "0.06pt solid #bf0041");

        // add paragraph styles
        return odt;
    }

    public static void main(String[] args) throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        InputStream s = BaseOdtDocument.class.getResourceAsStream("default_custom_styles.json");
        CustomStyles customStyles = mapper.readValue(s, CustomStyles.class);
        System.out.println(customStyles);

        String json = mapper.writeValueAsString(customStyles);
        System.out.println(json);

/*
        CustomStyles.CustomStyle cs = new CustomStyles.CustomStyle();
        cs.name = "_name_";
        cs.type = "_type_";


        ObjectMapper mapper2 = new ObjectMapper();
*/
    }
}
