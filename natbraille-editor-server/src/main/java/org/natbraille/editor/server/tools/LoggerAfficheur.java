package org.natbraille.editor.server.tools;

import org.natbraille.core.gestionnaires.Afficheur;
import org.natbraille.core.gestionnaires.LogLevel;
import org.natbraille.core.gestionnaires.LogMessage;
import org.natbraille.core.gestionnaires.MessageContents;
import org.slf4j.Logger;

public class LoggerAfficheur extends Afficheur {
    @Override
    public void afficheMessage(LogMessage logMessage) {
        MessageContents contents = logMessage.getContents();
        if (logMessage.getLevel() >= LogLevel.DEBUG) {
            LOGGER.debug(contents.getRawText());
        } else if (logMessage.getLevel() >= LogLevel.NORMAL) {
            LOGGER.info(contents.getRawText());
        } else {
            LOGGER.trace(contents.getRawText());
        }
    }

    public LoggerAfficheur(Logger LOGGER) {
        this.LOGGER = LOGGER;
    }

    private final Logger LOGGER;

}
