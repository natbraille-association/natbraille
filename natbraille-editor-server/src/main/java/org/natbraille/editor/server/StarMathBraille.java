 /*
  * Natbraille
  * Copyright (C) 2008 Bruno Mascret
  * Copyright (C) 2024 Vivien Guillet
  * Contact: vgu@gmx.fr
  * http://natbraille.free.fr
  *
  * This program is free software; you can redistribute it and/or
  * modify it under the terms of the GNU General Public License
  * as published by the Free Software Foundation; either version 2
  * of the License.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
  */
 package org.natbraille.editor.server;

 import org.natbraille.core.document.NatDocumentException;
 import org.natbraille.core.filter.NatFilterException;
 import org.natbraille.core.filter.options.NatFilterOption;
 import org.natbraille.core.filter.options.NatFilterOptions;
 import org.natbraille.editor.server.faketranscriptor.math.MathTranslator;
 import org.natbraille.editor.server.faketranscriptor.tan.NatbrailleTan;
 import org.natbraille.editor.server.unoconvertwrapper.UnoConvert;
 import spark.Spark;

 import javax.xml.transform.TransformerException;
 import java.io.IOException;
 import java.util.HashMap;
 import java.util.Map;

 /**
  * This class implements a specialized HTTP server that perform transcription from StarMath
  * to Braille and from Braille to StarMath
  */
 class StarMathBraille {

     private final MathTranslator.NatbrailleMathTranslators natbrailleMathTranslators;

     public StarMathBraille() {
         natbrailleMathTranslators = new MathTranslator.NatbrailleMathTranslators();
     }

     private String starMathToBraille(String starMathSource) throws NatFilterException, IOException, TransformerException, NatDocumentException {

         //
         // 1. starmath -> mathml
         //
         byte[] xmlBytes = UnoConvert.starmathToMathML(starMathSource);
         String mathmlXml = new String(xmlBytes);

         //
         // 2. mathml -> braille
         //

         // quirky remove <?xml version="1.0" encoding="UTF-8"?> mathml header
         String mathml = mathmlXml.replaceAll(".*\\s+?\n", "");

         // for a given math transcription, retrieve appropriate natbrailleMathTranslators
         NatFilterOptions nfo = new NatFilterOptions();
         nfo.setOption(NatFilterOption.MODE_MATH_SPECIFIC_NOTATION, "true");
         nfo.setOption(NatFilterOption.MODE_MATH_ALWAYS_PREFIX, "true");
         MathTranslator.NatbrailleMathTranslator natbrailleMathTranslator = natbrailleMathTranslators.getNatbrailleMathTranslator(nfo);

         // translate to braille
         Map<String, String> equations = new HashMap<>();
         String key = "eq1";
         equations.put(key, mathml);
         final Map<String, String> brailleMap = natbrailleMathTranslator.translate(equations);
         String brailleMathWithSpecialChars = brailleMap.get(key);

         // remove special chars (hyphenations marks, prefix etc.)
         String brailleMath = MathTranslator.NatbraillePreTypesetBraille.getCleanString(brailleMathWithSpecialChars);

         return brailleMath;
     }

     private String brailleToStarMath(String brailleMathSource) throws Exception {

         //
         // 1. braille -> mathml
         //

         // prepend with braille math prefix
         String mathPrefix = "⠠⠄"; // pt6 pt3
         String prefixedMath = mathPrefix + brailleMathSource;

         // setup options
         NatFilterOptions nfo = new NatFilterOptions();
         nfo.setOption(NatFilterOption.MODE_MATH_SPECIFIC_NOTATION, "true");
         nfo.setOption(NatFilterOption.MODE_MATH_ALWAYS_PREFIX, "true");
         nfo.setOption(NatFilterOption.MODE_DETRANS_MATH_USE_BRAILLE_SEMANTICS_ANNOTATION, "false");

         // detrans to html + mathml
         String htmlPlusMathMLString = NatbrailleTan.natbrailleBrailleTextToBlackHtml(prefixedMath, nfo);

         //
         // 2. mathml -> starmath
         //

         // extract mathml from html + mathml
         int startIndex = htmlPlusMathMLString.indexOf("<math");
         String endLookupString = "</math>";
         int endIndex = htmlPlusMathMLString.indexOf(endLookupString) + endLookupString.length();
         String mathMLString = htmlPlusMathMLString.substring(startIndex, endIndex);

         // convert mathml -> mathml + starmath annotation
         byte[] mathmlPlusStarMath = UnoConvert.mathMLToMathML(mathMLString);
         String mathmlPlusStarMathString = new String(mathmlPlusStarMath);

         // quirky extract starmath annotation
         String starMathString = mathmlPlusStarMathString
                 .replaceAll("(?s).*<annotation encoding=\"StarMath.*?>", "")
                 .replaceAll("(?s)</annotation>.*", "");

         return starMathString;
     }

     public static void startServer(String ipAddress, int port) {
         Spark.ipAddress(ipAddress);
         Spark.port(port);

         System.setProperty("javax.xml.parsers.DocumentBuilderFactory",
                 "org.apache.xerces.jaxp.DocumentBuilderFactoryImpl");
         System.setProperty("javax.xml.xpath.XPathFactory:http://java.sun.com/jaxp/xpath/dom",
                 "com.sun.org.apache.xpath.internal.jaxp.XPathFactoryImpl");

         StarMathBraille starMathBraille = new StarMathBraille();

         // curl -X POST localhost:4567/starmath-to-braille -H "Content-Type: text/plain" -d 'sqrt (a+b)'
         Spark.post("starmath-to-braille", (req, res) -> starMathBraille.starMathToBraille(req.body()));

         // curl -X POST localhost:4567/braille-to-starmath -H "Content-Type: text/plain" -d '⠜⠰⠦⠁⠖⠃⠴⠆'
         Spark.post("braille-to-starmath", (req, res) -> starMathBraille.brailleToStarMath((req.body())));

     }

     public static void main(String[] args) throws NatFilterException, IOException, TransformerException, NatDocumentException {
         startServer("localhost", 4567);
     }

/*
     public static void usage() throws NatFilterException, IOException, TransformerException, NatDocumentException {

         StarMathBraille starMathBraille = new StarMathBraille();

         // starmath -> braille
         String starMathSource = "%alpha";
         String brailleMath = starMathBraille.starMathToBraille(starMathSource);
         System.out.println(brailleMath);

         // braille -> starmath
         String starMathOutput = starMathBraille.brailleToStarMath(brailleMath);
         System.out.println(starMathOutput);


     }

 */
 }