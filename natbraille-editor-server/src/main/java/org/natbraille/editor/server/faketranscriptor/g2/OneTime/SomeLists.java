package org.natbraille.editor.server.faketranscriptor.g2.OneTime;

import com.opencsv.exceptions.CsvValidationException;
import org.natbraille.editor.server.faketranscriptor.g2.Morphalou;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * One-time creation of triplet lists from pre-generated transcription files for nat/liblouis comparison.
 */
public class SomeLists {
    static String liblouisToUnicode(String ll) {
        // https://raw.githubusercontent.com/liblouis/liblouis/master/tables/fr-bfu-comp6.utb
        StringBuilder ub = new StringBuilder();
        for (int i = 0; i < ll.length(); i++) {
            String substring = ll.substring(i, i + 1);
            String ubc = switch (substring) {
                case "a" -> "⠁";
                case "â" -> "⠡";
                case "à" -> "⠷";
                case "b" -> "⠃";
                case "c" -> "⠉";
                case "ç" -> "⠯";
                case "d" -> "⠙";
                case "e" -> "⠑";
                case "ê" -> "⠣";
                case "é" -> "⠿";
                case "è" -> "⠮";
                case "ë" -> "⠫";
                case "f" -> "⠋";
                case "g" -> "⠛";
                case "h" -> "⠓";
                case "i" -> "⠊";
                case "î" -> "⠩";
                case "j" -> "⠚";
                case "k" -> "⠅";
                case "l" -> "⠇";
                case "m" -> "⠍";
                case "n" -> "⠝";
                case "ñ" -> "⠻";
                case "o" -> "⠕";
                case "ô" -> "⠹";
                case "ó" -> "⠬";
                case "p" -> "⠏";
                case "q" -> "⠟";
                case "r" -> "⠗";
                case "s" -> "⠎";
                case "t" -> "⠞";
                case "u" -> "⠥";
                case "û" -> "⠱";
                case "ü" -> "⠳";
                case "ù" -> "⠾";
                case "v" -> "⠧";
                case "w" -> "⠺";
                case "x" -> "⠭";
                case "y" -> "⠽";
                case "z" -> "⠵";
                case "œ" -> "⠪";
                case "@" -> "⠜";
                case "." -> "⠲";
                case "," -> "⠂";
                case ";" -> "⠆";
                case ":" -> "⠒";
                case "?" -> "⠢";
                case "!" -> "⠖";
                case "'" -> "⠄";
                case "´" -> "⠐";
                case "„" -> "⠰";
                case "`" -> "⠠";
                case "^" -> "⠈";
                case "¨" -> "⠨";
                case "\"" -> "⠶";
                case "(" -> "⠦";
                case ")" -> "⠴";
                case "/" -> "⠌";
                case "0" -> "⠼";
                case "¤" -> "⠘";
                case "*" -> "⠔";
                case "-" -> "⠤";
                case "|" -> "⠸";
                case " " -> "⠀"; // braille blank
                default -> "∉";
                //default -> substring;
            };
            if (ubc.equals("∉")) {
                throw new Error("unknown liblouis char [" + substring + "]");
            }
            ub.append(ubc);
        }
        return ub.toString();
    }
    static String debraille(String ll) {
        StringBuilder ub = new StringBuilder();
        for (int i = 0; i < ll.length(); i++) {
            String substring = ll.substring(i, i + 1);
            String ubc = switch (substring) {
                case "⠁" -> "a";
                case "⠡" -> "â";
                case "⠷" -> "à";
                case "⠃" -> "b";
                case "⠉" -> "c";
                case "⠯" -> "ç";
                case "⠙" -> "d";
                case "⠑" -> "e";
                case "⠣" -> "ê";
                case "⠿" -> "é";
                case "⠮" -> "è";
                case "⠫" -> "ë";
                case "⠋" -> "f";
                case "⠛" -> "g";
                case "⠓" -> "h";
                case "⠊" -> "i";
                case "⠩" -> "î";
                case "⠚" -> "j";
                case "⠅" -> "k";
                case "⠇" -> "l";
                case "⠍" -> "m";
                case "⠝" -> "n";
                case "⠻" -> "ñ";
                case "⠕" -> "o";
                case "⠹" -> "ô";
                // case "ó" -> "⠬"-> ;
                case "⠏" -> "p";
                case "⠟" -> "q";
                case "⠗" -> "r";
                case "⠎" -> "s";
                case "⠞" -> "t";
                case "⠥" -> "u";
                case "⠱" -> "û";
                case "⠳" -> "ü";
                case "⠾" -> "ù";
                case "⠧" -> "v";
                case "⠺" -> "w";
                case "⠭" -> "x";
                case "⠽" -> "y";
                case "⠵" -> "z";
                case "⠪" -> "œ";
                case "⠜" -> "@";
                case "⠲" -> ".";
                case "⠂" -> ",";
                case "⠆" -> ";";
                case "⠒" -> ":";
                case "⠢" -> "?";
                case "⠖" -> "!";
                case "⠄" -> "'";
                //case "´" -> "⠐" -> "´";
                //case "„" -> "⠰"-> "„";
                //case "`" -> "⠠" -> "`";
                //case "^" -> "⠈" -> "^";
                //case "¨" -> "⠨" -> "¨";
                //case "⠶" -> "\"";
                case "⠦" -> "(";
                case "⠴" -> ")";
                case "⠌" -> "/";
                case "⠼" -> "0";
                //case "¤" -> "⠘" -> "¤";
                case "⠔" -> "*";
                case "⠤" -> "-";
                case "⠸" -> "|";
                case "⠀" -> " "; // braille blank -> standard blank
                default -> substring;
            };
            ub.append(ubc);
        }
        return ub.toString();
    }
    static List<String> getMorphalouGraphies() throws IOException, CsvValidationException {
        final String path = "/home/vivien/pr-src/doc-g2/morphalou/morphalou/5/Morphalou3.1_formatCSV_toutEnUn/Morphalou3.1_CSV.csv";
        List<Morphalou.Word> words = Morphalou.parseCsv(path);
        List<String> graphies = new ArrayList<>();
        for (Morphalou.Word word : words) {
            graphies.add(word.getGraphie());
        }
        return graphies;
    }
    static String natbrailleToUnicode(String ll) {
        return ll.replaceAll("ⴹ", "");

    }
    static List<String> getLiblouisG2() throws IOException {
        final String path = "/home/vivien/pr-src/doc-g2/liblouis/liblouis-g2-morphalou.txt";
        List<String> g2 = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            Morphalou.Lemme lemme = null;
            String line;
            while ((line = br.readLine()) != null) {
                g2.add(line);
            }
        }
        return g2;
    }


    static List<String> getNatV2G2() throws IOException {
        final String path = "/home/vivien/pr-src/doc-g2/nat-braille temp file for all odt/tmp_mep.xml";

        List<String> g2 = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            Morphalou.Lemme lemme = null;
            String line;
            while ((line = br.readLine()) != null) {
                String startOfLine = "   <phrase modeBraille=\"3-1\">";
                int indexStart = line.indexOf(startOfLine);
                int indexEnd = line.indexOf("</phrase>");
                if ((indexStart != -1) && (indexEnd != -1)) {
                    String brailleString = line.substring(startOfLine.length(), indexEnd);
                    // System.out.println(line + " " + indexStart + " " + indexEnd+" "+brailleString);
                    g2.add(brailleString);
                } else {
                    // System.out.println(line + " " + indexStart + " " + indexEnd);
                }
            }
        }
        return g2;
    }



    //en plus d'  	|	  ⠢ ⠫ ⠙⠄  	|	  ⠢⠀⠫⠀⠙⠄  	|	  notok




    static void writeTipletFile(List<String> morphalouGraphies, List<String> libLouisG2, List<String> natV2G2) throws IOException {
        FileOutputStream fos = new FileOutputStream("/home/vivien/pr-src/doc-g2/compare/triplets.tsv");
        if ((morphalouGraphies.size() != libLouisG2.size()) || (morphalouGraphies.size() != natV2G2.size())) {
            throw new Error("sizes do not match");
        }
        for (int i = 0; i < morphalouGraphies.size(); i++) {

            String morphalou = morphalouGraphies.get(i);
            String liblouis = liblouisToUnicode(libLouisG2.get(i));
            String natbrailleOriginal = natV2G2.get(i);
            String natbraille = natbrailleToUnicode(natbrailleOriginal);
            boolean match = liblouis.equals(natbraille);

            // String sep = "  \t|\t  ";
            String sep = "\t";
            //if (!match) {
            fos.write((match ? "==" : "!=").getBytes(StandardCharsets.UTF_8));
            fos.write(sep.getBytes(StandardCharsets.UTF_8));

            fos.write(morphalou.getBytes(StandardCharsets.UTF_8));
            fos.write(sep.getBytes(StandardCharsets.UTF_8));

            fos.write(liblouis.getBytes(StandardCharsets.UTF_8));
            fos.write(sep.getBytes(StandardCharsets.UTF_8));

            fos.write(debraille(liblouis).getBytes(StandardCharsets.UTF_8));
            fos.write(sep.getBytes(StandardCharsets.UTF_8));

            fos.write(natbrailleOriginal.getBytes(StandardCharsets.UTF_8));
            fos.write(sep.getBytes(StandardCharsets.UTF_8));

            fos.write(natbraille.getBytes(StandardCharsets.UTF_8));
            fos.write(sep.getBytes(StandardCharsets.UTF_8));

            fos.write(debraille(natbraille).getBytes(StandardCharsets.UTF_8));
            fos.write(sep.getBytes(StandardCharsets.UTF_8));

            fos.write("\n".getBytes(StandardCharsets.UTF_8));
            //}
        }
        fos.close();
    }

    public static void main(String[] args) throws IOException, CsvValidationException {
        List<String> morphalouGraphies = getMorphalouGraphies();
        List<String> libLouisG2 = getLiblouisG2();
        List<String> natV2G2 = getNatV2G2();

        //System.out.println(morphalouGraphies);
        //System.out.println(libLouisG2);
        //System.out.println(natV2G2);
        writeTipletFile(morphalouGraphies, libLouisG2, natV2G2);
    }
}
