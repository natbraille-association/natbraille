package org.natbraille.editor.server.faketranscriptor;

import net.sf.saxon.s9api.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;

public class XmlUtils {

    public static void main(String[] args) throws ParserConfigurationException, UnsupportedEncodingException, SaxonApiException {
        System.setProperty("javax.xml.parsers.DocumentBuilderFactory", "org.apache.xerces.jaxp.DocumentBuilderFactoryImpl");
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

        // https://cheatsheetseries.owasp.org/cheatsheets/XML_External_Entity_Prevention_Cheat_Sheet.html
        dbf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
        dbf.setXIncludeAware(false);

        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.newDocument();
        Element element = doc.createElement("html");
        doc.appendChild(element);
        System.out.println(doc.getDoctype());
/*

        {
            Element el1 = doc.createElement("el1");
            el1.setTextContent("content of el1 /1");
            element.appendChild(el1);
        }
        {
            Element el1 = doc.createElement("el1");
            el1.setTextContent("content of el1 /2");
            element.appendChild(el1);
        }*/
        System.out.println("--");
        System.out.println(doc.getDocumentElement().getTagName());
        System.out.println("--");
        System.out.println(toString(doc));

    }

    public static String toString(Document doc) throws SaxonApiException, UnsupportedEncodingException {
        Processor processor = new Processor(false); // False = does not required a feature from a licensed version of Saxon.
        Serializer serializer = processor.newSerializer();
//        serializer.setOutputProperty(Serializer.Property.OMIT_XML_DECLARATION, "no");
        serializer.setOutputProperty(Serializer.Property.METHOD,"xml");
        Source domSource = new DOMSource(doc);
        return serializer.serializeToString(domSource);
    }
    public static String toString(Element element, boolean omitXmlDeclaration) throws SaxonApiException, UnsupportedEncodingException {
        Processor processor = new Processor(false); // False = does not required a feature from a licensed version of Saxon.
        Serializer serializer = processor.newSerializer();
        serializer.setOutputProperty(Serializer.Property.OMIT_XML_DECLARATION, omitXmlDeclaration?"yes":"no");
        serializer.setOutputProperty(Serializer.Property.METHOD,"xml");
        Source domSource = new DOMSource(element);
        return serializer.serializeToString(domSource);
    }
}
