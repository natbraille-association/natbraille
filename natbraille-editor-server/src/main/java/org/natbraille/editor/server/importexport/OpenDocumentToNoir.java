package org.natbraille.editor.server.importexport;

import org.natbraille.editor.server.importexport.DomSax.DomSaxHandler;
import org.odftoolkit.odfdom.doc.OdfDocument;
import org.odftoolkit.odfdom.doc.OdfTextDocument;
import org.odftoolkit.odfdom.dom.element.draw.DrawObjectElement;
import org.odftoolkit.odfdom.dom.element.svg.SvgDescElement;
import org.odftoolkit.odfdom.dom.element.text.*;
import org.odftoolkit.odfdom.dom.style.OdfStyleFamily;
import org.odftoolkit.odfdom.incubator.doc.style.OdfStyle;
import org.odftoolkit.odfdom.incubator.doc.text.OdfTextListStyle;
import org.odftoolkit.odfdom.pkg.OdfPackage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Stack;


public class OpenDocumentToNoir {
    private static final Logger LOGGER = LoggerFactory.getLogger(OpenDocumentToNoir.class);

    /**
     * get the level of the textListElement in the document (1 based).
     *
     * @param textListElement the text:list element
     * @return the level of the textListElement
     */
    private static int getTextListElementLevel(TextListElement textListElement) {
        Node node = textListElement;
        int level = 0;
        while (node != null) {
            if (node.getNodeName().equals("text:list")) {
                level++;
            }
            node = node.getParentNode();
        }
        return level;
    }

    /**
     * return the style name of text:list defined as nearest non-empty text:style-name attribute of
     * parent-or-self text:list
     *
     * @param textListElement the text:list element
     * @return the style name of the textListElement
     */
    private static String getTextListElementStyleName(TextListElement textListElement) {
        Node node = textListElement;
        while (node != null) {
            if (node.getNodeName().equals("text:list")) {
                String styleName = ((TextListElement) node).getTextStyleNameAttribute();
                if (styleName != null) {
                    return styleName;
                }
            }
            node = node.getParentNode();
        }
        // defined by spec to be a bullet list
        return null;
    }

    public static class OpenDocumentToNoirStyle {
        public enum ListKind {ol, ul}

        final OdfTextDocument odt;

        OdfTextListStyle getTextStyleListByName(String name) throws IOException, SAXException {
            OdfTextListStyle style = null;
            style = odt.getContentDom().getAutomaticStyles().getListStyle(name);
            if (style == null) {
                style = odt.getStylesDom().getAutomaticStyles().getListStyle(name);
                if (style == null) {
                    style = odt.getStylesDom().getOfficeStyles().getListStyle(name);
                    if (style == null) {
                        style = odt.getStylesDom().getMasterStyles().getListStyle(name);
                    }
                }
            }
            return style;
        }

        OdfStyle getParagraphStyleByName(String styleName, OdfStyleFamily odfStyleFamily) throws IOException, SAXException {
            OdfStyle az;
            az = odt.getContentDom().getAutomaticStyles().getStyle(styleName, odfStyleFamily);
            if (az != null) return az;
            az = odt.getStylesDom().getAutomaticStyles().getStyle(styleName, odfStyleFamily);
            if (az != null) return az;
            az = odt.getStylesDom().getOfficeStyles().getStyle(styleName, odfStyleFamily);
            if (az != null) return az;
            az = odt.getStylesDom().getMasterStyles().getStyle(styleName, odfStyleFamily);
            return az;
        }

        boolean styleHasNameOrHasParentHavingName(String highStyleName, String styleNameToMatch, OdfStyleFamily odfStyleFamily) throws IOException, SAXException {
            OdfStyle style = getParagraphStyleByName(highStyleName, odfStyleFamily);
            while (style != null) {
                if (styleNameToMatch.equals(style.getStyleNameAttribute())) {
                    return true;
                }
                String parentStyleName = style.getStyleParentStyleNameAttribute();
                if (parentStyleName == null) {
                    return false;
                }
                style = getParagraphStyleByName(parentStyleName, odfStyleFamily);
            }
            return false;
        }

        ListKind getListKind(TextListElement textListElement) {
            final int level = getTextListElementLevel(textListElement);
            final String styleName = getTextListElementStyleName(textListElement);
            // System.out.println("level" + level + " " + styleName + " " + styleName);
            try {
                final OdfTextListStyle odfTextListStyle = getTextStyleListByName(styleName);
                final TextListLevelStyleElementBase textListLevelStyleElementBase = odfTextListStyle.getLevel(level);
                // System.out.println("level" + level + " " + styleName + " " + styleName + " " + textListLevelStyleElementBase.getTagName());
                return switch (textListLevelStyleElementBase.getTagName()) {
                    case "text:list-level-style-number" -> ListKind.ol;
                    case "text:list-level-style-bullet" -> ListKind.ul;
                    default -> ListKind.ul;
                };
            } catch (IOException | SAXException | NullPointerException e) {
                LOGGER.debug("cannot find style for TextListElement with style name {} at level {}", styleName, level);
            }
            // defaults to ul
            return ListKind.ul;
        }

        public OpenDocumentToNoirStyle(OdfTextDocument odt) {
            this.odt = odt;
        }
    }


    public static class OdtHandler implements DomSaxHandler {

        public boolean isInsideAChemistryContainer(Element e) {
            Node n = e;
            while (n != null) {
                try {
                    if (n instanceof TextSpanElement source) {
                        if (style.styleHasNameOrHasParentHavingName(source.getStyleName(), chemistryStyleName, OdfStyleFamily.Text)) {
                            return true;
                        }
                    } else if (n instanceof TextParagraphElementBase source) {
                        if (style.styleHasNameOrHasParentHavingName(source.getStyleName(), chemistryStyleName, OdfStyleFamily.Paragraph)) {
                            return true;
                        }
                    }
                } catch (IOException | SAXException ex) {
                    throw new RuntimeException(ex);
                }
                n = n.getParentNode();
            }
            return false;
        }

        public void setMathChemistryAttribute(Element e) {
            e.setAttribute("chemistry", "true");
        }

        @Override
        public void startElement(Element e) {
            // dbgDepth++;
            // System.out.println("- ".repeat(dbgDepth) + "startElement " + e.getTagName());
            if (e instanceof TextSpanElement) {
                // noop atm
                // TextSpanElement source = (TextSpanElement) e;
            } else if (e instanceof TextPElement) {
                TextPElement source = (TextPElement) e;
                Element p;
                if ("pre-typeset-braille".equals(source.getStyleName())){
                    p = doc.createElement("pre");
                } else {
                    p = doc.createElement("p");
                }
                stack.peek().appendChild(p);
                stack.push(p);
            } else if (e instanceof TextHElement) {
                TextHElement source = (TextHElement) e;
                Integer outlineLevel = source.getTextOutlineLevelAttribute();
                Element h = doc.createElement("h" + String.valueOf(outlineLevel));
                stack.peek().appendChild(h);
                stack.push(h);
            } else if (e instanceof TextListElement) {
                // it seems that a <text:list> having only a <text:list-item> is a special case
                // not easily representable in noir/html
                TextListElement source = (TextListElement) e;
                String tagName = switch (style.getListKind(source)) {
                    case ol -> "ol";
                    case ul -> "ul";
                };
                Element ulOrOl = doc.createElement(tagName);
                stack.peek().appendChild(ulOrOl);
                stack.push(ulOrOl);
            } else if (e instanceof TextListItemElement) {
                TextListItemElement source = (TextListItemElement) e;
                Element li = doc.createElement("li");
                stack.peek().appendChild(li);
                stack.push(li);
            } else if (e instanceof SvgDescElement) {
                stack.push(e);
            } else if (e instanceof DrawObjectElement) {
                DrawObjectElement source = (DrawObjectElement) e;
                NodeList embededmath = e.getElementsByTagName("math:math");
                if (embededmath.getLength() != 0) {
                    Element mathML = (Element) embededmath.item(0);
                    Node mathmlClone = doc.adoptNode(mathML);
                    if (isInsideAChemistryContainer(source)) {
                        setMathChemistryAttribute((Element) mathmlClone);
                    }
                    stack.peek().appendChild(mathmlClone);
                    // stack.push((Element)mathmlClone);
                } else {
                    String contentHRef = source.getXlinkHrefAttribute() + "/content.xml";
                    try {
                        Document maybeMathMLDom = odfPackage.getDom(contentHRef);
                        Node maybeMathMLElement = maybeMathMLDom.getFirstChild();
                        if ((maybeMathMLElement != null) && (maybeMathMLElement.getNodeName().equals("math"))) {
                            Element mathML = (Element) maybeMathMLElement;
                            Node mathmlClone = doc.adoptNode(mathML);
                            if (isInsideAChemistryContainer(source)) {
                                setMathChemistryAttribute((Element) mathmlClone);
                            }
                            stack.peek().appendChild(mathmlClone);
                            //stack.push(mathmlClone);
                        }
                    } catch (SAXException ex) {
                        throw new RuntimeException(ex);
                    } catch (ParserConfigurationException ex) {
                        throw new RuntimeException(ex);
                    } catch (TransformerException ex) {
                        throw new RuntimeException(ex);
                    } catch (IOException ex) {
                        throw new RuntimeException(ex);
                    }
                }
                //   stack.push(e);
            }
        }

        @Override
        public void endElement(Element e) {
            // System.out.println("- ".repeat(dbgDepth) + "endElement " + e.getTagName());
            // dbgDepth--;
            // (e instanceof TextSpanElement)
            if ((e instanceof TextPElement)
                    || (e instanceof TextHElement)
                    || (e instanceof TextListElement)
                    || (e instanceof TextListItemElement)
                    || (e instanceof SvgDescElement)) {
                stack.pop();
            }
        }

        @Override
        public void characters(String s) {
            Element currentStackTop = stack.peek();
            if (currentStackTop instanceof SvgDescElement) {
                // noop
            } else {
                currentStackTop.appendChild(doc.createTextNode(s));
            }
        }

        int dbgDepth = 0;
        private final Stack<Element> stack = new Stack<>();
        public final Document doc;
        public final OpenDocumentToNoirStyle style;
        public final OdfPackage odfPackage;
        //
        public final String chemistryStyleName;// = "chimie";

        public OdtHandler(OpenDocumentToNoirStyle style, OdfPackage odfPackage, String chemistryStyleName) throws ParserConfigurationException {

            this.style = style;
            this.odfPackage = odfPackage;
            this.chemistryStyleName = chemistryStyleName;
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = dbf.newDocumentBuilder();
            this.doc = builder.newDocument();

            Element html = doc.createElement("html");
            this.doc.appendChild(html);
            Element body = doc.createElement("body");
            html.appendChild(body);

            stack.push(body);
        }
    }

    public static Document fromOdt(byte[] bytes, String chemistryStyleName) throws Exception {

        // parse bytes to OdfTextDocument
        OdfTextDocument odt = (OdfTextDocument) OdfDocument.loadDocument(new ByteArrayInputStream(bytes));

        // get style (for ul/ol list)
        OpenDocumentToNoirStyle style = new OpenDocumentToNoirStyle(odt);

        // convert to Noir
        OdtHandler odtHandler = new OdtHandler(style, odt.getPackage(), chemistryStyleName);
        DomSax.parse(odt.getContentRoot(), odtHandler);

        return odtHandler.doc;
    }
}
