package org.natbraille.editor.server.faketranscriptor.layout;

import org.natbraille.editor.server.faketranscriptor.FakeTranscription;
import org.natbraille.editor.server.faketranscriptor.Tag;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.xpath.XPathExpressionException;
import java.util.ArrayList;
import java.util.List;

public class BlockNavigator {
    public Element block;
    //    private NodeList nodeList;
    private List<Node> nodeList;

    public static class Position {
        final Node node;
        final int nodeIndex;
        public final int innerOffset;
        public final int innerLength;
        public final int globalOffset;
        public final String character;
        public final Node typeNode;
        public final Tag type;

        public Position(Node node, int nodeIndex, int innerOffset, int innerLength, int globalOffset, String character, Node typeNode, Tag type) {
            this.node = node;
            this.nodeIndex = nodeIndex;
            this.innerOffset = innerOffset;
            this.globalOffset = globalOffset;
            this.innerLength = innerLength;
            this.character = character;
            this.typeNode = typeNode;
            this.type = type;
        }
    }

    public final List<Position> elements = new ArrayList<>();

    public BlockNavigator(Element block, FakeTranscription ft) throws XPathExpressionException {
        this.block = block;
        // ft.xpathNodes 16.631ms

        //nodeList = ft.xpathNodes(block, "descendant::braille/descendant-or-self::text()");
        List<Node> nodeList = new ArrayList<>();

        Node blockChild = block.getFirstChild();
        while (blockChild != null) {
            Element blockChildElement = (Element) blockChild;
            Element braille = (Element) blockChildElement.getElementsByTagName("braille").item(0);
            if (braille == null) {
                // this should not happen
                throw new Error("block child element " + blockChild.getNodeName() + " has no braille child");
            }
            Node brailleChild = braille.getFirstChild();
            while (brailleChild != null) {
                if (brailleChild.getNodeType() == Node.TEXT_NODE) {
                    // spacing/braille has a text node as child
                    nodeList.add(brailleChild);
                } else {
                    // word/braille/letter has text node as child
                    Node brailleGrandChild = brailleChild.getFirstChild();
                    while (brailleGrandChild != null) {
                        if (brailleGrandChild.getNodeType() == Node.TEXT_NODE) {
                            nodeList.add(brailleGrandChild);
                        }
                        brailleGrandChild = brailleGrandChild.getNextSibling();
                    }
                }
                //if (brailleChild.getTextContent().length()<1) {
                //    System.out.println("@" + braille.getAttribute("fragmentId") + " " + braille.getNodeName() + "->" + brailleChild.getNodeName() + ":'" + brailleChild.getTextContent()+"':");
                //}
                brailleChild = brailleChild.getNextSibling();
            }
//            nodeList.add()

            blockChild = blockChild.getNextSibling();
        }

        int gOffset = 0;
        //   for (int nodeIndex = 0; nodeIndex < nodeList.getLength(); nodeIndex++) {
        //   Node node = nodeList.item(nodeIndex);
        for (int nodeIndex = 0; nodeIndex < nodeList.size(); nodeIndex++) {
            Node node = nodeList.get(nodeIndex);

            String nodeText = node.getTextContent();
            for (int innerOffset = 0; innerOffset < nodeText.length(); innerOffset++) {
                String character = nodeText.substring(innerOffset, innerOffset + 1);
                Node pn = node.getParentNode();
                Node ppn = pn.getParentNode();
                Node typeNode = pn.getNodeName().equals(Tag.braille.tagName) ? ppn : pn;
                // System.out.println("->"+typeNode);
                Tag type = Tag.byTagName(typeNode.getNodeName());
                elements.add(new Position(node, nodeIndex, innerOffset, nodeText.length(), gOffset + innerOffset, character, typeNode, type));
            }
            gOffset += nodeText.length();
        }
    }

}