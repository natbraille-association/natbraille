package org.natbraille.editor.server;

/*
 * Options of the natbraille-editor server
 */

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ServerConfiguration {
    /**
     * version of the server configuration (for stored configurations)
     */
    public int version = 1;
    /**
     * host of the web server. If set to the empty string, Sparks default will be used
     */
    public String host = "localhost";
    /**
     * port of the web server. If set to the empty string, Sparks default will be used
     */
    public String port = "4567";
    /**
     * dev URL
     * in case the static js/css/assets are not served by natbraille-editor-server
     * i.e. in dev situation where webpack dev web server is used, it will be found
     * at devHost:devPort.
     * This is used for redirection to a login page hosted by dev server
     */
    public String devURL = "http://localhost:5173";
    /**
     * This is used to build a link to the registration challenge URL which is
     * passed in an email
     */
    public String publicServerURLForExternalLinks = "http://localhost:5173";
    /**
     * directory where static files resides (i.e. index.js and assets from the frontend)
     * If set to the empty string, no static files are served.
     * Must be an absolute path, not a relative path.
     */
    public String staticFilesDirectory = "";
    /**
     * selects the source of data for User Documents. If set to true javax.sql.DataSource interface is used, if set to false, the local filesystem is used.
     */
    public Boolean useDb = false;
    /**
     * dbConnectionString for javax.sql.DataSource interface (if {@link #useDb} is true) of User Documents
     */
    public String dbConnectionString = "jdbc:postgresql://localhost:5432/user_documents?user=postgres&password=pgpwd";
    /**
     * Root in local filesystem (if {@link #useDb} is false) where user documents are stored
     */
    public String documentsRoot = "./SERVER_DOCUMENTS_ROOT";
    /**
     * smtp transport configuration properties
     */
    public HashMap<String, String> smtp;

    /**
     * email recipient to which the feedback form content is transmitted
     */
    public String feedbackRecipientEmail = "noreply@example.com";

    public static class EmailConfig {
        /**
         * "From" field of registration email address
         */
        public String fromAddress = "noreply@example.com";
        /**
         * "From" field of registration email name
         */
        public String fromName = "natbraille";
        /**
         * "Reply-To" field of registration email
         */
        public String replyTo = "noreply@example.com";
    }

    public EmailConfig email = new EmailConfig();

    public static String getAsJsonString(ServerConfiguration serverConfiguration) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(serverConfiguration);
    }

    /**
     * When enabled, all transcription steps result will be written as temporary files.
     * {@see #useFixedSingleTempDirectory}
     */
    public boolean writeServerTranscriptionStepsFiles = false;

    /**
     * When enabled, all transcription done from the webservice
     * write intermediary documents in the same fixed debug directory.
     * Only for debug. Do not set to true in a multiuser/production env.
     * This requires {@link #writeServerTranscriptionStepsFiles} to be set to true.
     */
    public boolean useFixedSingleTempDirectory = false;
    /**
     * When enabled, api token check always returns true.
     * Only for debug. Do not set to true in a multiuser/production env.
     */
    public boolean useUniversalApiToken = false;

    /**
     * unoconvert is used for StarMath5 notation to mathml conversion
     * @see org.natbraille.editor.server.unoconvertwrapper.UnoConvert
     */
    public String unoconvertExecutablePath = "/usr/local/bin/unoconvert";

    /**
     * unoservers config list, defaults to one server on port 2003
     */
    public static class UnoServerConfig {
        /**
         * port of unoserver (not the uno port)
         */
        public String port = "2003";

    }
    public List<UnoServerConfig> unoServers = List.of(new UnoServerConfig());

    /**
     * cache base directory for G2 json dictionaries
     */
    public String g2CacheRoot = "./G2_CACHE/";
    /**
     * write the G2 cache at server shutdown
     */
    public boolean writeG2CacheAtShutdown = true;
}
