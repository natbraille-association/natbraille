package org.natbraille.editor.server.users;

public class MaskEmailAddress {
    public static int SHOWN_CHARACTERS_START_DEFAULT_COUNT = 2;
    public static int SHOWN_CHARACTERS_END_DEFAULT_COUNT = 1;
    public static String mask(String email) {
        return mask(email, SHOWN_CHARACTERS_START_DEFAULT_COUNT, SHOWN_CHARACTERS_END_DEFAULT_COUNT);
    }
    public static String mask(String email, int revealStart, int revealEnd) {
        if (email == null || email.isEmpty()) {
            return email;
        }
        int atIndex = email.indexOf('@');
        if (atIndex == -1) {
            return email; // Not a valid email, return as is
        }
        String localPart = email.substring(0, atIndex);
        String domainPart = email.substring(atIndex);
        revealStart = Math.min(revealStart, localPart.length());
        revealEnd = Math.min(revealEnd, localPart.length() - revealStart);
        String start = localPart.substring(0, revealStart);
        String end = localPart.substring(localPart.length() - revealEnd);
        String maskedMiddle = "*".repeat(localPart.length() - revealStart - revealEnd);
        String maskedLocalPart = start + maskedMiddle + end;
        return maskedLocalPart + domainPart;
    }
}
