package org.natbraille.editor.server;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.sql.Connection;
import java.sql.SQLException;

public class DatabaseManager {
    private final HikariDataSource dataSource;

    private static DatabaseManager instance = null;

    public static DatabaseManager getInstance() {
        if (instance == null) {
            synchronized (DatabaseManager.class) {
                if (instance == null) {
                    instance = new DatabaseManager();
                }
            }
        }
        return instance;
    }

    public DatabaseManager() {
        String connectionString = Server.serverConfiguration.dbConnectionString;
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(connectionString);
        config.setIdleTimeout(60000);
        config.setConnectionTimeout(60000);
        config.setMinimumIdle(2);
        config.setMaximumPoolSize(10);
        dataSource = new HikariDataSource(config);
    }

    public Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }

    public void close() {
        dataSource.close();
    }
}
