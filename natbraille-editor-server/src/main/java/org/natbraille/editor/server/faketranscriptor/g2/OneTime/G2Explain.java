package org.natbraille.editor.server.faketranscriptor.g2.OneTime;

import com.opencsv.exceptions.CsvValidationException;
import org.natbraille.core.NatDynamicResolver;
import org.natbraille.core.UserDocumentLoader;
import org.natbraille.core.document.NatDocument;
import org.natbraille.core.document.NatDocumentException;
import org.natbraille.core.filter.NatFilterChain;
import org.natbraille.core.filter.NatFilterConfigurator;
import org.natbraille.core.filter.NatFilterException;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.core.filter.rules.Regle;
import org.natbraille.core.filter.rules.RulesToolKit;
import org.natbraille.core.filter.transcodeur.TranscodeurNormal;
import org.natbraille.core.gestionnaires.AfficheurAnsiConsole;
import org.natbraille.core.gestionnaires.GestionnaireErreur;
import org.natbraille.core.gestionnaires.LogLevel;
import org.natbraille.editor.server.faketranscriptor.g2.G2AdHocDoc;
import org.natbraille.editor.server.faketranscriptor.g2.Morphalou;
import org.natbraille.editor.server.faketranscriptor.g2.MorphalouG2Utils;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * One time search for explaing wich natbraille g2 rule is used for each word of a given lexicon
 * by trying to remove rules one-by-one
 */
public class G2Explain {

    public static class RuleFileVariants {
        public static class RuleFileVariant {
            final Regle missingRule;
            final int missingRuleIndex;
            final byte[] contents;

            public RuleFileVariant(Regle missingRule, int missingRuleIndex, byte[] contents) {
                this.missingRule = missingRule;
                this.missingRuleIndex = missingRuleIndex;
                this.contents = contents;
            }
        }

        final URI uri;
        final List<Regle> rules;
        final List<RuleFileVariant> variants;

        public RuleFileVariants(URI uri, List<Regle> rules, List<RuleFileVariant> variants) {
            this.uri = uri;
            this.rules = rules;
            this.variants = variants;
        }

        public static RuleFileVariants fromURI(URI xsl_g2_dict, NatFilterOptions nfo, GestionnaireErreur ge) throws IOException {

            NatDynamicResolver ndr = new NatDynamicResolver(nfo, ge);
            ArrayList<Regle> allRules = RulesToolKit.getRules(ndr, xsl_g2_dict);

            List<RuleFileVariant> allRuleFileVariants = new ArrayList<>();

            for (int ruleIndex = 0; ruleIndex < allRules.size(); ruleIndex++) {
                Regle rule = allRules.get(ruleIndex);
                //System.out.println(rule.isActif() + " " + "index:" + ruleIndex + " " + rule);
                if (rule.isActif()) {
                    // disable
                    rule.setActif(false);
                    // make ruleFileVariant
                    byte[] ruleFileContents = RulesToolKit.getRuleFileBytes(allRules);
                    // ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    // RulesToolKit.writeRuleFile(allRules, baos);
                    // byte[] ruleFileContents = baos.toByteArray();
                    RuleFileVariant ruleFileVariant = new RuleFileVariant(rule, ruleIndex, ruleFileContents);
                    allRuleFileVariants.add(ruleFileVariant);
                    // re-enable
                    rule.setActif(true);
                }
            }
            return new RuleFileVariants(xsl_g2_dict, allRules, allRuleFileVariants);
        }
    }


    public static void main(String[] args) throws NatFilterException, IOException, TransformerException, NatDocumentException, SAXException, ParserConfigurationException, URISyntaxException, CsvValidationException {

        // BUILD SUCCESSFUL in 8m 3s : List<Morphalou.Word> wordsSubList = words.subList(0, 10);
        // BUILD SUCCESSFUL in 11m 13s : List<Morphalou.Word> wordsSubList = words.subList(0, 100);
        // BUILD SUCCESSFUL in 46m 25s : List<Morphalou.Word> wordsSubList = words.subList(0, 1000);
        // BUILD SUCCESSFUL in 6h 58m 18s : List<Morphalou.Word> wordsSubList = words.subList(0, 10000);
        // 10000 : mer. 06 déc. 2023 17:15:18 CET <- mer. 06 déc. 2023 10:19:56 CET
        // -> 15 jours

        System.setProperty("javax.xml.parsers.DocumentBuilderFactory", "org.apache.xerces.jaxp.DocumentBuilderFactoryImpl");
        System.setProperty("javax.xml.xpath.XPathFactory:http://java.sun.com/jaxp/xpath/dom", "com.sun.org.apache.xpath.internal.jaxp.XPathFactoryImpl");

        // get word list and filter blacklist
        final String path = "/home/vivien/pr-src/doc-g2/morphalou/morphalou/5/Morphalou3.1_formatCSV_toutEnUn/Morphalou3.1_CSV.csv";
        List<Morphalou.Word> words = MorphalouG2Utils.keepOnlyOneInstanceOfAGraphie( Morphalou.parseCsv(path) );
        List<Morphalou.Word> blacklistedWords = new ArrayList<>();
        for (Morphalou.Word word : words) {
            if (MorphalouG2Utils.blacklist.contains(word.getGraphie())) {
                blacklistedWords.add(word);
            }
        }
        words.removeAll(blacklistedWords);
        for (Morphalou.Word word : words) {
            if (word.getGraphie().contains("&")) {
                System.out.println("word" + "!" + word.flexion.id + "!" + "!" + word.getGraphie() + "!");
            }
        }

        //

        AfficheurAnsiConsole afficheurConsole = new AfficheurAnsiConsole(LogLevel.ULTRA);
        afficheurConsole.setLogLevel(LogLevel.NONE);
//        afficheurConsole.setLogLevel(LogLevel.DEBUG);

        GestionnaireErreur ge = new GestionnaireErreur();
        //ge.addAfficheur(afficheurConsole);

        // create rulefile variants
        RuleFileVariants ruleFileVariants;
        {
            String xsl_g2_dict = "nat://system/xsl/dicts/fr-g2.xml";
            NatFilterOptions nfo = new NatFilterOptions();
            nfo.setOption(NatFilterOption.MODE_G2, "true");
            nfo.setOption(NatFilterOption.XSL_G2_DICT, xsl_g2_dict);
            nfo.setOption(NatFilterOption.debug_xsl_processing, "false");
            ruleFileVariants = RuleFileVariants.fromURI(new URI(xsl_g2_dict), nfo, ge);
        }

        List<Morphalou.Word> wordsSubList = words;//.subList(0, 10000);

        // create id / noir map
        Map<String, String> idNoir = new HashMap<>();
        for (Morphalou.Word word : wordsSubList) {
            idNoir.put(word.flexion.id, word.getGraphie());
        }

        // create/init id / used rules map
        Map<String, List<RuleFileVariants.RuleFileVariant>> idUsedRuleFileVariant = new HashMap<>();
        for (Morphalou.Word word : wordsSubList) {
            idUsedRuleFileVariant.put(word.flexion.id, new ArrayList<>());
        }

        // create reference
        Map<String, String> idBrailleReference;
        {
            String resourceName = "nat://user/$explain$/fr-g2-rules/fr-g2.xml";
            NatFilterOptions nfo = new NatFilterOptions();
            nfo.setOption(NatFilterOption.MODE_G2, "true");
            nfo.setOption(NatFilterOption.XSL_G2_DICT, resourceName);
            nfo.setOption(NatFilterOption.debug_xsl_processing, "false");
            UserDocumentLoader adhocUserDocumentLoader = (UserDocumentLoader) (username, kind, name) -> {
                if (username.equals("$explain$") && kind.equals("fr-g2-rules") && name.equals("fr-g2.xml")) {
                    return RulesToolKit.getRuleFileBytes(ruleFileVariants.rules);
                } else {
                    throw new Error("could not load UserDocument");
                }
            };
            NatFilterConfigurator configurator = new NatFilterConfigurator(nfo, ge, adhocUserDocumentLoader);
            NatFilterChain trans = new NatFilterChain(configurator);
            trans.addNewFilter(TranscodeurNormal.class);
            NatDocument inDoc = new NatDocument();
            inDoc.setString(G2AdHocDoc.docDocFromMorphalou(wordsSubList));
            NatDocument outDoc = trans.run(inDoc);
            Document outDomDoc = outDoc.getDocument(configurator.getNatDynamicResolver().getXMLReader());
            idBrailleReference = G2AdHocDoc.extractBrailleStringsFromOutputDocument(outDomDoc);
            System.out.println(idBrailleReference);
        }

        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream((new File("list-of-used-rules.100000-unique-graphie.tsv"))));

        // transcribe using variants
        for (RuleFileVariants.RuleFileVariant ruleFileVariant : ruleFileVariants.variants) {

            System.out.println("#"+ruleFileVariant.missingRuleIndex+"/"+ruleFileVariants.variants.size()+" - "+ruleFileVariant.missingRule.toString());

            String resourceName = "nat://user/$explain$/fr-g2-rules/fr-g2.xml";

            NatFilterOptions nfo = new NatFilterOptions();
            nfo.setOption(NatFilterOption.MODE_G2, "true");
            nfo.setOption(NatFilterOption.XSL_G2_DICT, resourceName);
            nfo.setOption(NatFilterOption.debug_xsl_processing, "false");

            UserDocumentLoader adhocUserDocumentLoader = (UserDocumentLoader) (username, kind, name) -> {
                if (username.equals("$explain$") && kind.equals("fr-g2-rules") && name.equals("fr-g2.xml")) {
                    return ruleFileVariant.contents;
                } else {
                    throw new Error("could not load UserDocument");
                }
            };
            NatFilterConfigurator configurator = new NatFilterConfigurator(nfo, ge, adhocUserDocumentLoader);
            NatFilterChain trans = new NatFilterChain(configurator);
            trans.addNewFilter(TranscodeurNormal.class);
            NatDocument inDoc = new NatDocument();
//            inDoc.setString(G2AdHocDoc.docDocFromWord("c'est-à-dire"));
            inDoc.setString(G2AdHocDoc.docDocFromMorphalou(wordsSubList));
            NatDocument outDoc = trans.run(inDoc);

//            System.out.println("-----------------------------------");
//            System.out.println("-- missing : " + ruleFileVariant.missingRuleIndex + " " + ruleFileVariant.missingRule.toString());
            // System.out.println("===============");
            // System.out.println(outDoc.getString());
            // System.out.println("===============");
            Document outDomDoc = outDoc.getDocument(configurator.getNatDynamicResolver().getXMLReader());
            Map<String, String> idBraille = G2AdHocDoc.extractBrailleStringsFromOutputDocument(outDomDoc);
//            System.out.println(idBraille);


            for (String id : idBraille.keySet()) {
                if (!idBraille.get(id).equals(idBrailleReference.get(id))) {

                    // add
                    idUsedRuleFileVariant.get(id).add(ruleFileVariant);
                    /*String dbg = new StringBuilder()
                            .append("(").append("id").append(":").append(id).append(")")
                            .append("(").append("graphie").append(":").append(idNoir.get(id)).append(")")
                            .append("(").append("braille").append(":").append(idBrailleReference.get(id)).append(")")
                            .append("(").append("wrong-braille").append(":").append(idBraille.get(id)).append(")")
                            .append("(").append("rule").append(":").append(ruleFileVariant.missingRuleIndex).append(")")
                            .append("(").append("rule").append(":").append(ruleFileVariant.missingRule.toString()).append(")")
                            .append("\n")
                            .toString();

                     */
                    String dbg = new StringBuilder()
                            .append(id).append("\t")
                            .append(idNoir.get(id)).append("\t")
                            .append(idBrailleReference.get(id)).append("\t")
                            .append(idBraille.get(id)).append("\t")
                            .append(ruleFileVariant.missingRuleIndex).append("\t")
                            .append(ruleFileVariant.missingRule).append("\t")
                            .append("\n")
                            .toString();
                    bos.write(dbg.getBytes(StandardCharsets.UTF_8));
                    System.out.print(dbg);

                }
            }
        }
        bos.close();

        //String outString = outDoc.getString();
        //System.out.println(outString);
    }

}