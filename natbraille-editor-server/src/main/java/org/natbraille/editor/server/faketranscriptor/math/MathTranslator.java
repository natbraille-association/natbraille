package org.natbraille.editor.server.faketranscriptor.math;

import org.natbraille.core.document.NatDocument;
import org.natbraille.core.document.NatDocumentException;
import org.natbraille.core.filter.NatFilterChain;
import org.natbraille.core.filter.NatFilterConfigurator;
import org.natbraille.core.filter.NatFilterException;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.core.filter.transcodeur.TranscodeurNormal;
import org.natbraille.core.gestionnaires.AfficheurAnsiConsole;
import org.natbraille.core.gestionnaires.GestionnaireErreur;
import org.natbraille.core.gestionnaires.LogLevel;
import org.natbraille.editor.server.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.util.*;

/**
 * Encapsulates Natbraille Math transcription.
 */
public class MathTranslator {
    private static final Logger LOGGER = LoggerFactory.getLogger(MathTranslator.class);
    static final String PhraseFragmentIdPrefix = "equation";

    /**
     * create a Natbraille document string from given <id,MathML string> entries
     */
    public static String docDocFromMaths(Map<String, String> mathMLStrings) {
        // TODO : escape forbidden xml chars in word
        StringBuilder sb = new StringBuilder();
        sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                + "<!DOCTYPE doc:doc SYSTEM \"nat://system/xsl/mmlents/windob.dtd\">"
                + "<doc:doc xmlns:doc=\"espaceDoc\">");
        for (var entry : mathMLStrings.entrySet()) {
            String id = entry.getKey();
            String mathMLString = entry.getValue();
            sb.append("<phrase styleOrig=\"Standard\" fragmentId=\"" + PhraseFragmentIdPrefix).append(id).append("\">");
            //
            // WHY is <mot>X</mot> needed ?
            // a word is created before and after equation
            // - if none is present, the maths is considered to be "alone on its paragraph", so simple or double math prefix
            //   that we need to get would be omitted
            // - using MODE_MATH_ALWAYS_PREFIX option would not work for this purpose because is always put the double math prefix
            // in post-treatement, the words can be ignored the markup is still present.
            //
            sb.append("<lit><mot>X</mot></lit>");
            sb.append(mathMLString);
            sb.append("<lit><mot>X</mot></lit>");
            sb.append("</phrase>");
        }
        sb.append("</doc:doc>");
        return sb.toString();
    }

    public static class NatbraillePreTypesetBraille {
        // [#text: ⴲ⠁ⴰ⠖⠃ⴱ⠶⠜⠰⠦⠉⠴⠆ⴳ]
        private static final String SPECIAL_MATH_START = "ⴲ";
        public static final String SPECIAL_MATH_END = "ⴳ";
        private static final String SPECIAL_CUT_FINE = "ⴱ"; // "coupure esthétique"
        private static final String SPECIAL_CUT_OK = "ⴰ";
        private static final String SPECIAL_TABLE_START = "ⴶ";
        private static final String SPECIAL_TABLE_END = "ⴷ";
        private static final String SPECIAL_BREAKABLE_SPACE = "ⴵ"; // todo
        private static final String SPECIAL_SPACE = "ⴴ"; // todo
        private static final String SPECIAL_LINE_BREAK = "ⴸ"; // todo

        private static final String BRAILLE_FR_MATH_PREFIX_DOUBLE = "⠠⠄";
        private static final String BRAILLE_FR_MATH_PREFIX_SIMPLE = "⠠";
        private static final String SPECIAL_CHARACTER_CLASS = "["
                + SPECIAL_MATH_START + SPECIAL_MATH_END + SPECIAL_CUT_FINE + SPECIAL_CUT_OK
                + SPECIAL_TABLE_START + SPECIAL_TABLE_END + SPECIAL_BREAKABLE_SPACE + SPECIAL_SPACE
                + SPECIAL_LINE_BREAK
                + "]";

        public static String getPrefix(String raw){
            if (raw.startsWith(SPECIAL_MATH_START+BRAILLE_FR_MATH_PREFIX_DOUBLE)){
                return BRAILLE_FR_MATH_PREFIX_DOUBLE;
            } else if (raw.startsWith(SPECIAL_MATH_START+BRAILLE_FR_MATH_PREFIX_SIMPLE)){
                return BRAILLE_FR_MATH_PREFIX_SIMPLE;
            } else {
                LOGGER.error("Unknown prefix on natbraille math output {}",raw);
                return BRAILLE_FR_MATH_PREFIX_SIMPLE;
            }
        }
        private static String unprefixedString(String raw) {
            if (raw.startsWith(SPECIAL_MATH_START+BRAILLE_FR_MATH_PREFIX_DOUBLE)){
                return raw.substring(3);
            } else if (raw.startsWith(SPECIAL_MATH_START+BRAILLE_FR_MATH_PREFIX_SIMPLE)){
                return raw.substring(2);
            } else {
                LOGGER.error("Unknown prefix on natbraille math output {}",raw);
                return raw.substring(2);
            }
            //return raw.replaceAll(BRAILLE_FR_MATH_PREFIX_DOUBLE, "");
        }
        public static String getCleanString(String raw) {
            return unprefixedString(raw).replaceAll(SPECIAL_CHARACTER_CLASS, "");
        }

        public static int[] getHyphenationArray(String raw, int fineScore, int okScore, int noneScore) {
            String unprefixedRaw = unprefixedString(raw);
            int[] scores = new int[unprefixedRaw.length()];
            int position = 0;
            int cutValue = noneScore;
            for (int i = 0; i < scores.length; i++) {
                String character = unprefixedRaw.substring(i, i + 1);
                switch (character) {
                    case SPECIAL_MATH_START:
                    case SPECIAL_MATH_END:
                    case SPECIAL_TABLE_START:
                    case SPECIAL_TABLE_END:
                    case SPECIAL_BREAKABLE_SPACE:
                    case SPECIAL_SPACE:
                    case SPECIAL_LINE_BREAK:
                        break;
                    case SPECIAL_CUT_FINE:
                        cutValue = fineScore;
                        break;
                    case SPECIAL_CUT_OK:
                        cutValue = okScore;
                        break;
                    default:
                        scores[position] = cutValue;
                        position++;
                        cutValue = noneScore;
                        break;
                }
            }
            return Arrays.copyOfRange(scores, 0, position);
        }
    }

    // public static void main(String[] args) {
    //         System.out.println(Arrays.toString(NatbraillePreTypesetBraille.getHypenationArray("0ⴱ12ⴱ345ⴰ6", 9, 6, 1)));
    //  }


    public static String removeExtraSpacing(String rawBrailleMathString){
        // raw String has this form : "ⴲ⠠⠄⠁ⴰ⠖⠃ⴰ⠖⠜⠰⠦⠩⠴⠆ⴳ\n               ⠀\n            \n         ";
        int lastIndexOfEndMath = rawBrailleMathString.lastIndexOf(NatbraillePreTypesetBraille.SPECIAL_MATH_END);
        if (lastIndexOfEndMath!=-1) {
            return rawBrailleMathString.substring(0, lastIndexOfEndMath + 1);
        } else {
            LOGGER.error("Math end special character ⴳ was not found in braille math string {}",rawBrailleMathString);
            return rawBrailleMathString;
        }
    }
    /**
     * Retrieves braille equations from Natbraille transcription result String in a <id,braille String> Map.
     */
    public static Map<String, String> extractBrailleMathStringsFromOutputDocument(Document outDomDoc) {
        Map<String, String> idBraille = new HashMap<>();


        /*   Math nodes output
         *
         *    <par fragmentId="phrase1" modeBraille="3-1">
         *       <math>
         *          <braille>ⴲ⠁ⴰ⠖⠃ⴳ</braille>
         *       </math>
         *    </par>
         */

        /* Chemistry nodes output
         *
         * <par fragmentId="equationf2" modeBraille="3-1">ⴲ⠠⠄⠨⠕⠣ⴳ</par>
         */

        NodeList brailleNodes = outDomDoc.getElementsByTagName("par");
        for (int i = 0; i < brailleNodes.getLength(); i++) {
            Element parNode = (Element) brailleNodes.item(i);
            String fragmentId = parNode.getAttribute("fragmentId");
            if (!fragmentId.isEmpty()) {
                String id = fragmentId.substring(PhraseFragmentIdPrefix.length());
                NodeList oneMaths = parNode.getElementsByTagName("math"); // should be one or zero.
                if (oneMaths.getLength() == 1) {
                    // oneMaths[0] should be a braille math node
                    Element mathNode = (Element)oneMaths.item(0);
                    NodeList oneBrailles = mathNode.getElementsByTagName("braille"); // should be exactly one
                    if (oneBrailles.getLength() == 1 ){
                        Element brailleNode = (Element)oneBrailles.item(0);
                        String brailleString = removeExtraSpacing(brailleNode.getTextContent());
                        idBraille.put(id, brailleString);
                    }
                    Element brailleNode = (Element) brailleNodes.item(i);
                } else if (oneMaths.getLength() == 0) {
                    // par should be a braille chemistry node
                    String brailleString = removeExtraSpacing(parNode.getTextContent());
                    idBraille.put(id, brailleString);
                }
            }
        }

        return idBraille;
    }

    /**
     * Encapsulate MathML String -> Braille String transcription with given NatFilterOptions.
     * Translation is thread safe ("synchronized")
     */
    public static class NatbrailleMathTranslator {
        private final GestionnaireErreur ge;
        private final NatFilterOptions nfo;
        private final NatFilterConfigurator configurator;
        private final NatFilterChain trans;

        public NatbrailleMathTranslator(GestionnaireErreur ge, NatFilterOptions nfo, NatFilterConfigurator configurator, NatFilterChain trans) {
            this.ge = ge;
            this.nfo = nfo;
            this.configurator = configurator;
            this.trans = trans;
        }

        public synchronized Map<String, String> translate(Map<String, String> mathMLStrings) throws NatDocumentException, NatFilterException, IOException, TransformerException {
            NatDocument inDoc = new NatDocument();

            // System.out.println("==============");
            // System.out.println("============== IN ");
            // System.out.println(mathMLStrings);
            // System.out.println("============== /IN");

            // System.out.println("============== INS ");
            // System.out.println(docDocFromMaths(mathMLStrings));
            // System.out.println("============== /INS");


            inDoc.setString(docDocFromMaths(mathMLStrings));
            /*
            inDoc.setString("""
                                        <?xml version="1.0" encoding="UTF-8"?>
                                        <!DOCTYPE doc:doc SYSTEM "nat://system/xsl/mmlents/windob.dtd"><doc:doc xmlns:doc="espaceDoc"><phrase fragmentId="equationf2"><math xmlns="http://www.w3.org/1998/Math/MathML" chemistry="true" fragmentId="f2">
                                                    <semantics  fragmentId="t2">
                                                       <msub fragmentId="t3">
                                                          <mi fragmentId="t4">O</mi>
                                                          <mn fragmentId="t5">2</mn>
                                                       </msub>
                                                    </semantics>
                                                 </math></phrase><phrase fragmentId="equationf3"><math xmlns="http://www.w3.org/1998/Math/MathML" fragmentId="f3" overflow="scroll">
                                                    <semantics  fragmentId="t7">
                                                       <msub fragmentId="t8">
                                                          <mi fragmentId="t9">O</mi>
                                                          <mn fragmentId="t10">2</mn>
                                                       </msub>
                                                    </semantics>
                                                 </math></phrase></doc:doc>
                                        """);
*/

            NatDocument outDoc = trans.run(inDoc);
            String s = outDoc.getString();

            // System.out.println("==============");
            // System.out.println("============== OUT ");
            // System.out.println(s);
            // System.out.println("============== /OUT ");

            Document outDomDoc = outDoc.getDocument(configurator.getNatDynamicResolver().getXMLReader());
            return extractBrailleMathStringsFromOutputDocument(outDomDoc);
        }

        public static NatbrailleMathTranslator build(NatFilterOptions sourceNfo) throws NatFilterException, IOException {
            GestionnaireErreur ge = new GestionnaireErreur();

            AfficheurAnsiConsole afficheurConsole = new AfficheurAnsiConsole(LogLevel.ULTRA);
            //afficheurConsole.setLogLevel(LogLevel.NONE);
            //afficheurConsole.setLogLevel(LogLevel.DEBUG);
            //afficheurConsole.setLogLevel(LogLevel.DEBUG);
            //afficheurConsole.setLogLevel(LogLevel.ULTRA);
            //ge.addAfficheur(afficheurConsole);

            // copy options
            NatFilterOptions nfo = new NatFilterOptions();
            nfo.setOptions(sourceNfo);

            //    nfo.setOption(NatFilterOption.debug_dyn_xsl_show,"true");


            // ensure math is treated
            nfo.setOption(NatFilterOption.MODE_MATH, "true");

            // ensure chemistry is treated
            nfo.setOption(NatFilterOption.MODE_CHEMISTRY, "true");

            // set non math related
            nfo.setOption(NatFilterOption.MODE_G2, "false");

            //nfo.setOption(NatFilterOption.XSL_G2_DICT, resourceName);
            //nfo.setOption(NatFilterOption.debug_xsl_processing, "false");
            nfo.setOption(NatFilterOption.debug_xsl_processing, "true");

            NatFilterConfigurator configurator = new NatFilterConfigurator(nfo, ge);//, adhocUserDocumentLoader);
            NatFilterChain trans = new NatFilterChain(configurator);
            trans.addNewFilter(TranscodeurNormal.class);
            return new NatbrailleMathTranslator(ge, nfo, configurator, trans);
        }
    }

    /**
     * Defines all possible Natbraille math translators from all math-related NatFilterOptions combinations.
     * Acts as a Natbraille math translators cache.
     * Creation and first use takes a lot of time (seconds), so reuse is needed.
     * If a translator exist for a given set of math-related NatFilterOptions, it is reused, else it is built.
     * Translation is thread safe (synchronized)
     */
    public static class NatbrailleMathTranslators {

        /**
         * Defines the set of math related NatFilterOptions values
         * and identify each value combination by a unique hash
         */
        private static class MathRelatedNatFilterOptions {
            private static final List<NatFilterOption> optionsList = Arrays.asList(new NatFilterOption[]{
                    //NatFilterOption.MODE_MATH,
                    NatFilterOption.MODE_MATH_ALWAYS_PREFIX,
                    NatFilterOption.MODE_MATH_SPECIFIC_NOTATION
            });

            public static String getHash(NatFilterOptions nfo) throws NatFilterException {
                StringJoiner joiner = new StringJoiner(",");
                for (NatFilterOption natFilterOption : optionsList) {
                    joiner.add(nfo.getValue(natFilterOption));
                }
                return joiner.toString();
            }
        }

        /**
         * Map of NatbrailleMathTranslator with MathRelatedNatFilterOptions hash as key
         */
        private final Map<String, NatbrailleMathTranslator> translators = new HashMap<>();

        /**
         * Retrieves or create a NatbrailleMathTranslator for given math related NatFilterOptions values
         */
        public NatbrailleMathTranslator getNatbrailleMathTranslator(NatFilterOptions nfo) throws NatFilterException, IOException {
            String hash = MathRelatedNatFilterOptions.getHash(nfo);
            if (translators.containsKey(hash)) {
                return translators.get(hash);
            } else {
                NatbrailleMathTranslator natbrailleMathTranslator = NatbrailleMathTranslator.build(nfo);
                translators.put(hash, natbrailleMathTranslator);
                return natbrailleMathTranslator;
            }
        }
    }

    public static void usage() throws NatFilterException, IOException, TransformerException, NatDocumentException {

        // create NatbrailleMathTranslator Set/Cache (maybe a singleton)
        NatbrailleMathTranslators natbrailleMathTranslators = new NatbrailleMathTranslators();

        // for a given math transcription, retrieve appropriate natbrailleMathTranslators
        NatFilterOptions nfo = new NatFilterOptions();
        nfo.setOption(NatFilterOption.MODE_MATH_SPECIFIC_NOTATION, "true");
        nfo.setOption(NatFilterOption.MODE_MATH_ALWAYS_PREFIX, "true");
        NatbrailleMathTranslator natbrailleMathTranslator = natbrailleMathTranslators.getNatbrailleMathTranslator(nfo);

        // make a map of all equations to translate (processing multiple equations in one pass is s a lot faster)
        Map<String, String> equations = new HashMap<>();
        String mathMLString1 = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\" display=\"block\" overflow=\"scroll\">"
                + "<mrow><mi>a</mi><mo stretchy=\"false\">+</mo><mi>b</mi></mrow>"
                + "</math>";
        equations.put("eq1", mathMLString1);
        String mathMLString2 = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\" display=\"block\" overflow=\"scroll\">"
                + "<mrow><mi>a</mi><mo stretchy=\"false\">-</mo><mi>b</mi></mrow>"
                + "</math>";
        equations.put("eq2", mathMLString2);

        // process
        final Map<String, String> brailleMap = natbrailleMathTranslator.translate(equations);
        System.out.println(brailleMap);
    }

    public static void mainUsage(String[] args) throws NatFilterException, IOException, TransformerException, NatDocumentException {
        usage();
    }

    public static void measure(String[] args) throws NatFilterException, IOException, TransformerException, NatDocumentException {


        NatbrailleMathTranslators natbrailleMathTranslators = new NatbrailleMathTranslators();


        NatFilterOptions nfo = new NatFilterOptions();
        nfo.setOption(NatFilterOption.MODE_MATH_SPECIFIC_NOTATION, "true");
        nfo.setOption(NatFilterOption.MODE_MATH_ALWAYS_PREFIX, "true");

        NatbrailleMathTranslator natbrailleMathTranslator = natbrailleMathTranslators.getNatbrailleMathTranslator(nfo);

        String mathMLString = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\" display=\"block\" overflow=\"scroll\">"
                + "<mrow><mi>a</mi><mo stretchy=\"false\">+</mo><mi>b</mi></mrow>"
                + "</math>";

        // create mathml to be translated
        Map<String, String> mathMLStrings = new HashMap<>();
        double eqByPass = 10;
        for (double i = 1; i <= eqByPass; i++) {
            String fragmentId = Double.toString(i);
            mathMLStrings.put(fragmentId, mathMLString);
        }

        {
            // warm up
            double count = 10;
            for (double i = 0; i < count; i++) {
                final Map<String, String> brailleMap = natbrailleMathTranslator.translate(mathMLStrings);
            }

        }
        double count = 10;
        double t0 = System.currentTimeMillis();
        for (double i = 0; i < count; i++) {
            final Map<String, String> brailleMap = natbrailleMathTranslator.translate(mathMLStrings);
            System.out.println(brailleMap);
        }
        double t1 = System.currentTimeMillis();
        double total_s = (t1 - t0) / 1000.0;
        double mean = total_s / count;
        System.out.println(total_s + "s " + mean + " s/item " + " " + (mean * 1000) + "ms/item");

        // 2eq / item   : 22.8s   0.0228 s/item    22.8ms/items
        // 4eq / item   : 23.199s 0.023199 s/item  23.199ms/items
        // 8eq / item   : 23.669s 0.023669 s/item  23.669ms/items
        // 16eq / item  : 25.434s 0.025434000000000002 s/item  25.434ms/items
        // 32eq / item  : 28.971s 0.028971 s/item  28.971ms/items
        // 64eq / item  : 34.532s 0.03453199999999999 s/item  34.532ms/items
        // 128eq / item : 46.296s 0.046296 s/item  46.296ms/items
        // 256eq / item : 67.776s 0.067776 s/item  67.776ms/items
        // 512eq / item : 108.093s 0.10809300000000001 s/item  108.093ms/items
    }

}
