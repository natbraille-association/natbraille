package org.natbraille.editor.server.faketranscriptor.g2;

import java.util.*;

public class MorphalouG2Utils {
    public static List<String> blacklist = Arrays.asList(new String[]{
            "administrationalisassent",
            "administrationalisassiez",
            "administrationalisassions",
            "administrationaliseraient",
            "administrationaliserions",
            "anticonstitutionnellement",
            "constitutionnalisassions",
            "constitutionnaliseraient",
            "dibromoxymercurifluorescéine",
            "interprofessionnellement",
            "radiocristallographiques",
            "hypothalamohypophysaires",
            "dicyclohexylcarbodiimide",
            "dicyclohexylcarbodiimides",
            "inositohexaphosphoriques",
            "lipomucopolysaccharidose",
            "lipomucopolysaccharidoses",
            "neuropsychopharmacologie",
            "neuropsychopharmacologies",
            "sympathicogonioblastomes",
            "thromboplastinoformation",
            "thromboplastinoformations",
            "chromolithographiassions",
            "chromolithographieraient",
            "R&D",
            "\"R &amp",
            "R&D",
            "R &amp; D"
    });


    public static List<Morphalou.Word> keepOnlyOneInstanceOfAGraphie(List<Morphalou.Word> words) {
        List<Morphalou.Word> filtered = new ArrayList<>();
        System.out.println("before filter:" + words.size());
        Set<String> alreadySeenGraphie = new HashSet<>();
        for (Morphalou.Word word : words) {
            String graphie = word.getGraphie();
            if (!alreadySeenGraphie.contains(graphie)) {
                filtered.add(word);
                alreadySeenGraphie.add(graphie);
            }
        }
        System.out.println("after filter:" + filtered.size());
        return filtered;
    }

}
