package org.natbraille.editor.server;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ServerConfigurationReader {
    private static Logger LOGGER = LoggerFactory.getLogger(ServerConfigurationReader.class);

    private static Path serverConfigurationFilename = Paths.get("server.conf.json");

    /*
     * Ordered Paths where configuration is searched ; only the first found is used
     */
    private static Path[] configurationPaths = new Path[]{
            // 1. config found in user directory (i.e. current directory)
            Paths.get(System.getProperty("user.dir")),
            // 2. config found in ".config" subdirectory of user directory (i.e. current directory)
            Paths.get(System.getProperty("user.dir"), ".config"),
            // 3. config found in user home config directory
            Paths.get(System.getProperty("user.home"), ".config", "natbraille"),
            // 4. config found in system config directory
            Paths.get("/", "etc", "natbraille"),
    };
    private static ObjectMapper mapper = new ObjectMapper();

    public static ServerConfiguration parseServerConfiguration(Path filePath) throws IOException {
        return mapper.readValue(
                Files.readString(filePath),
                ServerConfiguration.class
        );
    }
    public static ServerConfiguration getServerConfiguration() {
        for (Path configurationPath : configurationPaths) {
            Path filePath = configurationPath.resolve(serverConfigurationFilename);
            try {
                ServerConfiguration serverConfiguration = parseServerConfiguration(filePath);
                LOGGER.info("using config file {}", filePath);
                return serverConfiguration;
            } catch (JsonParseException e) {
                LOGGER.info("not a valid config file {}", filePath, e);
            } catch (IOException e) {
                LOGGER.debug("no config file {}", filePath, e);
            }
        }
        LOGGER.info("no valid config file found");
        return null;
    }
}
