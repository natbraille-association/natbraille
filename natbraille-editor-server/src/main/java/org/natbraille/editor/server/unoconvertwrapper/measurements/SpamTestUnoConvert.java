package org.natbraille.editor.server.unoconvertwrapper.measurements;

import org.natbraille.editor.server.unoconvertwrapper.UnoConvert;

import java.util.ArrayList;
import java.util.List;

public class SpamTestUnoConvert {

    static final List<Boolean> successes = new ArrayList<>();

    public static List<Double> allTimes = new ArrayList<>();

    public static synchronized boolean task() {
        long startTime = System.nanoTime(); // Start the timer
        boolean testResult = UnoConvert.isUnoStarMathToMathMLOk();
        long endTime = System.nanoTime(); // Start the timer
        double duration_s = (endTime - startTime) / 1_000_000_000.0;
        allTimes.add(duration_s);
        return testResult;
    }

    public static class SpamTestThread extends Thread {
        public final int threadIndex;
        public final int count;
        public int successes = 0;

        public List<Double> successTimes = new ArrayList<>();
        public List<Double> failTimes = new ArrayList<>();

        public SpamTestThread(int threadIndex, int count) {
            this.threadIndex = threadIndex;
            this.count = count;
        }

        @Override
        public void run() {
            for (int i = 0; i < count; i++) {
                long startTime = System.nanoTime(); // Start the timer
                boolean testResult = task();
                long endTime = System.nanoTime(); // Start the timer
                double duration_s = (endTime - startTime) / 1_000_000_000.0;
                if (testResult) {
                    successes++;
                    successTimes.add(duration_s);
                } else {
                    System.out.printf("[thread %d] %d %d %b\n", threadIndex, i, count, testResult);
                    failTimes.add(duration_s);
                    break;
                }
            }
        }
    }


    public static void main(String[] args) throws InterruptedException {

        int opCountByThread = 100;
        int threadCount = 200;

        // int opCountByThread = 10;
        // int threadCount = 20;

        SpamTestThread[] threads = new SpamTestThread[threadCount];

        System.out.printf("opCountByThread:%d threadCount:%d\n", opCountByThread, threadCount);
        for (int i = 0; i < threadCount; i++) {
            threads[i] = new SpamTestThread(i, opCountByThread);
        }
        for (int i = 0; i < threadCount; i++) {
            threads[i].start();
        }
        boolean ko = false;
        for (int i = 0; i < threadCount; i++) {
            threads[i].join();
            if (threads[i].successes != opCountByThread) {
                ko = true;
                System.out.println("fail thread" + threads[i].threadIndex + " did " + threads[i].successes + " / " + opCountByThread);
            }
        }
        for (int i = 0; i < threadCount; i++) {
            SpamTestThread t = threads[i];
            StringBuilder sb = new StringBuilder();
            sb.append("thread").append(i).append(" ");
            if (!t.successTimes.isEmpty()) {
                BasicStats bs = new BasicStats(t.successTimes);
                sb.append("success times").append(" max:").append(bs.max).append(" min:").append(bs.min).append(" avg:").append(bs.avg).append(" med:").append(bs.med).append(" ; ");
            }
            if (!t.failTimes.isEmpty()) {
                BasicStats bs = new BasicStats(t.failTimes);
                sb.append("fail times").append(" max:").append(bs.max).append(" min:").append(bs.min).append(" avg:").append(bs.avg).append(" med:").append(bs.med).append(" ; ");
            }
            System.out.println(sb);
        }
        if (ko) {
            System.out.println("Something went wrong");
        } else {
            System.out.println("All went ok !");
        }

        {
            BasicStats bs = new BasicStats(allTimes);
            System.out.println("overall :: " + "success times" + " max:" + bs.max + " min:" + bs.min + " avg:" + bs.avg + " med:" + bs.med + " ; ");
        }

    }
}
