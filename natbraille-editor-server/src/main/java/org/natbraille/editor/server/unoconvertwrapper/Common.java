package org.natbraille.editor.server.unoconvertwrapper;

import org.natbraille.editor.server.tools.SimpleXMLString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static org.natbraille.editor.server.Server.serverConfiguration;

public class Common {
    private static final Logger LOGGER = LoggerFactory.getLogger(Common.class);

    public enum UnoDestinationFormats {
        MML("mml"),
        ODT("odt");
        public final String cliString;

        UnoDestinationFormats(String cliString) {
            this.cliString = cliString;
        }
    }

    public static String mmlSourceDocument(String starMath5) {
        return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                + "\n"
                + "<math xmlns=\"http://www.w3.org/1998/Math/MathML\" display=\"block\">"
                + "\n"
                + "<semantics>"
                + "\n"
                + "<annotation encoding=\"StarMath 5.0\">" + SimpleXMLString.encodeXML(starMath5) + "</annotation>"
                + "\n"
                + "</semantics>"
                + "\n"
                + "</math>";
    }

    public static void pipeStream(InputStream input, OutputStream output) throws IOException {
        byte[] buffer = new byte[1024];
        int numRead = 0;
        do {
            numRead = input.read(buffer);
            if (numRead > 0) {
                output.write(buffer, 0, numRead);
            }
        } while (input.available() > 0);
        output.flush();
    }

    /**
     * Wraps a call to unoconvert
     *
     * @param source            input document bytes
     * @param destinationFormat destination format
     * @param unoServerPort     unoserver port (the python wrapper, not the libreoffice uno port itself), defaults to unconvert default value if null
     * @return
     */
    public static byte[] convertTo(byte[] source, UnoDestinationFormats destinationFormat, String unoServerPort) {
        // https://stackoverflow.com/questions/11336157/running-external-program-with-redirected-stdin-and-stdout-from-java
        //  LOGGER.info("ENTER CONVERT TO");
        ByteArrayInputStream bais = new ByteArrayInputStream(source);
        ByteArrayOutputStream baosOut = new ByteArrayOutputStream();
        ByteArrayOutputStream baosErr = new ByteArrayOutputStream();

        OutputStream procIn = null;
        InputStream procOut = null;
        InputStream procErr = null;
        String unoconvertExecutablePath = serverConfiguration.unoconvertExecutablePath;

        try {
            List<String> commandArray = new ArrayList<>();
            commandArray.add(unoconvertExecutablePath);
            if (unoServerPort != null) {
                commandArray.add("--port");
                commandArray.add(unoServerPort);
            }
            commandArray.add("--convert-to");
            commandArray.add(destinationFormat.cliString);
            commandArray.add("-");
            commandArray.add("-");
            Process process = Runtime.getRuntime().exec(commandArray.toArray(new String[0]));

            procIn = process.getOutputStream();
            procErr = process.getErrorStream();
            procOut = process.getInputStream();
            pipeStream(bais, procIn);
            procIn.close();
            pipeStream(procErr, baosErr);
            pipeStream(procOut, baosOut);
        } catch (IOException e) {
            LOGGER.error(e.toString());
            try {
                if (procIn != null) procIn.close();
            } catch (Exception ee) {
                LOGGER.error(ee.toString());
            }
        }
        if (baosErr.size() != 0) {
            if (!baosErr.toString().trim().equals("INFO:unoserver:Connecting.")) {
                System.out.println(baosErr.toString());
                LOGGER.error(baosErr.toString());
            }
        }
        // LOGGER.info("EXIT CONVERT TO");
        return baosOut.toByteArray();
    }

}
