package org.natbraille.editor.server.formats;

public class AllTranscriptionSteps {
    public String _0_source ;
    public String _1_pre ;
    public String _2_fragmentId;
    public String _3_trans;
    public String _4_mep_xml;
    public String _4_mep_txt;
    public String _5_table;

    public String _5b_table_text_only;
}
