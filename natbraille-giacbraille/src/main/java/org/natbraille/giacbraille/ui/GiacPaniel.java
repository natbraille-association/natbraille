/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.natbraille.giacbraille.ui;

import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javagiac.gen;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.xml.parsers.ParserConfigurationException;
import org.natbraille.brailletable.BrailleTable;
import org.natbraille.brailletable.BrailleTableException;
import org.natbraille.brailletable.BrailleTables;
import org.natbraille.brailletable.system.SystemBrailleTables;
import org.natbraille.core.filter.NatFilterException;
import org.natbraille.giacbraille.GiacBraille;
import org.natbraille.giacbraille.GiacFuncs.GiacFunc;

/**
 *
 * @author vivien
 */
public class GiacPaniel extends javax.swing.JFrame {

    final GiacBraille giacBraille;
    final List<JCheckBoxMenuItem> brailleTableItems;
    final GiacBrailleLog giacBrailleLog;

    final List<String> namesOfChoosenOnes = new ArrayList<>();

    {
        namesOfChoosenOnes.add("_eval");
        namesOfChoosenOnes.add("simplify");
        namesOfChoosenOnes.add("_float");
        namesOfChoosenOnes.add("_floor");
    }

    private void addToChoosenOne(GiacFunc giacFunc) {
        if (!namesOfChoosenOnes.contains(giacFunc.name)) {
            jMenu5.add(buildOperationMenuItem(giacFunc));
            namesOfChoosenOnes.add(giacFunc.name);
        }
    }

    private void updateFromGiacInput(String giacInput, boolean updateAndEval) throws Exception {

        gen giacGenOrig = giacBraille.getGenFromGiacInput(giacInput);
        gen giacGen;
        if (updateAndEval) {
            GiacFunc func = giacBraille.getGiacFuncs().byName("_eval");
            giacGen = giacBraille.applyGenFunc(func, giacGenOrig);
        } else {
            giacGen = giacGenOrig;
        }

        // append to giac input history
        if (jTextAreaGiacInput.getText().length() > 1) {
            jTextAreaGiacInput.append("\n");
        }
        jTextAreaGiacInput.append(giacBraille.getGiacInputFromGen(giacGen));

        //  append to braille history
        if (jTextAreaBraille.getText().length() > 1) {
            jTextAreaBraille.append("\n");
        }
        jTextAreaBraille.append(giacBraille.getBrailleFromGen(giacGen));

        // mathml text
        jTextAreaMathmlString.setText(giacBraille.getMathmlStringFromGen(giacGen));

        // visual mathml
        jMathComponent.setDocument(giacBraille.getMathmlNodeFromGen(giacGen));

        // replace input
        jTextFieldGiacInput.setText(giacBraille.getGiacInputFromGen(giacGen));

        int lastCarriageReturnPos = jTextAreaBraille.getText().lastIndexOf("\n");
        jTextAreaBraille.setCaretPosition(lastCarriageReturnPos + 1);

        int lastCarriageReturnPos2 = jTextAreaGiacInput.getText().lastIndexOf("\n");
        jTextAreaGiacInput.setCaretPosition(lastCarriageReturnPos2 + 1);

    }

    private String getCurrentGiacInputString() {
        return jTextFieldGiacInput.getText();
    }

    public final JMenuItem buildOperationMenuItem(final GiacFunc giacFunc) {
        JMenuItem newOpMenuItem = new javax.swing.JMenuItem();
        newOpMenuItem.setText(giacFunc.name);// + " " + giacFunc.params.size());
        newOpMenuItem.setActionCommand(giacFunc.name);
        newOpMenuItem.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {

                try {
                    String giacInputString = getCurrentGiacInputString();
                    gen giacGen = giacBraille.getGenFromGiacInput(giacInputString);
                    gen newGen = giacBraille.applyGenFunc(giacFunc, giacGen);
                    String newgiacInput = giacBraille.getGiacInputFromGen(newGen);
                    updateFromGiacInput(newgiacInput, false);
                    addToChoosenOne(giacFunc);
                } catch (Exception ex) {
                    Logger.getLogger(GiacPaniel.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        });
        return newOpMenuItem;

    }

    private void selectBrailleTableItem(BrailleTable brailleTable) {
        for (JCheckBoxMenuItem brailleTableItem : brailleTableItems) {
            brailleTableItem.setSelected(brailleTableItem.getText().equals(brailleTable.getName()));
        }
    }

    private void selectMathSpecificNotation(boolean useSpecificNotation) throws IOException, NatFilterException {
        jCheckBoxMenuItemModeMathUseSpecific.setSelected(useSpecificNotation);
    }

    public final JCheckBoxMenuItem buildBrailleTableItem(final BrailleTable brailleTable) {
        final JCheckBoxMenuItem newBrailleTableMenuItem = new javax.swing.JCheckBoxMenuItem();
        newBrailleTableMenuItem.setText(brailleTable.getName());
        newBrailleTableMenuItem.setActionCommand(brailleTable.getName());
        newBrailleTableMenuItem.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try {
                    giacBraille.setBrailleTable(brailleTable);
                    selectBrailleTableItem(brailleTable);
                    updateFromGiacInput(getCurrentGiacInputString(), false);
                } catch (IOException | NatFilterException ex) {
                    Logger.getLogger(GiacPaniel.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(GiacPaniel.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        return newBrailleTableMenuItem;

    }

    /**
     * Creates new form GiacPaniel
     *
     * @throws org.natbraille.brailletable.BrailleTableException
     */
    public GiacPaniel() throws BrailleTableException, ParserConfigurationException, NatFilterException, IOException {

        initComponents();
        setTitle("Natbraille-giac");
        jScrollPaneMathmlString.setVisible(false);
        //
        giacBrailleLog = new GiacBrailleLog();
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                giacBrailleLog.setVisible(false);
            }
        });

        giacBraille = new GiacBraille();
        giacBraille.getGestionnaireErreur().addAfficheur(giacBrailleLog.getAsAfficheur());

        // build operations menu
        List<GiacFunc> directFuncs = giacBraille.getGiacFuncs().getDirectGenFuncs();
        int count = 0;
        JMenu appendToMenu = null;
        String lastName = null;

        for (GiacFunc giacFunc : directFuncs) {

            // populate everything menu
            String abrvName = giacFunc.name;
            if (abrvName.length() > 4) {
                abrvName = abrvName.substring(0, 4);
            }
            if (count == 0) {
                if (lastName != null) {
                    appendToMenu.setText(appendToMenu.getText() + lastName);
                }
                appendToMenu = new JMenu();
                jMenuOperations.add(appendToMenu);
                appendToMenu.setText(abrvName + " ... ");
                count = 30;

            }
            lastName = abrvName;
            appendToMenu.add(buildOperationMenuItem(giacFunc));
            count--;
            // populate choosen menu
            if (namesOfChoosenOnes.contains(giacFunc.name)) {
                jMenu5.add(buildOperationMenuItem(giacFunc));
            }

        }
        brailleTableItems = new ArrayList<>();
        // build braille table option menu
        for (String brailleTableName : SystemBrailleTables.getNames()) {

            BrailleTable brailleTable = BrailleTables.forName(brailleTableName);
            JCheckBoxMenuItem item = buildBrailleTableItem(brailleTable);
            jMenuOptionBrailleTables.add(item);
            brailleTableItems.add(item);

        }
        selectBrailleTableItem(giacBraille.getBrailleTable());
        selectMathSpecificNotation(giacBraille.getMathSpecificNotation());
        //
        //  String defaultMathml = "<math><mi>5</mi><mo>+</mo><mi>3</mi><mi>5</mi><mo>+</mo><mi>3</mi></math>";
        // jMathComponent1.setDocument(GiacUtils.getMathmlNodeFromMathmlString(GiacUtils.buildDocumentBuilderFactory(), defaultMathml));

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMathComponent = new net.sourceforge.jeuclid.swing.JMathComponent();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextAreaGiacInput = new javax.swing.JTextArea();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextAreaBraille = new javax.swing.JTextArea();
        jScrollPaneMathmlString = new javax.swing.JScrollPane();
        jTextAreaMathmlString = new javax.swing.JTextArea();
        jTextFieldGiacInput = new javax.swing.JTextField();
        jCheckBox1 = new javax.swing.JCheckBox();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenu5 = new javax.swing.JMenu();
        jMenuOperations = new javax.swing.JMenu();
        jMenu4 = new javax.swing.JMenu();
        jMenuOptionBrailleTables = new javax.swing.JMenu();
        jCheckBoxMenuItemModeMathUseSpecific = new javax.swing.JCheckBoxMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jCheckBoxMenuItem1 = new javax.swing.JCheckBoxMenuItem();
        jCheckBoxMenuItem2 = new javax.swing.JCheckBoxMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jMathComponent.setMinimumSize(new java.awt.Dimension(50, 50));

        jTextAreaGiacInput.setColumns(20);
        jTextAreaGiacInput.setRows(5);
        jScrollPane1.setViewportView(jTextAreaGiacInput);

        jTextAreaBraille.setColumns(20);
        jTextAreaBraille.setLineWrap(true);
        jTextAreaBraille.setRows(5);
        jScrollPane2.setViewportView(jTextAreaBraille);

        jTextAreaMathmlString.setEditable(false);
        jTextAreaMathmlString.setColumns(20);
        jTextAreaMathmlString.setLineWrap(true);
        jTextAreaMathmlString.setRows(5);
        jScrollPaneMathmlString.setViewportView(jTextAreaMathmlString);

        jTextFieldGiacInput.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldGiacInputActionPerformed(evt);
            }
        });
        jTextFieldGiacInput.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextFieldGiacInputKeyPressed(evt);
            }
        });

        jCheckBox1.setText("jCheckBox1");

        jMenu1.setText("Fichier");

        jMenuItem4.setText("Quitter");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem4);

        jMenuBar1.add(jMenu1);

        jMenu3.setText("Aller");

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_B, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem1.setText("Braille");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem1);

        jMenuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_G, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem2.setText("Giac");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem2);

        jMenuItem3.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem3.setText("Saisie");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem3);

        jMenuBar1.add(jMenu3);

        jMenu5.setText("Calcul");
        jMenuBar1.add(jMenu5);

        jMenuOperations.setText("Opérations");
        jMenuBar1.add(jMenuOperations);

        jMenu4.setText("Options");

        jMenuOptionBrailleTables.setText("table braille");
        jMenu4.add(jMenuOptionBrailleTables);

        jCheckBoxMenuItemModeMathUseSpecific.setSelected(true);
        jCheckBoxMenuItemModeMathUseSpecific.setText("abréger les symboles");
        jCheckBoxMenuItemModeMathUseSpecific.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxMenuItemModeMathUseSpecificActionPerformed(evt);
            }
        });
        jMenu4.add(jCheckBoxMenuItemModeMathUseSpecific);

        jMenu2.setText("vues");

        jCheckBoxMenuItem1.setText("debug natbraille");
        jCheckBoxMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxMenuItem1ActionPerformed(evt);
            }
        });
        jMenu2.add(jCheckBoxMenuItem1);

        jCheckBoxMenuItem2.setText("debug mathml");
        jCheckBoxMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxMenuItem2ActionPerformed(evt);
            }
        });
        jMenu2.add(jCheckBoxMenuItem2);

        jMenu4.add(jMenu2);

        jMenuBar1.add(jMenu4);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jMathComponent, javax.swing.GroupLayout.DEFAULT_SIZE, 80, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPaneMathmlString, javax.swing.GroupLayout.DEFAULT_SIZE, 211, Short.MAX_VALUE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jTextFieldGiacInput))
                .addGap(12, 12, 12))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPaneMathmlString)
                    .addComponent(jMathComponent, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 88, Short.MAX_VALUE)
                .addGap(2, 2, 2)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 88, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextFieldGiacInput, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    private void jCheckBoxMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxMenuItem1ActionPerformed
        giacBrailleLog.setVisible(!giacBrailleLog.isVisible());
    }//GEN-LAST:event_jCheckBoxMenuItem1ActionPerformed

    private void jCheckBoxMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxMenuItem2ActionPerformed
        jScrollPaneMathmlString.setVisible(!jScrollPaneMathmlString.isVisible());
        pack();
    }//GEN-LAST:event_jCheckBoxMenuItem2ActionPerformed

    private void jTextFieldGiacInputKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldGiacInputKeyPressed
        //if (evt.isControlDown() && (evt.getKeyCode() == KeyEvent.VK_ENTER)) {
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            try {
                String text = getCurrentGiacInputString();
                updateFromGiacInput(text, evt.isControlDown());
            } catch (Exception ex) {
                Logger.getLogger(GiacPaniel.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }//GEN-LAST:event_jTextFieldGiacInputKeyPressed

    private void jTextFieldGiacInputActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldGiacInputActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldGiacInputActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        jTextAreaBraille.requestFocus();
        int lastCarriageReturnPos = jTextAreaBraille.getText().lastIndexOf("\n");
        jTextAreaBraille.setCaretPosition(lastCarriageReturnPos + 1);
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        jTextAreaGiacInput.requestFocus();
        int lastCarriageReturnPos = jTextAreaGiacInput.getText().lastIndexOf("\n");
        jTextAreaGiacInput.setCaretPosition(lastCarriageReturnPos + 1);
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        jTextFieldGiacInput.requestFocus();
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        giacBrailleLog.dispose();
        dispose();
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    private void jCheckBoxMenuItemModeMathUseSpecificActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxMenuItemModeMathUseSpecificActionPerformed

        try {
            giacBraille.setMathSpecificNotation(!giacBraille.getMathSpecificNotation());
            updateFromGiacInput(getCurrentGiacInputString(), false);
        } catch (IOException ex) {
            Logger.getLogger(GiacPaniel.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NatFilterException ex) {
            Logger.getLogger(GiacPaniel.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(GiacPaniel.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_jCheckBoxMenuItemModeMathUseSpecificActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new GiacPaniel().setVisible(true);
                } catch (BrailleTableException | ParserConfigurationException | NatFilterException | IOException ex) {
                    Logger.getLogger(GiacPaniel.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JCheckBoxMenuItem jCheckBoxMenuItem1;
    private javax.swing.JCheckBoxMenuItem jCheckBoxMenuItem2;
    private javax.swing.JCheckBoxMenuItem jCheckBoxMenuItemModeMathUseSpecific;
    private net.sourceforge.jeuclid.swing.JMathComponent jMathComponent;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenu jMenuOperations;
    private javax.swing.JMenu jMenuOptionBrailleTables;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPaneMathmlString;
    private javax.swing.JTextArea jTextAreaBraille;
    private javax.swing.JTextArea jTextAreaGiacInput;
    private javax.swing.JTextArea jTextAreaMathmlString;
    private javax.swing.JTextField jTextFieldGiacInput;
    // End of variables declaration//GEN-END:variables
}
