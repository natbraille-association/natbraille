package org.natbraille.giacbraille;
// -*- compile-command: "javac *.java" -*-
// This example illustrates how giac can be used from Java using SWIG.
// On linux type java runme, on mac os x with giac 32 bits, java -d32 runme

import java.io.ByteArrayInputStream;
import java.io.IOException;
import javagiac.context;
import javagiac.gen;
import javagiac.giac;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.natbraille.core.document.NatDocument;
import org.natbraille.core.document.NatDocumentException;
import org.natbraille.core.filter.NatFilterException;
import org.natbraille.core.NatDynamicResolver;
import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.core.filter.transcriptor.GiacConverter;
import org.natbraille.core.gestionnaires.Afficheur;
import org.natbraille.core.gestionnaires.AfficheurAnsiConsole;
import org.natbraille.core.gestionnaires.GestionnaireErreur;
import org.natbraille.core.gestionnaires.LogLevel;
import org.w3c.dom.Document;

/*
 * Static methods for GiacBraille objects.
 */
public class GiacUtils {

    static {
        boolean found = false;
        System.out.println("default library path" + System.getProperty("java.library.path"));
        for (String libraryName : new String[]{"javagiac64", "javagiac"}) {
            if (found) {
                System.out.println("oounf");
                continue;
            }
            try {
                System.out.println("Loading giac java interface "+ libraryName+ "...");
                System.loadLibrary(libraryName);
                found = true;
            } catch (UnsatisfiedLinkError e) {
                System.err.println("Cannot find native library :" + libraryName + " " + e);
            }
            if (found) {
                System.out.println("Giac java interface loaded");
            }
        }
    }

    public static GiacConverter buildGiacConverter() {

        // create error handler and display
        GestionnaireErreur ge = new GestionnaireErreur();
        Afficheur aff = new AfficheurAnsiConsole(LogLevel.NONE);
        ge.addAfficheur(aff);

        GiacConverter newGiacConverter = null;
        try {
            newGiacConverter = new GiacConverter(ge);   
            //	    giacConverter.setOption(NatFilterOption.FORMAT_OUTPUT_BRAILLE_TABLE, SystemBrailleTables.TbFr2007);	    
//            newGiacConverter.setOption(NatFilterOption.debug_document_show, "true");
//            newGiacConverter.setOption(NatFilterOption.debug_document_write_options_file, "true");
//            newGiacConverter.setOption(NatFilterOption.debug_document_show_max_char, "10000");
//            newGiacConverter.setOption(NatFilterOption.debug_document_write_temp_file, "true");
//            newGiacConverter.setOption(NatFilterOption.debug_dyn_xsl_show, "true");
            System.out.println("Natbraille starting...");

           // String testmathml = "<math></math>";
           // NatDocument doc = newGiacConverter.convert(testmathml);

            System.out.println("Natbraille ready.");
        } catch (NatFilterException ex) {
            ex.printStackTrace();
        }/* catch (IOException ex) {
        //    Logger.getLogger(GiacUtils.class.getName()).log(Level.SEVERE, null, ex);
          
      //  }*/
        return newGiacConverter;
    }

    public static context buildContext() {
        context c = new context();
        return c;
    }

    public static DocumentBuilderFactory buildDocumentBuilderFactory() {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        factory.setExpandEntityReferences(true);
        return factory;
    }

    // TODO ; pass gest err param & nat filter options
    public static DocumentBuilder buildDocumentBuilder(DocumentBuilderFactory documentBuilderFactory) throws ParserConfigurationException, NatFilterException {
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        NatDynamicResolver ndr = new NatDynamicResolver(new NatFilterOptions(), new GestionnaireErreur());
        documentBuilder.setEntityResolver(ndr);
        documentBuilder.setErrorHandler(ndr);
        return documentBuilder;
    }

    public static String getBrailleFromMathmlString(GiacConverter giacConverter, String mathml) {
        String resu = null;
        try {
            NatDocument doc = giacConverter.convert(mathml);
            resu = doc.getString();
            if (resu.length() > 0) {
                if ((resu.charAt(resu.length() - 1) == '\n') || (resu.charAt(resu.length() - 1) == '\r')) {
                    resu = resu.substring(0, resu.length() - 1);
                }
            }
        } catch (NatFilterException | NatDocumentException | IOException | TransformerException ex) {
            ex.printStackTrace();
        }
        if (resu == null) {
            resu = "error!";
        }
        System.out.println("              braille : " + resu);
        return resu;
    }

    public static gen getGenFromGiacInput(context c, String giacInput) {
        gen g = new gen(giacInput, c);
        System.out.println("* Created gen of type : " + g.getType());
        System.out.println("              giac    : " + g.print(c));
        return g;
    }

    public static String getMathmlStringFromGen(context c, gen g) {

        String mathml = "<math>" + giac.gen2mathml(g, c) + "</math>";
        System.out.println("              mathml  : " + mathml);
        return mathml;
    }

    public static String getGiacInputFromGen(context c, gen g) {
        return g.print(c);
    }

    public static Document loadXMLFromString(DocumentBuilder documentBuilder, String xml) throws Exception {

        String xmlString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        String doctype = "<!DOCTYPE math PUBLIC \"-//W3C//DTD MathML 2.0//EN\" \n\"http://www.w3.org/Math/DTD/mathml2/mathml2.dtd\">";
        xml = xmlString + "\n" + doctype + "\n" + xml + "\n";
        System.err.println(xml);
        return documentBuilder.parse(new ByteArrayInputStream(xml.getBytes()));
    }

    public static Document getMathmlDocumentFromMathmlString(DocumentBuilder documentBuilder, String mathml) throws Exception {
        Document doc = loadXMLFromString(documentBuilder, mathml);
        return doc;

    }

}
