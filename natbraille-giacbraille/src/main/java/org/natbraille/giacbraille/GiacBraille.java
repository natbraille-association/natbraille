/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.natbraille.giacbraille;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javagiac.context;
import javagiac.gen;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.natbraille.brailletable.BrailleTable;
import org.natbraille.brailletable.BrailleTableException;
import org.natbraille.brailletable.BrailleTables;
import org.natbraille.core.filter.NatFilterException;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.filter.transcriptor.GiacConverter;
import org.natbraille.core.gestionnaires.GestionnaireErreur;
import org.w3c.dom.Node;

/**
 *
 * @author vivien
 */
public class GiacBraille {

    /**
     * Giac mathml to braille Converter
     */
    GiacConverter giacConverter;
    /**
     * Giac context
     */
    context giacContext;
    /**
     * Xml builder factory for dom mathml
     */
    DocumentBuilderFactory documentBuilderFactory;
    
    /**
     * Xml builder for dom mathml
     */
    DocumentBuilder documentBuilder;
    
    
    /**
     * Giac functions
     */
    GiacFuncs giacFuncs;

    public GiacFuncs getGiacFuncs() {
        return giacFuncs;
    }

    public GiacBraille(GiacConverter giacConverter, context giacContext, DocumentBuilderFactory documentBuilderFactory, DocumentBuilder documentBuilder, GiacFuncs giacFuncs) {
        this.giacConverter = giacConverter;
        this.giacContext = giacContext;
        this.documentBuilderFactory = documentBuilderFactory;
        this.documentBuilder = documentBuilder;
        this.giacFuncs = giacFuncs;
    }


    public GiacBraille() throws ParserConfigurationException, NatFilterException {
        
        this.giacConverter = GiacUtils.buildGiacConverter();
        this.giacContext = GiacUtils.buildContext();
        this.documentBuilderFactory = GiacUtils.buildDocumentBuilderFactory();
        this.documentBuilder = GiacUtils.buildDocumentBuilder(documentBuilderFactory);
        this.giacFuncs = new GiacFuncs();

    }
    public GestionnaireErreur getGestionnaireErreur(){
        return this.giacConverter.getGestionnaireErreur();
    }
    public gen getGenFromGiacInput(String giacInput) {
        gen g = GiacUtils.getGenFromGiacInput(giacContext, giacInput);
        return g;

    }

    public Node getMathmlNodeFromGen(gen g) throws Exception {
        String mathmlString = GiacUtils.getMathmlStringFromGen(giacContext, g);
        return GiacUtils.getMathmlDocumentFromMathmlString(documentBuilder, mathmlString);
    }

    public String getBrailleFromGen(gen g) {
        String mathmlString = GiacUtils.getMathmlStringFromGen(giacContext, g);
        return GiacUtils.getBrailleFromMathmlString(giacConverter, mathmlString);

    }

    public String getMathmlStringFromGen(gen g) {
        String mathmlString = GiacUtils.getMathmlStringFromGen(giacContext, g);
        return mathmlString;
    }

    public String getGiacInputFromGen(gen g) {
        String gen = GiacUtils.getGiacInputFromGen(giacContext, g);
        return gen;
    }

    public gen applyGenFunc(GiacFuncs.GiacFunc func, gen g) throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        gen newGen = giacFuncs.applyGenFunc(func, g, giacContext);
        return newGen;
    }

    ///// options
    
    public void setBrailleTable(BrailleTable brailleTable) throws IOException, NatFilterException {
        giacConverter.setOption(NatFilterOption.FORMAT_OUTPUT_BRAILLE_TABLE, brailleTable.getName());
    }

    public void setMathSpecificNotation(boolean useSpecificNotation) throws IOException, NatFilterException {
        giacConverter.setOption(NatFilterOption.MODE_MATH_SPECIFIC_NOTATION, Boolean.toString(useSpecificNotation));
    }
    
    public BrailleTable getBrailleTable() {
        BrailleTable brailleTable = null;
        try {
            String brailleTableName = giacConverter.getOption(NatFilterOption.FORMAT_OUTPUT_BRAILLE_TABLE);
            brailleTable = BrailleTables.forName(brailleTableName);
        } catch (NatFilterException | BrailleTableException ex) {
            Logger.getLogger(GiacBraille.class.getName()).log(Level.SEVERE, null, ex);
        }
        return brailleTable;
    }

    public Boolean getMathSpecificNotation() {
        Boolean o=null;
        try {
            o = Boolean.getBoolean(giacConverter.getOption(NatFilterOption.MODE_MATH_SPECIFIC_NOTATION));
        } catch (NatFilterException ex) {
            Logger.getLogger(GiacBraille.class.getName()).log(Level.SEVERE, null, ex);
        }
        return o;
    }
}
