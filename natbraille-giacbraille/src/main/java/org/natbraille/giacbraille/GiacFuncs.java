/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.natbraille.giacbraille;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javagiac.context;
import javagiac.gen;
import javagiac.giac;
import org.apache.tika.io.IOUtils;

/*
 * Get giac functions for text file.
 * cat javagiac/giac.java | grep "public static" | sed 's/public static//'| grep -v void | sed 's/{//' | sort > giac_func.txt
 */
public class GiacFuncs {

    private final List<GiacFunc> funcs = new ArrayList<>();

    public class GiacFunc {

        public class GiacFuncParam {

            public final String name;
            public final String type;

            public GiacFuncParam(String name, String type) {
                this.name = name;
                this.type = type;
            }
        }

        public final String raw;
        public final String name;
        public final String returnType;
        public final List<GiacFuncParam> params = new ArrayList<>();

        public GiacFunc(String raw) {
            this.raw = raw;
            Pattern funcPattern = Pattern.compile("(?<returnType>\\w+)\\s(?<name>\\w+)\\((?<params>.*)\\)");
            Matcher funcMatcher = funcPattern.matcher(raw);
            funcMatcher.find();
            this.name = funcMatcher.group("name");
            this.returnType = funcMatcher.group("returnType");
            String paramsstr = funcMatcher.group("params");
            Pattern paramPattern = Pattern.compile("(?<type>\\w+)\\s+(?<name>\\w+)\\s*");
            Matcher paramMatcher = paramPattern.matcher(paramsstr);
            while (paramMatcher.find()) {
                GiacFuncParam param = new GiacFuncParam(paramMatcher.group("name"), paramMatcher.group("type"));
                params.add(param);
            }

        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append(returnType);
            sb.append(" ");
            sb.append(name);
            sb.append("(");
            for (int i = 0; i < params.size(); i++) {
                GiacFuncParam param = params.get(i);
                sb.append(param.type);
                sb.append(" ");
                sb.append(param.name);
                if ((i + 1) != params.size()) {
                    sb.append(",");
                }
            }
            sb.append(")");
            return sb.toString();
        }

    }

    public GiacFuncs() {
        try {
            InputStream is = getClass().getResourceAsStream("giac_func.txt");
            String toString = IOUtils.toString(is);
            for (String line : toString.split("\n")) {
                GiacFunc gf = new GiacFunc(line);
                funcs.add(gf);
            }
        } catch (IOException ex) {
            Logger.getLogger(GiacFunc.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<GiacFunc> getFuncs() {
        return funcs;
    }

    /**
     * get GiacFuncs returning a gen, operating on a giac.gen and a
     * giac.context.
     *
     * @return
     */
    public List<GiacFunc> getDirectGenFuncs() {
        List<GiacFunc> filtered = new ArrayList<>();
        for (GiacFunc func : getFuncs()) {
            boolean add = false;
            //if (!func.name.startsWith("_")) {
            if ("gen".equals(func.returnType)) {
                if (func.params.size() == 1) {
                    if ("gen".equals(func.params.get(0).type)) {
                        filtered.add(func);
                        System.err.println("*1*" + func.toString());
                    }
                }
                if (func.params.size() == 2) {
                    if ("gen".equals(func.params.get(0).type)) {
                        if ("context".equals(func.params.get(1).type)) {
                            add = true;
                        }
                    }
                }

            }
            //}
            if (add) {
                filtered.add(func);
            }
        }
        return filtered;
    }
    public GiacFunc byName(String funcName){       
       for (GiacFunc func : getDirectGenFuncs()){
           if (func.name.equals(funcName)){
               return func;
           }
       }
       return null;
    }
    public gen applyGenFunc(GiacFunc func, gen g, context c) throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        Method method = null;
        gen returnGen = null;
        Object o = null;
        if ((func.params.size() == 2)
                && "gen".equals(func.params.get(0).type)
                && "context".equals(func.params.get(1).type)) {
            method = giac.class.getMethod(func.name, gen.class, context.class);
            o = method.invoke(func.name, g, c);
        } else if ((func.params.size() == 1)
                && "gen".equals(func.params.get(0).type)) {
            method = giac.class.getMethod(func.name, gen.class);
            o = method.invoke(func.name, g);
        }

        if (o instanceof gen) {
            returnGen = (gen) o;
        }
        return returnGen;
    }
}
