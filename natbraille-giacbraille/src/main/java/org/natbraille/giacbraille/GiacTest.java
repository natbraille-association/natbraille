/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.natbraille.giacbraille;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import javagiac.context;
import javagiac.gen;
import javagiac.giac;
import org.natbraille.core.filter.transcriptor.GiacConverter;

/**
 *
 * @author vivien
 */
public class GiacTest {

    /*
     The gen might be:

     an immediate int: test by e.type==_INT_, get value by int i=e.val;
     a double: test by e.type==_DOUBLE_, get value by double d=e._DOUBLE_val;
     an arbitrary precision integer: test by e.type==_ZINT, get value by mpz_t * m=e._ZINTptr; (see GMP documentation for more details on the underlying type).
     an arbitrary precision float: test by e.type==_REAL),
     a complex number, test by e.type==_CINT), the pointer value points to two objects of type gen the real part gen realpart=*e._CINTptr; and the imaginary part gen imagpart=*(e._CINTptr+1);
     a vector object (in fact it is a list), test by e.type==_VECT), e._VECTptr is a pointer to the vecteur type (a shortcut for std::vector<gen>). For example e._VECTptr->size() will give the size of the vector object. See the vector section above for more details.
     a global name, test by e.type==_IDNT, with a pointer to an identificateur type identificateur i=*e._IDNTptr; . See giac/identificateur.h for more details.
     a symbolic object, test by e.type==_SYMB, with a pointer to a symbolic type symbolic s=*e._SYMBptr;. See the symbolic section below and giac/symbolic.h for more details.
     a function object, test by e.type==_FUNC, with a pointer to a unary_function_ptr type unary_function_ptr u=*e._FUNCptr. See the unary function section below and giac/unary.h for more details. 
     */
    public static void main(String argv[]) {

        GiacConverter giacConverter = GiacUtils.buildGiacConverter();

        context C = new context();
        String s = new String("");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter an expression :");

        for (;;) {

            try {
                s = br.readLine();
            } catch (IOException ioe) {
                System.out.println("Error reading expression");
                System.exit(1);
            }
            gen g = new gen(s, C);
            System.out.println("Created gen of type : " + g.getType());
            String mathml = "<math>" + giac.gen2mathml(g, C) + "</math>";

            System.out.println("id giac    : " + g.print(C));
            System.out.println("id mathml  : " + mathml);
            System.out.println("id braille : " + GiacUtils.getBrailleFromMathmlString(giacConverter, mathml));

	    // gen g=new gen(10,12);
            // gen g=new gen("x**4-1",C);
            // if (g.getType()==gen_unary_types._SYMB.swigValue()){
            //     System.out.println( "g operator is "+g.operator_at(0,C).print(C));
            //     System.out.println( "g has "+giac._size(g,C).getVal()+" arguments");
            //     System.out.println( "First argument of g is "+g.operator_at(1,C).print(C));
            //     System.out.println( ""+g.print(C));
            // }
            //    gen h=giac._factor(g,C);
            //System.out.println( "Factor: " + h.print(C) );
            //    h=giac.add(g,g);
            gen h = giac._simplify(g, C);
            String mathmlSimple = "<math>" + giac.gen2mathml(h, C) + "</math>";

            System.out.println("sim giac    : " + h.print(C));
            System.out.println("sim mathml  : " + mathmlSimple);
            System.out.println("sim braille : " + GiacUtils.getBrailleFromMathmlString(giacConverter, mathmlSimple));

            // h=new gen(giac.makevecteur(h,new gen(2)),(short)1);
            //    h=new gen(giac.makevecteur(h,new gen(2),h),(short)gen_comp_subtypes._SEQ__VECT.swigValue());
            //    System.out.println( "Value of h: " + h.print(C) );
        }
        //System.out.println( "Goodbye" );
    }
}
