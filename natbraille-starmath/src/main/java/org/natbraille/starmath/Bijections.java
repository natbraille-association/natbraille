/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * Author : Vivien Guillet <vivien.guillet@laposte.net>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.starmath;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

// TODO  not bijection.... and, & <-> ∧
// TODO not bijection %perthousand %pourmille
// TODO not bijection" lline rline
// TODO not bijection ‖ ldline rdline
// TODO not bijection %ialpha %alpha
// TODO not bijection %iALPHA %ALPHA
// TODO not bijection %infinite infinity infty
// TODO not bijection vec widewec etc.
/**
 *
 * @author vivien
 */
public class Bijections {

    // todo: remove some from identifier / operator beeing in other categories
    @Deprecated
    public static final List<String> identifierStartOffice = Arrays.asList(new String[]{
        "%ALPHA", "%BETA", "%GAMMA", "%DELTA", "%EPSILON", "%ZETA", "%ETA", "%THETA", "%IOTA", "%KAPPA", "%LAMBDA", "%MU", "%NU", "%XI", "%OMICRON", "%PI", "%RHO", "%SIGMA", "%TAU", "%UPSILON", "%PHI", "%CHI", "%PSI", "%OMEGA",
        "%alpha", "%beta", "%gamma", "%delta", "%varepsilon", "%zeta", "%eta", "%theta", "%iota", "%kappa", "%lambda", "%mu", "%nu", "%xi", "%omicron", "%pi", "%rho", "%varsigma", "%sigma", "%tau", "%upsilon", "%varphi", "%chi", "%psi", "%omega", "%vartheta", "%phi", "%varpi", "%varrho", "%epsilon",
        "%iALPHA", "%iBETA", "%iGAMMA", "%iDELTA", "%iEPSILON", "%iZETA", "%iETA", "%iTHETA", "%iIOTA", "%iKAPPA", "%iLAMBDA", "%iMU", "%iNU", "%iXI", "%iOMICRON", "%iPI", "%iRHO", "%iSIGMA", "%iTAU", "%iUPSILON", "%iPHI", "%iCHI", "%iPSI", "%iOMEGA",
        "%ialpha", "%ibeta", "%igamma", "%idelta", "%ivarepsilon", "%izeta", "%ieta", "%itheta", "%iiota", "%ikappa", "%ilambda", "%imu", "%inu", "%ixi", "%iomicron", "%ipi", "%irho", "%ivarsigma", "%isigma", "%itau", "%iupsilon", "%ivarphi", "%ichi", "%ipsi", "%iomega", "%ivartheta", "%iphi", "%ivarpi", "%ivarrho", "%iepsilon",
        "emptyset", "aleph", "setN", "setZ", "setQ", "setR", "setC",
        "%perthousand",
        "%tendto",
        "%element",
        "%noelement",
        "%infinite", "%infinity", "%infty",
        "%infini", // french
        "%angle",
        "%and",
        "%or",
        "%notequal",
        "%identical",
        "%strictlylessthan",
        "%strictlygreaterthan",
        "re", "im",
        "wp", "backepsilon", "hbar", "lambdabar"

    });
    @Deprecated
    public static final List<String> identifierUnicode = Arrays.asList(new String[]{
        "Α", "Β", "Γ", "Δ", "Ε", "Ζ", "Η", "Θ", "Ι", "Κ", "Λ", "Μ", "Ν", "Ξ", "Ο", "Π", "Ρ", "Σ", "Τ", "Υ", "Φ", "Χ", "Ψ", "Ω",
        "α", "β", "γ", "δ", "ε", "ζ", "η", "θ", "ι", "κ", "λ", "μ", "ν", "ξ", "ο", "π", "ρ", "ς", "σ", "τ", "υ", "φ", "χ", "ψ", "ω", "ϑ", "ϕ", "ϖ", "ϱ", "ϵ",
        "Α", "Β", "Γ", "Δ", "Ε", "Ζ", "Η", "Θ", "Ι", "Κ", "Λ", "Μ", "Ν", "Ξ", "Ο", "Π", "Ρ", "Σ", "Τ", "Υ", "Φ", "Χ", "Ψ", "Ω",
        "α", "β", "γ", "δ", "ε", "ζ", "η", "θ", "ι", "κ", "λ", "μ", "ν", "ξ", "ο", "π", "ρ", "ς", "σ", "τ", "υ", "φ", "χ", "ψ", "ω", "ϑ", "ϕ", "ϖ", "ϱ", "ϵ",
        "∅", "ℵ", "ℕ", "ℤ", "ℚ", "ℝ", "ℂ",
        "‰", "→", "∈", "∉", "∞", "∞", "∞",
        "∞", // french
        "∢", "∧", "∨", "≠", "≡", "≪", "≫",
        "ℜ", "ℑ",
        "℘", "∍", "ℏ", "ƛ"
    //  "–"
    });

    public static final List<String> standaloneIdentifierStarOffice = Arrays.asList(new String[]{
        //
        "nospace",
        "setC", "setN", "setQ", "setR", "setZ",
        "wp",
        "Im", "Re", "aleph",
        "hbar", "infinity", "infty", "lambdabar",
        "backepsilon",
        "emptyset",});

    public static final List<String> standaloneIdentifierUnicode = Arrays.asList(new String[]{
        //
        "nospace",
        "ℂ", "ℕ", "ℚ", "ℝ", "ℤ",
        "℘",
        "ℑ", "ℜ", "ℵ",
        "ℏ", "∞", "∞", "ƛ",
        "∍",
        "∅",});

    //
    public static final List<String> standaloneSymbolStarOffice = Arrays.asList(new String[]{
        "circ", "dlarrow", "dlrarrow",
        "dotsaxis", "dotsdiag", "dotsup", "dotsdown", "dotslow", "dotsvert",
        "downarrow", "drarrow", "exists", "notexists",
        "forall",
        "leftarrow", "<-", "nabla",
        "partial", "rightarrow", "->",
        "uparrow",});

    public static final List<String> standaloneSymbolUnicode = Arrays.asList(new String[]{
        "∘", "⇐", "⇔",
        "⋯", "⋰", "⋰", "⋱", "…", "⋮",
        "↓", "⇒", "∃", "∄",
        "∀",
        "←", "←", "∇",
        "∂", "→", "→",
        "↑",});

    public static String get(List<String> from, List<String> to, String text) {

        int but = from.indexOf(text);
        if (but == -1) {
            return text;
        } else {
            return to.get(but);
        }
    }

    public static String getMaybe(List<String> from, List<String> to, String text) {

        int but = from.indexOf(text);
        if (but == -1) {
            return null;
        } else {
            return to.get(but);
        }
    }

    public static boolean exists(List<String> list, String text) {
        return (list.indexOf(text) != -1);
    }
    @Deprecated
    public static final List<String> operatorsStarOffice = Arrays.asList(new String[]{
        "times", "widebslash", "+-", "-+", "cdot", "*", "otimes", "div", "/", "circ",
        "neg", "and", /*"&",*/ "or", "bslash",
        "AAA",
        "=", "<>", "<", "<=", "leslant", ">", ">=", "geslant", "<<", ">>",
        "approx", "sim", "simeq", "equiv", "prop", "parallel", "ortho", "divides", "ndivides", "toward",
        "BBB",
        "dlarrow", "dlrarrow", "drarrow",
        "prec", "succ", "preccurlyeq", "succcurlyeq", "precsim", "succsim", "nprec", "nsucc",
        "CCC",
        "in", "notin", "owns",
        "intersection", "union", "setminus", "slash", "subset", "subseteq", "supset", "supseteq", "nsubset", "nsubseteq", "nsupset", "nsupseteq",
        "DDD",
        "sum", "prod", "coprod", "int", "iint", "iiint", "lint", "llint", "lllint",
        "liminf", "limsup",
        "EEE",
        "partial",
        "FFF",
        "(", "[", "langle", "lbrace", "lceil", "ldbracket", "ldline", "lfloor", "lline",
        ")", "]", "rangle", "rbrace", "rceil", "rdbracket", "rdline", "rfloor", "rline",
        "GGG",
        "exists", "notexists",
        "transl", "transr",
        "nabla",
        "HHHH",
        "dotsaxis", "dotsdown", "dotslow", "dotsup", "dotsvert",
        "IIII",
        "downarrow", "leftarrow", "uparrow", "rightarrow",
        "def",
        "odivide", "odot", "ominus", "oplus",
        "JJJJ",
        "mline", "gt", "forall"

    });

    @Deprecated
    public static final List<String> operatorsUnicode = Arrays.asList(new String[]{
        "×", "∖", "±", "∓", "⋅", "*", "⊗", "÷", "/", "∘",
        "¬", "∧", /*"∧",*/ "∨", "∖",
        "aaa",
        "=", "≠", "<", "≤", "⩽", ">", "≥", "⩾", "≪", "≫",
        "≈", "∼", "≃", "≡", "∝", "∥", "⊥", "∣", "∤", "→",
        "bbb",
        "⇐", "⇔", "⇒",
        "≺", "≻", "≼", "≽", "≾", "≿", "⊀", "⊁",
        "ccc",
        "∈", "∉", "∋",
        "∩", "∪", "∖", "/", "⊂", "⊆", "⊃", "⊇", "⊄", "⊈", "⊅", "⊉",
        "ddd",
        "∑", "∏", "∐", "∫", "∬", "∭", "∮", "∯", "∰",
        "lim inf", "lim sup",
        "eee",
        "∂",
        "fff",
        "(", "[", "⟨", "{", "⌈", "⟦", "‖", "⌊", "|",
        ")", "]", "⟩", "}", "⌉", "⟧", "‖", "⌋", "|",
        "ggg",
        "∃", "∄",
        "⊷", "⊶",
        "∇",
        "hhhh",
        "⋯", "⋱", "…", "⋰", "⋮",
        "iiii",
        "↓", "←", "↑", "→",
        "≝",
        "⊘", "⊙", "⊖", "⊕",
        "jjjj",
        "|", ">", "∀"
    });

    public static final List<String> overAttributesStarOffice = Arrays.asList(new String[]{
        "acute", "grave", "breve", "circle",
        "dot", "ddot", "dddot",
        "bar",
        "vec", "tilde", "hat",
        "check",
        "widevec", "widetilde", "widehat",
        "overline"
    });

    // vec stretchy = true    
    public static final List<String> overAttributesUnicode = Arrays.asList(new String[]{
        "´", "`", "˘", "˚",
        "˙", "¨", "\u20DB",
        "¯",
        "\u20D7", "~", "^",
        "ˇ",
        "\u20D7", "~", "^", // wide is same as not wide but stretched
        "¯"
    });
    public static final List<String> underAttributesStarOffice = Arrays.asList(new String[]{
        "underline"
    });
    public static final List<String> underAttributesUnicode = Arrays.asList(new String[]{
        "̲"
    });
    
    public static final List<String> bracesStarOffice = Arrays.asList(new String[]{
        "(","lparent",
        "[", "lbracket",
        "lbrace",
        "ldbracket",
        "lline",
        "ldline",
        "langle",
        "lfloor",
        "lceil",
        
        ")", "rparent",
        "]", "rbracket",
        "rbrace",
        "rdbracket",
        "rline",
        "rdline",
        "rangle",
        "rfloor",
        "rceil"
    });
    
    public static final List<String> bracesUnicode = Arrays.asList(new String[]{
        "(", "(", "[", "[", "{", "⟦", "|", "‖", "⟨", "⌊", "⌈",
        ")", ")", "]", "]", "}", "⟧", "|", "‖", "⟩", "⌋", "⌉"

    });
    public static final String matchingBrace(List<String> braceList, String brace){
        int idx = braceList.indexOf( brace );
        if (idx == -1 ){
            return null;
        } else {
            int l = braceList.size()/2;
            int p = ( l + idx ) % braceList.size();
            return braceList.get(p);
        }
    }
    
    public static final List<String> relopStarOffice = Arrays.asList(new String[]{
        "approx", "def", "divides", "equiv", ">=", "ge", "geslant",  ">>", "gg",  ">", "gt", "in", "<=", "le", 
        "leslant",  "<<", "ll",  "<", "lt", "ndivides",  "<>", "neq", "ni", "notin", "nsubset", "nsupset", "nsubseteq", 
        "nsupseteq", "ortho", "owns", "parallel", "prec", "preccurlyeq", "precsim", "nprec", "prop", "sim",
        "simeq", "subset", "succ", "succcurlyeq", "succsim", "nsucc", "subseteq", "supset", "supseteq", "toward", 
        "transl", "transr","="
    });
    public static final List<String> relopUnicode = Arrays.asList(new String[]{
        "≈", "≝", "∣", "≡", "≥", "⩾", "⩾", "≫", "≫", ">", ">", "∈", "≤", "≤", 
        "⩽", "≪", "≪", "<", "<", "∤", "≠", "≠", "∋", "∉", "⊄", "⊅", "⊈", 
        "⊉", "⊥", "∋", "∥", "≺", "≼", "≾", "⊀", "∝", "∼", 
        "≃", "⊂", "≻", "≽", "≿", "⊁", "⊆", "⊃", "⊇", "→", 
        "⊷", "⊶","="
    });
    /*public static final List<String> attributesStarOffice = Arrays.asList(new String[]{
     "overstrike",
      "widehat", "widetilde", "widevec,
     "overline",  "acute", "bar", "breve", "check", "circle", "dddot", "ddot", "dot", "grave", "hat", "tilde", "vec";
     "underline"
    ; 
    });*/
    private static String padR(String def, int padSize, String with) {
        for (int i = def.length(); i < padSize; i++) {
            def += with;
        }
        return def;
    }

    private static String formatinout(String grp, String s, String u) {
        return grp + " " + s + " " + u;
    }

    public static void main(String[] argv) throws IOException {

        identifierStartOffice.stream().forEachOrdered(so -> {
            String op = get(identifierStartOffice, identifierUnicode, so);
            System.out.println(formatinout("id", so, op));
        });
        operatorsStarOffice.stream().forEachOrdered(so -> {
            String op = get(operatorsStarOffice, operatorsUnicode, so);
            System.out.println(formatinout("op", so, op));
        });

        overAttributesStarOffice.stream().forEachOrdered(so -> {
            String op = get(overAttributesStarOffice, overAttributesUnicode, so);
            System.out.println(formatinout("oa", so, op));
        });
        bracesStarOffice.stream().forEachOrdered(so -> {
            String op = get(bracesStarOffice, bracesUnicode, so);
            System.out.println(formatinout("br", so, op));
        });
        standaloneIdentifierStarOffice.stream().forEachOrdered(so -> {
            String op = get(standaloneIdentifierStarOffice, standaloneIdentifierUnicode, so);
            System.out.println(formatinout("si", so, op));
        });
        relopStarOffice.stream().forEachOrdered(so -> {
            String op = get(relopStarOffice, relopUnicode, so);
            System.out.println(formatinout("ro", so, op));
        });
        bracesUnicode.stream().forEachOrdered(so -> {
            String c = matchingBrace(bracesUnicode,so);            
            System.out.println(formatinout("br", so, c));
        });
        bracesStarOffice.stream().forEachOrdered(so -> {
            String c = matchingBrace(bracesStarOffice,so);            
            System.out.println(formatinout("br", so, c));
        });
    }
}
