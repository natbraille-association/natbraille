/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * Author : Vivien Guillet <vivien.guillet@laposte.net>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.starmath.toStarMath;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

import org.natbraille.starmath.Bijections;
import org.natbraille.starmath.Constants;
import org.natbraille.starmath.DomUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

// todo <mo fence="true" stretchy="false">‖</mo>
// TODO REMOVE TERMINAL?
/*
TODO homonym ?
sum from a to b c
sum csub{a} csup{b} c
 */
 /*
TODO homonym ?
stack{a # b # c} =  matrix{a ## b ## c} ==> same mathml for both sides
 */
 /*
TODO arctan & common functions
 */
 /*
TODO isbraced in constructor
 */
 /*
*
<maction> 
f <math>
<menclose>
<merror>
<mfenced>
<mfrac>
<mglyph>
f <mi>
<mlabeledtr>
<mmultiscripts>
f <mn>
f <mo>
f <mover>
<mpadded>
<mphantom>
f <mroot>
f <mrow>
<ms>
<mspace>
f <msqrt>
<mstyle>
f <msub>
f <msubsup>
f <msup>
<mtable>
<mtd>
<mtext>
<mtr>
f <munder>
f <munderover>
<semantics>
 */
// TODO empty mi
/**
 *
 * @author vivien
 */
class MathMLToStarMathHandler extends DefaultHandler {

    interface StackElement {

        public Integer getStart();

        public Integer getEnd();
    }

    interface CanBeBraced {

        boolean isBraced();
    }

    // mathml element, as given by the sax 'endElement' callback
    private class DomStackElement implements StackElement {

        public final String qName;
        public final Attributes attributes;

        public DomStackElement(String qName, Attributes attributes) {
            this.qName = qName;
            this.attributes = attributes;
        }

        @Override
        public Integer getStart() {
            String start = this.attributes.getValue(Constants.START_ATTRIBUTE_NAME);
            Integer i;
            try {
                i = (start == null) ? null : Integer.parseInt(start);
            } catch (NumberFormatException e) {
                i = null;
            }
            return i;
        }

        @Override
        public Integer getEnd() {
            String end = this.attributes.getValue(Constants.END_ATTRIBUTE_NAME);
            Integer i;
            try {
                i = (end == null) ? null : Integer.parseInt(end);
            } catch (NumberFormatException e) {
                i = null;
            }
            return i;
        }
    }

    // mathml text as given by the sax 'character' callback
    // or starmath terminal element 
    private class TerminalStackElement implements StackElement {

        public final String text;
        public final Integer start;
        public final Integer end;

        public TerminalStackElement(String text, Integer start, Integer end) {
            this.text = text;
            this.start = start;
            this.end = end;
        }

        public TerminalStackElement(String text) {
            this.text = text;
            this.start = null;
            this.end = null;
        }

        @Override
        public Integer getStart() {
            return start;
        }

        @Override
        public Integer getEnd() {
            return end;
        }

    }

    private class BraceStackElement extends TerminalStackElement {

        public final boolean stretchy;

        public BraceStackElement(String text, Integer start, Integer end, boolean stretchy) {
            super(text, start, end);
            this.stretchy = stretchy;
        }
    };

    private class RelopStackElement extends TerminalStackElement {

        public RelopStackElement(String text, Integer start, Integer end) {
            super(text, start, end);
        }
    }

    // group of TerminalStackElement
    private class ListStackElement implements StackElement {

        public final List<TerminalStackElement> childs;
        public final Integer start;
        public final Integer end;
        public final boolean isBraced;

        public ListStackElement(List<TerminalStackElement> childs, Integer start, Integer end, boolean isBraced) {
            this.childs = childs;
            this.start = start;
            this.end = end;
            this.isBraced = isBraced;
        }

        @Override
        public Integer getStart() {
            return start;
        }

        @Override
        public Integer getEnd() {
            return end;
        }

        public boolean isBraced() {
            return this.isBraced;
        }
    }

    // TODO : should be either type 
    final ArrayList<StackElement> stack = new ArrayList();

    int stackFindOpeningElement(String qName) {
        for (int i = stack.size() - 1; i >= 0; i--) {
            StackElement candidate = stack.get(i);

            if ((candidate != null) && (candidate instanceof DomStackElement)) {
                DomStackElement domCandidate = (DomStackElement) candidate;
                if (domCandidate.qName.equals(qName)) {
                    return i;
                }
            }
        }
        return -1;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        stack.add(new DomStackElement(qName, attributes));
    }

    private static boolean isTerminal(StackElement se) {
        if ((se != null) && (se instanceof DomStackElement)) {
            DomStackElement domStackElement = (DomStackElement) se;
            return ("mo".equals(domStackElement.qName)
                    || "mi".equals(domStackElement.qName)
                    || "mn".equals(domStackElement.qName));
        }
        return false;
    }

    private static String stripSpaces(String s) {
        Pattern stripSpacesPattern = Pattern.compile("(^(\\s)*|(\\s)*$)", Pattern.DOTALL);
        return stripSpacesPattern.matcher(s).replaceAll("");
    }

    @Override
    public void characters(char ch[], int start, int length) throws SAXException {
        String g = stripSpaces(new String(ch, start, length));
        StackElement headElement = stack.get(stack.size() - 1);
        if ((g.length() != 0) || isTerminal(headElement)) {
            stack.add(new TerminalStackElement(g));
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {

        System.out.println("bef" + getResultDebug());

        // find stack segment from opening to closing
        int pos = stackFindOpeningElement(qName);
        if (pos == -1) {
            throw new SAXException("no match for" + qName);
        }
        // should throw if not correct
        DomStackElement openingElement = (DomStackElement) stack.remove(pos);

        //if (!match.isElement) {
        //            throw new SAXException("match for" + qName + "is not a dom element");
        //      }
        // pop corresponding stack elements        
        List<StackElement> tail = new ArrayList();
        int max = stack.size();
        for (int i = pos; i < max; i++) {
            StackElement removed = stack.remove(pos);
            tail.add(removed);
        }

        // concat stack start and end position of matching elements
        Integer start;

        if (openingElement.getStart() != null) {
            start = openingElement.getStart();
        } else {
            Optional<Integer> starts = tail.stream().map(x -> x.getStart()).filter(x -> x != null).min((a, b) -> a - b);
            start = starts.isPresent() ? starts.get() : null;
        }

        Integer end;

        if (openingElement.getEnd() != null) {
            end = openingElement.getEnd();
        } else {
            Optional<Integer> ends = tail.stream().map(x -> x.getEnd()).filter(x -> x != null).max((a, b) -> a - b);
            end = (ends.isPresent()) ? ends.get() : null;
        }

        // convert
        tail = convert(openingElement, start, end, tail);

        // concat 
        StackElement el = flattenStackElementList(tail, start, end);

        // push result
        stack.add(el);

        //System.out.println("aft" + getResultDebug());
    }

    // flatten to list of terminals
    ListStackElement flattenStackElementList(List<StackElement> lst, Integer start, Integer end) throws SAXException {

        // flatten childs
        List<TerminalStackElement> children = new ArrayList();
        for (int i = 0; i < lst.size(); i++) {
            StackElement el = lst.get(i);
            if (el instanceof DomStackElement) {
                DomStackElement del = (DomStackElement) el;
                throw new SAXException("cannot flatten dom element :" + del.qName);
            } else if (el instanceof TerminalStackElement) {
                TerminalStackElement tel = (TerminalStackElement) el;
                children.add(tel);
            } else if (el instanceof ListStackElement) {
                ListStackElement sel = (ListStackElement) el;
                children.addAll(sel.childs);
            }
        }
        boolean isBraced = false;
        if (lst.size() == 1) {
            if (lst.get(0) instanceof ListStackElement) {
                ListStackElement lse = (ListStackElement) lst.get(0);
                if (lse.isBraced()) {
                    isBraced = true;
                }
            }

        }
        ListStackElement el = new ListStackElement(children, start, end, isBraced);
        return el;
    }

    ListStackElement braceMaybe(StackElement element) {
        List<TerminalStackElement> list = new ArrayList();

        // do not brace already braced
        boolean isBraced = (element instanceof ListStackElement) && ((ListStackElement) element).isBraced();

        // do not brace terminals        
        boolean isTerminal = (element instanceof TerminalStackElement)
                || ((element instanceof ListStackElement) && ((ListStackElement) element).childs.size() == 1);

        // do not brace ending with relop;
        boolean endsWithRelop = false;
        {
            // mathml can mrow(mrow(mi(a),mo(=)),mrow(mi(b)) for a = b
            // but stamath does not allow { { a = } b }
            if (element instanceof ListStackElement) {
                ListStackElement lse = (ListStackElement) element;
                TerminalStackElement lastChild = lse.childs.get(lse.childs.size() - 1);
                if (lastChild instanceof RelopStackElement) {
                    endsWithRelop = true;
                }
            } else if (element instanceof RelopStackElement) {
                endsWithRelop = true;
            }
        }

        boolean mustBrace = (!isTerminal) && (!isBraced) && (!endsWithRelop);

        if (mustBrace) {
            list.add(new TerminalStackElement("{"));
        }
        if (element instanceof TerminalStackElement) {
            list.add((TerminalStackElement) element);
        } else if (element instanceof ListStackElement) {
            ListStackElement tse = (ListStackElement) element;
            for (TerminalStackElement e : tse.childs) {
                list.add(e);
            }
        } else if (element instanceof DomStackElement) {
            // TODO throw
        }
        if (mustBrace) {
            list.add(new TerminalStackElement("}"));
        }

        ListStackElement lse = new ListStackElement(list, element.getStart(), element.getEnd(), true);
        return lse;
    }

    public List<StackElement> convert(DomStackElement openingElement, Integer start, Integer end, List<StackElement> tail) throws SAXException {

        // get frst which is the opening dom element    
        //MixedStackElement fst = tail.remove(0);
        String openingElementqName = openingElement.qName;
        if (null == openingElementqName) {
            throw new SAXException("no element root qName");
        }
        switch (openingElementqName) {
            case "mi": {
                if (tail.size() != 1) {
                    throw new SAXException("no!");
                }
                TerminalStackElement one = (TerminalStackElement) tail.remove(0);
                String standaloneIdentifierMaybe = Bijections.getMaybe(Bijections.standaloneIdentifierUnicode, Bijections.standaloneIdentifierStarOffice, one.text);
                String starMathText;

                if (standaloneIdentifierMaybe != null) {
                    starMathText = standaloneIdentifierMaybe;
                } else {
                    starMathText = Bijections.get(
                            Bijections.identifierUnicode,
                            Bijections.identifierStartOffice, one.text);

                }

                tail.add(new TerminalStackElement(starMathText, start, end));
                break;
            }
            case "mo": {
                if (tail.size() != 1) {
                    throw new SAXException("no!");
                }

                TerminalStackElement one = (TerminalStackElement) tail.remove(0);

                String stretchy = openingElement.attributes.getValue("stretchy");
                String fence = openingElement.attributes.getValue("fence");

                String overAttributeMaybe = Bijections.getMaybe(Bijections.overAttributesUnicode, Bijections.overAttributesStarOffice, one.text);
                String aBraceMaybe = Bijections.getMaybe(Bijections.bracesUnicode, Bijections.bracesStarOffice, one.text);
                String aRelopMaybe = Bijections.getMaybe(Bijections.relopUnicode, Bijections.relopUnicode, one.text);
                if (aRelopMaybe != null) {
                    tail.add(new RelopStackElement(aRelopMaybe, start, end));
                } else if (overAttributeMaybe != null) {
                    String starMathText;
                    if ("vec".equals(overAttributeMaybe) && "true".equals(stretchy)) {
                        starMathText = "widevec";
                    } else {
                        starMathText = overAttributeMaybe;
                    }
                    tail.add(new TerminalStackElement(starMathText, start, end));
                } else if (aBraceMaybe != null) {
                    if ("true".equals(stretchy)) {
                        tail.add(new BraceStackElement(aBraceMaybe, start, end, true));
                    } else {
                        tail.add(new BraceStackElement(aBraceMaybe, start, end, false));
                    }
                } else {
                    String starMathText = starMathText = Bijections.get(
                            Bijections.operatorsUnicode,
                            Bijections.operatorsStarOffice, one.text);
                    tail.add(new TerminalStackElement(starMathText, start, end));
                }

                break;
            }
            case "mn": {
                if (tail.size() != 1) {
                    throw new SAXException("no!");
                }       // noop
                TerminalStackElement one = (TerminalStackElement) tail.remove(0);
                String starMathText = one.text;
                tail.add(new TerminalStackElement(starMathText, start, end));
                break;
            }
            case "mroot": {
                if (tail.size() != 2) {
                    throw new SAXException("no!");
                }
                StackElement one = tail.remove(0);
                StackElement two = tail.remove(0);
                tail.add(new TerminalStackElement("nroot"));
                tail.add(braceMaybe(two));
                tail.add(braceMaybe(one));
                break;
            }
            case "msqrt": {
                if (tail.size() != 1) {
                    throw new SAXException("no!");
                }
                StackElement one = tail.remove(0);
                // TODO : root ?
                tail.add(new TerminalStackElement("root"));
                tail.add(braceMaybe(one));
                break;
            }
            case "mrow": {
                ListStackElement fse = flattenStackElementList(tail, start, end);
                List<TerminalStackElement> fset = fse.childs;

                if (fse.childs.size() > 0) {

                    TerminalStackElement fst = fse.childs.get(0);
                    boolean fstIs = (fst instanceof BraceStackElement);
                    boolean fstIsStrechy = fstIs && ((BraceStackElement) fst).stretchy;

                    TerminalStackElement lst = fse.childs.get(fse.childs.size() - 1);
                    boolean lstIs = (lst instanceof BraceStackElement) && (fse.childs.size() > 1);
                    boolean lstIsStrechy = lstIs && ((BraceStackElement) lst).stretchy;

                    if (fstIs || lstIs) {
                        // prefixed left / right braces must match
                        // mathml does not need to write missing brace but starmath
                        // needs to explicit missing with left/right none
                        if (lstIsStrechy || fstIsStrechy) {
                            if (fstIs) {
                                fset.add(0, new TerminalStackElement("left"));
                            } else {
                                fset.add(0, new TerminalStackElement("left none"));
                            }
                            if (lstIs) {
                                fset.add(fset.size() - 1, new TerminalStackElement("right"));
                            } else {
                                fset.add(new TerminalStackElement("right none"));
                            }
                        } else {
                            if (fstIs && lstIs){
                                // opening closing paris (lline, rline) and (ldline, rdline)
                                // does not exist in mathml, using the same symbol 
                                // for opening and closing.
                                // this problem does not arise witch stretchy fences
                                // because left / right can hold unmatching fence pairs.
                                BraceStackElement o = (BraceStackElement)fst;
                                BraceStackElement c = (BraceStackElement)lst;
                                if ( ("lline".equals(o.text) && ("lline".equals(c.text) ) ) ){
                                    TerminalStackElement poped = fset.remove(fset.size()-1);
                                    fset.add(new TerminalStackElement("rline",c.start,c.end));
                                } else if ( ("ldline".equals(o.text) && ("ldline".equals(c.text) ) ) ){
                                    TerminalStackElement poped = fset.remove(fset.size()-1);
                                    fset.add(new TerminalStackElement("rdline",c.start,c.end));
                                } else {                                    
                                    String mb = Bijections.matchingBrace(Bijections.bracesStarOffice,o.text);
                                    if ( ! mb.equals( c.text ) ){
                                        // braces does not match, so they must be prefixed
                                        // by backslash for starmath to not try to pair them
                                        fset.remove(0);
                                        fset.add(0,new TerminalStackElement("\\"+o.text,o.start,o.end));
                                        fset.remove(fset.size()-1);
                                        fset.add(new TerminalStackElement("\\"+c.text,c.start,c.end));
                                    }
                                }
                            }
                        }
                    }
                }
                tail.clear();
                ListStackElement fse2 = flattenStackElementList((List<StackElement>) (List<?>) fset, start, end);
                tail.add(braceMaybe(fse2));
                break;
            }
            case "msup": {
                if (tail.size() != 2) {
                    throw new SAXException("no!");
                }
                StackElement one = tail.remove(0);
                StackElement two = tail.remove(0);

                tail.add(braceMaybe(one));
                tail.add(new TerminalStackElement("^"));
                tail.add(braceMaybe(two));
                break;
            }
            case "msub": {
                if (tail.size() != 2) {
                    throw new SAXException("no!");
                }
                StackElement one = tail.remove(0);
                StackElement two = tail.remove(0);

                tail.add(braceMaybe(two));
                tail.add(new TerminalStackElement("_"));
                tail.add(braceMaybe(one));
                break;
            }
            case "msubsup": {
                if (tail.size() != 3) {
                    throw new SAXException("no!");
                }
                StackElement base = tail.remove(0);
                StackElement subscript = tail.remove(0);
                StackElement superscript = tail.remove(0);

                tail.add(braceMaybe(base));
                tail.add(new TerminalStackElement("^"));
                tail.add(braceMaybe(superscript));
                tail.add(new TerminalStackElement("^"));
                tail.add(braceMaybe(subscript));
                break;
            }
            case "munderover": {
                if (tail.size() != 3) {
                    throw new SAXException("no!");
                }
                StackElement base = tail.remove(0);
                StackElement underscript = tail.remove(0);
                StackElement overscript = tail.remove(0);
                tail.add(base);
                tail.add(new TerminalStackElement("from"));
                tail.add(braceMaybe(underscript));
                tail.add(new TerminalStackElement("to"));
                tail.add(braceMaybe(overscript));
                break;
            }
            case "munder": {
                if (tail.size() != 2) {
                    throw new SAXException("no!");
                }
                StackElement base = tail.remove(0);
                StackElement underscript = tail.remove(0);
                tail.add(base);
                tail.add(new TerminalStackElement("from"));
                tail.add(braceMaybe(underscript));
                break;
            }
            case "mover": {
                if (tail.size() != 2) {
                    throw new SAXException("no!");
                }
                StackElement base = tail.remove(0);
                StackElement overscript = tail.remove(0);

                boolean isVector = false;

                if (overscript instanceof ListStackElement) {
                    ListStackElement x = (ListStackElement) overscript;
                    if (x.childs.size() == 1) {
                        TerminalStackElement tse = x.childs.get(0);
                        isVector = Bijections.exists(Bijections.overAttributesStarOffice, tse.text);
                    }
                }

                if (isVector) {
                    tail.add(overscript);
                    tail.add(braceMaybe(base));
                } else {
                    tail.add(base);
                    tail.add(new TerminalStackElement("to"));
                    tail.add(braceMaybe(overscript));
                }
                break;
            }
            case "mtable": {
                // TODO : stack !
                int nRows = tail.size();
                List<StackElement> newTail = new ArrayList();
                newTail.add(new TerminalStackElement("matrix"));
                newTail.add(new TerminalStackElement("{"));
                for (int i = 0; i < nRows; i++) {
                    if (i > 0) {
                        newTail.add(new TerminalStackElement(" ## "));
                    }
                    // TODO : get -> remove ???
                    newTail.add(tail.get(i));
                }
                newTail.add(new TerminalStackElement("}"));
                tail = newTail;
                break;
            }
            case "mtr": {
                int nCols = tail.size();
                List<StackElement> newTail = new ArrayList();
                for (int i = 0; i < nCols; i++) {
                    if (i > 0) {
                        newTail.add(new TerminalStackElement(" # "));
                    }
                    newTail.add(tail.get(i));
                }
                tail = newTail;
                break;
            }
            // void
            case "mtd":
                break;
            case "mfrac": {
                if (tail.size() != 2) {
                    throw new SAXException("no!");
                }
                StackElement upper = tail.remove(0);
                StackElement lower = tail.remove(0);
                List<StackElement> newTail = new ArrayList();

                //ListStackElement fse2 = flattenStackElementList((List<StackElement>) (List<?>) fset, start, end);
                newTail.add(braceMaybe(upper));
                newTail.add(new TerminalStackElement(" over "));
                newTail.add(braceMaybe(lower));

                ListStackElement fse = braceMaybe(flattenStackElementList(newTail, start, end));
                tail = (List<StackElement>) (List<?>) fse.childs;

                break;
            }
            default:
                break;
        }

        return tail;

    }

    /*
     *  Text Debug Export
     */
    String getStackElementDebug(StackElement e) {
        String s = "";

        if (e instanceof TerminalStackElement) {
            s += "<";
            if (e instanceof BraceStackElement) {
                s += "BE!";
            }
            if (e instanceof RelopStackElement) {
                s += "RO!";
            }
            TerminalStackElement tse = (TerminalStackElement) e;
            if (tse.start != null) {
                s += tse.start + ":";
            }
            if (tse.end != null) {
                s += tse.end + ":";
            }
            if (tse.text != null) {
                s += "\"" + tse.text + "\"";
            }
            s += ">";
        } else if (e instanceof ListStackElement) {
            ListStackElement sel = (ListStackElement) e;
            s += "(";
            for (TerminalStackElement c : sel.childs) {
                s += getStackElementDebug(c);
            }
            s += ")";
        } else if (e instanceof DomStackElement) {
            DomStackElement dse = (DomStackElement) e;
            s += "<";
            s += "DOM!" + dse.qName;
            s += ">";
        }

        return s;
    }

    String getResultDebug() {
        String s = "";
        for (StackElement e : stack) {
            s += " | ";
            s += getStackElementDebug(e);
        }
        return s;
    }

    Element getStackElementAsDomElement(StackElement e, Document doc, boolean notFirst) {
        Element newElement = doc.createElement("span");

        if (e instanceof TerminalStackElement) {
            TerminalStackElement tse = (TerminalStackElement) e;
            if (tse.start != null) {
                newElement.setAttribute(Constants.START_ATTRIBUTE_NAME, tse.start.toString());
            }
            if (tse.end != null) {
                newElement.setAttribute(Constants.END_ATTRIBUTE_NAME, tse.end.toString());
            }
            if (tse.text != null) {
                newElement.setTextContent(tse.text + " ");
            }
        } else if (e instanceof ListStackElement) {
            ListStackElement sel = (ListStackElement) e;
            for (TerminalStackElement c : sel.childs) {
                newElement.appendChild(getStackElementAsDomElement(c, doc, notFirst));
            }
        } else if (e instanceof DomStackElement) {
            DomStackElement dse = (DomStackElement) e;
            Element domelmeent = doc.createElement("dom");
            domelmeent.setTextContent(dse.qName);
            newElement.appendChild(domelmeent);
        }

        return newElement;
    }

    /*
     *  Dom export
     */
    Element getStackAsDomElement(Document doc) {
        Element container = doc.createElement("starmath");

        for (StackElement e : stack) {
            //Element newElement = doc.createElement("div");
            //newElement.setAttribute("mainstack", "true");
            //container.appendChild(newElement);
            container.appendChild(getStackElementAsDomElement(e, doc, false));
        }
        return container;
    }

    Element getResultAsDomElement() {
        Document domDoc = DomUtils.newDoc();
        Element p = getStackAsDomElement(domDoc);
        domDoc.appendChild(p);

        // container.setTextContent(getResultDebug());
        return p;

    }

    public static void main(String[] argv) {
//        int count = 1000;
//        long t = 0;
//        while (count > 0) {
//            long start = System.nanoTime();
        StarMathExport.main(argv);
//            // do stuff
//            long end = System.nanoTime();
//            long microseconds = (end - start) / 1000;
//            t += microseconds;
//            count--;
//        }
//        System.out.println("ms" + t / 100000);

    }

}
