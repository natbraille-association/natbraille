/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * Author : Vivien Guillet <vivien.guillet@laposte.net>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.starmath.toStarMath;

import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.dom.DOMSource;

import org.natbraille.starmath.DomUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author vivien
 */
public class StarMathExport {

    private static int saxParse() {
        SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
        try {
            // String path = "/home/vivien/src/braille/mathmltestsuite/testsuite/testsuite/Presentation/ScriptsAndLimits/mover/mover1.mml";            
            //        String path = "./test/test3.mml";
            // String path = "./test/test2.mml";
            // String path = "./test/test4.mml";
            // String path = "./test/test5.mml";
            // String path = "./test/test6.mml";
            String path = "./test/test10.mml";
            //            String path = "./test/test7.mml";
            String fileContent = new String(Files.readAllBytes(Paths.get(path)));

            SAXParser saxParser = saxParserFactory.newSAXParser();
            MathMLToStarMathHandler handler = new MathMLToStarMathHandler();

            saxParser.parse(new InputSource(new StringReader(fileContent)), handler);
            System.err.println("resu:" + handler.getResultDebug());

        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
        return 42;
    }

    private static int saxParse2() {
        SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
        try {
            // String path = "/home/vivien/src/braille/mathmltestsuite/testsuite/testsuite/Presentation/ScriptsAndLimits/mover/mover1.mml";            
            //        String path = "./test/test3.mml";
            // String path = "./test/test2.mml";
            // String path = "./test/test4.mml";
            // String path = "./test/test5.mml";
            // String path = "./test/test6.mml";
            // String path = "./test/test10.mml";
            // String path = "./test/test11.mml";
            // String path = "./test/test12.mml";
    //            String path = "./test/test13.mml";
                //String path = "./test/test14.mml";
                // String path = "./test/test15.mml";
                //String path = "./test/test16.mml";
                //String path = "./test/test17.mml";
                String path = "./test/unmatchingbracket.mml";
            //            String path = "./test/test7.mml";
            String fileContent = new String(Files.readAllBytes(Paths.get(path)));

            SAXParser saxParser = saxParserFactory.newSAXParser();
            MathMLToStarMathHandler handler = new MathMLToStarMathHandler();

            saxParser.parse(new InputSource(new StringReader(fileContent)), handler);

            System.err.println("resu:" + handler.getResultDebug());
            Element element = handler.getResultAsDomElement();

            System.err.println("resu:" + DomUtils.outerXml(element));

            System.err.println("resuu:" + element.getTextContent());

        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
        return 42;
    }

    public static String mathmlToStarMathAsString(Document doc) throws SAXException, IOException, IOException, ParserConfigurationException {

        SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
        SAXParser saxParser = saxParserFactory.newSAXParser();
        MathMLToStarMathHandler handler = new MathMLToStarMathHandler();

        DOMSource is = new DOMSource(doc.getFirstChild());

        saxParser.parse(DomUtils.sourceToInputSource(is), handler);
        // System.err.println("resu:" + handler.getResultDebug());
        return handler.getResultDebug();
    }

    public static Element mathmlToStarMathAsElement(Element mathml) throws SAXException, IOException, IOException, ParserConfigurationException {

        SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
        SAXParser saxParser = saxParserFactory.newSAXParser();
        MathMLToStarMathHandler handler = new MathMLToStarMathHandler();

        DOMSource is = new DOMSource(mathml);

        saxParser.parse(DomUtils.sourceToInputSource(is), handler);
        // System.err.println("resu:" + handler.getResultDebug());
        Element element = handler.getResultAsDomElement();

        return element;
    }

    public static Element mathmlToStarMathAsElement(Document doc) throws SAXException, IOException, IOException, ParserConfigurationException {

        SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
        SAXParser saxParser = saxParserFactory.newSAXParser();
        MathMLToStarMathHandler handler = new MathMLToStarMathHandler();

        DOMSource is = new DOMSource(doc.getFirstChild());

        saxParser.parse(DomUtils.sourceToInputSource(is), handler);
        // System.err.println("resu:" + handler.getResultDebug());
        Element element = handler.getResultAsDomElement();

        return element;
    }

    public static void main(String[] argv) {
        saxParse2();
    }
}
