/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * Author : Vivien Guillet <vivien.guillet@laposte.net>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.starmath.toMathML;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.ParseTreeListener;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.natbraille.starmath.StarMath5Parser;

/**
 *
 * @author vivien
 */
public class ParseTreePrinter implements ParseTreeListener {

    public int offset = 0;

    public String pad() {
        String a = "";
        for (int i = 1; i < offset; i++) {
            a += "| ";
        }
        return a;
    }

    public void padPrint(String... args) {
        System.out.println(pad() + String.join(" ", args));
    }
    public void padPrintErr(String... args) {
        System.err.println(pad() + String.join(" ", args));
    }
    private String quote(String s){
        return "'" + s + "'";
    }
    @Override
    public void visitTerminal(TerminalNode arg0) {
        padPrint(quote(arg0.getText()));
    }

    @Override
    public void visitErrorNode(ErrorNode arg0) {
        padPrintErr("<!> " + arg0.getText());
    }

    @Override
    public void enterEveryRule(ParserRuleContext arg0) {
        offset += 1;
        String ruleName = StarMath5Parser.ruleNames[arg0.getRuleIndex()];
        String text = arg0.getText();
        padPrint(ruleName, text);
    }

    @Override
    public void exitEveryRule(ParserRuleContext arg0) {
        String ruleName = StarMath5Parser.ruleNames[arg0.getRuleIndex()];
        String text = arg0.getText();
        padPrint("/"+ruleName, quote(text));
        offset -= 1;
    }

}
