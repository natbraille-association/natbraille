/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * Author : Vivien Guillet <vivien.guillet@laposte.net>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.starmath.toMathML;

import java.io.IOException;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.ParseTreeListener;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.natbraille.starmath.Bijections;
import org.natbraille.starmath.DomUtils;
import org.natbraille.starmath.StarMath5Parser;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

// TODO
//
// done:matrix{ tl # tr ## bl # br } 
//
// done:spaces
// done:normal chars mi
// "left (" vs  "(" vs "\("
//
// redo unicodeIdentifier ?
// todo "string litteral"  
// todo Sum SuM sUm
/**
 *
 * @author vivien
 */
public class MathMLFromParseTreeOld implements ParseTreeListener {

    public Document doc;
    private Node pointer;
    private final boolean writePositions;

    MathMLFromParseTreeOld(boolean writePositions) {
        this.doc = DomUtils.newDoc();
        this.pointer = doc;
        this.writePositions = writePositions;
    }

    void setStartStopFromContext(Element e, ParserRuleContext ctx) {
        if (writePositions) {
            e.setAttribute("sso", String.valueOf(ctx.getStart().getStartIndex()));
            Token stop = ctx.getStop();
            if (stop != null) {
                e.setAttribute("sse", String.valueOf(stop.getStopIndex()));
            }
        }
    }

    @Override
    public void enterEveryRule(ParserRuleContext ctx) {
        String ruleName = StarMath5Parser.ruleNames[ctx.getRuleIndex()];
        // System.out.println("rule:" + ruleName);
        if (("multilineequation").equals(ruleName)) {
            Element el = doc.createElement("mrow");
            el.setAttribute("orig", "multilineequation");
            setStartStopFromContext(el, ctx);
            pointer.appendChild(el);
            pointer = el;

        } else if (("equations").equals(ruleName)) {
            Element el = doc.createElement("mrow");
            el.setAttribute("orig", "equations");
            setStartStopFromContext(el, ctx);
            pointer.appendChild(el);
            pointer = el;
        } else if (("equation").equals(ruleName)) {
            Element el = doc.createElement("mrow");
            el.setAttribute("orig", "equation");
            setStartStopFromContext(el, ctx);
            pointer.appendChild(el);
            pointer = el;
        } else if (("expressions").equals(ruleName)) {
            Element el = doc.createElement("math");
            el.setAttribute("xmlns", "http://www.w3.org/1998/Math/MathML");
            el.setAttribute("display", "block");
            Element el2 = doc.createElement("semantics");
            el.appendChild(el2);
            Element el3 = doc.createElement("mrow");
            el2.appendChild(el3);
            pointer.appendChild(el);
            pointer = el3;
        } else if (("starmath").equals(ruleName)) {
            /*            Element el = doc.createElement("math");
            el.setAttribute("xmlns", "http://www.w3.org/1998/Math/MathML");
            el.setAttribute("display", "block");
            Element el2 = doc.createElement("semantics");
            el.appendChild(el2);
            Element el3 = doc.createElement("mrow");
            el2.appendChild(el3);
            pointer.appendChild(el);
            pointer = el3;*/
        } else if ("atom".equals(ruleName)) {
        } else if ("prefixExpression".equals(ruleName)) {
        } else if ("number".equals(ruleName)) {
        } else if ("operator".equals(ruleName)) {
        } else if ("greek".equals(ruleName)) {
        } else if ("identifier".equals(ruleName)) {
        } else if (("expression").equals(ruleName) || ("group").equals(ruleName)) {
            Element el = doc.createElement("mrow");
            setStartStopFromContext(el, ctx);
            pointer.appendChild(el);
            pointer = el;
        } else if ("fraction".equals(ruleName)) {
            Element el = doc.createElement("mfrac");
            setStartStopFromContext(el, ctx);
            pointer.appendChild(el);
            pointer = el;
        } else if ("nroot".equals(ruleName)) {
            Element el = doc.createElement("mroot");
            setStartStopFromContext(el, ctx);
            pointer.appendChild(el);
            pointer = el;
        } else if ("sqrt".equals(ruleName)) {
            Element el = doc.createElement("msqrt");
            setStartStopFromContext(el, ctx);
            pointer.appendChild(el);
            pointer = el;
        } else if ("sup".equals(ruleName)) {
            Element el = doc.createElement("msup");
            setStartStopFromContext(el, ctx);
            pointer.appendChild(el);
            pointer = el;
        } else if ("fromto".equals(ruleName)) {

            Element el = doc.createElement("mrow");
            setStartStopFromContext(el, ctx);

            Element el2 = doc.createElement("munderover");
            el.appendChild(el2);
            pointer.appendChild(el);
            pointer = el2;
        } else if ("from".equals(ruleName)) {
            Element el = doc.createElement("mrow");
            setStartStopFromContext(el, ctx);

            Element el2 = doc.createElement("munder");
            el.appendChild(el2);
            pointer.appendChild(el);
            pointer = el2;
        } else if ("to".equals(ruleName)) {
            Element el = doc.createElement("mrow");
            setStartStopFromContext(el, ctx);

            Element el2 = doc.createElement("mover");
            el.appendChild(el2);
            pointer.appendChild(el);
            pointer = el2;
        } else if ("matrix".equals(ruleName)) {
            Element el = doc.createElement("mtable");
            setStartStopFromContext(el, ctx);
            pointer.appendChild(el);
            pointer = el;

        } else if ("matrixline".equals(ruleName)) {
            Element el = doc.createElement("mtr");
            setStartStopFromContext(el, ctx);
            pointer.appendChild(el);
            pointer = el;

        } else if ("matrixcolumn".equals(ruleName)) {
            Element el = doc.createElement("mtd");
            setStartStopFromContext(el, ctx);
            pointer.appendChild(el);
            pointer = el;
        } else if ("stack".equals(ruleName)) {
            Element el = doc.createElement("mtable");
            setStartStopFromContext(el, ctx);
            pointer.appendChild(el);
            pointer = el;
        } else if ("stackline".equals(ruleName)) {
            Element el = doc.createElement("mtr");
            setStartStopFromContext(el, ctx);
            pointer.appendChild(el);

            Element el2 = doc.createElement("mtd");
            setStartStopFromContext(el2, ctx);
            el.appendChild(el2);

            pointer = el2;
        } else if ("fenceExpression".equals(ruleName)) {
            Element el = doc.createElement("mrow");
            setStartStopFromContext(el, ctx);
            pointer.appendChild(el);
            pointer = el;
        } else if ("stretchedfenceExpression".equals(ruleName)) {
            Element el = doc.createElement("mrow");
            setStartStopFromContext(el, ctx);
            pointer.appendChild(el);
            pointer = el;
        } else if ("fencetype".equals(ruleName)) {
        } else if ("overbrace".equals(ruleName)) {
            Element el = doc.createElement("mover");
            setStartStopFromContext(el, ctx);
            pointer.appendChild(el);
            pointer = el;
        } else if ("underbrace".equals(ruleName)) {
            Element el = doc.createElement("underbrace");
            setStartStopFromContext(el, ctx);
            pointer.appendChild(el);
            pointer = el;
        } else if ("unicodeIdentifier".equals(ruleName)) {
            Element el = doc.createElement("mi");
            pointer.appendChild(el);
            pointer = el;
        } else if ("fourCharHexaChar".equals(ruleName)) {
        } else if ("sub".equals(ruleName)) {
            Element el = doc.createElement("msub");
            pointer.appendChild(el);
            pointer = el;

        } else if ("sup".equals(ruleName)) {
            Element el = doc.createElement("msup");
            pointer.appendChild(el);
            pointer = el;

        } else if ("subsup".equals(ruleName)) {
            Element el = doc.createElement("msubsup");
            pointer.appendChild(el);
            pointer = el;

        } else if ("quotedString".equals(ruleName)) {
            Element el = doc.createElement("mtext");
            pointer.appendChild(el);
            pointer = el;

        } else if ("quotedStringContent".equals(ruleName)) {
        } else if ("function".equals(ruleName)) {
            Element el = doc.createElement("mrow");
            pointer.appendChild(el);
            pointer = el;

        } else if ("knownFunction".equals(ruleName)) {
            Element el = doc.createElement("mi");
            pointer.appendChild(el);
            pointer = el;

//        } else if ("styleAttribute".equals(ruleName)) {
//            Element el = doc.createElement("mstyle");
//            pointer.appendChild(el);
//            pointer = el;
//
//            /* else if ("fractype".equals(ruleName)) {  } */
        } else {
            System.out.println("!! enter " + ruleName + " not defined");
        }
    }

    @Override
    public void exitEveryRule(ParserRuleContext ctx) {
        String ruleName = StarMath5Parser.ruleNames[ctx.getRuleIndex()];
        if (("expressions").equals(ruleName)) {
            pointer = pointer.getParentNode();
        } else if (("multilineequation").equals(ruleName)) {
            pointer = pointer.getParentNode();
        } else if (("equations").equals(ruleName)) {
            pointer = pointer.getParentNode();
        } else if (("equation").equals(ruleName)) {
            pointer = pointer.getParentNode();
        } else if ("atom".equals(ruleName)) {
        } else if ("prefixExpression".equals(ruleName)) {
        } else if ("number".equals(ruleName)) {
            String number = ctx.getText();
            Element el = doc.createElement("mn");
            el.setTextContent(number);
            setStartStopFromContext(el, ctx);
            pointer.appendChild(el);
        } else if ("operator".equals(ruleName)) {
            String text = ctx.getText();
            Element el = doc.createElement("mo");
            String op = Bijections.get(Bijections.operatorsStarOffice, Bijections.operatorsUnicode, text);
            el.setTextContent(op);
            setStartStopFromContext(el, ctx);
            pointer.appendChild(el);
        } else if ("greek".equals(ruleName)) {
            String soSymbol = ctx.getText();
            String mlSymbol = Bijections.get(Bijections.identifierStartOffice, Bijections.identifierUnicode, soSymbol);
            Element el = doc.createElement("mi");
            el.setTextContent(mlSymbol);
            setStartStopFromContext(el, ctx);
            pointer.appendChild(el);
        } else if ("identifier".equals(ruleName)) {
            String symbol = ctx.getText();
            Element el = doc.createElement("mi");
            el.setTextContent(symbol);
            setStartStopFromContext(el, ctx);
            pointer.appendChild(el);
        } else if (("expression").equals(ruleName) || ("group").equals(ruleName)) {
            if ("mrow".equals(pointer.getNodeName())) {
                int nChild = pointer.getChildNodes().getLength();
                if (nChild == 1) {
                    Node oneChild = pointer.getFirstChild();
                    Node parent = pointer.getParentNode();
                    parent.replaceChild(oneChild, pointer);
                    pointer = parent;
                } else {
                    pointer = pointer.getParentNode();
                }
            }
        } /*else if (("starmath").equals(ruleName)) {
            pointer = pointer.getParentNode();
        } */ else if (("expressions").equals(ruleName)) {
            pointer = pointer.getParentNode();
        } else if ("fraction".equals(ruleName)) {
            pointer = pointer.getParentNode();
        } else if ("nroot".equals(ruleName)) {
            if (pointer.getChildNodes().getLength() == 2) {
                // reverse arguments
                Node child1 = pointer.getFirstChild();
                pointer.removeChild(child1);
                pointer.appendChild(child1);
            } else {
                System.err.println("SHOULD------------------------ have two childs");
            }
            pointer = pointer.getParentNode();
        } else if ("sqrt".equals(ruleName)) {
            pointer = pointer.getParentNode();
        } else if ("sup".equals(ruleName)) {
            pointer = pointer.getParentNode();
        } else if ("fractype".equals(ruleName)) {
            if ("mfrac".equals(pointer.getNodeName())) {
                String text = ctx.getText();
                if ("wideslash".equals(text)) {
                    ((Element) pointer).setAttribute("bevelled", "true");

                }
                /* else if ("widebslash".equals(text)) {                    
                    ((Element) pointer).setAttribute("bevelled", "true");
                } */
            }
            //  pointer = pointer.getParentNode();
        } else if ("fromto".equals(ruleName)) {                        
            Node follow = pointer.getChildNodes().item(3);
            
            NodeList childs = pointer.getChildNodes();
            for (int i = 0 ; i <childs.getLength();i++){
                Node child = childs.item(i);
                System.err.println(DomUtils.outerXml(child));
            }
            
           /* pointer.removeChild(follow);
            pointer = pointer.getParentNode();
            pointer.appendChild(follow);
*/
            pointer = pointer.getParentNode();
        } else if ("from".equals(ruleName)) {
            Node follow = pointer.getChildNodes().item(2);
            pointer.removeChild(follow);
            /*pointer = pointer.getParentNode();
            pointer.appendChild(follow);
            */
            pointer = pointer.getParentNode();
        } else if ("to".equals(ruleName)) {
            Node follow = pointer.getChildNodes().item(2);
/*          
            pointer.removeChild(follow);
            pointer = pointer.getParentNode();
            pointer.appendChild(follow);
            */
            pointer = pointer.getParentNode();
        } else if ("matrix".equals(ruleName)) {
            pointer = pointer.getParentNode();
        } else if ("matrixline".equals(ruleName)) {
            pointer = pointer.getParentNode();
        } else if ("matrixcolumn".equals(ruleName)) {
            pointer = pointer.getParentNode();
        } else if ("stack".equals(ruleName)) {
            pointer = pointer.getParentNode();
        } else if ("stackline".equals(ruleName)) {
            pointer = pointer.getParentNode();
            pointer = pointer.getParentNode();
        } else if ("fencetype".equals(ruleName)) {
            String symbol = ctx.getText();
            if (symbol.equals("none")) {
            } else {
                Element el = doc.createElement("mo");
                el.setAttribute("fence", "true");
                setStartStopFromContext(el, ctx);
                pointer.appendChild(el);
                String op = Bijections.get(Bijections.operatorsStarOffice, Bijections.operatorsUnicode, symbol);
                el.setTextContent(op);
                //            pointer = el;
            }
            //      pointer = pointer.getParentNode();
        } else if ("overbrace".equals(ruleName)) {
            String symbol = ctx.getText();

            Element el = doc.createElement("mover");
            setStartStopFromContext(el, ctx);

            Element brace = doc.createElement("mo");
            brace.setAttribute("stretchy", "true");
            brace.setTextContent("⏞");

            Node child1 = pointer.getFirstChild();
            pointer.removeChild(child1);

            Node child2 = pointer.getFirstChild();
            pointer.removeChild(child2);

            el.appendChild(child1);
            el.appendChild(brace);

            pointer.appendChild(el);
            pointer.appendChild(child2);

            pointer = pointer.getParentNode();
        } else if ("underbrace".equals(ruleName)) {
            String symbol = ctx.getText();

            Element el = doc.createElement("munder");
            setStartStopFromContext(el, ctx);

            Element brace = doc.createElement("mo");
            brace.setAttribute("stretchy", "true");
            brace.setTextContent("⏟");

            Node child1 = pointer.getFirstChild();
            pointer.removeChild(child1);

            Node child2 = pointer.getFirstChild();
            pointer.removeChild(child2);

            el.appendChild(child1);
            el.appendChild(brace);

            pointer.appendChild(el);
            pointer.appendChild(child2);

            pointer = pointer.getParentNode();
        } else if ("unicodeIdentifier".equals(ruleName)) {
        } else if ("fourCharHexaChar".equals(ruleName)) {
            String symbol = ctx.getText();
            String asString = String.valueOf((char) Integer.parseInt(symbol, 16));
            pointer.setTextContent(asString);
            pointer = pointer.getParentNode();
        } else if ("sub".equals(ruleName)) {
            pointer = pointer.getParentNode();
        } else if ("subsup".equals(ruleName)) {
            pointer = pointer.getParentNode();
        } else if ("quotedStringContent".equals(ruleName)) {
            String content = ctx.getText();
            pointer.setTextContent(content);
        } else if ("quotedString".equals(ruleName)) {
            pointer = pointer.getParentNode();
        } else if ("knownFunction".equals(ruleName)) {
            String name = ctx.getText();
            setStartStopFromContext((Element) pointer, ctx);
            pointer.setTextContent(name);
            pointer = pointer.getParentNode();
        } else if ("function".equals(ruleName)) {

            pointer = pointer.getParentNode();
//        } else if ("color".equals(ruleName)) {
//            String name = ctx.getText();
//            setStartStopFromContext((Element) pointer, ctx);
//            ((Element) pointer).setAttribute("color", name);
//        } else if ("size".equals(ruleName)) {
//            String size = ctx.getText();
//            setStartStopFromContext((Element) pointer, ctx);
//            ((Element) pointer).setAttribute("mathsize", size);
//        } else if ("variantAttribute".equals(ruleName)) {
//            // TODO bold italic nitalic a -> mathvariant normal should be bold
//            String variant = ctx.getText();
//            if ("bold".equals(variant)) {
//                ((Element) pointer).setAttribute("mathvariant", "bold");
//            } else if ("italic".equals(variant)) {
//                ((Element) pointer).setAttribute("mathvariant", "italic");
//            } else if ("ital".equals(variant)) {
//                ((Element) pointer).setAttribute("mathvariant", "italic");
//            } else if ("nbold".equals(variant)) {
//                ((Element) pointer).setAttribute("mathvariant", "normal");
//            } else if ("nitalic".equals(variant)) {
//                ((Element) pointer).setAttribute("mathvariant", "normal");
//            }
//            setStartStopFromContext((Element) pointer, ctx);
//
//        } else if ("styleAttribute".equals(ruleName)) {
//            pointer = pointer.getParentNode();
//        } else if ("topAttribute".equals(ruleName)) {
//
//            String name = ctx.getText();
//
//            Node child1 = pointer.getFirstChild();
//            pointer.removeChild(child1);
//
//            Element mover = doc.createElement("mover");
//            mover.setAttribute("accent", "true");
//            mover.appendChild(child1);
//
//            Element mo = doc.createElement("mo");            
//            mo.setAttribute("stretchy", name.startsWith("wide")?"true":"false");
//            
//            String op = Bijections.get(Bijections.overAttributesStarOffice, Bijections.overAttributesUnicode, name);
//            mo.setTextContent(op);
//            mover.appendChild(mo);
//            
//            pointer.appendChild(mover);
//
//            pointer = pointer.getParentNode();
        } else {
            System.out.println("!! leave " + ruleName + " not defined");
        }
    }

    @Override
    public void visitTerminal(TerminalNode arg0
    ) {
        //      throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void visitErrorNode(ErrorNode arg0
    ) {
        //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public static void main(String[] argv) throws IOException {

        StarMathParser.main(argv);

    }
}
