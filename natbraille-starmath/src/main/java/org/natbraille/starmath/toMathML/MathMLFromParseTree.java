/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * Author : Vivien Guillet <vivien.guillet@laposte.net>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.starmath.toMathML;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.ParseTreeListener;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.natbraille.starmath.Bijections;
import org.natbraille.starmath.DomUtils;
import org.natbraille.starmath.StarMath5Parser;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.natbraille.starmath.Constants;

// todo Sum SuM sUm
// done : ( matrix { a # b ## c # d } ) --> wrong lparent (stretched)
// todo line / newline
// todo merror
// todo overline overstrike and other char attributes.
// todo and color font
// done binom is two-elements stack
// todo sum ^ _   (Operator)
// todo font attributes / attributes
// done %UxE036 mystery -> https://bugs.documentfoundation.org/show_bug.cgi?id=122196
// todo use mfenced ?
// todo alignment
/**
 *
 * @author vivien
 */
//doGlyphSpecial_unary
public class MathMLFromParseTree implements ParseTreeListener {
//
//    // public Document doc;
//    // private Node pointer;
//  //  private final boolean writePositions;
//    MathMLFromParseTreeBuilder B;
//    MathMLFromParseTree(boolean writePositions) {
//        
//       // this.doc = DomUtils.newDoc();
//        // this.pointer = doc; 
//        B = new MathMLFromParseTreeBuilder(writePositions);
////        this.writePositions = writePositions;
//    }

    public Document doc;
    private Node pointer;
    private final boolean writePositions;

    public boolean DEBUG_KEEP_PARSE_TREE_ORIGIN_RULE = true;
    public boolean DEBUG_KEEP_PARSE_TREE_MROWS = false;

    public final List<String> errors = new ArrayList();

    MathMLFromParseTree(boolean writePositions) {
        this.doc = DomUtils.newDoc();
        this.pointer = doc;
        this.writePositions = writePositions;
    }

    void setStartStopFromContext(Element e, ParserRuleContext ctx) {
        if (writePositions) {
            e.setAttribute(Constants.START_ATTRIBUTE_NAME, String.valueOf(ctx.getStart().getStartIndex()));
            Token stop = ctx.getStop();
            if (stop != null) {
                e.setAttribute(Constants.END_ATTRIBUTE_NAME, String.valueOf(stop.getStopIndex()));
            }
        }
    }

    private void setOriginRule(Element el, String ruleName) {
        if (DEBUG_KEEP_PARSE_TREE_ORIGIN_RULE) {
            el.setAttribute("x-ruleName", ruleName);
        }
    }

    /*
     * lift Element e single "mrow" child childrens to e child
     */
    private void preventSingleMrow(Node n) {
        if (pointer == n) {
            System.out.println("error ! pointer will be removed");
        }
        if (DEBUG_KEEP_PARSE_TREE_MROWS) {
            return;
        }
        if (n.getNodeType() == Node.ELEMENT_NODE) {
            Element e = (Element) n;
            if ("mrow".equals(e.getTagName())) {
                NodeList childs = e.getChildNodes();
                Node parent = e.getParentNode();
                if ((parent != null) && (childs.getLength() == 1)) {
                    Node child = childs.item(0);
                    e.removeChild(child);
                    parent.appendChild(child);
                    parent.removeChild(e);
                }
                // remove extra mrow

            }
        }
    }

    @Override
    public void enterEveryRule(ParserRuleContext ctx) {
        String ruleName = StarMath5Parser.ruleNames[ctx.getRuleIndex()];

        switch (ruleName) {
            case "customFunction":
            case "knownFunction":
            case "placeholder":
            case "standaloneIdentifier":
            case "userDefinedIdentifier": {
                Element el = doc.createElement("mi");
                setOriginRule(el, ruleName);
                setStartStopFromContext(el, ctx);
                el.setAttribute("mathvariant", "normal");
                pointer.appendChild(el);
                pointer = el;
                break;
            }
            case "identifier": {
                Element el = doc.createElement("mi");
                setOriginRule(el, ruleName);
                setStartStopFromContext(el, ctx);
                el.setAttribute("mathvariant", "italic");
                pointer.appendChild(el);
                pointer = el;
                break;
            }
            case "operatorOperatorGroup":
            case "relationOperatorGroup":
            case "productOperatorGroup":
            case "standaloneSymbol":
            case "sumOperatorGroup":
            case "normalUnOperSymbol":
            case "escapedBrace": {
                Element el = doc.createElement("mo");
                el.setAttribute("stretchy", "false");
                setOriginRule(el, ruleName);
                setStartStopFromContext(el, ctx);
                pointer.appendChild(el);
                pointer = el;
                break;
            }
            case "mline": {
                Element el = doc.createElement("mo");
                el.setAttribute("stretchy", "false");
                setOriginRule(el, ruleName);
                setStartStopFromContext(el, ctx);
                pointer.appendChild(el);
                pointer = el;
                break;
            }
            // case "br_lgroup":
            case "br_lparent":
            case "br_lbracket":
            case "br_lbrace":
            case "br_ldbracket":
            case "br_lline":
            case "br_ldline":
            case "br_langle":
            case "br_lfloor":
            case "br_lceil":
            // case "br_rgroup":
            case "br_rparent":
            case "br_rbracket":
            case "br_rbrace":
            case "br_rdbracket":
            case "br_rline":
            case "br_rdline":
            case "br_rangle":
            case "br_rfloor":
            case "br_rceil": {
                Element el = doc.createElement("mo");
                el.setAttribute("fence", "true");
                setOriginRule(el, ruleName);
                setStartStopFromContext(el, ctx);
                pointer.appendChild(el);
                pointer = el;
                break;

            }
            case "number": {
                Element el = doc.createElement("mn");
                setOriginRule(el, ruleName);
                setStartStopFromContext(el, ctx);
                pointer.appendChild(el);
                pointer = el;
                break;
            }
            case "string_litteral": {
                Element el = doc.createElement("mtext");
                setOriginRule(el, ruleName);
                setStartStopFromContext(el, ctx);
                pointer.appendChild(el);
                pointer = el;
                break;
            }
            case "blank_wide":
            case "blank_narrow": {
                break;
            }
            case "doBlank": {
                Element el = doc.createElement("mspace");
                setOriginRule(el, ruleName);
                setStartStopFromContext(el, ctx);
                pointer.appendChild(el);
                pointer = el;
                break;
            }
            case "doTable": {
                Element el = doc.createElement("math");
                el.setAttribute("xmlns", "http://www.w3.org/1998/Math/MathML");
                el.setAttribute("display", "block");
                Element el2 = doc.createElement("semantics");
                el.appendChild(el2);
                pointer.appendChild(el);
                pointer = el2;
                break;
            }
            case "doSubSup_oper_Limit_from_to": {
                Element el = doc.createElement("munderover");
                pointer.appendChild(el);
                pointer = el;
                break;
            }
            case "doSubSup_oper_Limit_from": {
                Element el = doc.createElement("munder");
                pointer.appendChild(el);
                pointer = el;
                break;
            }
            case "doSubSup_oper_Limit_to": {
                Element el = doc.createElement("mover");
                pointer.appendChild(el);
                pointer = el;
                break;
            }
            // case "doPower" :
            // case "doPower" :
            // case "doSubSup_power" :

            case "doBinom":
            case "doStack":
            case "doMatrix": {
                Element el = doc.createElement("mtable");
                pointer.appendChild(el);
                pointer = el;
                break;
            }
            case "matrixLine": {
                Element el = doc.createElement("mtr");
                pointer.appendChild(el);
                pointer = el;
                break;
            }
            case "matrixColumn": {
                Element el = doc.createElement("mtd");
                pointer.appendChild(el);
                pointer = el;
                break;
            }
            case "binomElement":
            case "doStackElement": {
                Element el = doc.createElement("mtr");
                pointer.appendChild(el);
                Element el2 = doc.createElement("mtd");
                el.appendChild(el2);
                pointer = el2;
                break;
            }
            case "doUnOper_sqrt": {
                Element el = doc.createElement("msqrt");
                setOriginRule(el, ruleName);
                setStartStopFromContext(el, ctx);
                pointer.appendChild(el);
                pointer = el;
                break;
            }
            case "doUnOper_nroot": {
                Element el = doc.createElement("mroot");
                setOriginRule(el, ruleName);
                setStartStopFromContext(el, ctx);
                pointer.appendChild(el);
                pointer = el;
                break;
            }
            case "doUnOper_abs": {
                Element el = doc.createElement("mrow");
                setOriginRule(el, ruleName);
                setStartStopFromContext(el, ctx);
                pointer.appendChild(el);
                {
                    Element el2 = doc.createElement("mo");
                    el2.setAttribute("fence", "true");
                    el2.setAttribute("stretchy", "false");
                    el2.setTextContent("|");
                    setOriginRule(el2, ruleName);
                    setStartStopFromContext(el2, ctx);
                    el.appendChild(el2);
                }
                {
                    Element elp = doc.createElement("mrow");
                    setOriginRule(elp, ruleName);
                    setStartStopFromContext(elp, ctx);
                    el.appendChild(elp);
                    pointer = elp;
                }
                {
                    Element el2 = doc.createElement("mo");
                    el2.setAttribute("fence", "true");
                    el2.setAttribute("stretchy", "false");
                    el2.setTextContent("|");
                    setOriginRule(el2, ruleName);
                    setStartStopFromContext(el2, ctx);
                    el.appendChild(el2);

                }
                break;
            }
            case "sm_wideslash":
            case "sm_over": {

                boolean bevelled = ruleName.equals("sm_wideslash");

                Element previousElement = (Element) pointer.removeChild(pointer.getLastChild());
                Element el = doc.createElement("mfrac");
                if (bevelled) {
                    el.setAttribute("bevelled", "true");
                }
                setOriginRule(el, ruleName);
                setStartStopFromContext(el, ctx);
                pointer.appendChild(el);
                pointer = el;

                pointer.appendChild(previousElement);

                break;
            }
            case "sm_underbrace":
            case "sm_overbrace": {

                Element previousElement = (Element) pointer.removeChild(pointer.getLastChild());

                String elementName = "sm_underbrace".equals(ruleName) ? "munder" : "mover";
                String symbol = "sm_underbrace".equals(ruleName) ? "⏟" : "⏞";

                Element el1 = doc.createElement(elementName);
                setOriginRule(el1, ruleName);
                setStartStopFromContext(el1, ctx);
                pointer.appendChild(el1);
                {
                    Element el2 = doc.createElement(elementName);
                    setOriginRule(el2, ruleName);
                    setStartStopFromContext(el2, ctx);
                    el2.appendChild(previousElement);
                    el1.appendChild(el2);
                    {
                        Element el3 = doc.createElement("mo");
                        el3.setAttribute("stretchy", "true");
                        el3.setTextContent(symbol);
                        el2.appendChild(el3);
                    }
                }
                pointer = el1;

                break;
            }
            case "doSubSup_Power": {
                /*
                 *   we create six placeholders which position denotes
                 *   role, and write each sub or sup according to this
                 *   position. 
                 *   When closing parse tree element, depending on
                 *   which placeholders are empty, we create the 
                 *   appropriate structure.
                 */
                Element el = doc.createElement("mrow");
                setOriginRule(el, ruleName);
                setStartStopFromContext(el, ctx);
                pointer.appendChild(el);
                pointer = el;

                for (int i = 0; i < 6; i++) {
                    Element powerPlaceholder = doc.createElement("placeholder");
                    el.appendChild(powerPlaceholder);
                }
                break;
            }
            case "p_lsub":
            case "p_lsup":
            case "p_csub":
            case "p_csup":
            case "p_rsub":
            case "p_rsup": {
                int position = 666;
                switch (ruleName) {
                    case "p_lsub":
                        position = 0;
                        break;
                    case "p_lsup":
                        position = 1;
                        break;
                    case "p_csub":
                        position = 2;
                        break;
                    case "p_csup":
                        position = 3;
                        break;
                    case "p_rsub":
                        position = 4;
                        break;
                    case "p_rsup":
                        position = 5;
                        break;
                }
                // Element parent = (Element) pointer.getParentNode();
                pointer = pointer.getChildNodes().item(position);
                break;
            }
            case "matrixContent": {
                // skip
                break;
            }
            case "attribute_rect": {
                // first child will be added on closing attributedTerm
                Element el;
                el = doc.createElement("menclose");
                el.setAttribute("notation", "horizontalstrike");
                pointer.appendChild(el);
                break;
            }
            case "attribute_under":
            case "attribute_over":
            case "attribute_wide_over": {
                // create a "mover"/"munder" and a nested "mo"
                // mo content wille be filled with text later
                // first child will be added on closing attributedTerm
                Element el;
                if (ruleName.equals("attribute_under")) {
                    el = doc.createElement("munder");
                } else {
                    el = doc.createElement("mover");
                }
                el.setAttribute("accent", "true");
                setOriginRule(el, ruleName);
                setStartStopFromContext(el, ctx);

                Element mo = doc.createElement("mo");
                if (ruleName.equals("attribute_wide_over")) {
                    mo.setAttribute("stretchy", "true");
                } else {
                    mo.setAttribute("stretchy", "false");
                }
                setOriginRule(el, ruleName);
                setStartStopFromContext(el, ctx);
                el.appendChild(mo);

                pointer.appendChild(el);
                // two steps !
                pointer = mo;
                break;
            }
            case "nospace": {
                break;
            }
            case "doLine":
            case "doExpression":
            case "doRelation":
            case "doSum":
            case "doProduct":
            case "doPower":
            case "doTerm":
            default: {
                Element el = doc.createElement("mrow");
                setOriginRule(el, ruleName);
                setStartStopFromContext(el, ctx);
                pointer.appendChild(el);
                pointer = el;
                break;
            }

        }
    }

    @Override
    public void exitEveryRule(ParserRuleContext ctx) {
        String ruleName = StarMath5Parser.ruleNames[ctx.getRuleIndex()];
        setStartStopFromContext((Element) pointer, ctx);
        String text = ctx.getText();
        switch (ruleName) {
            case "standaloneIdentifier": {
                pointer.setTextContent(Bijections.get(Bijections.standaloneIdentifierStarOffice, Bijections.standaloneIdentifierUnicode, text));
                pointer = pointer.getParentNode();
                break;
            }
            case "userDefinedIdentifier":
            case "customFunction":
            case "knownFunction":
            case "identifier": {
                pointer.setTextContent(Bijections.get(Bijections.identifierStartOffice, Bijections.identifierUnicode, text));
                pointer = pointer.getParentNode();
                break;
            }
            case "placeholder": {
                pointer.setTextContent("❑");
                pointer = pointer.getParentNode();
                break;
            }
            case "operatorOperatorGroup":
            case "productOperatorGroup":
            case "relationOperatorGroup":
            case "sumOperatorGroup":
            case "normalUnOperSymbol":
            case "escapedBrace": {
                pointer.setTextContent(Bijections.get(Bijections.operatorsStarOffice, Bijections.operatorsUnicode, text));
                pointer = pointer.getParentNode();
                break;
            }
            case "mline": {
                pointer.setTextContent("|");
                pointer = pointer.getParentNode();
                break;
            }
            case "standaloneSymbol": {
                pointer.setTextContent(Bijections.get(Bijections.standaloneSymbolStarOffice, Bijections.standaloneSymbolUnicode, text));
                pointer = pointer.getParentNode();
                break;

            }
            case "br_lparent":
            case "br_lbracket":
            case "br_lbrace":
            case "br_ldbracket":
            case "br_lline":
            case "br_ldline":
            case "br_langle":
            case "br_lfloor":
            case "br_lceil":
            // case "br_lgroup":
            // case "br_rgroup":
            case "br_rparent":
            case "br_rbracket":
            case "br_rbrace":
            case "br_rdbracket":
            case "br_rline":
            case "br_rdline":
            case "br_rangle":
            case "br_rfloor":
            case "br_rceil": {
                pointer.setTextContent(Bijections.get(Bijections.bracesStarOffice, Bijections.bracesUnicode, text));
                pointer = pointer.getParentNode();
                break;
            }
            case "wideBrace":
            case "brace": {
                String stretchy = ("wideBrace".equals(ruleName)) ? "true" : "false";
                NodeList childs = pointer.getChildNodes();
                if (childs.getLength() > 0) {
                    Element open = (Element) pointer.getFirstChild();
                    if (open != null) {
                        open.setAttribute("stretchy", stretchy);
                    }
                    Element close = (Element) pointer.getLastChild();
                    if (close != null) {
                        close.setAttribute("stretchy", stretchy);
                    }
                }
                pointer = pointer.getParentNode();
                break;
            }
            case "number": {
                pointer.setTextContent(text);
                pointer = pointer.getParentNode();
                break;
            }
            case "string_litteral": {
                pointer.setTextContent(text.substring(1, text.length() - 1));
                pointer = pointer.getParentNode();
                break;
            }
            case "blank_wide":
            case "blank_narrow": {
                String width = "blank_wide".equals(ruleName) ? "2em" : "0.5em";
                ((Element) pointer).setAttribute("width", width);
                break;
            }
            case "binomElement":
            case "doStackElement": {
                pointer = pointer.getParentNode();
                pointer = pointer.getParentNode();
                break;
            }
            case "doUnOper_nroot": {
                // always two children
                pointer.appendChild(pointer.removeChild(pointer.getFirstChild()));
                pointer = pointer.getParentNode();
                break;
            }
            /*case "sm_over" :{
                Node parent = pointer.getParentNode();
                break;
            }*/
            case "doUnOper_abs": {
                pointer = pointer.getParentNode();
                pointer = pointer.getParentNode();
                break;
            }
            case "doUnOper_fact": {
                Element el = doc.createElement("mo");
                el.setAttribute("stretchy", "false");
                el.setTextContent("!");
                setOriginRule(el, ruleName);
                setStartStopFromContext(el, ctx);
                pointer.appendChild(el);
                pointer = pointer.getParentNode();
                break;
            }
            case "doAlign": {
                // pointer.setTextContent(text);
                pointer = pointer.getParentNode();
                break;

            }
            /*
            case "doTable": {                
                // Element semantic = (Element) pointer.getFirstChild();
                NodeList childs = pointer.getChildNodes();
                
                if (childs.getLength() > 1){
                    Element table = doc.createElement("mtable");
                    table.setAttribute("BOF","BOF");
                    do {
                        Element tr= doc.createElement("mtr");
                        table.appendChild(tr);
                        Element td = doc.createElement("mtd");
                        tr.appendChild(td);
                        System.out.println(DomUtils.outerXml(pointer.getFirstChild()));
                        Node child = pointer.removeChild(pointer.getFirstChild());
                        td.appendChild(child);
                        preventSingleMrow(td);
                    } while (pointer.getChildNodes().getLength() > 0);
                    pointer.appendChild(table);
                    // pointer = = pointer.getParentNode();
                } else {
                    System.err.println("");
                }
                break;
            }*/
            case "doSubSup_Power": {

                // put previous term as first child
                Element parent = (Element) pointer.getParentNode();
                Node term = parent.removeChild(parent.getFirstChild());

                int childed[] = {1, 2, 4, 8, 16, 32};
                int reduced = 0;
                NodeList placeholders = (NodeList) pointer.getChildNodes();
                for (int i = 0; i < childed.length; i++) {
                    reduced |= placeholders.item(i).hasChildNodes() ? childed[i] : 0;
                }
                Node innerTerm;
                if ((reduced & (4 | 8)) == (4 | 8)) {
                    Element munderover = doc.createElement("munderover");
                    munderover.appendChild(term);
                    Node under = placeholders.item(2).removeChild(placeholders.item(2).getFirstChild());
                    munderover.appendChild(under);
                    Node over = placeholders.item(3).removeChild(placeholders.item(3).getFirstChild());
                    munderover.appendChild(over);
                    innerTerm = munderover;
                } else if ((reduced & 4) != 0) {
                    Element munder = doc.createElement("munder");
                    munder.appendChild(term);
                    Node under = placeholders.item(2).removeChild(placeholders.item(2).getFirstChild());
                    munder.appendChild(under);
                    innerTerm = munder;
                } else if ((reduced & 8) != 0) {
                    Element mover = doc.createElement("mover");
                    mover.appendChild(term);
                    Node over = placeholders.item(3).removeChild(placeholders.item(3).getFirstChild());
                    mover.appendChild(over);
                    innerTerm = mover;
                } else {
                    System.out.println("use noneA");
                    innerTerm = term;
                }

                Node outerStruct;

                if ((reduced & (1 | 2)) != 0) {
                    Element mmultiscripts = doc.createElement("mmultiscripts");
                    mmultiscripts.appendChild(innerTerm);

                    int mmidxs[] = {4, 5, 0, 1};
                    for (int i = 0; i < mmidxs.length; i++) {
                        Node n;
                        int num = mmidxs[i];
                        if ((reduced & childed[num]) != 0) {
                            n = placeholders.item(num).removeChild(placeholders.item(num).getFirstChild());
                        } else {
                            n = doc.createElement("none");
                        }
                        mmultiscripts.appendChild(n);

                        if (i == 1) {
                            n = doc.createElement("mprescripts");
                            mmultiscripts.appendChild(n);
                        }
                    }
                    outerStruct = mmultiscripts;
                } else if ((reduced & (16 | 32)) == (16 | 32)) {
                    Element msubsup = doc.createElement("msubsup");
                    msubsup.appendChild(innerTerm);
                    Node sub = placeholders.item(4).removeChild(placeholders.item(4).getFirstChild());
                    msubsup.appendChild(sub);
                    Node sup = placeholders.item(5).removeChild(placeholders.item(5).getFirstChild());
                    msubsup.appendChild(sup);
                    outerStruct = msubsup;
                } else if ((reduced & (16)) != 0) {
                    Element msub = doc.createElement("msub");
                    msub.appendChild(innerTerm);
                    Node sub = placeholders.item(4).removeChild(placeholders.item(4).getFirstChild());
                    msub.appendChild(sub);
                    outerStruct = msub;
                } else if ((reduced & (32)) != 0) {
                    Element msup = doc.createElement("msup");
                    msup.appendChild(innerTerm);
                    Node sup = placeholders.item(5).removeChild(placeholders.item(5).getFirstChild());
                    msup.appendChild(sup);
                    outerStruct = msup;
                } else {
                    outerStruct = innerTerm;
                }

                ((Node) parent).removeChild(parent.getFirstChild());
                ((Node) parent).appendChild(outerStruct);
                pointer = parent;

                break;

            }
            case "attribute_under": {
                String mml = Bijections.get(Bijections.underAttributesStarOffice, Bijections.underAttributesUnicode, text);
                pointer.setTextContent(mml);
                pointer = pointer.getParentNode().getParentNode();
                break;
            }
            case "attribute_over":
            case "attribute_wide_over": {
                String mml = Bijections.get(Bijections.overAttributesStarOffice, Bijections.overAttributesUnicode, text);
                pointer.setTextContent(mml);
                pointer = pointer.getParentNode().getParentNode();
                break;
            }
            case "attributedTerm": {
                Node b = pointer;

                while (b.getChildNodes().getLength() > 1) {
                    // ( a(),b(),c() ) -> ( a(),b(c()) )
                    NodeList childs = b.getChildNodes();
                    Node lastChild = childs.item(childs.getLength() - 1);
                    Node penultimateChild = childs.item(childs.getLength() - 2);
                    Node penultimateFirstChild = penultimateChild.getFirstChild();
                    b.removeChild(lastChild);
                    penultimateChild.insertBefore(lastChild, penultimateFirstChild);
                }
                pointer = pointer.getParentNode();
                break;
            }
            case "nospace": {
                break;
            }
            case "matrixContent": {
                // skip
                break;
            }
            case "doMatrix":
            case "doBracebody": // case "doBrace":
            //case "doLine": 
            {
                pointer = pointer.getParentNode();
                break;
            }
            case "doExpression":
            case "doRelation":
            case "doSum":
            case "doProduct":
            case "doPower":
            case "doTerm":
            default: {

                Node doneElement = pointer;
                pointer = pointer.getParentNode();
                preventSingleMrow(doneElement);

                break;
            }
        }

    }

    @Override
    public void visitTerminal(TerminalNode arg0) {
    }

    @Override
    public void visitErrorNode(ErrorNode arg0) {
        errors.add(arg0.getText());
    }

    public static void main(String[] argv) throws IOException {

        StarMathParser.main(argv);

    }
}
