/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * Author : Vivien Guillet <vivien.guillet@laposte.net>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.starmath.toMathML;


import java.io.IOException;
import java.nio.CharBuffer;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.print.PrintException;
import org.antlr.v4.gui.TreeViewer;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeListener;
import org.natbraille.starmath.DomUtils;
import org.natbraille.starmath.StarMath5Lexer;
import org.natbraille.starmath.StarMath5Parser;
import org.w3c.dom.Document;

//import org.antlr.v4.runtime.tree.*;
// import org.antlr.v4.runtime.tree.*;
/**
 *
 * @author vivien
 * https://wiki.documentfoundation.org/images/2/26/MG44-MathGuide.pdf
 */
public class StarMathParser {

    String[][] tests = {
        new String[]{"%alpha", "<math><mi>α</mi></math>"},
        new String[]{"+5-3times5,3%alpha", "<math><mo>+</mo><mn>5</mn><mo>-</mo><mn>3</mn><mo>times</mo><mn>5,3</mn><mi>α</mi></math>"},};

    private static CodePointCharStream StringToCodePointCharStream(String code) {
        CharBuffer charBuffer = CharBuffer.wrap(code.toCharArray());
        CodePointBuffer codePointBuffer = CodePointBuffer.withChars(charBuffer);
        CodePointCharStream cpcs = CodePointCharStream.fromBuffer(codePointBuffer);
        return cpcs;
    }

    public static void parseDev(boolean writePositions) throws IOException {
        //String code = readFile(file, Charset.forName("UTF-8"));
        // String code = "A = pi^2";
        // String code = "{{%alpha12,56}{%beta}}";
        // String code = "%alpha";
        // String code = "+5-3times5,3%alpha";
        // String code = "%alpha{%beta}%gamma25+3";
        // String code = "%alphaover%beta";
        // String code="%alphawideslash2";
        // String code="%alphawidebslash2+{5times3}";
        // String code="%alpha + {%beta + %delta}";
        // String code="%alpha over %beta";        
        // String code="{%alpha over %beta}";
        // String code = "+-%alpha";
        // String code = "-+%alpha";
        // String code = "1 cdot 2";
        // String code = "nroot12%alpha";
        // String code = "sqrt3";
        //  String code ="%alpha ^ %beta";
        // String code = "coprod from %alpha";
        // String code = "coprod from %alpha to %beta=3 12";
        // String code = "matrix{%alpha#%beta##%gamma#%delta}";
        // String code = "stack{%alpha#%beta}";
        // String code = "anc+5";        
        // String code = "%beta + be3";        
        // String code = "left ( A over B right none"; 
        // String code = "A overbrace B";
        //String code = "\u2013";
        // String code = "– {1 over 2}";
        // String code = "\u2013";
        //String code = Character.toString((char)0x2212);
        // String code = "@";
        // String code = "a%Ux21CCb";
        // String code ="NH_3 + H_2";
        // String code ="H_3";
        // String code ="H_2^+";
        //String code ="A transl B";
        // -> String code ="\"bob\"";
        // String code = "sin a+5";
        // String code = "coprod from a to b c";
        // String code = "coprod from a c";
        // String code = "coprod to a c";
        // String code = "color red size 20.5 bold italic underline breve ddot hat color maroon a";
        // String code = "%alpha";
        // String code = "sum from i = 2 to 5 i^2";
        //////////////////
        // String code = "sum from a to b c";
        //String code = "a divides b";
        // String code = "5 times 44";

        // String code = "{iiint from {A} to {B}C}";
        // String code = "iint to{A} B";
        //String code = "left ( A over B right )";
        // String code = "a+b + c / 5 + sum from a to i = %infty c";
        // String code = "sum from i = 0 to i = %infini+12 func s(3) + 8 ";
        // String code = "a * matrix {  a = sum from a to b {c} # \"boby\" ## c # d }";
        // String code = "stack{ a # b +e #c +d }";
        // String code = "<?> <> %beta";
        // String code="binom x+56565 y-365*5 %alpha";
        //String code = "a = setN";
        // String code="[ %alpha %gamma ]";
        //String code="  left   [ %alpha %gamma right ] ";
        // String code="  ( %alpha %gamma ) ";
        
        // String code = "abs 3+5";
        // String code = "abs a + b";
        // String code = "left lbrace a = 25 right none";
        // String code = "abs x = left lbrace stack {x \"for\" x >= 0 # -x \"for\" x < 0} right none";
        // String code = "coprod from{A} to{B} C";
        ///String code = "fact 32";
        
         // String code = "A newline B newline alignr C newline alignc newline D";
         
// String code = "alignl aaaaa newline alignr b newline alignb c";
//         String code = "a bslash c";
            // String code = "leftarrow setN";
         //   String code = "int from 0 to x f(t) dt";
            // String code = "int from 0 to x { f(t) dt } ";
        // String code = "( matrix { a # b ## c # d } )";
         
        //String code = "coprod from A  to B {C}";
        // String code = "sqrt 5*5";
        // String code = "a + 12 over b";
        // String code ="{ a overbrace b underbrace c over d }";
        // String code ="{ a overbrace b } = { a underbrace b } = { a over b }";
        // String code ="{ a overbrace b * 3 }";
         // String code ="a over b over c";
         // String code ="a over b overbrace c = d";
         // String code="a overbrace b overbrace c overbrace d";
         // String code="a over b overbrace c underbrace d";
         //String code = "exists dotsup";
        // String code = "+-a";
        // String code = "a^%beta_%gamma";
        // String code = "\\{ a +\\}";
        // String code = "sin a + b";
        // String code = "func u";
        // String code = "%pi ~ simeq ~ 3.14159";
        // String code ="langle AB mline CD rangle";
        // String code ="(12 mline A 3)";
        // String code="123`456";
        // String code ="langle AB 25 rangle";
        // String code ="langle AB 25 CD rangle";
        // String code ="( a )"; // ok 
        // String code ="( a + b )";  // ok
        // String code ="( matrix{a} )";  // ok
        //String code ="( matrix { a # b ## c # d } )";
        // String code ="a lsub b lsup c csub d csup e rsub f rsup g { e }";  // ok
         //String code ="a lsub b lsup c csub d csup e rsub f rsup g { e }";  // ok
         // String code ="a csub z rsup b rsub c { e }";  // ok
         //String code ="a csub z rsub c { e }";  // ok
         // String code ="%alpha + a csup x csub z rsup c { %beta }";  // ok
         //String code="20 lsub{10}";
         String code = "binom{AB}{12}";
        //String code ="a rsup g { e }";  // ok
        System.out.println(code);

        // Lexer -> tokens
        CodePointCharStream cpcs = StringToCodePointCharStream(code);
        StarMath5Lexer lexer = new StarMath5Lexer(cpcs);

        CommonTokenStream tokens = new CommonTokenStream(lexer);

        // Parser
        StarMath5Parser parser = new StarMath5Parser(tokens);

        System.err.println(parser.getParseInfo());

        ParseTreeListener listener2 = new ParseTreePrinter();

        MathMLFromParseTree listener = new MathMLFromParseTree(writePositions);
        parser.addParseListener(listener);
        parser.addParseListener(listener2);

        // lisp like
        ParseTree tree = parser.doTable();
        System.out.println(tree.toStringTree(parser));
        // png
        boolean doPng = false;
        if (doPng) {
            TreeViewer viewer = new TreeViewer(Arrays.asList(
                    parser.getRuleNames()), tree);
            viewer.setScale(0.8);
            try {

                viewer.save("parseResult.png");
            } catch (PrintException ex) {
                Logger.getLogger(StarMathParser.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println("------");
        System.out.println(DomUtils.outerXml(listener.doc.getDocumentElement()));

    }

    public static Document parseString(String starMath, boolean writePositions) {

        CodePointCharStream cpcs = StringToCodePointCharStream(starMath);
        StarMath5Lexer lexer = new StarMath5Lexer(cpcs);

        CommonTokenStream tokens = new CommonTokenStream(lexer);
        StarMath5Parser parser = new StarMath5Parser(tokens);

        System.err.println(parser.getParseInfo());

        // ParseTreeListener listener2 = new ParseTreePrinter();
        MathMLFromParseTree listener = new MathMLFromParseTree(writePositions);
        parser.addParseListener(listener);
        // parser.addParseListener(listener2);

        parser.doTable();
        // System.out.println("------");
        // System.out.println(DomUtils.outerXml(listener.doc.getDocumentElement()));
        return listener.doc;
        //listener.errors;
    }

    public static void main(String[] argv) throws IOException {
        parseDev(true);

        /*
        // test greek letters
        Arrays.asList(starOffice).stream().forEachOrdered(s -> {
            System.err.println(s+" "+parseGreek(s));                        
        });
         */
    }
}
