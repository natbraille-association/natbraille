/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * Author : Vivien Guillet <vivien.guillet@laposte.net>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.starmath;

import java.util.ArrayList;
import java.util.List;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author vivien
 */
public class MathMLFromParseTreeBuilder {

    private boolean DEBUG_KEEP_PARSE_TREE_ORIGIN_RULE = true;
    ;
    private boolean DEBUG_KEEP_PARSE_TREE_MROWS = false;

    public void setDEBUG_KEEP_PARSE_TREE_MROWS(boolean DEBUG_KEEP_PARSE_TREE_MROWS) {
        this.DEBUG_KEEP_PARSE_TREE_MROWS = DEBUG_KEEP_PARSE_TREE_MROWS;
    }

    public void setDEBUG_KEEP_PARSE_TREE_ORIGIN_RULE(boolean DEBUG_KEEP_PARSE_TREE_ORIGIN_RULE) {
        this.DEBUG_KEEP_PARSE_TREE_ORIGIN_RULE = DEBUG_KEEP_PARSE_TREE_ORIGIN_RULE;
    }

    public Document doc;
    public Node pointer = null;
    public boolean writePositions = true;

    public final List<String> errors = new ArrayList();

    public MathMLFromParseTreeBuilder(boolean writePositions) {
        /*this.doc = DomUtils.newDoc();
        this.pointer = doc;
         */
        this.writePositions = writePositions;
    }

    public void setStartStopFromContext(Element e, ParserRuleContext ctx) {
        if (writePositions) {
            e.setAttribute(Constants.START_ATTRIBUTE_NAME, String.valueOf(ctx.getStart().getStartIndex()));
            Token stop = ctx.getStop();
            if (stop != null) {
                e.setAttribute(Constants.END_ATTRIBUTE_NAME, String.valueOf(stop.getStopIndex()));
            }
        }
    }

    public void setOriginRule(Element el, String ruleName) {
        if (DEBUG_KEEP_PARSE_TREE_ORIGIN_RULE) {
            el.setAttribute(Constants.TREE_ORIGIN_RULE, ruleName);
        }
    }

    /*
     * lift Element e single "mrow" child childrens to e child
     */
    public void preventSingleMrow(Node n) {
        if (pointer == n) {
            System.out.println("error ! pointer will be removed");
        }
        if (DEBUG_KEEP_PARSE_TREE_MROWS) {
            return;
        }
        if (n.getNodeType() == Node.ELEMENT_NODE) {
            Element e = (Element) n;
            if ("mrow".equals(e.getTagName())) {
                NodeList childs = e.getChildNodes();
                Node parent = e.getParentNode();
                if ((parent != null) && (childs.getLength() == 1)) {
                    Node child = childs.item(0);
                    e.removeChild(child);
                    parent.appendChild(child);
                    parent.removeChild(e);
                }
                // remove extra mrow

            }
        }
    }
    
}
