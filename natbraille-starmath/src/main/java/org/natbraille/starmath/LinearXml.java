/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * Author : Vivien Guillet <vivien.guillet@laposte.net>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.starmath;

import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.regex.Pattern;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author vivien
 */
public class LinearXml {

    private static class DefaultHandlerImpl extends DefaultHandler {

        private final StringBuilder sb = new StringBuilder();
        private final boolean printAttributes;

        public DefaultHandlerImpl(boolean printAttributes) {
            super();
            this.printAttributes = printAttributes;
        }

        private String stripSpaces(String s) {
            Pattern stripSpacesPattern = Pattern.compile("(^(\\s)*|(\\s)*$)", Pattern.DOTALL);
            return stripSpacesPattern.matcher(s).replaceAll("");
        }

        @Override
        public void characters(char[] ch, int start, int length) throws SAXException {
            String g = stripSpaces(new String(ch, start, length));
            if (g.length() != 0) {
                sb.append('"').append(g).append('"');
            }
        }

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            if (printAttributes) {
                boolean hasAttributes = (attributes.getLength() != 0);
                if (hasAttributes) {
                    sb.append("[");
                }
                for (int i = 0; i < attributes.getLength(); i++) {
                    sb.append(attributes.getQName(i));
                    sb.append(":");
                    sb.append("\"");
                    sb.append(attributes.getValue(i));
                    sb.append("\"");
                }
                if (hasAttributes) {
                    sb.append("]");
                }
            }
            sb.append(qName).append("(");
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            sb.append(")");
        }

        public String getString() {
            return sb.toString();
        }
    }

    private static int saxParse() {
        SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
        try {
            // String path = "/home/vivien/src/braille/mathmltestsuite/testsuite/testsuite/Presentation/ScriptsAndLimits/mover/mover1.mml";            
            String path = "./test/test1.mml";
            // String path = "./test/test2.mml";
            String fileContent = new String(Files.readAllBytes(Paths.get(path)));

            SAXParser saxParser = saxParserFactory.newSAXParser();

            DefaultHandlerImpl handler = new DefaultHandlerImpl(true);

            saxParser.parse(new InputSource(new StringReader(fileContent)), handler);
            System.out.println(handler.getString());

            //System.err.println("resu:" + handler.getResultDebug());
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
        return 42;
    }

    public static void main(String[] argv) {
        saxParse();
    }

}
