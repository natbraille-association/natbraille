/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * Author : Vivien Guillet <vivien.guillet@laposte.net>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

parser grammar StarMath5Parser ;

// todo :  mismatched attribute / font attribute
// todo : ^ and _
// done : mline
// todo font size
// todo : identifier spacing problem
//      : "sum to b e" should be writtern "sum to {b} e" or "sum to b {e}"
//      : "a msub b e" should be writtern "a msub {b} e" or "a msub b {e}

options { tokenVocab = StarMath5Lexer; }


// eol_comment : EOL_COMMENT ;
string_litteral : STRING_LITTERAL;
// user_defined_identifier : USER_DEFINED_IDENTIFIER ;
number : NUMBER_TYPE_COMMA | NUMBER_TYPE_REGULAR;
identifier : OTHER+;
/*
main
        : ( eol_comment | string_litteral | user_defined_identifier | identifier
        | number
        | identifier )*
        ;
*/
//doTable :
//     TK_newline?  doLine ( TK_newline doLine )* EOF
//    ;

//comment : EOL_COMMENT_TAIL;

newline : TK_newline   ;

doTable :
     ( newline | doLine )* EOF
    ;

doLine
    : doExpression+
    ;

doAlign
    : TG_Align ;

// REPLACES doALign
doExpression :
    doAlign? nospace? doRelation+
    ;


relationOperatorGroup
    : TK_approx   | TK_def   | TK_divides   | TK_equiv
    | TK_ge   | TK_geslant   | TK_gg   | TK_gt   | TK_in
    | TK_le   | TK_leslant   | TK_ll   | TK_lt   | TK_ndivides
    | TK_neq   | TK_ni   | TK_notin   | TK_nsubset   | TK_nsupset
    | TK_nsubseteq   | TK_nsupseteq   | TK_ortho   | TK_owns
    | TK_parallel   | TK_prec   | TK_preccurlyeq   | TK_precsim
    | TK_nprec   | TK_prop   | TK_sim   | TK_simeq   | TK_subset
    | TK_succ   | TK_succcurlyeq   | TK_succsim   | TK_nsucc
    | TK_subseteq   | TK_supset   | TK_supseteq   | TK_toward
    | TK_transl   | TK_transr | TK_assign
    ;

// opsupsup -> powre ?
doRelation :
    doSum (relationOperatorGroup doOpSubSup? doSum )*
    ;


sumOperatorGroup
    : TK_plus | TK_minus | TK_ominus | TK_oplus | TK_or | TK_union
    ;

// opsupsup -> powre ?
doSum :
    doProduct ( sumOperatorGroup doOpSubSup? doProduct)*
    ;

// extract special operators from productOperatorGroup
// original
// doProduct :  doPower ( productOperatorGroup doPower )*
productOperatorGroup
    : doGlyphSpecial_binary
    | TK_and | TK_bslash | TK_cdot | TK_div | TK_intersection
    | TK_odivide  | TK_odot  |  TK_otimes
    | TK_setminus | TK_slash | TK_times | TK_widebslash
    | TK_divideby | TK_multiply
    ;
//    | TK_overbrace   // mathml mover
//    | TK_underbrace  // mathml munder
//    | TK_over        // mathml mfrac
//    | TK_wideslash   // mathml mfrac bevelled = true
sm_over : TK_over doPower ;
sm_wideslash : TK_wideslash doPower ;
sm_overbrace : TK_overbrace doPower ;
sm_underbrace: TK_underbrace doPower ;

doProduct
    :  doPower
    (
        ( productOperatorGroup doPower )
        | sm_over
        | sm_wideslash
        | sm_overbrace
        | sm_underbrace
    )*
    ;

// doSubSup has param (TG nActiveGroup)

// mathml mmultiscripts
subsupOperatorGroup
    : TK_csub | TK_csup | TK_lsub | TK_lsup | TK_sub
    | TK_rsub | TK_sup | TK_rsup
    ;

// from, to         -> TG::Limit -> limit_ftom_to ...
// csup, csub, etc. -> TG::power

// the x^2n32132dvlkjdlvkj
// only in case of power, doterm(true)
// is replaced by fuckedIdentifier
// does not perfectly match staroffice (see lexer)
// + subparsing must be done in visitor
//fuckedup_sub : FUCKEDUP_SUB;
//fuckedup_sup : FUCKEDUP_SUP;

//doSubSup_Power
//    : (
///*        fuckedup_sub
//        | fuckedup_sup
//
//        | */( subsupOperatorGroup doTerm(/*true*/)  )
//      )+
//    ;

// special from and to takes expression
// other take doTerm

p_csub : TK_csub doTerm ;
p_csup : TK_csup doTerm;
p_lsub : TK_lsub doTerm ;
p_lsup : TK_lsup doTerm;
p_rsub : ( TK_sub | TK_rsub ) doTerm;
p_rsup : ( TK_sup | TK_rsup ) doTerm;

doSubSup_Power
    :( p_csub |      p_csup |     p_lsub|     p_lsup|     p_rsub|     p_rsup )+
     ;


// nullable
doOpSubSup :
    doSubSup_Power doPower?
    ;

doPower :
    doTerm /* term(false) */
    doSubSup_Power?
    ;

blank_wide : TK_blank_wide;
blank_narrow : TK_blank_narrow;
doBlank
    : ( blank_wide | blank_narrow )+
    ;


// TK_toward is relational
// SmMathSymbolNode
standaloneSymbol
    : TK_leftarrow  | TK_rightarrow | TK_uparrow | TK_downarrow
    | TK_circ
    | TK_dlrarrow | TK_dlarrow | TK_drarrow
    | TK_exists | TK_notexists
    | TK_forall | TK_partial  | TK_nabla
    | TK_dotsaxis  | TK_dotsdiag | TK_dotsdown | TK_dotslow  | TK_dotsup | TK_dotsvert
    | TK_toward
    ;

// SmMathIdentifierNode
standaloneIdentifier
    : TK_setN | TK_setZ   | TK_setQ   | TK_setR     |  TK_setC
    | TK_hbar | TK_lambdabar  | TK_backepsilon | TK_aleph
    | TK_Im | TK_Re
    | TK_wp
    | TK_emptyset
    | TK_infinity | TK_infty
    ;

nospace
    : TK_nospace
    ;

placeholder : TK_PLACE;

// special_identifier_number
//    : (OTHER | NUMBER_TYPE_COMMA | NUMBER_TYPE_REGULAR)+
//    ;
// true / false ( 2_3n feature )
// TLGROUP
doTerm :
    doEscape
    /* nospace */
    /* tlgroup empty ? */
    | braceGroup
    | doBlank
    | string_litteral
    /* tchar */
    /* tchar */
    /* tident */
    /* tnumber */
    // | special_identifier_number
    | identifier
    | number
    | standaloneSymbol
    | standaloneIdentifier
    | placeholder
    | doSpecial
    | doBinom
    | doStack
    | doMatrix
    | doBrace
    | doOperator
    | doUnOper
    // | ( doAttribut | doFontAttribut )+ doPower?
    | attributedTerm
    | doFunction
    ;

attributedTerm
    : ( doAttribut | doFontAttribut )+ doPower?
    ;

escapedBrace
    : TK_lgroup | TK_rgroup | TK_lparent | TK_rparent | TK_lbracket | TK_rbracket
    | TK_lbrace | TK_rbrace | TK_ldbracket | TK_rdbracket | TK_lline | TK_rline
    | TK_ldline | TK_rdline | TK_langle | TK_rangle | TK_lfloor | TK_rfloor
    | TK_lceil | TK_rceil
;

doEscape
    :  TK_escape escapedBrace
    ;

// operator is sums, lims, etc.
operatorOperatorGroup
    : TK_sum  | TK_prod | TK_coprod
    | TK_iiint | TK_iint | TK_int
    | TK_lint | TK_llint | TK_lllint
    | TK_lim | TK_liminf | TK_limsup
;

/* seems to accept TK_oper identifier */
/* but : 'oper alpha from a to b c' -> shown in editor / mathml do not match */
customOperator
    : TK_oper userDefinedIdentifier
    ;

doOper
    : customOperator | operatorOperatorGroup
    ;

// finer pattern matching
doSubSup_oper_Limit_empty :   doOper doRelation;
doSubSup_oper_Limit_from :    doOper ( TK_from | TK_csub ) doRelation ;
doSubSup_oper_Limit_to :      doOper ( TK_to | TK_csup ) doRelation ;
doSubSup_oper_Limit_from_to
    : doOper
    (
        ( TK_from doRelation TK_to doRelation  )
      | ( TK_to doRelation TK_from doRelation  )
     )
     ;

//doSubSup_oper_Limit_from :    doOper ( ( TK_from doRelation) | fuckedup_sub );
//doSubSup_oper_Limit_to :      doOper ( ( TK_to doRelation )  | fuckedup_sup );
//doSubSup_oper_Limit_from_to
//    : doOper
//    (
//        ( TK_from doRelation TK_to doRelation  )
//      | ( TK_to doRelation TK_from doRelation  )
//      | ( fuckedup_sup | fuckedup_sup )
//      | ( fuckedup_sup | fuckedup_sub )
//     )
//     ;

doSubSup_oper_Limit
    : doSubSup_oper_Limit_empty
    | doSubSup_oper_Limit_from_to
    | doSubSup_oper_Limit_from
    | doSubSup_oper_Limit_to
    //| doSubSup_oper_Limit_empty
    ;

doOperator
    : doSubSup_oper_Limit
    doPower?
    ;

// opsubsup ou subsup ?

// refactor to extract abs, sqrt
//doUnOper
//    : (
//        ( TK_abs  | TK_sqrt |  TK_intd )
//        | doGlyphSpecial_unary
//        | ( TK_nroot doPower )
//        | (
//             ( TK_neg | TK_fact | TK_minusplus | TK_plusminus | TK_plus | TK_minus )
//             doOpSubSup?
//          )
//    )
//    doPower
//    ;
//

doUnOper_abs
    : TK_abs  doPower;

doUnOper_sqrt
    : TK_sqrt doPower;

doUnOper_nroot
    : TK_nroot doPower doPower;

// should be ?
//doUnOper_fact
//    : TK_fact doOpSubSup? doPower

doUnOper_fact
    : TK_fact  doPower
    ;

normalUnOperSymbol
    : ( TK_neg /*| TK_fact */| TK_minusplus | TK_plusminus | TK_plus | TK_minus )
    ;
doUnOper
    : doUnOper_abs
    | doUnOper_sqrt
    | doUnOper_nroot
    | doUnOper_fact
    | (
        (
            (  TK_intd )
            | doGlyphSpecial_unary
            | ( normalUnOperSymbol  doOpSubSup? )
         )
        doPower
    )
    ;

// braces
// TODO avoid one token rules

br_lgroup : TK_lgroup ;
br_lparent : TK_lparent ;
br_lbracket : TK_lbracket ;
br_lbrace : TK_lbrace ;
br_ldbracket : TK_ldbracket ;
br_lline : TK_lline ;
br_ldline : TK_ldline ;
br_langle :TK_langle;
br_lfloor :TK_lfloor;
br_lceil :TK_lceil;
br_rgroup : TK_rgroup ;
br_rparent : TK_rparent ;
br_rbracket : TK_rbracket ;
br_rbrace : TK_rbrace ;
br_rdbracket : TK_rdbracket ;
br_rline : TK_rline ;
br_rdline : TK_rdline ;
br_rangle :TK_rangle;
br_rfloor :TK_rfloor;
br_rceil :TK_rceil;

brace
    : ( br_lparent doBracebody? br_rparent )
    | ( br_lbracket doBracebody? br_rbracket )
    | ( br_lbrace doBracebody? br_rbrace )
    | ( br_ldbracket doBracebody? br_rdbracket )
    | ( br_lline doBracebody? br_rline )
    | ( br_ldline doBracebody? br_rdline )
    | ( br_langle doBracebody? br_rangle )
    | ( br_lfloor doBracebody? br_rfloor )
    | ( br_lceil doBracebody? br_rceil )
    ;

braceGroup
    : ( TK_lgroup nospace? doBracebody? TK_rgroup )
    ;


// left right

br_none :
    TK_none
    ;
anyWideBrace
    : br_lgroup | br_rgroup
    | br_lparent | br_rparent
    | br_lbracket | br_rbracket
    | br_lbrace | br_rbrace
    | br_ldbracket | br_rdbracket
    | br_lline | br_rline
    | br_ldline | br_rdline
    | br_langle | br_rangle
    | br_lfloor | br_rfloor
    | br_lceil | br_rceil
    | br_none
    ;

wideBrace
    : TK_left anyWideBrace doBracebody? TK_right anyWideBrace
    /*(
    ( br_lgroup doBracebody? TK_right br_rgroup )
    | ( br_lparent doBracebody? TK_right br_rparent )
    | ( br_lbracket doBracebody? TK_right br_rbracket )
    | ( br_lbrace doBracebody? TK_right br_rbrace )
    | ( br_ldbracket doBracebody? TK_right br_rdbracket )
    | ( br_lline doBracebody? TK_right br_rline )
    | ( br_ldline doBracebody? TK_right br_rdline )
    | ( br_langle doBracebody? TK_right br_rangle )
    | ( br_lfloor doBracebody? TK_right br_rfloor )
    | ( br_lceil doBracebody? TK_right br_rceil )
    )
*/
    ;

doBrace
    : wideBrace | brace
    ;


// TODO mline ????
mline :
    TK_mline
    ;
// simplifiy to dpExpression
// true / false
doBracebody
    : (mline | doExpression)+
    ;

customFunction
    : OTHER+
    ;

knownFunction
    : ( TK_arcosh   | TK_arcoth | TK_arccos | TK_arccot | TK_arcsin | TK_arctan
            | TK_arsinh | TK_artanh | TK_cos | TK_cosh | TK_cot  | TK_coth
            | TK_exp | TK_ln | TK_log | TK_sin | TK_sinh | TK_tan | TK_tanh )
            ;

doFunction
    : ( TK_func customFunction )
    | knownFunction
    ;

binomElement : doSum ;
doBinom
    : TK_binom binomElement binomElement
    ;

// stack
doStackElement
    : doAlign? doExpression
    ;

doStack
    : TK_stack TK_lgroup doStackElement ( TK_pound doStackElement )* TK_rgroup
    ;

// matrix
matrixContent
        : matrixLine ( TK_dpound matrixLine )*
        ;

matrixLine
        : matrixColumn ( TK_pound matrixColumn )*
        ;

matrixColumn
        : doAlign? doExpression
        ;

doMatrix
    : TK_matrix TK_lgroup matrixContent? TK_rgroup
    ;

// user defined identifier
userDefinedIdentifier
    : USER_DEFINED_IDENTIFIER
    ;

doSpecial: userDefinedIdentifier
    ;

// user defined operator
operatorName
    : identifier
    ;
doGlyphSpecial_binary
    : TK_boper  operatorName
    ;
doGlyphSpecial_unary
    : TK_uoper operatorName
    ;

// attributes
attribute_rect
    :  TK_overstrike
    ;

attribute_wide_over
    :  TK_widehat | TK_widetilde | TK_widevec
    ;

attribute_over
    : TK_overline  |  TK_acute | TK_bar | TK_breve | TK_check | TK_circle
    | TK_dddot | TK_ddot | TK_dot
    | TK_grave| TK_hat| TK_tilde| TK_vec
;
attribute_under
    : TK_underline
    ;

attribute
    : attribute_rect | attribute_wide_over | attribute_over | attribute_under
    ;

doAttribut
    : attribute
    ;

// font attributes
doFontAttribut
    : doFontSize |  doFont | doColor | realFontAttribute
    ;
realFontAttribute
    : TK_bold | TK_ital | TK_italic |TK_nbold | TK_nitalic
     | TK_phantom
    ;

fontColor
    : TG_Color
    ;
doColor
    :TK_color fontColor
    ;
fontType
    : TG_Font
    ;
doFont
    : TK_font fontType
    ;
fontsize
    : NUMBER_TYPE_REGULAR
    ;
doFontSize
    : TK_size fontsize
    ;

//
// notes about original starmath parse.cxx source
//
// 1. doSubSup has a parameter in sm code (type)
// -> duplicate function to match the two different params
//
// 2. doTerm has a parameter (boolean)
// used for the 2_2n special case
// -> requires mitigation to keep skiping spaces (see lexer)
//
// 3. doBraceBody has a parameter (boolean)
// used for "left ( * right )" notation
// -> handle cases separately
//