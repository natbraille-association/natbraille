// original start offset
var START_ATTRIBUTE_NAME = "oso";
//  original start offset
var END_ATTRIBUTE_NAME = "oeo";

function findTrParent(e){
    //	console.log(e.tagName);
    if (e.tagName.toUpperCase() !== "TR"){
	let p = e.parentNode;
	if (p){
	    return findTrParent(p);
	}
    }  else {
	return e;
    }
    
}
function getStartStop(e){
    let [a,b] =	[e.getAttribute(START_ATTRIBUTE_NAME),
                 e.getAttribute(END_ATTRIBUTE_NAME)]
    let start = parseInt(a);
    let stop = parseInt(b) + 1;
    return [start,stop]
}
document.body.addEventListener("mouseout",function(e) {
    let [start,stop] = getStartStop(e.target);

    if (!(isNaN(start) || isNaN(stop) )){
	let related = findTrParent(e.target);
	if (related){
	    let starMath = related.querySelector(".starmath");
	    let text = starMath.textContent;
	    
	    starMath.innerHTML = '';

	    let e1 = document.createElement("span");
	    e1.textContent = text
	    starMath.appendChild(e1);
	}
    }
})
document.body.addEventListener("mouseover",function(e) {
    //	console.log(e)
    let [start,stop] = getStartStop(e.target);
    if (!(isNaN(start) || isNaN(stop) )){
	let related = findTrParent(e.target);
	if (related){
	    let starMath = related.querySelector(".starmath");
	    let text = starMath.textContent;
	    let part1 = text.slice(0,start);
	    let part2 = text.slice(start,stop)
	    let part3 = text.slice(stop)
	    
	    starMath.innerHTML = '';

	    let e1 = document.createElement("span");
	    e1.textContent = part1
	    starMath.appendChild(e1);

	    let e2 = document.createElement("span");
	    e2.setAttribute("style","color:red;");
	    e2.textContent = part2
	    starMath.appendChild(e2);

	    let e3 = document.createElement("span");
	    e3.textContent = part3
	    starMath.appendChild(e3);


	    console.log(e.target,start,stop,starMath,text,"====",part1,"====",part2,"====",part3);	
	}
    }
})
