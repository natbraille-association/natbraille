grammar stringliteral;


string_literal_content
        : STRING_LITERAL_CONTENT ;

string_literal
        : ( STRING_DELIM string_literal_content?  STRING_DELIM )
        ;

eol_comment
    : EOL_COMMENT
    ;

all :
    ( special_indentifier | string_literal | eol_comment ) *;

special_identifier
    : '%' ( a-z | [AZ] )+
    ;

STRING_LITERAL_CONTENT
    : ( ~["\\] | '\\"' | '\\\\' )+
    ;
STRING_DELIM:'"';

EOL_COMMENT : '%%' .*? ('\n' | EOF ) ;