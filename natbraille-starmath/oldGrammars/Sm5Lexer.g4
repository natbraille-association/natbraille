lexer grammar Sm5Lexer ;

EOL_COMMENT : EOL_COMMENT_HEAD  EOL_COMMENT_TAIL;
STRING_LITTERAL : STRING_LITTERAL_START  STRING_LITTERAL_CONTENT STRING_LITTERAL_END;

USER_DEFINED_IDENTIFIER
    : '%' (~( [ \t\n\r+-/=#%\\"~><&|(){}[\]^_] | '*' ))+
    ;
EOL_COMMENT_HEAD
    : '%%' -> pushMode(EOL_COMMENT_MODE)
    ;
STRING_LITTERAL_START
    : '"' -> pushMode(STRING_LITTERAL_MODE)
    ;

// NUMBER_1 : /*NUMBER_TYPE_1 | NUMBER_TYPE_2 | NUMBER_TYPE_3 | */
// ;
NUMBER_TYPE_REGULAR :
    (    DIGIT+ ( '.' DIGIT? )? )
    |  ('.' DIGIT+)
    ;
NUMBER_TYPE_COMMA:
    (    DIGIT+ ( ',' DIGIT? )? )
    |  (',' DIGIT+)
    ;// fragment NUMBER_TYPE_1 : '.' ( DIGIT | ',')+ ;
// fragment NUMBER_TYPE_2 : ',' DIGIT+ ;
// fragment NUMBER_TYPE_3 : DIGIT ( DIGIT | ',')* ( '.' ( DIGIT | ',')+)? ;


fragment DIGIT : [0123456789] ;



mode EOL_COMMENT_MODE ;

EOL_COMMENT_TAIL :
    ~[\n\r]* ( EOF | [\n\r]? ) -> popMode
    ;

mode STRING_LITTERAL_MODE ;

STRING_LITTERAL_END :
    '"'  -> popMode;

STRING_LITTERAL_CONTENT :
    (~["\\] | ESCAPED )* ;

fragment ESCAPED : '\\' . ;

mode DEFAULT_MODE;



/*
 * TG::Standalone
 */
// TG_Standalone   : TK_Im   | TK_Re   | TK_aleph   | TK_backepsilon   | TK_circ   | TK_dlarrow   | TK_dlrarrow   | TK_dotsaxis   | TK_dotsdiag   | TK_dotsdown   | TK_dotslow   | TK_dotsup   | TK_dotsvert   | TK_downarrow   | TK_drarrow   | TK_emptyset   | TK_exists   | TK_notexists   | TK_forall   | TK_hbar   | TK_infinity   | TK_infty   | TK_lambdabar   | TK_leftarrow   | TK_nabla   | TK_nospace   | TK_partial   | TK_rightarrow   | TK_setC   | TK_setN   | TK_setQ   | TK_setR   | TK_setZ   | TK_uparrow   | TK_wp ;
TK_Im           : 'Im'         ; // Im; TIM; MS_IM; TG::Standalone; 5
TK_Re           : 'Re'         ; // Re; TRE; MS_RE; TG::Standalone; 5
TK_aleph        : 'aleph'      ; // aleph; TALEPH; MS_ALEPH; TG::Standalone; 5
TK_backepsilon  : 'backepsilon' ; // backepsilon; TBACKEPSILON; MS_BACKEPSILON; TG::Standalone; 5
TK_circ         : 'circ'       ; // circ; TCIRC; MS_CIRC; TG::Standalone; 5
TK_dlarrow      : 'dlarrow'    ; // dlarrow; TDLARROW; MS_DLARROW; TG::Standalone; 5
TK_dlrarrow     : 'dlrarrow'   ; // dlrarrow; TDLRARROW; MS_DLRARROW; TG::Standalone; 5
TK_dotsaxis     : 'dotsaxis'   ; // dotsaxis; TDOTSAXIS; MS_DOTSAXIS; TG::Standalone; 5
TK_dotsdiag     : 'dotsdiag'   ; // dotsdiag; TDOTSDIAG; MS_DOTSUP; TG::Standalone; 5
TK_dotsdown     : 'dotsdown'   ; // dotsdown; TDOTSDOWN; MS_DOTSDOWN; TG::Standalone; 5
TK_dotslow      : 'dotslow'    ; // dotslow; TDOTSLOW; MS_DOTSLOW; TG::Standalone; 5
TK_dotsup       : 'dotsup'     ; // dotsup; TDOTSUP; MS_DOTSUP; TG::Standalone; 5
TK_dotsvert     : 'dotsvert'   ; // dotsvert; TDOTSVERT; MS_DOTSVERT; TG::Standalone; 5
TK_downarrow    : 'downarrow'  ; // downarrow; TDOWNARROW; MS_DOWNARROW; TG::Standalone; 5
TK_drarrow      : 'drarrow'    ; // drarrow; TDRARROW; MS_DRARROW; TG::Standalone; 5
TK_emptyset     : 'emptyset'   ; // emptyset; TEMPTYSET; MS_EMPTYSET; TG::Standalone; 5
TK_exists       : 'exists'     ; // exists; TEXISTS; MS_EXISTS; TG::Standalone; 5
TK_notexists    : 'notexists'  ; // notexists; TNOTEXISTS; MS_NOTEXISTS; TG::Standalone; 5
TK_forall       : 'forall'     ; // forall; TFORALL; MS_FORALL; TG::Standalone; 5
TK_hbar         : 'hbar'       ; // hbar; THBAR; MS_HBAR; TG::Standalone; 5
TK_infinity     : 'infinity'   ; // infinity; TINFINITY; MS_INFINITY; TG::Standalone; 5
TK_infty        : 'infty'      ; // infty; TINFINITY; MS_INFINITY; TG::Standalone; 5
TK_lambdabar    : 'lambdabar'  ; // lambdabar; TLAMBDABAR; MS_LAMBDABAR; TG::Standalone; 5
TK_leftarrow    : 'leftarrow' | '<-'  ; // leftarrow; TLEFTARROW; MS_LEFTARROW; TG::Standalone; 5
TK_nabla        : 'nabla'      ; // nabla; TNABLA; MS_NABLA; TG::Standalone; 5
TK_nospace      : 'nospace'    ; // nospace; TNOSPACE; ''; TG::Standalone; 5
TK_partial      : 'partial'    ; // partial; TPARTIAL; MS_PARTIAL; TG::Standalone; 5
TK_rightarrow   : 'rightarrow' | '->' ; // rightarrow; TRIGHTARROW; MS_RIGHTARROW; TG::Standalone; 5
TK_setC         : 'setC'       ; // setC; TSETC; MS_SETC; TG::Standalone; 5
TK_setN         : 'setN'       ; // setN; TSETN; MS_SETN; TG::Standalone; 5
TK_setQ         : 'setQ'       ; // setQ; TSETQ; MS_SETQ; TG::Standalone; 5
TK_setR         : 'setR'       ; // setR; TSETR; MS_SETR; TG::Standalone; 5
TK_setZ         : 'setZ'       ; // setZ; TSETZ; MS_SETZ; TG::Standalone; 5
TK_uparrow      : 'uparrow'    ; // uparrow; TUPARROW; MS_UPARROW; TG::Standalone; 5
TK_wp           : 'wp'         ; // wp; TWP; MS_WP; TG::Standalone; 5



/*
 * TG::UnOper
 */
// TG_UnOper   : TK_abs   | TK_fact   | TK_intd   | TK_neg   | TK_nroot   | TK_sqrt   | TK_uoper ;
TK_abs          : 'abs'        ; // abs; TABS; ''; TG::UnOper; 13
TK_fact         : 'fact'       ; // fact; TFACT; MS_FACT; TG::UnOper; 5
TK_intd         : 'intd'       ; // intd; TINTD; MS_INT; TG::UnOper; 5
TK_neg          : 'neg'        ; // neg; TNEG; MS_NEG; TG::UnOper; 5
TK_nroot        : 'nroot'      ; // nroot; TNROOT; MS_SQRT; TG::UnOper; 5
TK_sqrt         : 'sqrt'       ; // sqrt; TSQRT; MS_SQRT; TG::UnOper; 5
TK_uoper        : 'uoper'      ; // uoper; TUOPER; ''; TG::UnOper; 5

/*
 * TG::Function
 */
// TG_Function   : TK_arcosh   | TK_arcoth   | TK_arccos   | TK_arccot   | TK_arcsin   | TK_arctan   | TK_arsinh   | TK_artanh   | TK_cos   | TK_cosh   | TK_cot   | TK_coth   | TK_exp   | TK_func   | TK_ln   | TK_log   | TK_sin   | TK_sinh   | TK_tan   | TK_tanh ;
TK_arcosh       : 'arcosh'     ; // arcosh; TACOSH; ''; TG::Function; 5
TK_arcoth       : 'arcoth'     ; // arcoth; TACOTH; ''; TG::Function; 5
TK_arccos       : 'arccos'     ; // arccos; TACOS; ''; TG::Function; 5
TK_arccot       : 'arccot'     ; // arccot; TACOT; ''; TG::Function; 5
TK_arcsin       : 'arcsin'     ; // arcsin; TASIN; ''; TG::Function; 5
TK_arctan       : 'arctan'     ; // arctan; TATAN; ''; TG::Function; 5
TK_arsinh       : 'arsinh'     ; // arsinh; TASINH; ''; TG::Function; 5
TK_artanh       : 'artanh'     ; // artanh; TATANH; ''; TG::Function; 5
TK_cos          : 'cos'        ; // cos; TCOS; ''; TG::Function; 5
TK_cosh         : 'cosh'       ; // cosh; TCOSH; ''; TG::Function; 5
TK_cot          : 'cot'        ; // cot; TCOT; ''; TG::Function; 5
TK_coth         : 'coth'       ; // coth; TCOTH; ''; TG::Function; 5
TK_exp          : 'exp'        ; // exp; TEXP; ''; TG::Function; 5
TK_func         : 'func'       ; // func; TFUNC; ''; TG::Function; 5
TK_ln           : 'ln'         ; // ln; TLN; ''; TG::Function; 5
TK_log          : 'log'        ; // log; TLOG; ''; TG::Function; 5
TK_sin          : 'sin'        ; // sin; TSIN; ''; TG::Function; 5
TK_sinh         : 'sinh'       ; // sinh; TSINH; ''; TG::Function; 5
TK_tan          : 'tan'        ; // tan; TTAN; ''; TG::Function; 5
TK_tanh         : 'tanh'       ; // tanh; TTANH; ''; TG::Function; 5

/*
 * TG::Attribute
 */
TG_Attribute   : TK_acute   | TK_bar   | TK_breve   | TK_check   | TK_circle   | TK_dddot   | TK_ddot   | TK_dot   | TK_grave   | TK_hat   | TK_overline   | TK_overstrike   | TK_tilde   | TK_underline   | TK_vec   | TK_widehat   | TK_widetilde   | TK_widevec ;
TK_acute        : 'acute'      ; // acute; TACUTE; MS_ACUTE; TG::Attribute; 5
TK_bar          : 'bar'        ; // bar; TBAR; MS_BAR; TG::Attribute; 5
TK_breve        : 'breve'      ; // breve; TBREVE; MS_BREVE; TG::Attribute; 5
TK_check        : 'check'      ; // check; TCHECK; MS_CHECK; TG::Attribute; 5
TK_circle       : 'circle'     ; // circle; TCIRCLE; MS_CIRCLE; TG::Attribute; 5
TK_dddot        : 'dddot'      ; // dddot; TDDDOT; MS_DDDOT; TG::Attribute; 5
TK_ddot         : 'ddot'       ; // ddot; TDDOT; MS_DDOT; TG::Attribute; 5
TK_dot          : 'dot'        ; // dot; TDOT; MS_DOT; TG::Attribute; 5
TK_grave        : 'grave'      ; // grave; TGRAVE; MS_GRAVE; TG::Attribute; 5
TK_hat          : 'hat'        ; // hat; THAT; MS_HAT; TG::Attribute; 5
TK_overline     : 'overline'   ; // overline; TOVERLINE; ''; TG::Attribute; 5
TK_overstrike   : 'overstrike' ; // overstrike; TOVERSTRIKE; ''; TG::Attribute; 5
TK_tilde        : 'tilde'      ; // tilde; TTILDE; MS_TILDE; TG::Attribute; 5
TK_underline    : 'underline'  ; // underline; TUNDERLINE; ''; TG::Attribute; 5
TK_vec          : 'vec'        ; // vec; TVEC; MS_VEC; TG::Attribute; 5
TK_widehat      : 'widehat'    ; // widehat; TWIDEHAT; MS_HAT; TG::Attribute; 5
TK_widetilde    : 'widetilde'  ; // widetilde; TWIDETILDE; MS_TILDE; TG::Attribute; 5
TK_widevec      : 'widevec'    ; // widevec; TWIDEVEC; MS_VEC; TG::Attribute; 5

/*
 * TG::Align
 */
TG_Align   : TK_alignb   | TK_alignc   | TK_alignl   | TK_alignm   | TK_alignr   | TK_alignt ;
TK_alignb       : 'alignb'     ; // alignb; TALIGNC; ''; TG::Align; 0
TK_alignc       : 'alignc'     ; // alignc; TALIGNC; ''; TG::Align; 0
TK_alignl       : 'alignl'     ; // alignl; TALIGNL; ''; TG::Align; 0
TK_alignm       : 'alignm'     ; // alignm; TALIGNC; ''; TG::Align; 0
TK_alignr       : 'alignr'     ; // alignr; TALIGNR; ''; TG::Align; 0
TK_alignt       : 'alignt'     ; // alignt; TALIGNC; ''; TG::Align; 0

/*
 * TG::Product
 */
// TG_Product   : TK_and   | TK_boper   | TK_bslash   | TK_cdot   | TK_div   | TK_intersection   | TK_odivide   | TK_odot   | TK_otimes   | TK_over   | TK_overbrace   | TK_setminus   | TK_slash   | TK_times   | TK_underbrace   | TK_widebslash   | TK_wideslash ;
TK_and          : 'and' | '&'       ; // and; TAND; MS_AND; TG::Product; 0
TK_boper        : 'boper'      ; // boper; TBOPER; ''; TG::Product; 0
TK_bslash       : 'bslash'     ; // bslash; TBACKSLASH; MS_BACKSLASH; TG::Product; 0
TK_cdot         : 'cdot'       ; // cdot; TCDOT; MS_CDOT; TG::Product; 0
TK_div          : 'div'        ; // div; TDIV; MS_DIV; TG::Product; 0
TK_intersection : 'intersection' ; // intersection; TINTERSECT; MS_INTERSECT; TG::Product; 0
TK_odivide      : 'odivide'    ; // odivide; TODIVIDE; MS_ODIVIDE; TG::Product; 0
TK_odot         : 'odot'       ; // odot; TODOT; MS_ODOT; TG::Product; 0
TK_otimes       : 'otimes'     ; // otimes; TOTIMES; MS_OTIMES; TG::Product; 0
TK_over         : 'over'       ; // over; TOVER; ''; TG::Product; 0
TK_overbrace    : 'overbrace'  ; // overbrace; TOVERBRACE; MS_OVERBRACE; TG::Product; 5
TK_setminus     : 'setminus'   ; // setminus; TBACKSLASH; MS_BACKSLASH; TG::Product; 0
TK_slash        : 'slash'      ; // slash; TSLASH; MS_SLASH; TG::Product; 0
TK_times        : 'times'      ; // times; TTIMES; MS_TIMES; TG::Product; 0
TK_underbrace   : 'underbrace' ; // underbrace; TUNDERBRACE; MS_UNDERBRACE; TG::Product; 5
TK_widebslash   : 'widebslash' ; // widebslash; TWIDEBACKSLASH; MS_BACKSLASH; TG::Product; 0
TK_wideslash    : 'wideslash'  ; // wideslash; TWIDESLASH; MS_SLASH; TG::Product; 0
TK_divideby     : '/' ;
/*
--
                    {
                        m_aCurToken.eType    = TDIVIDEBY;
                        m_aCurToken.cMathChar = MS_SLASH;
                        m_aCurToken.nGroup       = TG::Product;
                        m_aCurToken.nLevel       = 0;
                        m_aCurToken.aText = "/";
                    }
*/
TK_multiply     : '*' ;
/*
--
                    {
                        m_aCurToken.eType    = TMULTIPLY;
                        m_aCurToken.cMathChar = MS_MULTIPLY;
                        m_aCurToken.nGroup       = TG::Product;
                        m_aCurToken.nLevel       = 0;
                        m_aCurToken.aText = "*";
                    }
*/

/*
 * TG::Relation
 */
// TG_Relation   : TK_approx   | TK_def   | TK_divides   | TK_equiv   | TK_ge   | TK_geslant   | TK_gg   | TK_gt   | TK_in   | TK_le   | TK_leslant   | TK_ll   | TK_lt   | TK_ndivides   | TK_neq   | TK_ni   | TK_notin   | TK_nsubset   | TK_nsupset   | TK_nsubseteq   | TK_nsupseteq   | TK_ortho   | TK_owns   | TK_parallel   | TK_prec   | TK_preccurlyeq   | TK_precsim   | TK_nprec   | TK_prop   | TK_sim   | TK_simeq   | TK_subset   | TK_succ   | TK_succcurlyeq   | TK_succsim   | TK_nsucc   | TK_subseteq   | TK_supset   | TK_supseteq   | TK_toward   | TK_transl   | TK_transr ;
TK_approx       : 'approx'     ; // approx; TAPPROX; MS_APPROX; TG::Relation; 0
TK_def          : 'def'        ; // def; TDEF; MS_DEF; TG::Relation; 0
TK_divides      : 'divides'    ; // divides; TDIVIDES; MS_LINE; TG::Relation; 0
TK_equiv        : 'equiv'      ; // equiv; TEQUIV; MS_EQUIV; TG::Relation; 0
TK_ge           : 'ge' | '>='  ; // ge; TGE; MS_GE; TG::Relation; 0
TK_geslant      : 'geslant'    ; // geslant; TGESLANT; MS_GESLANT; TG::Relation; 0
TK_gg           : 'gg' | '>>'  ; // gg; TGG; MS_GG; TG::Relation; 0
TK_gt           : 'gt' | '>'   ; // gt; TGT; MS_GT; TG::Relation; 0
TK_in           : 'in'         ; // in; TIN; MS_IN; TG::Relation; 0
TK_le           : 'le' | '<='  ; // le; TLE; MS_LE; TG::Relation; 0
TK_leslant      : 'leslant'    ; // leslant; TLESLANT; MS_LESLANT; TG::Relation; 0
TK_ll           : 'll' | '<<'  ; // ll; TLL; MS_LL; TG::Relation; 0
TK_lt           : 'lt' | '<'   ; // lt; TLT; MS_LT; TG::Relation; 0
TK_ndivides     : 'ndivides'   ; // ndivides; TNDIVIDES; MS_NDIVIDES; TG::Relation; 0
TK_neq          : 'neq'| '<>'  ; // neq; TNEQ; MS_NEQ; TG::Relation; 0
TK_ni           : 'ni'         ; // ni; TNI; MS_NI; TG::Relation; 0
TK_notin        : 'notin'      ; // notin; TNOTIN; MS_NOTIN; TG::Relation; 0
TK_nsubset      : 'nsubset'    ; // nsubset; TNSUBSET; MS_NSUBSET; TG::Relation; 0
TK_nsupset      : 'nsupset'    ; // nsupset; TNSUPSET; MS_NSUPSET; TG::Relation; 0
TK_nsubseteq    : 'nsubseteq'  ; // nsubseteq; TNSUBSETEQ; MS_NSUBSETEQ; TG::Relation; 0
TK_nsupseteq    : 'nsupseteq'  ; // nsupseteq; TNSUPSETEQ; MS_NSUPSETEQ; TG::Relation; 0
TK_ortho        : 'ortho'      ; // ortho; TORTHO; MS_ORTHO; TG::Relation; 0
TK_owns         : 'owns'       ; // owns; TNI; MS_NI; TG::Relation; 0
TK_parallel     : 'parallel'   ; // parallel; TPARALLEL; MS_DLINE; TG::Relation; 0
TK_prec         : 'prec'       ; // prec; TPRECEDES; MS_PRECEDES; TG::Relation; 0
TK_preccurlyeq  : 'preccurlyeq' ; // preccurlyeq; TPRECEDESEQUAL; MS_PRECEDESEQUAL; TG::Relation; 0
TK_precsim      : 'precsim'    ; // precsim; TPRECEDESEQUIV; MS_PRECEDESEQUIV; TG::Relation; 0
TK_nprec        : 'nprec'      ; // nprec; TNOTPRECEDES; MS_NOTPRECEDES; TG::Relation; 0
TK_prop         : 'prop'       ; // prop; TPROP; MS_PROP; TG::Relation; 0
TK_sim          : 'sim'        ; // sim; TSIM; MS_SIM; TG::Relation; 0
TK_simeq        : 'simeq'      ; // simeq; TSIMEQ; MS_SIMEQ; TG::Relation; 0
TK_subset       : 'subset'     ; // subset; TSUBSET; MS_SUBSET; TG::Relation; 0
TK_succ         : 'succ'       ; // succ; TSUCCEEDS; MS_SUCCEEDS; TG::Relation; 0
TK_succcurlyeq  : 'succcurlyeq' ; // succcurlyeq; TSUCCEEDSEQUAL; MS_SUCCEEDSEQUAL; TG::Relation; 0
TK_succsim      : 'succsim'    ; // succsim; TSUCCEEDSEQUIV; MS_SUCCEEDSEQUIV; TG::Relation; 0
TK_nsucc        : 'nsucc'      ; // nsucc; TNOTSUCCEEDS; MS_NOTSUCCEEDS; TG::Relation; 0
TK_subseteq     : 'subseteq'   ; // subseteq; TSUBSETEQ; MS_SUBSETEQ; TG::Relation; 0
TK_supset       : 'supset'     ; // supset; TSUPSET; MS_SUPSET; TG::Relation; 0
TK_supseteq     : 'supseteq'   ; // supseteq; TSUPSETEQ; MS_SUPSETEQ; TG::Relation; 0
TK_toward       : 'toward'     ; // toward; TTOWARD; MS_RIGHTARROW; TG::Relation; 0
TK_transl       : 'transl'     ; // transl; TTRANSL; MS_TRANSL; TG::Relation; 0
TK_transr       : 'transr'     ; // transr; TTRANSR; MS_TRANSR; TG::Relation; 0
TK_assign       : '=' ;
/*
--
                    {
                        m_aCurToken.eType    = TASSIGN;
                        m_aCurToken.cMathChar = MS_ASSIGN;
                        m_aCurToken.nGroup       = TG::Relation;
                        m_aCurToken.nLevel       = 0;
                        m_aCurToken.aText = "=";
                    }

*/


/*
 * TG::Color
 */
TG_Color   : TK_aqua   | TK_black   | TK_blue   | TK_cyan   | TK_fuchsia   | TK_gray   | TK_green   | TK_lime   | TK_magenta   | TK_maroon   | TK_navy   | TK_olive   | TK_purple   | TK_red   | TK_silver   | TK_teal   | TK_white   | TK_yellow ;
TK_aqua         : 'aqua'       ; // aqua; TAQUA; ''; TG::Color; 0
TK_black        : 'black'      ; // black; TBLACK; ''; TG::Color; 0
TK_blue         : 'blue'       ; // blue; TBLUE; ''; TG::Color; 0
TK_cyan         : 'cyan'       ; // cyan; TCYAN; ''; TG::Color; 0
TK_fuchsia      : 'fuchsia'    ; // fuchsia; TFUCHSIA; ''; TG::Color; 0
TK_gray         : 'gray'       ; // gray; TGRAY; ''; TG::Color; 0
TK_green        : 'green'      ; // green; TGREEN; ''; TG::Color; 0
TK_lime         : 'lime'       ; // lime; TLIME; ''; TG::Color; 0
TK_magenta      : 'magenta'    ; // magenta; TMAGENTA; ''; TG::Color; 0
TK_maroon       : 'maroon'     ; // maroon; TMAROON; ''; TG::Color; 0
TK_navy         : 'navy'       ; // navy; TNAVY; ''; TG::Color; 0
TK_olive        : 'olive'      ; // olive; TOLIVE; ''; TG::Color; 0
TK_purple       : 'purple'     ; // purple; TPURPLE; ''; TG::Color; 0
TK_red          : 'red'        ; // red; TRED; ''; TG::Color; 0
TK_silver       : 'silver'     ; // silver; TSILVER; ''; TG::Color; 0
TK_teal         : 'teal'       ; // teal; TTEAL; ''; TG::Color; 0
TK_white        : 'white'      ; // white; TWHITE; ''; TG::Color; 0
TK_yellow       : 'yellow'     ; // yellow; TYELLOW; ''; TG::Color; 0

/*
 * TG::NONE
 */
// TG_NONE   : TK_binom   | TK_left   | TK_matrix   | TK_mline   | TK_newline   | TK_right   | TK_stack ;
TK_binom        : 'binom'      ; // binom; TBINOM; ''; TG::NONE; 5
TK_left         : 'left'       ; // left; TLEFT; ''; TG::NONE; 5
TK_matrix       : 'matrix'     ; // matrix; TMATRIX; ''; TG::NONE; 5
TK_mline        : 'mline'      ; // mline; TMLINE; MS_VERTLINE; TG::NONE; 0
TK_newline      : 'newline'    ; // newline; TNEWLINE; ''; TG::NONE; 0
TK_right        : 'right'      ; // right; TRIGHT; ''; TG::NONE; 0
TK_stack        : 'stack'      ; // stack; TSTACK; ''; TG::NONE; 5
/*
* PLACEHOLDER
*/
TK_PLACE : '<?>';
/*
--
                        {
                            m_aCurToken.eType    = TPLACE;
                            m_aCurToken.cMathChar = MS_PLACE;
                            m_aCurToken.nGroup       = TG::NONE;
                            m_aCurToken.nLevel       = 5;
                            m_aCurToken.aText = "<?>";

*/
/*
 * stack & matrix separator
 */
TK_dpound : '##' ;
/*

                        {
                            m_aCurToken.eType    = TDPOUND;
                            m_aCurToken.cMathChar = '\0';
                            m_aCurToken.nGroup       = TG::NONE;
                            m_aCurToken.nLevel       = 0;
                            m_aCurToken.aText = "##";

*/
TK_pound : '#' ;
/*
                        {
                            m_aCurToken.eType    = TPOUND;
                            m_aCurToken.cMathChar = '\0';
                            m_aCurToken.nGroup       = TG::NONE;
                            m_aCurToken.nLevel       = 0;
                            m_aCurToken.aText = "#";
                        }
*/

/*
 * TG::FontAttr
 */
// TG_FontAttr   : TK_bold   | TK_color   | TK_font   | TK_ital   | TK_italic   | TK_nbold   | TK_nitalic   | TK_phantom   | TK_size ;
TK_bold         : 'bold'       ; // bold; TBOLD; ''; TG::FontAttr; 5
TK_color        : 'color'      ; // color; TCOLOR; ''; TG::FontAttr; 5
TK_font         : 'font'       ; // font; TFONT; ''; TG::FontAttr; 5
TK_ital         : 'ital'       ; // ital; TITALIC; ''; TG::FontAttr; 5
TK_italic       : 'italic'     ; // italic; TITALIC; ''; TG::FontAttr; 5
TK_nbold        : 'nbold'      ; // nbold; TNBOLD; ''; TG::FontAttr; 5
TK_nitalic      : 'nitalic'    ; // nitalic; TNITALIC; ''; TG::FontAttr; 5
TK_phantom      : 'phantom'    ; // phantom; TPHANTOM; ''; TG::FontAttr; 5
TK_size         : 'size'       ; // size; TSIZE; ''; TG::FontAttr; 5

/*
 * TG::Oper
 */
// TG_Oper   : TK_coprod   | TK_iiint   | TK_iint   | TK_int   | TK_lim   | TK_liminf   | TK_limsup   | TK_lint   | TK_llint   | TK_lllint   | TK_oper   | TK_prod   | TK_sum ;
TK_coprod       : 'coprod'     ; // coprod; TCOPROD; MS_COPROD; TG::Oper; 5
TK_iiint        : 'iiint'      ; // iiint; TIIINT; MS_IIINT; TG::Oper; 5
TK_iint         : 'iint'       ; // iint; TIINT; MS_IINT; TG::Oper; 5
TK_int          : 'int'        ; // int; TINT; MS_INT; TG::Oper; 5
TK_lim          : 'lim'        ; // lim; TLIM; ''; TG::Oper; 5
TK_liminf       : 'liminf'     ; // liminf; TLIMINF; ''; TG::Oper; 5
TK_limsup       : 'limsup'     ; // limsup; TLIMSUP; ''; TG::Oper; 5
TK_lint         : 'lint'       ; // lint; TLINT; MS_LINT; TG::Oper; 5
TK_llint        : 'llint'      ; // llint; TLLINT; MS_LLINT; TG::Oper; 5
TK_lllint       : 'lllint'     ; // lllint; TLLLINT; MS_LLLINT; TG::Oper; 5
TK_oper         : 'oper'       ; // oper; TOPER; ''; TG::Oper; 5
TK_prod         : 'prod'       ; // prod; TPROD; MS_PROD; TG::Oper; 5
TK_sum          : 'sum'        ; // sum; TSUM; MS_SUM; TG::Oper; 5

/*
 * TG::Power
 */
// TG_Power   : TK_csub   | TK_csup   | TK_lsub   | TK_lsup   | TK_rsub   | TK_rsup   | TK_sub   | TK_sup ;
TK_csub         : 'csub'       ; // csub; TCSUB; ''; TG::Power; 0
TK_csup         : 'csup'       ; // csup; TCSUP; ''; TG::Power; 0
TK_lsub         : 'lsub'       ; // lsub; TLSUB; ''; TG::Power; 0
TK_lsup         : 'lsup'       ; // lsup; TLSUP; ''; TG::Power; 0
TK_sub          : 'sub'        ; // sub; TRSUB; ''; TG::Power; 0
//TK_rsub         : 'rsub' | '_' ; // rsub; TRSUB; ''; TG::Power; 0
TK_rsub         : 'rsub'  ; // rsub; TRSUB; ''; TG::Power; 0
TK_sup          : 'sup'        ; // sup; TRSUP; ''; TG::Power; 0
//TK_rsup         : 'rsup' | '^' ; // rsup; TRSUP; ''; TG::Power; 0
TK_rsup         : 'rsup' | '^' ; // rsup; TRSUP; ''; TG::Power; 0

// 2_2n -> 2_{2n}
// does not work : 2_2sin5 -> 2_2 {sin 5}

FUCKEDUP_SUP : '^' FUCKEDUP_CONTENT ;
FUCKEDUP_SUB : '_' FUCKEDUP_CONTENT ;

fragment FUCKEDUP_CONTENT : (~( [ \t\n\r+-/=#%\\"~><&|(){}[\]^_] | '*' ))+;


/*
 * TG::Font
 */
TG_Font   : TK_fixed   | TK_sans   | TK_serif ;
TK_fixed        : 'fixed'      ; // fixed; TFIXED; ''; TG::Font; 0
TK_sans         : 'sans'       ; // sans; TSANS; ''; TG::Font; 0
TK_serif        : 'serif'      ; // serif; TSERIF; ''; TG::Font; 0

/*
 * TG::Limit
 */
// TG_Limit   : TK_from   | TK_to ;
TK_from         : 'from'       ; // from; TFROM; ''; TG::Limit; 0
TK_to           : 'to'         ; // to; TTO; ''; TG::Limit; 0

/*
 * TG::LBrace
 */
// TG_LBrace   : TK_langle   | TK_lbrace   | TK_lceil   | TK_ldbracket   | TK_ldline   | TK_lfloor   | TK_lline ;
TK_langle       : 'langle'     ; // langle; TLANGLE; MS_LMATHANGLE; TG::LBrace; 5
TK_lbrace       : 'lbrace'     ; // lbrace; TLBRACE; MS_LBRACE; TG::LBrace; 5
TK_lceil        : 'lceil'      ; // lceil; TLCEIL; MS_LCEIL; TG::LBrace; 5
TK_ldbracket    : 'ldbracket'  ; // ldbracket; TLDBRACKET; MS_LDBRACKET; TG::LBrace; 5
TK_ldline       : 'ldline'     ; // ldline; TLDLINE; MS_DVERTLINE; TG::LBrace; 5
TK_lfloor       : 'lfloor'     ; // lfloor; TLFLOOR; MS_LFLOOR; TG::LBrace; 5
TK_lline        : 'lline'      ; // lline; TLLINE; MS_VERTLINE; TG::LBrace; 5
TK_lbracket : '[' ;
/*
--
                    {
                        m_aCurToken.eType    = TLBRACKET;
                        m_aCurToken.cMathChar = MS_LBRACKET;
                        m_aCurToken.nGroup       = TG::LBrace;
                        m_aCurToken.nLevel       = 5;
                        m_aCurToken.aText = "[";
                    }
*/
TK_lparent : '(' ;
/*
--
                    {
                        m_aCurToken.eType    = TLPARENT;
                        m_aCurToken.cMathChar = MS_LPARENT;
                        m_aCurToken.nGroup       = TG::LBrace;
                        m_aCurToken.nLevel       = 5;     //! 0 to continue expression
                        m_aCurToken.aText = "(";
                    }
*/


/*
 * TG::RBrace
 */
// TG_RBrace   : TK_rangle   | TK_rbrace   | TK_rceil   | TK_rdbracket   | TK_rdline   | TK_rfloor   | TK_rline ;
TK_rangle       : 'rangle'     ; // rangle; TRANGLE; MS_RMATHANGLE; TG::RBrace; 0
TK_rbrace       : 'rbrace'     ; // rbrace; TRBRACE; MS_RBRACE; TG::RBrace; 0
TK_rceil        : 'rceil'      ; // rceil; TRCEIL; MS_RCEIL; TG::RBrace; 0
TK_rdbracket    : 'rdbracket'  ; // rdbracket; TRDBRACKET; MS_RDBRACKET; TG::RBrace; 0
TK_rdline       : 'rdline'     ; // rdline; TRDLINE; MS_DVERTLINE; TG::RBrace; 0
TK_rfloor       : 'rfloor'     ; // rfloor; TRFLOOR; MS_RFLOOR; TG::RBrace; 0
TK_rline        : 'rline'      ; // rline; TRLINE; MS_VERTLINE; TG::RBrace; 0
TK_rbracket : ']';
/*
--
                    {
                        m_aCurToken.eType    = TRBRACKET;
                        m_aCurToken.cMathChar = MS_RBRACKET;
                        m_aCurToken.nGroup       = TG::RBrace;
                        m_aCurToken.nLevel       = 0;
                        m_aCurToken.aText = "]";
                    }
*/
TK_rparent : ')';
/*
--
                    {
                        m_aCurToken.eType    = TRPARENT;
                        m_aCurToken.cMathChar = MS_RPARENT;
                        m_aCurToken.nGroup       = TG::RBrace;
                        m_aCurToken.nLevel       = 0;     //! 0 to terminate expression
                        m_aCurToken.aText = ")";
                    }
*/

/*
 * TG::LBrace | TG::RBrace
 */
// TG_LBrace | TG::RBrace   : TK_none ;
TK_none         : 'none'       ; // none; TNONE; ''; TG::LBrace | TG::RBrace; 0


/*
 * TG::UnOper | TG::Sum
 */
// TG_UnOper | TG::Sum   : TK_minusplus   | TK_plusminus ;
TK_minusplus    : 'minusplus' | '+-'  ; // minusplus; TMINUSPLUS; MS_MINUSPLUS; TG::UnOper | TG::Sum; 5
TK_plusminus    : 'plusminus' | '-+' ; // plusminus; TPLUSMINUS; MS_PLUSMINUS; TG::UnOper | TG::Sum; 5
TK_plus : '+' ;
/*
--
                        {
                            m_aCurToken.eType    = TPLUS;
                            m_aCurToken.cMathChar = MS_PLUS;
                            m_aCurToken.nGroup       = TG::UnOper | TG::Sum;
                            m_aCurToken.nLevel       = 5;
                            m_aCurToken.aText = "+";
                        }
*/
TK_minus : '-' ;
/*
--
                        {
                            m_aCurToken.eType    = TMINUS;
                            m_aCurToken.cMathChar = MS_MINUS;
                            m_aCurToken.nGroup       = TG::UnOper | TG::Sum;
                            m_aCurToken.nLevel       = 5;
                            m_aCurToken.aText = "-";
                        }
*/


/*
 * TG::Sum
 */
// TG_Sum   : TK_ominus   | TK_oplus   | TK_or   | TK_union ;
TK_ominus       : 'ominus'     ; // ominus; TOMINUS; MS_OMINUS; TG::Sum; 0
TK_oplus        : 'oplus'      ; // oplus; TOPLUS; MS_OPLUS; TG::Sum; 0
TK_or           : 'or' | '|'   ; // or; TOR; MS_OR; TG::Sum; 0
TK_union        : 'union'      ; // union; TUNION; MS_UNION; TG::Sum; 0


/*
 * TG::Blank
 */
TK_blank : '`' | '#~' ;
/*
--
                    {
                        m_aCurToken.eType    = TSBLANK;
                        m_aCurToken.cMathChar = '\0';
                        m_aCurToken.nGroup       = TG::Blank;
                        m_aCurToken.nLevel       = 5;
                        m_aCurToken.aText = "`";
                    }
*/
/*
--
                    {
                        m_aCurToken.eType    = TBLANK;
                        m_aCurToken.cMathChar = '\0';
                        m_aCurToken.nGroup       = TG::Blank;
                        m_aCurToken.nLevel       = 5;
                        m_aCurToken.aText = "~";
                    }
*/


/*

*/

/*
 * group
 */
TK_lgroup : '{';
/*
--
                    {
                        m_aCurToken.eType    = TLGROUP;
                        m_aCurToken.cMathChar = MS_LBRACE;
                        m_aCurToken.nGroup       = TG::NONE;
                        m_aCurToken.nLevel       = 5;
                        m_aCurToken.aText = "{";
                    }
*/
TK_rgroup : '}';
/*
--
                    {
                        m_aCurToken.eType    = TRGROUP;
                        m_aCurToken.cMathChar = MS_RBRACE;
                        m_aCurToken.nGroup       = TG::NONE;
                        m_aCurToken.nLevel       = 0;
                        m_aCurToken.aText = "}";
                    }
*/









//////////////////////
/////////////////////

/*
--
                            // will be treated as numbers
                            m_aCurToken.eType     = TNUMBER;
                            m_aCurToken.cMathChar = '\0';
                            m_aCurToken.nGroup    = TG::NONE;
                            m_aCurToken.nLevel    = 5;

--

                            m_aCurToken.aText = m_aBufferString.copy( nTxtStart, m_nBufferIndex - nTxtStart );
                            aRes.EndPos = m_nBufferIndex;
*/

/*
--
                        // character
                        m_aCurToken.eType      = TTEXT;
                        m_aCurToken.cMathChar  = '\0';
                        m_aCurToken.nGroup     = TG::NONE;
                        m_aCurToken.nLevel     = 5;
                        m_aCurToken.aText      ="%";
                        m_aCurToken.nRow       = m_Row;
                        m_aCurToken.nCol       = nTmpStart - m_nColOff;

*/
/*
--
                            sal_Int32 n = aTmpRes.EndPos - nTmpStart;
                            m_aCurToken.eType      = TSPECIAL;
                            m_aCurToken.aText      = m_aBufferString.copy( nTmpStart-1, n+1 );

*/
TK_escape : '\\'
;
/*
--
                    {
                        m_aCurToken.eType    = TESCAPE;
                        m_aCurToken.cMathChar = '\0';
                        m_aCurToken.nGroup       = TG::NONE;
                        m_aCurToken.nLevel       = 5;
                        m_aCurToken.aText = "\\";
                    }
*/


OTHER : ~[\t ];
SPACE : [\t ] -> skip;


