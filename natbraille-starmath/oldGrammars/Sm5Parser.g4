parser grammar Sm5Parser ;

options { tokenVocab=Sm5Lexer; }


// eol_comment : EOL_COMMENT ;
string_litteral : STRING_LITTERAL;
// user_defined_identifier : USER_DEFINED_IDENTIFIER ;
number : NUMBER_TYPE_COMMA | NUMBER_TYPE_REGULAR;
identifier : OTHER+;
/*
main
        : ( eol_comment | string_litteral | user_defined_identifier | identifier
        | number
        | identifier )*
        ;
*/
doTable :
     TK_newline?  doLine ( TK_newline doLine )* EOF
    ;

doLine
    : doExpression+
    ;

doAlign
    : TG_Align ;

// REPLACES doALign
doExpression :
    doAlign? doRelation+
    ;


relationOperatorGroup
    : TK_approx   | TK_def   | TK_divides   | TK_equiv
    | TK_ge   | TK_geslant   | TK_gg   | TK_gt   | TK_in
    | TK_le   | TK_leslant   | TK_ll   | TK_lt   | TK_ndivides
    | TK_neq   | TK_ni   | TK_notin   | TK_nsubset   | TK_nsupset
    | TK_nsubseteq   | TK_nsupseteq   | TK_ortho   | TK_owns
    | TK_parallel   | TK_prec   | TK_preccurlyeq   | TK_precsim
    | TK_nprec   | TK_prop   | TK_sim   | TK_simeq   | TK_subset
    | TK_succ   | TK_succcurlyeq   | TK_succsim   | TK_nsucc
    | TK_subseteq   | TK_supset   | TK_supseteq   | TK_toward
    | TK_transl   | TK_transr | TK_assign
    ;

// opsupsup -> powre ?
doRelation :
    doSum (relationOperatorGroup doOpSubSup? doSum )*
    ;


sumOperatorGroup
    : TK_plus | TK_minus | TK_ominus | TK_oplus | TK_or | TK_union
    ;

// opsupsup -> powre ?
doSum :
    doProduct ( sumOperatorGroup doOpSubSup? doProduct)*
    ;

// ?
productOperatorGroup
    : doGlyphSpecial_binary
    | TK_and | TK_bslash | TK_cdot | TK_div | TK_intersection
    | TK_odivide  | TK_odot  |  TK_otimes | TK_over | TK_overbrace
    | TK_setminus | TK_slash | TK_times | TK_underbrace | TK_widebslash
    | TK_wideslash | TK_divideby | TK_multiply
    ;

doProduct :
    doPower ( productOperatorGroup doPower )*
    ;
// ?

subsupOperatorGroup
    : TK_csub | TK_csup | TK_lsub | TK_lsup | TK_sub
    | TK_rsub | TK_sup | TK_rsup
    ;

// (TG nActiveGroup)
// from, to         -> TG::Limit
// csup, csub, etc. -> TG::power

doSubSup_Limit
    : ( TK_from doRelation )
      | ( TK_to doRelation )
    ;

// the x^2n32132dvlkjdlvkj
// only in case of power, doterm(true)
// is replaced by fuckedIdentifier
// does not perfectly match staroffice (see lexer)
// + subparsing must be done in visitor
fuckedup_sub : FUCKEDUP_SUB;
fuckedup_sup : FUCKEDUP_SUP;

doSubSup_Power
    : fuckedup_sub | fuckedup_sup
    | ( subsupOperatorGroup doTerm(/*true*/) )+
    ;

// nullable
doOpSubSup :
    doSubSup_Power doPower?
    ;

doPower :
    doTerm /* term(false) */
    doSubSup_Power?
    ;

doBlank
    : TK_blank+
    ;


// TK_toward is relational
// SmMathSymbolNode
standaloneSymbol
    : TK_leftarrow  | TK_rightarrow | TK_uparrow | TK_downarrow
    | TK_circ
    | TK_dlrarrow | TK_dlarrow | TK_drarrow
    | TK_exists | TK_notexists
    | TK_forall | TK_partial  | TK_nabla
    | TK_dotsaxis  | TK_dotsdiag | TK_dotsdown | TK_dotslow  | TK_dotsup | TK_dotsvert
    | TK_toward
    ;

// SmMathIdentifierNode
standaloneIdentifier
    : TK_setN | TK_setZ   | TK_setQ   | TK_setR     |  TK_setC
    | TK_hbar | TK_lambdabar  | TK_backepsilon | TK_aleph
    | TK_Im | TK_Re
    | TK_wp
    | TK_emptyset
    | TK_infinity | TK_infty
    /*  TK_nospace   ?? */
    ;

placeholder : TK_PLACE;

// special_identifier_number
//    : (OTHER | NUMBER_TYPE_COMMA | NUMBER_TYPE_REGULAR)+
//    ;
// true / false ( 2_3n feature )
// TLGROUP
doTerm :
    doEscape
    /* nospace */
    /* tlgroup empty ? */
    | doBrace
    | doBlank
    | string_litteral
    /* tchar */
    /* tident */
    /* tnumber */
    // | special_identifier_number
    | identifier
    | number
    | standaloneSymbol
    | standaloneIdentifier
    | placeholder
    | doSpecial
    | doBinom
    | doStack
    | doMatrix
    | doBrace
    | doOperator
    | doUnOper
    | ( doAttribut | doFontAttribut )+ doPower?
    | doFunction
    ;

escaped
    : TK_lgroup | TK_rgroup | TK_lparent | TK_rparent | TK_lbracket | TK_rbracket
    | TK_lbrace | TK_rbrace | TK_ldbracket | TK_rdbracket | TK_lline | TK_rline
    | TK_ldline | TK_rdline | TK_langle | TK_rangle | TK_lfloor | TK_rfloor
    | TK_lceil | TK_rceil
;

doEscape
    :  TK_escape escaped
    ;

// operator is sums, lims, etc.
operatorOperatorGroup
    : TK_sum  | TK_prod | TK_coprod
    | TK_iiint | TK_iint | TK_int
    | TK_lint | TK_llint | TK_lllint
    | TK_lim | TK_liminf | TK_limsup
;

/* seems to accept TK_oper identifier */
/* but : 'oper alpha from a to b c' -> shown in editor / mathml do not match */
customOperator
    : TK_oper userDefinedIdentifier
    ;

doOper
    : customOperator | operatorOperatorGroup
    ;

doOperator
    : doOper
    doSubSup_Limit*
    doPower?
    ;

// opsubsup ou subsup ?
doUnOper
    : (
        ( TK_abs  | TK_sqrt |  TK_intd )
        | doGlyphSpecial_unary
        | ( TK_nroot doPower )
        | (
             ( TK_neg | TK_fact | TK_minusplus | TK_plusminus | TK_plus | TK_minus )
             doOpSubSup?
          )
    )
    doPower
    ;

// braces
brace
    : ( TK_lgroup doBracebody? TK_rgroup )
    | ( TK_lparent doBracebody? TK_rparent )
    | ( TK_lbracket doBracebody? TK_rbracket )
    | ( TK_lbrace doBracebody? TK_rbrace )
    | ( TK_ldbracket doBracebody? TK_rdbracket )
    | ( TK_lline doBracebody? TK_rline )
    | ( TK_ldline doBracebody? TK_rdline )
    | ( TK_langle doBracebody? TK_rangle )
    | ( TK_lfloor doBracebody? TK_rfloor )
    | ( TK_lceil doBracebody? TK_rceil )
    ;

wideBrace
    : ( TK_left TK_lgroup doBracebody? TK_right TK_rgroup )
    | ( TK_left TK_lparent doBracebody? TK_right TK_lparent )
    | ( TK_left TK_lbracket doBracebody? TK_right TK_rbracket )
    | ( TK_left TK_lbrace doBracebody? TK_right TK_rbrace )
    | ( TK_left TK_ldbracket doBracebody? TK_right TK_rdbracket )
    | ( TK_left TK_lline doBracebody? TK_right TK_rline )
    | ( TK_left TK_ldline doBracebody? TK_right TK_rdline )
    | ( TK_left TK_langle doBracebody? TK_right TK_rangle )
    | ( TK_left TK_lfloor doBracebody? TK_right TK_rfloor )
    | ( TK_left TK_lceil doBracebody? TK_right TK_rceil )
    ;

doBrace
    : wideBrace | brace
    ;

// simplifiy to dpExpression
// true / false
doBracebody
    : doExpression
    ;

doFunction
    : ( TK_func identifier )
    | ( TK_arcosh   | TK_arcoth | TK_arccos | TK_arccot | TK_arcsin | TK_arctan
        | TK_arsinh | TK_artanh | TK_cos | TK_cosh | TK_cot  | TK_coth
        | TK_exp | TK_ln | TK_log | TK_sin | TK_sinh | TK_tan | TK_tanh )
    ;

doBinom
    : TK_binom doSum doSum
    ;

// stack
doStackElement
    : doAlign? doExpression
    ;

doStack
    : TK_stack TK_lgroup doStackElement ( TK_pound doStackElement )* TK_rgroup
    ;

// matrix
matrixContent
        : matrixLine ( TK_dpound matrixLine )*
        ;

matrixLine
        : matrixColumn ( TK_pound matrixColumn )*
        ;

matrixColumn
        : doAlign? doExpression
        ;

doMatrix
    : TK_matrix TK_lgroup matrixContent? TK_rgroup
    ;

// user defined identifier
userDefinedIdentifier
    : USER_DEFINED_IDENTIFIER
    ;

doSpecial: userDefinedIdentifier
    ;

// user defined operator
operatorName
    : identifier
    ;
doGlyphSpecial_binary
    : TK_boper  operatorName
    ;
doGlyphSpecial_unary
    : TK_uoper operatorName
    ;

// attributes
attribute_rect
    : TK_overline  | TK_overstrike  | TK_underline
    ;

attribute_wide_over
    :  TK_widehat | TK_widetilde | TK_widevec
    ;

attribute_over
    :   TK_acute | TK_bar | TK_breve | TK_check | TK_circle
    | TK_dddot | TK_ddot | TK_dot
    | TK_grave| TK_hat| TK_tilde| TK_vec
;
attribute
    : attribute_rect | attribute_wide_over | attribute_over
    ;

doAttribut
    : attribute
    ;

// font attributes
doFontAttribut
    : doFontSize |  doFont | doColor
    ;

fontColor
    : TG_Color
    ;
doColor
    :TK_color fontColor
    ;
fontType
    : TG_Font
    ;
doFont
    : TK_font fontType
    ;
fontsize
    : NUMBER_TYPE_REGULAR
    ;
doFontSize
    : TK_size fontsize
    ;

//
// notes about original starmath parse.cxx source
//
// 1. doSubSup has a parameter in sm code (type)
// -> duplicate function to match the two different params
//
// 2. doTerm has a parameter (boolean)
// used for the 2_2n special case
// -> requires mitigation to keep skiping spaces (see lexer)
//
// 3. doBraceBody has a parameter (boolean)
// used for "left ( * right )" notation
// -> handle cases separately
//