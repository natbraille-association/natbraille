grammar comments;


/*
OBRACE:'{';
CBRACE:'}';
OBRACE2:'(';
CBRACE2:')';
*/
STRING_DELIM:'"';
//string_literal : STRING_DELIM ((~["]) | '\"')+? STRING_DELIM ;

// string_literal : STRING_DELIM .+? STRING_DELIM ;

STRING_LITERAL_CONTENT
    : ( ~["] | '\\"' )
    ;

string_literal : STRING_DELIM STRING_LITERAL_CONTENT STRING_DELIM ;

EOL_COMMENT : '%%' .*? ('\n' | EOF ) ;
// WS: [ \n\t\r]+ -> skip;


TEXT: ~[%{}() \n\t\r"]+;
text : TEXT;
comment : EOL_COMMENT;

SPECIALSYMBOL
    : '%' TEXT
    ;

specialSymbol
    : SPECIALSYMBOL
    ;

thing :
     comment
     | text
     | string_literal
      //  EOL_COMMENT
     | specialSymbol
 //   | OBRACE thing+? CBRACE
//    | OBRACE2 thing+? CBRACE2
    ;

start_rule
    : thing+?;