grammar StarMath5;

starmath5
    : multilineequation
    ;

multilineequation
    : equations (' '* NEWLINE ' '* equations)*
    ;


equations
   : equation (' '* equation)*
   ;

equation
   : expression (' '* relop ' '* expression)*
   ;

// expression
//   : multiplyingExpression  (' '* (PLUS | MINUS) ' '* multiplyingExpression)*
//   ;

expression
   : multiplyingExpression  (' '* (PLUS | MINUS) ' '* multiplyingExpression)*
   ;


// multiplyingExpression
//    : ( powExpression  ( ' '* (multiplyLevelOperator) ' '* powExpression)* )
//   ;


multiplyingExpression
   : ( overExpression  ( ' '* (multiplyLevelOperator) ' '* overExpression)* )
   ;


overExpression
   : powExpression( ' '* 'over' ' '* powExpression )*
   ;

multiplyLevelOperator
    // : '+-' | '-+' | '+' | '-'   |
    : '/' | 'times' | 'widebslash' | 'cdot' | '*' | 'otimes' | 'div' | 'circ'
    // | 'neg' | 'and' | 'or'
    // | '=' | '<>' | '<' | '<=' | 'leslant' | '>' | '>=' | 'geslant' | '<<' | '>>'
    | 'approx' | 'sim' | 'simeq' | 'equiv' | 'prop' | 'parallel' | 'ortho' | 'divides' | 'ndivides' | 'toward'
    | 'dlarrow' | 'dlrarrow' | 'drarrow'
    | 'prec' | 'succ' | 'preccurlyeq' | 'succcurlyeq' | 'precsim' | 'succsim' | 'nprec' | 'nsucc'
    | 'in' | 'notin' | 'owns'
    | 'intersection' | 'union' | 'setminus' | 'slash' | 'subset' | 'subseteq' | 'supset' | 'supseteq' | 'nsubset' | 'nsubseteq' | 'nsupset' | 'nsupseteq'
    // | 'sum' | 'prod' | 'coprod' | 'int' | 'iint' | 'iiint' | 'lint' | 'llint' | 'lllint'
    | 'partial'
    //| 'exists' | 'notexists'
    | 'transl' | 'transr'
    //| 'nabla'
    | 'def'
    | 'odivide' | 'odot' | 'ominus' | 'oplus'
    // |  'mline' | 'gt' | 'forall'
    ;


powExpression
   : signedAtom (' '* POW ' '* signedAtom)*
    ;



signedAtom
   : PLUSMINUS ' '* signedAtom
   | PLUS ' '* signedAtom
   | MINUS ' '* signedAtom
   | func
   | attributeList ' '* atom
   | atom
   ;

atom
   : fromto
   | from
   | to
  // | overExpression
   | identifierOperators
   | matrix
   | strechedFenceExpression
   | fenceExpression
   | greek
   | scientific
   | variable
   | constant
   | LPAREN equations RPAREN
   | '{' equations '}'
   ;

/*overExpression
   : atom (' '* 'over' ' '* powExpression)
   ;
*/
identifierOperators
    :  'dotsaxis' | 'dotsdown' | 'dotslow' |'dotsup' |'dotsvert'
      | 'downarrow' |'leftarrow' |'uparrow' |'rightarrow'
    ;
strechedFenceExpression
    : 'left' ' '* fenceType ' '* ( equations | ' '* ) ' '* 'right' ' '* fenceType
    ;

fenceExpression
    : fenceType ' '* ( equations| ' '* ) ' '* fenceType
    ;


fenceType
    : 'none'
    | '(' | '[' | 'langle' | 'lbrace' | 'lceil' | 'ldbracket' | 'ldline' | 'lfloor' | 'lline'
    | ')' | ']' | 'rangle' | 'rbrace' | 'rceil' | 'rdbracket' | 'rdline' | 'rfloor' | 'rline'
    ;

fromto
    : sumOperator ' '* 'from' ' '* equation ' '+ 'to' ' '* equation ' '* multiplyingExpression
    ;

to
    : sumOperator ' '*  'to' ' '* equation ' '* multiplyingExpression
    ;

from
    : sumOperator ' '* 'from' ' '* equation ' '* multiplyingExpression
    ;



sumOperator
    :  'sum' | 'prod' | 'coprod' | 'int' | 'iint' | 'iiint' | 'lint' | 'llint' | 'lllint'
    ;

matrix
    : 'matrix' '{' matrixcontent '}'
    ;

matrixcontent
    : matrixline ( '##' matrixline )*
    ;

matrixline
    : matrixcolumn( '#' matrixcolumn )*
    ;

matrixcolumn
    : equations
    ;

stack
    : 'stack' '{' stackcontent '}'
    ;

stackcontent
    : stackline ( '#' stackline )*
    ;

stackline
    : equations
    ;


attributeList
    : attributes (' '+ attributes)*
    ;

attributes
    : ( topAttribute | bottomAttribute | styleAttribute )
    ;

topAttribute
    : 'acute' | 'grave' | 'breve' | 'circle' | 'dot' | 'ddot' | 'dddot' | 'bar' | 'vec' | 'tilde' | 'hat'
    | 'check' | 'widevec' | 'widetilde' | 'widehat' | 'overline'
    ;

bottomAttribute
    : 'underline'
    ;

styleAttribute
    : colorAttribute | sizeAttribute | variantAttribute | fontAttribute
    ;

colorAttribute
    : 'color' ' '+ color
    ;

color
    : 'black' | 'blue' | 'green' | 'red' | 'cyan' | 'magenta' | 'yellow' | 'gray' | 'lime' | 'maroon' | 'navy' | 'olive' | 'purple' | 'silver' | 'teal'
    ;

fontAttribute
    : 'font' ' '+ 'fixed'
    ;

sizeAttribute
    : 'size' ' '+ size
    ;

size
    : SCIENTIFIC_NUMBER
    ;

variantAttribute
    : 'bold' | 'nbold'| 'ital' | 'italic' | 'nitalic'
    ;


greek
    : 're' | 'im'
    | '%ALPHA' | '%BETA' | '%GAMMA' | '%DELTA' | '%EPSILON' | '%ZETA' | '%ETA' | '%THETA' | '%IOTA' | '%KAPPA' | '%LAMBDA' | '%MU' | '%NU' | '%XI' | '%OMICRON' | '%PI' | '%RHO' | '%SIGMA' | '%TAU' | '%UPSILON' | '%PHI' | '%CHI' | '%PSI' | '%OMEGA' | '%alpha' | '%beta' | '%gamma' | '%delta' | '%varepsilon' | '%zeta' | '%eta' | '%theta' | '%iota' | '%kappa' | '%lambda' | '%mu' | '%nu' | '%xi' | '%omicron' | '%pi' | '%rho' | '%varsigma' | '%sigma' | '%tau' | '%upsilon' | '%varphi' | '%chi' | '%psi' | '%omega' | '%vartheta' | '%phi' | '%varpi' | '%varrho' | '%epsilon'
    | '%iALPHA' | '%iBETA' | '%iGAMMA' | '%iDELTA' | '%iEPSILON' | '%iZETA' | '%iETA' | '%iTHETA' | '%iIOTA' | '%iKAPPA' | '%iLAMBDA' | '%iMU' | '%iNU' | '%iXI' | '%iOMICRON' | '%iPI' | '%iRHO' | '%iSIGMA' | '%iTAU' | '%iUPSILON' | '%iPHI' | '%iCHI' | '%iPSI' | '%iOMEGA' | '%ialpha' | '%ibeta' | '%igamma' | '%idelta' | '%ivarepsilon' | '%izeta' | '%ieta' | '%itheta' | '%iiota' | '%ikappa' | '%ilambda' | '%imu' | '%inu' | '%ixi' | '%iomicron' | '%ipi' | '%irho' | '%ivarsigma' | '%isigma' | '%itau' | '%iupsilon' | '%ivarphi' | '%ichi' | '%ipsi' | '%iomega' | '%ivartheta' | '%iphi' | '%ivarpi' | '%ivarrho' | '%iepsilon'
    | 'emptyset' | 'aleph'
    | 'setN' | 'setZ' | 'setQ' | 'setR' | 'setC'
    | '%perthousand' | '%tendto'  | '%element' | '%noelement' | '%infinite' | '%angle' | '%and' | '%or' | '%notequal' | '%identical' | '%strictlylessthan' |'%strictlygreaterthan'
    | 'infinity' | 'infty'
    | 'wp' | 'backepsilon' | 'hbar' | 'lambdabar'
    ;
scientific
   : SCIENTIFIC_NUMBER
   ;

constant
   : PI
   | EULER
   | I
   ;

variable
   : VARIABLE
   ;

func
   : funcname LPAREN expression (COMMA expression)* RPAREN
   ;

funcname
   : COS
   | TAN
   | SIN
   | ACOS
   | ATAN
   | ASIN
   | LOG
   | LN
   | SQRT
   ;

relop
   : EQ
   | GT
   | LT
   ;

NEWLINE
    : 'newline'
    ;


COS
   : 'cos'
   ;


SIN
   : 'sin'
   ;


TAN
   : 'tan'
   ;


ACOS
   : 'acos'
   ;


ASIN
   : 'asin'
   ;


ATAN
   : 'atan'
   ;


LN
   : 'ln'
   ;


LOG
   : 'log'
   ;


SQRT
   : 'sqrt'
   ;


LPAREN
   : '('
   ;


RPAREN
   : ')'
   ;


PLUSMINUS
   : '+-'
   ;

PLUS
   : '+'
   ;


MINUS
   : '-'
   ;

/*
times
    : TIMES
    ;

TIMES
   : '*' | 'times'
   ;
*/

DIV
   : '/'
   ;


GT
   : '>'
   ;


LT
   : '<'
   ;


EQ
   : '='
   ;


COMMA
   : ','
   ;


POINT
   : '.'
   ;


POW
   : '^'
   ;


PI
   : 'pi'
   ;


EULER
   : E2
   ;


I
   : 'i'
   ;


VARIABLE
   : VALID_ID_START VALID_ID_CHAR*
   ;


fragment VALID_ID_START
   : ('a' .. 'z') | ('A' .. 'Z') | '_'
   ;


fragment VALID_ID_CHAR
   : VALID_ID_START /* | ('0' .. '9')*/
   ;


SCIENTIFIC_NUMBER
   : NUMBER ((E1 | E2) SIGN? NUMBER)?
   ;


fragment NUMBER
   : ('0' .. '9') + ('.' ('0' .. '9') +)?
   ;


fragment E1
   : 'E'
   ;


fragment E2
   : 'e'
   ;


fragment SIGN
   : ('+' | '-')
   ;

/*
WS
   : [ \r\n\t] + -> skip
   ;
*/

/*

// TODO check priority

operator


    : '+-' | '-+' | '+' | '-' | '/' | 'times' | 'widebslash' | 'cdot' | '*' | 'otimes' | 'div' | 'circ' | 'neg' | 'and' | 'or'
    | '=' | '<>' | '<' | '<=' | 'leslant' | '>' | '>=' | 'geslant' | '<<' | '>>'
    | 'approx' | 'sim' | 'simeq' | 'equiv' | 'prop' | 'parallel' | 'ortho' | 'divides' | 'ndivides' | 'toward'
    | 'dlarrow' | 'dlrarrow' | 'drarrow'
    | 'prec' | 'succ' | 'preccurlyeq' | 'succcurlyeq' | 'precsim' | 'succsim' | 'nprec' | 'nsucc'
    | 'in' | 'notin' | 'owns'
    | 'intersection' | 'union' | 'setminus' | 'slash' | 'subset' | 'subseteq' | 'supset' | 'supseteq' | 'nsubset' | 'nsubseteq' | 'nsupset' | 'nsupseteq'
    | 'sum' | 'prod' | 'coprod' | 'int' | 'iint' | 'iiint' | 'lint' | 'llint' | 'lllint'
    | 'partial'
    | 'exists' | 'notexists'
    | 'transl' | 'transr'
    | 'nabla'
    | 'dotsaxis' | 'dotsdown' | 'dotslow' |'dotsup' |'dotsvert'
    | 'downarrow' |'leftarrow' |'uparrow' |'rightarrow'
    | 'def'
    | 'odivide' | 'odot' | 'ominus' | 'oplus'
    |  'mline' | 'gt' | 'forall'

    ;

*/

// todo : a * 3 over a * 4 = { { a * 3 } over { a } } * 4
// todo : sum a
/*
todo
a = b over c = d newline
a + b over c + d newline
a * b over c * d newline
a ^ b over c ^ d newline
*/
/*
{a = b over c = d } equiv { a = { b over c } = d }
newline
{a + b over c + d } equiv { a + { b over c } + d }
newline
{ a * b over c * d } equiv { { { a * b } over c } * d }
newline
{ sin(a) over sin(b) } equiv { sin { (a) over sin } (b) }
newline
{ a ^ b over c ^ d } equiv { { a ^ b } over { c ^ d  } }
newline
{ sum from a b over sum from b c } equiv { { sum from a b } over { sum from b c } }
newline
{ a over b over c + d } equiv { { { a over b } over c } + d}
newline
{ a over b = c over d } equiv { { a over b } = { c over d } }
newline
{ a over b + c over d } equiv { { a over b } + { c over d } }
newline
{ a over b * c over d } equiv {{{ a over b } * c } over d }
*/