/*
GPL v2

Copyright (c) 2018 | Vivien Guillet
All rights reserved. 
*/

grammar One;

starmath
    : expressions
    ;

expressions
    : expression (' '+ expression)*
    ;

expression 
    : (attributeList)? (strechedFenceExpression | fenceExpression  | overbrace | underbrace | matrix | stack | fraction
    | fromto | from | to | subsup | sup  | sub | nroot | sqrt  | function | atom)
    ;

atom
    : ' '* ( greek | number | operator | unicodeIdentifier | group  | identifier /*|  quotedString */  ) ' '*
    ;

group
    : '{' expressions  '}'
    ;
// matrix ok
matrix
    : 'matrix' '{' matrixcontent '}'
    ;

matrixcontent
    : matrixline ( '##' matrixline )*
    ;

matrixline
    : matrixcolumn( '#' matrixcolumn )*
    ;

matrixcolumn
    : expression
    ;
// stack ok
stack 
    : 'stack' '{' stackcontent '}'
    ;

stackcontent
    : stackline ( '#' stackline )*
    ;

stackline
    : expression
    ;


fraction
    : atom fractype atom
    ;

fractype
    : ( 'over' | 'wideslash' /* | 'widebslash' */ )
    ;
/* widebslash does not translate to mfrac | but is a standard operator */

nroot
    : 'nroot' atom expression
    ;

sqrt
    : 'sqrt' expression
    ;

subsup
    : ( atom '_' atom '^' expression )
    ;

sup
    : atom ('^'|'exp') expression
    ;

sub
    : atom ('_'|'sub') expression
    ;

// ported
fromto
    : operator ' '* 'from' ' '* expression 'to' ' '* expression ' '* expression
    ;

from
    : operator ' '* 'from' ' '* expression ' '* expression
    ;

to
    : operator ' '* 'to' ' '* expression' '* expression
    ;

// ported
strechedFenceExpression
    : 'left' ' '* fencetype ( expression| ' '* ) 'right' ' '* fencetype
    ;

fenceExpression
    : fencetype ( expression| ' '* ) fencetype
    ;


fencetype
    : 'none'
    | '(' | '[' | 'langle' | 'lbrace' | 'lceil' | 'ldbracket' | 'ldline' | 'lfloor' | 'lline' 
    | ')' | ']' | 'rangle' | 'rbrace' | 'rceil' | 'rdbracket' | 'rdline' | 'rfloor' | 'rline' 
    ;

//
overbrace
    : atom' '* 'overbrace' ' '* expression
    ;

underbrace
    : atom' '* 'underbrace' ' '* expression
    ;

function
    : ( knownFunction expression )

    ;
// 'fact' | 'abs'
knownFunction
    :   'ln' | 'exp' | 'log'
    | 'sin' | 'cos' | 'tan' | 'cot'
    | 'sinh' | 'cosh' | 'tanh' | 'coth'
    | 'arcsin' | 'arccos' | 'arctan' | 'arccot'
    | 'arsinh' | 'arcosh' | 'artanh' | 'arcoth'
    ;


/*
quotedString
    : '"' quotedStringContent '"'
    ;

quotedStringContent
    :  ~('"')
    ;
*/

attributeList
    : attributes (' '+ attributes)*
    ;

attributes
    : ( topAttribute | bottomAttribute | styleAttribute )
    ;

topAttribute
    : 'acute' | 'grave' | 'breve' | 'circle' | 'dot' | 'ddot' | 'dddot' | 'bar' | 'vec' | 'tilde' | 'hat'
    | 'check' | 'widevec' | 'widetilde' | 'widehat' | 'overline' |

    ;
bottomAttribute
    : 'underline'
    ;

styleAttribute
    : colorAttribute | sizeAttribute | variantAttribute
    ;

colorAttribute
    : 'color' ' '+ color
    ;

color
    : 'black' | 'blue' | 'green' | 'red' | 'cyan' | 'magenta' | 'yellow' | 'gray' | 'lime' | 'maroon' | 'navy' | 'olive' | 'purple' | 'silver' | 'teal'
    ;

sizeAttribute
    : 'size' ' '+ size
    ;

size
    : ( '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' )+ ( '.' ( '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' )+ )?
    ;

variantAttribute
    : 'bold' | 'nbold'| 'ital' | 'italic' | 'nitalic'
    ;

/*
    phantom
    : 'phantom'
    ;
*/
/*
encloseAttribute
    : 'overstrike'
    ;
*/
/*
     font <?> {<?>}
*/

unicodeIdentifier
    : '%Ux' fourCharHexaChar
    ;


fourCharHexaChar
    : oneHexaChar oneHexaChar oneHexaChar oneHexaChar
    ;

oneHexaChar
    : ( '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' | 'A' | 'B' | 'C' | 'D' | 'E' | 'F')
    ;

number
    : ( '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' )+ ( ',' ( '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' )+)*
    ;
    
identifier
    :  'A' | 'B' | 'C' | 'D' | 'E' | 'F' | 'G' | 'H' | 'I' | 'J' | 'K' | 'L' | 'M' | 'O' | 'P' | 'Q' | 'R' | 'S' | 'T' | 'U' | 'V' | 'W' | 'X' | 'Y' | 'Z'
    | 'a' | 'b' | 'c' | 'd' | 'e' | 'f' | 'g' | 'h' | 'i' | 'j' | 'k' | 'l' | 'm' | 'n' | 'o' | 'p' | 'q' | 'r' | 's' | 't' | 'u' | 'v' | 'w' | 'x' | 'y' | 'z'
    | '–'
    ;

operator
    : '+-' | '-+' | '+' | '-' | '/' | 'times' | 'widebslash' | 'cdot' | '*' | 'otimes' | 'div' | 'circ' | 'neg' | 'and' | 'or'
    | '=' | '<>' | '<' | '<=' | 'leslant' | '>' | '>=' | 'geslant' | '<<' | '>>'
    | 'approx' | 'sim' | 'simeq' | 'equiv' | 'prop' | 'parallel' | 'ortho' | 'divides' | 'ndivides' | 'toward'
    | 'dlarrow' | 'dlrarrow' | 'drarrow' 
    | 'prec' | 'succ' | 'preccurlyeq' | 'succcurlyeq' | 'precsim' | 'succsim' | 'nprec' | 'nsucc'
    | 'in' | 'notin' | 'owns' 
    | 'intersection' | 'union' | 'setminus' | 'slash' | 'subset' | 'subseteq' | 'supset' | 'supseteq' | 'nsubset' | 'nsubseteq' | 'nsupset' | 'nsupseteq'
    | 'sum' | 'prod' | 'coprod' | 'int' | 'iint' | 'iiint' | 'lint' | 'llint' | 'lllint'
    | 'partial'
    | 'exists' | 'notexists'
    | 'transl' | 'transr'
    | 'nabla'
    | 'dotsaxis' | 'dotsdown' | 'dotslow' |'dotsup' |'dotsvert'
    | 'downarrow' |'leftarrow' |'uparrow' |'rightarrow'
    | 'def'
    | 'odivide' | 'odot' | 'ominus' | 'oplus'
    |  'mline' | 'gt' | 'forall'

    ;


greek
    : 're' | 'im'
    | '%ALPHA' | '%BETA' | '%GAMMA' | '%DELTA' | '%EPSILON' | '%ZETA' | '%ETA' | '%THETA' | '%IOTA' | '%KAPPA' | '%LAMBDA' | '%MU' | '%NU' | '%XI' | '%OMICRON' | '%PI' | '%RHO' | '%SIGMA' | '%TAU' | '%UPSILON' | '%PHI' | '%CHI' | '%PSI' | '%OMEGA' | '%alpha' | '%beta' | '%gamma' | '%delta' | '%varepsilon' | '%zeta' | '%eta' | '%theta' | '%iota' | '%kappa' | '%lambda' | '%mu' | '%nu' | '%xi' | '%omicron' | '%pi' | '%rho' | '%varsigma' | '%sigma' | '%tau' | '%upsilon' | '%varphi' | '%chi' | '%psi' | '%omega' | '%vartheta' | '%phi' | '%varpi' | '%varrho' | '%epsilon'
    | '%iALPHA' | '%iBETA' | '%iGAMMA' | '%iDELTA' | '%iEPSILON' | '%iZETA' | '%iETA' | '%iTHETA' | '%iIOTA' | '%iKAPPA' | '%iLAMBDA' | '%iMU' | '%iNU' | '%iXI' | '%iOMICRON' | '%iPI' | '%iRHO' | '%iSIGMA' | '%iTAU' | '%iUPSILON' | '%iPHI' | '%iCHI' | '%iPSI' | '%iOMEGA' | '%ialpha' | '%ibeta' | '%igamma' | '%idelta' | '%ivarepsilon' | '%izeta' | '%ieta' | '%itheta' | '%iiota' | '%ikappa' | '%ilambda' | '%imu' | '%inu' | '%ixi' | '%iomicron' | '%ipi' | '%irho' | '%ivarsigma' | '%isigma' | '%itau' | '%iupsilon' | '%ivarphi' | '%ichi' | '%ipsi' | '%iomega' | '%ivartheta' | '%iphi' | '%ivarpi' | '%ivarrho' | '%iepsilon'
    | 'emptyset' | 'aleph'
    | 'setN' | 'setZ' | 'setQ' | 'setR' | 'setC'
    | '%perthousand' | '%tendto'  | '%element' | '%noelement' | '%infinite' | '%angle' | '%and' | '%or' | '%notequal' | '%identical' | '%strictlylessthan' |'%strictlygreaterthan'
    | 'infinity' | 'infty'
    | 'wp' | 'backepsilon' | 'hbar' | 'lambdabar'
    ;

