parser grammar uuuparser ;
options { tokenVocab=uuulexer; }

number : NUMBER;
others : OTHER;
bob : BOB ;

all : (bob|number|others)* ;
