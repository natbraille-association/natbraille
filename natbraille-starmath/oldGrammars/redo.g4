grammar redo;


pluffe : STRING+;

COMMENT : '%%' .*? '\n'-> skip;

specialIdentifier : SPECIAL_IDENTIFIER;
otherIdentifier : STRING;


SPECIAL_IDENTIFIER
    : SPECIAL_INDENTIFIER_alpha | SPECIAL_INDENTIFIER_ALPHA | SPECIAL_INDENTIFIER_beta | SPECIAL_INDENTIFIER_BETA | SPECIAL_INDENTIFIER_gamma | SPECIAL_INDENTIFIER_GAMMA | SPECIAL_INDENTIFIER_delta | SPECIAL_INDENTIFIER_DELTA | SPECIAL_INDENTIFIER_epsilon | SPECIAL_INDENTIFIER_EPSILON | SPECIAL_INDENTIFIER_zeta | SPECIAL_INDENTIFIER_ZETA | SPECIAL_INDENTIFIER_eta | SPECIAL_INDENTIFIER_ETA | SPECIAL_INDENTIFIER_theta | SPECIAL_INDENTIFIER_THETA | SPECIAL_INDENTIFIER_iota | SPECIAL_INDENTIFIER_IOTA | SPECIAL_INDENTIFIER_kappa | SPECIAL_INDENTIFIER_KAPPA | SPECIAL_INDENTIFIER_lambda | SPECIAL_INDENTIFIER_LAMBDA | SPECIAL_INDENTIFIER_mu | SPECIAL_INDENTIFIER_MU | SPECIAL_INDENTIFIER_nu | SPECIAL_INDENTIFIER_NU | SPECIAL_INDENTIFIER_xi | SPECIAL_INDENTIFIER_XI | SPECIAL_INDENTIFIER_omicron | SPECIAL_INDENTIFIER_OMICRON | SPECIAL_INDENTIFIER_pi | SPECIAL_INDENTIFIER_PI | SPECIAL_INDENTIFIER_rho | SPECIAL_INDENTIFIER_RHO | SPECIAL_INDENTIFIER_sigma | SPECIAL_INDENTIFIER_SIGMA | SPECIAL_INDENTIFIER_tau | SPECIAL_INDENTIFIER_TAU | SPECIAL_INDENTIFIER_upsilon | SPECIAL_INDENTIFIER_UPSILON | SPECIAL_INDENTIFIER_phi | SPECIAL_INDENTIFIER_PHI | SPECIAL_INDENTIFIER_chi | SPECIAL_INDENTIFIER_CHI | SPECIAL_INDENTIFIER_psi | SPECIAL_INDENTIFIER_PSI | SPECIAL_INDENTIFIER_omega | SPECIAL_INDENTIFIER_OMEGA | SPECIAL_INDENTIFIER_varepsilon | SPECIAL_INDENTIFIER_vartheta | SPECIAL_INDENTIFIER_varpi | SPECIAL_INDENTIFIER_varrho | SPECIAL_INDENTIFIER_varsigma | SPECIAL_INDENTIFIER_varphi | SPECIAL_INDENTIFIER_element | SPECIAL_INDENTIFIER_noelement | SPECIAL_INDENTIFIER_strictlylessthan | SPECIAL_INDENTIFIER_strictlygreaterthan | SPECIAL_INDENTIFIER_notequal | SPECIAL_INDENTIFIER_identical | SPECIAL_INDENTIFIER_tendto | SPECIAL_INDENTIFIER_infinite | SPECIAL_INDENTIFIER_angle | SPECIAL_INDENTIFIER_perthousand | SPECIAL_INDENTIFIER_and | SPECIAL_INDENTIFIER_or
    ;

STRING : .+
    ;

SPECIAL_INDENTIFIER_alpha: '%alpha' ;
SPECIAL_INDENTIFIER_ALPHA: '%ALPHA' ;
SPECIAL_INDENTIFIER_beta: '%bêta' ;
SPECIAL_INDENTIFIER_BETA: '%BÊTA' ;
SPECIAL_INDENTIFIER_gamma: '%gamma' ;
SPECIAL_INDENTIFIER_GAMMA: '%GAMMA' ;
SPECIAL_INDENTIFIER_delta: '%delta' ;
SPECIAL_INDENTIFIER_DELTA: '%DELTA' ;
SPECIAL_INDENTIFIER_epsilon: '%epsilon' ;
SPECIAL_INDENTIFIER_EPSILON: '%EPSILON' ;
SPECIAL_INDENTIFIER_zeta: '%zêta' ;
SPECIAL_INDENTIFIER_ZETA: '%ZÊTA' ;
SPECIAL_INDENTIFIER_eta: '%êta' ;
SPECIAL_INDENTIFIER_ETA: '%ÊTA' ;
SPECIAL_INDENTIFIER_theta: '%thêta' ;
SPECIAL_INDENTIFIER_THETA: '%THÊTA' ;
SPECIAL_INDENTIFIER_iota: '%iota' ;
SPECIAL_INDENTIFIER_IOTA: '%IOTA' ;
SPECIAL_INDENTIFIER_kappa: '%kappa' ;
SPECIAL_INDENTIFIER_KAPPA: '%KAPPA' ;
SPECIAL_INDENTIFIER_lambda: '%lambda' ;
SPECIAL_INDENTIFIER_LAMBDA: '%LAMBDA' ;
SPECIAL_INDENTIFIER_mu: '%mu' ;
SPECIAL_INDENTIFIER_MU: '%MU' ;
SPECIAL_INDENTIFIER_nu: '%nu' ;
SPECIAL_INDENTIFIER_NU: '%NU' ;
SPECIAL_INDENTIFIER_xi: '%xi' ;
SPECIAL_INDENTIFIER_XI: '%XI' ;
SPECIAL_INDENTIFIER_omicron: '%omicron' ;
SPECIAL_INDENTIFIER_OMICRON: '%OMICRON' ;
SPECIAL_INDENTIFIER_pi: '%pi' ;
SPECIAL_INDENTIFIER_PI: '%PI' ;
SPECIAL_INDENTIFIER_rho: '%rhô' ;
SPECIAL_INDENTIFIER_RHO: '%RHÔ' ;
SPECIAL_INDENTIFIER_sigma: '%sigma' ;
SPECIAL_INDENTIFIER_SIGMA: '%SIGMA' ;
SPECIAL_INDENTIFIER_tau: '%tau' ;
SPECIAL_INDENTIFIER_TAU: '%TAU' ;
SPECIAL_INDENTIFIER_upsilon: '%upsilon' ;
SPECIAL_INDENTIFIER_UPSILON: '%UPSILON' ;
SPECIAL_INDENTIFIER_phi: '%phi' ;
SPECIAL_INDENTIFIER_PHI: '%PHI' ;
SPECIAL_INDENTIFIER_chi: '%khi' ;
SPECIAL_INDENTIFIER_CHI: '%KHI' ;
SPECIAL_INDENTIFIER_psi: '%psi' ;
SPECIAL_INDENTIFIER_PSI: '%PSI' ;
SPECIAL_INDENTIFIER_omega: '%oméga' ;
SPECIAL_INDENTIFIER_OMEGA: '%OMÉGA' ;
SPECIAL_INDENTIFIER_varepsilon: '%varepsilon' ;
SPECIAL_INDENTIFIER_vartheta: '%varthêta' ;
SPECIAL_INDENTIFIER_varpi: '%varpi' ;
SPECIAL_INDENTIFIER_varrho: '%varrhô' ;
SPECIAL_INDENTIFIER_varsigma: '%varsigma' ;
SPECIAL_INDENTIFIER_varphi: '%varphi' ;
SPECIAL_INDENTIFIER_element: '%élément' ;
SPECIAL_INDENTIFIER_noelement: '%pasélément' ;
SPECIAL_INDENTIFIER_strictlylessthan: '%trèsinférieurà' ;
SPECIAL_INDENTIFIER_strictlygreaterthan: '%trèssupérieurà' ;
SPECIAL_INDENTIFIER_notequal: '%différent' ;
SPECIAL_INDENTIFIER_identical: '%identique' ;
SPECIAL_INDENTIFIER_tendto: '%tend' ;
SPECIAL_INDENTIFIER_infinite: '%infini' ;
SPECIAL_INDENTIFIER_angle: '%angle' ;
SPECIAL_INDENTIFIER_perthousand: '%pourmille' ;
SPECIAL_INDENTIFIER_and: '%et' ;
SPECIAL_INDENTIFIER_or: '%ou' ;

