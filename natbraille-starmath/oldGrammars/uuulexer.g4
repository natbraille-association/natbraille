lexer grammar uuulexer ;

// EOL_COMMENT : EOL_COMMENT_HEAD  EOL_COMMENT_TAIL;
// STRING_LITTERAL : STRING_LITTERAL_START  STRING_LITTERAL_CONTENT STRING_LITTERAL_END;
/*
USER_DEFINED_IDENTIFIER
    : '%' (~( [ \t\n\r+-/=#%\\"~><&|(){}[\]^_] | '*' ))+
    ;
EOL_COMMENT_HEAD
    : '%%' -> pushMode(EOL_COMMENT_MODE)
    ;
STRING_LITTERAL_START
    : '"' -> pushMode(STRING_LITTERAL_MODE)
  */
NUMBER : DIGIT+ ;
DIGIT : [0123456789] ;
BOB : 'bob';
/*

mode EOL_COMMENT_MODE ;

EOL_COMMENT_TAIL :
    ~[\n\r]* ( EOF | [\n\r]? ) -> popMode
    ;

mode STRING_LITTERAL_MODE ;

STRING_LITTERAL_END :
    '"'  -> popMode;

STRING_LITTERAL_CONTENT :
    (~["\\] | ESCAPED )* ;

fragment ESCAPED : '\\' . ;

mode DEFAULT_MODE;
*/
OTHER : ~[\t ]+;
SPACE : [\t ] -> skip;

