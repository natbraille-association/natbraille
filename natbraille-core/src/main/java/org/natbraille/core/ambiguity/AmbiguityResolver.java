   /*
 * Nat Braille
 * Copyright (C) 2008 Bruno Mascret
 * Contact: bmascret@free.fr
 * http://natbraille.free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.core.ambiguity;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import org.natbraille.core.gestionnaires.GestionnaireErreur;
import org.natbraille.core.gestionnaires.LogLevel;
import org.natbraille.core.gestionnaires.MessageContents;
import org.natbraille.core.gestionnaires.MessageKind;


/**
 * Résoud les ambiguïtés détectée lors de la transcription
 *
 * @author bruno
 *
 */
public class AmbiguityResolver {

    /**
     * Nom du fichier transcrit contenant des ambiguïtés
     */
//    private String fTransAmb = "";
    /**
     * Nom du fichier transcrit contenant les ambiguïtés résolues
     */
//    private String fTransAmbResol = "";
    /**
     * Instance du gestionnaire d'erreurs
     */
    private GestionnaireErreur gest = null;

    /**
     * instance de AmbiguityResolverUI
     */
    private final AmbiguityResolverUI arUI;

    /**
     * Create an ambiguity resolver
     *
     * @param fTrAmb adresse du fichier à analyser
     * @param g 
     */
    public AmbiguityResolver(GestionnaireErreur g) {
        gest = g;
        arUI = new AmbiguityResolverUIAuto();
    }
    
    /**
     * Create an ambiguity resolver
     * @param g
     * @param ambiguityResolverUI 
     */
    public AmbiguityResolver(GestionnaireErreur g, AmbiguityResolverUI ambiguityResolverUI) {
        gest = g;
        arUI = ambiguityResolverUI;
    }

    /**
     * Charge les ambiguités
     *
     * @return true si analyse terminée correctement
     */
    public boolean resolveAmbiguity(InputStream withAmbiguity, OutputStream withoutAmbiguity) {
        boolean retour = true;

        gest.afficheMessage(MessageKind.INFO,MessageContents.Tr("Resolve ambiguities"), LogLevel.VERBOSE);
        
        try {
            BufferedReader raf = new BufferedReader(new InputStreamReader(withAmbiguity, "UTF-8"));
            BufferedWriter fcible = new BufferedWriter(new OutputStreamWriter(withoutAmbiguity, "UTF8"));
            String ligne = raf.readLine();
            while (ligne != null) {
                if (ligne.contains("[Amb:")) {
                    //récupération de l'ambiguïté
                    String[] decoup = ligne.split("\\[Amb:|\\]");
                    for (int i = 0; i < decoup.length; i++) {
                        if (i % 2 == 0) {
                            fcible.write(decoup[i]);
                        } else {
                            Ambiguity amb = new Ambiguity(decoup[i]);
                            arUI.resolve(amb);
                            fcible.write(amb.getSolution());
                        }
                    }
                    fcible.write("\n");
                } else {
                    fcible.write(ligne + "\n");
                }
                ligne = raf.readLine();
            }
            raf.close();
            fcible.close();
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return retour;
    }
}


/**
 * was commented out in resolveAmbiguity. 
 */
          /*Document doc;
         DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
         dbf.setValidating(ConfigNat.getCurrentConfig().getNiveauLog()==Nat.LOG_DEBUG);
    
         DocumentBuilder db;
         try
         {
         db = dbf.newDocumentBuilder();
         doc = db.parse(new File(fTransAmb));
			
         NodeList ambs = doc.get
			
         for(int i=0;i<ambs.getLength();i++)
         {	
         ambiguities.add(new Ambiguity(ambs.item(i)));
         }
         }
         catch (ParserConfigurationException e) {e.printStackTrace();}//TODO
         catch (SAXException e) {e.printStackTrace();}
         catch (IOException e) {e.printStackTrace();}

* 
 */