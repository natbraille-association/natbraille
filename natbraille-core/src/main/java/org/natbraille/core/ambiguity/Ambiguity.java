/*
 * Trace assistant ?????????????????
 * Copyright (C) 2008 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.core.ambiguity;
import java.util.ArrayList;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Représente une ambiguité
 * @author bruno
 *
 */
public class Ambiguity
{
	/** chaine de description de l'ambiguïté */
	private String description ="";
	/** chaine de contexte avant */
	private String avant ="";
	/** chaine de contexte apres */
	private String apres ="";
	/** expression posant problème */
	private String expression = "";
	/** liste des solutions possibles en braille*/
	private ArrayList<String> propositions = new ArrayList<>();
	/** liste des descriptions des solutions */
	private ArrayList<String> descSolution = new ArrayList<>();
	/** solution retenue pour l'ambiguité */
	private String solution = "";
	
	/**
     * @param ambXML le noeud ambiguité d'un fichier transcrit
     */
    public Ambiguity(Node ambXML)
    {
	    NodeList fils = ambXML.getChildNodes();
	    for(int i = 0; i< fils.getLength(); i++)
	    {
	    	Node f = fils.item(i);
	    	//if(f.getNodeName().equals("ambBr")||f.getNodeName().equals("ambBl")){System.out.println("not yet implemented");}
	    	/*
	    	else if (f.getNodeName().equals("description"))
	    	{
	    		description = f.getTextContent();
	    	}
	    	else if(f.getNodeName().equals("proposition"))
	    	{
	    		Node p = f.getChildNodes().item(0);
	    		Node d = f.getChildNodes().item(1);
	    		if(p.getNodeName().equals("desc"))
	    		{
	    			descSolution.add(p.getTextContent());
	    			propositions.add(d.getTextContent());
	    		}
	    		else
	    		{
	    			descSolution.add(d.getTextContent());
	    			propositions.add(p.getTextContent());
	    		}
	    	}*/
	    	if(f.getNodeName().equals("message"))
	    	{
	    		description = f.getTextContent();
	    	}
	    	else if(f.getNodeName().equals("alternatives"))
	    	{
	    		NodeList alt = f.getChildNodes();
	    		for(int j = 0; j< alt.getLength(); j++)
	    	    {
	    			Node a = alt.item(j);
	    			if(a.getNodeName().equals("alternative"))
	    			{
	    				propositions.add(a.getTextContent());
	    			}
	    			else if(a.getNodeName().equals("description"))
	    			{
	    				descSolution.add(a.getTextContent());
	    			}
	    	    }
	    	}
	    }
    }
    
    /**
     * Charge une chaine contenant une ambiguité
     * Si une proposition vaut "null", elle est remplacée par une chaîne vide
     * @param amb l'ambiguité sous forme de chaine
     */
    public Ambiguity(String amb)
    {
	    String [] decoup = amb.split("\\|");
	 //   System.out.println(amb);
	    /*for(int i =0;i<decoup.length;i++)
	    {
	    	System.out.println(" decoup["+i+"]=" + decoup[i]);
	    }*/
	    if (decoup.length > 5)
	    {
	    	description = decoup[0];
	    	avant = decoup[1];
	    	expression = decoup[2];
	    	apres = decoup[3];
	    	for(int i =4; i<decoup.length;i++)
	    	{
	    		String prop = decoup[i];
	    		if(prop.equals("null")){prop="";}
	    		propositions.add(prop);
	    	}
	    }
    }

	/**
     * Pour les tests
     * Affiche une ambiguité dans la console
     */
    public void afficheAmbiguity()
    {
    	System.out.println("Ambiguité:" + description + "\nPropositions:");
    	for(int i=0; i<propositions.size();i++)
    	{
    		//System.out.println(" - "+descSolution.get(i));
    		System.out.println(" * "+propositions.get(i));
    	}
    	System.out.println("  Contexte:" + avant + " {"+expression+" }"+apres);
    }
    
    /**
     * compare deux ambiguités en fonction de leur description et de leur expression
     * @param a ambiguité à comparer
     * @return true si {@link #expression} et {@link #description} sont identiques
     */
    @Override
    public boolean equals(Object a)
    {
    	boolean retour = false;
    	if (a instanceof Ambiguity)
    	{
    		Ambiguity amb = (Ambiguity) a;
    		if(amb.description.equals(description) && amb.expression.equals(expression)){retour=true;}
    	}
    	else{retour = super.equals(a);}
    	return retour;
    }
   
	/**
     * @param sol the solution to set
     */
    public void setSolution(String sol){solution = sol;}

	/**
     * @return la solution retenue
     */
    public String getSolution(){return solution;}

	/**
     * @return {@link #propositions}
     */
    public ArrayList<String> getPropositions(){return propositions;}
    
    /**
     * @return {@link #expression}
     */
    public String getExpression(){return expression;}
    /**
     * @return {@link #avant}
     */
    public String getAvant(){return avant;}
    /**
     * @return {@link #apres}
     */
    public String getApres(){return apres;}
    /**
     * @return {@link #description}
     */
    public String getDescription(){return description;}

	/**
	 * Return a String representation of XML format with 
	 * @param indent the string to add for indentation
	 * @return a XML string
	 */
    public String getXML(String indent)
    {
	    String ret = indent + "<message>" + description + "</message>\n";
	    if(propositions.size()>0)
	    {
	    	ret += indent + "<alternatives>\n";
	    	for(String s:propositions){ret += indent + "\t<alternative>"+s+"</alternative>\n";}
	    	ret += indent + "</alternatives>\n";
	    }
	    return ret;
    }

	

}



