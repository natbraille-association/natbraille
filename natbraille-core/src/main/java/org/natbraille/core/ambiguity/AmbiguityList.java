/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.natbraille.core.ambiguity;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import org.natbraille.core.document.NatDocument;
import org.natbraille.core.document.NatDocumentException;
import org.natbraille.core.document.Utils;

/**
 *
 * @author vivien
 */
public class AmbiguityList {

    private final List<Ambiguity> ambiguities = new ArrayList<>();

    private Ambiguity getAmbiguity(Ambiguity ambiguity) {
        Ambiguity foundAmbiguity = null;
        for (Ambiguity existingAmbiguity : ambiguities) {
            if ((existingAmbiguity.getDescription().equals(ambiguity.getDescription())
                    && existingAmbiguity.getDescription().equals(ambiguity.getDescription()))) {
                foundAmbiguity = existingAmbiguity;
                break;
            }
        }
        return foundAmbiguity;
    }

    private boolean isNew(Ambiguity ambiguity) {
        return (getAmbiguity(ambiguity) == null);
    }

    public void addAmbiguity(Ambiguity ambiguity, boolean forceAddExisting) {
        if (forceAddExisting || isNew(ambiguity)) {
            ambiguities.add(ambiguity);

        }
    }

    public class AmbiguityResolverUIFromList implements AmbiguityResolverUI {

        private final AmbiguityResolverUIAuto defaultResolverUI;

        public AmbiguityResolverUIFromList() {
            defaultResolverUI = new AmbiguityResolverUIAuto();
        }

        @Override
        public void resolve(Ambiguity ambiguityToBeResolved) {
            Ambiguity ambiguityFromList = getAmbiguity(ambiguityToBeResolved);
            if (ambiguityFromList != null) {
                if (!ambiguityFromList.getSolution().isEmpty()) {
                    ambiguityToBeResolved.setSolution(ambiguityFromList.getSolution());
                }
            } else {
                defaultResolverUI.resolve(ambiguityToBeResolved);
                //ambiguities.add(ambiguityToBeResolved);                
            }
        }

    }

    public AmbiguityResolverUIFromList newAmbiguityResolverUI() {
        return new AmbiguityResolverUIFromList();
    }

    public List<Ambiguity> getList() {
        return ambiguities;
    }

    public List<Ambiguity> getUnresolved() {
        List<Ambiguity> unresolved = new ArrayList<>();
        for (Ambiguity oneAmbiguity : ambiguities) {
            if (oneAmbiguity.getSolution().isEmpty()) {
                unresolved.add(oneAmbiguity);
            }
        }
        return unresolved;
    }

    public void updateFromDocument(NatDocument natDocument) throws NatDocumentException {
        InputStream inputStream = Utils.toInputStream(natDocument.getBytebuffer());
        for (Ambiguity ambiguity : extractAmbiguities(inputStream)) {
            addAmbiguity(ambiguity, true);
        }
    }

    private static List<Ambiguity> extractAmbiguities(InputStream withAmbiguity) {
        List<Ambiguity> allAmbiguitites = new ArrayList<>();
        try {
            BufferedReader raf = new BufferedReader(new InputStreamReader(withAmbiguity, "UTF-8"));
            String ligne = raf.readLine();
            while (ligne != null) {
                if (ligne.contains("[Amb:")) {
                    //récupération de l'ambiguïté
                    String[] decoup = ligne.split("\\[Amb:|\\]");
                    for (int i = 0; i < decoup.length; i++) {
                        if (i % 2 == 0) {
                        } else {
                            Ambiguity amb = new Ambiguity(decoup[i]);
                            allAmbiguitites.add(amb);
                        }
                    }
                } else {
                }
                ligne = raf.readLine();
            }
            raf.close();

        } catch (UnsupportedEncodingException | FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return allAmbiguitites;

    }

}
