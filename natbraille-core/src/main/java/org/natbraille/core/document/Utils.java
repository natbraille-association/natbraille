/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.core.document;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.charset.Charset;
import java.nio.file.Path;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.natbraille.core.tools.CharsetToolkit;
import org.w3c.dom.Document;
import org.xml.sax.DTDHandler;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

/**
 * Conversion utils between bytebuffers, files, inputstream and documents mainly
 * for use by NatDocument
 * 
 * @author vivien
 * 
 */
public class Utils {

	/** to ByteBuffer **/

	/**
	 * Path to ByteBuffer raw bytes
	 * no charset modification
	 * 
	 * @param path
	 * @return
	 * @throws IOException
	 */
	public static ByteBuffer toByteBuffer(Path path) throws IOException {
		URL url = path.toUri().toURL();
		InputStream url_is = url.openStream();
		return toByteBuffer(url_is);
	}

	/**
	 * InputStream to ByteBuffer
	 * no charset modification
	 * 
	 * @param inputstream
	 * @return
	 * @throws IOException
	 */
	public static ByteBuffer toByteBuffer(InputStream inputstream) throws IOException {
		BufferedInputStream bis = new BufferedInputStream(inputstream);
		ByteArrayOutputStream out = new ByteArrayOutputStream(BUFSIZE);
		byte[] tmp = new byte[BUFSIZE];
		while (true) {
			int r = bis.read(tmp);
			if (r == -1)
				break;
			out.write(tmp, 0, r);
		}
		return ByteBuffer.wrap(out.toByteArray());
	}

	/**
	 * Document to ByteBuffer
	 * 
	 * @param document
	 * @param charset the Charset of the output ByteBuffer
	 * @return
	 * @throws IOException
	 * @throws TransformerException
	 */
	public static ByteBuffer toByteBuffer(Document document, Charset charset) throws IOException, TransformerException {
		ByteArrayOutputStream out = new ByteArrayOutputStream(BUFSIZE);

		String encoding = charset.name();
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer = tf.newTransformer();
                if (document.getDoctype() != null){
                    if (document.getDoctype().getSystemId() != null){
                     transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM,document.getDoctype().getSystemId());
                    }
                }
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
		transformer.setOutputProperty(OutputKeys.METHOD, "xml");
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty(OutputKeys.ENCODING, encoding);
		transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
                
                DOMSource domSource;
                /*
                if (document.getDoctype() != null && document.getDoctype().getSystemId() !=null){
                    String sysId = document.getDoctype().getSystemId();                    
                    domSource =  new DOMSource(document,sysId);
                } else {
                    domSource =  new DOMSource(document);                    
                }
                */
                domSource =  new DOMSource(document);                    
		transformer.transform(domSource, new StreamResult(new OutputStreamWriter(out, encoding)));
	    // WARNING: Supplied DOM uses namespaces, but is not created as namespace-aware
		return ByteBuffer.wrap(out.toByteArray());
	}


	/**
	 * String to ByteBuffer
	 * 
	 * @param string
	 * @param charset
	 *            the Charset used for the String encoding
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static ByteBuffer toByteBuffer(String string, Charset charset) throws UnsupportedEncodingException {
		return ByteBuffer.wrap(string.getBytes(charset));
	}

	/**
	 * ByteBuffer to InputStream
	 * 
	 * @param bytebuffer
	 * @return
	 */
	public static InputStream toInputStream(ByteBuffer bytebuffer) {
		return new ByteArrayInputStream(bytebuffer.array());
	}

	/**
	 * Path to InputStream
	 * no charset modification
	 * 
	 * @param path
	 * @return
	 * @throws MalformedURLException
	 * @throws IOException
	 */
	public static InputStream toInputStream(Path path) throws MalformedURLException, IOException {
		URL url;
		url = path.toUri().toURL();
		InputStream url_is;
		url_is = url.openStream();
		return new BufferedInputStream(url_is);
	}

	/**
	 * 
	 * String to InputStream
	 * 
	 * @param string
	 * @param charset the Charset of the output
	 * @return
	 */
	public static InputStream toInputStream(String string, Charset charset) {
		return new ByteArrayInputStream(string.getBytes(charset));
	}

	/**
	 * Document to InputStream
	 * 
	 * @param document
	 * @return
	 * @throws IOException
	 * @throws TransformerException
	 */
	public static InputStream toInputStream(Document document, Charset charset) throws IOException, TransformerException {
		return toInputStream(toByteBuffer(document,charset));
	}

	// http://docs.oracle.com/javase/7/docs/api/javax/xml/parsers/DocumentBuilder.html

	/**
	 * InputStream to Document
	 * @param inputstream
	 * @param xmlReader
	 * @return
	 * @throws SAXException
	 * @throws IOException
	 * @throws ParserConfigurationException
	 */
	public static Document toDocument(InputStream inputstream, XMLReader xmlReader) throws SAXException, IOException, ParserConfigurationException {

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

		dbf.setNamespaceAware(true);
		dbf.setValidating(true);
		dbf.setIgnoringElementContentWhitespace(true);
		dbf.setIgnoringComments(true);
		dbf.setIgnoringElementContentWhitespace(false);

		// sinon, génère parfois des null pointer exp au parsage (problème
		// avec les simples quote)
		dbf.setFeature("http://apache.org/xml/features/dom/defer-node-expansion", false);

		DocumentBuilder db = dbf.newDocumentBuilder();
		db.setEntityResolver(xmlReader.getEntityResolver());
		db.setErrorHandler(xmlReader.getErrorHandler());

		return db.parse(inputstream);

	}
	/**
	 * ByteBuffer to Document
	 * @param bytebuffer
	 * @param xmlReader
	 * @return
	 * @throws SAXException
	 * @throws IOException
	 * @throws ParserConfigurationException
	 */
	public static Document toDocument(ByteBuffer bytebuffer, XMLReader xmlReader) throws SAXException, IOException, ParserConfigurationException {
		return toDocument(toInputStream(bytebuffer), xmlReader);
	}

	/**
	 * Path to Document
	 * @param path
	 * @param xmlReader
	 * @return
	 * @throws MalformedURLException
	 * @throws SAXException
	 * @throws IOException
	 * @throws ParserConfigurationException
	 */
	public static Document toDocument(Path path, XMLReader xmlReader) throws MalformedURLException, SAXException, IOException, ParserConfigurationException {
		return toDocument(toInputStream(path), xmlReader);
	}

	/**
	 * String to document
	 * @param string
	 * @param charset encoding of the document
	 * @param xmlReader
	 * @return
	 * @throws SAXException
	 * @throws IOException
	 * @throws ParserConfigurationException
	 */
	public static Document toDocument(String string, Charset charset, XMLReader xmlReader) throws SAXException, IOException, ParserConfigurationException {
		return toDocument(toInputStream(string,charset), xmlReader);

	}

	/**
	 * ByteBuffer to string
	 * @param bytebuffer
	 * @param charset encoding of the byte buffer
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static String toString(ByteBuffer bytebuffer,Charset charset) throws UnsupportedEncodingException {
		return new String(bytebuffer.array(), charset);
	}


	/**
	 * InputStream to String
	 * @param inputstream
	 * @param charset of the InputStream
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws IOException
	 */
	public static String toString(InputStream inputstream,Charset charset) throws UnsupportedEncodingException, IOException {
		return toString(toByteBuffer(inputstream),charset );
	}


	/**
	 * Document to String
	 * @param document
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws IOException
	 * @throws TransformerException
	 */
	public static String toString(Document document) throws UnsupportedEncodingException, IOException, TransformerException {
		Charset charset = Charset.forName("UTF-8");
		return toString(toByteBuffer(document,charset),charset);
	}

	/**
	 * Path to String 
	 * @param path
	 * @param charset of the data in the Path
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws IOException
	 */
	public static String toString(Path path, Charset charset) throws UnsupportedEncodingException, IOException {
		return toString(toByteBuffer(path),charset);
	}
	
	static int BUFSIZE = 1024;

	/*
	 * public static void documenttoOutputStream(Document doc, OutputStream out)
	 * throws IOException, TransformerException { TransformerFactory tf =
	 * TransformerFactory.newInstance(); Transformer transformer =
	 * tf.newTransformer();
	 * transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
	 * transformer.setOutputProperty(OutputKeys.METHOD, "xml");
	 * transformer.setOutputProperty(OutputKeys.INDENT, "yes");
	 * transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
	 * transformer.setOutputProperty(
	 * "{http://xml.apache.org/xslt}indent-amount", "4");
	 * transformer.transform(// new DOMSource(doc), null, new StreamResult(new
	 * OutputStreamWriter(out, "UTF-8"))); }
	 */

	public static void URItoFile(URI downloadURI, URI localURI) throws IOException {
            
            System.out.println("download "+downloadURI+" to "+localURI);
		File saveToFile = new File(localURI);
		if (!saveToFile.exists()) {
			new File(saveToFile.getParent()).mkdirs();
			FileOutputStream fos = new FileOutputStream(saveToFile);
			URL website = downloadURI.toURL();
			ReadableByteChannel rbc = Channels.newChannel(website.openStream());
			fos.getChannel().transferFrom(rbc, 0, 1 << 24);
			fos.close();
		} else {
			// TODO if file exists
		}
	}

	/**
	 * use a explicit SAX Reader to be able to set the entity resolver, which is
	 * not possible with the default reader
	 * http://stackoverflow.com/questions/1091384
	 * /prevent-dtd-download-when-using-xslt-i-e-xml-transformer
	 */
	public static XMLReader newXMLReader(EntityResolver entityResolver, DTDHandler dtdHandler, ErrorHandler readerErrorHandler) {
		// create the Reader
		SAXParserFactory spf = SAXParserFactory.newInstance();
		spf.setNamespaceAware(true);
		XMLReader xslReader = null;
		try {
			xslReader = spf.newSAXParser().getXMLReader();
			xslReader.setEntityResolver(entityResolver);
			xslReader.setDTDHandler(dtdHandler);
			xslReader.setErrorHandler(readerErrorHandler);
			}
		catch (SAXException | ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return xslReader;
	}


	/**
	 * guess the charset of the Document based
	 * on its input encoding (while parsing)
	 * or the xml encoding specified in the Document
	 * @param document
	 * @return
	 * @throws NatDocumentException
	 */
	public static Charset guessDocumentCharset(Document document) {
		Charset foundCharset = null;
		if (document != null){
			String docIe = document.getInputEncoding();
			String docXe = document.getXmlEncoding();
			if (docIe != null){
				foundCharset = Charset.forName(docIe);
			} else if (docXe != null) {
				foundCharset= Charset.forName(docXe);			
			}
		}
		return foundCharset;
	}

	/**
	 * guess the charset of a bytebuffer by magic
	 * @param byteBuffer
	 * @return
	 * @throws NatDocumentException
	 */
	@Deprecated
	public static Charset guessByteBufferCharset(ByteBuffer byteBuffer){
		Charset foundCharset = null;
		CharsetToolkit ct = new CharsetToolkit(byteBuffer.array());
		foundCharset = ct.guessEncoding();
		return foundCharset;
	}

	public static Charset guessPathCharset(Path path)  {
		Charset resu = null;
		try {
			URL url = new URL(path.toString());
			URLConnection connection = url.openConnection();
			resu = Charset.forName(connection.getContentEncoding());
		} catch (Exception e){
			// resu will be null
		}
		return resu;
	}	




}
