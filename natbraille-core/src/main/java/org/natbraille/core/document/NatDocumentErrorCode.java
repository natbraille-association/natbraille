/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.core.document;

public enum NatDocumentErrorCode  {
	/**
	 * thrown if unable to get or guess the mime type of the data
	 */
	DOCUMENT_UNKNOWN_MIME_TYPE, 
	
	
	/**
	 * thrown when trying to set data multiple times
	 */
	DOCUMENT_DATA_ALREADY_SET, 

	/**
	 * thrown when unable to set the data
	 */
	DOCUMENT_CANNOT_SET_DATA,
	

	/**
	 * thrown when unable to get the data
	 */
	DOCUMENT_CANNOT_GET_DATA,
	
	/**
	 * thrown when unable to get a temporary file
	 */
	DOCUMENT_TMP_FILE_ERROR, 
	
	/**
	 *  thrown when unable to guess the Charset of the NatDocument data
	 */
	DOCUMENT_CANNOT_GUESS_CHARSET, 
	
	/**
	 *  thrown when unable to guess the Document type of the NatDocument data
	 */	
	DOCUMENT_CANNOT_GUESS_CONTENT_TYPE,
	
	/**
	 * thrown when no charset has been provided and told to force to it
	 */
	DOCUMENT_NO_FORCING_CHARSET, 
	
	/**
	 * thrown when an error occurs while trying to build a copy of
	 * the document in a different charset
	 */
	DOCUMENT_CANNOT_CONVERT_CHARSET
}
