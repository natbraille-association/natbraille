package org.natbraille.core.document;

        import org.w3c.dom.Document;
        import org.xml.sax.*;

        import javax.xml.parsers.DocumentBuilder;
        import javax.xml.parsers.DocumentBuilderFactory;
        import javax.xml.parsers.ParserConfigurationException;
        import java.io.IOException;
        import java.io.InputStream;
        import java.io.StringReader;
/*
 * TODO error handling.
 */
public class NatDocumentXMLReader {
    public static Document toSimpleDocument(InputStream inputstream, XMLReader xmlReader) throws SAXException, IOException, ParserConfigurationException {

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

        dbf.setNamespaceAware(false);
        dbf.setValidating(false);
        dbf.setIgnoringElementContentWhitespace(true);
        dbf.setIgnoringComments(true);
        dbf.setIgnoringElementContentWhitespace(false);
        dbf.setSchema(null);

        dbf.setValidating(false);
        dbf.setNamespaceAware(true);
	/*	dbf.setFeature("http://xml.org/sax/features/namespaces", false);
		dbf.setFeature("http://xml.org/sax/features/validation", false);
		dbf.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
		dbf.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);

*/
        // sinon, génère parfois des null pointer exp au parsage (problème
        // avec les simples quote)
        dbf.setFeature("http://apache.org/xml/features/dom/defer-node-expansion", false);

        DocumentBuilder db = dbf.newDocumentBuilder();
//        System.out.println("gen a document");
        db.setEntityResolver(xmlReader.getEntityResolver());
        db.setErrorHandler(xmlReader.getErrorHandler());

        return db.parse(inputstream);

    }
    public static XMLReader getSimpleXMLReader() {
        return Utils.newXMLReader(
                new EntityResolver() {
                    @Override
                    public InputSource resolveEntity(String s, String s1) throws SAXException, IOException {
                        // System.out.println("ER-ICIA"+s+" ------ "+s1);
                        return new InputSource(new StringReader(""));
                    }
                },
                new DTDHandler() {
                    @Override
                    public void notationDecl(String s, String s1, String s2) throws SAXException {
//                        System.out.println("DTDH-ICIA");
                    }

                    @Override
                    public void unparsedEntityDecl(String s, String s1, String s2, String s3) throws SAXException {
//                        System.out.println("DTDH-ICIB");
                    }
                },
                new ErrorHandler() {
                    @Override
                    public void warning(SAXParseException e) throws SAXException {
                        e.printStackTrace();
                        //                        System.out.println("EH-ICIA");
//                        System.out.println(e.toString());
                    }

                    @Override
                    public void error(SAXParseException e) throws SAXException {
                        e.printStackTrace();
                        //                        System.out.println("EH-ICIB");

                    }

                    @Override
                    public void fatalError(SAXParseException e) throws SAXException {
                        e.printStackTrace();
                        //                        System.out.println("EH-ICIC");
                    }
                }
        );
    }
}
