package org.natbraille.core;

import org.natbraille.core.document.NatDocument;
import org.natbraille.core.document.NatDocumentException;
import org.natbraille.core.filter.NatFilterChain;
import org.natbraille.core.filter.NatFilterConfigurator;
import org.natbraille.core.filter.NatFilterException;
import org.natbraille.core.filter.convertisseur.ConvertisseurTexte;
import org.natbraille.core.filter.convertisseur.FragmentIdentifier;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.core.filter.transcodeur.TranscodeurNormal;
import org.natbraille.core.gestionnaires.AfficheurAnsiConsole;
import org.natbraille.core.gestionnaires.GestionnaireErreur;
import org.natbraille.core.gestionnaires.LogLevel;

import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;

public class TestUserDocumentLoader {
    public static void main(String[] args) throws NatFilterException, IOException, TransformerException, NatDocumentException {

        // regle le probleme du parser par défaut merdique.
        System.setProperty(
                "javax.xml.parsers.DocumentBuilderFactory",
                "org.apache.xerces.jaxp.DocumentBuilderFactoryImpl"
        );
        System.setProperty(
                "javax.xml.xpath.XPathFactory:http://java.sun.com/jaxp/xpath/dom",
                "com.sun.org.apache.xpath.internal.jaxp.XPathFactoryImpl"
        );

        // -> ⠷⠸⠏⠸⠖
        testG2RuleAndStylist("à peu près",
                "nat://user/joe/fr-g2-rules/fr-g2.xml",
                "nat://system/xsl/ressources/styles_default.xml",
                "nat://system/xsl/dicts/hyph_fr_nat.dic");

        // -> à peu près
        testG2RuleAndStylist("à peu près",
                "nat://user/joe/fr-g2-rules/abrege1.xml",
                "nat://user/joe/stylist/styles1.xml",
                "nat://user/joe/hyphenation-dictionary/hyph_fr_nat.dic");

    }

    public static void testG2RuleAndStylist(String text, String xsl_g2_dict, String stylist_xml, String hyph_dict_txt) throws NatFilterException, NatDocumentException, IOException, TransformerException {
        GestionnaireErreur ge = new GestionnaireErreur();

        AfficheurAnsiConsole afficheurConsole = new AfficheurAnsiConsole(LogLevel.ULTRA);
        afficheurConsole.setLogLevel(LogLevel.DEBUG);
        ge.addAfficheur(afficheurConsole);

        NatFilterOptions nfo = new NatFilterOptions();
        //nfo.setOption(NatFilterOption.debug_dyn_res_show,"true");
        nfo.setOption(NatFilterOption.MODE_G2, "true");
        nfo.setOption(NatFilterOption.XSL_G2_DICT, xsl_g2_dict);
        nfo.setOption(NatFilterOption.Stylist, stylist_xml);
        nfo.setOption(NatFilterOption.XSL_FR_HYPH_DIC, hyph_dict_txt);
        UserDocumentLoader adhocUserDocumentLoader = new UserDocumentLoader() {
            @Override
            public byte[] loadDocument(String username, String kind, String name) {
                if (kind.equals("fr-g2-rules")) {
                    // keeps only "name" part of NatFilterOption.XSL_G2_DICT option and get file in resource dicts
                    InputStream is = NatStaticResolver.getNatResourceAsInputStream(URI.create("/xsl/dicts/" + name));
                    try {
                        byte[] data = is.readAllBytes();
                        return data;
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                } else if (kind.equals("stylist")) {
                    // always return the default file
                    InputStream is = NatStaticResolver.getNatResourceAsInputStream(URI.create("/xsl/ressources/styles_default.xml"));
                    try {
                        byte[] data = is.readAllBytes();
                        return data;
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                } else if (kind.equals("hyphenation-dictionary")) {
                    // always return the default file
                    InputStream is = NatStaticResolver.getNatResourceAsInputStream(URI.create("/xsl/dicts/hyph_fr_nat.dic"));
                    try {
                        byte[] data = is.readAllBytes();
                        return data;
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                }
                return new byte[0];
            }
        };

        NatFilterConfigurator configurator = new NatFilterConfigurator(nfo, ge, adhocUserDocumentLoader);

        NatFilterChain trans = new NatFilterChain(configurator);

        trans.addNewFilter(ConvertisseurTexte.class);
        trans.addNewFilter(FragmentIdentifier.class);
        trans.addNewFilter(TranscodeurNormal.class);
        //trans.addNewFilter(BrailleTableModifierJava.class);

        NatDocument inDoc = new NatDocument();
        inDoc.setString(text);
        NatDocument outDoc = trans.run(inDoc);
        String outString = outDoc.getString();

        System.out.println(outString);

//        transcriptor.addNewFilter(CleanupFilter.class);

    }
}
