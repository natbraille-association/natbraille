package org.natbraille.core;

import java.io.IOException;

/**
 * UserDocumentLoader is used by NatDynamicResolver to load user data referred by URLs
 * UserDocumentLoader is extended by UserDocuments , defining a CRUD interface
 * which is in turn implemented by db based UserDocumentsDB or fs based UserDocumentsFilesystem
 *
 * The reasons why NatDynamicResolver cannot use UserDocuments are :
 * - it would cause a circular dependency problem between natbraille-core and natbraille-editor-project ;
 * - list and write operations are not relevant in natbraille-core
 */
public interface UserDocumentLoader {
    /**
     * Load a document
     * TODO : what if name contains PATH separator ?
     * @param user user name
     * @param kind document kind
     * @param name file name
     * @return the document data if succeeded, null otherwise
     *
     */
    public byte[] loadDocument(String username, String kind, String name) throws IOException;
}
