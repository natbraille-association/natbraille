package org.natbraille.core;

//import com.jcabi.manifests.Manifests;

/**
 * uses /revision/core resources files to determine svn version and status
 *
 * @author vivien
 *
 */
public class CoreVersion {

    private static String version;
    private static String build;

    public static String getBuild() throws Exception {
        if (build == null) {
            build = get("natbraille-core-build");
        }
        return build;
    }

    public static String getVersion() throws Exception {
        if (version == null) {
            version = get("natbraille-core-version");
        }
        return version;
    }

    public static String getDescription() throws Exception {
        getBuild();
        getVersion();
        if ((build == null) && (version == null)) {
            return "Natbraille";
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append((version != null) ? version : "");
            sb.append(" ");
            sb.append((build != null) ? build : "");
            return sb.toString();
        }

    }

    private static String get(String whatKey) throws Exception {
        /*String what;
        try {
            what = Manifests.read(whatKey);
        } catch (Exception e) {
            what = null;
        }
        return what;*/
        String what = null;
        return what;
    }

    @Override
    public String toString() {
        try {
            return getDescription();
        } catch (Exception ex) {
            return ex.getMessage();
        }

    }

}
