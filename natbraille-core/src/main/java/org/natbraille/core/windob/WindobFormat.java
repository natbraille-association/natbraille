/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.natbraille.core.windob;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;

/**
 *
 * @author vivien
 */
public class WindobFormat {
    
   public static Document newWindobDomDocument(EntityResolver er, ErrorHandler eh) throws ParserConfigurationException {

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        dbf.setValidating(true);
        dbf.setIgnoringComments(true);
        dbf.setIgnoringElementContentWhitespace(false);
        dbf.setFeature("http://apache.org/xml/features/dom/defer-node-expansion", false);

        DocumentBuilder docBuilder = dbf.newDocumentBuilder();
        docBuilder.setEntityResolver(er);
        docBuilder.setErrorHandler(eh);

        Document doc = docBuilder.newDocument();
        DOMImplementation domImpl = doc.getImplementation();
        DocumentType doctype = domImpl.createDocumentType("doc:doc", null, "nat://system/xsl/mmlents/windob.dtd");
        
        doc.appendChild(doctype);
        doc.appendChild(doc.createElementNS("espaceDoc", "doc:doc"));
        doc.setXmlVersion("1.1");
        // emit
        //System.out.println("windob" + ((DOMImplementationLS) domImpl).createLSSerializer().writeToString(doc));
        return doc;
    }

}
