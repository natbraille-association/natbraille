/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.natbraille.core.tools;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
@Deprecated
public class FileToolKit {

	/** méthode privée ajoute les entetes XML a une string donnée et l'enregistre ds un fichier
	 * @param stringToSave string du contenu texte
	 * @param fileOut fichier xml de sortie
	 * @return true si ça s'est bien passé
	 */
	
	public static boolean ajouteEntete(String stringToSave, String fileOut)
	{
		boolean retour = false;
		try
		{
			//gest.afficheMessage("\n** Ajout des entêtes et création fichier temp impression", Nat.LOG_VERBEUX);
			//il faut retranscrire en UTF-8 avant le traitement XSL
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileOut),"UTF-8"));
			//ajout entete
			bw.write("<?xml version=\"1.1\" encoding=\"UTF-8\"?><textconv>");
			bw.write(stringToSave.replaceAll("&","&amp;").replaceAll(Character.toString((char)12), "&#xC;").replaceAll("<","&lt;"));
			//gest.AfficheMessage ("Fichier "+fileName,Nat.LOG_VERBEUX)
			bw.write("</textconv>");
			bw.close();
			retour = true;
		}
		catch (IOException e)
		{
			System.err.println("erreur d'ajout d'entete dans: " + e);
			//gest.afficheMessage("Erreur entrée/sortie ajout entête impression", Nat.LOG_SILENCIEUX);
		}
		return retour;
	}
}
