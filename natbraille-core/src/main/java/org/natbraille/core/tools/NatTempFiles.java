package org.natbraille.core.tools;

/*
 * NatBrailleServer 
 * A web frontend for NAT, an universal Translator,
 * Copyright (C) 2012 Vivien Guillet, Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS sheFOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class NatTempFiles {

	private static File baseDir = new File(System.getProperty("java.io.tmpdir") + "/nat-braille/");
	private static Path getRoot() {
		return baseDir.toPath();
	}

	/**
	 * get an absolute path of a potentially relative path whithin the nat
	 * temporary directory "/tmp/nat-braille/super/genial.txt" <=>
	 * "/super/genial.txt" <=> "super/genial.txt"
	 * 
	 * @param path
	 *            the absolute or relative path
	 * @return the full path in the filesystem
	 */
	private static Path getFullPath(Path path) {
		Path fullPath = null;
		if (path.toString().startsWith(getRoot().toString())) {
			// dirs is already a full path
			fullPath = path;
		} else {
			// dirs is relative to the baseDir, add the base dir
			fullPath = new File(getRoot().toFile().getAbsolutePath() + File.separator + path).toPath();
		}
		return fullPath;
	}

	/**
	 * creates a temporary file within nat temporary dir
	 * 
	 * @param dirs
	 *            path within nat temporary dir, created if not existent. the
	 *            path can represent 1. an absolute path in the filesystem
	 *            pointing to somwhere inside nat temporary base as given by
	 *            getRoot(), or a relative path in the nat temp filesystem
	 * @param prefix
	 *            prefix of the temp file
	 * @param suffix
	 *            suffix of the temp file
	 * @return the newley created temporary file
	 * @throws IOException
	 */
	public static File newFile(Path dirs, String prefix, String suffix) throws IOException {

		Path fullPath = getFullPath(dirs);

		// create base directory if it doesn't already exists
		fullPath.toFile().mkdirs();

		// create a new file within
		File tmpFile = File.createTempFile(prefix, suffix, fullPath.toFile());
		return tmpFile;
	}

	/**
	 * creates a temporary file within nat temporary dir
	 * 
	 * @param dirs
	 *            path within nat temporary dir, created if not existent. the
	 *            path can represent 1. an absolute path in the filesystem
	 *            pointing to somwhere inside nat temporary base as given by
	 *            getRoot(), or a relative path in the nat temp filesystem
	 * @param prefix
	 *            prefix of the temp file
	 * @return the Path of the created temporary dir
	 * @throws IOException
	 */
	public static Path newDir(Path dirs, String prefix) throws IOException {

		Path fullPath = getFullPath(dirs);

		// create base directory if it doesn't already exists
		fullPath.toFile().mkdirs();

		// create a new directory within
		Path resu = Files.createTempDirectory(fullPath, prefix);
		resu.toFile().mkdirs();
		return resu;
	}
	
	
/*
	public static void main(String[] args) throws Exception {
		System.err.println(getRoot());

		// Path zou = newDir(new File("temporaire/chiant/très").toPath(),
		// "traitementnumero");
		Path zou = newDir(new File("super/camion").toPath(), "traitementnumero");
		System.err.println(zou.toString());

		Path zou2 = newDir(new File("super/camion2").toPath(), "traitementnumero");
		System.err.println(zou2.toString());

		File zouFile = newFile(zou2, "mylife", ".txt");
		System.err.println(zouFile.toString());

		File zsdouFile = newFile(new File("zou").toPath(), "mylife", ".txt");
		System.err.println(zsdouFile.toString());

	}
*/
}
