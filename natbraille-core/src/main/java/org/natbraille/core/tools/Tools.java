/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.natbraille.core.tools;

import java.util.ArrayList;

public class Tools {

	/**
	 * Splits a string using a separator which is regarded as a single caracter
	 * if doubled
	 * 
	 * @return a String[] of splited values
	 * @param s
	 *            : the string
	 * @param sep
	 *            : the separator
	 */
	static public String[] intelliSplit(String s, String sep) {
		ArrayList<String> z = new ArrayList<String>();
		String curString = "";
		String[] ca = s.split(""); // each letter is an array element
		int i = 0;
		while (i < (ca.length - 1)) {
			String c = ca[i];
			String n = ca[i + 1];
			if (c.equals(sep)) {
				if (n.equals(sep)) { // double separateur => caractere simple
					curString += c;
					i++;
				} else { // veritable separateur => nouvelle string
					z.add(curString);
					curString = new String();
				}
			} else {
				curString += c;
			}// caractere normal
			i++;
		}
		// dernier element
		if (i <= ca.length) {
			curString += ca[i];
		}
		z.add(curString);
		return z.toArray(new String[0]);
	}
	/**
	 * produces correct formatting for the publicId/systemId string
	 * @param publicId
	 * @param systemId
	 * @return a String containing the SYSTEM keyword if publicId is not set
	 */
	public static String getPublicIdSystemIdString(String publicId,String systemId){
		StringBuilder sb = new StringBuilder();

		boolean hasPublic = (publicId != null)&&(!publicId.isEmpty());
		boolean hasSystem = (systemId != null)&&(!systemId.isEmpty());

		if (hasPublic && hasSystem){
			sb.append(publicId).append(" ").append('"').append(systemId).append('"');
		} else if (hasPublic){
			sb.append(publicId);			
		} else if (hasSystem){			
			sb.append("SYSTEM").append(" \"").append(systemId).append('"');
		}

		return sb.toString();
	}
}
