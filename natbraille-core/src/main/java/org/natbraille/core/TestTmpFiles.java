package org.natbraille.core;

import org.natbraille.core.document.NatDocument;
import org.natbraille.core.document.NatDocumentException;
import org.natbraille.core.filter.NatFilter;
import org.natbraille.core.filter.NatFilterChain;
import org.natbraille.core.filter.NatFilterConfigurator;
import org.natbraille.core.filter.NatFilterException;
import org.natbraille.core.filter.convertisseur.*;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.core.filter.transcodeur.TranscodeurNormal;
import org.natbraille.core.gestionnaires.AfficheurAnsiConsole;
import org.natbraille.core.gestionnaires.GestionnaireErreur;
import org.natbraille.core.gestionnaires.LogLevel;

import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.nio.file.Path;

public class TestTmpFiles {
    public static void main(String[] args) throws NatFilterException, IOException, TransformerException, NatDocumentException {

        System.setProperty(
                "javax.xml.parsers.DocumentBuilderFactory",
                "org.apache.xerces.jaxp.DocumentBuilderFactoryImpl"
        );
        System.setProperty(
                "javax.xml.xpath.XPathFactory:http://java.sun.com/jaxp/xpath/dom",
                "com.sun.org.apache.xpath.internal.jaxp.XPathFactoryImpl"
        );
        tempTmpFile("String" );

    }

    public static void tempTmpFile(String text ) throws NatFilterException, NatDocumentException, IOException, TransformerException {
        GestionnaireErreur ge = new GestionnaireErreur();

        AfficheurAnsiConsole afficheurConsole = new AfficheurAnsiConsole(LogLevel.ULTRA);
        afficheurConsole.setLogLevel(LogLevel.NONE);
        ge.addAfficheur(afficheurConsole);

        NatFilterOptions nfo = new NatFilterOptions();
        /*
        nfo.setOption(NatFilterOption.debug_do_not_cleanup_temp_files,"true");
        nfo.setOption(NatFilterOption.debug_document_write_options_file,"true");
        nfo.setOption(NatFilterOption.debug_document_write_temp_file,"true");
        nfo.setOption(NatFilterOption.debug_dyn_xsl_write_temp_file,"true");
    */
        nfo.setOption(NatFilterOption.debug_do_not_cleanup_temp_files,"true");
        nfo.setOption(NatFilterOption.debug_document_write_options_file,"false");
        nfo.setOption(NatFilterOption.debug_document_write_temp_file,"false");
        nfo.setOption(NatFilterOption.debug_dyn_xsl_write_temp_file,"true");

//        nfo.setOption(NatFilterOption.MODE_G2, "true");
  //      nfo.setOption(NatFilterOption.XSL_G2_DICT, xsl_g2_dict);
        NatFilterConfigurator configurator = new NatFilterConfigurator(nfo, ge);

        NatFilterChain trans = new NatFilterChain(configurator);

        //NatFilter trans = new ConvertisseurTexte(nfo,ge);

        if (false) {
            NatDocument inDoc = new NatDocument();
            // from ODT
            trans.addNewFilter(ConvertisseurTexte.class);
            //  trans.addNewFilter(ConvertisseurTexte.class);
            trans.addNewFilter(FragmentIdentifier.class);
            trans.addNewFilter(TranscodeurNormal.class);
            //trans.addNewFilter(BrailleTableModifierJava.class);
            inDoc.setString( text );
        } else {
            trans.addNewFilter(Convertisseur2ODT.class);
            trans.addNewFilter(ConvertisseurOpenOffice.class);
            trans.addNewFilter(ConvertisseurXHTML.class);
            trans.addNewFilter(FragmentIdentifier.class);
            trans.addNewFilter(TranscodeurNormal.class);

            for ( int i = 0 ; i < 20 ; i++){
                NatDocument inDoc = new NatDocument();

                inDoc.setPath(Path.of("/home/vivien/pr-src/natbraille/UnBienBeauDocument.odt"));
                NatDocument outDoc = trans.run(inDoc);
                String outString = outDoc.getString();
                System.out.println(outString);
            }

        }
        System.out.println("DONE");



//        transcriptor.addNewFilter(CleanupFilter.class);

    }
}
