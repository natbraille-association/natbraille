/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.natbraille.core;

import org.natbraille.brailletable.system.SystemBrailleTables;
import org.natbraille.core.document.NatDocument;
import org.natbraille.core.filter.NatFilter;
import org.natbraille.core.filter.NatFilterChain;
import org.natbraille.core.filter.NatFilterConfigurator;
import org.natbraille.core.filter.NatFilterException;
import org.natbraille.core.filter.convertisseur.ConvertisseurTexte;
import org.natbraille.core.filter.transcodeur.TranscodeurNormal;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.core.gestionnaires.AfficheurConsole;
import org.natbraille.core.gestionnaires.GestionnaireErreur;
import org.natbraille.core.gestionnaires.LogLevel;

/******************** aVIRER
 * ****************
 */
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * UNUSED, DO NOT USE , MUST BE REMOVED
 * @author vivien
 */
public class G2LookupGenerator {

    static String MODE_MATH_SPECIFIC_NOTATION = "false";
    
    @FunctionalInterface
    interface NoopFilterFunction  {
        void apply(NatDocument indoc ) throws Exception;
    }
    private static NatFilter createNoopFilter(NatFilterOptions nfo,GestionnaireErreur ge,NoopFilterFunction nooopop) throws NatFilterException {
        return new NatFilter(nfo, ge) {

            @Override
            protected NatDocument process(NatDocument indoc) throws Exception {
                try {
                    nooopop.apply(indoc);
                } catch (Exception e){
                    // noop cannot fail
                }
                return indoc;                
            }
            
            @Override
            protected String getPrettyName() {
                return "show";
            }
        };
    }
    public static NatFilter getMepTranscriptor() throws NatFilterException {

        System.setProperty("javax.xml.parsers.DocumentBuilderFactory",
                "org.apache.xerces.jaxp.DocumentBuilderFactoryImpl");
        System.setProperty("javax.xml.xpath.XPathFactory:http://java.sun.com/jaxp/xpath/dom",
                "com.sun.org.apache.xpath.internal.jaxp.XPathFactoryImpl");

        GestionnaireErreur ge = new GestionnaireErreur();

        //AfficheurAnsiConsole afficheurConsole = new AfficheurAnsiConsole(LogLevel.NONE);
        AfficheurConsole afficheurConsole = new AfficheurConsole(LogLevel.NONE);
        ge.addAfficheur(afficheurConsole);

        NatFilterOptions nfo = new NatFilterOptions();
        // nfo.setOption(NatFilterOption.MODE_DETRANS, "true");
        // nfo.setOption(NatFilterOption.MODE_MATH_ALWAYS_PREFIX, "true");
        nfo.setOption(NatFilterOption.MODE_MATH_SPECIFIC_NOTATION, MODE_MATH_SPECIFIC_NOTATION);
        nfo.setOption(NatFilterOption.FORMAT_LINE_LENGTH, "30");
        nfo.setOption(NatFilterOption.FORMAT_OUTPUT_BRAILLE_TABLE,SystemBrailleTables.CBFr1252);
        nfo.setOption(NatFilterOption.LAYOUT_PAGE_EMPTY_LINES_MODE,"3");
        nfo.setOption(NatFilterOption.MODE_G2, "true");
        
        /*nfo.setOption(NatFilterOption.MODE_LIT, "false");
        nfo.setOption(NatFilterOption.MODE_G2, "false");
        nfo.setOption(NatFilterOption.LAYOUT, "false");
         */

        NatFilterConfigurator configurator = new NatFilterConfigurator(nfo, ge);
        NatFilterChain nfc = new NatFilterChain(configurator);

        
        nfc.addNewFilter(ConvertisseurTexte.class);
        
        nfc.addFilters(createNoopFilter(nfo, ge,(indoc) -> {
            System.out.println("=============================== ConvertisseurTexte");
            System.out.println(indoc.getString());                    
        }));
        /*
        
        nfc.addNewFilter(ConvertisseurXHTML.class);
        nfc.addFilters(createNoopFilter(nfo, ge,(indoc) -> {
            System.out.println("=============================== convertiXML (xhtml2interne.xsl)");
            System.out.println(indoc.getString());                    
        }));
*/
        nfc.addNewFilter(TranscodeurNormal.class);
        nfc.addFilters(createNoopFilter(nfo, ge,(indoc) -> {
            System.out.println("=============================== transcoded");
            System.out.println(indoc.getString());                    
        }));

//        nfc.addNewFilter(PresentateurMEP.class);
//        nfc.addNewFilter(BrailleTableModifierJava.class);
//        nfc.addFilters(createNoopFilter(nfo, ge,(indoc) -> {
//            System.out.println("=============================== MEP");
//            System.out.println(indoc.getString());                    
//        }));
    
        
        

        return nfc;
    }

    public static void generateG2Lookup(String[] args) throws Exception {
        NatFilter transcriptor = getMepTranscriptor();
        // String filename = "./documents/testDivers.odt";
        // String filename = "./documents/testmep/testDiversSimple.odt";
        // String filename = "./documents/testmep/testMaths.odt";
        //String filename = "./documents/testmep/tests.odt";
        //byte[] fb = Files.readAllBytes(Paths.get(filename));
        // String text = "convient";
        String text = "Transformations";
        NatDocument inDoc = new NatDocument();
        inDoc.setString(text);
        NatDocument outDoc;
        try {
            outDoc = transcriptor.run(inDoc);
            System.out.println("done!");
        } catch (NatFilterException ex) {
            Logger.getLogger(Nat.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void main(String[] args) throws Exception {
        generateG2Lookup(args);

    }

}
