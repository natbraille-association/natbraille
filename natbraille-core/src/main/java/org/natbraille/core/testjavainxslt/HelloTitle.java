package org.natbraille.core.testjavainxslt;

import net.sf.saxon.Configuration;
import net.sf.saxon.dom.DocumentWrapper;
import net.sf.saxon.expr.XPathContext;
import net.sf.saxon.lib.ExtensionFunctionCall;
import net.sf.saxon.lib.ExtensionFunctionDefinition;
import net.sf.saxon.ma.arrays.SimpleArrayItem;
import net.sf.saxon.om.*;
import net.sf.saxon.s9api.*;
import net.sf.saxon.trans.XPathException;
import net.sf.saxon.tree.iter.NodeListIterator;
import net.sf.saxon.tree.linked.ElementImpl;
import net.sf.saxon.value.SequenceType;
import net.sf.saxon.value.StringValue;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.*;
import org.xml.sax.InputSource;

import java.io.StringReader;

//https://www.saxonica.com/html/documentation12/extensibility/java-to-xdm-conversion.html

public class HelloTitle extends ExtensionFunctionDefinition {
    @Override
    public StructuredQName getFunctionQName() {
        return new StructuredQName("natj", "http://natbraille.org/saxon-extension", "hello-title");
    }

    @Override
    public SequenceType[] getArgumentTypes() {
        return new SequenceType[]{SequenceType.SINGLE_NODE};
    }

    @Override
    public SequenceType getResultType(SequenceType[] suppliedArgumentTypes) {
        return SequenceType.SINGLE_NODE;
    }

    private static Document makeNodeList1(String v0) {

       System.setProperty("javax.xml.parsers.DocumentBuilderFactory", "org.apache.xerces.jaxp.DocumentBuilderFactoryImpl");
       // System.setProperty("javax.xml.parsers.DocumentBuilderFactory", "net.sf.saxon.dom.DocumentBuilderFactoryImpl");
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = null;
        try {
            db = dbf.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            throw new RuntimeException(e);
        }
        Document document = db.newDocument();

        Element div = document.createElement("div");
        //h0.setTextContent(v0);
        document.appendChild(div);

        Element h1 = document.createElement("h1");
        h1.setTextContent(v0);
        div.appendChild(h1);


        return document;
        /*
        NodeList nodeList = new NodeList() {
            @Override
            public Node item(int index) {
                return h1;
            }

            @Override
            public int getLength() {
                return 1;
            }
        };
        Sequence s = (Sequence) nodeList;
        return s;

         */
    }

    @Override
    public ExtensionFunctionCall makeCallExpression() {
        return new ExtensionFunctionCall() {

            @Override
            public Sequence call(XPathContext context, Sequence[] arguments) throws XPathException {
                Processor processor = (Processor)context.getConfiguration().getProcessor();
                net.sf.saxon.s9api.DocumentBuilder documentBuilder = processor.newDocumentBuilder();
                Document document = null;
                //try {
                    document = makeNodeList1("oooooo");
                //} catch (ParserConfigurationException e) {
//                    throw new RuntimeException(e);
  //              }
                XdmNode wrapped = documentBuilder.wrap(document);
                return wrapped.getUnderlyingNode();
            }

            public Sequence call2(XPathContext context, Sequence[] arguments) throws XPathException {
                String v0 = arguments[0].toString();
                /*
                System.out.println("got argument " + v0);
                //return makeNodeList1(v0);
                Document document = makeNodeList1(v0);
                SAXSource source = new SAXSource();

                InputSource inputSource = new InputSource();
                source.setInputSource(inputSource);
                TreeInfo t = context.getConfiguration().buildDocumentTree(source);
                return (Sequence) arguments;
                */
                /** wrap
                Document node = makeNodeList1(v0);
                DocumentWrapper documentWrapper = new DocumentWrapper(node,"http://my-url.com/url",context.getConfiguration());
                 // documentWrapper.wrap(node);//.getFirstChild());
                 return documentWrapper.getRootNode();
                 **/

                /** so
                Processor processor  = new Processor();
                net.sf.saxon.s9api.DocumentBuilder documentBuilder = processor.newDocumentBuilder();
                Document document = makeNodeList1(v0);
                XdmNode wrapped = documentBuilder.wrap(document.getFirstChild());
                return wrapped.getUnderlyingNode();
                **/

                Configuration.ApiProvider processor0 = context.getConfiguration().getProcessor();
                Processor processor = (Processor)processor0;
                //net.sf.saxon.s9api.DocumentBuilder documentBuilder = processor.newDocumentBuilder();
                net.sf.saxon.s9api.DocumentBuilder documentBuilder = processor.newDocumentBuilder();
                Document document = makeNodeList1(v0);

                XdmNode wrapped = documentBuilder.wrap(document.getFirstChild());
                return wrapped.getUnderlyingNode();


            }
        };
    }
}
