package org.natbraille.core.testjavainxslt;

import net.sf.saxon.Configuration;
import net.sf.saxon.TransformerFactoryImpl;
import net.sf.saxon.s9api.*;
import org.xml.sax.InputSource;

import javax.xml.transform.*;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;

// https://stackoverflow.com/questions/66471400/saxon-he-java-extension-functions-problem-configuring

public class testJavaInXsl {

    public static Source getSourceFromPath(Path path) throws IOException {
        byte bytes[] = Files.readAllBytes(path);
        InputStream inputStream = new ByteArrayInputStream(bytes);
        InputSource inputSource = new InputSource(inputStream);
        Source source = new SAXSource(inputSource);
        return source;
    }

    public static Transformer getTransformer(Path path, Configuration configuration) throws TransformerConfigurationException, IOException {
        Source source = getSourceFromPath(path);
        TransformerFactoryImpl transformerFactory = new TransformerFactoryImpl();
        transformerFactory.setConfiguration(configuration);
        Transformer transformer = transformerFactory.newTransformer(source);
        return transformer;
    }

    public static void main2(String[] args) throws IOException, TransformerException {
      System.setProperty(
                "javax.xml.parsers.DocumentBuilderFactory",
                "org.apache.xerces.jaxp.DocumentBuilderFactoryImpl"
        );
        System.setProperty(
                "javax.xml.xpath.XPathFactory:http://java.sun.com/jaxp/xpath/dom",
                "com.sun.org.apache.xpath.internal.jaxp.XPathFactoryImpl"
        );

        Configuration configuration = new Configuration();
        configuration.registerExtensionFunction(new ShiftLeft());
        configuration.registerExtensionFunction(new HelloString());
        configuration.registerExtensionFunction(new HelloTitle());
        Processor processor = new Processor();
        configuration.setProcessor(processor);


        Transformer transformer = getTransformer(Path.of("./natbraille-core/src/main/java/org/natbraille/core/testjavainxslt/testjavafunction.xsl"), configuration);
        Source xmlSource = getSourceFromPath(Path.of("./natbraille-core/src/main/java/org/natbraille/core/testjavainxslt/testjavafunction.xml"));
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(byteArrayOutputStream, "UTF8"));
        StreamResult out = new StreamResult(bufferedWriter);
        transformer.transform(xmlSource, out);
        System.out.println(byteArrayOutputStream.toString("UTF-8"));

    }

    public static void main(String[] args) throws IOException, TransformerException, SaxonApiException {
       /*
       System.setProperty(

                "javax.xml.parsers.DocumentBuilderFactory",
                "org.apache.xerces.jaxp.DocumentBuilderFactoryImpl"
        );
        System.setProperty(
                "javax.xml.xpath.XPathFactory:http://java.sun.com/jaxp/xpath/dom",
                "com.sun.org.apache.xpath.internal.jaxp.XPathFactoryImpl"
        );
        */
        Processor processor = new Processor();
        processor.registerExtensionFunction(new ShiftLeft());
        processor.registerExtensionFunction(new HelloString());
        processor.registerExtensionFunction(new HelloTitle());
        processor.getUnderlyingConfiguration().setProcessor(processor);

        Path xslPath = Path.of("./natbraille-core/src/main/java/org/natbraille/core/testjavainxslt/testjavafunction.xsl");
        XsltCompiler xsltCompiler = processor.newXsltCompiler();
        XsltExecutable xsltExecutable = xsltCompiler.compile(xslPath.toFile());
        XsltTransformer xsltTransformer = xsltExecutable.load();

        // xml source
        Path xmlPath = Path.of("./natbraille-core/src/main/java/org/natbraille/core/testjavainxslt/testjavafunction.xml");
        InputStream inputStream = new FileInputStream(xmlPath.toFile());
        InputSource inputSource = new InputSource(inputStream);
        Source source = new SAXSource(inputSource);
        xsltTransformer.setSource(source);

        // destination
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Serializer serializerDestination = processor.newSerializer(byteArrayOutputStream);
        xsltTransformer.setDestination(serializerDestination);

        // transform
        xsltTransformer.transform();

        // dbg print
        System.out.println(byteArrayOutputStream.toString("UTF-8"));


    }
}
