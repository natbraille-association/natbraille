<xsl:stylesheet
        version="1.0"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        xmlns:natj="http://natbraille.org/saxon-extension">

    <xsl:template match="/">
        <html>
            <p>Le shift</p>
            <p>
                <xsl:value-of select="natj:shift-left(1,1)"/>
            </p>
            <p>
                <xsl:value-of select="natj:hello-string('joe')"/>
            </p>
            <xxx>
            <xsl:value-of select="//p" />
            </xxx>
            <xsl:copy-of select="natj:hello-title(//p)" />
        </html>
    </xsl:template>

</xsl:stylesheet>