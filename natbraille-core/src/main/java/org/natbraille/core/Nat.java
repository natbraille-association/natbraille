/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.core;

//import org.natbraille.core.commmandLine.NatCommandLine;

import org.natbraille.brailletable.system.SystemBrailleTables;
import org.natbraille.core.filter.NatFilterChain;
import org.natbraille.core.filter.convertisseur.FragmentIdentifier;
import org.natbraille.core.filter.presentateur.PresentateurMEP;
import org.natbraille.core.filter.transcodeur.TranscodeurNormal;
import org.natbraille.core.tools.LibreOfficeTools;

import org.natbraille.core.gestionnaires.AfficheurAnsiConsole;
import org.natbraille.core.gestionnaires.GestionnaireErreur;
import org.natbraille.core.gestionnaires.LogLevel;

import org.natbraille.core.document.NatDocument;
import org.natbraille.core.filter.NatFilterConfigurator;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.filter.options.NatFilterOptions;

//import org.natbraille.core.document.NatDocumentXMLReader; //bruno :?
import org.natbraille.core.gestionnaires.MessageKind;

import java.nio.file.Path;

public class Nat {


    /**
     * @param args
     * @throws Exception
     */
    public static void welcome(String[] args) throws Exception {

        // regle le probleme du parser par défaut merdique.
        System.setProperty(
                "javax.xml.parsers.DocumentBuilderFactory",
                "org.apache.xerces.jaxp.DocumentBuilderFactoryImpl"
        );
        System.setProperty(
                "javax.xml.xpath.XPathFactory:http://java.sun.com/jaxp/xpath/dom",
                "com.sun.org.apache.xpath.internal.jaxp.XPathFactoryImpl"
        );

        System.err.println(CoreVersion.getDescription());

        GestionnaireErreur ge = new GestionnaireErreur();

        //AfficheurAnsiConsole afficheurConsole = new AfficheurAnsiConsole(LogLevel.NORMAL);
        AfficheurAnsiConsole afficheurConsole = new AfficheurAnsiConsole(LogLevel.ULTRA);
        //afficheurConsole.setLogLevel(LogLevel.SILENT);
        afficheurConsole.setLogLevel(LogLevel.DEBUG);//Bruno
        afficheurConsole.setBlackList(MessageKind.STEP);

        ge.addAfficheur(afficheurConsole);

        NatFilterOptions nfo = new NatFilterOptions();
//        nfo.setOption(NatFilterOption.FORMAT_OUTPUT_BRAILLE_TABLE, SystemBrailleTables.TbFr2007);
        nfo.setOption(NatFilterOption.FORMAT_OUTPUT_BRAILLE_TABLE, SystemBrailleTables.MixedFrUnicode);
        nfo.setOption(NatFilterOption.LAYOUT, "true");
        nfo.setOption(NatFilterOption.MODE_LIT_FR_MAJ_DOUBLE, "true");
        nfo.setOption(NatFilterOption.LAYOUT_NUMBERING_MODE, "'bb'");
        //nfo.setOption(NatFilterOption.MODE_G2, "false");
        nfo.setOption(NatFilterOption.MODE_G2, "true");

        // nfo.setOption(NatFilterOption.XSL_G2_DICT, "nat://user/fr-g2-rules/fr-g2.xml");

        nfo.setOption(NatFilterOption.FORMAT_LINE_LENGTH, "40");
        nfo.setOption(NatFilterOption.FORMAT_PAGE_LENGTH, "6");
        nfo.setOption(NatFilterOption.LAYOUT_NUMBERING_START_PAGE, "0");


        nfo.setOption(NatFilterOption.debug_document_show, "true");
        nfo.setOption(NatFilterOption.debug_document_show_max_char,"1000000");
        nfo.setOption(NatFilterOption.debug_document_write_options_file, "true");

        nfo.setOption(NatFilterOption.debug_document_write_temp_file, "true");
        nfo.setOption(NatFilterOption.debug_dyn_res_show, "false");
        nfo.setOption(NatFilterOption.debug_dyn_xsl_show, "false");

        nfo.setOption(NatFilterOption.debug_dyn_ent_res_show, "false");
        nfo.setOption(NatFilterOption.debug_xsl_processing, "true");
        nfo.setOption(NatFilterOption.debug_dyn_xsl_write_temp_file,"true");

        nfo.setOption(NatFilterOption.debug_do_not_cleanup_temp_files,"true");

        //nfo.setOption(NatFilterOption.LAYOUT,"false");
        //nfo.setOption(NatFilterOption.preserveTags,"true");



        NatFilterConfigurator configurator = new NatFilterConfigurator(nfo, ge);
        //Transcriptor transcriptor = new Transcriptor(configurator);
        NatFilterChain transcriptor = new NatFilterChain(configurator);
        //transcriptor.addNewFilter(ConvertisseurTexte.class);

        //transcriptor.addNewFilter(ConvertisseurOpenOffice.class);
        //transcriptor.addNewFilter(ConvertisseurXML.class);
        transcriptor.addNewFilter(FragmentIdentifier.class);

        transcriptor.addNewFilter(TranscodeurNormal.class);
        transcriptor.addNewFilter(PresentateurMEP.class);
        //transcriptor.addNewFilter(BrailleTableModifierJava.class);


//        transcriptor.addNewFilter(CleanupFilter.class);


        StringBuilder sb = new StringBuilder();
        for ( int i = 0 ; i < 50 ; i++ ){
            sb.append("Bon sang, la clavicule, dis donc. ");
        }

        double mean = 0;
        double count = 1;
        for ( int r = 0 ; r < count ; r++) {
            for (String message : new String[]{
                    //("La première transcription est un peu longue. ".repeat(2) + "\n").repeat(2)
                    //("Les tout à fait convient. Merci; réaction fraction depuis chiens")
                    ("convient Bonjour bonjour déménagement")
                    //"\n   Un peu plus looooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooongue.",
                    //"Cependant, il faut CONSTATER",
                    //"Une petite pousse-au-crime que les 123456 suivantes SONT NETTEMENT PLUS RAPIDES : tant mieux !"
                    /*"1234 5678 90",
                    "àâç èéêë îï ôœ ùûü",
                    "a, b. c ! d ; e ? f... "*/
            }) {
                System.out.println(r + "/" + count);
                long beginTime = System.currentTimeMillis();

                NatDocument inDoc = new NatDocument();
                //inDoc.setPath(Path.of("/home/vivien/pr-src/natbraille/UnBienBeauDocument.odt"));
                //inDoc.setPath(Path.of("/home/bruno/developpement/natgit/natbraille-core/tests/test.xml"));
                inDoc.setPath(Path.of("./natbraille-core/tests/test.xml"));
                //inDoc.setString(message);
                System.out.println(inDoc.getString());
                //inDoc.getDocument(NatDocumentXMLReader.getXMLReader());

                NatDocument outDoc = transcriptor.run(inDoc);
                System.out.println(outDoc.getString());
                long endime = System.currentTimeMillis();
                // if (message.startsWith("La")){

                //} else {
                mean += (endime - beginTime) / 3.0 / count;
                //}

                java.io.BufferedWriter writer;
                writer = new java.io.BufferedWriter(new java.io.FileWriter("./output_"+r+".brl.xml"));
                writer.write(outDoc.getString());

                writer.close();
            }
        }
        System.out.println("mean:"+mean);
        LibreOfficeTools.stopServer();

    }

    public static String getLicence(String string, String string2) {
        // TODO Auto-generated method stub
        return "GPL v2";
    }
    public static void main(String args[]) throws Exception{
        welcome(args);
/*
        PerfMark.setEnabled(true);
        PerfMark.event("Transc");
        try (TaskCloseable task = PerfMark.traceTask("Transc")) {
            welcome(args);
        }
        PerfMark.event("Transc-end");
        TraceEventViewer.writeTraceHtml();
*/
        /*
        PerfMark.setEnabled(true);
        PerfMark.startTask("Foo.start");
        welcome(args);
        PerfMark.stopTask("Foo.start");
        TraceEventViewer.writeTraceHtml();
*/

    }

    public static class Minimal {
    }
}
