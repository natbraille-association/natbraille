package org.natbraille.core.filter.options;

/**
 * categories of nat options (for user interface automatic presentation of options)
 * @author vivien
 *
 */
public enum NatFilterOptionCategory {

	Infos,

	Mode,
	ModeBase,
	ModeIntegralMaj, 
	ModeIntegralEmph, 
	ModeAbrv, 
	ModeTable,
	ModeDetrans,
	
	Layout, 
	LayoutBase,
	LayoutNumbering, 
	LayoutEmptyLines, 
	LayoutPageBreak, 
	LayoutPageTitles, 
	LayoutPageIndexTable, 
	LayoutNoLayout,

	Format,

	Debug,

	Advanced; 


	public boolean isin(NatFilterOptionCategory ... categs){
		for (NatFilterOptionCategory c : categs){
			if (equals(c)) return true;			
		}
		return false;
	}
}
