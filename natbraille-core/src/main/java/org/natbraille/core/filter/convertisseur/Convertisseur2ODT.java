/*
 /*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.core.filter.convertisseur;

import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Files;

import org.natbraille.core.document.NatDocument;
import org.natbraille.core.filter.NatFilter;
import org.natbraille.core.filter.NatFilterErrorCode;
import org.natbraille.core.filter.NatFilterException;
import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.core.gestionnaires.GestionnaireErreur;
import org.natbraille.core.tools.LibreOfficeTools;

/**
 * This {@link  NatFilter} will convert document of any soffice supported format
 * to odt. Conversion is done using
 * {@link org.natbraille.core.tools.LibreOfficeTools2}
 * <p>
 * @author vivien
 */
public class Convertisseur2ODT extends NatFilter {

    public Convertisseur2ODT(NatFilterOptions options, GestionnaireErreur gestionnaireErreurs) {
        super(options, gestionnaireErreurs);
    }

    @Override
    protected NatDocument process(NatDocument indoc) throws Exception {

        NatDocument odtdoc = new NatDocument();

        File inputFile = indoc.writeTmpFile(getTmpDir(), "anytoodt_src_", ".src");
        File outputFile = odtdoc.reserveTmpFile(getTmpDir(), "anytoodt_resu_", ".odt");

        if (LibreOfficeTools.getSofficeConverter() != null) {
            LibreOfficeTools.getSofficeConverter().convert(inputFile, outputFile);
            odtdoc.setByteBuffer(Files.readAllBytes(outputFile.toPath()));
        } else {
            throw new NatFilterException(NatFilterErrorCode.NO_CONFIGURED_SOFFICE,"");
        }
        
        return odtdoc;
    }

    @Override
    protected String getPrettyName() {
        return "anythingtoodt(soffice)";
    }

}
