package org.natbraille.core.filter.options;

import org.natbraille.core.filter.NatFilterException;
import org.natbraille.core.filter.options.NatFilterOption;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class ExportNatFilterOptionsToCSV {
    public static void main(String[] args) throws NatFilterException, FileNotFoundException {

        StringBuilder sb = new StringBuilder();
        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        for ( NatFilterOption option: NatFilterOption.values()){
            sb.append( option.name() );
            sb.append( "\t" );
            sb.append( option.getDetail().comment() );
            sb.append( "\t" );
            sb.append( option.getDetail().defaultValue() );
            sb.append( "\t" );
            sb.append( String.join( "\t", option.getDetail().possibleValues() ));
            sb.append( "\n" );
        }
        System.out.println(sb);
        PrintWriter out = new PrintWriter("natfilteroptions_export.csv");
        out.print(sb);
        out.close();
    }
}
