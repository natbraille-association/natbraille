/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package org.natbraille.core.filter.presentateur;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.natbraille.core.ConfigNat;
import org.natbraille.core.Nat;
import org.natbraille.core.gestionnaires.MessageKind;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import gestionnaires.GestionnaireErreur;
/**
 * Réalise la présentation du fichier transcrit en appliquant une feuille
 * xsl de transformation, puis en encodant le fichier de sortie dans le
 * charset désiré.
 * @author bruno
 *
 */
public class PresentateurMEP extends PresentateurSans
{
	/** XSL filter for no page layout */
	private static final String NO_LAYOUT_FILTER = "nat://system/xsl/no-mep.xsl";
	/** XSL Filter for page Layout */
	private static final String LAYOUT_FILTER = "nat://system/xsl/miseEnPage.xsl";
	/** XSL Filter for no page layout and tag preservation */
	private static final String NO_LAYOUT_PRESERVE_TAG_FILTER = "nat://system/sl/no-mep-preserve-tag.xsl";

	/** adresse de la feuille xsl de mise en page */
	protected String filtre = ConfigNat.getUserTempFolder()+"xsl-mep.xsl";
	/** adresse du fichier temporaire produit après l'application de la feuille xsl de mise en page */
	protected String ftmp = ConfigNat.getUserTempFolder()+"tmp-pass2.txt";
	
	/**
	 * Creates the xsl-mep.xsl file for page-layout transformations including other xsl stylesheets depending on the configuration stored in <code>configNat</code>
	 * @throws ParserConfigurationException erreur de parsage
	 * @throws TransformerException erreur lors de la transformation
	 */
	private void createXslMep() throws ParserConfigurationException, TransformerException 
	{
		DocumentBuilderFactory fabrique = DocumentBuilderFactory.newInstance();
		DocumentBuilder constructeur= fabrique.newDocumentBuilder();
	    Document docXSL = constructeur.newDocument();
	    // Propriétés de docParam
	    docXSL.setXmlVersion("1.1");
	    docXSL.setXmlStandalone(true);
	    //docXSL.createEntityReference("Table_pour_chaines.ent");
	    
	    //racine
	    Element racine = docXSL.createElement("xsl:stylesheet");
	    racine.setAttribute("version", "2.0");
	    racine.setAttribute("xmlns:xsl", "http://www.w3.org/1999/XSL/Transform");
	    racine.setAttribute("xmlns:xs","http://www.w3.org/2001/XMLSchema");
	    racine.setAttribute("xmlns:saxon","http://icl.com/saxon");
	    racine.setAttribute("xmlns:m","http://www.w3.org/1998/Math/MathML");
	    racine.setAttribute("xmlns:fn","http://www.w3.org/2005/xpath-functions");
	    racine.setAttribute("xmlns:lit","espacelit");
	    racine.setAttribute("xmlns:nat", "http://natbraille/free/fr/xsl");
	    racine.setAttribute("xmlns:doc","espaceDoc");
	    racine.appendChild(docXSL.createComment("Auto-generated file; see PresentateurMEP.java"));
	    racine.appendChild(docXSL.createComment(Nat.getLicence("", "")));

		//FileWriter fichierXSL2 = new FileWriter(filtre);
		Element output = docXSL.createElement("xsl:output");
		output.setAttribute("encoding","UTF-8");
		output.setAttribute("indent","no");
		racine.appendChild(output);
		
		Element paramAll = docXSL.createElement("xsl:include");
		paramAll.setAttribute("href","paramsAll.xsl");
		racine.appendChild(paramAll);
		
		Element paramTrans = docXSL.createElement("xsl:include");
		paramTrans.setAttribute("href","paramsMEP.xsl");
		racine.appendChild(paramTrans);
		
		String fichMEP ;
		if(ConfigNat.getCurrentConfig().getMep())
		{
			fichMEP= LAYOUT_FILTER;
			output.setAttribute("method","text");
			String fichHyphens = ConfigNat.getUserTempFolder()+"hyphens.xsl";
			Element hyph = docXSL.createElement("xsl:include");
			hyph.setAttribute("href",fichHyphens);
			racine.appendChild(hyph);
		}
		else if(ConfigNat.getCurrentConfig().getPreserveTag())
		{
			fichMEP = NO_LAYOUT_PRESERVE_TAG_FILTER;
			output.setAttribute("method","xml");
		}
		else
		{
			output.setAttribute("method","text");
			fichMEP = NO_LAYOUT_FILTER;
			//Element table_chaine = docXSL.createElement("xsl:include");
			//table_chaine.setAttribute("href","Table_pour_chaines.ent");
			//racine.appendChild(table_chaine);
		}
		
		Element mep = docXSL.createElement("xsl:include");
		mep.setAttribute("href",fichMEP);
		racine.appendChild(mep);
		docXSL.appendChild(racine);		    
	    /* Sauvegarde de document dans un fichier */
        Source src = new DOMSource(docXSL);
        
        // Création du fichier de sortie
        File f = new File(filtre);
        Result resultat = new StreamResult(f);
        
        // Configuration du transformer
        TransformerFactory tfabrique = TransformerFactory.newInstance();
        Transformer transformer = tfabrique.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");  
        transformer.setOutputProperty(OutputKeys.VERSION, "1.1");
        transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, ConfigNat.getCurrentConfig().getDTD());
        //transformer.setOutputProperty(OutputKeys.MEDIA_TYPE, "Table_pour_chaines.ent");
        // Transformation
        transformer.transform(src, resultat);
	}

	/**
	 * Redéfinition de {@link PresentateurSans#presenter()}
	 * <p>Réalise la présentation du fichier transcrit en appliquant une feuille
	 * xsl de transformation, puis en encodant le fichier de sortie dans le
	 * charset désiré.</p>
	 */
	@Override
	public boolean presenter() 
	{
		long tempsDebut = System.currentTimeMillis();
		gest.afficheMessage("Début de la mise en forme du document ... ok\n",Nat.LOG_SILENCIEUX);
		boolean retour = true;
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		//configuration de la fabrique
		factory.setNamespaceAware(true);
		factory.setValidating(ConfigNat.getCurrentConfig().getNiveauLog()==Nat.LOG_DEBUG);
		factory.setIgnoringElementContentWhitespace(true);
		factory.setIgnoringComments(true);
		factory.setIgnoringElementContentWhitespace(false);
		try 
		{
			// sinon, génère parfois des null pointer exp au parsage (problème avec les simples quote)
			factory.setFeature("http://apache.org/xml/features/dom/defer-node-expansion", false);
			DocumentBuilder builder = factory.newDocumentBuilder();
			gest.afficheMessage(MessageKind.DEFAULT,"Parsage avec SAX du fichier interne à mettre en page...",Nat.LOG_VERBEUX);
			builder.setErrorHandler(gest);
			gest.afficheMessage("   Entree :"+source+";filtre :"+filtre+";",Nat.LOG_VERBEUX);
			Document doc = builder.parse(new FileInputStream(source),"UTF-8"); //(new File(entree));
			doc.setStrictErrorChecking(true);
			gest.afficheMessage(MessageKind.DEFAULT,"Initialisation et lecture de la feuille de style ...",Nat.LOG_VERBEUX);
			//TransformerFactory transformFactory = TransformerFactory.newInstance();
			TransformerFactory transformFactory = TransformerFactory.newInstance();
			StreamSource styleSource = new StreamSource(new File(filtre));
			// lire le style
			
			Transformer transform = transformFactory.newTransformer(styleSource);
			//transform.setOutputProperty(name, value)
			// conformer le transformeur au style
			DOMSource in = new DOMSource(doc);
			gest.afficheMessage(MessageKind.DEFAULT,"Création du fichier de sortie ...",Nat.LOG_VERBEUX);
			// Création du fichier de sortie
			File file = new File(ftmp);
			///Result resultat = new StreamResult(fichier);
			StreamResult out = new StreamResult(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file),"UTF-8")));
			gest.afficheMessage(MessageKind.DEFAULT,"Transformation du document ...",Nat.LOG_VERBEUX);
			transform.transform(in, out);
			// transformer selon le style 
			
			//application du traitement de l'encodage (ce que faisait avant PresentateurSans)
			source = ftmp;
			gest.afficheMessage("ok\n** Encodage...",Nat.LOG_VERBEUX);
			addImages(source);
			retour = encode();
			tempsExecution = System.currentTimeMillis() - tempsDebut;
		}
		catch (Exception e)  
		{
			gest.setException(e);
			gest.gestionErreur();
			retour = false;
		}

		return retour;
	}

}
