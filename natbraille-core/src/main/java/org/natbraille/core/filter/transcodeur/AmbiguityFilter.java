/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.natbraille.core.filter.transcodeur;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.List;
import org.natbraille.core.ambiguity.Ambiguity;
import org.natbraille.core.ambiguity.AmbiguityResolver;
import org.natbraille.core.ambiguity.AmbiguityResolverUI;
import org.natbraille.core.document.NatDocument;
import org.natbraille.core.document.Utils;
import org.natbraille.core.filter.NatFilter;
import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.core.gestionnaires.GestionnaireErreur;

/**
 *
 * @author vivien
 */
public class AmbiguityFilter extends NatFilter {
    
    /**
     * Amn
     */
    private final AmbiguityResolver ambiguityResolver ;
    public AmbiguityFilter(NatFilterOptions options, GestionnaireErreur gestionnaireErreurs) {
        super(options, gestionnaireErreurs);
        this.ambiguityResolver = new AmbiguityResolver(gestionnaireErreurs);
    }
    public AmbiguityFilter(NatFilterOptions options, GestionnaireErreur gestionnaireErreurs, AmbiguityResolverUI ambiguityResolverUI) {
        super(options, gestionnaireErreurs);                    
        this.ambiguityResolver = new AmbiguityResolver(gestionnaireErreurs,ambiguityResolverUI);
    }

    public AmbiguityResolver getAmbiguityResolver() {
        return ambiguityResolver;
    }

    @Override
    protected NatDocument process(NatDocument indoc) throws Exception {
        NatDocument outDoc = new NatDocument();
        
        InputStream withAmbiguity = Utils.toInputStream(indoc.getBytebuffer());        
        ByteArrayOutputStream withoutAmbiguity = new ByteArrayOutputStream();
        getAmbiguityResolver().resolveAmbiguity(withAmbiguity,withoutAmbiguity);
        
        /*
         withAmbiguity = Utils.toInputStream(indoc.getBytebuffer());        
         List<Ambiguity> ambs = getAmbiguityResolver().getAllAmbiguities(withAmbiguity);
         for (Ambiguity ambiguity : ambs ){
             System.out.print("-------- ");
             ambiguity.afficheAmbiguity();
         }
        */
        
        outDoc.setByteArrayOutputStream(withoutAmbiguity);
        outDoc.setBrailletable(indoc.getBrailletable());
        outDoc.forceCharset(Charset.forName("UTF-8"));
       
        return outDoc;
    }

    @Override
    protected String getPrettyName() {
        return "g2 ambiguity resolver";
    }
    
}
