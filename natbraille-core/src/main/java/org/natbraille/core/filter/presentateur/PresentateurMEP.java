/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.core.filter.presentateur;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.OutputStreamWriter;
import java.net.URI;
import javax.xml.transform.Transformer;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import org.natbraille.core.NatStaticResolver;
import org.natbraille.core.document.NatDocument;
import org.natbraille.core.document.NatDocumentXMLReader;
import org.natbraille.core.filter.NatTransformerFilter;
import org.natbraille.core.NatDynamicResolver;
import org.natbraille.core.gestionnaires.LogLevel;
import org.natbraille.core.gestionnaires.MessageContents;
import org.natbraille.core.gestionnaires.MessageKind;
import org.w3c.dom.Document;
import org.xml.sax.XMLReader;

public class PresentateurMEP extends NatTransformerFilter {

    public PresentateurMEP(NatDynamicResolver ndr) {
        super(ndr);
    }

    @Override
    protected void initialize() throws Exception {
        getGestionnaireErreur().afficheMessage(MessageKind.INFO, MessageContents.Tr("make transformer "), LogLevel.VERBOSE);
        setTransformer(new URI(NatStaticResolver.XSL_NAT_TMP_XSL_MEP));
    }

    @Override
    protected NatDocument process(NatDocument indoc) throws Exception {
        NatDocument outDoc = null;

//		try {
//		
        getGestionnaireErreur().afficheMessage(MessageKind.INFO, MessageContents.Tr("get document"), LogLevel.VERBOSE);
        // document
        // document
        //Document doc = indoc.getDocument(getUriResolver().getXMLReader());
        //Document doc = indoc.getDocument(NatDocumentXMLReader.getXMLReader());
        XMLReader anXmlReader = NatDocumentXMLReader.getSimpleXMLReader();
        Document doc = NatDocumentXMLReader.toSimpleDocument(indoc.getInputstream(),anXmlReader);

        doc.setStrictErrorChecking(false);
/*
        // debug print fragment position
        XPath xPath = XPathFactory.newInstance().newXPath();
        Double numStartingLine = (Double) xPath.evaluate("*[@numStartingLine][1]/@numStartingLine", doc.getDocumentElement(), XPathConstants.NUMBER);
        System.err.println("++++++++++++++++++++>+++++ " + numStartingLine);
        Double numStartingPage = (Double) xPath.evaluate("*[@numStartingPage][1]/@numStartingPage", doc.getDocumentElement(), XPathConstants.NUMBER);
       System.err.println("++++++++++++++++++++>+++++ " + numStartingPage);
*/
      Double  numStartingLine = 1.0;
       Double numStartingPage = 1.0;
        
        // get Document as DOMSource)
        DOMSource in = new DOMSource(doc);
        in.setSystemId(doc.getDocumentURI());

        getGestionnaireErreur().afficheMessage(MessageKind.INFO, MessageContents.Tr("make output buffer"), LogLevel.VERBOSE);

        // Création du buffer de sortie
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        BufferedWriter fcible = new BufferedWriter(new OutputStreamWriter(baos, "UTF8"));
        StreamResult out = new StreamResult(fcible);

        getGestionnaireErreur().afficheMessage(MessageKind.INFO, MessageContents.Tr("transform"), LogLevel.VERBOSE);

        Transformer transformer = getTransformer();
        if (!numStartingPage.isNaN()) {
            transformer.setParameter("numStartingPage", numStartingPage.intValue());
        }
        if (!numStartingLine.isNaN()) {
            transformer.setParameter("numStartingLine", numStartingLine.intValue());
        } else {

        }
         if ((!numStartingLine.isNaN()) || (!numStartingPage.isNaN())) {
            transformer.setParameter("doNotAddEmptyLinesAtEnd", true);
        }
        
        getTransformer().transform(in, out);

        outDoc = new NatDocument();
        getGestionnaireErreur().afficheMessage(MessageKind.INFO, MessageContents.Tr("set output document buffer"), LogLevel.VERBOSE);
        outDoc.setByteBuffer(baos.toByteArray());

//		} catch (Exception e){
//			e.printStackTrace();
//			System.exit(0);
//		}
//		
        return outDoc;

    }

    @Override
    protected String getPrettyName() {
        return "PRESENTATEUR (mise en page)";
    }

//	/** XSL filter for no page layout */
//	private static final String NO_LAYOUT_FILTER = ConfigNat.getInstallFolder()+"xsl/no-mep.xsl";
//	/** XSL Filter for page Layout */
//	private static final String LAYOUT_FILTER = ConfigNat.getInstallFolder()+"xsl/miseEnPage.xsl";
////    private static final String LAYOUT_FILTER = ConfigNat.getInstallFolder()+"xsl/miseEnPageSansIterate.xsl";
//	/** XSL Filter for no page layout and tag preservation */
//	private static final String NO_LAYOUT_PRESERVE_TAG_FILTER = ConfigNat.getInstallFolder()+"xsl/no-mep-preserve-tag.xsl";
//
//	
//	/** adresse de la feuille xsl de mise en page */
//	protected String filtre = ConfigNat.getUserTempFolder()+"xsl-mep.xsl";
//	/** adresse du fichier temporaire produit après l'application de la feuille xsl de mise en page */
//	protected String ftmp = ConfigNat.getUserTempFolder()+"tmp-pass2.txt";
    /**
     * Constructeur
     *
     * @param g une instance de {@link GestionnaireErreur}
     * @param src L'adresse du fichier transcrit à présenter
     * @param sor l'adresse de la sortie
     * @param cs encodage du fichier de sortie
     * @param tab la table braille de sortie
     */
    /*
     public PresentateurMEP(GestionnaireErreur g, String cs, String src, String sor, String tab)
     {
     super(g,  cs,src, sor, tab);
     try {createXslMep();}
     catch (ParserConfigurationException e) {e.printStackTrace();}
     catch (TransformerException e) {e.printStackTrace();}
     }
     */
    /**
     * Redéfinition de {@link PresentateurSans#presenter()}
     * <p>
     * Réalise la présentation du fichier transcrit en appliquant une feuille
     * xsl de transformation, puis en encodant le fichier de sortie dans le
     * charset désiré.</p>
     */
    /*
     @Override
     public boolean presenter() 
     {
     long tempsDebut = System.currentTimeMillis();
     gest.afficheMessage("\nDébut de la mise en forme du document ... ok\n",Nat.LOG_SILENCIEUX);
     boolean retour = true;
     DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
     //configuration de la fabrique
     factory.setNamespaceAware(true);
     factory.setValidating(ConfigNat.getCurrentConfig().getNiveauLog()==Nat.LOG_DEBUG);
     factory.setIgnoringElementContentWhitespace(true);
     factory.setIgnoringComments(true);
     factory.setIgnoringElementContentWhitespace(false);
     try 
     {
     // sinon, génère parfois des null pointer exp au parsage (problème avec les simples quote)
     factory.setFeature("http://apache.org/xml/features/dom/defer-node-expansion", false);
     DocumentBuilder builder = factory.newDocumentBuilder();
     gest.afficheMessage("** Parsage avec SAX du fichier interne à mettre en page...",Nat.LOG_VERBEUX);
     builder.setErrorHandler(gest);
     gest.afficheMessage("   Entree :"+source+";filtre :"+filtre+";",Nat.LOG_VERBEUX);
     Document doc = builder.parse(new FileInputStream(source),"UTF-8"); //(new File(entree));
     doc.setStrictErrorChecking(true);
     gest.afficheMessage("ok\n** Initialisation et lecture de la feuille de style ...",Nat.LOG_VERBEUX);
     //TransformerFactory transformFactory = TransformerFactory.newInstance();
     TransformerFactory transformFactory = TransformerFactory.newInstance();
     StreamSource styleSource = new StreamSource(new File(filtre));
     // lire le style
			
     Transformer transform = transformFactory.newTransformer(styleSource);
     //transform.setOutputProperty(name, value)
     // conformer le transformeur au style
     DOMSource in = new DOMSource(doc);
     gest.afficheMessage("ok\n** Création du fichier de sortie ...",Nat.LOG_VERBEUX);
     // Création du fichier de sortie
     File file = new File(ftmp);
     ///Result resultat = new StreamResult(fichier);
     StreamResult out = new StreamResult(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file),"UTF-8")));
     gest.afficheMessage("ok\n** Transformation du document ...",Nat.LOG_VERBEUX);
     transform.transform(in, out);
     // transformer selon le style 
			
     //application du traitement de l'encodage (ce que faisait avant PresentateurSans)
     source = ftmp;
     gest.afficheMessage("ok\n** Encodage...",Nat.LOG_VERBEUX);
     addImages(source);
     retour = encode();
     tempsExecution = System.currentTimeMillis() - tempsDebut;
     }
     catch (Exception e)  
     {
     gest.setException(e);
     gest.gestionErreur();
     retour = false;
     }

     return retour;
     }

     */
}
