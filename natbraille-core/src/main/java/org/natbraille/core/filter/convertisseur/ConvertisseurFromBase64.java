/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * Author : Vivien Guillet <vivien.guillet@laposte.net>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.core.filter.convertisseur;


import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import org.natbraille.core.document.NatDocument;
import org.natbraille.core.filter.NatFilter;
import org.natbraille.core.filter.NatFilterErrorCode;
import org.natbraille.core.filter.NatFilterException;
import org.natbraille.core.filter.NatTransformerFilter;
import org.natbraille.core.NatDynamicResolver;
import org.natbraille.core.gestionnaires.GestionnaireErreur;
import org.natbraille.core.gestionnaires.LogLevel;
import org.natbraille.core.gestionnaires.MessageKind;
import org.natbraille.core.tools.NatTempFiles;
import writer2xhtml.api.Config;
import writer2xhtml.api.Converter;
import writer2xhtml.api.ConverterFactory;
import writer2xhtml.api.ConverterResult;
import writer2xhtml.office.MIMETypes;

import java.util.Base64;

/**
 * set document from base64 string
 * @author vivien
 *
 */
public class ConvertisseurFromBase64 extends NatTransformerFilter {

    //private final String WRITER2LATEX_CONFIG_FILE  = "cleanxhtml.xml";
	@Override
	protected String getPrettyName() {
		return "Base64 vers Binary";
	}

	public ConvertisseurFromBase64(NatDynamicResolver ndr) {
		super(ndr);
	}
    
// 		GestionnaireErreur gest = getGestionnaireErreur();

// 		//Create a writertoXHTML converter
// 		Converter converter = ConverterFactory.createConverter(MIMETypes.XHTML_MATHML);

// 		//Create a writertoXHTML configuration
// 		Config config = converter.getConfig();
// 		NatDocument xhtmlNatDocument = null;

// 		File targetFile = null;
// 		try
// 		{
                        
// 			InputStream w2lConfIs = this.getClass().getResourceAsStream(WRITER2LATEX_CONFIG_FILE);

// 			//	config.read(new FileInputStream("/home/vivien/temps/narrelease/trunk/writer2latex/xhtml/config/cleanxhtml.xml"));
// 			config.read(w2lConfIs);
// 			config.setOption("inputencoding","utf-8");
// 			config.setOption("use_named_entities", "true");

// 			InputStream is = inDoc.getInputstream();


// 			targetFile = NatTempFiles.newFile(getTmpDir(),"xhtml",".xhtml");
// 			gest.afficheMessage(MessageKind.INFO,"fichier xhtml: "+targetFile.getAbsolutePath(),LogLevel.VERBOSE);

// 			// TODO ?????????????
// 			ConverterResult result = converter.convert(is, targetFile.getName());
// //			ConverterResult result = converter.convert(is, targetFile.getAbsolutePath());
// 			result.write(new File(targetFile.getParent()));
// 			xhtmlNatDocument = new NatDocument();
// 			xhtmlNatDocument.setInputstream(new FileInputStream(targetFile));


// 		} catch(Exception e){
// 			String details = "Problème lors de la conversion avec Writer2XHTML "+e.getLocalizedMessage();
// 			throw new NatFilterException(NatFilterErrorCode.NAT_FILTER_EXTERNAL_ERROR, details,e);
// 		}

// 		//conversion de xhtml vers interne
// 		/*
// 		try {
// 			ConvertisseurXML convXML = new ConvertisseurXML(getUriResolver());
// 			convXML.setParentTmpDir(getTmpDir());
// 			outDoc = convXML.run(xhtmlNatDocument);
// 		} catch (NatFilterException e) {
// 			throw e;
// 		} catch (Exception e){
// 			String details = e.getLocalizedMessage();
// 			throw new NatFilterException(getClass(), NatFilterErrorCode.NAT_FILTER_EXTERNAL_ERROR, details); 
// 		}
// 		 */
// 		outDoc = xhtmlNatDocument;
// 		return outDoc;
// 	}
	@Override
	protected NatDocument process(NatDocument indoc) throws Exception {	
	    NatDocument outDoc  = null;
	    try {
		outDoc = new NatDocument();	    
		String base64string = indoc.getString();
		System.out.println("base64string:"+base64string);
		byte[] decoded = Base64.getDecoder().decode(base64string);
		System.out.println("decoded:"+decoded);
		outDoc.setByteBuffer( decoded );
		//		outdoc.setBrailleTable(indoc.getBrailleTable())
		//		outdoc.setBrailleTable(indoc.getBrailleTable())
		    
		return outDoc;
	    } catch (Exception e){

	    }
	    return outDoc;
	}



}
