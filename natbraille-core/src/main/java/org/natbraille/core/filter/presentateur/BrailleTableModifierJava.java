/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.core.filter.presentateur;

import java.io.ByteArrayOutputStream;
import java.nio.charset.Charset;
import org.natbraille.brailletable.BrailleTable;
import org.natbraille.brailletable.BrailleTableException;
import org.natbraille.brailletable.BrailleTables;
import org.natbraille.brailletable.StreamConverter;
import org.natbraille.brailletable.system.SystemBrailleTables;
import org.natbraille.core.document.NatDocument;
import org.natbraille.core.filter.NatFilter;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.core.gestionnaires.GestionnaireErreur;
import org.natbraille.core.gestionnaires.LogLevel;
import org.natbraille.core.gestionnaires.MessageContents;
import org.natbraille.core.gestionnaires.MessageKind;

/**
 * this filter converts a utf8 Braille Unicode NatDocuments to the Braille
 * Table/Encoding pair specified in NatFilterOptions.
 *
 * @author vivien
 */
public class BrailleTableModifierJava extends NatFilter {

    @Override
    protected String getPrettyName() {
        return "BRAILLE TABLE TRANSLITERATION (java)";
    }

    public BrailleTableModifierJava(NatFilterOptions options, GestionnaireErreur gestionnaireErreurs) {
        super(options, gestionnaireErreurs);
    }

    @Override
    protected void initialize() throws Exception {

    }

    @Override
    protected NatDocument process(NatDocument indoc) throws Exception {

        // create braille table stream converter
        try {
            String inCharsetName = "UTF-8";
            BrailleTable inBrailleTable = BrailleTables.forName(SystemBrailleTables.BrailleUTF8);
            String outCharsetName = getOption(NatFilterOption.FORMAT_OUTPUT_ENCODING);
            BrailleTable outBrailleTable = BrailleTables.forName(getOption(NatFilterOption.FORMAT_OUTPUT_BRAILLE_TABLE));

            StreamConverter sc = new StreamConverter(
                    inCharsetName,
                    inBrailleTable,
                    outCharsetName,
                    outBrailleTable
            );

            // convert
            indoc.forceCharset(Charset.forName(inCharsetName));
            indoc.setBrailletable(inBrailleTable);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            sc.convert(indoc.getInputstream(), baos);
            
            // create output document
            NatDocument outputDoc = new NatDocument();
            outputDoc.setBrailletable(BrailleTables.forName(getOption(NatFilterOption.FORMAT_OUTPUT_BRAILLE_TABLE)));
            outputDoc.forceCharset(Charset.forName(getOption(NatFilterOption.FORMAT_OUTPUT_ENCODING)));
            outputDoc.setByteBuffer(baos.toByteArray());
            return outputDoc;
        } catch (BrailleTableException bte) {
            MessageContents mc = MessageContents.Tr("error with braille table : {0}").setValue(bte.getLocalizedMessage());
            getGestionnaireErreur().afficheMessage(MessageKind.ERROR, mc, LogLevel.NORMAL);
            throw bte;
        }
        
    }

}
//
//	private HashMap<Character, Character> transliterationMap= null;
//	private BrailleTable outputBrailleTable = null;
//
//	
//	
//
//	/**
//	 * @return the transliterationMap
//	 */
//	private HashMap<Character, Character> getTransliterationMap() {
//		return transliterationMap;
//	}
//
//	/**
//	 * @param transliterationMap the transliterationMap to set
//	 */
//	private void setTransliterationMap(HashMap<Character, Character> transliterationMap) {
//		this.transliterationMap = transliterationMap;
//	}
//	/**
//	 * @return the outBrailleTable
//	 */
//	private BrailleTable getOutputBrailleTable() {
//		return outputBrailleTable;
//	}
//
//	/**
//	 * @param outBrailleTable the outBrailleTable to set
//	 */
//	private void setOutputBrailleTable(BrailleTable outBrailleTable) {
//		this.outputBrailleTable = outBrailleTable;
//	}
//
//	/**
//	 * 
//	 * @param options
//	 * @param gestionnaireErreurs
//	 */
//	public BrailleTableModifierJava(NatFilterOptions options, GestionnaireErreur gestionnaireErreurs) {
//		super(options, gestionnaireErreurs);
//	}
//
//	@Override
//	protected void initialize() throws Exception {
//
//		URI inputTableUri = new URI("nat://system/xsl/tablesBraille/brailleUTF8.ent");
//		URI outputTableUri = null;
//
//		BrailleTable outBrailleTable = null;
//
//		String outputTableOption = getOption(NatFilterOption.FORMAT_OUTPUT_BRAILLE_TABLE);
//		String outputCharsetOption = getOption(NatFilterOption.FORMAT_OUTPUT_ENCODING);
//
//		if (outputTableOption.endsWith(".ent")){
//			outputTableUri = new URI(outputTableOption);			
//			outBrailleTable = new BrailleTable(outputTableUri,"custom",Charset.forName(outputCharsetOption));
//		} else {
//			outBrailleTable = BrailleTables.forName(outputTableOption);
//			outputTableUri = outBrailleTable.getUri();
//		}
//		setOutputBrailleTable(outBrailleTable);
//		
//				//auto
//		/*
//		try {
//			outBrailleTable = BrailleTables.getStatic(outputTableUri);
//		} catch (Exception e){
//			String contents = "cannot find braille table at URI :"+outputTableUri;
//			throw new NatFilterException(getClass(),NatFilterErrorCode.NO_BRAILLE_TABLE_AT_URI,contents);
//		}
//		 */
//
//		/*
//		 * transliterate
//		 */
//		HashMap<Character, Character> tmap = BrailleTable.getTransliterationMap(inputTableUri,outputTableUri);
//		setTransliterationMap(tmap);
//	
//	}
//
//	@Override
//	protected NatDocument process(NatDocument indoc) throws Exception {
//
//
//		HashMap<Character, Character> tmap = getTransliterationMap();
//
//		String input = indoc.getString();
//		String output = BrailleTable.transliterate(tmap, input);
//
//
//		BrailleTable outBrailleTable = getOutputBrailleTable();
//		
//		tmap = getTransliterationMap();
//		
//		Charset outputEncoding = outBrailleTable.getCharset();
//
//		byte[] buffer_outencoded = output.getBytes(outputEncoding);
//
//		NatDocument outDoc = new NatDocument();
//		outDoc.forceCharset(outputEncoding);
//		outDoc.setByteBuffer(buffer_outencoded);		
//		outDoc.setBrailletable(outBrailleTable);
//
//
//
//
//		return outDoc;
//	}
//
//	@Override
//	protected String getPrettyName() {
//		return "BRAILLE TABLE TRANSLITERATION (java)";
//	}
//
//
//}
