package org.natbraille.core.filter;

import org.natbraille.core.NatDynamicResolver;

import java.lang.reflect.Constructor;
import java.util.HashMap;

import org.natbraille.core.UserDocumentLoader;
import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.core.gestionnaires.GestionnaireErreur;

/**
 * build {@link NatFilter} from filter class name. Uses previously set class-object
 * assocation to to get the set of correct actual filter constructor parameters.
 *
 * @author vivien
 */
public class NatFilterConfigurator {

    /**
     * association of natfilter constructor classes and objects.
     */
    private final HashMap<Class<?>, Object> objectForClass = new HashMap<>();


    /**
     * @return the objectForClass
     */
    private HashMap<Class<?>, Object> getObjectForClass() {
        return objectForClass;
    }

    /**
     * associate a class with an object
     *
     * @param c
     * @param o
     * @return
     */
    public final NatFilterConfigurator set(Class<?> c, Object o) {
        getObjectForClass().put(c, o);
        return this;
    }

    /**
     * associate the class of an object with the object
     *
     * @param o
     * @return
     */
    public final NatFilterConfigurator set(Object o) {
        return set(o.getClass(), o);
    }

    /**
     * get the object associated with the class
     *
     * @param c
     * @return
     */
    public Object get(Class<?> c) {
        return getObjectForClass().get(c);
    }

    /**
     * Build an array of objects matching each constructor parameters class.
     *
     * @param constructor
     * @return
     * @throws NatFilterException
     */
    public Object[] getConstructorParams(Constructor<?> constructor) throws NatFilterException {

        Class<?>[] consParamsClasses = constructor.getParameterTypes();
        Object consParamsObjects[] = new Object[consParamsClasses.length];

        int idx = 0;

        for (Class<?> c : consParamsClasses) {
            if (getObjectForClass().containsKey(c)) {
                Object o = getObjectForClass().get(c);
                consParamsObjects[idx] = o;
            } else {
                throw new NatFilterException(NatFilterErrorCode.FACTORY_CANNOT_BUILD_FILTER_PARAM, c.getName());
            }
            idx++;
        }


        return consParamsObjects;
    }


    /**
     * builds a new nat filter
     *
     * @param filterClass
     * @return
     * @throws NatFilterException
     */
    public NatFilter newFilter(Class<? extends NatFilter> filterClass) throws NatFilterException {
        NatFilter newFilter = null;

        try {

            // get filterClass constructor list
            Constructor<?>[] filterConstructors = filterClass.getDeclaredConstructors();

            // try every existing constructor
            for (Constructor<?> c : filterConstructors) {
                try {
                    Object[] cp = getConstructorParams(c);
                    return (NatFilter) c.newInstance(cp);
                } catch (Exception e) {
                    //
                }
            }

            return newFilter;
        } catch (Exception e) {
            throw new NatFilterException(NatFilterErrorCode.FACTORY_CANNOT_BUILD_FILTER, filterClass.toString(), e);
        }
    }

    public NatFilterChain newFilterChain() {
        NatFilterChain nfo = new NatFilterChain(this);
        return nfo;
    }

    public GestionnaireErreur getGestionnaireErreur() {
        return (GestionnaireErreur) get(GestionnaireErreur.class);
    }

    public NatFilterOptions getNatFilterOptions() {
        return (NatFilterOptions) get(NatFilterOptions.class);
    }

    public NatDynamicResolver getNatDynamicResolver() {
        return (NatDynamicResolver) get(NatDynamicResolver.class);

    }

    /**
     * Empty NatFilterConfigurator.
     */
    public NatFilterConfigurator() {
        set(NatFilterConfigurator.class, this);
//                set(this);
    }

    /**
     * Build NatFilterConfigurator from NatFilterOptions and GestionnaireErreur.
     * Creates a new NatDynamicResolver from both
     *
     * @param natFilterOptions
     * @param ge
     */
    public NatFilterConfigurator(NatFilterOptions natFilterOptions, GestionnaireErreur ge) {
        set(natFilterOptions);
        set(ge);
        set(new NatDynamicResolver(natFilterOptions, ge));
        set(this);
    }

    /**
     * Build NatFilterConfigurator from NatFilterOptions, a GestionnaireErreur and a UserDocumentLoader
     * Creates a new NatDynamicResolver from all three
     * @param natFilterOptions
     * @param ge
     * @param userDocumentLoader
     */
    public NatFilterConfigurator(NatFilterOptions natFilterOptions, GestionnaireErreur ge, UserDocumentLoader userDocumentLoader) {
        set(natFilterOptions);
        set(ge);
        set(userDocumentLoader);
        set(new NatDynamicResolver(natFilterOptions, ge, userDocumentLoader));
        set(this);
    }
}
