/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.natbraille.core.filter.convertisseur;

import org.natbraille.core.document.NatDocument;
import org.natbraille.core.filter.NatTransformerFilter;
import org.natbraille.core.NatDynamicResolver;
import org.natbraille.core.windob.WindobFormat;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.ls.DOMImplementationLS;

/**
 * Giac Mathml -> interne
 *
 * @author vivien
 */
public class ConvertisseurGiacMathml extends NatTransformerFilter {

    @Override
    protected String getPrettyName() {
        return "Giac Mathml -> interne";
    }

    public ConvertisseurGiacMathml(NatDynamicResolver ndr) {
        super(ndr);
    }

    @Override
    protected NatDocument process(NatDocument indoc) throws Exception {

        NatDocument outDoc = new NatDocument();

        String mathmlString = indoc.getString();
        System.err.println("== giacInput ==\n" + mathmlString + "\n====");

        // interpret input document as Mathml xml document
        NatDocument inXmlDoc = new NatDocument();
        String xmlString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        String doctype = "<!DOCTYPE math PUBLIC \"-//W3C//DTD MathML 2.0//EN\" \n\"http://www.w3.org/Math/DTD/mathml2/mathml2.dtd\">";
        String xml = xmlString + "\n" + doctype + "\n" + mathmlString;
        inXmlDoc.setString(xml);
        Document inplaceDoc = inXmlDoc.getDocument(getUriResolver().getXMLReader());

        // modification : msqrt childs must be packed under an mrow.
        NodeList allMsqrt = inplaceDoc.getElementsByTagName("msqrt");
        for (int i = 0; i < allMsqrt.getLength(); i++) {
            Node msqrtNode = allMsqrt.item(i);
            // get childs of msqrt
            NodeList msqrtChildren = msqrtNode.getChildNodes();
            if ((msqrtChildren.getLength() == 1) && ("mrow".equals(msqrtChildren.item(0).getNodeName()))) {
                // one child, and is an mrow, do nothing.
            } else {
                // pack everything under an mrow
                Element newMrowElement = inplaceDoc.createElement("mrow");
                Node removeMe[] = new Node[msqrtChildren.getLength()];
                for (int j = 0; j < msqrtChildren.getLength(); j++) {
                    Node oldChild = msqrtChildren.item(j);
                    Node newChild = oldChild.cloneNode(true);
                    newMrowElement.appendChild(newChild);
                    removeMe[j] = oldChild;
                }
                // remove old nodes;
                for (Node node : removeMe) {
                    msqrtNode.removeChild(node);
                }
                msqrtNode.appendChild(newMrowElement);
            }
        }

        // make windob dom document
        Document windobDomDocument = WindobFormat.newWindobDomDocument(getUriResolver(), getUriResolver());

        // clone math childs into windob dom document
        NodeList mathElements = inplaceDoc.getElementsByTagName("math");
        for (int i = 0; i < mathElements.getLength(); i++) {
            Element phrase = windobDomDocument.createElement("phrase");
            Element math = windobDomDocument.createElementNS("http://www.w3.org/1998/Math/MathML", "math");
            NodeList mathChilds = mathElements.item(i).getChildNodes();
            for (int j = 0; j < mathChilds.getLength(); j++) {
                math.appendChild(windobDomDocument.adoptNode(mathChilds.item(j).cloneNode(true)));
            }
            phrase.appendChild(math);
            windobDomDocument.getDocumentElement().appendChild(phrase);
        }

        // print
        System.err.println("== windob2   ==\n" + ((DOMImplementationLS) windobDomDocument.getImplementation()).createLSSerializer().writeToString(windobDomDocument) + "\n====");

        outDoc = new NatDocument();
        outDoc.setDocument(windobDomDocument);
        System.err.println("== windob3   ==\n" + outDoc.getString());
        return outDoc;

    }
}
