/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.natbraille.core.filter;

import org.natbraille.core.NatDynamicResolver;
import java.net.URI;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.sax.SAXSource;
import org.natbraille.core.NatStaticResolver;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.gestionnaires.LogLevel;
import org.natbraille.core.gestionnaires.MessageKind;


/**
 * a {@link NatFilter} capable of doing Xslt transformations
 * using a {@link NatDynamicResolver} for entities and 
 * url resolution
 * @author vivien
 *
 */
public abstract class NatTransformerFilter extends NatFilter  {


	private Transformer transformer = null;
	
	/**
	 * @return the transformer
	 */
	protected Transformer getTransformer() {
		return transformer;
	}

	/**
	 * @param transformer the transformer to set
	 */
	protected void setTransformer(Transformer transformer) {
		this.transformer = transformer;
	}
	/**
	 * @param transformer the transformer to set
	 * @throws TransformerException 
	 * @throws NatFilterException 
	 */
	protected void setTransformer(URI uri) throws TransformerException, NatFilterException {
		setTransformer(makeTransformer(uri));
	}
	/**
	 * 
	 * @param options
	 * @param gestionnaireErreur
	 * @param ndr
	 */
	public NatTransformerFilter(NatDynamicResolver ndr) {
		super(ndr.getOptions(),ndr.getGestionnaireErreur());
		setUriResolver(ndr);
	}	
	

	private NatDynamicResolver uriResolver = null;		
	public final NatDynamicResolver getUriResolver() {
		return uriResolver;
	}		
	private final void setUriResolver(NatDynamicResolver ndr) {
		this.uriResolver = ndr;
	
	}	
	
	/**
	 * display init message of transformer filter
	 */
	@Override
	protected boolean getDisplayInitializationMessage() {
		return true;
	}
	
	/**
	 * constructs an Xslt {@link Transformer} from an uri
	 * using the resolver. URI must be resolvable by the instance's 
	 * {@link NatDynamicResolver}
	 * @param uri the uri of the transformer document
	 * @return
	 * @throws TransformerException
	 * @throws NatFilterException
	 */
	protected Transformer makeTransformer(URI uri) throws TransformerException, NatFilterException {
		

		Transformer transformer = null;
		String filterUri = uri.toString();		
		
		try  {
			
			// not safe at all, because the uri resolver is shared by filters
			// but it's only for debug... so we won't build a uri resolver for
			// every filter.
			if (getOptionAsBoolean(NatFilterOption.debug_dyn_xsl_write_temp_file)) {
				getUriResolver().setTmpDir(getTmpDir());
			}
			getGestionnaireErreur().afficheMessage(MessageKind.INFO,"making transformer from "+filterUri, LogLevel.VERBOSE);		
			Source resolvedSource = getUriResolver().resolve(filterUri, null);
			
			SAXSource resolvedSaxSource = null;// = (SAXSource) getUriResolver().resolve(filterUri, null);
			
			// not always the case in theory
			if (resolvedSource.getClass().equals(SAXSource.class)){
				 resolvedSaxSource = (SAXSource) getUriResolver().resolve(filterUri, null);
			} else if (resolvedSource.getClass().equals(DOMSource.class)){
				// TODO...
			}
			
			transformer = getUriResolver().newTransformer(resolvedSaxSource);
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			transformer.setOutputProperty(OutputKeys.VERSION, "1.1");
			transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, NatStaticResolver.WINDOB_DTD_SYSTEM_ID);

	
		} catch (Exception e){
			throw new NatFilterException(NatFilterErrorCode.FILTER_INITIALIZATION_ERROR,"unable to create transformer for uri :"+filterUri,e);			
		}
		return transformer;
	}
}
