/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


package org.natbraille.core.filter.rules;

/** décrit une règle dans nat */
public abstract class Regle
{
	/** Description de la règle ou catégorie de règle
	 * <p>Exemples:
	 * 	<ul>
	 * 		<li>"Locution",</li>
	 * 		<li> "Le préfixe CONTRE ne s'abrège que lorsqu'il est suivi d'un trait d'union")</li>
	 * 	</ul>
	 * </p>
	 **/
	protected String description="";
	/**
	 * Référence de la règle (pour l'instant, issue du manuel d'abrégé de l'AVH).
	 * <p>Conventions de nommage:
	 * 	<ul>
	 * 		<li>Chapitre: <strong>chiffres romains</strong>: la règle est définie dans un chapître (par exemple, locution, mots abrégés par un seul signe, etc)</li>
	 * 		<li>Règle d'abréviation: <strong>chiffre arabes</strong>: n° de la règle dans le manuel</li>
	 * 		<li>Règle non présente dans le manuel: <strong>Source de la règle + référence dans la source</strong>: par exemple "Méthode Le Rest/Perdoux, édition 2008, page x, §y</li>
	 * 	</ul>
	 * <p>
	 * <p>Il est recommandé de donner une référence complète en rappelant le n° du chapitre: par exemple, préférer "II-19" plutôt que "19"</p>
	 */
	protected String reference="";
	
	/**
	 * Vrai si la règle est active dans la configuration
	 */
	protected boolean actif = true;
	
	/**
	 * Vrai si la règle est une règle pédagogique (non nécessaire dans la norme) dans la configuration
	 */
	protected boolean peda = false;
	/**
	 * @param d la description de la règle
	 * @param ref la référence de la règle 
	 * @param a true si règle active
	 */
	public Regle(String d, String ref, boolean a)
	{
		description = d;
		reference = ref;
		actif = a;
	}
	
	/**
	 * @param d la description de la règle
	 * @param ref la référence de la règle 
	 * @param a true si règle active
	 * @param p true si règle pédagogique
	 */
	public Regle(String d, String ref, boolean a, boolean p)
	{
		description = d;
		reference = ref;
		actif = a;
		peda = p;
	}
	/**
	 * @param d la description de la règle
	 * @param ref la référence de la règle 
     */
    public Regle(String d, String ref)
    {
    	description = d;
		reference = ref;
		actif = true;
    }

	/**
	 * Pour obliger la redéfinition de toString() de la Classe Object
	 * @return une chaine représentant la règle
	 */
	@Override
	public abstract String toString();
	
	/**
	 * Vérifie si deux règles sont identiques
	 * @param o instance d'Object à comparer
	 * @return vrai si les règles sont identiques
	 */
	@Override
	public abstract boolean equals(Object o);
	
	/**
	 * Renvoie un noeud xml sous forme de chaine représentant la règle
	 * @return une chaine xml représentant la règle
	 */
	public abstract String getXML();

	/**
     * @param a valeur pour #actif
     */
    public void setActif(boolean a){actif = a;}
    
    /**
     * @return valeur de #actif
     */
    public boolean isActif(){return actif;}
    
    /**
     * @return valeur de {@link #peda}
     */
    public boolean isPeda(){return peda;}
    
    /**
     * @param p valeur pour {@link #peda}
     */
    public void setPeda(boolean p){peda = p;}
    
    
}
