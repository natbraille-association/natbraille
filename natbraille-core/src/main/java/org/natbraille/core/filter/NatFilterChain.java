/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.core.filter;

import java.util.ArrayList;
import org.natbraille.core.document.NatDocument;
import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.core.gestionnaires.GestionnaireErreur;
import org.natbraille.core.gestionnaires.LogLevel;
import org.natbraille.core.gestionnaires.MessageKind;
/**
 * chain of nat filters
 * @author vivien
 *
 */
public class NatFilterChain extends NatFilter 
{

	ArrayList<NatFilter> chain = new ArrayList<>();

	NatFilterConfigurator natFilterConfigurator;

	/**
	 * sets the natFilterConfigurator
	 * @param natFilterConfigurator
	 */
	private void setNatFilterConfigurator(NatFilterConfigurator natFilterConfigurator) {
		this.natFilterConfigurator = natFilterConfigurator;

	}

	/**
	 * @return the natFilterConfigurator
	 */
	protected NatFilterConfigurator getNatFilterConfigurator() {
		return natFilterConfigurator;
	}

	/**
	 * @return the chain
	 */
	protected ArrayList<NatFilter> getChain() {
		return chain;
	}


	/**
	 *	
	 * @param natFilterConfigurator used for filter adding 
	 * by class name ({@see addNewFilter}, must contain {@link GestionnaireErreur};
	 */
	public NatFilterChain(NatFilterConfigurator natFilterConfigurator){
		super((NatFilterOptions) natFilterConfigurator.get(NatFilterOptions.class),
				(GestionnaireErreur) natFilterConfigurator.get(GestionnaireErreur.class));
		setNatFilterConfigurator(natFilterConfigurator);
	}


	/**
	 * a string representation of completed steps
	 * @param steps
	 * @param totalSteps
	 * @return
	 */
	private static String getStepString(int steps, int totalSteps){
		return steps+"/"+totalSteps;
	}

	/**
	 * add filters to the filter chain
	 * @param natFilter
         * @return the filter chain for parameter chaining
	 */
	public NatFilterChain addFilters(NatFilter ... natFilter){
		for (NatFilter f : natFilter){
			getChain().add(f);
		}
                return this;
	}

	/**
	 * prepend filters to the filter chain
	 * @param natFilter
         * @return the filter chain for parameter chaining
	 */
	public NatFilterChain prependFilters(NatFilter ... natFilter){
		for (NatFilter f : natFilter){
			try {
				f.setParentTmpDir(getTmpDir());
			} catch(Exception e){
				getGestionnaireErreur().afficheMessage(MessageKind.WARNING,"unable to set temp directory",LogLevel.NORMAL);
			}
			getChain().add(0, f);
		}
                return this;
	}
        /**
	 * build and append filter to the filter chain
	 * @param natFilter
     * @return the filter chain for parameter chaining
	 * @throws NatFilterException
	 */
	public NatFilterChain addNewFilter(Class<? extends NatFilter> natFilter) throws NatFilterException{
		addFilters(getNatFilterConfigurator().newFilter(natFilter));
                return this;
	}
	/**
	 * build and prepend filter to the filter chain
	 * @param natFilter
     * @return the filter chain for parameter chaining
	 * @throws NatFilterException
	 */
	public NatFilterChain prependNewFilter(Class<? extends NatFilter> natFilter) throws NatFilterException{
		prependFilters(getNatFilterConfigurator().newFilter(natFilter));
                return this;
	}
	public void removeFilters(){
		getChain().clear();
	}
	/**
	 * preProcess is always called after filter initialization
	 * and before actual processing, for the filter
	 * to do document-wise initialization.
	 * @param indoc
	 * @throws NatFilterException
	 */
	protected void preProcess(NatDocument indoc) throws NatFilterException{

	}

	@Override
	protected final NatDocument process(NatDocument indoc) throws Exception {

            // TODO : why keep temp results ?
		ArrayList<NatDocument> docs = new ArrayList<>();
		docs.add(indoc);
		
		preProcess(indoc);
	
		int steps = 1;
		int totalSteps = getChain().size()+1;

		
		NatDocument resu = null;
		try {
			for (NatFilter filter : getChain()){
				getGestionnaireErreur().afficheMessage(MessageKind.STEP,getStepString(steps,totalSteps) , LogLevel.NORMAL);
				NatDocument in = docs.get(docs.size()-1);
				NatDocument out = filter.run(in);
				docs.add(out);
				steps+=1;
			}
			getGestionnaireErreur().afficheMessage(MessageKind.STEP,getStepString(steps,totalSteps) , LogLevel.NORMAL);
			resu = docs.get(docs.size()-1);
		} catch (Exception e){
			throw new NatFilterException(NatFilterErrorCode.FILTER_CHAIN_RUNTIME_ERROR, getPrettyName(),e);
		}

		return resu;	
	}

	@Override
	protected String getPrettyName() {

		StringBuilder sb = new StringBuilder();
		sb.append("FILTER CHAIN ");
		sb.append("(");			
		for (NatFilter f : getChain()){
			if (f != null){
				sb.append(f.getPrettyName());
			} else {
				sb.append("???");
			}
			sb.append(", ");
		}
		sb.append(")");
		return sb.toString();
	}
	
	public NatFilter[] getFilters() {
		return chain.toArray(new NatFilter[]{});
	}

}
