/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.core.filter.convertisseur;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import org.natbraille.core.NatStaticResolver;
import org.natbraille.core.document.NatDocument;
import org.natbraille.core.filter.NatFilter;
import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.core.gestionnaires.GestionnaireErreur;
import org.natbraille.core.gestionnaires.LogLevel;
import org.natbraille.core.gestionnaires.MessageKind;

/**
 * litterary text to natbraille internal format
 * @author vivien
 *
 */
public class ConvertisseurTexte extends NatFilter {

	@Override
	protected String getPrettyName() {
		return "TEXTE vers FORMAT INTERNE";
	}

	public ConvertisseurTexte(NatFilterOptions options, GestionnaireErreur gestionnaireErreur) {
		super(options, gestionnaireErreur);
	}

	int nbCars;
	int nbMots;
	int nbPhrases;

	/** caractère délimiteur de mots */
	protected char espace = ' ';

	// "%", "‰", ponctuation? pourquoi???
	// FEINTE: on remplace ... par … dans ligne lit
	/** Tableau des ponctuations possibles en fin de mot */
	protected String[] ponctuationFin = { "”", "’", ",", ".", ":", ";", "!", "?", "»", "…", ")", "]", "}", "\"", "*" };
	/** TAbleau des ponctuations possibles en début de mot */
	protected String[] ponctuationDebut = { "¡", "¿", "«", "“", "‘", "(", "[", "{", "\"", "*" };

	@Override
	public NatDocument process(NatDocument indoc) throws Exception {

		nbCars = 0;
		nbMots = 0;
		nbPhrases = 0;

		String sourceEncoding;

		InputStream is = null;

		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		//try {
			sourceEncoding = indoc.getOrGuessCharset().name();
			
			is = indoc.getInputstream();
			BufferedReader raf = new BufferedReader(new InputStreamReader(is, sourceEncoding));
			BufferedWriter fcible = new BufferedWriter(new OutputStreamWriter(baos, "UTF8"));

			// on met les entêtes au fichier xml
			fcible.write("<?xml version=\"1.1\" encoding=\"UTF-8\"?>");

			String doctype = " doc:doc "
			// + NatEntityResolver.WINDOB_DTD_PUBLIC_ID
					+ "SYSTEM \"" + NatStaticResolver.WINDOB_DTD_SYSTEM_ID + "\"";
			// fcible.write("\n<!DOCTYPE doc:doc SYSTEM \"" + DTD + "\">");
			fcible.write("\n<!DOCTYPE " + doctype + ">");
			fcible.write("\n<doc:doc xmlns:doc=\"espaceDoc\">");

			String ligne;

			int phraseId = 1;
			ligne = raf.readLine();
			while (ligne != null) {
				nbPhrases++;
				fcible.write("\n\t<phrase fragmentId=\""+phraseId+"\">\n");
				ligneLit(ligne, fcible);
				fcible.write("\n\t</phrase>\n");
				ligne = raf.readLine();
				phraseId++;
			}
			raf.close();
			fcible.write("\n</doc:doc>");
			fcible.close();
			gestionnaireErreur.afficheMessage(MessageKind.INFO, 
					"Le document contient " + nbPhrases + " paragraphes, " + nbMots + " mots et " + nbCars + " caractères.",
					LogLevel.VERBOSE);

			NatDocument outDoc = new NatDocument();
			outDoc.setByteBuffer(baos.toByteArray());
			outDoc.forceCharset(Charset.forName("UTF-8"));
			return outDoc;

		//} catch (Exception e) {
		//	e.printStackTrace();
		//}
		//return null;
	}

	/**
	 * Convertit une ligne littéraire au format interne
	 * 
	 * @param ligne
	 *            la ligne à convertir
	 * @param gest
	 *            une instance de {@link GestionnaireErreur}
	 * @param fcible
	 *            le BufferedWriter utilisé pour {@link Convertisseur#cible}
	 * @throws IOException 
	 */
	protected void ligneLit(String ligne, BufferedWriter fcible) throws IOException {
		int i = 0;
		int j = 0;
		String[] mots = null;
		// feinte pour pas s'emmm... avec les points de suspensions et les tab:
		ligne = ligne.replace("...", "…");

		ligne = ligne.replace("\t", "" + espace);
		ligne = ligne.replace("\n", "");
		ligne = ligne.replace("’", "'");
		ligne = ligne.replace("\u00A0", "" + espace); // espace insécable
		if (ligne != null && ligne.length() > 0) {
			mots = ligne.split("" + espace);
			if (mots.length == 0) // si il n'y a qu'un seul mot dans la ligne
			{
				mots = new String[1];
				mots[0] = ligne;
			}
			// System.err.println("ligne:" + ligne + " mot0:" + mots[0]);
		}
		if ((mots != null) && !(mots.length == 1 && mots[0] == "" + espace))// changer
																			// avec
																			// taille
																			// split:fait
		{
			//try {
				fcible.write("\n\t\t<lit>");
				nbMots = nbMots + mots.length;
				while (i < mots.length) {
					j = 0;
					// boolean trouve=false;
					boolean suivant = false;
					if (mots[i].equals("-")) {
						fcible.write("\n\t\t\t<ponctuation>-</ponctuation>");
						suivant = true;
						i++;
					} else {
						// int debutMot = 0;
						while (j < ponctuationDebut.length) {
							if (mots[i].startsWith(ponctuationDebut[j]) || mots[i] == ponctuationDebut[j]) { // mots[i].length()-1
								fcible.write("\n\t\t\t<ponctuation>" + mots[i].charAt(0) + "</ponctuation>");
								if (mots[i].length() > 1) {
									mots[i] = mots[i].substring(1, mots[i].length());
									j = 0;
								} else {
									// c'est fini, on passe au mot suivant
									suivant = true;
								}
								nbCars = nbCars + 1;
							}
							j++;
						}
						j = 0;
						// trouve = false;
						if (!suivant) {
							ArrayList<String> ponctfin = new ArrayList<String>();
							// on extrait les ponctuations de fin
							while (j < ponctuationFin.length) {
								if (mots[i].endsWith(ponctuationFin[j]) || mots[i] == ponctuationFin[j]) {
									ponctfin.add("\n\t\t\t<ponctuation>" + mots[i].charAt(mots[i].length() - 1) + "</ponctuation>");
									mots[i] = mots[i].substring(0, mots[i].length() - 1);
									j = 0;
								} else {
									j++;
								}

							}
							fcible.write("\n\t\t\t<mot>" + mots[i].replace("&", "&amp;").replace("<", "&lt;") + "</mot>");// \t\t\t<espace></espace>");
							int nbPonct = ponctfin.size();
							nbCars = nbCars + mots[i].length() + nbPonct;
							// on écrit les ponctuations si il y en a
							for (j = nbPonct - 1; j >= 0; j--) {
								fcible.write(ponctfin.get(j));
							}
						}
						i++;
					}
				}
				i = 0;
				fcible.write("\n\t\t</lit>\n");
			//} catch (java.io.IOException e) {
				//e.printStackTrace();
		//	}
		}
	}

}
