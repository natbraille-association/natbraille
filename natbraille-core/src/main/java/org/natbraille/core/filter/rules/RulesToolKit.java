/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.core.filter.rules;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.natbraille.core.NatStaticResolver;
import org.natbraille.core.filter.NatFilterException;
import org.natbraille.core.NatDynamicResolver;
import org.natbraille.core.filter.options.NatFilterOption;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Cette classe gère la création et l'exploitation des fichiers de règles en xml
 *
 * @author bruno
 */
public class RulesToolKit {
    /**
     * Constante identifiant une règle de type signe
     */
    public static final String SIGNE = "signe";
    /**
     * Constante identifiant une règle de type symbole
     */
    public static final String SYMBOLE = "symbole";
    /**
     * Constante identifiant une règle de type générique qui s'applique toujours
     */
    public static final String ALL = "all";
    /**
     * Constante identifiant une règle de type général qui s'applique dans le
     * cas général
     */
    public static final String GENERAL = "general";
    /**
     * Constante représentant l'id minimum à partir duquel la règle s'applique
     * en 2ème passe
     */
    public static final int ID_MIN_DELAYED = 500;

    /**
     * @param uri
     * @param f
     * @return une ArrayList de Regle
     * @throws URISyntaxException
     * @throws NatFilterException
     * @see outils.regles.Regle
     */

    /**
     * get rules contained in the reference braille rulefile dict
     * (url found in option NatFilterOption.XSL_G2_DICT)
     *
     * @param ndr
     * @return an array of Regles
     * @throws URISyntaxException
     * @throws NatFilterException
     */
    public static ArrayList<Regle> getRules(NatDynamicResolver ndr) throws URISyntaxException, NatFilterException {
        //nat://system/xsl/dicts/fr-g2.xml
        URI uri = new URI(ndr.getOptions().getValue(NatFilterOption.XSL_G2_DICT));
        return getRules(ndr, uri);
//)new URI(NatStaticResolver.CONTRACTION_DICO_RULES_G2_DEFAULT_SYSTEM_ID));
    }

    /**
     * get rules contained in the passed braille rulefile dict
     *
     * @param ndr
     * @param uriBrailleRules xml rule file (utf-8)
     * @return
     */
    public static ArrayList<Regle> getRules(NatDynamicResolver ndr, URI uriBrailleRules) {

        //uriBrailleRules =
        ArrayList<Regle> retour = new ArrayList<Regle>();
        // lecture du document contenant les règles
        try {
            // Parsage du document source
            DocumentBuilderFactory fabrique = DocumentBuilderFactory.newInstance();
            DocumentBuilder constructeur = fabrique.newDocumentBuilder();
            InputSource is = ndr.resolveEntity(null, uriBrailleRules.toString());
            constructeur.setEntityResolver(ndr);
            Document doc = constructeur.parse(is);

            /*
             * les règles
             */

            // locutions
            NodeList regles = doc.getElementsByTagName("locution");
            for (int i = 0; i < regles.getLength(); i++) {
                boolean actif = true;
                boolean peda = false;
                NodeList enfants = regles.item(i).getChildNodes();
                Node nActif = regles.item(i).getAttributes().getNamedItem("actif");
                Node nPeda = regles.item(i).getAttributes().getNamedItem("peda");
                if (nActif != null) {
                    String pl = nActif.getTextContent();
                    if (!Boolean.parseBoolean(pl)) {
                        actif = false;
                    }
                }
                if (nPeda != null) {
                    String pl = nPeda.getTextContent();
                    if (Boolean.parseBoolean(pl)) {
                        peda = true;
                    }
                }
                String noir = "";
                String braille = "";
                for (int j = 0; j < enfants.getLength(); j++) {
                    Node n = enfants.item(j);
                    if (n.getNodeName().equals("noir")) {
                        noir = n.getTextContent();
                    } else if (n.getNodeName().equals("braille")) {
                        braille = n.getTextContent();
                    }
                }
                retour.add(new RegleLocution(noir, braille, actif, peda));
            }
            // signes
            regles = doc.getElementsByTagName("signe");
            for (int i = 0; i < regles.getLength(); i++) {
                boolean pluriel = false;
                boolean actif = true;
                Node nActif = regles.item(i).getAttributes().getNamedItem("actif");
                if (nActif != null) {
                    String pl = nActif.getTextContent();
                    if (!Boolean.parseBoolean(pl)) {
                        actif = false;
                    }
                }
                NodeList enfants = regles.item(i).getChildNodes();
                Node nPluriel = regles.item(i).getAttributes().getNamedItem("pluriel");
                Node nPeda = regles.item(i).getAttributes().getNamedItem("peda");
                String noir = "";
                String braille = "";
                boolean pedago = false;

                if (nPluriel != null) {
                    String pl = nPluriel.getTextContent();
                    if (Boolean.parseBoolean(pl)) {
                        pluriel = true;
                    }
                }
                if (nPeda != null) {
                    String pl = nPeda.getTextContent();
                    if (Boolean.parseBoolean(pl)) {
                        pedago = true;
                    }
                }

                for (int j = 0; j < enfants.getLength(); j++) {
                    Node n = enfants.item(j);
                    if (n.getNodeName().equals("noir")) {
                        noir = n.getTextContent();
                    } else if (n.getNodeName().equals("braille")) {
                        braille = n.getTextContent();
                    }
                }
                retour.add(new RegleSigne(noir, braille, pluriel, actif, pedago));
            }

            // symboles
            regles = doc.getElementsByTagName("symbole");
            for (int i = 0; i < regles.getLength(); i++) {
                boolean actif = true;
                Node nActif = regles.item(i).getAttributes().getNamedItem("actif");
                if (nActif != null) {
                    String pl = nActif.getTextContent();
                    if (!Boolean.parseBoolean(pl)) {
                        actif = false;
                    }
                }
                NodeList enfants = regles.item(i).getChildNodes();
                Node attrInv = regles.item(i).getAttributes().getNamedItem("invariant");
                Node attrCmp = regles.item(i).getAttributes().getNamedItem("composable");
                Node attrPed = regles.item(i).getAttributes().getNamedItem("peda");
                String noir = "";
                String braille = "";
                boolean invariant = false;
                boolean composable = true;
                boolean pedago = false;

                for (int j = 0; j < enfants.getLength(); j++) {
                    Node n = enfants.item(j);
                    if (n.getNodeName().equals("noir")) {
                        noir = n.getTextContent();
                    } else if (n.getNodeName().equals("braille")) {
                        braille = n.getTextContent();
                    }
                }

                if (attrInv != null) {
                    String pl = attrInv.getTextContent();
                    if (Boolean.parseBoolean(pl)) {
                        invariant = true;
                    }
                }
                if (attrCmp != null) {
                    String cp = attrCmp.getTextContent();

                    if (!Boolean.parseBoolean(cp)) {
                        composable = false;
                    }
                }
                if (attrPed != null) {
                    String cp = attrPed.getTextContent();

                    if (!Boolean.parseBoolean(cp)) {
                        pedago = false;
                    }
                }

                retour.add(new RegleSymbole(noir, braille, invariant, composable, actif, pedago));
            }

            // regles sur ensembles
            regles = doc.getElementsByTagName("rule");
            for (int i = 0; i < regles.getLength(); i++) {
                boolean actif = true;
                Node nActif = regles.item(i).getAttributes().getNamedItem("actif");
                if (nActif != null) {
                    String pl = nActif.getTextContent();
                    if (!Boolean.parseBoolean(pl)) {
                        actif = false;
                    }
                }
                NodeList enfants = regles.item(i).getChildNodes();
                Node id = regles.item(i).getAttributes().getNamedItem("id");
                String in = "";
                String out = "";
                String desc = "";
                String ref = "";
                String p2 = "";
                String p2br = "";
                String inBr = "";
                String outBr = "";
                ArrayList<String> app = new ArrayList<String>();
                int ident = 0;

                if (id != null) {
                    ident = Integer.parseInt(id.getTextContent());
                }
                for (int j = 0; j < enfants.getLength(); j++) {
                    Node n = enfants.item(j);
                    if (n.getNodeName().equals("desc")) {
                        desc = n.getTextContent();
                    } else if (n.getNodeName().equals("ref")) {
                        ref = n.getTextContent();
                    } else if (n.getNodeName().equals("regIn")) {
                        in = n.getTextContent();
                    } else if (n.getNodeName().equals("regOut")) {
                        out = n.getTextContent();
                    } else if (n.getNodeName().equals("for")) {
                        app.add(n.getTextContent());
                    } else if (n.getNodeName().equals("pass2")) {
                        p2 = n.getTextContent();
                    } else if (n.getNodeName().equals("pass2Br")) {
                        p2br = n.getTextContent();
                    } else if (n.getNodeName().equals("regInBr")) {
                        inBr = n.getTextContent();
                    } else if (n.getNodeName().equals("regOutBr")) {
                        outBr = n.getTextContent();
                    }
                }
                retour.add(new RegleEnsemble(desc, ref, in, out, app, Boolean.parseBoolean(p2), Boolean.parseBoolean(p2br), ident, actif, inBr, outBr));
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException ioe) {
            // TODO Auto-generated catch block
            ioe.printStackTrace();
        } catch (SAXException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return retour;
    }

    /**
     * Ecrit le fichier xsl de règle à inclure dans la transcription
     *
     * @param regles liste des règles à écrire
     * @return une ArrayList de Regle
     * @throws NatFilterException
     * @see outils.regles.Regle
     */
    public static Document writeRules(NatDynamicResolver ndr, ArrayList<Regle> regles) throws NatFilterException {
        Document doc = null;

        // liste des règles de référence
        ArrayList<Regle> reglesRef = null;
        try {
            reglesRef = RulesToolKit.getRules(ndr);
            for (Regle r : reglesRef) {
                if (!regles.contains(r)) {
                    // System.out.println("ajout de "+r);
                    r.setActif(true);
                    regles.add(r);
                }
            }
            // liste des locutions sur 3 mots
            ArrayList<String> loc3 = new ArrayList<String>();
            // liste des locutions sur 2 mots
            ArrayList<String> loc2 = new ArrayList<String>();
            // liste des locutions sur 1 mot
            ArrayList<String> loc1 = new ArrayList<String>();
            // liste des signes
            ArrayList<String> signes = new ArrayList<String>();
            // liste des signes acceptant des dérivés
            ArrayList<String> signesDeriv = new ArrayList<String>();
            // liste des symboles
            ArrayList<String> symbs = new ArrayList<String>();
            // liste des symboles non composables
            ArrayList<String> symbsNonComp = new ArrayList<String>();

            // liste braille des locutions sur 3 mots
            ArrayList<String> loc3Br = new ArrayList<String>();
            // liste braille des locutions sur 2 mots
            ArrayList<String> loc2Br = new ArrayList<String>();
            // liste braille des locutions sur 1 mot
            ArrayList<String> loc1Br = new ArrayList<String>();
            // liste braille des signes
            ArrayList<String> signesBr = new ArrayList<String>();
            // liste braille des signes acceptant des dérivés
            ArrayList<String> signesDerivBr = new ArrayList<String>();
            // liste braille des symboles composables
            ArrayList<String> symbsBr = new ArrayList<String>();
            // liste braille des symboles non composables
            ArrayList<String> symbsNonCompBr = new ArrayList<String>();

            // liste des mots à ne pas abréger
            ArrayList<String> nAbr = new ArrayList<String>();

            // liste des règles s'appliquant sur les signes
            ArrayList<RegleEnsemble> lReglesSignes = new ArrayList<RegleEnsemble>();
            // liste des patterns pour les signes
            ArrayList<String> signesRegles = new ArrayList<String>();
            // liste des remplacements pour les signes
            ArrayList<String> signesReglesBr = new ArrayList<String>();
            // liste des patterns pour les signes braille
            ArrayList<String> signesBrRegles = new ArrayList<String>();
            // liste des remplacements pour les signes braille
            ArrayList<String> signesBrReglesBlack = new ArrayList<String>();
            /*
             * liste booléenne indiquant si la règle sur le signe s'applique
             * ArrayList<Boolean> lRSAppl = new ArrayList<Boolean>();
             */

            // liste des règles s'appliquant sur les symboles
            ArrayList<RegleEnsemble> lReglesSymbs = new ArrayList<RegleEnsemble>();
            // liste des patterns pour les symboles
            ArrayList<String> symbsRegles = new ArrayList<String>();
            // liste des remplacements pour les symboles
            ArrayList<String> symbsReglesBr = new ArrayList<String>();
            // règles de pass 2
            ArrayList<String> symbsReglesP2 = new ArrayList<String>();
            // liste des remplacements pour les symboles (pass2)
            ArrayList<String> symbsReglesP2Br = new ArrayList<String>();
            // liste des patterns pour les symboles braille
            ArrayList<String> symbsBrRegles = new ArrayList<String>();
            // liste des remplacements pour les symboles braille
            ArrayList<String> symbsBrReglesBlack = new ArrayList<String>();

            // liste des règles générales
            ArrayList<RegleEnsemble> lReglesGeneral = new ArrayList<RegleEnsemble>();
            // liste des patterns pour le cas général
            ArrayList<String> cgenRegles = new ArrayList<String>();
            // liste des remplacements pour le cas général
            ArrayList<String> cgenReglesBr = new ArrayList<String>();
            // liste des patterns pour les symboles braille
            ArrayList<String> cgenBrRegles = new ArrayList<String>();
            // liste des remplacements pour les symboles braille
            ArrayList<String> cgenBrReglesBlack = new ArrayList<String>();
            // liste des patterns pour les symboles braille
            ArrayList<String> cgenBrPass2Regles = new ArrayList<String>();
            // liste des remplacements pour les symboles braille
            ArrayList<String> cgenBrPass2ReglesBlack = new ArrayList<String>();

            // liste des règles générales
            ArrayList<RegleEnsemble> lReglesFinGeneral = new ArrayList<RegleEnsemble>();
            // liste des patterns pour le cas général
            ArrayList<String> cgenFinRegles = new ArrayList<String>();
            // liste des remplacements pour le cas général
            ArrayList<String> cgenFinReglesBr = new ArrayList<String>();

            // liste des règles générales en pass2
            ArrayList<RegleEnsemble> lReglesPass2 = new ArrayList<RegleEnsemble>();
            // liste des patterns pour le cas général pass2
            ArrayList<String> cgenPass2Regles = new ArrayList<String>();
            // liste des remplacements pour le cas général pass2
            ArrayList<String> cgenPass2ReglesBr = new ArrayList<String>();

            // liste des symboles/locutions/signes contenant un tiret
            ArrayList<String> lMotTiret = new ArrayList<String>();
            ArrayList<RegleMot> lMotTiretDef = new ArrayList<RegleMot>();

            // liste des règles génériques
            ArrayList<RegleEnsemble> lReglesAll = new ArrayList<RegleEnsemble>();
            ArrayList<String> all = new ArrayList<String>();// nom des variables
            // à créer
            ArrayList<Boolean> all_status = new ArrayList<Boolean>();// nom des
            // variables
            // à
            // créer

            // Parsage du document source
            DocumentBuilderFactory fabrique = DocumentBuilderFactory.newInstance();
            DocumentBuilder constructeur;
            constructeur = fabrique.newDocumentBuilder();
            doc = constructeur.newDocument();
            doc.setXmlVersion("1.0");

            Element racine = createXSL(doc);
            // liste des symboles
            ArrayList<RegleSymbole> listeSymboles = new ArrayList<RegleSymbole>();
            for (Regle r : regles) {
                if (r instanceof RegleMot n) {
                    // pour les symboles ayant une apostrophe
                    // non (si la règle est pédagogique et qu'elle n'est pas
                    // active)
                    if (!(!n.isActif() && n.isPeda())) {
                        if (!n.isActif()) {
                            String noir = n.getNoir();
                            nAbr.add(noir);
                        }
                        if (n.getNoir().contains("-")) {
                            lMotTiretDef.add(n);
                        }
                        if (r instanceof RegleLocution rl) {
                            String noir = rl.getNoir();
                            String braille = rl.getBraille();
                            switch (noir.split(" ").length) {
                                case 1:
                                    loc1.add(noir);
                                    loc1Br.add(braille);
                                    break;
                                case 2:
                                    loc2.add(noir);
                                    loc2Br.add(braille);
                                    break;
                                case 3:
                                    loc3.add(noir);
                                    loc3Br.add(braille);
                                    break;
                            }
                        } else if (r instanceof RegleSigne) {
                            RegleSigne rs = (RegleSigne) r;
                            String noir = rs.getNoir();
                            String braille = rs.getBraille();
                            if (rs.isInvariant()) {
                                signes.add(noir);
                                signesBr.add(braille);
                            } else {
                                signesDeriv.add(noir);
                                signesDerivBr.add(braille);
                            }
                        } else if (r instanceof RegleSymbole) {
                            RegleSymbole rs = (RegleSymbole) r;
                            String noir = rs.getNoir();
                            String braille = rs.getBraille();
                            if (rs.isInvariant()) {
                                loc1.add(noir);
                                loc1Br.add(braille);
                            } else {
                                listeSymboles.add(rs);
                            }
                        }
                    }
                    /*
                     * else { System.out.println("On ignore "+ r.toString()); }
                     */
                } else if (r instanceof RegleEnsemble re) {
                    String in = re.getRegIn();
                    String out = re.getRegOut();
                    String inBr = re.getRegInBr();
                    String outBr = re.getRegOutBr();
                    if (re.isFor(RulesToolKit.ALL)) {
                        lReglesAll.add(re);
                        all.add(in);
                        all_status.add(re.isActif());
                    } else if (r.isActif()) {
                        if (re.isFor(RulesToolKit.SIGNE)) {
                            lReglesSignes.add(re);
                            signesRegles.add(in);
                            signesReglesBr.add(out);
                            signesBrRegles.add(inBr);
                            signesBrReglesBlack.add(outBr);
                        }
                        if (re.isFor(RulesToolKit.SYMBOLE)) {
                            lReglesSymbs.add(re);
                            if (!(inBr.isEmpty() || outBr.isEmpty())) {
                                symbsBrRegles.add(inBr);
                                symbsBrReglesBlack.add(outBr);
                            }
                            if (re.isPass2()) {
                                symbsReglesP2.add(in);
                                symbsReglesP2Br.add(out);
                            } else {
                                symbsRegles.add(in);
                                symbsReglesBr.add(out);
                            }
                        } else if (re.isFor(RulesToolKit.GENERAL)) {
                            int id = re.getId();
                            if (!(inBr.isEmpty() || outBr.isEmpty())) {
                                if (re.isPass2Br()) {
                                    cgenBrPass2Regles.add(inBr);
                                    cgenBrPass2ReglesBlack.add(outBr);
                                } else {
                                    cgenBrRegles.add(inBr);
                                    cgenBrReglesBlack.add(outBr);
                                }
                            }
                            if (id >= ID_MIN_DELAYED)// la règle est à appliquer
                            // en deuxième passe
                            {
                                lReglesPass2.add(re);
                                cgenPass2Regles.add(in);
                                cgenPass2ReglesBr.add(out);
                            } else if (in.endsWith("$"))// c'est une finale
                            {
                                lReglesFinGeneral.add(re);
                                cgenFinRegles.add(in);
                                cgenFinReglesBr.add(out);
                            } else {
                                lReglesGeneral.add(re);
                                cgenRegles.add(in);
                                cgenReglesBr.add(out);
                            }
                        }
                    }
                    /*
                     * else { System.out.println("Règle " + re
                     * +" non prise en compte pour l'instant"); }
                     */
                } else {
                    System.out.println("Regle non prise en compte pour l'instant");
                }
            }

            // tri des listes de règles sur ensemble
            Collections.sort(lReglesSignes);
            Collections.sort(listeSymboles);
            Collections.sort(lMotTiretDef);

            // listes des mots exceptions contenant un tiret; remplacement des
            // 'espace (c'est-à-dire)
            for (RegleMot r : lMotTiretDef) {
                lMotTiret.add(r.getNoir().replaceAll("'' ", "''"));
            }

            for (RegleSymbole r : listeSymboles) {
                if (r.isComposable()) {
                    symbs.add(r.getNoir());
                    symbsBr.add(r.getBraille());
                } else {
                    symbsNonComp.add(r.getNoir());
                    symbsNonCompBr.add(r.getBraille());
                }
            }

            // création des listes
            racine.appendChild(doc.createComment("Liste des locutions sur 3 mots"));
            racine.appendChild(createListe("loc3", loc3, doc));
            racine.appendChild(doc.createComment("Liste des transcriptions des locutions sur 3 mots"));
            racine.appendChild(createListe("loc3Br", loc3Br, doc));
            racine.appendChild(doc.createComment("Liste des locutions sur 2 mots"));
            racine.appendChild(createListe("loc2", loc2, doc));
            racine.appendChild(doc.createComment("Liste des transcriptions locutions sur 2 mots"));
            racine.appendChild(createListe("loc2Br", loc2Br, doc));
            racine.appendChild(doc.createComment("Liste des locutions sur 1 mot"));
            racine.appendChild(createListe("loc1", loc1, doc));
            racine.appendChild(doc.createComment("Liste des transcriptions des locutions sur 1 mot"));
            racine.appendChild(createListe("loc1Br", loc1Br, doc));
            // signes
            racine.appendChild(doc.createComment("Liste des signes invariants"));
            racine.appendChild(createListe("signesInv", signes, doc));
            racine.appendChild(doc.createComment("Liste des transcriptions des signes invariants"));
            racine.appendChild(createListe("signesInvBr", signesBr, doc));
            racine.appendChild(doc.createComment("Liste des signes dérivables"));
            racine.appendChild(createListe("signesDeriv", signesDeriv, doc));
            racine.appendChild(doc.createComment("Liste des transcriptions des signes dérivables"));
            racine.appendChild(createListe("signesDerivBr", signesDerivBr, doc));
            racine.appendChild(doc.createComment("Liste des signes"));
            racine.appendChild(getParam("signes", "($signesDeriv,$signesInv)", doc));
            racine.appendChild(doc.createComment("Liste des transcriptions des signes"));
            racine.appendChild(getParam("signesBr", "($signesDerivBr,$signesInvBr)", doc));
            racine.appendChild(doc.createComment("Liste des patterns possibles pour les signes"));
            racine.appendChild(createListe("signesRules", signesRegles, doc));
            racine.appendChild(doc.createComment("Liste des patterns de remplacement pour les signes"));
            racine.appendChild(createListe("signesRulesBr", signesReglesBr, doc));
            racine.appendChild(doc.createComment("Liste des patterns possibles pour les signes brailles"));
            racine.appendChild(createListe("signesBrRules", signesBrRegles, doc));
            racine.appendChild(doc.createComment("Liste des patterns de remplacement pour les signes braille"));
            racine.appendChild(createListe("signesBrRulesBlack", signesBrReglesBlack, doc));

            racine.appendChild(doc.createComment("Liste des symboles fondamentaux composables"));
            racine.appendChild(createListe("symbs", symbs, doc));
            racine.appendChild(doc.createComment("Liste des transcriptions des symboles fondamentaux composables"));
            racine.appendChild(createListe("symbsBr", symbsBr, doc));
            racine.appendChild(doc.createComment("Liste des symboles fondamentaux non composables"));
            racine.appendChild(createListe("symbsNC", symbsNonComp, doc));
            racine.appendChild(doc.createComment("Liste des transcriptions des symboles fondamentaux non composables"));
            racine.appendChild(createListe("symbsNCBr", symbsNonCompBr, doc));
            racine.appendChild(doc.createComment("Liste des patterns possibles pour les symboles"));
            racine.appendChild(createListe("symbsRules", symbsRegles, doc));
            racine.appendChild(doc.createComment("Liste des patterns de remplacement pour les symboles"));
            racine.appendChild(createListe("symbsRulesBr", symbsReglesBr, doc));
            racine.appendChild(doc.createComment("Liste des patterns possibles pour les symboles, pass2"));
            racine.appendChild(createListe("symbsRulesP2", symbsReglesP2, doc));
            racine.appendChild(doc.createComment("Liste des patterns de remplacement pour les symboles, pass2"));
            racine.appendChild(createListe("symbsRulesP2Br", symbsReglesP2Br, doc));
            racine.appendChild(doc.createComment("Liste des patterns possibles pour les symboles brailles"));
            racine.appendChild(createListe("symbsBrRules", symbsBrRegles, doc));
            racine.appendChild(doc.createComment("Liste des patterns de remplacement pour les symboles braille"));
            racine.appendChild(createListe("symbsBrRulesBlack", symbsBrReglesBlack, doc));

            racine.appendChild(doc.createComment("Liste des patterns possibles pour le cas général"));
            racine.appendChild(createListe("cgenRules", cgenRegles, doc));
            racine.appendChild(doc.createComment("Liste des patterns de remplacement pour le cas général"));
            racine.appendChild(createListe("cgenRulesBr", cgenReglesBr, doc));
            racine.appendChild(doc.createComment("Liste des patterns possibles pour le das général, détranscription"));
            racine.appendChild(createListe("cgenBrRules", cgenBrRegles, doc));
            racine.appendChild(doc.createComment("Liste des patterns de remplacement pour le cas général, détranscription"));
            racine.appendChild(createListe("cgenBrRulesBlack", cgenBrReglesBlack, doc));
            racine.appendChild(doc.createComment("Liste des finales possibles pour le cas général"));
            racine.appendChild(createListe("cgenFinRules", cgenFinRegles, doc));
            racine.appendChild(doc.createComment("Liste des patterns de remplacement pour le cas général"));
            racine.appendChild(createListe("cgenFinRulesBr", cgenFinReglesBr, doc));
            racine.appendChild(doc.createComment("Liste des règles de pass2 pour le cas général"));
            racine.appendChild(createListe("cgenPass2Rules", cgenPass2Regles, doc));
            racine.appendChild(doc.createComment("Liste des pattern de remplacement pour le cas général pass2"));
            racine.appendChild(createListe("cgenPass2RulesBr", cgenPass2ReglesBr, doc));
            racine.appendChild(doc.createComment("Liste des patterns possibles pour le das général, pass 2, détranscription"));
            racine.appendChild(createListe("cgenBrPass2Rules", cgenBrPass2Regles, doc));
            racine.appendChild(doc.createComment("Liste des patterns de remplacement pour le cas général, pass2, détranscription"));
            racine.appendChild(createListe("cgenBrPass2RulesBlack", cgenBrPass2ReglesBlack, doc));

            racine.appendChild(doc.createComment("Liste des mots exceptions contenant un tiret"));
            racine.appendChild(createListe("motsTiret", lMotTiret, doc));

            racine.appendChild(doc.createComment("Liste des mots à ne pas abréger"));
            racine.appendChild(createListe("nAbr", nAbr, doc));

            racine.appendChild(doc.createComment("Liste des variables booléennes"));
            for (int i = 0; i < all.size(); i++) {
                Element e = doc.createElement("xsl:param");
                e.setAttribute("name", all.get(i));
                e.setAttribute("as", "xs:boolean");
                e.setAttribute("select", all_status.get(i) + "()");
                racine.appendChild(e);
            }

            // Création du transformeur
            // TransformerFactory transformFactory =
            // TransformerFactory.newInstance();
            // lire le style
            // Transformer transform = transformFactory.newTransformer();

            // configuration de la transformation
            // DOMSource in = new DOMSource(doc);
            // StreamResult out;
            // System.out.println(new File
            // (ConfigNat.getUserTempFolder()+"fr-g2-rules.xsl").toURI().toString());
            // System.out.println(new File
            // (ConfigNat.getUserTempFolder()+"fr-g2-rules.xsl").toURI().getPath());
            // System.out.println(new File
            // (ConfigNat.getUserTempFolder()+"fr-g2-rules.xsl").toURI().getRawPath());
            // out = new StreamResult(new File
            // (ConfigNat.getUserTempFolder()+"fr-g2-rules.xsl").toURI().toString());

            // transformation
            // transform.setOutputProperty("indent", "yes");
            // transform.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM,
            // NatEntityResolver.WINDOB_DTD_SYSTEM_ID);
            // transform.transform(in, out);

        } catch (ParserConfigurationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            //		System.exit(0);
        } catch (URISyntaxException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
            //		System.exit(0);
        }
        return doc;
    }

    /**
     * Sauve les règles de la liste l dans le fichier f
     *
     * @param l liste des règles
     * @param f adresse du fichier cible
     * @return false si erreur ou si tentative de modification du fichier de
     * référence
     */
    public static void saveRuleFile(List<Regle> l, String f) throws IOException {
        writeRuleFile(l, new FileOutputStream(f));
    }
    public static byte[] getRuleFileBytes(List<Regle> l) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        RulesToolKit.writeRuleFile(l, baos);
        return baos.toByteArray();
    }
    public static void writeRuleFile(List<Regle> l, OutputStream outputStream) throws IOException {
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(outputStream, StandardCharsets.UTF_8));
        bw.write("<?xml version='1.0' encoding='UTF-8'?>\n" + "<!DOCTYPE doc:doc SYSTEM \"" + NatStaticResolver.WINDOB_DTD_SYSTEM_ID + "\">\n" + "<rules>\n");
        for (Regle r : l) {
            bw.write(r.getXML());
        }
        bw.write("</rules>");
        bw.close();
    }


    /**
     * Créer un paramètre de nom <code>nom</code>, de valeur <code>valeur</code>
     * et de type xs:string*
     *
     * @param nom    nom du paramètre
     * @param valeur valeur pour le select du paramètre
     * @param doc    document xsl
     * @return un élément xsl:param
     */
    private static Element getParam(String nom, String valeur, Document doc) {
        Element retour = doc.createElement("xsl:param");
        retour.setAttribute("name", nom);
        retour.setAttribute("as", "xs:string*");
        retour.setAttribute("select", valeur);
        return retour;
    }

    /**
     * Crée un paramètre xsl à partir de <code>doc</code> de nom
     * <code>nom</code> et de type xs:string* La séquence pour la valeur du
     * paramètre est créée à partir de la liste <code>liste</code>
     *
     * @param nom   le nom du paramètre dans la feuille xsl
     * @param liste la liste de règles
     * @param doc   le document xsl
     * @return l'élément produit par <doc>
     */
    private static Element createListe(String nom, ArrayList<String> liste, Document doc) {
        Element retour = doc.createElement("xsl:param");
        retour.setAttribute("name", nom);
        retour.setAttribute("as", "xs:string*");
        if (liste.size() > 0) {
            String valeur = "(";
            for (String s : liste) {
                valeur += "'" + s + "',";
            }
            valeur = valeur.substring(0, valeur.length() - 1) + ")";// pour
            // virer la
            // dernière
            // virgule
            // et mettre
            // la
            // parenthèse
            // fermant
            // la
            // séquence
            retour.setAttribute("select", valeur);
        }
        return retour;
    }

    /**
     * Crée un fichier xsl contenant les listes de règle
     *
     * @param doc le document à partir duquel créer le fichier xsl
     * @return la racine du document
     */
    private static Element createXSL(Document doc) {
        Element racine = doc.createElement("xsl:stylesheet");
        racine.setAttribute("version", "2.0");
        racine.setAttribute("xmlns:xsl", "http://www.w3.org/1999/XSL/Transform");
        racine.setAttribute("xmlns:xs", "http://www.w3.org/2001/XMLSchema");

        racine.appendChild(doc.createComment("\nFichier généré automatiquement par NAT\nCe fichier contient les règles personnalisée pour l'abrégé\n"));
        /*
         * Element out = doc.createElement("xsl:output");
         * out.setAttribute("method","xml");
         * out.setAttribute("encoding","UTF-8");
         * out.setAttribute("indent","yes"); racine.appendChild(out);
         */
        doc.appendChild(racine);
        return racine;
    }

}
