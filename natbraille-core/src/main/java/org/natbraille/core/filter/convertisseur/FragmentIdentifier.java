package org.natbraille.core.filter.convertisseur;

import org.natbraille.core.NatDynamicResolver;
import org.natbraille.core.document.NatDocument;
import org.natbraille.core.filter.NatTransformerFilter;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * This filter adds a @fragmentId attribute to document elements that need to be identified in the following transcription steps
 */
public class FragmentIdentifier extends NatTransformerFilter {

    /*
     * list of names of elements needing a @fragmentId attribute
     */
    private static final String[] matchedElementsNames = new String[]{
            "phrase",
            "titre",
            "ol","ul","li","dl","mot", "ponctuation", "echap",
            "math", "mi", "mo"
    };

    private static String xPathLocalNameEquals(String localName) {
        return "local-name() = '" + localName + "'";
    }

    private static final String matchedElementsXPathExpression = "//*["
            + Arrays.stream(matchedElementsNames).map(FragmentIdentifier::xPathLocalNameEquals).collect(Collectors.joining(" or "))
            + "]";

    @Override
    protected String getPrettyName() {
        return "Add fragment ids";
    }

    /**
     * @param ndr
     */
    public FragmentIdentifier(NatDynamicResolver ndr) {
        super(ndr);
    }

    @Override
    protected NatDocument process(NatDocument indoc) throws Exception {

        // get DOM Document
        Document inDomDocument = indoc.getDocument(getUriResolver().getXMLReader());

        // get elements needing a @fragmentId attribute
        XPath xPath = XPathFactory.newInstance().newXPath();
        NodeList phrases = (NodeList) xPath.compile(matchedElementsXPathExpression).evaluate(inDomDocument, XPathConstants.NODESET);
        for (int i = 0; i < phrases.getLength(); i++) {
            Node node = phrases.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {

                // set @fragmentId attribute
                String fragmentId = String.valueOf(i + 1);
                ((Element) node).setAttribute("fragmentId", fragmentId);
            }
        }
        NatDocument outNatDocument = new NatDocument();
        outNatDocument.setDocument(inDomDocument);
        return outNatDocument;
    }


}
