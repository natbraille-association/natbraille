/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.core.filter;

/**
 * error codes used by {@link NatFilter}s
 * @author vivien
 *
 */
public enum NatFilterErrorCode {

	/**
	 * triggered when a filter tries to read an unset option
	 */
	FILTER_OPTION_UNSET,

	/**
	 * triggered when default options cannot be loaded. Should not happen.
	 */
	FILTER_DEFAULT_OPTIONS_UNAVAILABLE, 

	/**
	 * generic error occurring during filter initialization 
	 */
	FILTER_INITIALIZATION_ERROR,
	
	/**
	 * triggered when a generic runtime error occurs
	 */
	FILTER_RUNTIME_ERROR,

	/**
	 * triggered by generic filter chain runtime error
	 */
	FILTER_CHAIN_RUNTIME_ERROR, 
	
	
	/**
	 * triggered by generic filter chain preprocessing error
	 */
	FILTER_CHAIN_PREPROCESSING_ERROR,
	
	/**
	 * triggered when trying to load an unknown (not existing NatFilterOption) 
	 * option
	 */
	UNKNOWN_FILTER_OPTION,
	
	/**
	 * triggered by error in external libraries (jodconverter, loffice)
	 */
	NAT_FILTER_EXTERNAL_ERROR, 

	/**
	 * triggered by error in external programs (latex conversion)
	 */
	NAT_EXTERNAL_FILTER_ERROR, 

	/**
	 * Transcriptor specific
	 * the input format of the document is recognised, making it impossible to transcribex
	 */
	TRANS_CANNOT_DETERMINE_IMPORT_FILTER,
	
	/**
	 * Transcriptor specific:
	 * the transcriptor cannot be built
	 */
	TRANS_CANNOT_BUILD_TRANSCODEUR, 
	
	/**
	 * Transcriptor specific:
	 * the presentator cannot be built
	 */
	TRANS_CANNOT_BUILD_PRESENTATOR, 

	/**
	 * Transcriptor specific:
	 * the presentator braille table modifier cannot be built
	 */
	TRANS_CANNOT_BUILD_BRAILLE_TABLE_MODIFIER, 
	
	
	/**
	 * Invalid ref to a system braille table
	 */
	NO_BRAILLE_TABLE_AT_URI, 
	
	
	/**
	 * filter configurator cannot instanciate a filter
	 */
	FACTORY_CANNOT_BUILD_FILTER, 
	
	/**
	 * filter configurator cannot instanciate a filter constructor parameter
	 */
	FACTORY_CANNOT_BUILD_FILTER_PARAM, 
	
	
	/**
	 * error, warning or fatal error while sax is parsing
	 */
	FILTER_SAXPARSE, 
	
	/**
	 *  NatDynamicXsl cannot build the xsl stylesheet document
	 */
	FACTORY_CANNOT_BUILD_XSL_DOCUMENT, 
        
        
        /**
         * No openoffice translator found
         */
        NO_CONFIGURED_SOFFICE
	 
}


