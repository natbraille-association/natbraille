/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.natbraille.core.filter.options;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import org.natbraille.brailletable.system.SystemBrailleTables;


import org.natbraille.core.document.NatDocument;
import org.natbraille.core.filter.NatFilter;

/*
 used in confs.
 CONFIG_NAME=Embossage interligne double
 CONFIG_DESCRIPTION=Pour l'embossage sur une embosseuse en code U.S. (32x28), numerotation des pages 
 CONFIG_VERSION=3
 LongueurLigne=32
 NbLigne=28
 Numerotation='hs'
 NumberingStartPage=1
 MepModelignes=5
 #OUTPUT_ENCODING=UTF-8
 #TABLE_EMBOSSAGE=brailleUTF8
 TABLE_EMBOSSAGE=CodeUS

 */

/**
 * options for {@link NatDocument} processing within {@link NatFilter}
 * @author vivien
 */
public enum NatFilterOption implements Comparable<NatFilterOption> {

	/**
	 * infos 
	 * ==================================================
	 *//**
	 * infos 
	 * ==================================================
	 *//**
	 * infos 
	 * ==================================================
	 *//**
	 * infos 
	 * ==================================================
	 */
	@NatFilterOptionDetail(
			comment = "Configuration format version",
			defaultValue = "5",
			hidden = "true",
			categories = { NatFilterOptionCategory.Infos, NatFilterOptionCategory.Advanced }
			)
	CONFIG_VERSION,

	@NatFilterOptionDetail(
			comment = "Configuration name",
			defaultValue = "default name",
			hidden = "",
			categories = { NatFilterOptionCategory.Infos }
			)
	CONFIG_NAME,

	@NatFilterOptionDetail(
			comment = "Configuration description",
			defaultValue = "default description",
			hidden = "",
			categories = { NatFilterOptionCategory.Infos }
			)
	CONFIG_DESCRIPTION,

	/**
	 * Mode de transcription 
	 * ====================================================
	 */
	@NatFilterOptionDetail(
			type = NatFilterOptionType.Boolean, 
			comment = "Convert literary expressions",
			defaultValue = "true",
			hidden = "",
			categories = { NatFilterOptionCategory.Mode,NatFilterOptionCategory.ModeBase }
			)
	MODE_LIT,

	@NatFilterOptionDetail(
			type = NatFilterOptionType.Boolean, 
			comment = "Convert mathematical expressions",
			defaultValue = "true",
			hidden = "",
			categories = { NatFilterOptionCategory.Mode,NatFilterOptionCategory.ModeBase }
			)
	MODE_MATH,

	@NatFilterOptionDetail(
			type = NatFilterOptionType.Boolean, 
			comment = "Use specific trigonometry notation",
			defaultValue = "false",
			hidden = "",
			categories = { NatFilterOptionCategory.Mode,NatFilterOptionCategory.ModeBase }
			)
	MODE_MATH_SPECIFIC_NOTATION,

	@NatFilterOptionDetail(
			type = NatFilterOptionType.Boolean, 
			comment = "always put mathematical prefix",
			defaultValue = "false",
			hidden = "",
			categories = { NatFilterOptionCategory.Mode,NatFilterOptionCategory.ModeBase  }
			)
	MODE_MATH_ALWAYS_PREFIX,

	@NatFilterOptionDetail(
			type = NatFilterOptionType.Boolean, 
			comment = "Convert musical expressions (not used)",
			defaultValue = "false",
			hidden = "true",
			categories = {
					NatFilterOptionCategory.Mode, NatFilterOptionCategory.ModeBase }
			)
	MODE_MUSIC,

	@NatFilterOptionDetail(
			type = NatFilterOptionType.Boolean, 
			comment = "Convert chemical expressions",
			defaultValue = "true",
			hidden = "",
			categories = { NatFilterOptionCategory.Mode,NatFilterOptionCategory.ModeBase }
			)
	MODE_CHEMISTRY,

	@NatFilterOptionDetail(
			type = NatFilterOptionType.Boolean, 
			comment = "Process images (not used)",
			defaultValue = "false",
			hidden = "true",
			categories = { NatFilterOptionCategory.Mode,NatFilterOptionCategory.ModeBase }
			)
	MODE_IMAGES,

	@NatFilterOptionDetail(
			type = NatFilterOptionType.Boolean, 
			comment = "Reverse transcription (braille to black)",
			defaultValue = "false",
			hidden = "",
			categories = { NatFilterOptionCategory.ModeBase,NatFilterOptionCategory.ModeDetrans }
			)
	MODE_DETRANS,
	/**
	 * Mode de transcription / Integral / majuscules
	 * ===============================================
	 */

	@NatFilterOptionDetail(
			type = NatFilterOptionType.Boolean, 
			comment = "Use double prefix rule for upcase words",
			defaultValue = "true",
			hidden = "",
			categories = { NatFilterOptionCategory.Mode,NatFilterOptionCategory.ModeIntegralMaj }
			)
	MODE_LIT_FR_MAJ_DOUBLE,

	@NatFilterOptionDetail(
			type = NatFilterOptionType.Boolean, 
			comment = "Use special rule if a whole text passage is upcase",
			defaultValue = "true",
			hidden = "",
			categories = {
					NatFilterOptionCategory.Mode, NatFilterOptionCategory.ModeIntegralMaj }
			)
	MODE_LIT_FR_MAJ_PASSAGE,

	@NatFilterOptionDetail(
			type = NatFilterOptionType.Boolean, 
			comment = "Take account of mixed upcase lowcase word",
			defaultValue = "true",
			hidden = "",
			categories = { NatFilterOptionCategory.Mode,NatFilterOptionCategory.ModeIntegralMaj }
			)
	MODE_LIT_FR_MAJ_MELANGE,

	/**
	 * Mode de transcription / Integral / Mise en evidence
	 * ===================================================
	 */

	@NatFilterOptionDetail(
			type = NatFilterOptionType.Boolean, 
			comment = "Process word emphasis",
			defaultValue = "true",
			hidden = "",
			categories = { NatFilterOptionCategory.Mode,NatFilterOptionCategory.ModeIntegralEmph }
			)
	MODE_LIT_EMPH,

	@NatFilterOptionDetail(
			type = NatFilterOptionType.Boolean, 
			comment = "Process emphasis in a word",
			defaultValue = "true",
			hidden = "",
			categories = { NatFilterOptionCategory.Mode,NatFilterOptionCategory.ModeIntegralEmph }
			)
	MODE_LIT_FR_EMPH_IN_WORD,

	@NatFilterOptionDetail(
			type = NatFilterOptionType.Boolean, 
			comment = "Process emphasis in a passage",
			defaultValue = "true",
			hidden = "",
			categories = { NatFilterOptionCategory.Mode,NatFilterOptionCategory.ModeIntegralEmph }
			)
	MODE_LIT_FR_EMPH_IN_PASSAGE,

	/**
	 * Mode de transcription / Abrege 
	 * =====================================================
	 */
	@NatFilterOptionDetail(
			type = NatFilterOptionType.Boolean, 
			comment = "Convert literary expressions to grade 2 Braille",
			defaultValue = "false",
			hidden = "",
			categories = {
					NatFilterOptionCategory.Mode, NatFilterOptionCategory.ModeAbrv }
			)
	MODE_G2,

	@NatFilterOptionDetail(
			type = NatFilterOptionType.Number, 
			comment = "Use grade 2 braille for titles starting for this heading level",
			defaultValue = "3",
			hidden = "",
			possibleValues = {"1","2","3","4","5","6","7"}, 
			categories = { NatFilterOptionCategory.Mode,NatFilterOptionCategory.ModeAbrv }
			)
	MODE_G2_HEADING_LEVEL,
	
	
	
	

	@Deprecated
	@NatFilterOptionDetail(
			type = NatFilterOptionType.Boolean, 
			comment = "always true",
			defaultValue = "true",
			hidden = "true",
			categories = { NatFilterOptionCategory.Mode, NatFilterOptionCategory.ModeAbrv }
			)
	IvbMajSeule,

	@NatFilterOptionDetail(
			comment = "List,of,words that should be kept in G1 Braille",
			defaultValue = "",
			hidden = "",
			categories = { NatFilterOptionCategory.Mode, NatFilterOptionCategory.ModeAbrv }
			)
	MODE_G2_G1_WORDS,

	@NatFilterOptionDetail(
			comment = "List,of,ambiguous,words that should be kept in G1 Braille",
			defaultValue = "",
			hidden = "",
			categories = { NatFilterOptionCategory.Mode, NatFilterOptionCategory.ModeAbrv }
			)
	MODE_G2_G1_AMB_WORDS,

	@NatFilterOptionDetail(
			comment = "List,of,words that require an \"ivb\"",
			defaultValue = "",
			hidden = "",
			categories = { NatFilterOptionCategory.Mode, NatFilterOptionCategory.ModeAbrv }
			)
	MODE_G2_IVB_WORDS,

	@NatFilterOptionDetail(
			comment = "List,of,ambiguous,words that that require an \"ivb\"",
			defaultValue = "",
			hidden = "",
			categories = { NatFilterOptionCategory.Mode, NatFilterOptionCategory.ModeAbrv }
			)
	MODE_G2_IVB_AMB_WORDS,

	/**
	 * Mode de transcription / Tableaux
	 * ===================================================
	 */

	@NatFilterOptionDetail(
			type = NatFilterOptionType.Number, 
			comment = "Minimum table cell number for 2D (non linear) table",
			defaultValue = "4",
			hidden = "",
			categories = {
					NatFilterOptionCategory.Mode, NatFilterOptionCategory.ModeTable }
			)
	MODE_ARRAY_2D_MIN_CELL_NUM,

	@NatFilterOptionDetail(
			type = NatFilterOptionType.Boolean, 
			comment = "Always linearize tables",
			defaultValue = "false",
			hidden = "",
			categories = { NatFilterOptionCategory.Mode,NatFilterOptionCategory.ModeTable }
			)
	MODE_ARRAY_LINEARIZE_ALWAYS,

	/**
	 * Mode de transcription / Detranscription
	 * ===================================================
	 */

	@NatFilterOptionDetail(
			type = NatFilterOptionType.Boolean, 
			comment = "Preserve hyphenation found in original braille document",
			defaultValue = "false",
			hidden = "",
			categories = {
					NatFilterOptionCategory.Mode, NatFilterOptionCategory.ModeDetrans }
			)
	MODE_DETRANS_KEEP_HYPHEN,

	@NatFilterOptionDetail(
			type = NatFilterOptionType.Boolean,
			comment = "keep track of position",
			defaultValue = "true",
			hidden = "false",
			categories = { NatFilterOptionCategory.ModeDetrans }
	)
	MODE_DETRANS_KEEP_SOURCE_OFFSETS,

	@NatFilterOptionDetail(
			type = NatFilterOptionType.Boolean,
			comment = "set a semantics/annotation mathml element with braille source",
			defaultValue = "true",
			hidden = "false",
			categories = { NatFilterOptionCategory.Format }
	)
	MODE_DETRANS_MATH_USE_BRAILLE_SEMANTICS_ANNOTATION,

	/**
	 * Mise en page / activer la mise en page
	 * ===================================================
	 */

	@NatFilterOptionDetail(
			type = NatFilterOptionType.Boolean, 
			comment = "Do final layout",
			defaultValue = "true",
			hidden = "",
			categories = { NatFilterOptionCategory.Layout,
					NatFilterOptionCategory.LayoutBase }
			)
	LAYOUT,

	@NatFilterOptionDetail(
			type = NatFilterOptionType.Boolean, 
			comment = "Activate of literary Braille line breaking",
			defaultValue = "true",
			hidden = "",
			categories = { NatFilterOptionCategory.Layout,
					NatFilterOptionCategory.LayoutBase }
			)
	LAYOUT_LIT_HYPHENATION,

	@NatFilterOptionDetail(
			type = NatFilterOptionType.Boolean, 
			comment = "Activate quirky line breaking",
			defaultValue = "false",
			hidden = "",
			categories = { NatFilterOptionCategory.Layout,
					NatFilterOptionCategory.LayoutBase }
			)
	LAYOUT_DIRTY_HYPHENATION,

	/**
	 * Mise en page / propriété de la page - numerotation
	 * ===================================================
	 */
        /* First letter : 
         * b means bottom of the page ; 
         * h means top of the page. 
         * Second  letter : 
         * s means the page number has its own line ; 
         * b means the line number is integrated with the text
        */
	@NatFilterOptionDetail(
			comment = "Page numbering position (one of 'bs' 'hs' 'bb' 'hb' 'nn') ",
			defaultValue = "'bs'",
			hidden = "",
			possibleValues = { "'bs'","'hs'","'bb'","'hb'",	"'nn'" }, 
			categories = { NatFilterOptionCategory.Layout,NatFilterOptionCategory.LayoutNumbering }
        )
	LAYOUT_NUMBERING_MODE,

	@NatFilterOptionDetail(
			type = NatFilterOptionType.Number, 
			comment = "First numbered page",
			defaultValue = "2",
			hidden = "",
			categories = { NatFilterOptionCategory.Layout,
					NatFilterOptionCategory.LayoutNumbering }
			)
	LAYOUT_NUMBERING_START_PAGE,

	/**
	 * Mise en page / propriété de la page - lignes vides
	 * ===================================================
	 */
	@NatFilterOptionDetail(
			type = NatFilterOptionType.String, 
			comment = "Empty line mode. \"0\" means \"as in source\" ; \"1\" means "
					+ " \"customized\" (see mepminligneN) ; \"2\" means \"no empty lines\" ; \"3\" means, " + " as in French braille norm, 'aéré' ; \"4\" means, as in French braille norm, "
					+ " 'compact' ; \"5\" means double line spacing ",
					defaultValue = "0",
					hidden = "",
					categories = { NatFilterOptionCategory.Layout, NatFilterOptionCategory.LayoutEmptyLines }, 
					possibleValues = {	"0", "1", "2", "3", "4", "5" }
			)
	LAYOUT_PAGE_EMPTY_LINES_MODE,

	@NatFilterOptionDetail(
			type = NatFilterOptionType.Number, 
			comment = "Number of consecutive empty lines in input outputing one empty braille line",
			defaultValue = "2",
			hidden = "",
			categories = {
					NatFilterOptionCategory.Layout, NatFilterOptionCategory.LayoutEmptyLines }
			)
	LAYOUT_PAGE_EMPTY_LINES_CUSTOM_1,

	@NatFilterOptionDetail(
			type = NatFilterOptionType.Number, 
			comment = "number of consecutive empty lines in input outputing two empty braille lines",
			defaultValue = "3",
			hidden = "",
			categories = {
					NatFilterOptionCategory.Layout, NatFilterOptionCategory.LayoutEmptyLines }
			)
	LAYOUT_PAGE_EMPTY_LINES_CUSTOM_2,

	@NatFilterOptionDetail(
			type = NatFilterOptionType.Number, 
			comment = "number of consecutive empty lines in input outputing three empty braille lines",
			defaultValue = "4",
			hidden = "",
			categories = {
					NatFilterOptionCategory.Layout, NatFilterOptionCategory.LayoutEmptyLines }
			)
	LAYOUT_PAGE_EMPTY_LINES_CUSTOM_3,

	/**
	 * Mise en page / propriétés de page - sauts de page
	 * ===================================================
	 */

	@NatFilterOptionDetail(
			type = NatFilterOptionType.Boolean, 
			comment = "Generate page breaks",
			defaultValue = "false",
			hidden = "",
			categories = { NatFilterOptionCategory.Layout,
					NatFilterOptionCategory.LayoutPageBreak }
			)
	LAYOUT_PAGE_BREAKS,

	// TODO correct default value
	@NatFilterOptionDetail(
			type = NatFilterOptionType.Number, 
			comment = "Minimum number of line for a page break",
			defaultValue = "5",
			hidden = "",
			categories = { NatFilterOptionCategory.Layout, NatFilterOptionCategory.LayoutPageBreak }
			)
	LAYOUT_PAGE_BREAKS_MIN_LINES,

	@NatFilterOptionDetail(
			type = NatFilterOptionType.Boolean, 
			comment = "Keep the page breaks of the original document",
			defaultValue = "false",
			hidden = "",
			categories = {
					NatFilterOptionCategory.Layout, NatFilterOptionCategory.LayoutPageBreak }
			)
	LAYOUT_PAGE_BREAKS_KEEP_ORIGINAL,

	/**
	 * Mise en page / propriétés de page - niveaux de titres
	 * ===================================================
	 */

	@NatFilterOptionDetail(
			comment = "Heading,levels,list in output in input document heading level order" + " \"1,2,3,4,5,5,5,5,5\"",
			defaultValue = "1,2,3,4,5,5,5,5,5",
			hidden = "",
			categories = {
					NatFilterOptionCategory.Layout, NatFilterOptionCategory.LayoutPageTitles }
			)
	LAYOUT_PAGE_HEADING_LEVEL,

	/**
	 * Mise en page / propriétés de page - table des matières
	 * =====================================================
	 */

	@NatFilterOptionDetail(
			type = NatFilterOptionType.Boolean, 
			comment = "Build a table of contents",
			defaultValue = "false",
			hidden = "",
			categories = { NatFilterOptionCategory.Layout,
					NatFilterOptionCategory.LayoutPageIndexTable }
			)
	LAYOUT_PAGE_TOC,

	@NatFilterOptionDetail(
			type = NatFilterOptionType.String, 
			comment = "Build a table of contents title",
			defaultValue = "Table des Matières",
			hidden = "false",
			categories = { NatFilterOptionCategory.Layout,
					NatFilterOptionCategory.LayoutPageIndexTable }
			)
	LAYOUT_PAGE_TOC_NAME,

	/**
	 * Mise en page / pas de mise en page
	 * =====================================================
	 */

	@NatFilterOptionDetail(
			comment = "String replacement sources",
			defaultValue = "",
			hidden = "",
			categories = { NatFilterOptionCategory.Layout, NatFilterOptionCategory.LayoutNoLayout }
			)
	LAYOUT_NOLAYOUT_TR_INPUT,

	@NatFilterOptionDetail(
			comment = "String replacement destinations",
			defaultValue = "",
			hidden = "",
			categories = { NatFilterOptionCategory.Layout, NatFilterOptionCategory.LayoutNoLayout }
			)
	LAYOUT_NOLAYOUT_TR_OUTPUT,

	@NatFilterOptionDetail(
			comment = "List,of,string to be added before/after : document, paragraph, line, mathematics, lit, music ",
			defaultValue = "'','','','','','','','','','','',''",
			hidden = "",
			categories = {
					NatFilterOptionCategory.Layout, NatFilterOptionCategory.LayoutNoLayout }
			)
	LAYOUT_NOLAYOUT_OUTPUT_TAGS,

	/**
	 * Format de Document et embossage / embossage
	 * =====================================================
	 */
	@NatFilterOptionDetail(
			type = NatFilterOptionType.Number, 
			comment = "Number of characters in a line",
			defaultValue = "40",
			hidden = "",
			categories = { NatFilterOptionCategory.Format }
			)
	FORMAT_LINE_LENGTH,

	@NatFilterOptionDetail(
			type = NatFilterOptionType.Number, 
			comment = "Number of lines in a page",
			defaultValue = "40",
			hidden = "",
			categories = { NatFilterOptionCategory.Format }
			)
	FORMAT_PAGE_LENGTH,

	@NatFilterOptionDetail(
			comment = "Output document encoding",
			defaultValue = "UTF-8",
			hidden = "",
			categories = { NatFilterOptionCategory.Format }
			)
	FORMAT_OUTPUT_ENCODING,

	@NatFilterOptionDetail(
			comment = "Output braille table",
			defaultValue = SystemBrailleTables.BrailleUTF8, hidden = "",
			categories = { NatFilterOptionCategory.Format }, 
                        possibleValues = {
                            SystemBrailleTables.BrailleUTF8, 
                            SystemBrailleTables.CBFr1252,
                            SystemBrailleTables.CBISF,
                            SystemBrailleTables.CodeUS,
                            SystemBrailleTables.DuxCBfr1252,
                            SystemBrailleTables.DuxTbFr2007,
                            SystemBrailleTables.IBM850,
                            SystemBrailleTables.TbFr2007,
                            SystemBrailleTables.brailleUTF8web
                        }
                        )
	FORMAT_OUTPUT_BRAILLE_TABLE,

	/**
	 * Format de Document et embossage / styles
	 * =====================================================
	 */

	@NatFilterOptionDetail(
			type = NatFilterOptionType.Url, 
			comment = "url of the xml layout style document",
                        defaultValue = "nat://system/xsl/ressources/styles_default.xml",
                        //defaultValue = "file://home/vivien/src/natbraille/trunk/natbraille-core/src/main/resources/org/natbraille/core/xsl/ressources/styles_default.xml",
			hidden = "",
			categories = { NatFilterOptionCategory.Format }
			)
	Stylist,

	/**
	 * system stylesheets
	 */
	@NatFilterOptionDetail(
			type = NatFilterOptionType.Url, 
			comment = "url of Grade 1 Braille conversion xsl stylesheet",
			defaultValue = "nat://system/xsl/fr-g1.xsl",
			hidden = "true",
			categories = { NatFilterOptionCategory.Advanced }
			)
	XSL_g1,

	@NatFilterOptionDetail(
			type = NatFilterOptionType.Url, 
			comment = "url of Grade 2 Braille conversion xsl stylesheet",
			defaultValue = "nat://system/xsl/fr-g2.xsl",
			hidden = "true",
			categories = { NatFilterOptionCategory.Advanced }
			)
	XSL_g2,

	@NatFilterOptionDetail(
			type = NatFilterOptionType.Url, 
			comment = "url of mathematical Braille conversion xsl stylesheet",
			defaultValue = "nat://system/xsl/fr-maths.xsl",
			hidden = "true",
			categories = { NatFilterOptionCategory.Advanced }
			)
	XSL_maths,

	@NatFilterOptionDetail(
			type = NatFilterOptionType.Url, 
			comment = "url of Braille music conversion xsl stylesheet",
			defaultValue = "nat://system/xsl/musique.xsl",
			hidden = "true",
			categories = { NatFilterOptionCategory.Advanced }
			)
	XSL_musique,

	@NatFilterOptionDetail(
			type = NatFilterOptionType.Url, 
			comment = "url of Chemical music conversion xsl stylesheet",
			defaultValue = "nat://system/xsl/fr-chimie.xsl",
			hidden = "true",
			categories = { NatFilterOptionCategory.Advanced }
			)
	XSL_chimie,

	/**
	 * temp stylesheets
	 */

	@NatFilterOptionDetail(
			type = NatFilterOptionType.Url, 
			comment = "rules for grade 2",
			defaultValue = "nat://temp/xsl/fr-g2-rules.xsl",
			hidden = "true",
			categories = { NatFilterOptionCategory.Advanced }
			)	
	XSL_g2_Rules,

	@NatFilterOptionDetail(
			type = NatFilterOptionType.Url, 
			comment = "hyphenation dictionnary",
			defaultValue = "nat://system/xsl/dicts/hyph_fr_nat.dic",
			hidden = "true",
			categories = { NatFilterOptionCategory.Advanced }
			)
	XSL_FR_HYPH_DIC,
	
	@NatFilterOptionDetail(
			type = NatFilterOptionType.Url, 
			comment = "hyphenation rules",
//			defaultValue = "nat://system/xsl/dicts/hyph_fr_nat.dic",
			defaultValue = "",
			hidden = "true",
			categories = { NatFilterOptionCategory.Advanced }
			)
	XSL_FR_HYPH_RULES,

	
	@NatFilterOptionDetail(
			type = NatFilterOptionType.Url, 
			comment = "grade 2 dictionnary",
			defaultValue = "nat://system/xsl/dicts/fr-g2.xml",
			hidden = "true",
			categories = { NatFilterOptionCategory.Advanced }
			)
	XSL_G2_DICT, 
	//
	
	// //

	/**
	 * ????
	 */

	
	@NatFilterOptionDetail(
			comment = "image magick dir for image transcription (not used)",
			defaultValue = "",
			hidden = "true",
			categories = { NatFilterOptionCategory.Advanced }
			)
	imageMagickDir,

	/*
	 * misc
	 */

	@NatFilterOptionDetail(
			type = NatFilterOptionType.Boolean, 
			comment = "keep xml tags?",
			defaultValue = "false",
			hidden = "",
			categories = { NatFilterOptionCategory.Advanced }
			)
	preserveXhtmlTags,

	@NatFilterOptionDetail(
			type = NatFilterOptionType.Boolean, 
			comment = "keep xml tags?",
			defaultValue = "false",
			hidden = "",
			categories = { NatFilterOptionCategory.Advanced }
			)
	preserveTags,


	/**
	 * ????????????????????
	 */

	/**
	 * debug only options
	 */

	@NatFilterOptionDetail(
			type = NatFilterOptionType.Boolean, 
			comment = "write generated documents to temporary file",
			defaultValue = "false",
			hidden = "true",
			categories = { NatFilterOptionCategory.Debug }
			)
	debug_document_write_temp_file,

	@NatFilterOptionDetail(
			type = NatFilterOptionType.Boolean, 
			comment = "write generated xsl transformation stylesheets to temporary file",
			defaultValue = "false",
			hidden = "true",
			categories = { NatFilterOptionCategory.Debug }
			)
	debug_dyn_xsl_write_temp_file,

	
	@NatFilterOptionDetail(
			type = NatFilterOptionType.Boolean, 
			comment = "write transcription options to temporary file",
			defaultValue = "false",
			hidden = "true",
			categories = { NatFilterOptionCategory.Debug }
			)	
	debug_document_write_options_file,

	@NatFilterOptionDetail(
			type = NatFilterOptionType.Boolean, 
			comment = "show generated documents for debug",
			defaultValue = "false",
			hidden = "",
			categories = { NatFilterOptionCategory.Debug }
			)
	debug_document_show,

	@NatFilterOptionDetail(
			type = NatFilterOptionType.Number, 
			comment = "size of tail of generated documents display for debug",
			defaultValue = "200",
			hidden = "",
			categories = { NatFilterOptionCategory.Debug }
			)
	debug_document_show_max_char,

	@NatFilterOptionDetail(
			type = NatFilterOptionType.Boolean, 
			comment = "show detail of dynamic entities resolution for debug",
			defaultValue = "false",
			hidden = "",
			categories = { NatFilterOptionCategory.Debug }
			)
	debug_dyn_ent_res_show,

	@NatFilterOptionDetail(
			type = NatFilterOptionType.Boolean, 
			comment = "show detail of dynamic resolution for debug",
			defaultValue = "false",
			hidden = "",
			categories = { NatFilterOptionCategory.Debug }
			)
	debug_dyn_res_show,

	@NatFilterOptionDetail(
			type = NatFilterOptionType.Boolean, 
			comment = "show generated xsl stylesheets for debug",
			defaultValue = "false",
			hidden = "",
			categories = { NatFilterOptionCategory.Debug }
			)
	debug_dyn_xsl_show,

	@NatFilterOptionDetail(
			type = NatFilterOptionType.Boolean, 
			comment = "debug the transcodeur (boolean)",
			defaultValue = "false",
			hidden = "",
			categories = { NatFilterOptionCategory.Debug }
			)
	debug_xsl_processing, 

	@NatFilterOptionDetail(
			type = NatFilterOptionType.Boolean,
			comment = "do not cleanup temp files after each filter run",
			defaultValue = "false",
			hidden ="",
			categories = { NatFilterOptionCategory.Debug }
			)
	debug_do_not_cleanup_temp_files
	;
	/*
	 * methods =======================
	 */

	/**
	 * get an annotation of the given option and the given annotationClass
	 * 
	 * @param opt
	 * @param annotationClass
	 * @return
	 * @throws NoSuchFieldException
	 * @throws SecurityException
	 */
	private static ArrayList<Annotation> getEnumAnnotations(NatFilterOption opt, Class<?> annotationClass) throws NoSuchFieldException, SecurityException {
		Field field = opt.getClass().getField(opt.name());
		Annotation[] od = field.getAnnotations();
		ArrayList<Annotation> details = new ArrayList<>();
		for (Annotation a : od) {
			if (a.annotationType().equals(annotationClass)) {
				details.add(a);
			}
		}
		return details;
	}

	/**
	 * get NatFitlerOptionsDetail of a given NatFilterOption
	 * 
	 * @return the details
	 */
	public NatFilterOptionDetail getDetail() {
		NatFilterOptionDetail detail = null;
		try {
			ArrayList<Annotation> filterOptionAnnotations = getEnumAnnotations(this, NatFilterOptionDetail.class);
			for (Annotation a : filterOptionAnnotations) {
				if (a.annotationType().equals(NatFilterOptionDetail.class)) {
					detail = (NatFilterOptionDetail) a;
					return detail;
				}
			}
		} catch (NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * test options for correct definition of option detail
	 * @param failOnWarnings
	 * @return returns false if an error is found, or if a warning is found
	 * and failOnWarnings is true. Returns true otherwise.
	 */
	public static boolean testOptionDetails(boolean failOnWarnings) {
		StringBuilder sb = new StringBuilder();
		boolean resu = true;
		List<String> problems = new ArrayList<>();
		for (NatFilterOption o : NatFilterOption.values()) {
			try {
				NatFilterOptionDetail d = o.getDetail();
				if (d.comment().isEmpty()) {
					problems.add("W '"+o.name() +"' has no comment");
					if (failOnWarnings) resu = false;
				}
				if (d.defaultValue() == null) {
					problems.add("E '"+o.name() +"' has no default value");
					resu = false;
				}
				if (d.categories().length == 0) {
					problems.add("W '"+o.name() +"' has no category");
					if (failOnWarnings) resu = false;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (!problems.isEmpty()) {
			sb.append("Should not start with this bad filter option definitions\n");
			for (String pb : problems) {
				sb.append(" - ").append(pb).append("\n");
			}
			System.err.println(sb.toString());
		}
		return resu;
	}
}
