package org.natbraille.core.filter.dynxsl;

import org.natbraille.core.Nat;
import org.natbraille.core.filter.NatFilterException;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.core.gestionnaires.GestionnaireErreur;
import org.natbraille.core.gestionnaires.LogLevel;
import org.natbraille.core.gestionnaires.MessageKind;
import org.natbraille.core.tools.Tools;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.util.ArrayList;

public class ParamsAllXsl extends AbstractNatDynamicXslDocument {


    /*
     * Constantes
     */
    /**
     * indice pour le char de coupure
     */
    public static final int CHAR_COUP = 0;
    /**
     * indice pour le char de coupure esthétique
     */
    public static final int CHAR_COUP_ESTH = 1;
    /**
     * indice pour le char d'espace insécable
     */
    public static final int CHAR_SPACE_UNBREAKABLE = 4;
    /**
     * indice pour le char d'espace sécable
     */
    public static final int CHAR_SPACE = 5;
    /**
     * indice pour le char ne plus couper
     */
    public static final int CHAR_STOP_COUP = 9;

    private NatFilterOptions nfo;


    private NatFilterOptions getOptions() {
        return nfo;
    }

    public ParamsAllXsl(GestionnaireErreur ge, NatFilterOptions nfo) {
        super(ge);
        this.nfo = nfo;
    }

    @Override
    protected Document makeStyleSheet() throws Exception {
        return createParamCommuns(getOptions(), getGestionnaireErreur());
    }

    /**
     * <p>
     * Depuis 2.0, commence à partir la recherche à partir de \u2D30 (alphabet
     * berbère, http://fr.wikipedia.org/wiki/Alphabet_berb%C3%A8re)
     * </p>
     * <p>
     * Avant, cherchait quels étaient les nb premiers caractères non utilisés
     * dans la table braille pour s'en servir comme caractères spéciaux;
     *
     * @param nb         nombre de caractères à rechercher
     * @param gestErreur Un objet <code>GestionnaireErreur</code> pour l'affichage et
     *                   la gestion
     * @param quote      true si il faut mettre les caractères entre quotes (') des
     *                   improbables erreurs.
     * @return les nb premiers caractères disponibles, rendus sous forme de
     * String[]
     */
    public static String[] donneCharNonUtilise(int nb, GestionnaireErreur gestErreur, boolean quote) {
        String[] retour = new String[nb];
        gestErreur.afficheMessage(MessageKind.INFO, "Recherche de " + nb + " caractères non utilisés...", LogLevel.ULTRA);

        String q = "";
        if (quote) {
            q = "'";
        }
        int j = 0;
        while (j < nb) {
            retour[j] = q + (char) ('\u2D30' + j) + q;
            gestErreur.afficheMessage(MessageKind.INFO, "trouvé: '" + retour[j] + "'(" + j + ") ", LogLevel.ULTRA);
            j++;
        }
        return retour;
    }

    /**
     * Fabrique le fichier paramsCommuns.xsl contenant les valeurs des éléments
     * de configuration et de paramétrage partagés par la transcription et la
     * mise en page
     *
     * @param gestErreur Une instance de GestionnaireErreur
     * @throws ParserConfigurationException erreur de parsage
     * @throws TransformerException         erreur lors de la transformation
     * @throws NatFilterException
     */
    public static Document createParamCommuns(NatFilterOptions options, GestionnaireErreur gestErreur) throws ParserConfigurationException, TransformerException, NatFilterException {

        DocumentBuilderFactory fabrique = DocumentBuilderFactory.newInstance();
        DocumentBuilder constructeur = fabrique.newDocumentBuilder();
        Document docParams = constructeur.newDocument();
        // Propriétés de docParam
        docParams.setXmlVersion("1.1");
        docParams.setXmlStandalone(true);

        // racine
        Element racine = docParams.createElement("xsl:stylesheet");
        racine.setAttribute("version", "2.0");
        racine.setAttribute("xmlns:xsl", "http://www.w3.org/1999/XSL/Transform");
        racine.setAttribute("xmlns:xs", "http://www.w3.org/2001/XMLSchema");
        racine.appendChild(docParams.createComment("Auto-generated file; see Transcodeur.java"));
        racine.appendChild(docParams.createComment(Nat.getLicence("", "")));
        // paramètres
        ArrayList<Element> params = new ArrayList<Element>();
        params.add(fabriqueParam(docParams, "longueur", "" + options.getValue(NatFilterOption.FORMAT_LINE_LENGTH), "xs:integer"));
        params.add(fabriqueParam(docParams, "mise_en_page", options.getValue(NatFilterOption.LAYOUT) + "()", "xs:boolean"));
        // caractères de marquage
        String[] carSpec = donneCharNonUtilise(10, gestErreur, true);// je
        // commence
        // hors
        // plage
        // pénible
        gestErreur.afficheMessage(MessageKind.INFO, "Utilisation de '" + carSpec[CHAR_COUP] + "' pour la coupure ", LogLevel.DEBUG);
        gestErreur.afficheMessage(MessageKind.INFO, "Utilisation de '" + carSpec[CHAR_COUP_ESTH] + "' pour la coupure esthétique en mathématique ", LogLevel.DEBUG);
        gestErreur.afficheMessage(MessageKind.INFO, "Utilisation de '" + carSpec[2] + "' pour début d'expression mathématique ", LogLevel.DEBUG);
        gestErreur.afficheMessage(MessageKind.INFO, "Utilisation de '" + carSpec[3] + "' pour fin d'expression mathématique ", LogLevel.DEBUG);
        gestErreur.afficheMessage(MessageKind.INFO, "Utilisation de '" + carSpec[4] + "' pour l'espace insécable à générer ", LogLevel.DEBUG);
        gestErreur.afficheMessage(MessageKind.INFO, "Utilisation de '" + carSpec[5] + "' pour l'espace sécable à générer ", LogLevel.DEBUG);
        gestErreur.afficheMessage(MessageKind.INFO, "utilisation de '" + carSpec[6] + "' pour début de table ", LogLevel.DEBUG);
        gestErreur.afficheMessage(MessageKind.INFO, "utilisation de '" + carSpec[7] + "' pour fin de table ", LogLevel.DEBUG);
        gestErreur.afficheMessage(MessageKind.INFO, "utilisation de '" + carSpec[8] + "' pour saut de ligne à générer ", LogLevel.DEBUG);
        gestErreur.afficheMessage(MessageKind.INFO, "utilisation de '" + carSpec[9] + "' pour ne plus couper à partir de là ", LogLevel.DEBUG);

        params.add(fabriqueParam(docParams, "coupe", carSpec[0], "xs:string"));
        params.add(fabriqueParam(docParams, "coupeEsth", carSpec[1], "xs:string"));
        params.add(fabriqueParam(docParams, "debMath", carSpec[2], "xs:string"));
        params.add(fabriqueParam(docParams, "finMath", carSpec[3], "xs:string"));
        params.add(fabriqueParam(docParams, "espace", carSpec[4], "xs:string"));
        params.add(fabriqueParam(docParams, "espaceSecable", carSpec[5], "xs:string"));
        params.add(fabriqueParam(docParams, "debTable", carSpec[6], "xs:string"));
        params.add(fabriqueParam(docParams, "finTable", carSpec[7], "xs:string"));
        params.add(fabriqueParam(docParams, "sautAGenerer", carSpec[8], "xs:string"));
        params.add(fabriqueParam(docParams, "stopCoup", carSpec[9], "xs:string"));
        params.add(fabriqueParam(docParams, "abrege", options.getValue(NatFilterOption.MODE_G2) + "()", "xs:boolean"));
        params.add(fabriqueParam(docParams, "styles", "document('" + options.getValue(NatFilterOption.Stylist) + "')/styles/*", "node()*"));

        String log = options.getValueAsBoolean(NatFilterOption.debug_xsl_processing) ? "true()" : "false()";

        params.add(fabriqueParam(docParams, "dbg", log, "xs:boolean"));

                /*
                
                
                                        boolean keepSourceOffset = options.getValueAsBoolean(NatFilterOption.KEEP_SOURCE_OFFSETS);
                        Element keepSourceOffsetVariable = docXSL.createElement("xsl:variable");
                        System.out.println("-------ddancing"+NatFilterOption.KEEP_SOURCE_OFFSETS.name());
                        keepSourceOffsetVariable.setAttribute("name", NatFilterOption.KEEP_SOURCE_OFFSETS.name());
                    	keepSourceOffsetVariable.setAttribute("as", "xs:boolean");
                        keepSourceOffsetVariable.setAttribute("select", keepSourceOffset?"true()":"false()");
                        racine.appendChild(keepSourceOffsetVariable);
                */
        {
            boolean keepSourceOffset = options.getValueAsBoolean(NatFilterOption.MODE_DETRANS_KEEP_SOURCE_OFFSETS);
            params.add(fabriqueVariable(
                    docParams, NatFilterOption.MODE_DETRANS_KEEP_SOURCE_OFFSETS.name(),
                    keepSourceOffset ? "true()" : "false()",
                    "xs:boolean"
            ));
        }
        {
            boolean useBrailleSemanticsAnnotation = options.getValueAsBoolean(NatFilterOption.MODE_DETRANS_MATH_USE_BRAILLE_SEMANTICS_ANNOTATION);
            params.add(fabriqueVariable(
                    docParams, NatFilterOption.MODE_DETRANS_MATH_USE_BRAILLE_SEMANTICS_ANNOTATION.name(),
                    useBrailleSemanticsAnnotation ? "true()" : "false()",
                    "xs:boolean"
            ));
        }

        Element e = fabriqueVariable(docParams, "apos", "xs:string");
        e.setTextContent("'");
        params.add(e);
        e = fabriqueVariable(docParams, "quot", "xs:string");
        e.setTextContent("\"");
        params.add(e);
        params.add(fabriqueVariable(docParams, "carcoup", "concat($coupeEsth,$coupe,$debMath,$finMath,$debTable,$finTable)", "xs:string"));

        // les comptes des rajouts
        // Element rajCpt =
        // fabriqueVariable(docParams,"compteRajouts","xs:boolean*");
        // la séquence pour les comptes des rajouts
        // Element seqRajCpt = docParams.createElement("xsl:sequence");
        // String seqRajString = options.getValue(NatOption.RajoutCompte();
        // seqRajString = seqRajString.replaceAll(",", "(),")+"()";//pour avoir
        // les booléens xsl
        // seqRajCpt.setAttribute("select", "("+seqRajString+")");
        // rajCpt.appendChild(seqRajCpt);
        // params.add(rajCpt);

        // les rajouts
        Element raj = fabriqueVariable(docParams, "rajouts", "xs:string*");
        // la séquence pour les rajouts
        Element seqRaj = docParams.createElement("xsl:sequence");
        // On double ici les apostrophes et les & pour pas que ça plante la
        // séquence xsl
        // Il n'est pas possible d'avoir une chaine ajout contenant des champs
        // vides car les string.split ne fonctionnent pas sinon
        // On est donc obligé d'encadrer dès maintenant les champs par des ''
        // ex: ajouts="'','','','','','a','b','c','d'" et pas
        // ajouts=",,,,,,a,b,c,d"

        String[] tabSeqRaj = Tools.intelliSplit(options.getValue(NatFilterOption.LAYOUT_NOLAYOUT_OUTPUT_TAGS), ",");
        String strSeqRaj = "";
        for (int i = 0; i < tabSeqRaj.length; i++) {
            strSeqRaj = strSeqRaj + "'" + tabSeqRaj[i].substring(1, tabSeqRaj[i].length() - 1).replaceAll("'", "''") + "',";
        }
        seqRaj.setAttribute("select", "(" + strSeqRaj.substring(0, strSeqRaj.length() - 1) + ")");
        raj.appendChild(seqRaj);
        params.add(raj);

        // ajout des parametres
        for (int i = 0; i < params.size(); i++) {
            racine.appendChild(params.get(i));
        }
        docParams.appendChild(racine);
/*
		try {
			System.out.println(Utils.toString(docParams));
		} catch (Exception exception) {
			// TODO Auto-generated catch block
			exception.printStackTrace();
		}
	*/
        /* Sauvegarde de document dans un fichier */
        // Source source = new DOMSource(docParams);
        // return source;
        return docParams;
    }


}
