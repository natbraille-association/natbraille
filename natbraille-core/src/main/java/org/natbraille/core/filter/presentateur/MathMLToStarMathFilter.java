/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * Author : Vivien Guillet <vivien.guillet@laposte.net>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.core.filter.presentateur;

import java.io.ByteArrayOutputStream;
import org.natbraille.core.NatDynamicResolver;
import org.natbraille.core.document.NatDocument;
import org.natbraille.core.filter.NatTransformerFilter;
import static org.natbraille.starmath.toStarMath.StarMathExport.mathmlToStarMathAsElement;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 *
 * @author vivien
 */
public class MathMLToStarMathFilter extends NatTransformerFilter {

    public MathMLToStarMathFilter(NatDynamicResolver ndr) {
        super(ndr);
    }

    @Override
    protected NatDocument process(NatDocument indoc) throws Exception {
        NatDocument outDoc = new NatDocument();

        String s = indoc.getString().replace("<m:", "<").replace("</m:", "</");;
        
        NatDocument tmpNatDocument = new NatDocument();
        tmpNatDocument.setString(s);               
        Document doc = tmpNatDocument.getDocument(getUriResolver().getXMLReader());
        NodeList mathElements = doc.getElementsByTagName("math");
        
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                
        for (int i = 0; i < mathElements.getLength(); i++) {
            Element mathElement = (Element) mathElements.item(i);
            
            Element starMath = mathmlToStarMathAsElement(mathElement);            
            //starMath.getTextContent();
           // baos.write(starMath.getTextContent().getBytes());
            org.natbraille.starmath.DomUtils.ElementToStream(starMath, baos);
        }
        outDoc.setByteArrayOutputStream(baos);
    
        return outDoc;
    }

    @Override
    protected String getPrettyName() {
        return "Présentateur StarMath";
    }

}
