package org.natbraille.core.filter.dynxsl;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.natbraille.core.Nat;
import org.natbraille.core.NatStaticResolver;
import org.natbraille.core.filter.NatFilterException;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.core.gestionnaires.GestionnaireErreur;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class XslMepXsl extends AbstractNatDynamicXslDocument {

	NatFilterOptions options;
	
	private NatFilterOptions getOptions() {
		return options;
	}

	public XslMepXsl(GestionnaireErreur ge, NatFilterOptions nfo) {
		super(ge);
		this.options = nfo;
	}
       
        
	@Override
	protected Document makeStyleSheet() throws Exception {
		return createXslMep(getOptions(), getGestionnaireErreur());
	}
	/**
	 * Creates the xsl-mep.xsl file for page-layout transformations including other xsl stylesheets depending on the configuration stored in <code>configNat</code>
	 * @throws ParserConfigurationException erreur de parsage
	 * @throws TransformerException erreur lors de la transformation
	 * @throws NatFilterException 
	 * @throws DOMException 
	 */
	public static Document createXslMep(NatFilterOptions options, GestionnaireErreur ge) throws ParserConfigurationException, DOMException, NatFilterException { 
	
		//NatFilterOptions options = getUriResolver().getOptions();
	//	GestionnaireErreur ges = getUriResolver().getGestionnaireErreur();
		
		DocumentBuilderFactory fabrique = DocumentBuilderFactory.newInstance();
		DocumentBuilder constructeur= fabrique.newDocumentBuilder();
	    Document docXSL = constructeur.newDocument();
	    // Propriétés de docParam
	    docXSL.setXmlVersion("1.1");
	    docXSL.setXmlStandalone(true);
	    //docXSL.createEntityReference("Table_pour_chaines.ent");
	    
	    //racine
	    Element racine = docXSL.createElement("xsl:stylesheet");
	    racine.setAttribute("version", "2.0");
	    racine.setAttribute("xmlns:xsl", "http://www.w3.org/1999/XSL/Transform");
	    racine.setAttribute("xmlns:xs","http://www.w3.org/2001/XMLSchema");
	    racine.setAttribute("xmlns:saxon","http://icl.com/saxon");
	    racine.setAttribute("xmlns:m","http://www.w3.org/1998/Math/MathML");
	    racine.setAttribute("xmlns:fn","http://www.w3.org/2005/xpath-functions");
	    racine.setAttribute("xmlns:lit","espacelit");
	    racine.setAttribute("xmlns:nat", "http://natbraille/free/fr/xsl");
	    racine.setAttribute("xmlns:doc","espaceDoc");
	    racine.appendChild(docXSL.createComment("Auto-generated file; see PresentateurMEP.java"));
	    racine.appendChild(docXSL.createComment(Nat.getLicence("", "")));

		//FileWriter fichierXSL2 = new FileWriter(filtre);
		Element output = docXSL.createElement("xsl:output");
		output.setAttribute("encoding","UTF-8");
		output.setAttribute("indent","no");
		racine.appendChild(output);
		
		Element paramAll = docXSL.createElement("xsl:include");
		paramAll.setAttribute("href",NatStaticResolver.XSL_NAT_TMP_PARAMS_ALL);
		racine.appendChild(paramAll);
		
		Element paramTrans = docXSL.createElement("xsl:include");
		paramTrans.setAttribute("href",NatStaticResolver.XSL_NAT_TMP_PARAMS_MEP);
		racine.appendChild(paramTrans);
		
		String fichMEP ;
		if (options.getValueAsBoolean(NatFilterOption.LAYOUT)){
		//if(ConfigNat.getCurrentConfig().getMep()){
		
			fichMEP= NatStaticResolver.LAYOUT_FILTER;
			//output.setAttribute("method","text");
			output.setAttribute("method","xml");
			String fichHyphens = NatStaticResolver.XSL_NAT_TMP_HYPHENS;
			Element hyph = docXSL.createElement("xsl:include");
			hyph.setAttribute("href",fichHyphens);
			racine.appendChild(hyph);
		} else if 
			//if(ConfigNat.getCurrentConfig().getPreserveTag())
		 (options.getValueAsBoolean(NatFilterOption.preserveTags)){
			fichMEP = NatStaticResolver.NO_LAYOUT_PRESERVE_TAG_FILTER;
			output.setAttribute("method","xml");
		} else	{
			output.setAttribute("method","text");
			fichMEP = NatStaticResolver.NO_LAYOUT_FILTER;
			//Element table_chaine = docXSL.createElement("xsl:include");
			//table_chaine.setAttribute("href","Table_pour_chaines.ent");
			//racine.appendChild(table_chaine);
		}
		
		Element mep = docXSL.createElement("xsl:include");
		mep.setAttribute("href",fichMEP);
		racine.appendChild(mep);
		docXSL.appendChild(racine);		    
	    
		return docXSL;
	}


	
	
}
