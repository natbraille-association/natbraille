package org.natbraille.core.filter.options;

public enum NatFilterOptionType {

	String,
	
	Boolean,
	
	Url,
	
	Number
}
