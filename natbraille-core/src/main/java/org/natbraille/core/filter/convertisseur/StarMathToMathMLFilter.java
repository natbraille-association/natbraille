/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.natbraille.core.filter.convertisseur;

import java.nio.charset.Charset;
import org.natbraille.core.NatDynamicResolver;
import org.natbraille.core.document.NatDocument;
import org.natbraille.core.filter.NatTransformerFilter;
import org.natbraille.core.windob.WindobFormat;
import org.natbraille.starmath.toMathML.StarMathParser;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.ls.DOMImplementationLS;

/**
 *
 * @author vivien
 */
public class StarMathToMathMLFilter extends NatTransformerFilter {

    public StarMathToMathMLFilter(NatDynamicResolver ndr) {
        super(ndr);
    }

    @Override
    protected NatDocument process(NatDocument indoc) throws Exception {
        Document domDocument = StarMathParser.parseString(indoc.getString(), true);
        NatDocument outDoc = new NatDocument();

        if (false) {
            outDoc.setDocument(domDocument);

            String s2 = outDoc.getString();
            // s2 = s2.replace("<math xmlns=\"http://www.w3.org/1998/Math/MathML\" display=\"block\">","<math>");
            s2 = s2.replace("<semantics xmlns=\"\" oeo=\"32\" oso=\"0\">", "");
            s2 = s2.replace("</semantics>", "");
            NatDocument outDoc2 = new NatDocument();

            System.out.println(s2);
            outDoc2.setString(s2);
            outDoc = outDoc2;
            // outDoc.forceContentType("application/xml");
        } else if (true) {

            // make windob dom document
            Document windobDomDocument = WindobFormat.newWindobDomDocument(getUriResolver(), getUriResolver());

            // clone math childs into windob dom document
            //NodeList mathElements = domDocument.getElementsByTagName("math");
            NodeList mathElements = domDocument.getElementsByTagName("semantics");
            for (int i = 0; i < mathElements.getLength(); i++) {
                Element phrase = windobDomDocument.createElement("phrase");
                Element math = windobDomDocument.createElementNS("http://www.w3.org/1998/Math/MathML", "math");
                NodeList mathChilds = mathElements.item(i).getChildNodes();
                for (int j = 0; j < mathChilds.getLength(); j++) {
                    Node cloned = windobDomDocument.adoptNode(mathChilds.item(j).cloneNode(true));
                    // ((Element)cloned).removeAttribute("xmlns");
                    math.appendChild(cloned);
                }
                phrase.appendChild(math);
                windobDomDocument.getDocumentElement().appendChild(phrase);
            }
            // print
            System.err.println("== windob2   ==\n" + ((DOMImplementationLS) windobDomDocument.getImplementation()).createLSSerializer().writeToString(windobDomDocument) + "\n====");

            // outDoc.setDocument(windobDomDocument);
            // ??????
            NatDocument tmp = new NatDocument();
            tmp.setDocument(windobDomDocument);                        
            outDoc.setString(tmp.getString());

            
            
            System.out.println("== windob1515 ==\n" + windobDomDocument.getDoctype().getPublicId());
            System.out.println("== windob1515 ==\n" + windobDomDocument.getDoctype().getSystemId());
        } else {
            // ok
            String asString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                    + "<!DOCTYPE doc:doc\n"
                    + "  SYSTEM \"nat://system/xsl/mmlents/windob.dtd\">\n"
                    + "<doc:doc xmlns:doc=\"espaceDoc\">\n"
                    + "   <phrase>\n"
                    + "      <math xmlns=\"http://www.w3.org/1998/Math/MathML\">\n"
                    + "         <mrow xmlns=\"\" oeo=\"4\" oso=\"0\" x-ruleName=\"doSum\">\n"
                    + "            <mi mathvariant=\"italic\" oeo=\"0\" oso=\"0\" x-ruleName=\"identifier\">a</mi>\n"
                    + "            <mo oeo=\"1\" oso=\"1\" stretchy=\"false\" x-ruleName=\"sumOperatorGroup\">+</mo>\n"
                    + "            <mi mathvariant=\"italic\" oeo=\"2\" oso=\"2\" x-ruleName=\"identifier\">b</mi>\n"
                    + "            <mo oeo=\"3\" oso=\"3\" stretchy=\"false\" x-ruleName=\"sumOperatorGroup\">+</mo>\n"
                    + "            <mi mathvariant=\"italic\" oeo=\"4\" oso=\"4\" x-ruleName=\"identifier\">c</mi>\n"
                    + "         </mrow>\n"
                    + "      </math>\n"
                    + "   </phrase>\n"
                    + "</doc:doc>";

            outDoc.setString(asString);
        }

        outDoc.forceCharset(Charset.forName("utf8"));
        System.err.println("== windob3   ==\n" + outDoc.getString());
        return outDoc;

    }

    @Override
    protected String getPrettyName() {
        return "starmath 5.0 to mathml3";
    }

}
