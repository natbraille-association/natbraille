/*
/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.core.filter.convertisseur;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import org.natbraille.core.NatStaticResolver;
import org.natbraille.core.document.NatDocument;
import org.natbraille.core.filter.NatFilterErrorCode;
import org.natbraille.core.filter.NatFilterException;
import org.natbraille.core.filter.NatTransformerFilter;
import org.natbraille.core.NatDynamicResolver;
import org.natbraille.core.gestionnaires.LogLevel;
import org.natbraille.core.gestionnaires.MessageKind;

/**
 * Permet l'exécution d'un convertisseur externe (ligne de commande et paramètre)
 * Contient également, si besoin, le convertisseur suivant à utiliser
 * @author bruno
 *
 */
public class ConvertisseurExterne extends NatTransformerFilter
{
	public ConvertisseurExterne(NatDynamicResolver ndr, String commandLine, int code){
		super(ndr);
		this.commandLine = commandLine;
		this.code = code;
	}

	/** Constante indiquant qu'il ne faut pas utiliser de convertisseur supplémentaire après la conversion */
	public static final int CONV_AUCUN = 0;
	/** Constante indiquant qu'il faut utiliser le convertisseur XHTML après la conversion */
	public static final int CONV_XHTML = 1;

	/** 
	 * ligne de commande à exécuter
	 * $i sera remplacé par le fichier source
	 * $o sera remplacé par le fichier cible
	 **/
	private String commandLine = "";
	/** code pour le convertisseur supplémentaire */
	private int code = 0; 

	private String inFileSuffix = "tmp";
	private String outFileSuffix = "tmp";

	public String getCommandLine() {
		return commandLine;
	}

	public void setCommandLine(String commandLine) {
		this.commandLine = commandLine;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getInFileSuffix() {
		return inFileSuffix;
	}

	public void setInFileSuffix(String inFileSuffix) {
		this.inFileSuffix = inFileSuffix;
	}

	public String getOutFileSuffix() {
		return outFileSuffix;
	}

	public void setOutFileSuffix(String outFileSuffix) {
		this.outFileSuffix = outFileSuffix;
	}

	@Override
	protected NatDocument process(NatDocument inDoc) throws NatFilterException {
		NatDocument retour = null;

		try {
			/* si besoin, appel du convertisseur supplémentaire */
			switch(getCode())
			{
			case CONV_AUCUN:
				retour = execCommande(inDoc);
				break;
			case CONV_XHTML:
				NatDocument preImportedDoc = execCommande(inDoc);
				/*
				 * ????
				 * Le fichier généré contient une dtd qui doit être résolue avec une connexion
				 * internet: on supprime cette ligne du fichier
				 */
				if(preImportedDoc != null) {
					String s =  preImportedDoc.getString();

					//s=s.replaceFirst(".*DOCTYPE.*", "").replaceFirst(".*xhtml-math11-f.dtd.*", "");
					s=s.replaceFirst("\".*xhtml-math11-f.dtd", "\""+NatStaticResolver.WINDOB_DTD_SYSTEM_ID);

					NatDocument tmpXhtmlDoc = new NatDocument();
					tmpXhtmlDoc.setString(s);

					ConvertisseurXHTML convertisseurXHTML = new ConvertisseurXHTML(getUriResolver());
					retour = convertisseurXHTML.run(tmpXhtmlDoc);

				}
				break;
			}
		} catch (Exception e){
			throw new NatFilterException(NatFilterErrorCode.FILTER_RUNTIME_ERROR, "",e);
		}
		return retour;
	}

	/**
	 * Exécute le programme externe avec ses paramètres
	 * @param c la cible du convertisseur (fichier produit)
	 * @param gest the error manager in use
	 * @return true si conversion réalisée
	 * @throws Exception 
	 */
	private NatDocument execCommande(NatDocument inDoc) throws Exception
	{
		NatDocument outDoc = null;
		File inDocTmpFile = null;
		inDocTmpFile = inDoc.writeTmpFile(getTmpDir(),"texdoc",getInFileSuffix());

		NatDocument tmpNatDoc = new NatDocument();
		File outDocTmpFile = tmpNatDoc.reserveTmpFile(getTmpDir(),"texdoc",getOutFileSuffix());

		String ldcExec = commandLine.replaceFirst("[$]i", inDocTmpFile.getAbsolutePath()).replaceFirst("[$]o", outDocTmpFile.getAbsolutePath());

		gestionnaireErreur.afficheMessage(MessageKind.INFO,"ligne de commande",LogLevel.NORMAL);

		Runtime runTime = Runtime.getRuntime();
		Process p = runTime.exec(ldcExec);
		System.out.println("lancé");
		// read the standard output of the command
		BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
		BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
		String s ="";
		while (!procDone(p))
		{
			while((s=stdInput.readLine()) !=null){
				getGestionnaireErreur().afficheMessage(MessageKind.INFO,s, LogLevel.DEBUG);
			}
			while((s=stdError.readLine()) !=null){
				getGestionnaireErreur().afficheMessage(MessageKind.INFO, s, LogLevel.SILENT);
			}
		}
		stdInput.close();
		stdError.close();
		int res = p.waitFor();
		if (res != 0){
			throw new NatFilterException(NatFilterErrorCode.NAT_EXTERNAL_FILTER_ERROR,"process did not return normally");			
		}
		outDoc = new NatDocument();
		outDoc.setInputstream(new FileInputStream(outDocTmpFile));


		return outDoc;
	}
	/**
	 * Return true if the process p is done
	 * @param p the running process
	 * @return true when process p is done
	 */
	private boolean procDone(Process p)
	{
		boolean retour = true;
		try{p.exitValue();}
		catch(IllegalThreadStateException e){retour=false;}
		return retour;
	}

	@Override
	protected String getPrettyName() {
		return "external (command line) converter";
	}

}
