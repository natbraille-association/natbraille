package org.natbraille.core.filter.dynxsl;

import java.net.URI;
import java.nio.ByteBuffer;

import org.natbraille.core.NatDynamicResolver;
import org.natbraille.core.document.Utils;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.core.filter.transcodeur.HyphenationToolkit;
import org.natbraille.core.gestionnaires.GestionnaireErreur;
import org.w3c.dom.Document;
import org.xml.sax.XMLReader;

// http://natbraille.free.fr/aide/coupure.html

public class HyphensXsl extends AbstractNatDynamicXslDocument {

	private NatFilterOptions options;
	private XMLReader xmlReader;
	private NatDynamicResolver natDynamicResolver;



	private XMLReader getXmlReader() {
		return xmlReader;
	}

	private NatFilterOptions getOptions() {
		return options;
	}

	public HyphensXsl(GestionnaireErreur ge, NatFilterOptions nfo, XMLReader xmlReader, NatDynamicResolver natDynamicResolver) {
		super(ge);
		this.options = nfo;
		this.xmlReader = xmlReader;
		this.natDynamicResolver = natDynamicResolver;
	}


	@Override
	protected Document makeStyleSheet() throws Exception {
		// defaults : nat://system/xsl/dicts/hyph_fr_nat.dic
		String hyph_dic_url = getOptions().getValue(NatFilterOption.XSL_FR_HYPH_DIC);
		ByteBuffer xslByteBuffer = HyphenationToolkit.getDicoNatSource(new URI(hyph_dic_url), "UTF-8",this.natDynamicResolver);
		
		
		
		
		return Utils.toDocument(xslByteBuffer, getXmlReader());
	}

}
