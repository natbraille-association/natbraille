/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.core.filter.transcriptor;

import org.natbraille.core.document.NatDocument;
import org.natbraille.core.document.NatDocumentException;
import org.natbraille.core.filter.NatFilterChain;
import org.natbraille.core.filter.NatFilterErrorCode;
import org.natbraille.core.filter.NatFilterException;
import org.natbraille.core.filter.NatFilterConfigurator;
import org.natbraille.core.NatDynamicResolver;
import org.natbraille.core.filter.convertisseur.Convertisseur2ODT;
import org.natbraille.core.filter.convertisseur.ConvertisseurExterne;
import org.natbraille.core.filter.convertisseur.ConvertisseurOpenOffice;
import org.natbraille.core.filter.convertisseur.ConvertisseurTan;
import org.natbraille.core.filter.convertisseur.ConvertisseurTexte;
import org.natbraille.core.filter.convertisseur.ConvertisseurTexteMixte;
import org.natbraille.core.filter.convertisseur.ConvertisseurXHTML;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.core.filter.presentateur.BrailleTableModifierJava;
import org.natbraille.core.filter.presentateur.PresentateurMEP;
import org.natbraille.core.filter.transcodeur.AmbiguityFilter;
import org.natbraille.core.filter.transcodeur.TranscodeurNormal;
import org.natbraille.core.gestionnaires.GestionnaireErreur;
import org.natbraille.core.gestionnaires.LogLevel;
import org.natbraille.core.gestionnaires.MessageContents;
import org.natbraille.core.gestionnaires.MessageKind;

/**
 * End to end {@link NatDocument} transcription with automatic input format
 * recognition.
 *
 * @author vivien
 */
public class Transcriptor extends NatFilterChain {

    @Override
    protected String getPrettyName() {
        return "TRANSCRIPTOR";
    }

    public Transcriptor(NatFilterConfigurator natFilterConfigurator) {
        super(natFilterConfigurator);
    }

    // TODO could be precomputed
    private NatFilterChain postImportFilterChain = null;

    /**
     * @return the postImportFilterChain
     */
    private NatFilterChain getPostImportFilterChain() {
        return postImportFilterChain;
    }

    /**
     * @param postImportFilterChain the postImportFilterChain to set
     */
    private void setPostImportFilterChain(NatFilterChain postImportFilterChain) {
        this.postImportFilterChain = postImportFilterChain;
    }

    @Override
    protected void initialize() throws Exception {

        NatFilterOptions o = getNatFilterConfigurator().getNatFilterOptions();
        
        NatFilterChain piFilterChain = getNatFilterConfigurator().newFilterChain();

        try {
            piFilterChain.setParentTmpDir(getTmpDir());
        } catch (Exception e) {
            gestionnaireErreur.afficheMessage(MessageKind.WARNING, MessageContents.Tr("cannot set temp directory"), LogLevel.NORMAL);
        }

        try {
            piFilterChain.addNewFilter(TranscodeurNormal.class);
	    if (!o.getValueAsBoolean(NatFilterOption.MODE_DETRANS)) {
		piFilterChain.addNewFilter(AmbiguityFilter.class);
	    }
        } catch (Exception e) {
            throw new NatFilterException(NatFilterErrorCode.TRANS_CANNOT_BUILD_TRANSCODEUR, "", e);
        }

        try {
	    if (!o.getValueAsBoolean(NatFilterOption.MODE_DETRANS)) {
		piFilterChain.addNewFilter(PresentateurMEP.class);
	    }
        } catch (Exception e) {
            throw new NatFilterException(NatFilterErrorCode.TRANS_CANNOT_BUILD_PRESENTATOR, "", e);
        }

        try {
            if (!o.getValueAsBoolean(NatFilterOption.MODE_DETRANS)) {
                piFilterChain.addNewFilter(BrailleTableModifierJava.class);
            }
        } catch (Exception e) {
            throw new NatFilterException(NatFilterErrorCode.TRANS_CANNOT_BUILD_BRAILLE_TABLE_MODIFIER, "", e);
        }

        setPostImportFilterChain(piFilterChain);

    }

    public NatFilterChain getImportFilterChain(NatDocument natDoc) throws NatFilterException, NatDocumentException {

        String ct = natDoc.getOrGuessContentType();

        GestionnaireErreur g = getNatFilterConfigurator().getGestionnaireErreur();
        NatFilterOptions o = getNatFilterConfigurator().getNatFilterOptions();
        NatDynamicResolver ndr = getNatFilterConfigurator().getNatDynamicResolver();

        NatFilterChain filterChain = getNatFilterConfigurator().newFilterChain();
        try {
            filterChain.setParentTmpDir(getTmpDir());
        } catch (Exception e) {
            gestionnaireErreur.afficheMessage(MessageKind.WARNING, MessageContents.Tr("cannot set temp directory"), LogLevel.NORMAL);
        }

        g.afficheMessage(MessageKind.INFO, MessageContents.Tr("document content is {0}").setValue(ct), LogLevel.VERBOSE);

        switch (ct) {
            case "application/x-tex":
                filterChain.addFilters(new ConvertisseurExterne(ndr, "commandLine", 1));
                filterChain.addNewFilter(ConvertisseurXHTML.class);
                break;
            case "application/msword":
            case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
            case "application/rtf":
                filterChain.addNewFilter(Convertisseur2ODT.class);
                filterChain.addNewFilter(ConvertisseurOpenOffice.class);
                filterChain.addNewFilter(ConvertisseurXHTML.class);
                break;
            case "application/vnd.oasis.opendocument.text":
                filterChain.addNewFilter(ConvertisseurOpenOffice.class);
                filterChain.addNewFilter(ConvertisseurXHTML.class);
                break;
         //   case "application/xml"              :
            case "application/xhtml+xml":
                // TODO : chercher dans le doctype et appeler le convertisseur correspondant.
                // cf ligne 114 ConvertisseurXML
                filterChain.addNewFilter(ConvertisseurXHTML.class);
            case "application/html":
                filterChain.addNewFilter(ConvertisseurXHTML.class);
                break;
            case "text/plain":
                if ((natDoc.getBrailletable() != null) || getOptionAsBoolean(NatFilterOption.MODE_DETRANS)) {
                    g.afficheMessage(MessageKind.INFO, MessageContents.Tr("reverse transcription"), LogLevel.VERBOSE);
                    filterChain.addNewFilter(ConvertisseurTan.class);
                } else {
                    if (getOptionAsBoolean(NatFilterOption.MODE_MATH) || getOptionAsBoolean(NatFilterOption.MODE_MUSIC)) {
                        filterChain.addNewFilter(ConvertisseurTexteMixte.class);
                    } else {
                        filterChain.addNewFilter(ConvertisseurTexte.class);
                    }
                }
                break;
            default:
                g.afficheMessage(MessageKind.ERROR, MessageContents.Tr("don't know what to do with doctype {0}").setValue(ct), LogLevel.NORMAL);
                throw new NatFilterException(NatFilterErrorCode.TRANS_CANNOT_DETERMINE_IMPORT_FILTER, ct);
        }
        return filterChain;
    }

    @Override
    protected void preProcess(NatDocument natDocument) throws NatFilterException {
        try {
            removeFilters();

            MessageContents msg = MessageContents.Tr("document has content-type ''{0}'' and charset ''{1}''").
                    setValues(natDocument.getOrGuessContentType(), natDocument.getOrGuessCharset());

            getGestionnaireErreur().afficheMessage(MessageKind.INFO, msg, LogLevel.NORMAL);
            NatFilterChain chainImport = getImportFilterChain(natDocument);
            NatFilterChain chainPostImport = getPostImportFilterChain();

            addFilters(chainImport.getFilters());
            addFilters(chainPostImport.getFilters());

        } catch (Exception e) {
            throw new NatFilterException(NatFilterErrorCode.FILTER_CHAIN_PREPROCESSING_ERROR, null, e);
        }

    }
}
