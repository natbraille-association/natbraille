package org.natbraille.core.filter.dynxsl;

import java.net.URI;
import java.util.ArrayList;
import org.natbraille.core.NatDynamicResolver;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.filter.rules.Regle;
import org.natbraille.core.filter.rules.RulesToolKit;
import org.w3c.dom.Document;

public class FrG2RulesXsl extends AbstractNatDynamicXslDocument {

	private NatDynamicResolver ndr;
	

	private NatDynamicResolver getResolver() {
		return ndr;
	}

	public FrG2RulesXsl(NatDynamicResolver ndr) {
		super(ndr.getGestionnaireErreur());
		this.ndr = ndr;
	}

	@Override
	protected Document makeStyleSheet() throws Exception {
		String g2Dict = getResolver().getOptions().getValue(NatFilterOption.XSL_G2_DICT);
		//String g2Dict = "nat://system/xsl/dicts/fr-g2.xml";
		
		ArrayList<Regle> liste = RulesToolKit.getRules(getResolver(), new URI(g2Dict));
		return RulesToolKit.writeRules(getResolver(), liste);
	}

	

}
