/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.core.filter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.Properties;

import org.natbraille.core.document.NatDocument;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.core.gestionnaires.GestionnaireErreur;
import org.natbraille.core.gestionnaires.LogLevel;
import org.natbraille.core.gestionnaires.MessageContents;
import org.natbraille.core.gestionnaires.MessageKind;
import org.natbraille.core.tools.NatTempFiles;

public abstract class NatFilter {


    /**
     * NatFilterDetail annotation Handling
     */

    /**
     * filter options. Can only be set using the
     * filter constructor
     */
    private NatFilterOptions options = null;

    private final NatFilterOptions getOptions() {
        return options;
    }

    private final void setOptions(NatFilterOptions options) {
        this.options = options;
    }

    protected final String getOption(NatFilterOption key) throws NatFilterException {
        return getOptions().getValue(key);
    }

    protected final int getOptionAsInteger(NatFilterOption key) throws NatFilterException {
        return Integer.parseInt(getOptions().getValue(key));
    }

    protected final Boolean getOptionAsBoolean(NatFilterOption key) throws NatFilterException {
        return Boolean.parseBoolean(getOptions().getValue(key));
    }

    /**
     * error handler
     */
    protected GestionnaireErreur gestionnaireErreur = null;

    protected GestionnaireErreur getGestionnaireErreur() {
        return gestionnaireErreur;
    }

    private void setGestionnaireErreur(GestionnaireErreur gestionnaireErreur) {
        this.gestionnaireErreur = gestionnaireErreur;
    }

    /**
     * don't initialize things in the constructor, do it in {@see initialize}
     * if the filter is never called - for instance because a prececessor filter
     * failed.
     */
    public NatFilter(NatFilterOptions options, GestionnaireErreur gestionnaireErreurs) {
        setOptions(options);
        setGestionnaireErreur(gestionnaireErreurs);
    }

    /**
     * called for running the actual transformation
     * process must be implemented in deriving classes
     */
    protected abstract NatDocument process(NatDocument indoc) throws Exception;

    /**
     * returns a human-readable name of the filter
     */
    protected abstract String getPrettyName();

    /**
     * returns a start message for the filter
     */
    protected MessageContents getStartMessage() {
        MessageContents mc = MessageContents.Tr("filter {0} starting").setValue(getPrettyName());
        return mc;
    }

    /**
     * returns an end message for the filter
     */
    protected MessageContents getEndMessage(Long time) {
        MessageContents mc = MessageContents.Tr("filter {0} exits after {1} ms").setValues(getPrettyName(), time);
        return mc;
    }

    /**
     * returns an error message for the filter
     */
    protected MessageContents getErrorMessage() {
        MessageContents mc = MessageContents.Tr("filter {0} error").setValue(getPrettyName());
        return mc;
    }

    /**
     * returns a success message for the filter
     */
    protected MessageContents getSuccessMessage() {
        MessageContents mc = MessageContents.Tr("filter {0} success").setValue(getPrettyName());
        return mc;
    }

    protected boolean getDisplayInitializationMessage() {
        return false;
    }

    /**
     * returns the initialization message
     *
     * @return
     */
    private MessageContents getInitializationMessage() {
        MessageContents mc = MessageContents.Tr("filter {0} initializing").setValue(getPrettyName());
        return mc;
    }

    /**
     *
     */
    Boolean isInitialized = false;

    /**
     * @return the isInitialized
     */
    private Boolean getIsInitialized() {
        return isInitialized;
    }

    /**
     * @param isInitialized the isInitialized to set
     */
    private void setIsInitialized(Boolean isInitialized) {
        this.isInitialized = isInitialized;
    }

    /**
     * called at filter initialization. This step
     * is not done in the NatFilter constructor :
     * if a previous step does not work, the unnecesseray operation is not done at all.
     *
     * @throws Exception
     */
    protected void initialize() throws Exception {

    }

    /**
     * one time intialization of filter
     * can be called at filter construction
     * or before the first pass
     *
     * @throws Exception
     */
    private final void _initialize() throws NatFilterException {
        try {
            if (!getIsInitialized()) {

                //debug
                if (getOptionAsBoolean(NatFilterOption.debug_document_write_options_file)) {
                    saveTmpNatOptions(getOptions());
                }
                // don't display init if not overloaded with return true
                if (getDisplayInitializationMessage()) {
                    getGestionnaireErreur().afficheMessage(MessageKind.INFO, getInitializationMessage(), LogLevel.VERBOSE);
                }
                initialize();
                setIsInitialized(true);
            }
        } catch (Exception e) {
            throw new NatFilterException(NatFilterErrorCode.FILTER_INITIALIZATION_ERROR, null, e);
        }
    }

    private File saveTmpNatOptions(NatFilterOptions nfo) {
        File f = null;
        try {
            f = NatTempFiles.newFile(getTmpDir(), "config_", ".properties");
            Properties p = nfo.getAsProperties();
            p.store(new FileOutputStream(f), "copied");
        } catch (Exception e) {
            getGestionnaireErreur().afficheMessage(MessageKind.WARNING, MessageContents.Tr("cannot save the temporary document"), LogLevel.NORMAL);
        }
        return f;
    }

    private File saveTmpNatDocument(NatDocument doc, String prefix) {
        File f = null;
        try {
            StringBuilder baseName = new StringBuilder();
            String extension = "";

            String originalFilename = doc.getOriginalFileName();
            if (prefix != null) {
                baseName.append(prefix);
                baseName.append('_');
            }
            if (originalFilename != null) {
                // document name and extension
                String fn = Paths.get(originalFilename).getFileName().toString();
                baseName.append(fn.replaceAll("(.*)\\..*", "$1"));
                baseName.append('_');
                extension = fn.replaceAll(".*(\\..*)", "$1");
            }
            f = NatTempFiles.newFile(getTmpDir(), baseName.toString(), extension);
            doc.writeFile(f);
        } catch (Exception e) {
            getGestionnaireErreur().afficheMessage(MessageKind.WARNING, MessageContents.Tr("cannot save the temporary document"), LogLevel.NORMAL);
        }
        return f;
    }

    /**
     * runs a transcription by calling the process method and measures time
     * return the transformer document
     *
     * @param indoc
     * @return the transformed document or throw an exception
     * @throws NatFilterException encapsulate all exceptions happening in the process method
     */
    public final NatDocument run(NatDocument indoc) throws NatFilterException {

        NatDocument resu = null;

        GestionnaireErreur g = getGestionnaireErreur();
        chronometer.start();
        try {
            // renew the temp dir name in the same subdirectory
            if (getOptionAsBoolean(NatFilterOption.debug_document_write_temp_file)
                    || getOptionAsBoolean(NatFilterOption.debug_document_write_options_file)
                    || getOptionAsBoolean(NatFilterOption.debug_dyn_xsl_write_temp_file)
            ) {
                newTmpDir();
            }
            if (getOptionAsBoolean(NatFilterOption.debug_document_write_temp_file)) {
                File f = saveTmpNatDocument(indoc, "input");
                MessageContents mc = MessageContents.Tr("input document of ''{0}'' is in file : ''{1}''")
                        .setValues(getPrettyName(), f.getAbsolutePath());
                g.afficheMessage(MessageKind.INFO, mc, LogLevel.NONE);
            }


            g.afficheMessage(MessageKind.INFO, getStartMessage(), LogLevel.NORMAL);
            _initialize();

            resu = process(indoc);
            if (resu != null) {
                chronometer.stop();

                g.afficheMessage(MessageKind.SUCCESS, getEndMessage(chronometer.time()), LogLevel.NORMAL);

                if (getOptionAsBoolean(NatFilterOption.debug_document_write_temp_file)) {
                    File f = saveTmpNatDocument(resu, "output");
                    MessageContents mc = MessageContents.Tr("output document of ''{0}'' is in file : ''{1}''")
                            .setValues(getPrettyName(), f.getAbsolutePath());
                    g.afficheMessage(MessageKind.INFO, mc, LogLevel.NONE);
                }
                if (getOptionAsBoolean(NatFilterOption.debug_document_show)) {

                    int maxsize = options.getValueAsInteger(NatFilterOption.debug_document_show_max_char);
                    int size = resu.getString().length();
                    int realsize = maxsize > size ? size : maxsize;
                    MessageContents mc = MessageContents.Tr("first {0,number,integer} chars of document : {1}")
                            .setValues(realsize, resu.getString().subSequence(0, realsize));
                    g.afficheMessage(MessageKind.INFO, mc, LogLevel.NONE);
                }
            }
        } catch (Exception e) {
            throw new NatFilterException(NatFilterErrorCode.FILTER_RUNTIME_ERROR, getPrettyName(), e);
        }

        if (resu == null) {
            g.afficheMessage(MessageKind.ERROR, MessageContents.Tr("** we should not be here"), LogLevel.NONE);
            throw new NatFilterException(NatFilterErrorCode.FILTER_RUNTIME_ERROR, getPrettyName());
        }
        if (!getOptionAsBoolean(NatFilterOption.debug_do_not_cleanup_temp_files)) {
            // cleanup temp dirs
            try {
                if (Files.exists(getTmpDir())) {
                    Files.walk(getTmpDir())
                            .sorted(Comparator.reverseOrder())
                            .map(Path::toFile)
                            .forEach(File::delete);
                }
            } catch (Exception e) {

                e.printStackTrace();
//                throw new NatFilterException(NatFilterErrorCode.FILTER_RUNTIME_ERROR, getPrettyName());
            }
        }
        return resu;
    }

    /**
     * time measurement
     */
    private Chronometer chronometer = new Chronometer();

    public long getExecutionDuration() {
        return chronometer.time();
    }

    /**
     * working directories
     */

    // nattmpspace://parentTmpDir/instanceTmpDir/runTmpDir

    private Path subTmpDir = null;
    private Path parentTmpDir = null;

    public void setParentTmpDir(Path parentDir) {

        parentTmpDir = parentDir;

    }


    public void setParentTmpDir() throws IOException {
        parentTmpDir = Paths.get("filters");
    }

    private Path getParentTmpDir() throws IOException {
        if (parentTmpDir == null) {
            setParentTmpDir();
        }
        return parentTmpDir;
    }


    public void newTmpDir() throws IOException {
        subTmpDir = NatTempFiles.newDir(getParentTmpDir(), this.getClass().getSimpleName().toString());
    }


    public Path getTmpDir() throws IOException {
        if (parentTmpDir == null) {
            setParentTmpDir();
        }
        if (subTmpDir == null) {
            newTmpDir();
        }
        return subTmpDir;
    }
	/*
	public static void main(String[] args) throws Exception {
		NatFilter fi = new Transcriptor(Nat.getTheOptions(),new GestionnaireErreur(LogLevel.LOG_AUCUN));
		System.err.println(fi);
		Path tmpDir = fi.getTmpDir();
		System.err.println(tmpDir);
		fi.setParentTmpDir(tmpDir);
		tmpDir = fi.getTmpDir();
		System.err.println(tmpDir);
		fi.setParentTmpDir(tmpDir);
		tmpDir = fi.getTmpDir();
		System.err.println(tmpDir);
	}
	 */
}

