/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.core.filter.convertisseur;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.OutputStreamWriter;
import java.net.URI;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.natbraille.core.NatStaticResolver;
import org.natbraille.core.document.NatDocument;
import org.natbraille.core.document.NatDocumentXMLReader;
import org.natbraille.core.filter.NatFilterErrorCode;
import org.natbraille.core.filter.NatFilterException;
import org.natbraille.core.filter.NatTransformerFilter;
import org.natbraille.core.NatDynamicResolver;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.gestionnaires.GestionnaireErreur;
import org.natbraille.core.gestionnaires.LogLevel;
import org.natbraille.core.gestionnaires.MessageContents;
import org.natbraille.core.gestionnaires.MessageKind;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;

/**
 * XML to natbraille internal format conversion
 * trigo not speci sinus vos tan
 * @author vivien
 *
 */
public class ConvertisseurXHTML extends NatTransformerFilter {

    @Override
    protected String getPrettyName() {
        return "XML vers FORMAT INTERNE";
    }

    /**
     * adress of the XHTML to Internal format xsl stylesheet
     */
    private static final String XHTML_FILTER = "nat://system/xsl/xhtml2interne.xsl";
    /**
     * adress of the XHTML to Internal format xsl stylesheet
     */
    private static final String DTBOOK_FILTER = "nat://system/xsl/dtbook2interne.xsl";
    /**
     * adress of the XHTML to Internal format xsl stylesheet preserving XML tags
     */
    private static final String XHTML_FILTER_PRESERVE = "nat://system/xsl/xhtml2internePreserve.xsl";

    public ConvertisseurXHTML(NatDynamicResolver ndr) {
        super(ndr);
    }

    @Override
    protected NatDocument process(NatDocument indoc) throws NatFilterException {
        NatDocument retour;
        retour = xml2FormatInterne(indoc);
        return retour;
    }

    /**
     * Méthode réalisant la conversion via appel à la feuille xsl
     * <code>xsl/xhtml2interne.xsl</code>
     *
     * @return true si la conversion s'est bien passé, false sinon
     * @throws Exception
     */
    private NatDocument xml2FormatInterne(NatDocument indoc) throws NatFilterException {
        GestionnaireErreur gestErreur = getGestionnaireErreur();

        NatDocument outdoc = null;
        try {

            // creation du document d'entrée
            //Document doc = indoc.getDocument(getUriResolver().getXMLReader());
            Document doc = indoc.getDocument(NatDocumentXMLReader.getSimpleXMLReader());
            DOMSource in = new DOMSource(doc);
            in.setSystemId(doc.getDocumentURI());

			//doc.setStrictErrorChecking(false);
            //determining wich xml is used
            DocumentType doctype = doc.getDoctype();
			//System.out.println(doctype.getPublicId());

			// TODO : two diffrent filters
            String filtre;
            if (doctype != null && ( doctype.getPublicId() != null) && (doctype.getPublicId().contains("dtbook") || doctype.getSystemId().contains("dtbook"))) {
                filtre = DTBOOK_FILTER;
            } else if (getOptionAsBoolean(NatFilterOption.preserveXhtmlTags))//ConfigNat.getCurrentConfig().getPreserveTag())
            {
                filtre = XHTML_FILTER_PRESERVE;
            } else {
                filtre = XHTML_FILTER;
            }
            
            gestErreur.afficheMessage(MessageKind.INFO, MessageContents.Tr("filtre ''{0}''").setValue(filtre), LogLevel.DEBUG);
            gestErreur.afficheMessage(MessageKind.INFO, MessageContents.Tr("doctype''{0}''").setValue(doctype), LogLevel.DEBUG);

            Transformer tr = makeTransformer(new URI(filtre));

            tr.setParameter("dtd", NatStaticResolver.WINDOB_DTD_PUBLIC_ID);
            tr.setParameter("processImage", getOptionAsBoolean(NatFilterOption.MODE_IMAGES));

			// not used
//			transform.	setParameter("chemistryStyle", ConfigNat.getCurrentConfig().getStyleChimie());
            tr.setParameter("indexTableName", getOption(NatFilterOption.LAYOUT_PAGE_TOC_NAME));
            tr.setParameter("insertIndexBraille", getOptionAsBoolean(NatFilterOption.LAYOUT_PAGE_TOC));
            tr.setParameter("stylistFile", getOption(NatFilterOption.Stylist));

//			Properties a = tr.getOutputProperties();
            //	a.save(new FileOutputStream(new File("oooooo")),"i");
            tr.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, NatStaticResolver.WINDOB_DTD_SYSTEM_ID);

            // creation buffer de sortie
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            BufferedWriter fcible = new BufferedWriter(new OutputStreamWriter(baos, "UTF8"));
            StreamResult out = new StreamResult(fcible);

            gestErreur.afficheMessage(MessageKind.INFO, MessageContents.Tr("applying stylesheet..."), LogLevel.VERBOSE);

            tr.transform(in, out);

            //
            outdoc = new NatDocument();
            outdoc.setByteBuffer(baos.toByteArray());

        } catch (NatFilterException nfe) {
            throw nfe;
        } catch (Exception e) {
            throw new NatFilterException(NatFilterErrorCode.FILTER_RUNTIME_ERROR, e.getLocalizedMessage(), e);
        }

        /*
         } catch (NatFilterException e){
		 
         }
         catch (Exception e)  
         {
         throw e;
         }*/
        return outdoc;
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub

    }

}
