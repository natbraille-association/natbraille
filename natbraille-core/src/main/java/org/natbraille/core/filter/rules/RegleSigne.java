/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


package org.natbraille.core.filter.rules;


/**
 * Classe premettant de représenter une règle de type "signe";
 * Les règles de signe font correspondre un mot à un seul signe braille.
 * Les signes s'emploient seuls mais peuvent accepter la lettre "s" s'ils sont au pluriel
 * @author bruno
 *
 */
public class RegleSigne extends RegleMot
{
	/** vrai si le mot peut être mis au pluriel */
	private boolean pluriel=false;
	/**
	 * Constructeur
	 * @param n le signe en noir
	 * @param b la transcription en braille
	 * @param p pluriel possible
	 * @param a true si règle active
	 * @param ped true si règle pédagogique
	 */
	public RegleSigne(String n, String b, boolean p, boolean a, boolean ped)
	{
		super("Signe", "II",n,b,a,ped);
		pluriel = p;
	}
	
	/**
	 * Redéfinition de {@link outils.regles.Regle#toString()}
	 * @see outils.regles.Regle#toString()
	 */
	@Override
	public String toString()
	{
		String pl = "";
		if (pluriel){pl="; pluriel possible";}
		return description + " ("+reference+"): "+ noir + " est transcrit par " + braille+pl; 
	}
	/**
	 * Renvoie true si le signe est invariant (c'est à dire !{@link #pluriel})
	 * @return true si le signe est invariant
	 */
	public boolean isInvariant() {return !pluriel;}
	
	/**
	 * Renvoie vrai si r est une RegleSigne et que les attributs noir sont égaux
	 */
	@Override
	public boolean equals(Object r)
	{
		return (r instanceof RegleSigne) && ((RegleSigne)r).noir.equals(noir);
	}

	/**
	 * @see outils.regles.Regle#getXML()
	 */
	@Override
	public String getXML()
	{
		String p = pluriel ? " pluriel=\"true\"" : "";
		String a = actif ? " actif=\"true\"" : "actif=\"false\"";
		String ped = peda ? " peda=\"true\"" : "";
		return "\t<signe" + p + " "+a+ped+">\n" +
			"\t\t<noir>"+noir+"</noir>\n" +
			"\t\t<braille>" + braille + "</braille>\n" +
			"\t</signe>\n";
	}
}
