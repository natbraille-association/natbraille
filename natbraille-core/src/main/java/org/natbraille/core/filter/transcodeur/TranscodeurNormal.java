/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.natbraille.core.filter.transcodeur;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.OutputStreamWriter;
import java.net.URI;

import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.natbraille.core.document.NatDocument;
import org.natbraille.core.document.NatDocumentXMLReader;
import org.natbraille.core.filter.NatFilterErrorCode;
import org.natbraille.core.filter.NatFilterException;
import org.natbraille.core.filter.NatTransformerFilter;
import org.natbraille.core.NatDynamicResolver;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.gestionnaires.GestionnaireErreur;
import org.natbraille.core.gestionnaires.LogLevel;

import org.w3c.dom.Document;
import org.xml.sax.XMLReader;


//import java.io.FileInputStream;

/**
 * <p>
 * La classe <code>TranscodeurNormal</code> réalise la transcription d'un
 * fichier au format XML interne en Braille
 * </p>
 * 
 * @author Bruno Mascret
 */

public class TranscodeurNormal extends NatTransformerFilter {


	public TranscodeurNormal(NatDynamicResolver ndr) {
		super(ndr);
	}
	
	@Override
	protected String getPrettyName() {
		return "TRANSCODAGE : FORMAT INTERNE vers BRAILLE/NOIR";
	}

	
	@Override
	protected void initialize() throws Exception {
		setTransformer(new URI("nat://temp/xsl/xsl.xsl"));	
	}
	
	@Override
	public NatDocument process(NatDocument indoc) throws Exception {
	/*	try {
			setTransformer(makeTransformer());
		} catch (Exception e){
			
		}
	*/	NatDocument outdoc = null;
//		GestionnaireErreur gestErreur = getGestionnaireErreur();

		// Exception exception = null;
		//gestErreur.afficheMessage("Fichiers :\n Fichier d'entree:" + indoc.getUri(), LogLevel.LOG_VERBEUX);

		try {
			XMLReader anXmlReader = NatDocumentXMLReader.getSimpleXMLReader();
			Document doc = NatDocumentXMLReader.toSimpleDocument(indoc.getInputstream(),anXmlReader);

			// get nat doc as Document
//			Document doc = indoc.getDocument(getUriResolver().getXMLReader());
			doc.setStrictErrorChecking(false);

			// get Document as DOMSource)
			DOMSource in = new DOMSource(doc);
                        in.setSystemId(doc.getDocumentURI());

			// Création du buffer de sortie
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			BufferedWriter fcible = new BufferedWriter(new OutputStreamWriter(baos, "UTF8"));
			StreamResult out = new StreamResult(fcible);

			// transformation
//			gestErreur.afficheMessage("Transformation du document ...", LogLevel.VERBOSE);

			getTransformer().transform(in, out);


			outdoc = new NatDocument();
			outdoc.setByteBuffer(baos.toByteArray());

			/*
			 * Transcription des images
			 */
			if (getOptionAsBoolean(NatFilterOption.MODE_IMAGES)) {
				// pof pof pof
			}
		} catch (Exception e) {
			throw new NatFilterException(NatFilterErrorCode.FILTER_RUNTIME_ERROR, "sax failed parsing",e);

		}
		return outdoc;
	}

	/**
 * 
 */

}
