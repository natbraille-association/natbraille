/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.natbraille.core.filter.transcriptor;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.transform.TransformerException;
import org.natbraille.brailletable.system.SystemBrailleTables;
import org.natbraille.core.document.NatDocument;
import org.natbraille.core.document.NatDocumentException;
import org.natbraille.core.filter.NatFilterChain;
import org.natbraille.core.filter.NatFilterConfigurator;
import org.natbraille.core.filter.NatFilterException;
import org.natbraille.core.NatDynamicResolver;
import org.natbraille.core.filter.convertisseur.ConvertisseurGiacMathml;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.core.gestionnaires.Afficheur;
import org.natbraille.core.gestionnaires.AfficheurAnsiConsole;
import org.natbraille.core.gestionnaires.GestionnaireErreur;
import org.natbraille.core.gestionnaires.LogLevel;

/**
 * Converts Giac MathML to Braille. Encapsulates a GiacTranscriptor
 * (NatFilterChain), and adds GiacTranscriptor rebuild on option changes.
 *
 * @author vivien
 */
public class GiacConverter {

    /**
     * Transcriptor with initial mathml transformation to convert giac mathml to
     * natbraille mathml using ConvertisseurGiacMathml.
     */
    class GiacTranscriptor extends Transcriptor {

        public GiacTranscriptor(NatFilterConfigurator natFilterConfigurator) {
            super(natFilterConfigurator);
        }

        @Override
        protected String getPrettyName() {
            return "Giac Mathml -> Braille";
        }

        @Override
        public NatFilterChain getImportFilterChain(NatDocument natDoc) throws NatFilterException, NatDocumentException {
            /*
             replace the auto import filter chain of Transcriptor by
             the ConvertisseurGiacMathml filter.
             */
            NatFilterChain filterChain = getNatFilterConfigurator().newFilterChain();
            filterChain.addNewFilter(ConvertisseurGiacMathml.class);

            return filterChain;
        }

    }

    private final GestionnaireErreur gestionnaireErreurs;
    private final NatFilterOptions natFilterOptions;
    private Transcriptor transcriptor;

    /**
     * Get default giac transcription filter options.
     *
     * @return
     * @throws NatFilterException
     */
    private NatFilterOptions defaultNatFilterOptions() throws NatFilterException {
        NatFilterOptions nfo = new NatFilterOptions();
        nfo.setOption(NatFilterOption.CONFIG_NAME, "*special*");
        nfo.setOption(NatFilterOption.FORMAT_OUTPUT_BRAILLE_TABLE, SystemBrailleTables.TbFr2007);
        nfo.setOption(NatFilterOption.LAYOUT, "false");
        return nfo;
    }

    /**
     * Build and set the transcriptor with current NatFilterOptions and
     * GestionnaireErreurs.
     */
    private void buildTranscriptor() throws NatFilterException {
        NatDynamicResolver ndr = new NatDynamicResolver(natFilterOptions, gestionnaireErreurs);
        NatFilterConfigurator nff = new NatFilterConfigurator()
                .set(natFilterOptions)
                .set(gestionnaireErreurs)
                .set(ndr);

        transcriptor = new GiacTranscriptor(nff);
    }

    /**
     * Override NatFilterOptions with new values. Rebuilds a new Transcriptor.
     *
     * @param options
     * @throws IOException
     * @throws NatFilterException
     */
    public void setOptions(NatFilterOptions options) throws IOException, NatFilterException {
        natFilterOptions.setOptions(options);
        buildTranscriptor();
    }

    /**
     * Override a NatFilterOption with new value. Rebuilds a new Transcriptor.
     *
     * @param option
     * @param value
     * @throws IOException
     * @throws NatFilterException
     */
    public void setOption(NatFilterOption option, String value) throws IOException, NatFilterException {
        natFilterOptions.setOption(option, value);
        buildTranscriptor();
    }

    /**
     * Get a {@link NatFilterOption} value.
     * @param option
     * @return
     * @throws NatFilterException 
     */
    public String getOption(NatFilterOption option) throws NatFilterException {
        return natFilterOptions.getValue(option);
    }

    /**
     * Constructor for the Natbraille Giac Converter. Converts mathml produced
     * by giac to Braille.
     *
     * @param gestionnaireErreur
     * @throws NatFilterException
     */
    public GiacConverter(GestionnaireErreur gestionnaireErreur) throws NatFilterException {
        // set gestionnaire erreurs
        this.gestionnaireErreurs = gestionnaireErreur;
        // initialize options first
        this.natFilterOptions = defaultNatFilterOptions();
        // build transcriptor
        buildTranscriptor();
    }

    /**
     * Convert a Giac MathMl expression to a Braille NatDocument.
     *
     * @param giacMathMl the MathMl String
     * @return
     * @throws NatDocumentException
     * @throws NatFilterException
     */
    public NatDocument convert(String giacMathMl) throws NatDocumentException, NatFilterException {
        NatDocument natDocument = new NatDocument();
        natDocument.setString(giacMathMl);
        NatDocument outDocument = transcriptor.run(natDocument);
        return outDocument;
    }

    /**
     * Quick test on  mathml expressions.
     *
     * @param args
     */
    public static void main(String[] args) {

        try {
            // create error handler and display
            GestionnaireErreur ge = new GestionnaireErreur();
            Afficheur aff = new AfficheurAnsiConsole(LogLevel.NONE);
            //Afficheur aff = new AfficheurAnsiConsole(LogLevel.NORMAL);
            ge.addAfficheur(aff);

            // create giac converter
            GiacConverter giacConverter = new GiacConverter(ge);

            String strings[] = {
                //  "<math></math>",
                //"<math><mi>a</mi><mo>+</mo><mi>b</mi></math><math><mi>a</mi><mo>+</mo><mi>b</mi></math>",
//                "<math><msqrt><mrow><msup><mi>b</mi><mn>2</mn></msup><mo>-</mo><mrow><mn>4</mn><mo>&InvisibleTimes;</mo><mi>a</mi><mo>&InvisibleTimes;</mo><mi>c</mi></mrow></mrow></msqrt></math>",
  //              "<math><msqrt><mrow><msup><mi>b</mi><mn>2</mn></msup><mo>-</mo><mrow><mn>4</mn><mo>&InvisibleTimes;</mo><mi>a</mi><mo>&InvisibleTimes;</mo><mi>c</mi></mrow></mrow></msqrt></math>",
            
            "<math><msqrt><mi>a</mi><mo>+</mo><mi>b</mi></msqrt></math>",
            "<math><msqrt><mrow><mi>a</mi><mo>+</mo><mi>b</mi></mrow></msqrt></math>"
            };

            // Convert
            for (String s : strings) {

                NatDocument doc = giacConverter.convert(s);
                /*if (doc.getString().length()>1){
                 System.err.println(doc.getString().substring(0, s.length() - 1));
                 } else {
                 System.err.println("empty");
                 }
                 */
                System.err.println("source: "+s);
                System.err.println("=>" + doc.getString());
            }
        } catch (NatFilterException | NatDocumentException | IOException | TransformerException ex) {
            Logger.getLogger(GiacConverter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public GestionnaireErreur getGestionnaireErreur() {
        return gestionnaireErreurs;
    }
}
