/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.core.filter.options;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import org.natbraille.core.filter.NatFilter;
import org.natbraille.core.filter.NatFilterErrorCode;
import org.natbraille.core.filter.NatFilterException;

/**
 * options used by {@link NatFilter}s
 * @author vivien
 *
 */
public class NatFilterOptions {

	private final HashMap<NatFilterOption, String> options;


	/**
	 * @throws NatFilterException 
	 * 
	 */
	public NatFilterOptions() throws NatFilterException{
        this.options = new HashMap<NatFilterOption, String>();
	//	loadDefaults();
	}
	/**
	 * 
	 * @param options
	 * @throws NatFilterException
	 */
	public NatFilterOptions(HashMap<NatFilterOption, String> options) throws NatFilterException {
        this.options = new HashMap<NatFilterOption, String>();
		this.setOptions(options);
	}
	/**
	 * 
	 * @param options
	 * @throws NatFilterException
	 * @throws URISyntaxException 
	 */
	public NatFilterOptions(Properties options) throws NatFilterException {
        this.options = new HashMap<>();
	//	loadDefaults();
		this.setOptions(options);
	}
	/**
	 * Sets the values of a {@NatOption} 
	 * 
	 * @param key
	 * @param value
	 */
	public void setOption(NatFilterOption key, String value){
		options.put(key, value);
	}

	/**
	 * Sets the values of multiple {@NatOption}s 	  
	 * @param options
	 *            table of v
         * @throws org.natbraille.core.filter.NatFilterException
	 */
	public void setOptions(HashMap<NatFilterOption, String> options)
			throws NatFilterException {
		Iterator<Map.Entry<NatFilterOption, String>> it = options.entrySet()
				.iterator();
		while (it.hasNext()) {
			Map.Entry<NatFilterOption, String> entry = it.next();
			setOption(entry.getKey(), entry.getValue());
		}

	}
	/**
	 * Override the values of multiple {@NatOption}s if available.
	 * 
	 * @param p
	 *            table of v
	 * @throws NatFilterException
	 *             if one of the options key is not recognised
	 *             as a nat filter option
	 *             
	 */
	public void setOptions(Properties p) throws NatFilterException{
		Iterator<Entry<Object, Object>> it = p.entrySet().iterator();
		while (it.hasNext()) {
			Entry<Object, Object> entry = it.next();
			String key = (String) entry.getKey();
			try {
				NatFilterOption enumName = getEnumFromString(NatFilterOption.class, key );
				setOption(enumName, (String)entry.getValue());
			} catch (Exception e){
				throw new NatFilterException(NatFilterErrorCode.UNKNOWN_FILTER_OPTION, key);
			}
		}

	}

	/**
	 * Override filter options values from an input stream containing a Properties.
	 * @param is inputStream  
	 * @throws NatFilterException
	 * @throws IOException 
	 */
	public void setOptions(InputStream is) throws IOException, NatFilterException {
		Properties p = new Properties();
		p.load(is);
		setOptions(p);
	}

        /**
	 * Override filter options values from an input stream containing a Properties.
     * @param newnfo
	 * @throws NatFilterException
	 * @throws IOException 
	 */
	public void setOptions(NatFilterOptions newnfo) throws IOException, NatFilterException {            
		setOptions(newnfo.getAsProperties());
	}

        /**
         * 
         */

	/**
	 * Get the values of a {@NatOption} if available
	 * 
	 * @param key
	 * @return
	 * @throws NatFilterException
	 *             if one of the options is not available or not set
	 */
	public String getValue(NatFilterOption key) throws NatFilterException {
		String resu;
		if (options.containsKey(key)) {
			resu = options.get(key);
		} else {
			resu = key.getDetail().defaultValue();
			if (resu == null){
				throw new NatFilterException(NatFilterErrorCode.FILTER_OPTION_UNSET, key.toString());
			}
		}
		return resu;
	}

	public int getValueAsInteger(NatFilterOption key) throws NatFilterException {
		return Integer.parseInt(getValue(key));
	}
	public Boolean getValueAsBoolean(NatFilterOption key) throws NatFilterException {
		return Boolean.valueOf(getValue(key));
	}	
	
	/**
	 * get options as a .properties file, containing only non default values
	 * @return
	 */
	public Properties getAsProperties(){
		Properties p = new Properties();
		Iterator<Map.Entry<NatFilterOption, String>> it = options.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<NatFilterOption, String> entry = it.next();
			
			NatFilterOption o = entry.getKey();
			String s = entry.getValue();
			
			if (!(o.getDetail().defaultValue().equals(s))){
				p.setProperty(entry.getKey().name(), entry.getValue());
			}
		}
		return p;
	}
	
	/**
	 * A common method for all enums since they can't have another base class
	 * @param <T> Enum type
	 * @param c enum type. All enums must be all caps.
	 * @param string case insensitive
	 * @return corresponding enum, or null
	 */
    public static <T extends Enum<T>> T getEnumFromString(Class<T> c, String string) {
		if( c != null && string != null ) {
			return Enum.valueOf(c, string.trim());
		}
		return null;
	}
}
