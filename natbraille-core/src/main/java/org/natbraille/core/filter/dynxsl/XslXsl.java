package org.natbraille.core.filter.dynxsl;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.natbraille.core.Nat;
import org.natbraille.core.NatStaticResolver;
import org.natbraille.core.filter.NatFilterException;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.core.gestionnaires.GestionnaireErreur;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class XslXsl extends AbstractNatDynamicXslDocument {

	private NatFilterOptions nfo;
	
	private NatFilterOptions getOptions() {
		return nfo;
	}

	public XslXsl(GestionnaireErreur ge, NatFilterOptions nfo) {
		super(ge);
		this.nfo = nfo;
	}
	
	@Override
	protected Document makeStyleSheet() throws Exception {
		return createMainXsl(getOptions(),getGestionnaireErreur());
	}


	public static Document createMainXsl(NatFilterOptions options, GestionnaireErreur ge) throws NatFilterException, ParserConfigurationException {

		Boolean sens = options.getValueAsBoolean(NatFilterOption.MODE_DETRANS);

		DocumentBuilderFactory fabrique = DocumentBuilderFactory.newInstance();
		DocumentBuilder constructeur = fabrique.newDocumentBuilder();

		Document docXSL = constructeur.newDocument();
		// Propriétés de docParam
		docXSL.setXmlVersion("1.1");
		docXSL.setXmlStandalone(true);

		// racine
		Element racine = docXSL.createElement("xsl:stylesheet");
		racine.setAttribute("version", "3.0");
		racine.setAttribute("xmlns:xsl", "http://www.w3.org/1999/XSL/Transform");
		racine.setAttribute("xmlns:xs", "http://www.w3.org/2001/XMLSchema");
		racine.setAttribute("xmlns:saxon", "http://icl.com/saxon");
		racine.setAttribute("xmlns:m", "http://www.w3.org/1998/Math/MathML");
		racine.setAttribute("xmlns:fn", "http://www.w3.org/2005/xpath-functions");
		racine.setAttribute("xmlns:lit", "espacelit");
		racine.setAttribute("xmlns:nat", "http://natbraille/free/fr/xsl");
		racine.setAttribute("xmlns:doc", "espaceDoc");
		racine.setAttribute( "expand-text","true");
		racine.appendChild(docXSL.createComment("Auto-generated file; see Transcodeur.java"));
		racine.appendChild(docXSL.createComment(Nat.getLicence("", "")));

		// output
		Element output = docXSL.createElement("xsl:output");
		if (!sens) {
			output.setAttribute("method", "xml");
		} else {
			output.setAttribute("doctype-system", NatStaticResolver.SYSTEM_BASE_URL + "xsl/mmlents/xhtml-math11-f.dtd");
			output.setAttribute("doctype-public", "-//W3C//DTD XHTML 1.1 plus MathML 2.0//EN");
		}
		//output.setAttribute("encoding", options.getValue(NatFilterOption.OUTPUT_ENCODING));
		output.setAttribute("encoding", "UTF-8");
		
		output.setAttribute("indent", "yes");
		/* Bruno TODO don't know if there is any effect
		output.setAttribute("build-tree", "yes");
		output.setAttribute("item-separator", "\t");
		*/
		Element b2b = docXSL.createElement("xsl:variable");

		////////////
                b2b.setAttribute("name", "blackToBraille");
		b2b.setAttribute("as", "xs:boolean");
                ////////////////
		if (sens) {
                        // System.out.println("===============> TAN");
			Element tan = docXSL.createElement("xsl:include");
			tan.setAttribute("href", NatStaticResolver.SYSTEM_BASE_URL + "xsl/tan.xsl");
			racine.appendChild(tan);
                        b2b.setAttribute("select", "false()");
			racine.appendChild(b2b);
		} else {
                        // System.out.println("===============> PAS TAN");
			String fichHyphens = NatStaticResolver.TMP_BASE_URL + "xsl/hyphens.xsl";
			Element hyph = docXSL.createElement("xsl:import");
			hyph.setAttribute("href", fichHyphens);
			racine.appendChild(hyph);

			b2b.setAttribute("select", "true()");
			racine.appendChild(b2b);
                        
			Element base = docXSL.createElement("xsl:include");
			base.setAttribute("href", NatStaticResolver.SYSTEM_BASE_URL + "xsl/base.xsl");
			racine.appendChild(base);

			// if (options.getValue(NatOption.TraiterLiteraire))
			// {
			Element lit = docXSL.createElement("xsl:include");
			if (!options.getValueAsBoolean(NatFilterOption.MODE_LIT)) {
				lit.setAttribute("href", NatStaticResolver.SYSTEM_BASE_URL + "xsl/no-lit.xsl");
			} else if (options.getValueAsBoolean(NatFilterOption.MODE_G2)) {
				lit.setAttribute("href", options.getValue(NatFilterOption.XSL_g2));
			} else {
				lit.setAttribute("href", options.getValue(NatFilterOption.XSL_g1));
			}
			racine.appendChild(lit);
			// }
			if (options.getValueAsBoolean(NatFilterOption.MODE_MATH)) {
				Element math = docXSL.createElement("xsl:include");
				math.setAttribute("href", options.getValue(NatFilterOption.XSL_maths));
				racine.appendChild(math);
			}

			if (options.getValueAsBoolean(NatFilterOption.MODE_MUSIC)) {
				Element mus = docXSL.createElement("xsl:include");
				mus.setAttribute("href", options.getValue(NatFilterOption.XSL_musique));
				racine.appendChild(mus);
			}

			if (options.getValueAsBoolean(NatFilterOption.MODE_CHEMISTRY)) {
				Element math = docXSL.createElement("xsl:include");
				math.setAttribute("href", options.getValue(NatFilterOption.XSL_chimie));
				racine.appendChild(math);
			}
		}
		
		
		Element regles = docXSL.createElement("xsl:include");
		//regles.setAttribute("href", new File(ConfigNat.getCurrentConfig().getXSL_g2_Rules()).getName());
		regles.setAttribute("href", NatStaticResolver.TMP_BASE_URL + "xsl/fr-g2-rules.xsl");
		racine.appendChild(regles);

		racine.appendChild(output);
		Element strip = docXSL.createElement("xsl:strip-space");
		strip.setAttribute("elements", "doc:doc lit espace phrase mot ponctuation m:* m:math m:semantics m:mrow m:msqrt");
		racine.appendChild(strip);

		Element paramAll = docXSL.createElement("xsl:include");
		paramAll.setAttribute("href", NatStaticResolver.TMP_BASE_URL + "xsl/paramsAll.xsl");
		racine.appendChild(paramAll);

		Element paramTrans = docXSL.createElement("xsl:include");
		paramTrans.setAttribute("href", NatStaticResolver.TMP_BASE_URL + "xsl/paramsTrans.xsl");
		racine.appendChild(paramTrans);

		docXSL.appendChild(racine);

		return docXSL;
	}

	

}
