/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


package org.natbraille.core.gestionnaires;

import org.natbraille.core.Nat;
import org.xnap.commons.i18n.I18n;
import org.xnap.commons.i18n.I18nFactory;

/**
 * Interface décrivant le comportement d'un afficheur d'erreur
 * @author bruno
 * @see GestionnaireErreur
 */
public abstract class Afficheur {
	
	/** logs verbosity */
	private int logLevel;
	
	/** message translation **/
	private I18n i18n;
	
	/**
	 * get the {@link I18n} if set, or using the default locale
	 * @return
	 */
	protected I18n getI18n() {
		if (i18n == null){
		    try {
			i18n = I18nFactory.getI18n(Nat.class);
		    } catch (Exception e){
			//System.err.println("CANNOT FIND i18n");
			//e.printStackTrace();
		    }
		}
		return i18n;
	}

	public Afficheur setI18n(I18n i18n) {
		this.i18n = i18n;
		return this;
	}
	
	/** Message with messageKind in whiteList are always delivered */
	private MessageKind whiteList[];
	
	/** Message with messageKind in blackList are never delivered */
	private MessageKind blackList[];
	
	public abstract void afficheMessage(LogMessage logMessage);

	/**
	 * @return the logLevel
	 */
	public int getLogLevel() {
		return logLevel;
	}

	/**
	 * @param logLevel the logLevel to set
	 */
	public void setLogLevel(int logLevel) {
		this.logLevel = logLevel;
	}

	/**
	 * @return the whiteList
	 */
	public MessageKind[] getWhiteList() {
		return whiteList;
	}

	/**
	 * @param whiteList the whiteList to set
	 */
	public void setWhiteList(MessageKind ... whiteList) {
		this.whiteList = whiteList;
	}

	/**
	 * @return the blackList
	 */
	public MessageKind[] getBlackList() {
		return blackList;
	}

	/**
	 * @param blackList the blackList to set
	 */
	public void setBlackList(MessageKind ...  blackList) {
		this.blackList = blackList;
	} 
	
	/**
	 * 
	 * @param head
	 * @param tail
	 * @return true if head is in tail
	 */
	public Boolean isin(MessageKind head, MessageKind[] tail){
		if ((head == null)||(tail == null)){
			return false;
		} 
		for (MessageKind n : tail){
			if (n.equals(head)){
				return true;
			}
		}
		return false;
	}
	/**
	 * is this message kind blacklisted ?
	 * @param m
	 * @return
	 */
	public Boolean blackListed(MessageKind m){
		return isin(m,getBlackList());
	}
	/**
	 * is this message kind whitelisted?
	 * @param m
	 * @return
	 */
	public Boolean whiteListed(MessageKind m){
		return isin(m,getWhiteList());
	}
	
	protected String getTrContents(LogMessage logMessage){
		return logMessage.getTrContents(getI18n());
	}
}
