/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */



package org.natbraille.core.gestionnaires;

import jlibs.core.lang.Ansi;

/**
 * Classe d'affichage des messages des Gestionnaires d'erreur en mode console
 * @author bruno
 *
 */
public class AfficheurAnsiConsole extends Afficheur
{
	public AfficheurAnsiConsole(int logLevel) {
		setLogLevel(logLevel);
	}

	@Override
	public void afficheMessage(LogMessage logMessage) {
		

		Ansi ansiColored;

		switch (logMessage.getKind()){

		case WARNING:
			ansiColored = new Ansi(Ansi.Attribute.BRIGHT, Ansi.Color.MAGENTA,Ansi.Color.BLACK);
			break;
		case ERROR:
			ansiColored = new Ansi(Ansi.Attribute.BRIGHT, Ansi.Color.RED,Ansi.Color.BLACK);
			break;
		case SUCCESS:
			ansiColored = new Ansi(Ansi.Attribute.BRIGHT, Ansi.Color.GREEN,Ansi.Color.BLACK);
			break;
		case FINAL_SUCCESS:
			ansiColored = new Ansi(Ansi.Attribute.BRIGHT, Ansi.Color.GREEN,Ansi.Color.BLACK);
			break;
		case INFO:
			ansiColored = new Ansi(Ansi.Attribute.BRIGHT, Ansi.Color.WHITE,Ansi.Color.BLACK);
			break;
		case STEP:
			ansiColored = new Ansi(Ansi.Attribute.BRIGHT, Ansi.Color.WHITE,Ansi.Color.BLACK);
			break;
		default:
                    ansiColored = new Ansi(null,null,null);
			break;

		}
                
		ansiColored.err(logMessage.getKind().toString());
                ansiColored.err("\t");
		Ansi ansiNotColored = new Ansi(Ansi.Attribute.NORMAL, Ansi.Color.WHITE,Ansi.Color.BLACK);
		ansiNotColored.err(getTrContents(logMessage));
                ansiNotColored.err("\n");
		
	}


}
