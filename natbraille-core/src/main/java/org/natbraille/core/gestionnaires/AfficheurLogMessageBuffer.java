/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


package org.natbraille.core.gestionnaires;

import java.util.ArrayList;

public class AfficheurLogMessageBuffer extends Afficheur {

	public AfficheurLogMessageBuffer(int logLevel) {
		setLogLevel(logLevel);
	}

	ArrayList<LogMessage>messages = new ArrayList<>();
	
	@Override
	public void afficheMessage(LogMessage logMessage) {
		messages.add(logMessage);
	}
	public ArrayList<LogMessage> getMessages(){
		return messages;
	}

}
