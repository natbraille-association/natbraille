/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.core.gestionnaires;

import java.util.Date;
import org.xnap.commons.i18n.I18n;

public class LogMessage {

    private final MessageKind kind;
    private final MessageContents contents;
    private final int level;
    private final Date date;

    public Date getDate() {
        return date;
    }

    public MessageKind getKind() {
        return kind;
    }

    public MessageContents getContents() {
        return contents;
    }

    public String getTrContents(I18n i18n) {
        return getContents().getTr(i18n);
    }

    public int getLevel() {
        return level;
    }

    public LogMessage(MessageKind messageKind, MessageContents messageContents, int level) {
        this.kind = messageKind;
        this.contents = messageContents;
        this.level = level;
        this.date = new Date();
    }

    public String toString() {

        return "" + kind.name() + " " + getContents() + " " + level;

    }

}
