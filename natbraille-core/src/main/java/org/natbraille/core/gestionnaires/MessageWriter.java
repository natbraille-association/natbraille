package org.natbraille.core.gestionnaires;

import java.io.IOException;
import java.io.Writer;

public class MessageWriter extends Writer{

	private GestionnaireErreur ge;
	private MessageKind kind;
	private int logLevel;
	
	public MessageWriter(GestionnaireErreur ge, MessageKind kind, int logLevel) {
		this.ge = ge;
		this.kind=kind;
		this.logLevel=logLevel;
	}

	@Override
	public void close() throws IOException {
	}

	@Override
	public void flush() throws IOException {
	}

	@Override
	public void write(char[] cbuf, int off, int len) throws IOException {
		StringBuilder sb = new StringBuilder();
		for (int i=0;i<len;i++){
			sb.append(cbuf[i+off]);
		}
		ge.afficheMessage(kind,sb.toString(),logLevel);
	}

}
