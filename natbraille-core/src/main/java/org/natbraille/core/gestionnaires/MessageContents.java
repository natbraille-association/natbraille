package org.natbraille.core.gestionnaires;

import org.xnap.commons.i18n.I18n;

/**
 * MessageContents encapsultates the org.xnap.commons.i18n.I18n for use in
 * natbraille. MessageContents are created using the static methods
 * makeTr[something], whose parameters copy thoses of the I18n methods.
 * 
 * This makes extraction of translatable string possible by using distinct
 * creation method name for identification of parameters by xgetext, while
 * passing untranslated string to the {@link GestionnaireErreur} then to the
 * {@link Afficheur}, first to know the desired localization of the message.
 * 
 * @author vivien
 * 
 */
public class MessageContents {

	/**
	 * text to translate (="translation key"). contains references to values
	 */
	private String text;
	/**
	 * comment about the context to help translators
	 */
	private String comment;
	/**
	 * values to be inserted in text exemple "Salut {0}" with String[]{"ducon"}
	 * => Salut ducon.
	 */
	private Object values[];
	/**
	 * plural form of the text
	 */
	private String pluralText;
	/**
	 * quantity selector (for selection within the text and its plural form
	 */
	private Integer quantity;

	private boolean isXsltProcessingMessage = false;
	private	void setAsXsltProcessingMessage(){
		this.isXsltProcessingMessage = true;
	}
	public	boolean getIsXsltProcessingMessage(){
		return this.isXsltProcessingMessage;
	}
	public String getRawText(){
		return text;
	}
	private void setText(String text) {
		this.text = text;
	}


	private void setComment(String comment) {
		this.comment = comment;
	}

	private void setPluralText(String pluralText) {
		this.pluralText = pluralText;
	}

	public MessageContents setValues(Object... values) {
		this.values = values;
		return this;
	}
	public MessageContents setValue(Object value) {
		this.values = new Object[]{value};
		return this;
	}

	private void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	private MessageContents() {
	}

	public static MessageContents XsltMessage(String text){
		MessageContents messageContents = new MessageContents();
		messageContents.setText(text);
		messageContents.setAsXsltProcessingMessage();
		return messageContents;
	}
	/**
	 * gets a new MessageContents with given text
	 * 
	 * @param text
	 * @return a new MessageContents
	 */
	public static MessageContents Tr(String text) {
		MessageContents messageContents = new MessageContents();
		messageContents.setText(text);
		return messageContents;
	}

	/**
	 * @param text
	 * @return
	 */

	/**
	 * gets a new MessageContents with given text and a context comment for the
	 * translator
	 * 
	 * @param comment
	 * @param text
	 * @return a new MessageContents
	 */
	public static MessageContents Trc(String comment, String text) {
		MessageContents messageContents = new MessageContents();
		messageContents.setComment(comment);
		messageContents.setText(text);
		return messageContents;
	}

	/**
	 * gets a new MessageContents with given text, its plural form and the
	 * variable for selection amongst those texts
	 * 
	 * @param text
	 * @param pluralText
	 * @param quantity
	 * @return a new MessageContents
	 */
	public static MessageContents Trn(String text, String pluralText, int quantity) {
		MessageContents messageContents = new MessageContents();
		messageContents.setPluralText(pluralText);
		messageContents.setText(text);
		messageContents.setQuantity(quantity);
		return messageContents;

	}

	/**
	 * gets a new MessageContents with given text, its plural form and the
	 * variable for selection amongst those texts, and a context comment for the
	 * translator
	 * 
	 * @param comment
	 * @param text
	 * @param pluralText
	 * @param quantity
	 * @return a new MessageContents
	 */
	public static MessageContents Trnc(String comment, String text, String pluralText, int quantity) {
		MessageContents messageContents = new MessageContents();
		messageContents.setComment(comment);
		messageContents.setText(text);
		messageContents.setPluralText(pluralText);
		messageContents.setQuantity(quantity);
		return messageContents;
	}

	/**
	 * get the translated message content using the given {@link I18n} (containing the locale)
	 * @param i18n
	 * @return the translated string
	 */
	public String getTr(I18n i18n) {
		String tr;
		try {
		    if (comment == null){
			if ((pluralText == null)&&(quantity == null)){
			    tr = i18n.tr(text, values);				
			} else {
			    tr = i18n.trn(text, pluralText, quantity, values);								
			}
		    } else {
			if ((pluralText == null)&&(quantity == null)){
			    tr = i18n.trc(comment,text);				
			} else {
			    tr = i18n.trnc(comment, text, pluralText, quantity,values);								
			}
		    }
		} catch (Exception e){
		    tr = "(missing i18n for message :) "+text;
		}
		return tr;
	}

}
