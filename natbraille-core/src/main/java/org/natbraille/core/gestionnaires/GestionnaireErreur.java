/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.core.gestionnaires;

import java.util.ArrayList;
import javax.xml.transform.SourceLocator;
import net.sf.saxon.trans.XPathException;
import org.natbraille.core.tools.Tools;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Gestionnaire pour les messages d'erreurs générés par NAT
 * <p>
 * Utilise un pattern <code>Ecouteur</code> pour la diffusion des messages.
 * </p>
 *
 * @author bruno
 *
 */
public class GestionnaireErreur extends DefaultHandler {

    /**
     * Liste des afficheurs à notifier
     */
    private ArrayList<Afficheur> afficheurs = new ArrayList<Afficheur>();

    // constructeurs
    /**
     * Constructeur
     *
     * @param code exception à traiter (<code>null</code> si aucune)
     * @param al liste des afficheurs abonnés aux notifications
     */
    public GestionnaireErreur(ArrayList<Afficheur> al) {
        afficheurs = al;
    }

    public GestionnaireErreur() {
    }

    /**
     * add afficheur <code>a</code> to the list {@link #afficheurs}
     *
     * @param a afficheur à ajouter
     * @see Afficheur
     */
    public void addAfficheur(Afficheur a) {
        afficheurs.add(a);
    }

    /**
     * remove afficheur <code>a</code> from the list {@link #afficheurs}
     *
     * @param a afficheur à supprimer
     * @see Afficheur
     */
    public void removeAfficheur(Afficheur a) {
        afficheurs.remove(a);
    }

    /**
     * displays a {@link LogMessage} to all registered {@link AfficheurLog}
     *
     * @param logMessage
     */
    public void afficheMessage(LogMessage logMessage) {
        for (Afficheur a : getAfficheurs()) {

            Boolean blackListed = a.blackListed(logMessage.getKind());
            Boolean whiteListed = a.whiteListed(logMessage.getKind());
            Boolean levelOk = (logMessage.getLevel() <= a.getLogLevel());

            if (!blackListed && (whiteListed || levelOk)) {
                a.afficheMessage(logMessage);
            }
        }
    }

    private ArrayList<Afficheur> getAfficheurs() {
        return afficheurs;
    }

    /**
     * creates and passes a LogMessage with MessageKind.INFO and
     * {@link LogLevel} LogLevel.NORMAL
     *
     * @param contents
     */
    @Deprecated
    public void afficheMessage(String contents) {
        afficheMessage(MessageKind.INFO, contents, LogLevel.NORMAL);
    }

    /**
     * creates and passes a LogMessage with {@link LogLevel} LogLevel.NORMAL
     *
     * @param kind
     * @param contents
     */
    @Deprecated
    public void afficheMessage(MessageKind kind, String contents) {
        afficheMessage(kind, contents, LogLevel.NORMAL);
    }

    /**
     * creates and passes a LogMessage with and {@link LogLevel} LogLevel.NORMAL
     *
     * @param kind
     * @param contents
     */
    @Deprecated
    public void afficheMessage(String contents, int logLevel) {
        afficheMessage(MessageKind.INFO, contents, logLevel);
    }

    /**
     * creates and passes a LogMessage
     *
     * @param kind
     * @param contents
     * @param logLevel {@link LogLevel}
     */
    @Deprecated
    public void afficheMessage(MessageKind kind, String contents, int logLevel) {
        // this was the one used by XSLT Message emitter
        afficheMessage(new LogMessage(kind, MessageContents.Tr(contents), logLevel));
    }
    public void afficheXsltProcessingMessage(MessageKind kind, String contents, int logLevel){
        // this is the one used by XSLT Message emitter
        afficheMessage(new LogMessage(kind, MessageContents.XsltMessage(contents), logLevel));
    }
    /**
     * helper method wrapping the creation of a the {@link LogMessage}
     *
     * @param kind
     * @param contents
     * @param logLevel
     */
    public void afficheMessage(MessageKind kind, MessageContents contents, int logLevel) {
        afficheMessage(new LogMessage(kind, contents, logLevel));
    }

    /**
     * get explanation messages for XPathException
     *
     * @param xpe the exception
     * @return
     */
    public String getMessageForXPathException(XPathException xpe) {
        StringBuilder sb = new StringBuilder();

        sb.append(xpe.getMessage());
        sb.append(" (");
        //sb.append(xpe.getErrorCodeNamespace());
        //sb.append(":");
        sb.append(xpe.getErrorCodeLocalPart());
        sb.append(")");

        //sb.append(" xpac:" + xpe.getXPathContext());
        //sb.append(" erro:" + xpe.getErrorObject());
        SourceLocator l = xpe.getLocator();

        if (l != null) {
            sb.append(" for ");
            sb.append(l.getClass().getSimpleName());

            sb.append(" at ");
            sb.append(Tools.getPublicIdSystemIdString(l.getPublicId(), l.getSystemId()));
            sb.append(" :" + l.getLineNumber());
            sb.append(" :" + l.getColumnNumber());
        }
        return sb.toString();
    }

    /**
     * displays the content of a Throwable as a serie of {@link LogMessage}
     * {@see afficheMessage}
     *
     * @param kind
     * @param e
     */
    public void afficheMessage(MessageKind kind, Throwable e) {
        afficheMessage(kind, e, LogLevel.NONE);
    }

    /**
     * displays the content of a Throwable as a serie of {@link LogMessage}
     *
     * @param kind
     * @param e
     * @param minLogLevel the emitted messages will never have a lower
     * {@link LogLevel} than that.
     */
    public void afficheMessage(MessageKind kind, Throwable e, int minLogLevel) {
        boolean first = true;
        do {

            {
                StringBuilder sb = new StringBuilder();
                int ll;
                if (kind == MessageKind.ERROR) {
                    if (first) {
                        ll = LogLevel.SILENT;
                    } else {
                        sb.append("caused by : ");
                        ll = LogLevel.NORMAL;
                    }
                } else {
                    if (first) {
                        ll = LogLevel.VERBOSE;
                    } else {
                        sb.append("caused by : ");
                        ll = LogLevel.DEBUG;
                    }
                }

                if (ll < minLogLevel) {
                    ll = minLogLevel;
                }

                if (e.getClass().equals(XPathException.class)) {
                    sb.append(getMessageForXPathException((XPathException) e));
                } else {
                    sb.append(e.getMessage());
                }
                afficheMessage(kind, sb.toString(), ll);
            }

            //
            StackTraceElement[] stackTrace = e.getStackTrace();
            if ((stackTrace != null) && (stackTrace.length > 0)) {

                // for (StackTraceElement ste : stackTrace){
                StackTraceElement stackRoot = stackTrace[0];
                StackTraceElement ste = stackRoot;

                StringBuilder sb = new StringBuilder();
                sb.append("\t");
                sb.append(ste.getClassName());
                sb.append(".");
                sb.append(ste.getMethodName());
                sb.append(" (");
                sb.append(ste.getFileName());
                sb.append(":" + ste.getLineNumber());
                sb.append(") ");
                sb.append("threw ");
                sb.append(e.getClass().getName());
                // stack trace in debug messages
                afficheMessage(kind, sb.toString(), LogLevel.DEBUG);
                // }
            }

            first = false;
            // next
            e = e.getCause();
        } while (e != null);

    }

    public MessageWriter getMessageWriter(MessageKind kind, int logLevel) {
        return new MessageWriter(this, kind, logLevel);
    }
}
