<?xml version='1.1' encoding="UTF-8" ?>
<!--
* NAT - An universal Translator
* Copyright (C) 2009 Bruno Mascret
* Contact: bmascret@free.fr
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
-->

<!DOCTYPE xsl:stylesheet SYSTEM "nat://system/xsl/mmlents/windob.dtd">

<xsl:stylesheet version="2.0"
xmlns:nat='http://natbraille.free.fr/xsl' 
xmlns:xsl='http://www.w3.org/1999/XSL/Transform' 
xmlns:saxon='http://icl.com/saxon'
xmlns:m='http://www.w3.org/1998/Math/MathML'
xmlns:fn='http://www.w3.org/2005/xpath-functions'
xmlns:functx='http://www.functx.com'
xmlns:xs='http://www.w3.org/2001/XMLSchema'
xmlns:lit='espacelit'
xmlns:doc='espaceDoc'
xmlns:javaNat="java:nat.saxFuncts.SaxFuncts"
xmlns:layout='http://natbraille.free.fr/layout'>

<xsl:import href="nat://system/xsl/functions/functx.xsl" /><!-- "functx functions -->

<!-- variable contenant tous les codes brailles de caractères alpha-num +apos + union, sert pour la coupure -->
<xsl:variable name="brailleChar" as="xs:string">&pt1;&pt12;&pt14;&pt145;&pt15;&pt124;&pt1245;&pt125;&pt24;&pt245;&pt13;&pt123;&pt134;&pt1345;&pt135;&pt1234;&pt12345;&pt1235;&pt234;&pt2345;&pt136;&pt1236;&pt2456;&pt1346;&pt13456;&pt1356;&pt12356;&pt16;&pt2346;&pt126;&pt1246;&pt146;&pt12456;&pt1456;&pt23456;&pt156;&pt1256;&pt12346;&pt345;&pt246;&pt3;&pt123456;&pt3456;&pt36;</xsl:variable>
<!-- variable contenant tous les codes brailles non alpha-num  dont trait d'union, sert pour la coupure -->
<xsl:variable name="brailleNonChar" as="xs:string*" select="('&pt2;','&pt23;','&pt235;','&pt236;','&pt2356;','&pt25;','&pt256;','&pt26;','&pt34;','&pt346;','&pt35;','&pt356;','&pt4;','&pt45;','&pt456;','&pt46;','&pt5;','&pt56;','&pt6;','&pt36;')"></xsl:variable>
<!-- codes brailles non alpha possibles en début de mots -->
<xsl:variable name="brailleNonCharStart" as="xs:string*" select="('&pt46;','&pt25;','&pt456;','&pt6;','&pt36;','&pt2356;','&pt236;')"/>
<!-- cette variable sert dans certains cas du calcul des lignes restantes si numéro de page = 'bs' -->
<xsl:variable name="ligneEnMoins" select="xs:integer($numerosDePage='bs')" as="xs:integer" />
<!-- cette variable sert à générer un saut si c'est à NAT de décider 
$modeLigneVide 	= 0 -> conserver lignes d'origine 
			= 1 -> personnaliser 
			= 2 -> supprimer tout
			= 3 -> braille aere
			= 4 -> braille compact 
			= 5 -> double interligne -->
<!-- nombre de ligne d'interligne (0 la plupart des cas, 1 si modeLigneVide="double interligne" -->
<xsl:variable name="interligne" as="xs:integer" select="xs:integer($modeLigneVide=5)"/>
<!-- string containing all upper braille characters -->
<xsl:variable name="str_pt_sup" as="xs:string" select="'&pt1;&pt12;&pt13;&pt14;&pt15;&pt16;&pt123;&pt124;&pt125;&pt126;&pt1234;&pt1235;&pt1236;&pt12345;&pt1245;&pt12346;&pt12356;&pt1246;&pt1256;&pt12456;&pt123456;&pt134;&pt135;&pt136;&pt135;&pt136;&pt1345;&pt1346;&pt1356;&pt13456;&pt14;&pt15;&pt16;&pt145;&pt146;&pt1456;&pt156;&pt2345;&pt245;&pt23456;&pt234;&pt246;&pt2456;&pt345;&pt24;&pt3456;&pt346;&pt4;&pt45;&pt46;&pt456;'"/>

<!-- en attendant que ça soit un param -->
<!--<xsl:variable name="styles" select="document('./ressources/styles_default.xml')/styles/*" />-->

<xsl:output method="xml" encoding="UTF-8" indent="no"/>

<!-- ****** FONCTIONS de COUPURE *********** -->
<xsl:template name="coupure">
	<xsl:param name="phrase" as="xs:string*"/> <!-- tableau des mots de la phrase tokenize sur &pt; et $sautAgenerer -->
	<xsl:param name="used" select="0" as="xs:integer"/>
	<xsl:param name="contexte" select="'lit'" as="xs:string" />
	<!-- contexte en cours lit, math, table, informatique selon césure de la ligne précédente ou 'lit' si nouveau mot -->
	<xsl:param name="firstLine" select="false()" as="xs:boolean"/>
	<xsl:param name="numPage" as="xs:integer" tunnel="yes" />
	<xsl:param name="numLigne" as="xs:integer" tunnel="yes" />
	<xsl:param name="modeBraille" as="xs:string" tunnel="yes" select="'3-1'"/> <!-- exemple de modes : 3-1, 5 ... -->
	<!-- param qui checke si un tableau tient dans la page -->
	<xsl:param name="completePage" as="xs:boolean" select="false()"/>
	
	<xsl:choose><!-- le 1er when qd c'est fini. les when 2 et 3 servent au contrôle des numéros de page et des sauts de page -->
		<xsl:when test="count($phrase)=0"/><!-- fini pour cette phrase -->
		<xsl:when test="($numLigne=1 and $numerosDePage='hs') or ($numLigne=$lignesParPage and $numerosDePage='bs')">
		<!-- ce cas n'est jamais rencontré quand $lignesParPage=0 -->
			<xsl:value-of select="doc:sautAGenerer($numLigne,$numPage,0)" />
			<xsl:if test="$numLigne=$lignesParPage"><xsl:value-of select="doc:sautePage(false(),0,0)" /></xsl:if><!-- saut de page -->
			<!-- appel coupure un peu feintu pour en faire un seul -->
			<xsl:call-template name="coupure">
				<xsl:with-param name="phrase" select="$phrase" />
				<xsl:with-param name="used" select="0"/>
				<xsl:with-param name="firstLine" select="$firstLine" />
				<xsl:with-param name="contexte" select="$contexte" />
				<xsl:with-param name="numPage" select="$numPage + xs:integer($numLigne=$lignesParPage)" tunnel="yes" />
				<xsl:with-param name="numLigne" select="xs:integer(number($numLigne=1)*$numLigne)+1" tunnel="yes" />
			</xsl:call-template>
		</xsl:when>
		<!-- on a dépassé (de 1) le nb de lignes donc saut de page OU faut sauter une page -->
		<xsl:when test="not($lignesParPage=0) and ((($numLigne+$interligne) > $lignesParPage) or ($completePage))">
			<xsl:choose>
				<xsl:when test="($numLigne=$lignesParPage and $numerosDePage='bb')">
					<!-- <xsl:value-of select="functx:repeat-string('X',$longueur - $used - 3)" /> -->
					<xsl:value-of select="doc:sautAGenerer($numLigne,$numPage,$used)" />
					<xsl:value-of select="doc:sautePage(false(),0,0)" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="doc:sautePage(true(),$numLigne,$numPage)" /><!-- saut de page -->
				</xsl:otherwise>
			</xsl:choose>
			<xsl:call-template name="coupure">
				<xsl:with-param name="phrase" select="$phrase" />
				<xsl:with-param name="used" select="0"/>
				<xsl:with-param name="firstLine" select="$firstLine" />
				<xsl:with-param name="contexte" select="$contexte" />
				<xsl:with-param name="numPage" select="$numPage+1" tunnel="yes" />
				<xsl:with-param name="numLigne" select="1" tunnel="yes" />
			</xsl:call-template>
		</xsl:when>
		<xsl:when test="translate($phrase[1],$carcoup,'')=''"><!-- si le mot est vide on passe direct au mot suivant -->
			<xsl:call-template name="coupure">
				<xsl:with-param name="phrase" select="$phrase[position()>1]" />
				<xsl:with-param name="used" select="$used"/>
				<xsl:with-param name="firstLine" select="$firstLine" />
				<xsl:with-param name="contexte" select="$contexte" />
				<xsl:with-param name="completePage" select="$completePage" />
			</xsl:call-template>
		</xsl:when>
	<xsl:otherwise><!-- tout le reste du template est dans xsl:otherwise --><!-- -->
		<xsl:variable name="longRetrait" as="xs:integer">
			<xsl:choose>
				<!-- si on a un tableau, pas de retrait -->
				<xsl:when test="nat:starts-with-any-of($phrase[1],($debTable,concat($finTable,$debTable)))"><xsl:value-of select="0"/></xsl:when>
				<!-- si on a une phrase composée uniquement d'une expression mathématique on prend le retrait le plus petit -->
				<xsl:when test="$used=0 and $firstLine and starts-with($phrase[1],$debMath) and 
					ends-with($phrase[last()],$finMath) and count($phrase[position()&lt;last()][contains(.,$finMath)])=0">
					<xsl:value-of select="xs:integer(min(
					(functx:substring-before-if-contains($modeBraille,'-'),functx:substring-after-if-contains($modeBraille,'-'))))-1"/>
				</xsl:when>
				<xsl:when test="$used=0"> <!-- si used = 0, on prend en compte le mode braille pour le retrait  -->
					<xsl:value-of select="if ($firstLine) then xs:integer(number(functx:substring-before-if-contains($modeBraille,'-'))-1)
								else xs:integer(number(functx:substring-after-if-contains($modeBraille,'-'))-1)" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$used" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<!-- place réelle de la ligne pour mettre du braille -->
		<xsl:variable name="longLigne" as="xs:integer" select="doc:longueurNumPage($numLigne,$numPage) - $longRetrait" />
		
		<!--  PREPARATION DEs MOTS ENTIERS A AFFICHER -->
		<!-- la ligne1 est composée de tous les mots qui rentrent dans la longueur 
		MAIS S'ARRETE DES QU'IL Y A UN TABLEAU -->
		<xsl:variable name="ligne1" as="xs:string*">
			<xsl:if test="not(contains($contexte,'table')) or contains($phrase[1],$finTable)">
				<xsl:call-template name="remplitLigne">
					<xsl:with-param name="phrase" select="$phrase" />
					<xsl:with-param name="longueurDispo" select="$longLigne" />
				</xsl:call-template>
			</xsl:if>
		</xsl:variable>
		
		<!-- CALCUL DU NOUVEAU CONTEXTE -->
		<!-- le nouveau contexte est égal à l'ancien si on a mis aucun nouveau mot entier sur la ligne
		sinon c'est le dernier mot qui décide du nouveau contexte -->
		<xsl:variable name="derMot" select="$phrase[position()=(count($ligne1)+1)]" />
		<xsl:variable name="derMotBraille" select="translate($derMot,concat($carcoup,$stopCoup),'')" />
		<xsl:variable name="longRestante" select="$longLigne - string-length(string-join($ligne1,'&pt;')) - xs:integer(boolean($ligne1[1]))" />
		<xsl:variable name="longLigneSuivante" 
						select="doc:longueurNumPage($numLigne+1,$numPage) - xs:integer(number(functx:substring-after-if-contains($modeBraille,'-'))-1)"/>
		<!-- -boolean($ligne1[1]) est vrai donc 1 s'il y a un mot avant, donc il faut mettre un espace, sinon non -->
		<xsl:variable name="newContext" as="xs:string">
			<!--<xsl:message select="($derMot,'******',nat:toTbfr($derMot))"/>-->
			<xsl:choose>
				<xsl:when test="contains($derMot,$debTable)">
					<xsl:value-of select="if (contains($contexte,'math') or starts-with($derMot,$debMath)) then 'table-math' else 'table-lit'"/>
				</xsl:when>
				<xsl:when test="contains($derMot,$finTable) and contains($contexte,'table')">
					<xsl:value-of select="substring-after($contexte,'table-')" /><!--<xsl:message select="'cas2'" />-->
				</xsl:when>
				<xsl:when test="count($ligne1)=0 and $contexte!='lit'"><!--<xsl:message select="'cas3'" />--><xsl:value-of select="$contexte"/></xsl:when>
				<xsl:when test="starts-with($derMot,$debMath)"><xsl:value-of select="'math'"/></xsl:when>
				<!-- le matches du dessous plante quand on a une ligne de points alors work-around "sale -->
				<xsl:when test="contains(nat:toTbfr($derMot),'...')"><xsl:value-of select="'lit'"/></xsl:when>
				<!-- vgu 2024.04.30 - url regexp causes net.sf.saxon.trans.XPathException Regex backtracking limit exceeded processing -->
				<!--                  so remove this xsl:when for now -->
                      <!-- chgt matches car regex plante trop ; source : http://web.archive.org/web/20100415205252/http://www.eurelis.com/sites/fr/blog/20100126_expressions_regulieres -->
				      <!-- <xsl:when test="matches(nat:toTbfr($derMot),'^((((ftp|http|https)://){0,1}([A-Za-z0-9\._/-]+\.)+[A-Za-z]{2,4})|([\w.%+-]+@[a-zA-Z\d.-]+\.[A-Za-z]{2,4}))$')"> -->
				      <!-- ***  wwwfunctx:contains-any-of(nat:toTbfr($derMotBraille),('@','://','www'))"> -->
				      <!-- 	<xsl:value-of select="'informatique'"/> -->
				      <!-- </xsl:when> -->
				<xsl:otherwise><xsl:value-of select="'lit'"/></xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="cesure" as="xs:string" select="if ($newContext=('table-math','math','informatique')) then '&pt5;' else '&pt36;'"/>

		<!--<xsl:message select="concat (nat:toTbfr(string-join($phrase,'***')),' ',$contexte,' ',$newContext)" />-->
		<!-- MISE EN FORME DU DERNIER MOT -->
		<!-- le dernierMot sera en deux parties : ce qui est mis sur la ligne (SANS carcoup mais avec césure) et ce qui reste à transcrire (AVEC carcoup)-->
		<xsl:variable name="testDernierMot" as="xs:string*">
			<xsl:choose>
				<!--<xsl:when test="$newContext='table-lit'">
					<xsl:value-of select="$derMotBraille" />
				</xsl:when>-->
				<xsl:when test="contains($newContext,'table')">
					<xsl:choose>
						<!-- cas 1 : on est en cours de tableau donc on met la ligne courante, c'est tout -->
						<xsl:when test="not(contains($derMot,$debTable))">
							<xsl:value-of select="$derMotBraille" />
						</xsl:when>
						<!-- sinon on passe tout à coupureTableau -->
						<xsl:otherwise>
							<xsl:call-template name="coupureTableau">
								<xsl:with-param name="phrase" select="$phrase[position()>=(count($ligne1)+1)]" />
								<xsl:with-param name="numLigne" select="$numLigne" />
								<xsl:with-param name="numPage" select="$numPage" />
								<xsl:with-param name="place" select="$longRestante" />
								<xsl:with-param name="contexte" select="$newContext" />
								<xsl:with-param name="used" select="$used" />
							</xsl:call-template>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<!-- mode sagouin sélectionné : on coupe en sagouin -->
				<xsl:when test="$sagouin">
					<xsl:variable name="derMot1" select="substring($derMotBraille,1,$longRestante -1)" />
					<!-- -1 pour le char de coupe -->
					<xsl:choose>
						<!-- pour le mode sagouin, on s'en fout des $carcoup donc on peut mettre du motbraille en second item -->
						<xsl:when test="string-length($derMot1) > 0">
							<xsl:value-of select="concat($derMot1,$cesure)" />
							<xsl:value-of select="substring-after($derMotBraille,$derMot1)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="''" />
							<xsl:value-of select="$derMotBraille" />
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<!-- coupure mathématique -->
				<xsl:when test="$newContext='math'">
					<xsl:variable name="longExpression" select="string-length($derMotBraille)" /><!-- -->
					<xsl:choose>
						<!-- cas 1 : si il y a déjà des mots derrière et que ça tient dans la ligne suivante, on va à la ligne suivante -->
						<xsl:when test="count($ligne1) >0 and ($longLigneSuivante >= $longExpression)" />
						<!-- cas 2 : sinon on tente de couper -->
						<xsl:otherwise>
							<xsl:call-template name="coupureMaths">
								<xsl:with-param name="leMot" select="tokenize($derMot,$coupeEsth)" />
								<xsl:with-param name="charCesure" select="$cesure" />
								<xsl:with-param name="place" select="$longRestante" />
								<xsl:with-param name="longLigneSuivante" select="$longLigneSuivante" />
							</xsl:call-template>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<!-- coupure littéraire : on coupe le mot en deux -->
				<xsl:when test="$couplit and $newContext=('lit','informatique')">
					<xsl:call-template name="coupureLit">
						<xsl:with-param name="leMot" select="$derMot" />
						<xsl:with-param name="charCesure" select="$cesure" />
						<xsl:with-param name="place" select="$longRestante" />
					</xsl:call-template>
				</xsl:when>
				<!-- pas de coupe littéraire : rien  -->
				<xsl:otherwise />
			</xsl:choose>
		</xsl:variable>
		<!--<xsl:message select="if ($newContext='table-lit') then nat:toTbfr(string-join($testDernierMot,'**')) else '' "/>-->
		<xsl:variable name="dernierMot" as="xs:string*">
			<xsl:choose>
				<xsl:when test="$testDernierMot[3]='SAUTEPAGE'"/><!-- si on saute une page on imprime rien -->
				<xsl:when test="$testDernierMot[1]"><xsl:copy-of select="$testDernierMot[position()&lt;3]"/></xsl:when>
				<!-- s'il n'y a aucun mot entier sur la ligne1 et que le dernier mot ne rentre pas dans la ligne, on active le sagouin
				EN GARDANT LES CARCOUP DE LA SECONDE PARTIE DU MOT -->
				<xsl:when test="not($testDernierMot[1] or $ligne1[1]) and string-length($derMotBraille)>$longLigneSuivante">
					<!--<xsl:message select="concat('sagouin p',$numPage,' l',$numLigne,' ligne11 ',string-join($ligne1,'Z'),' testdermot ',nat:toTbfr(string-join($testDernierMot,'Z')))" />-->	
					<xsl:variable name="lettres" select="functx:chars($phrase[1])" as="xs:string*" />
					<xsl:variable name="derMot1" as="xs:string*">
						<xsl:for-each select="$lettres">
							<xsl:variable name="pos" select="position()"/>
							<xsl:if test="$longLigne>string-length(translate(string-join($lettres[position()&lt;=$pos],''),$carcoup,''))">
								<xsl:value-of select="." />
							</xsl:if>
						</xsl:for-each>
					</xsl:variable>
					<xsl:value-of select="if (count($derMot1)>0) then translate(string-join(($derMot1,$cesure),''),$carcoup,'') else ''" />
					<!-- TODO les $carcoup en dernière position sont perdus avec cette méthode -->
					<xsl:value-of select="string-join($lettres[position()>count($derMot1)],'')" />
				</xsl:when>
				<xsl:otherwise/>
			</xsl:choose>
		</xsl:variable>
		
		<!-- création de la ligne complete à afficher -->
		<xsl:element name="layout:indentation">
			<xsl:value-of select="functx:repeat-string('&pt;',$longRetrait)"/>
		</xsl:element>
		<xsl:variable name="ligneBrailleToken" as="xs:string*">
			<!--<xsl:value-of select="functx:repeat-string('&pt;',$longRetrait)"/>-->
			<xsl:value-of select="string-join($ligne1,'&pt;')"/>
			<xsl:value-of select="if ($dernierMot[1] and $ligne1[1]) then concat('&pt;',$dernierMot[1]) else $dernierMot[1]" />
		</xsl:variable>
		<xsl:variable name="ligneBraille" as="xs:string" select="string-join($ligneBrailleToken,'')"/>
		
		<!-- AFFICHAGE FINAL -->
		<xsl:if test="@styleOrig=$stylesEnvers">
			<!-- si on est en braille à l'envers, on aligne à droite -->
			<xsl:value-of select="functx:repeat-string($espace,xs:integer($longueur - string-length($ligneBraille)))" />
		</xsl:if>
		<xsl:value-of select="$ligneBraille" />
		<xsl:value-of select="doc:sautAGenerer($numLigne,$numPage,string-length($ligneBraille))" />
		<xsl:value-of select="for $i in 1 to $interligne return doc:sautAGenerer($numLigne + $i,$numPage,0)" />
		<!-- string($numLigne +1),doc:sautAGenerer($numLigne+1,$numPage,0))" /> -->
		<!--<xsl:message select="nat:toTbfr(concat($ligneBraille,doc:sautAGenerer($numLigne,$numPage,string-length($ligneBraille))))" /> -->
		
		<xsl:variable name="newPhrase" select="if ($dernierMot[2]) then
			($dernierMot[2],$phrase[position() > (count($ligne1)+xs:integer(count($dernierMot)>0))])
			else ($phrase[position() > (count($ligne1)+xs:integer(count($dernierMot)>0))])"/>

		<xsl:if test="count($newPhrase) > 0">
			<xsl:variable name="newUsed" as="xs:integer">
				<xsl:choose>
					<xsl:when test="functx:is-a-number($testDernierMot[3]) and number($testDernierMot[3])=0">
						<xsl:value-of select="0" />
					</xsl:when>
					<xsl:when test="functx:is-a-number($testDernierMot[3])">
						<xsl:value-of select="string-length(string-join($ligneBrailleToken[not(position()=last())],''))
								+xs:integer(number($testDernierMot[3]))+xs:integer(starts-with($ligneBrailleToken[3],'&pt;'))"/>
					</xsl:when>
					<xsl:when test="contains($newContext,'table')"><xsl:value-of select="$used" /></xsl:when>
					<xsl:otherwise><xsl:value-of select="0"/></xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			
			<xsl:call-template name="coupure">
				<xsl:with-param name="phrase" select="$newPhrase" />
				<xsl:with-param name="used" select="$newUsed"/>
				<xsl:with-param name="firstLine" select="false()" />
				<xsl:with-param name="contexte" select="$newContext" />
				<xsl:with-param name="numPage" select="$numPage" tunnel="yes" />
				<xsl:with-param name="numLigne" select="$numLigne+1+$interligne" tunnel="yes" />
				<xsl:with-param name="completePage" select="$testDernierMot[3]='SAUTEPAGE'" />
			</xsl:call-template>
		</xsl:if>
	</xsl:otherwise><!-- fin du otherwise "principal" -->
	</xsl:choose>
</xsl:template>

<!-- coupure littéraire : renvoie le début de mot à insérer (sans $carcoup mais avec cesure) ET le reste du mot AVEC char de coup ou rien si on peut pas couper -->
<xsl:template name="coupureLit" as="xs:string*">
	<xsl:param name="leMot" as="xs:string?"/><!-- mot à traiter -->
	<xsl:param name="place" select="0" as="xs:integer"/><!-- nb de caractères qu'il reste sur la ligne -->
	<xsl:param name="charCesure" select="'&pt36;'" as="xs:string?" />

	<!-- motAvecCoupure est pris tel quel s'il a déjà des $coupe en lui (cas du braille abrégé)  sinon on les crée avec hyphenate (cas du braille intégral) -->
	<xsl:variable name="motAvecCoupure" as="xs:string?">
		<xsl:choose>
			<xsl:when test="not($leMot)" />
			<xsl:when test="functx:contains-any-of($leMot,($coupe,$coupeEsth))">
				<xsl:value-of select="$leMot" />
			</xsl:when>
			<xsl:otherwise>
				<!-- préparation et appel d'hyphenate -->
				<!-- découpage de la chaîne en fonction des traits-d'union -->
				<xsl:variable name="leMotSplit1" as="xs:string*" select="if(starts-with($leMot,$stopCoup)) then $leMot else (tokenize($leMot ,'&pt36;'))"/>
				<!-- si la chaine commence par un tiret... -->
				<xsl:variable name="leMotSplit" as="xs:string*" select="if(translate($leMotSplit1[1],string-join($brailleNonCharStart,''),'')=''
				and contains(substring($leMot,1,string-length($leMotSplit1[1]) + 1),'&pt36;')) then
				((concat($leMotSplit1[1],'&pt36;',$leMotSplit1[2]),$leMotSplit1[position() &gt; 2]))
				else ($leMotSplit1)"/>
				<!--<xsl:message select="('lesplit1',$leMotSplit1,'Motsplit',$leMotSplit)"/>-->
				<!-- recherche des coupures du mot -->
				<xsl:variable name="motAvecCoupSplit" as="xs:string*">
					<!-- préparation et appel d'hyphenate sur chaque sous-chaine du mot -->
					<xsl:for-each select="$leMotSplit">
						<!-- sauvegarde des préfixes qui doivent être retirés du mot à couper -->
						<xsl:variable name="motSansPref" as="xs:string?">
							<xsl:choose>
								<xsl:when test="nat:starts-with-any-of(.,$brailleNonChar)">
									<!-- suppression des préfixes, e.reg: toute combinaison de prefixe en début de mot -->
									<!---<xsl:value-of select="replace($leMot,concat('^(',string-join($lPrefixesReg,'|'),')*'),'')"/>-->
									<xsl:value-of select="functx:substring-after-match(.,concat('^[',string-join($brailleNonChar,''),']+'))"/>
								</xsl:when>
								<xsl:otherwise><xsl:value-of select="."/></xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<!-- si il y a une marque d'arrêt de coupure (symbole fondamental ou assimilé en abrégé), on ajoute une coupe possible avant -->
						<xsl:variable name="prefixes1" as="xs:string?" select="substring-before(.,$motSansPref)"/>
						<xsl:variable name="prefixes" as="xs:string?" select="if(contains($motSansPref,$stopCoup) and not(starts-with($motSansPref,$stopCoup))
						and(string-length(translate($prefixes1,string-join($brailleNonChar,''),'')) &gt; 0)) then concat(substring-before(.,$motSansPref),$coupe) else $prefixes1"/>
						<!--<xsl:message select="('split:',$leMotSplit)"/>
						<xsl:message select="('pref:',$prefixes)"/>-->
						<!-- mot sans les suffixes, incluant les parties du mot à ne pas couper (abrégé) -->
						<xsl:variable name="motSansSuff" as="xs:string?">
							<xsl:choose>
								<xsl:when test="contains($motSansPref,$stopCoup)">
									<xsl:value-of select="translate(substring-before($motSansPref,$stopCoup),$stopCoup,'')"/>
								</xsl:when>
								<xsl:when test="nat:ends-with-any-of($motSansPref,$brailleNonChar)">
									<!-- suppression des suffixes, e.reg: toute combinaison de suffixes en fin de mot -->
									<xsl:value-of select="functx:substring-before-match($motSansPref,concat('[',string-join($brailleNonChar,''),']$'))"/>
								</xsl:when>
								<xsl:otherwise><xsl:value-of select="$motSansPref"/></xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="suffixes" as="xs:string?" select="if(contains($motSansPref,$stopCoup)) then translate(substring-after($motSansPref,$stopCoup),$stopCoup,'') 
							else (substring-after($motSansPref,$motSansSuff))"/>
						<!--<xsl:message select="concat('mot:',.,' pref:',$prefixes,' corps:',$motSansSuff,' suff:',$suffixes)"/>-->
						<!--coupure de la sous-chaine, et recomposition du mot original-->
						<!--<xsl:value-of select="concat($prefixes,javaNat:hyphenates($motSansSuff,$coupe,$patterns,$patterns0,$patterns0nd),$suffixes)"/>-->
						<xsl:value-of select="if($motSansSuff='') then concat($prefixes,$suffixes) else
						(concat($prefixes,nat:hyphenate($motSansSuff,$coupe),if(ends-with($motSansSuff,'&pt3;')) then '' else ($coupe),$suffixes))"/>
						<!--<xsl:message select="concat($prefixes,testNat:hyphenate($motSansSuff,$coupe,$patterns,$patterns0,$patterns0nd),$suffixes)" xmlns:testNat="java:nat.saxFuncts.SaxFuncts"/>-->
					</xsl:for-each>
				</xsl:variable>
				<xsl:variable name="hyphenated" select="string-join($motAvecCoupSplit,'&pt36;')"/>
				<!-- removing low-point hyphenations strarting strings -->
				<xsl:variable name="indexOfFirstUpperPointChar" select="functx:index-of-match-first($hyphenated, concat('[',$str_pt_sup,']'))"/>
				<xsl:choose>
					<xsl:when test="$indexOfFirstUpperPointChar &gt; 1">
						<xsl:value-of select="concat(translate(substring($hyphenated,1, $indexOfFirstUpperPointChar - 1),$coupe,''),substring($hyphenated, $indexOfFirstUpperPointChar))"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$hyphenated"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<!--<xsl:message select="nat:toTbfr(translate($motAvecCoupure,$coupe,'X'))" /> -->
	<!-- maintenant on cherche à mettre le max de patterns dans la place qu'il reste -->
	<xsl:variable name="boutsDeMot" select="tokenize($motAvecCoupure,$coupe)" />
	<xsl:variable name="boutEcrit" as="xs:string*">
		<xsl:for-each select="$boutsDeMot">
			<xsl:if test="string-length(.) &gt; 0">
				<xsl:variable name="pos" select="position()" />
				<xsl:if test="$place > string-length(string-join($boutsDeMot[position() &lt;= $pos],''))">
					<xsl:value-of select="." />
				</xsl:if>
			</xsl:if>
		</xsl:for-each>
	</xsl:variable>
	<!--<xsl:message select="$boutsDeMot"/>-->
	<!-- 1er renvoi : bout qu'on inscrit -->
	<xsl:value-of select="if (count($boutEcrit)>0) then string-join(($boutEcrit,$charCesure),'') else ''"/>
	<!-- 2eme renvoi : bout qu'il faut continuer à traiter avec chars de coupe -->
	<xsl:variable name="boutFin" select="string-join($boutsDeMot[position()>count($boutEcrit)],$coupe)" />
	<!-- on vire le tiret du début si il y en a un -->
	<xsl:value-of select="if(starts-with($boutFin,'&pt36;') and count($boutEcrit)>0) then substring-after($boutFin,'&pt36;') else ($boutFin)"/>
	<!--<xsl:message select="nat:toTbfr(string-join($boutsDeMot[position()>count($boutEcrit)],''))" /> -->
</xsl:template>

<!-- coupure mathematique : renvoie le début de mot à insérer (sans $carcoup mais avec cesure) ET le reste du mot AVEC char de coup 
ou rien si on peut pas couper ;  s'arrête s'il y a un tableau -->
<xsl:template name="coupureMaths" as="xs:string*">
	<xsl:param name="leMot" as="xs:string*"/>
	<!-- leMot est un tokenize de l'expression sur coupeEsth -->
	<xsl:param name="place" select="0" as="xs:integer"/>
	<xsl:param name="longLigneSuivante" select="$longueur" as="xs:integer" />
	<xsl:param name="charCesure" select="'&pt5;'" as="xs:string?" />
	
	<!--<xsl:message select="concat('pr',nat:toTbfr(string-join($leMot,'X')),'out')" />-->

	<xsl:variable name="boutEcrit" as="xs:string*">
		<xsl:call-template name="remplitLigne">
			<xsl:with-param name="phrase" select="$leMot" />
			<xsl:with-param name="longueurDispo" select="$place - string-length($charCesure)" />
			<xsl:with-param name="entreMot" select="0" />
			<xsl:with-param name="stopOnTable" select="false()" />
		</xsl:call-template>
	</xsl:variable>
	<xsl:choose>
		<!-- cas 1 : une coupure esthétique rentre dans $place : on renvoie l'expression en 2 parties -->
		<xsl:when test="count($boutEcrit) > 0">
			<!--<xsl:message>
				<xsl:value-of select="concat ('cas 1 ',$longLigneSuivante,' ',$place,' ',string-length(translate($leMot[1],$carcoup,'')),'&#10;')"/>
				<xsl:value-of select="nat:toTbfr(translate(string-join(($boutEcrit,$charCesure,'&#10;'),''),$carcoup,''))"/>
				<xsl:value-of select="nat:toTbfr(string-join($leMot[position() >count($boutEcrit)],$coupeEsth))"/>
			</xsl:message>-->
			<xsl:value-of select="translate(string-join(($boutEcrit,$charCesure),''),$carcoup,'')"/>
			<xsl:value-of select="string-join($leMot[position() >count($boutEcrit)],$coupeEsth)"/>
		</xsl:when>
		<!-- cas 2 : aucune coupeEsth ne rentre dans $place mais rentre dans $longLigneSuivante : on renvoie rien 
		sauf si on traite une fin de tableau auquel cas il faut renvoyer au moins la fin du tableau -->
		<xsl:when test="not(contains(string-join($leMot,''),$finTable)) and
				string-length(translate($leMot[1],$carcoup,'')) &lt;= ($longLigneSuivante - string-length($charCesure))">
			<!--<xsl:message select="concat ('cas 2 ',$longLigneSuivante,' ',$place,' ',string-length(translate($leMot[1],$carcoup,'')))"/>-->
		</xsl:when>
		<!-- cas 3 : aucune coupeEsth ne rentre dans ligne suivante ou on traite un tableau, on se la tente avec des $coupe -->
		<xsl:when test="contains($leMot[1],$coupe)">
			<!--<xsl:message select="'cas 3'" />-->
			<xsl:call-template name="coupureMaths">
				<xsl:with-param name="leMot" select="(tokenize($leMot[1],$coupe),$leMot[position() >1])" />
				<xsl:with-param name="charCesure" select="$charCesure" />
				<xsl:with-param name="place" select="$place" />
				<xsl:with-param name="longLigneSuivante" select="$longLigneSuivante" />
			</xsl:call-template>
		</xsl:when>
		<!-- cas 4 : si après un appel récursif c'est toujours pas possible, on fait rien, sagouin s'en chargera -->
		<xsl:otherwise><!--<xsl:message select="'cas 4'"/>--></xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- coupure tableau renvoie comme coupureMath mais en checkant s'il y a assez de lignes restantes sur la page
pour le tableau et renvoie une troisème string : 'SAUTEPAGE' si pas assez de lignes ou un nombre pour USED -->
<xsl:template name="coupureTableau" as="xs:string*">
	<xsl:param name="phrase" as="xs:string*"/>
	<xsl:param name="numLigne" as="xs:integer"/>
	<xsl:param name="numPage" as="xs:integer"/>
	<xsl:param name="place" select="0" as="xs:integer"/>
	<xsl:param name="contexte" select="'table-math'" as="xs:string" />
	<xsl:param name="used" />

	<xsl:variable name="derMotBraille" as="xs:string" select="translate($phrase[1],$carcoup,'')"/>
	<xsl:variable name="longExpression" select="string-length($derMotBraille)" /><!-- -->
	<xsl:variable name="longLigneSuivante" select="doc:longueurNumPage($numLigne+1,$numPage)"/>
	<xsl:variable name="nbLigFoireux" as="xs:integer" 
		select="if (exists($phrase[position() >1 and contains(.,$finTable)])) then
			index-of($phrase,$phrase[position() >1 and contains(.,$finTable)][1])[1] else 1"/>
	<!-- des fois, le $fintab est dans la suite du tableau car "aléatoirement" un $sautAGenerer est mis entre ou pas -->
	<xsl:variable name="nbLigTable" as="xs:integer" select="if (starts-with($phrase[$nbLigFoireux],$finTable)) then ($nbLigFoireux -1) else $nbLigFoireux" />
	<xsl:variable name="lignesRestantes" as="xs:integer" select="$lignesParPage - $ligneEnMoins - $numLigne" />
	<xsl:variable name="assezDeLignes" as="xs:boolean"
		select="if ($nbLigTable &lt;= ($lignesRestantes+1) or $nbLigTable > ($lignesParPage - $ligneEnMoins)) then true() else false()"/>
	<xsl:variable name="assezDeLignes2" as="xs:boolean"
		select="$assezDeLignes or ($nbLigTable = $lignesRestantes) or ($numLigne = $lignesParPage - $ligneEnMoins)"/>
	<!-- en effet, si $numLigne = $lignesParPage - $ligneEnMoins on est à la dernière ligne de la page donc en sautant une page y'aura assez de lignes -->
	
	<xsl:variable name="bonusChar" as="xs:integer"
		select="if (translate(substring-after($phrase[$nbLigFoireux],$finTable),$carcoup,'')='') then 1 else 0"/>
	<!-- s'il n'y a rien derrière le tableau, on a pas à prévoir la place le signe de coupe derrière -->
	
	<!--<xsl:message select="concat(nat:toTbfr($phrase[1]),' numlig ',$numLigne,' lignesrest ',$lignesRestantes,' nblig ',$nbLigTable,' lgligsuiv ', $longLigneSuivante,' asslig asslig2 ',$assezDeLignes,' ',$assezDeLignes2)"/> -->
	<xsl:choose>
		<!-- cas 0 : pour un tableau littéraire, on peut rentrer pile dans $place, on checke juste assezDeLignes -->
		<xsl:when test="$contexte='table-lit' and $longExpression &lt;=$place">
			<!--<xsl:message select="concat ('cas Lit ',nat:toTbfr(string-join($phrase,'/*/')),' nbligtab ',$nbLigTable,' numligne ',$numLigne,' ligrest ',$lignesRestantes,' assez ',$assezDeLignes)"/> -->
			<!-- si on est en numpage=bb et que ça tient pile il faut regarder si la dernière ligne du tableau tient sur la dernière ligne de la page -->
			<xsl:variable name="probDerLigne" select="$numerosDePage='bb' and $nbLigTable = $lignesRestantes + 1
				and (string-length(translate($phrase[$nbLigTable],$carcoup,'')) > doc:longueurNumPage($lignesParPage,$numPage))" /> 
			<xsl:sequence select="if ($assezDeLignes and not($probDerLigne)) then $derMotBraille else ('','','SAUTEPAGE')"/>
		</xsl:when>
		<!-- cas 1 : assez de place sur la ligne et assez de lignes pour le tableau -->
		<xsl:when test="$longExpression &lt; ($place+$bonusChar) and $assezDeLignes">
			<!--<xsl:message select="'cas A '"/> -->
			<xsl:sequence select="($derMotBraille,'',string(string-length(translate(substring-before($phrase[1],$debTable),$carcoup,''))))" />
		</xsl:when>
		<!-- cas 2 : assez de place sur la ligne suivante, on renvoie juste un saut de page si necessaire 
		SAUF si un tableau est déjà en cours de traitement -->
		<xsl:when test="$longExpression &lt; ($longLigneSuivante+$bonusChar) and not(contains($phrase[1],$finTable))">
			<xsl:sequence select="if ($assezDeLignes2) then () else ('','','SAUTEPAGE')"/>
			<!--<xsl:message select="'cas B '"/> -->
		</xsl:when>
		<!-- cas 3 : on coupe ce qu'il y a avant le tableau en maths  -->
		<xsl:otherwise>
			<!--<xsl:message select="'cas C '"/> -->
			<xsl:variable name="resu" as="xs:string*">
				<xsl:call-template name="coupureMaths">
					<xsl:with-param name="leMot" select="tokenize($phrase[1],$coupeEsth)" />
					<xsl:with-param name="charCesure" 
						select="if (functx:contains-any-of($phrase[1],($coupe,$coupeEsth))) then '&pt5;' else ''" />
					<!-- cas limite : si un tableau fait pile la taille de la ligne, on va pas le césurer, tant pis -->
					<xsl:with-param name="place" select="($place+$bonusChar)" />
					<xsl:with-param name="longLigneSuivante" select="($longLigneSuivante+$bonusChar)" />
				</xsl:call-template>
			</xsl:variable>
			<xsl:value-of select="$resu[1]" />
			<xsl:value-of select="$resu[2]" />
			<xsl:value-of select="if(contains($resu[2],$debTable) or not($resu[1])) then '0' else
					string(string-length(translate(substring-before($phrase[1],$debTable),$carcoup,'')))" />
			<!-- si on a plus de debTable dans resu[2] alors on a mis le tableau et il faut incrémenter $used -->
			
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- template qui renvoit une séquence de mots tenant dans une ligne en s'arrêtant dès qu'il y a un tableau -->
<xsl:template name="remplitLigne" as="xs:string*">
	<xsl:param name="phrase" as="xs:string*" />
	<xsl:param name="longueurDispo" as="xs:integer" />
	<xsl:param name="entreMot" as="xs:integer" select="1" />
	<xsl:param name="stopOnTable" as="xs:boolean" select="true()" />
	
	<xsl:variable name="motBraille" select="translate($phrase[1],concat($stopCoup,$carcoup),'')" as="xs:string?" />
	<xsl:variable name="longMotBraille" select="string-length($motBraille)" as="xs:integer" />
	<xsl:if test="$longMotBraille > 0 and $longueurDispo >= $longMotBraille and 
			not(contains($phrase[1],$debTable) and $stopOnTable)">
		<xsl:value-of select="$motBraille" />
		<!--<xsl:message select="concat('**',nat:toTbfr($motBraille),'**', $longueurDispo,' ',$longMotBraille)"/> -->
		<xsl:call-template name="remplitLigne">
			<xsl:with-param name="phrase" select="$phrase[position() > 1]" />
			<xsl:with-param name="entreMot" select="$entreMot" />
			<xsl:with-param name="longueurDispo" select="$longueurDispo - $longMotBraille - $entreMot" />
			<!-- entreMot= 1 pour l'espace qui précède le prochain mot (coupure) ou 0 pour coupureMath ou coupureLit-->
		</xsl:call-template>
	</xsl:if>
</xsl:template>

<!-- function de gestion de saut de ligne avec ou sans numero de page -->
<xsl:function name="doc:sautAGenerer" as="xs:string">
	<xsl:param name="numLigne" as="xs:integer" />
	<xsl:param name="numPage" as="xs:integer" />
	<xsl:param name="used" as="xs:integer" />
	
	<xsl:variable name="intermediaire" as="xs:string*">
		<xsl:variable name="pasDeNumero" as="xs:boolean" select="$numPage &lt; $startNumberingPage" />
		<xsl:variable name="numeroHaut" as="xs:boolean" select="$numLigne=1 and substring($numerosDePage,1,1)='h'" />
		<xsl:variable name="numeroBas" as="xs:boolean" select="$numLigne=$lignesParPage and substring($numerosDePage,1,1)='b'" />
		
		<xsl:if test="not($pasDeNumero) and ($numeroHaut or $numeroBas)">
			<xsl:value-of select="functx:repeat-string($espace,$longueur - $used - string-length(doc:numberBraille($numPage)))" />
			<xsl:value-of select="doc:numberBraille($numPage)"/>
			<!--<xsl:message select="concat('ligne ',$numLigne,' numpage ',$numPage)"/> -->
		</xsl:if>
		<xsl:value-of select="$sautAGenerer"/>
	</xsl:variable>
	
	<xsl:value-of select="string-join($intermediaire,'')"/>
</xsl:function>

<!-- function de gestion de saut de page -->
<xsl:function name="doc:sautePage" as="xs:string">
	<xsl:param name="completePage" as="xs:boolean" />
	<xsl:param name="numLigne" as="xs:integer" />
	<xsl:param name="numPage" as="xs:integer" />
	
	<xsl:variable name="intermediaire" as="xs:string*">
		<xsl:if test="$completePage and $numLigne &lt;= $lignesParPage">
				<xsl:value-of select="functx:repeat-string($sautAGenerer,$lignesParPage - $numLigne)" />
				<xsl:value-of select="doc:sautAGenerer($lignesParPage,$numPage,0)" />
		</xsl:if>
		<xsl:value-of select="'&#12;'" /><!-- saut de page -->
	</xsl:variable>
	<xsl:value-of select="string-join($intermediaire,'')"/>
</xsl:function>

<!-- fonction de longueur de ligne selon les numéros de page SANS RETRAIT -->
<xsl:function name="doc:longueurNumPage" as="xs:integer">
	<xsl:param name="numLigne" as="xs:integer" />
	<xsl:param name="numPage" as="xs:integer" />
	<!--<xsl:message select="$numLigne,':',$numPage,':',$startNumberingPage"/>-->
	<xsl:choose>
		<!--<xsl:when test="($numPage=1) and not($numberFirstPage)"><xsl:value-of select="$longueur" /></xsl:when>-->
		<xsl:when test="($numPage &lt; $startNumberingPage) or ($lignesParPage=0)"><xsl:value-of select="$longueur"/></xsl:when>
		<xsl:when test="(($numLigne mod $lignesParPage) = 1  and $numerosDePage='hb') or (($numLigne mod $lignesParPage)=0 and $numerosDePage='bb')">
			<!-- -3 car 3 espaces au moins dans norme avant numero de page -->
			<xsl:value-of select="$longueur - 3 - string-length(doc:numberBraille($numPage+floor($numLigne div $lignesParPage)))" />
		</xsl:when>
		<xsl:otherwise><xsl:value-of select="$longueur" /></xsl:otherwise>
	</xsl:choose>
</xsl:function>

<!-- fonction de numerotation braille -->
<xsl:function name="doc:numberBraille" as="xs:string"><!-- -->
	<xsl:param name="numero" as="xs:decimal" />
	
	<!-- obligé d'utiliser une variable à cause des guillemets et apostrophes comme pt6 pour cbfr1252-->
	<xsl:variable name="listeNumbers" as="xs:string">
		<xsl:text>&pt6;&pt3456;&pt16;&pt126;&pt146;&pt1456;&pt156;&pt1246;&pt12456;&pt1256;&pt246;</xsl:text>
	</xsl:variable>
	
	<xsl:value-of select="concat(substring($listeNumbers,1,1),translate(string($numero),'0123456789',substring($listeNumbers,2)))"/>
</xsl:function>

</xsl:stylesheet>
