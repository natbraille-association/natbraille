<!-- dans template mot -->

<!-- **************Mises en évidence ***********************-->
<!-- la variable memehauteur permet de savoir s'il faut considérer qu'on est dans le même mot ou pas pour les mev -->
<xsl:variable name="memeHauteur" as="xs:boolean"
	select="if (@hauteur) then (@hauteur=following-sibling::*[1]/@hauteur) else (empty(following-sibling::*[1]/@hauteur))"/>
<!--<xsl:message select="'mot ',text(),' : ',$memeHauteur" />-->
<!--<xsl:if test="@mev = ('1','2','3') and ($emph_w or $emph_part)">-->
	<!-- variable indiquant quel est le mot précédant susceptible d'être en évidence: utile dans des passage avec des maths intercallés -->
	<xsl:variable name="precMev" as="element()?" select="if(count(preceding-sibling::mot)=0) then ../preceding-sibling::lit[1]/mot[last()]
		else (preceding-sibling::mot[1])"/>
	<!-- variable indiquant si il faut ou non mettre en évidence le mot en fonction de ce qui précède 
	utile pour l'abréger qui découpe les mots sur apostrophe et tirets avant d'arriver ici -->
	<xsl:variable name="noMev" select="if(@mev = $precMev/@mev and $precMev/@doSpace='false') then true() else false()" as="xs:boolean"/>
	
	<xsl:if test="not(@hauteur and @mev=$precMev/@mev) and @mev = ('1','2','3') and
		($emph_w or $emph_part or $emph_mix ) and not($noMev)">
		<xsl:variable name="mevInWord" select="$memeHauteur and
			(($precMev[self::mot and @doSpace='false']) or (@doSpace='false' and following-sibling::*[1][self::mot]))" />
		<!-- on vérifie si on est dans un passage; c'ets le même genre d'algo que pour les majuscules -->
		<!--<xsl:message select="'mot ',text(),' ',$mevInWord" />-->
		<xsl:choose>
			<!-- applique-t-on la règle du passage ? -->
			<xsl:when test="not($emph_part) and (($emph_w and not($mevInWord)) or $emph_mix)"><xsl:value-of select="$emphPref"/></xsl:when>
			<xsl:when test="$emph_part"><!-- on applique la règle du passage -->
				<xsl:variable name="position1MotME">
					<xsl:call-template name="donnePosition1MotME">
						<xsl:with-param name="position" select="1" />
						<xsl:with-param name="prec" select="$precMev"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="positionDMotME">
					<xsl:call-template name="donnePositionDMotME">
						<xsl:with-param name="position" select="1" />
					</xsl:call-template>
				</xsl:variable>
				<xsl:choose>
					<!-- le mot est-il dans un passage en évidence? -->
					<!-- c'est le 1er et le dernier mot d'un passage en évidence -->
					<xsl:when test="$position1MotME ='1' and $positionDMotME = '1'">
						<!-- on n'est pas dans un passage en évidence -->
						<xsl:if test="$emph_w"><xsl:value-of select="$emphPref"/></xsl:if>
					</xsl:when>
					<xsl:when test="$position1MotME = '1'">
						<!-- y a-t-il au moins 3 autres mots suivants en évidence?-->
						<xsl:variable name="premierMotPassageME">
							<xsl:call-template name="estSuiviME">
								<xsl:with-param name="nb" select="3"/>
								<xsl:with-param name="position" select="1"/>
							</xsl:call-template>
						</xsl:variable>
						<xsl:choose>
							<xsl:when test="$premierMotPassageME = '1'"><xsl:value-of select="$emphPrefMulti"/></xsl:when>
							<xsl:otherwise>
								<xsl:if test="$emph_w"><xsl:value-of select="$emphPref"/></xsl:if>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<!--dernier mot -->
					<xsl:when test="$positionDMotME = '1'"><xsl:value-of select="$emphPref"/></xsl:when>
					<xsl:otherwise>
						<!-- ni le premier ni le dernier, est-ce un passage? -->
						<xsl:variable name="nbSuivant">
							<xsl:call-template name="estSuiviME">
								<xsl:with-param name="nb" select="2"/>
								<xsl:with-param name="position" select="1"/>
							</xsl:call-template>
						</xsl:variable>
						<xsl:variable name="nbPrec">
							<xsl:call-template name="estPrecedeME">
								<xsl:with-param name="nb" select="2"/>
								<xsl:with-param name="position" select="1"/>
							</xsl:call-template>
						</xsl:variable>
						<!-- passage pas en évidence -->
						<xsl:if test="$nbSuivant + $nbPrec = 0 and $emph_w"><xsl:value-of select="$emphPref"/></xsl:if>
						<!--<xsl:value-of select="concat($nbPrec,$nbSuivant)"/>-->
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
		</xsl:choose>
	</xsl:if>
	
	<!-- traitement du mot -->
	<!-- test pour fin de mise en évidence à l'intérieur d'un mot -->
	<xsl:if test="($emph_mix) and (@doSpace='false') and ($memeHauteur) and @mev=('1','2','3')
				 and following-sibling::*[1][self::mot] and not(following-sibling::*[1]/@mev = ('1','2','3'))">
		<xsl:text>&pt6;&pt3;</xsl:text>
	</xsl:if>
	
	<!-- FIN MEV-->
	
	<!-- numérique -->
	<!-- numérique (ou trait d'union) -->
	<!-- il y a des numériques avant et/ou après, ou il y a d'autres opérateurs mathématiques (+ ou = ) dans la phrase-->
	<xsl:when test="starts-with($deLigature,'-') and ((number(translate(string(preceding-sibling::*[1]),',0','.1')) or number(translate(string(following-sibling::*[1]),',0','.1'))) or contains(translate(string-join((preceding-sibling::*[position() &lt; 3],following-sibling::*[position() &lt; 3]),''),'+','='),'='))">
		<xsl:call-template name="numerique">
			<xsl:with-param name="mot" select="$deLigature" />
			<xsl:with-param name="position" select="1" />
			<xsl:with-param name="positionPrec" select="0" />
			<xsl:with-param name="PNS" select="3" />
		</xsl:call-template>
	</xsl:when>
	<xsl:when test="functx:contains-any-of($deLigature, $seqNum)">
		<xsl:call-template name="numerique">
			<xsl:with-param name="mot" select="$deLigature" />
			<xsl:with-param name="position" select="1" />
			<xsl:with-param name="positionPrec" select="0" />
			<xsl:with-param name="PNS" select="3" />
		</xsl:call-template>
	</xsl:when>
	
	<!--Majuscules-->
	<xsl:call-template name="majuscule">
		<xsl:with-param name="mot" select="$deLigature" />
		<xsl:with-param name="prefixe" select="0" />
		<xsl:with-param name="position" select="1" />
	</xsl:call-template>
	<xsl:call-template name="espace"/>
</xsl:otherwise>

<xsl:template name="numerique">
	<xsl:param name="mot" as="xs:string"/>
	<xsl:param name="positionPrec" as="xs:integer" select="-10"/>
	<xsl:param name="position" as="xs:integer" select="-10"/>
	<xsl:param name="PNS" as="xs:integer" select="-10"/>
	<xsl:param name="PNActif" as="xs:integer" select="-10"/>
	<xsl:choose>
		<xsl:when test="fn:matches(.,'^(\+|\*|:|=|×|÷)$') and functx:contains-any-of(..,('=','+','×','÷'))"><!-- conversion des , en . et des 0 en 1 car number(0) et number(NNN,NNN) renvoie faux -->
			<xsl:text>&pt6;</xsl:text>
			<xsl:call-template name="bijection">
				<xsl:with-param name="mot" select="$mot"/>
			</xsl:call-template>
			<xsl:call-template name="espace"/>
		</xsl:when>
		<!-- on n'a pas encore lu tout le mot -->
		<xsl:when test="$position &lt; string-length($mot) + 1">
			<xsl:choose>
				<!-- c'est un contenu numérique -->
				<!-- il n'y a plus que des chiffres -->
				<xsl:when test="string-length(translate($mot,$l_num,''))=0">
					<xsl:call-template name="numerique">
						<xsl:with-param name="mot" select="$mot" />
						<xsl:with-param name="position" select="string-length($mot) + 1" />
						<xsl:with-param name="positionPrec" select="1" />
						<xsl:with-param name="PNS" select="1"/>
						<xsl:with-param name="PNActif" select="$PNActif"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:when test="contains($l_num,substring($mot,$position,1))">
					<xsl:choose>
						<!-- il y a autre chose que des chiffres -->
						<!--
						<xsl:when test="(contains('+-×÷=',substring($mot,$position,1)) or $PNS=0) and not(number(translate(substring-before($mot,$position),',0&sup2;&sup3;','.111')))">
						-->
						<xsl:when test="$PNS=0">
							<xsl:if test="not($position = 1)">
								<!-- on sépare la chaine en deux avant le signe num -->
								<xsl:call-template name="majuscule">
									<xsl:with-param name="mot" select="substring($mot,1,$position -1)" />
									<xsl:with-param name="prefixe" select="0" />
									<xsl:with-param name="position" select="1" />
									<xsl:with-param name="PN" select="1"/>
								</xsl:call-template>
								<xsl:if test="not(contains('&sup2;&sup3;',substring($mot,$position,1)))"><xsl:value-of select="$numPref"/></xsl:if>
							</xsl:if>
							<xsl:choose>
								<!-- y a-t-il ambiguïté possible après? --><!-- ajouter test majuscule-->
								<xsl:when test="contains(substring(translate($mot,$l_amb,$l_amb_ahat),$position + 1,string-length($mot)-$position +1),'â')">
									<!-- on transcrit le numérique -->
									<xsl:call-template name="majuscule">
										<xsl:with-param name="mot" select="substring($mot,$position,1)" />
										<xsl:with-param name="prefixe" select="0" />
										<xsl:with-param name="position" select="1" />
										<xsl:with-param name="PN" select="1"/>
									</xsl:call-template>
									<xsl:call-template name="IVB">
										<xsl:with-param name="mot" select="substring($mot,$position + 1,string-length($mot)-$position +1)"/>
										<xsl:with-param name="position" select="1"/>
									</xsl:call-template>
								</xsl:when>
								<xsl:otherwise>
									<xsl:choose>
										<xsl:when test="contains(translate(substring($mot,1,$position),$l_maj,$l_maj_A),'A')"><!-- il y avait des majuscules avant -->
											<xsl:call-template name="majuscule">
												<xsl:with-param name="mot" select="substring($mot,$position, string-length($mot)-$position +1)" />
												<xsl:with-param name="prefixe" select="1" />
												<xsl:with-param name="position" select="1" />
												<xsl:with-param name="PN" select="1"/>
											</xsl:call-template>
										</xsl:when>
										<xsl:otherwise>
											<xsl:call-template name="majuscule">
												<xsl:with-param name="mot" select="substring($mot,$position, string-length($mot)-$position +1)" />
												<xsl:with-param name="prefixe" select="0" />
												<xsl:with-param name="position" select="1" />
												<xsl:with-param name="PN" select="1"/>
											</xsl:call-template>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:otherwise>
							</xsl:choose>
							<!-- espaces -->
							<xsl:call-template name="espace"/>
						</xsl:when>
						<!-- il n'y a que des chiffres et le mot est terminé -->
						<xsl:when test="$position = string-length($mot)"> <!--or number(translate(substring-after($mot,$position),',0&sup2;&sup3;','.111'))"> conversion des , en . et des 0, exp2 et 3 en 1 car number(0) et number(NNN,NNN) renvoie faux -->
							<xsl:call-template name="numerique">
								<xsl:with-param name="mot" select="$mot" />
								<xsl:with-param name="position" select="string-length($mot) + 1" />
								<xsl:with-param name="positionPrec" select="1" />
								<xsl:with-param name="PNS" select="1"/>
								<xsl:with-param name="PNActif" select="$PNActif"/>
							</xsl:call-template>
						</xsl:when>
						<!-- il n'y a que des chiffres pour l'instant -->
						<xsl:otherwise>
							<xsl:call-template name="numerique">
								<xsl:with-param name="mot" select="$mot" />
								<xsl:with-param name="position" select="$position + 1" />
								<xsl:with-param name="PNS" select="1"/>
								<xsl:with-param name="PNActif" select="$PNActif"/>
							</xsl:call-template>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:otherwise>
					<!-- le contenu n'est pas uniquement numérique ou pas numérique-->
					<xsl:variable name="precNum" as="xs:boolean" 
									 select="fn:matches(preceding-sibling::*[1],
									 if(preceding-sibling::*[1]/@doSpace='false') then '^(\+|-)|((\+|-)?\d+((\.|,)\d+)?)$' else ('^(\+|-)?\d+((\.|,)\d+)?$')) and
									 (functx:sequence-deep-equal((@hauteur),(preceding-sibling::*[1]/@hauteur)))"/>
					<xsl:choose>
						<!-- y avait-il un contenu numérique avant? -->
						<xsl:when test="$PNS = 1 and not($precNum)"><!-- oui -->
							<xsl:if test="not($PNActif = 1 or $precNum)"><xsl:value-of select="$numPref"/></xsl:if>
							<xsl:choose>
								<!-- faut-il utiliser l'indicateur de valeur de base pour la suite? -->
								<xsl:when test="contains(substring(translate($mot,$l_amb,$l_amb_ahat),$position,string-length($mot)-$position +1),'â')">
									<xsl:call-template name="majuscule">
										<xsl:with-param name="mot" select="substring($mot,1,$position - 1)" />
										<xsl:with-param name="prefixe" select="0" />
										<xsl:with-param name="position" select="1" />
										<xsl:with-param name="PN" select="1"/>
									</xsl:call-template>
									<xsl:call-template name="IVB">
										<xsl:with-param name="mot" select="substring($mot,$position,string-length($mot)-$position +1)"/>
										<xsl:with-param name="position" select="1"/>
									</xsl:call-template>
								</xsl:when>
								
								<xsl:otherwise>
									<xsl:call-template name="majuscule">
										<xsl:with-param name="mot" select="$mot" />
										<xsl:with-param name="prefixe" select="0" />
										<xsl:with-param name="position" select="1" />
										<xsl:with-param name="PN" select="1"/>
									</xsl:call-template>
									<!--espaces-->
									<xsl:call-template name="espace"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						<xsl:otherwise>
							<!-- il n'y avait pas de contenu numérique avant -->
							<xsl:choose>
								<!-- il peut y avoir des ambiguités -->
								<xsl:when test="contains(substring(translate($mot,$l_amb,$l_amb_ahat),1,$position),'â')">
									<xsl:call-template name="IVB">
										<xsl:with-param name="mot" select="$mot" />
										<xsl:with-param name="position" select="$position + 1" />
										<xsl:with-param name="PN" select="0"/>
									</xsl:call-template>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="numerique">
										<xsl:with-param name="mot" select="$mot" />
										<xsl:with-param name="position" select="$position + 1" />
										<xsl:with-param name="PNS" select="0"/>
										<xsl:with-param name="PNActif" select="$PNActif"/>
									</xsl:call-template>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		<!-- on a lu le mot et il ne contient que des chiffres -->
		<xsl:otherwise>
			<xsl:choose>
				<xsl:when test="$PNS=1">
					<xsl:variable name="motSuivNum" select="translate(following-sibling::*[1],',0&sup2;&sup3;','.111')" as="xs:string?"/>
					<!-- suppression également des <mot>-</mot> -->
					<xsl:variable name="precNum" as="xs:boolean" 
									 select="fn:matches(preceding-sibling::*[1],if(preceding-sibling::*[1]/@doSpace='false') then '^(\+|-)|((\+|-)?\d+((\.|,)\d+)?)$' else ('^(\+|-)?\d+((\.|,)\d+)?$'))"/>
					<xsl:choose>
						<xsl:when test="not($PNActif = 1 or
								($precNum and (functx:sequence-deep-equal((@hauteur),(preceding-sibling::*[1]/@hauteur)))))">
							<xsl:text>&pt6;</xsl:text>
							<xsl:call-template name="symbolesComposes">
								<xsl:with-param name="mot" select="$mot"/>
								<xsl:with-param name="PN" select="1"/>
							</xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
							<xsl:call-template name="symbolesComposes">
								<xsl:with-param name="mot" select="$mot"/>
								<xsl:with-param name="PN" select="$PNActif"/>
							</xsl:call-template>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:choose>
						<!-- on vérifie que le mot suivant ne commence pas par 3 nombres (cas d'un séparateur de milier + unités) OU n'est pas un nombre et que le mot ne contient pas des signes d'opération)-->
						<!-- functx:sequence-deep-equal renvoie true si les deux chaines sont égales ou vides ou séquence vide ; c'est le seul opérateur qui fait ça -->
						<xsl:when test="local-name(following-sibling::*[1])='mot' and
								(functx:sequence-deep-equal((@hauteur),(following-sibling::*[1]/@hauteur))) and
								(fn:matches(following-sibling::*[1],'^[0-9]{3}.*') or (number($motSuivNum) and not(starts-with($motSuivNum,'-')))) and
								number(translate($mot,',0&sup2;&sup3;','.111'))"><!-- conversion des , en . et des 0 en 1 car number(0) et number(NNN,NNN) renvoie faux -->
							<!--
							<xsl:call-template name="majuscule">
							<xsl:with-param name="mot" select="$mot" />
							<xsl:with-param name="prefixe" select="0" />
							<xsl:with-param name="position" select="1" />
							</xsl:call-template>-->
							
							<xsl:value-of select="$numSep"/>
						</xsl:when>
						<!-- le mot précédent n'est pas composé uniquement de 0123456789 -->
						
						<!-- espaces -->
						<!-- le suivant n'est pa sun nombre et lui-mème n'en est pas un -->
						<!--<xsl:if test="not(number($motSuivNum) and number(translate($mot,',0&sup2;&sup3;','.111')))">
						<xsl:call-template name="espace"/>
						</xsl:if>-->
						<xsl:otherwise>
							<xsl:call-template name="espace"/>
							<xsl:if test="number($motSuivNum) and starts-with($motSuivNum,'-')"><xsl:value-of select="$numPref"/></xsl:if>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="majuscule">
						<xsl:with-param name="mot" select="$mot" />
						<xsl:with-param name="prefixe" select="0" />
						<xsl:with-param name="position" select="1" />
					</xsl:call-template>
					<!-- espaces-->
					<xsl:call-template name="espace"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="IVB">
	<xsl:param name="mot" as="xs:string"/>
	<xsl:param name="position" as="xs:integer" select='0'/>
	<xsl:param name="PN" as="xs:integer" select='-10'/>
	<xsl:choose>
		<xsl:when test="$position = string-length($mot)">
			<xsl:if test="contains(translate($mot,$l_amb,$l_amb_ahat),'â')">
				<xsl:value-of select="$ivbPref"/>
				<xsl:call-template name="majuscule">
					<xsl:with-param name="mot" select="$mot" />
					<xsl:with-param name="prefixe" select="0" />
					<xsl:with-param name="position" select="1" />
					<xsl:with-param name="PN" select="0"/>
				</xsl:call-template>
				<xsl:call-template name="espace"/>
			</xsl:if>
		</xsl:when>
		<!-- on cherche le contenu numérique -->
		<xsl:when test="contains($l_num,substring($mot,$position,1))">
			<xsl:choose>
				<xsl:when  test="contains(substring(translate($mot,$l_amb,$l_amb_ahat),1,$position - 1),'â')">
					<xsl:if test="not($PN = 0)"><xsl:value-of select="$ivbPref"/></xsl:if>
					<xsl:call-template name="majuscule">
						<xsl:with-param name="mot" select="substring($mot,1,$position - 1)" />
						<xsl:with-param name="prefixe" select="0" />
						<xsl:with-param name="position" select="1" />
						<xsl:with-param name="PN" select="0"/>
					</xsl:call-template>
					<xsl:call-template name="numerique">
						<xsl:with-param name="mot" select="substring($mot,$position,string-length($mot) - $position + 1)" />
						<xsl:with-param name="position" select="1" />
						<xsl:with-param name="positionPrec" select="0" />
						<xsl:with-param name="PNS" select="3" />
						<xsl:with-param name="PNActif" select="0"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="majuscule">
						<xsl:with-param name="mot" select="substring($mot,1,$position - 1)" />
						<xsl:with-param name="prefixe" select="0" />
						<xsl:with-param name="position" select="1" />
						<xsl:with-param name="PN" select="1"/>
					</xsl:call-template>
					<xsl:if test="not($position = string-length($mot))">
						<xsl:call-template name="numerique">
							<xsl:with-param name="mot" select="substring($mot,$position,string-length($mot) - $position + 1)" />
							<xsl:with-param name="position" select="1" />
							<xsl:with-param name="positionPrec" select="0" />
							<xsl:with-param name="PNS" select="3" />
							<xsl:with-param name="PNActif" select="1"/>
						</xsl:call-template>
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		<xsl:otherwise>
			<xsl:call-template name="IVB">
				<xsl:with-param name="mot" select="$mot"/>
				<xsl:with-param name="position" select="$position + 1"/>
				<xsl:with-param name="PN" select="$PN"/>
			</xsl:call-template>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- ***************************************************
MAJUSCULES
*************************************************
-->

<xsl:template name="majuscule">
	<xsl:param name="mot" as="xs:string"/>
	<xsl:param name="prefixe" as="xs:integer" select='-10'/>
	<xsl:param name="position" as="xs:integer" select='-10'/>
	<xsl:param name="PN" as="xs:integer" select='-10'/>
	<xsl:param name="passage" select='1' as="xs:integer"/>
	<xsl:param name="ivb" select="true()" as="xs:boolean" tunnel="yes"/>
	
	<xsl:variable name="motPrefixe">
		<xsl:choose>
			<xsl:when test="not(functx:contains-any-of($mot,$seqMaj))">
				<xsl:call-template name="symbolesComposes">
					<xsl:with-param name="mot" select="$mot"/>
					<xsl:with-param name="PN" select="$PN"/>
				</xsl:call-template>
			</xsl:when>
			<!-- le mot ne contient pas de minuscules ni de chiffres, ni de point (cas d'un sigle) ou bien est potentiellement dans un passage et on prend en compte la règle du passage-->
			<!-- maj FRED 27 fév 2013 : ajout du cas particulier d'initiales d'un prénom : J.-C. avec fn:matches -->
			<xsl:when test="(not(functx:contains-any-of($mot,($seqMin,('.','°','&sup2;','&sup3;')))) or matches($mot,'^\w+\.-\w+$'))
						 and (not($passage = 0) and $cp_part)">
				<!--not(contains($mot,'abcdefghijklmnopqrstuvwxyzz'))">-->
				<xsl:choose>
					<!-- le mot ne contient pas de majuscules-->
					<xsl:when test="not(functx:contains-any-of($mot,$seqMaj))">
						<xsl:call-template name="symbolesComposes">
							<xsl:with-param name="mot" select="$mot"/>
							<xsl:with-param name="PN" select="$PN"/>
						</xsl:call-template>
					</xsl:when>
					<!-- le mot contient des majuscules mais on n'applique pas de règles complémentaires -
					<xsl:when test="$cp_2 = 0 and $cp_mix = 0">
					<xsl:if test="starts-with(translate($mot,'ABCDEFGHIJKLMNOPQRSTUVWXYZÀÂÉÈÊËÎÏÔÙÛÜ&Ccedil;&AElig;&OElig;','AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'),'A')">
					<xsl:text>&pt46;</xsl:text>
					</xsl:if>
					<xsl:call-template name="symbolesComposes">
					<xsl:with-param name="mot" select="$mot"/>
					<xsl:with-param name="PN" select="$PN"/>
					</xsl:call-template>
					</xsl:when>-->
					<xsl:otherwise>
						<!-- on va utiliser une variable pour savoir si c'est le premier mot en majuscule d'un passage -->
						<xsl:variable name="position1MotMaj">
							<xsl:call-template name="donnePosition1MotMaj">
								<xsl:with-param name="position" select="1" />
							</xsl:call-template>
						</xsl:variable>
						<xsl:variable name="positionDMotMaj">
							<xsl:call-template name="donnePositionDMotMaj">
								<xsl:with-param name="position" select="1" />
								<xsl:with-param name="doSpaceAPrendreEnCompte" select="@doSpace='false'" tunnel="yes"/>
							</xsl:call-template>
						</xsl:variable>
						<!--<xsl:value-of select="$position1MotMaj,$positionDMotMaj" />-->
						<!--<xsl:value-of select="trace(concat($position1MotMaj,' ',$positionDMotMaj),'position 1 / fin:)')"/>-->
						<xsl:choose>
							<!-- le mot est-il dans un passage en majuscule? -->
							<!-- c'est le 1er mot d'un passage en majuscule -->
							<xsl:when test="$position1MotMaj ='1' and $positionDMotMaj = '1'">
								<!-- on n'est pas dans un passage en majuscule -->
								<xsl:call-template name="majuscule">
									<xsl:with-param name="mot" select="$mot" />
									<xsl:with-param name="prefixe" select="$prefixe" />
									<xsl:with-param name="position" select="$position" />
									<xsl:with-param name="PN" select="$PN"/>
									<xsl:with-param name="passage" select="0"/>
								</xsl:call-template>
							</xsl:when>
							<xsl:when test="$position1MotMaj = '1'">
								<!-- y a-t-il au moins 3 autres mots suivants en majuscules?-->
								<xsl:variable name="premierMotPassageMaj">
									<xsl:call-template name="estSuiviMaj">
										<xsl:with-param name="nb" select="3"/>
										<xsl:with-param name="position" select="1"/>
									</xsl:call-template>
								</xsl:variable>
								<xsl:choose>
									<xsl:when test="$premierMotPassageMaj = '1'">
										<xsl:if test="not(preceding::*[1][self::mot and @doSpace='false'])">
											<!-- si le précédent mot a dospace false, on est au milieu d'un mot donc pas de préfixe -->
											<xsl:value-of select="$capPrefMulti"/>
										</xsl:if>
										<xsl:call-template name="symbolesComposes">
											<xsl:with-param name="mot" select="$mot"/>
											<xsl:with-param name="PN" select="$PN"/>
										</xsl:call-template>
									</xsl:when>
									<xsl:otherwise>
										<xsl:call-template name="majuscule">
											<xsl:with-param name="mot" select="$mot" />
											<xsl:with-param name="prefixe" select="$prefixe" />
											<xsl:with-param name="position" select="$position" />
											<xsl:with-param name="PN" select="$PN"/>
											<xsl:with-param name="passage" select="0"/>
										</xsl:call-template>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:when>
							<xsl:when test="$positionDMotMaj = '1'">
								<!-- dernier mot -->
								<xsl:variable name="dernierMotPassageMaj">
									<xsl:call-template name="estPrecedeMaj">
										<xsl:with-param name="nb" select="3"/>
										<xsl:with-param name="position" select="1"/>
									</xsl:call-template>
								</xsl:variable>
								<xsl:choose>
									<xsl:when test="$dernierMotPassageMaj = '1'">
										<xsl:if test="not(preceding::*[1][self::mot and @doSpace='false'])">
											<!-- si le précédent mot a dospace false, on est au milieu d'un mot donc pas de préfixe -->
											<xsl:value-of select="$capPref"/>
										</xsl:if>
										<xsl:call-template name="symbolesComposes">
											<xsl:with-param name="mot" select="$mot"/>
											<xsl:with-param name="PN" select="$PN"/>
										</xsl:call-template>
									</xsl:when>
									<xsl:otherwise>
										<xsl:call-template name="majuscule">
											<xsl:with-param name="mot" select="$mot" />
											<xsl:with-param name="prefixe" select="$prefixe" />
											<xsl:with-param name="position" select="$position" />
											<xsl:with-param name="PN" select="$PN"/>
											<xsl:with-param name="passage" select="0"/>
										</xsl:call-template>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:when>
							<!--
							<xsl:when test="$position1MotMaj = '3'">
							<xsl:text>2ème</xsl:text>
							<xsl:call-template name="symbolesComposes">
							<xsl:with-param name="mot" select="$mot"/>
							<xsl:with-param name="PN" select="$PN"/>
							</xsl:call-template>
							</xsl:when>-->
							<xsl:otherwise>
								<!-- ni le premier ni le dernier, est-ce un passage? -->
								<xsl:variable name="nbSuivant">
									<xsl:call-template name="estSuiviMaj">
										<xsl:with-param name="nb" select="2"/>
										<xsl:with-param name="position" select="1"/>
									</xsl:call-template>
								</xsl:variable>
								<xsl:variable name="nbPrec">
									<xsl:call-template name="estPrecedeMaj">
										<xsl:with-param name="nb" select="2"/>
										<xsl:with-param name="position" select="1"/>
									</xsl:call-template>
								</xsl:variable>
								<xsl:choose>
									<xsl:when test="$nbSuivant + $nbPrec = 0">
										<!-- passage pas en majuscule -->
										<xsl:call-template name="majuscule">
											<xsl:with-param name="mot" select="$mot" />
											<xsl:with-param name="prefixe" select="$prefixe" />
											<xsl:with-param name="position" select="$position" />
											<xsl:with-param name="PN" select="$PN"/>
											<xsl:with-param name="passage" select="0"/>
										</xsl:call-template>
									</xsl:when>
									<xsl:otherwise>	
										<xsl:call-template name="symbolesComposes">
											<xsl:with-param name="mot" select="$mot"/>
											<xsl:with-param name="PN" select="$PN"/>
										</xsl:call-template>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<!-- traitement d'un mot contenant des majuscules mais pas dans un passage -->
				<xsl:choose>
					<!-- le mot contient des majuscules mais on n'applique pas de règles complémentaires -->
					<xsl:when test="not($cp_2) and not($cp_mix)">
						<xsl:if test="nat:starts-with-any-of($mot,$seqMaj)"><xsl:value-of select="$capPref"/></xsl:if>
						<!-- cas des mots en majuscules avec trait d'union ou apostrophe-->
						<xsl:choose>
							<!-- le mot est entièrement en majusucle et contient au moins un trait d'union -->
							<xsl:when test="(contains($mot,'-') or contains($mot,'&#8209;')) and not(functx:contains-any-of($mot,($seqMin,'.','°')))">
								<xsl:call-template name="symbolesComposes">
									<xsl:with-param name="mot" select="substring-before($mot,'-')"/>
									<xsl:with-param name="PN" select="$PN"/>
								</xsl:call-template>
								<xsl:value-of select="$hyphenBr"/>
								<xsl:call-template name="majuscule">
									<xsl:with-param name="mot" select="substring-after($mot,'-')"/>
									<xsl:with-param name="PN" select="$PN"/>
								</xsl:call-template>
							</xsl:when>
							<xsl:when test="(contains($mot,$apos) or contains ($mot,'’')) and not(functx:contains-any-of($mot,($seqMin,'.','°')))">
								<!--<xsl:text>**nikX**</xsl:text>-->
								<xsl:call-template name="symbolesComposes">
									<xsl:with-param name="mot" select="functx:substring-before-match($mot,concat('[',$apos,'’]'))"/>
									<xsl:with-param name="PN" select="$PN"/>
								</xsl:call-template>
								<xsl:value-of select="$aposBr"/>
								<xsl:call-template name="majuscule">
									<xsl:with-param name="mot" select="functx:substring-after-match($mot,concat('[',$apos,'’]'))"/>
									<xsl:with-param name="PN" select="$PN"/>
								</xsl:call-template>
							</xsl:when>						
							<xsl:otherwise>
								<!--<xsl:text>**nikY**</xsl:text>-->
								<xsl:call-template name="symbolesComposes">
									<xsl:with-param name="mot" select="$mot"/>
									<xsl:with-param name="PN" select="$PN"/>
								</xsl:call-template>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:when test="not(functx:contains-any-of($mot,($seqMin,'.','°')))">
						<!-- le mot est entièrement en majuscule et contient au minimum une majuscule-->
						<xsl:choose>
							<!-- le mot commence par un numérique -->
							<xsl:when test="nat:starts-with-any-of($mot,$seqDigit)"><!--contains(translate(substring($mot, 1, 1), '0123456789', '1111111111'),'1')">-->
								<xsl:call-template name="symbolesComposes">
									<xsl:with-param name="mot" select="substring($mot, 1, 1)"/>
									<xsl:with-param name="PN" select="$PN"/>
								</xsl:call-template>
								<xsl:choose>
									<!-- doit-on appliquer les règles de contenu mixte -->
									<xsl:when test="$cp_mix">
										<xsl:call-template name="majuscule">
											<xsl:with-param name="mot" select="substring($mot, 2, string-length($mot) - 1)" />
											<xsl:with-param name="prefixe" select="0" />
											<xsl:with-param name="position" select="1" />
											<xsl:with-param name="PN" select="$PN"/>
											<xsl:with-param name="passage" select="0"/>
										</xsl:call-template>
									</xsl:when>
									<xsl:otherwise>
										<xsl:call-template name="symbolesComposes">
											<xsl:with-param name="mot" select="substring($mot, 2, string-length($mot) - 1)"/>
											<xsl:with-param name="PN" select="$PN"/>
										</xsl:call-template>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:when>
							<xsl:otherwise>
								<xsl:variable name="prefMaj">
									<xsl:if test="contains(translate($mot,$l_maj,$l_maj_A),'A') and not($prefixe= 1) 
								 and not(preceding-sibling::mot[1][@doSpace='false'] and upper-case(preceding-sibling::mot[1]) = preceding-sibling::mot[1]) ">
										<!-- on vérifie AUSSI dans ce qui suit qu'on a pas une lettre suivie d'un trait d'union (Sans-Merci en abrégé par exemple)-->
										<xsl:if test="(not(string-length(translate($mot, $l_maj,'')) = string-length($mot) - 1) or
									(@doSpace='false' and upper-case(following-sibling::mot[1]) = following-sibling::mot[1]) and not(following-sibling::*='-')) and $cp_2"><!-- il y a plus d'une majuscule-->
											<!--<xsl:value-of select="translate($mot, $l_maj,'')"/>-->
											<xsl:value-of select="$capPref"/>
										</xsl:if>
										<xsl:value-of select="$capPref"/>
									</xsl:if>
								</xsl:variable>
								<xsl:value-of select="$prefMaj"/>
								<xsl:call-template name="symbolesComposes">
									<xsl:with-param name="mot" select="$mot"/>
									<xsl:with-param name="PN" select="$PN"/>
									<xsl:with-param name="ivb" select="$prefMaj = '' and not($ivbMajSeule)" tunnel="yes"/>
								</xsl:call-template>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:when test="$position > string-length($mot)">
						<!-- c'est fini: on fait rien -->
						<!--<xsl:if test="$prefixe = 0">
						<xsl:text>&pt46;</xsl:text>
						<xsl:if test="string-length(.) > 1">
						<xsl:text>&pt46;</xsl:text>
						</xsl:if>
						<xsl:call-template name="symbolesComposes">
						<xsl:with-param name="mot" select="."/>
						<xsl:with-param name="PN" select="$PN"/>
						</xsl:call-template>
						</xsl:if>-->
						</xsl:when>
						<xsl:when test="$prefixe = 1">
							<xsl:if test="contains($l_maj,substring($mot,$position,1))"><xsl:value-of select="$capPref"/></xsl:if>
							<xsl:choose>
								<xsl:when test="($position= 1 or $position=string-length($mot)) and substring($mot,$position,1)='-'">
									<xsl:call-template name="symbolesComposes">
										<xsl:with-param name="mot" select="substring($mot,$position,1)"/>
										<xsl:with-param name="PN" select="$PN"/>
										<xsl:with-param name="tiret" select="if (matches(substring($mot,2,1),'\d')) then 1 else 2"/>
										<xsl:with-param name="ivb" select="false()" tunnel="yes"/>
									</xsl:call-template>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="symbolesComposes">
										<xsl:with-param name="mot" select="substring($mot,$position,1)"/>
										<xsl:with-param name="PN" select="$PN"/>
										<xsl:with-param name="ivb" select="false()" tunnel="yes"/>
									</xsl:call-template>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:call-template name="majuscule">
								<xsl:with-param name="mot" select="$mot" />
								<xsl:with-param name="prefixe" select="1" />
								<xsl:with-param name="position" select="$position + 1" />
								<xsl:with-param name="PN" select="$PN"/>
								<xsl:with-param name="passage" select="0"/>
							</xsl:call-template>
						</xsl:when>
						<xsl:when test="not(functx:contains-any-of($mot,($seqMin,'.','°')))">
							<xsl:call-template name="majuscule">
								<xsl:with-param name="mot" select="$mot" />
								<xsl:with-param name="prefixe" select="0" />
								<xsl:with-param name="position" select="$position + 1" />
								<xsl:with-param name="PN" select="$PN"/>
								<xsl:with-param name="passage" select="0"/>
							</xsl:call-template>
						</xsl:when>
						<xsl:when test="not($cp_mix)">
							<xsl:if test="nat:starts-with-any-of($mot,$seqMaj)"><xsl:value-of select="$capPref"/></xsl:if>
							<xsl:call-template name="symbolesComposes">
								<xsl:with-param name="mot" select="$mot"/>
								<xsl:with-param name="PN" select="$PN"/>
							</xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
							<!---->
							<!-- on recommence au début du mot et cette fois on préfixe toutes les majuscules -->
							<!---->
							<!--découpage sur tiret -->
							<xsl:variable name="decoup" as="xs:string*" select="if($cp_mix and not(contains($mot,'.-'))) then tokenize($mot,'-') else $mot"/>
							<!-- application des préfixes -->
							<xsl:variable name="resu" as="xs:string*">
								<xsl:for-each select="$decoup">
									<!--
									<xsl:variable name="end" select="if(matches(.,concat('^.*([',$l_maj,']([',$l_maj,']+))$'))) then replace(.,concat('^.*([',$l_maj,']([',$l_maj,']+))$'),concat($capPref,$capPref,'$1')) else ''"/>
									<xsl:variable name="deb" select="functx:substring-before-match(.,concat($capPref,$capPref))"/>
									<xsl:value-of select="concat(replace($deb,concat('([',$l_maj,'])'),concat($capPref,'$1')),$end)"/>
									-->
									<xsl:choose>
										<!-- tout en maj -->
										<!-- TODO: n'est appliqué que sur la dernière partie du mot pour simplifier 
										ne fonctionnera donc pas pour arc-EN-CIEL (le double pref commencera à ARC)
										-->
										<xsl:when test="position()=last() and matches(.,concat('^[',$l_maj,']+$'))">
											<xsl:value-of select="concat($capPref,$capPref,.)"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:variable name="end" select="replace(.,concat('^.*[^',$l_maj,']([',$l_maj,']{2,})$'),'$1')" as="xs:string"/>
											<xsl:variable name="deb" select="replace(if($end=.) then . else substring-before(.,$end), concat('([',$l_maj,'])'),concat($capPref,'$1'))"/>
											<xsl:value-of select="if($end=.) then $deb else concat($deb,$capPref,$capPref,$end)"/>
										</xsl:otherwise>
									</xsl:choose>
									<xsl:if test="not(position()=last())"><xsl:value-of select="$hyphenBr"/></xsl:if>
								</xsl:for-each>
							</xsl:variable>
							<xsl:call-template name="symbolesComposes">
								<xsl:with-param name="mot" select="string-join($resu,'')"/>
								<xsl:with-param name="PN" select="$PN"/>
							</xsl:call-template>
							<!--<xsl:call-template name="symbolesComposes">
							<xsl:with-param name="mot" select="."/>
							<xsl:with-param name="PN" select="$PN"/>
							</xsl:call-template>-->
							<!--<xsl:choose>
							<xsl:when test="contains($mot,'-') and not(starts-with($mot,'-')) and not(substring($mot,string-length($mot),1)='-') and $cp_mix">
							<xsl:call-template name="majuscule">
							<xsl:with-param name="mot" select="substring-before($mot, '-')" />
							<xsl:with-param name="PN" select="$PN"/>
							<xsl:with-param name="position" select="1" />
							<xsl:with-param name="passage" select="0"/>
							<xsl:with-param name="prefixe" select="0" />
							</xsl:call-template>
							<xsl:text>&pt36;</xsl:text>
							<xsl:call-template name="majuscule">
							<xsl:with-param name="mot" select="substring-after($mot, '-')" />
							<xsl:with-param name="PN" select="$PN"/>
							<xsl:with-param name="passage" select="0"/>
							<xsl:with-param name="position" select="1" />
							<xsl:with-param name="prefixe" select="0" />
							</xsl:call-template>
							</xsl:when>-->
						</xsl:otherwise>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<!-- TODO bourrin: permuter ivb et majuscule à terme -->
		<xsl:value-of select="replace($motPrefixe,'^(&pt46;+|&pt25;&pt46;)&pt56;','&pt56;$1')"/>
	</xsl:template>
