<?xml version="1.1" encoding="UTF-8" ?>
<!--
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Blanchard, Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
--> 
<!-- version 2.0 -->

<!DOCTYPE xsl:stylesheet SYSTEM "nat://system/xsl/mmlents/windob.dtd">

<xsl:stylesheet version="3.0"
xmlns:xsl='http://www.w3.org/1999/XSL/Transform' 
xmlns:saxon='http://icl.com/saxon'
xmlns:xs='http://www.w3.org/2001/XMLSchema'
xmlns:fn='http://www.w3.org/2005/xpath-functions'
xmlns:fo='http://www.w3.org/1999/XSL/Format'
xmlns:m='http://www.w3.org/1998/Math/MathML'
xmlns:functx='http://www.functx.com'
xmlns:lit='espacelit'
xmlns:doc='espaceDoc'
xmlns:nat='http://natbraille.free.fr/xsl'>

<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
<xsl:include href="nat://system/xsl/fr-commun.xsl"/>
<xsl:include href="nat://system/xsl/ressources/liste-fr-g2.xsl"/>

<!-- en faire une option -->
<xsl:variable name="nAbrnAbr" select="true()" as="xs:boolean"/>

<!-- this variable is used to get ambiguity BUT is defined in xsl.xsl-->
<!--<xsl:variable name="blackToBraille" select="true()" as="xs:boolean"/>-->
<!--<xsl:variable name="l_noms_propres" as="xs:string*" select="()"/>-->

<!-- (in|pré|sub)?consc| inutile pour conscient: symb fond-->
<!--convient X
déconvient X
expédient X
émollient X
obvient X
réexpédient X-->

<xsl:template match="lit">
	<xsl:message>
		-------------------------------------------------------------------------------------
		<xsl:copy-of select="."/>
		-------------------------------------------------------------------------------------
	</xsl:message>
	<xsl:variable name="laPhrase" as="element()?">
		<xsl:choose>
			<xsl:when test="(local-name(parent::*) = 'titre' and (../@niveauOrig &lt; $minTitleAbr)) or ../@abrege='false'">
				<xsl:if test="$dbg"><xsl:message select="'** La phrase ne doit pas être abrégée'"/></xsl:if>
				<xsl:copy copy-namespaces="no">
					<xsl:attribute name="abreger" select="'false'"/>
					<xsl:copy-of select="@*"/>
					<xsl:copy-of copy-namespaces="no" select="*|text()"/>
				</xsl:copy>
			</xsl:when>
			<xsl:when test="*">
				<xsl:variable name="phrMEF" as="element()">
					<xsl:copy copy-namespaces="no">
						<xsl:for-each select="*">
							<xsl:choose>
								<xsl:when test="local-name(.) = 'mot' and string(.)=''"/>
								<xsl:when test="local-name(.) = 'mot'">
									<xsl:variable name="attr" select="@*"/>
									<xsl:variable as="xs:string" name="min" select="lower-case(.)"/>
									<!-- vrai si le mot appartient à une locution à ne pas abréger-->
									<xsl:variable name="locNAbr" as="xs:boolean">
										<!-- liste des locutions pouvant poser problème -->
<!-- <xsl:message select="('noeud1:',.)"/> -->
										<xsl:variable name="lPossibleLoc" as="xs:string*" select="for $lc in ($loc2,$loc3) return (if (contains($lc,$min) and functx:is-value-in-sequence($lc,$nAbr)) then $lc else ())"/>
										<xsl:variable name="currentAndPrec" select="concat(lower-case(./preceding-sibling::*[1]),' ',$min)"/>
										<xsl:variable name="currentAndFol" select="concat($min,' ',lower-case(./following-sibling::*[1]))"/>
										<xsl:value-of select="some $m in $lPossibleLoc satisfies (contains($currentAndPrec,$m) or contains($currentAndFol,$m))"/>
									</xsl:variable>
									<!-- vrai si le mot fait partie d'une locution-->
									<xsl:variable name="isLoc" as="xs:boolean">
										<xsl:variable name="lPossibleLoc" as="xs:string*" select="for $lc in ($loc2,$loc3) return (if (contains($lc,$min)) then $lc else ())"/>
										<xsl:variable name="currentAndPrec" select="concat(lower-case(./preceding-sibling::*[1]),' ',$min)"/>
										<xsl:variable name="currentAndFol" select="concat($min,' ',lower-case(./following-sibling::*[1]))"/>
										<xsl:value-of select="some $m in $lPossibleLoc satisfies (contains($currentAndPrec,$m) or contains($currentAndFol,$m))"/>
<!-- <xsl:message select="('curP and curFol',$currentAndPrec,';',$currentAndFol)"/> -->
									</xsl:variable>
<!-- <xsl:message select="('1:',$min,':isLoc', $isLoc, 'locNAbr',$locNAbr)"/> -->
									<xsl:variable as="element()*" name="decoupApos">
										<xsl:choose>
											<!-- mot à ne pas abréger -->
											<!-- vgu 2024.04.30 - url regexp causes net.sf.saxon.trans.XPathException Regex backtracking limit exceeded processing -->
											<!--                  so remove this match -->
											<!-- <xsl:when test="(@abrege='false') or functx:is-value-in-sequence(.,$l_noms_propres) or (functx:is-value-in-sequence(lower-case(.),$nAbr) and not($isLoc)) or -->
											<!--	matches(.,'^((((ftp|http|https)://){0,1}([A-Za-z0-9-\._]+\.)+[A-Za-z]{2,})|(([a-zA-Z0-9]+(([\.-_]?[a-zA-Z0-9]+)+)?)@(([a-zA-Z0-9]+[\._-])+[a-zA-Z]{2,4})))$') -->
											<!--	or $locNAbr"> -->
											<xsl:when test="(@abrege='false') or functx:is-value-in-sequence(.,$l_noms_propres) or (functx:is-value-in-sequence(lower-case(.),$nAbr) and not($isLoc))
												or $locNAbr">
												<xsl:for-each select="tokenize(.,'-')">
													<xsl:element name="mot" inherit-namespaces="no">
														<xsl:if test="position() &lt; last()">
															<xsl:attribute name="doSpace" select="'false'"/>
														</xsl:if>
														<xsl:copy-of select="$attr"/>
														<xsl:attribute name="abreger" select="'false'"/>
														<xsl:value-of select="."/>
													</xsl:element>
													<xsl:if test="not(position() = last())">
														<xsl:element name="mot" inherit-namespaces="no">
															<xsl:attribute name="doSpace" select="'false'"/>
															<xsl:copy-of select="$attr"/>
															<xsl:attribute name="abreger" select="'false'"/>
															<xsl:value-of select="'-'"/>
														</xsl:element>
													</xsl:if>
												</xsl:for-each>
											</xsl:when>
											<!-- gestion des apostrophes -->
											<xsl:when test="contains(.,'''') and not(number(translate(.,''',.','')) or index-of($motsTiret, lower-case(.))>0)">
												<xsl:variable name="splitApos" select="tokenize(.,'''')" as="xs:string*"/>
												<xsl:for-each select="$splitApos">
													<xsl:element name="mot" inherit-namespaces="no">
														<xsl:copy-of select="$attr"/>
														<xsl:if test="not(position()=last())">
															<xsl:attribute name="doSpace" select="'false'"/>
														</xsl:if>
														<xsl:value-of select="if(position()=last()) then replace(.,'\$','&pt45;&pt234;') else (concat(replace(.,'\$','&pt45;&pt234;'),''''))"/>
													</xsl:element>
												</xsl:for-each>
											</xsl:when>
											<xsl:when test="starts-with(.,'$')">
												<xsl:element name="mot" inherit-namespaces="no">
													<xsl:copy-of select="$attr"/>
													<xsl:value-of select="replace(.,'\$','&pt45;&pt234;')"/>
												</xsl:element>
											</xsl:when>
											<xsl:otherwise>
												<xsl:copy-of select="." copy-namespaces="no"/>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:variable>
									<!-- gestion des tirets -->
<!-- <xsl:message select="('decApos:',$decoupApos)"/> -->
									<xsl:variable name="svt" select="lower-case(following-sibling::*[1])"/>
									<xsl:for-each select="$decoupApos">
										<xsl:variable name="min2" select="lower-case(.)" as="xs:string?"/>
										<!-- vrai si le mot appartient à une locution à ne pas abréger-->
										<xsl:variable name="locNAbr2" as="xs:boolean">
											<!-- liste des locutions pouvant poser problème -->
<!-- <xsl:message select="('prec,mot2,fol',./preceding-sibling::*[1],.,following-sibling::*[1])"/> -->
											<xsl:variable name="lPossibleLoc" as="xs:string*" select="for $lc in ($loc2,$loc3) return (if (contains($lc,$min2) and functx:is-value-in-sequence($lc,$nAbr)) then $lc else ())"/>
											<xsl:variable name="currentAndPrec" select="concat(lower-case(./preceding-sibling::*[1]),' ',$min2)"/>
											<xsl:variable name="currentAndFol" select="concat($min2,' ',lower-case(./following-sibling::*[1]),$svt)"/>
											<xsl:value-of select="some $m in $lPossibleLoc satisfies ($m=$currentAndPrec or $m=$currentAndFol)"/>
										</xsl:variable>
										<!-- vrai si le mot fait partie d'une locution-->
										<xsl:variable name="isLoc2" as="xs:boolean">
											<xsl:variable name="lPossibleLoc" as="xs:string*" select="for $lc in ($loc2,$loc3) return (if (contains($lc,$min2)) then $lc else ())"/>
											<xsl:variable name="currentAndPrec" select="concat(lower-case(./preceding-sibling::*[1]),' ',$min2)"/>
											<xsl:variable name="currentAndFol" select="concat($min2,' ',lower-case(./following-sibling::*[1]),$svt)"/>
											<xsl:value-of select="some $m in $lPossibleLoc satisfies ($m=$currentAndPrec or $m=$currentAndFol)"/>
<!-- <xsl:message select="('2:',$currentAndPrec,';',$currentAndFol)"/> -->
										</xsl:variable>
<!-- <xsl:message select="('2:',.,':isLoc', $isLoc2, 'locNAbr',$locNAbr2)"/> -->
										<xsl:variable name="attr" select="@*"/>
										<xsl:variable name="ambNP" select="functx:is-value-in-sequence(.,$l_noms_propres_amb)" as="xs:boolean"/>
										<xsl:variable name="ambIVB" select="functx:is-value-in-sequence(lower-case(.),$l_ivb_amb)" as="xs:boolean"/>
										<xsl:choose>
											<!-- ne pas abréger -->
											<xsl:when test="functx:is-value-in-sequence(.,($l_noms_propres,$nAbr)) and not($isLoc) and not($isLoc2) or $locNAbr2">
												<xsl:element name="mot" inherit-namespaces="no">
													<xsl:copy-of select="$attr"/>
													<xsl:attribute name="abreger" select="'false'"/>
													<xsl:value-of select="."/>
												</xsl:element>
											</xsl:when>
											<xsl:when test=".='-'">
												<xsl:copy-of select="."/>
											</xsl:when>
											<xsl:when test="contains(.,'-') and not(index-of($motsTiret, lower-case(.))>0)">
												<xsl:variable name="splitTiret" as="xs:string*" select="tokenize(.,'-')"/>
												<xsl:if test="starts-with(.,'-') and not(matches(substring(.,2,1),'\d'))">
													<xsl:element name="ponctuation" inherit-namespaces="no">
														<xsl:copy-of select="$attr"/>
														<xsl:attribute name="doSpace" select="'false'"/>
														<xsl:value-of select="'-'"/>
													</xsl:element>
												</xsl:if>
												<xsl:variable name="prefNum" select="if(starts-with(.,'-') and matches(substring(.,2,1),'\d')) then '-' else ()"/>
												<!--<xsl:message select="('prefixe tiret:',$prefNum,'courant:',.)"/>-->
												<xsl:variable name="space" select="if(./@doSpace) then ./@doSpace else 'true'"/>
												<!--<xsl:message select="'liste:'"/>-->
												<xsl:for-each select="if (starts-with(.,'-')) then $splitTiret[position() &gt;1 and not(position()=last() and .='')]
													else $splitTiret[not(position()=last() and .='')]">
													<!--<xsl:message select="('le mot courant:',.)"/>-->
													<xsl:element name="mot" inherit-namespaces="no">
														<xsl:copy-of select="$attr"/>
														<xsl:if test="not($space='true' and position() = count($splitTiret[not(position()=1 and .='')]))">
															<xsl:attribute name="doSpace" select="'false'"/>
														</xsl:if>
														<xsl:if test="$ambNP">
															<xsl:attribute name="ambNP" select="."/>
														</xsl:if>
														<xsl:if test="$ambIVB">
															<xsl:attribute name="ambIVB" select="."/>
														</xsl:if>
														<xsl:value-of select="if (position()=1) then concat($prefNum,.) else ."/>
													</xsl:element>
													<xsl:if test="not(position()=last())">
														<xsl:element name="mot" inherit-namespaces="no">
															<xsl:copy-of select="$attr"/>
															<xsl:attribute name="doSpace" select="'false'"/>
															<xsl:value-of select="'-'"/>
														</xsl:element>
													</xsl:if>
												</xsl:for-each>
												<!-- dernier élément (vide si le mot se fini par '-' -->
												<xsl:if test="ends-with(.,'-')">
													<xsl:element name="ponctuation" inherit-namespaces="no">
														<xsl:copy-of select="$attr"/>
														<xsl:value-of select="'-'"/>
													</xsl:element>
												</xsl:if>
											</xsl:when>
											<xsl:otherwise>
												<xsl:element name="mot" inherit-namespaces="no" >
													<xsl:if test="$ambNP">
														<xsl:attribute name="ambNP" select="."/>
													</xsl:if>
													<xsl:if test="$ambIVB">
														<xsl:attribute name="ambIVB" select="."/>
													</xsl:if>
													<xsl:copy-of select="$attr"/>
													<xsl:value-of select="."/>
												</xsl:element>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:for-each>
								</xsl:when>
								<!--<xsl:when test="local-name(.)='ponctuation' and .='-' and count(preceding-sibling::ponctuation[.='-']) mod 2 =1 ">
									<xsl:copy-of select="." copy-namespaces="no"/>
								</xsl:when>-->
								<xsl:when test="local-name(.)='ponctuation' and contains('«“‘&lsquo;(',.)">
									<xsl:variable name="attr" select="./@*"/>
									<xsl:element name="ponctuation" inherit-namespaces="no">
										<xsl:copy-of select="$attr"/>
										<xsl:attribute name="doSpace" select="'false'"/>
										<xsl:value-of select="."/>
									</xsl:element>
									<!--<xsl:message select="'ponctuation ouvrante'"/>-->
								</xsl:when>
								<xsl:otherwise>
									<xsl:copy-of select="." copy-namespaces="no"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</xsl:copy>
				</xsl:variable>
				<!--<xsl:message select="'PHRASE MEP:',$phrMEF"/>-->
				<xsl:copy copy-namespaces="no">
					<xsl:call-template name="litPass1Abr">
						<xsl:with-param name="noeud" select="$phrMEF/child::*[1]"/>
					</xsl:call-template>
				</xsl:copy>
			</xsl:when>
			<xsl:otherwise></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:if test="$dbg">
	<xsl:message select="'PHRASE AVANT PASS 2:',$laPhrase"/>
	</xsl:if>
	<xsl:if test="string($laPhrase)!=''">
		<xsl:apply-templates select="$laPhrase" mode="pass2"/>
		<xsl:call-template name="espace"/>
        </xsl:if>
        
</xsl:template>

<xsl:template match="lit" mode="pass2">
	<!--<xsl:message select="('phrase dans lit pass2',.)"/>-->
	<xsl:apply-templates select="*|text()|processing-instruction()" />
</xsl:template>

<xsl:template name="litPass1Abr">
	<xsl:param name="noeud" as="element()?"/>
	<xsl:variable name="noeudMin" select="fn:lower-case($noeud)"/>
	<xsl:variable name="noeudMaj" select="fn:upper-case($noeud)"/>
	<!--<xsl:message select="$noeud"/>-->
	<xsl:choose>
		<!-- noeud vide -->
		<xsl:when test="not($noeud/node())">
			<xsl:if test="$noeud/following-sibling::*[1]">
				<xsl:call-template name="litPass1Abr">
					<xsl:with-param name="noeud" select="$noeud/following-sibling::*[1] "/>
				</xsl:call-template>
			</xsl:if>
		</xsl:when>
		<!-- dernier mot -->
		<xsl:when test="count($noeud/following-sibling::*)=0">
			<xsl:call-template name="litPass1AbrSignes">
				<xsl:with-param name="noeud" select="$noeud"/>
			</xsl:call-template>
		</xsl:when>
		<!-- mot à ne pas abréger -->
		<xsl:when test="$noeud/@abreger = 'false'">
			<xsl:copy-of select="$noeud"/>
			<xsl:if test="$noeud/following-sibling::*[1]">
				<xsl:call-template name="litPass1Abr">
					<xsl:with-param name="noeud" select="$noeud/following-sibling::*[1] "/>
				</xsl:call-template>
			</xsl:if>
		</xsl:when>
		<xsl:when test="$noeud/self::mot 
			and($noeud = replace(lower-case($noeud),'^.',nat:escape-for-regex2(substring($noeud,1,1))) or $noeud = fn:upper-case($noeud))">
			<xsl:variable name="suivant" select="$noeud/following-sibling::*[1]" as="element()?"/>
			<xsl:variable name="suivant2" select="$noeud/following-sibling::*[2]" as="element()?"/>
			<xsl:variable name="suivant3" select="$noeud/following-sibling::*[3]" as="element()?"/>
			<xsl:variable name="suivantMin" select="lower-case($suivant)"/>
			<xsl:variable name="suivantMaj" select="upper-case($suivant)"/>
			<xsl:variable name="suivant2Min" select="lower-case($suivant2)"/>
			<xsl:variable name="suivant2Maj" select="upper-case($suivant2)"/>
<!-- <xsl:message select="($noeud,$suivant)"/> -->
			<xsl:choose>
				<!-- locutions soit tout en majuscules soit tout en minuscule, soit avec une majuscule au début-->
				<xsl:when test="local-name($suivant) = 'mot' and($suivant = $suivantMin or $suivant = $suivantMaj)">
					<xsl:variable name="pos" as="xs:integer?" select="if($suivant2 = $suivant2Min or $suivant2 = $suivant2Maj)
						then fn:index-of($loc3,concat($noeudMin,' ',$suivantMin,' ',$suivant2Min)) else 0"/>
					<!--<xsl:message select="concat($noeud,' ',$suivantMin,' ',$suivant2Min,' ',$pos)"/>-->
					<xsl:variable name="pbSuivant3" as="xs:boolean" select="ends-with($loc3Br[$pos],substring(
						if(local-name($suivant3)='ponctuation' or $suivant2/@doSpace='false') then 
						translate($suivant3,'-,;:.?!&quot;«»“”‘’&lsquo;&rsquo;&acute;&prime;()¡¿''','&pt36;&pt2;&pt23;&pt25;&pt256;&pt26;&pt235;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt3;&pt3;&pt3;&pt3;&pt236;&pt356;&pt235;&pt26;&pt3;')  else 'A',1,1))"/>
					<xsl:choose>
						<xsl:when test="$pos > 0 and not($pbSuivant3)">
							<xsl:choose>
								<xsl:when test="$nAbrnAbr and functx:is-value-in-sequence(concat($noeudMin,' ',$suivantMin,' ',$suivant2Min),$nAbr)">
									<xsl:element name="mot" inherit-namespaces="no">
										<xsl:attribute name="abreger" select="'false'"/>
										<xsl:copy-of select="$noeud/@*"/><!-- on ne prend que les derniers attr --><!--TODO améliorer si attr différents-->
										<xsl:value-of select="$noeud"/>
									</xsl:element>
									<xsl:element name="mot" inherit-namespaces="no">
										<xsl:attribute name="abreger" select="'false'"/>
										<xsl:copy-of select="$suivant/@*"/><!-- on ne prend que les derniers attr --><!--TODO améliorer si attr différents-->
										<xsl:value-of select="$suivant"/>
									</xsl:element>
									<xsl:element name="mot" inherit-namespaces="no">
										<xsl:attribute name="abreger" select="'false'"/>
										<xsl:copy-of select="$suivant2/@*"/><!-- on ne prend que les derniers attr --><!--TODO améliorer si attr différents-->
										<xsl:value-of select="$suivant2"/>
									</xsl:element>
								</xsl:when>
								<xsl:otherwise>
									<xsl:element name="mot" inherit-namespaces="no">
										<xsl:attribute name="abr" select="$loc3Br[$pos]"/>
										<xsl:copy-of select="$suivant2/@*"/><!-- on ne prend que les derniers attr --><!--TODO améliorer si attr différents-->
										<xsl:value-of select="concat($noeud,'_',$suivant,'_',$suivant2)"/>
									</xsl:element>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:call-template name="litPass1Abr">
								<xsl:with-param name="noeud" select="$noeud/following-sibling::*[3]"/>
							</xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
							<xsl:variable name="pos2" as="xs:integer?" select="fn:index-of($loc2,concat($noeudMin,' ',$suivantMin))"/>
							<xsl:variable name="pbSuivant2" as="xs:boolean" select="ends-with($loc2Br[$pos2],substring(
						if(local-name($suivant2)='ponctuation' or ($suivant/@doSpace='false')) then 
						translate($suivant2,'-,;:.?!&quot;«»“”‘’&lsquo;&rsquo;&acute;&prime;()¡¿''','&pt36;&pt2;&pt23;&pt25;&pt256;&pt26;&pt235;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt3;&pt3;&pt3;&pt3;&pt236;&pt356;&pt235;&pt26;&pt3;')  else 'A',1,1))"/>
							<!--<xsl:message select="'pbSuiv2', $pbSuivant2, 'suiv=',$suivant2"/>-->
							<xsl:choose>
								<xsl:when test="$pos2 > 0 and not($pbSuivant2)">
									<xsl:choose>
										<xsl:when test="$nAbrnAbr and functx:is-value-in-sequence(concat($noeudMin,' ',$suivantMin),$nAbr)">
											<xsl:element name="mot" inherit-namespaces="no">
												<xsl:attribute name="abreger" select="'false'"/>
												<xsl:copy-of select="$noeud/@*"/><!-- on ne prend que les derniers attr --><!--TODO améliorer si attr différents-->
												<xsl:value-of select="$noeud"/>
											</xsl:element>
											<xsl:element name="mot" inherit-namespaces="no">
												<xsl:attribute name="abreger" select="'false'"/>
												<xsl:copy-of select="$suivant/@*"/><!-- on ne prend que les derniers attr --><!--TODO améliorer si attr différents-->
												<xsl:value-of select="$suivant"/>
											</xsl:element>
										</xsl:when>
										<xsl:otherwise>
											<xsl:element name="mot" inherit-namespaces="no">
												<xsl:attribute name="abr" select="$loc2Br[$pos2]"/>
												<xsl:copy-of select="$suivant/@*"/><!-- on ne prend que les derniers attr --><!--TODO améliorer si attr différents-->
												<xsl:value-of select="concat($noeud,'_',$suivant)"/>
											</xsl:element>
										</xsl:otherwise>
									</xsl:choose>
									<xsl:call-template name="litPass1Abr">
										<xsl:with-param name="noeud" select="$noeud/following-sibling::*[2]"/>
									</xsl:call-template>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="litPass1AbrSignes">
										<xsl:with-param name="noeud" select="$noeud"/>
									</xsl:call-template>
									<xsl:call-template name="litPass1Abr">
										<xsl:with-param name="noeud" select="$noeud/following-sibling::*[1]"/>
									</xsl:call-template>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="litPass1AbrSignes">
						<xsl:with-param name="noeud" select="$noeud"/>
					</xsl:call-template>
					<xsl:call-template name="litPass1Abr">
						<xsl:with-param name="noeud" select="$noeud/following-sibling::*[1]"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		<xsl:otherwise>
			<xsl:call-template name="litPass1AbrSignes">
				<xsl:with-param name="noeud" select="$noeud"/>
			</xsl:call-template>
			<xsl:call-template name="litPass1Abr">
				<xsl:with-param name="noeud" select="$noeud/following-sibling::*[1]"/>
			</xsl:call-template>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="litPass1AbrSignes"><!--valable aussi pour les locutions sur un mot et les invariants-->
	<xsl:param name="noeud" as="element()"/>
	<xsl:choose>
		<!-- mot à ne pas abréger -->
		<xsl:when test="$noeud/@abreger = 'false'">
			<xsl:copy-of select="$noeud"/>
			<xsl:if test="$noeud/following-sibling::*[1]">
				<xsl:call-template name="litPass1Abr">
					<xsl:with-param name="noeud" select="$noeud/following-sibling::*[1] "/>
				</xsl:call-template>
			</xsl:if>
		</xsl:when>
		<xsl:otherwise>
			<xsl:variable name="motMin" as="xs:string" select="fn:lower-case($noeud)"/>
			<!-- la transcription du mot est-elle possible directement? -->
			<xsl:variable name="pos" as="xs:integer?" select="fn:index-of(($signes,$loc1),$motMin)"/>
			<!--<xsl:message select="('pass1signes',$motMin,';',$pos)"/>-->
			<xsl:variable name="suivant" select="$noeud/following-sibling::*[1]" as="element()?"/>
			<xsl:variable name="pbSuivant" as="xs:boolean" select="ends-with(($signesBr,$loc1Br)[$pos],substring(
				if(local-name($suivant)='ponctuation' or ($noeud/@doSpace='false')) then 
				translate($suivant,'-,;:.?!&quot;«»“”‘’&lsquo;&rsquo;&acute;&prime;()¡¿''',
					'&pt36;&pt2;&pt23;&pt25;&pt256;&pt26;&pt235;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt3;&pt3;&pt3;&pt3;&pt236;&pt356;&pt235;&pt26;&pt3;')
					else 'A',1,1))"/>
				<!--<xsl:message select="'trans=',($signesBr,$loc1Br)[$pos],' suivant:', $suivant, ' debSuiv=',substring(
				if(local-name($suivant)='ponctuation' or ($noeud/@doSpace='false')) then 
				translate($suivant,'-,;:.?!&quot;«»“”‘’&lsquo;&rsquo;&acute;&prime;()¡¿''',
					'&pt36;&pt2;&pt23;&pt25;&pt256;&pt26;&pt235;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt3;&pt3;&pt3;&pt3;&pt236;&pt356;&pt235;&pt26;&pt3;')
					else 'A',1,1)"/>-->
			<xsl:variable name="pbPrec" select="starts-with(($signesBr,$loc1Br)[$pos],substring(
				if($noeud/preceding-sibling::*[1]/@doSpace='false') then 
				translate($noeud/preceding-sibling::*[1],'-,;:.?!&quot;«»“”‘’&lsquo;&rsquo;&acute;&prime;()¡¿''',
					'&pt36;&pt2;&pt23;&pt25;&pt256;&pt26;&pt235;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt3;&pt3;&pt3;&pt3;&pt236;&pt356;&pt235;&pt26;&pt3;')
					else 'A',1,1))" 
				as="xs:boolean"/><!--cette dernière règle pour les tirets et les abr. en pt36;-->
			<!--<xsl:message select="' * Signe: pbSuiv', $pbSuivant, 'suiv=',$suivant,'\n * Signe: pbPrec', $pbPrec, 'prec=', $noeud/preceding-sibling::*[1]"/>-->
			<xsl:variable name="pbSuivPonct" as="xs:boolean" select="not(functx:contains-any-of(($signesBr,$loc1Br)[$pos],$l_pt_sup))
			and (local-name($suivant)='ponctuation' or $noeud/@doSpace='false') 
			and (translate($suivant, ',;:.?!»”’&rsquo;)¡¿','')=''
			or ($suivant='&quot;' and count($noeud/preceding-sibling::*[.='&quot;']) mod 2 = 1))
			and $motMin = $noeud
			and not(ends-with($noeud/preceding-sibling::*[1],''''))"/>
			<xsl:variable name="pbPrecPonct" as="xs:boolean" select="not(functx:contains-any-of(($signesBr,$loc1Br)[$pos],$l_pt_sup))
			and (local-name($noeud/preceding-sibling::*[1])='ponctuation' or $noeud/preceding-sibling::*[1]/@doSpace='false') 
			and (translate($noeud/preceding-sibling::*[1], '«“‘&lsquo;(','')='' 
			or ($noeud/preceding-sibling::*[1]='&quot;' and count($noeud/preceding-sibling::*[.='&quot;']) mod 2 = 1))
			and $motMin = $noeud"/>
			<!-- j'ai enlevé le - car pb avec les traits d'union, 
				si majuscule on peut utiliser l'abréviation  -->
			<!--<xsl:message select="' * Signe: pbSuivPonct', $pbSuivPonct, 'suiv=',$suivant, 'abr=', ($signesBr,$loc1Br)[$pos]"/>-->
			<xsl:choose>
				<!-- et que le signe contient au moins un point haut s'il est suivit d'une ponctuation -->
				<xsl:when test="$pos > 0 and not($pbSuivant or $pbPrec or $pbSuivPonct or $pbPrecPonct)">
					<xsl:element name="mot" inherit-namespaces="no">
						<xsl:attribute name="abr" select="($signesBr,$loc1Br)[$pos]"/>
						<xsl:attribute name="ori" select="$noeud"/>
						<xsl:copy-of select="$noeud/@*"/>
						<xsl:value-of select="substring($noeud,1,1)"/>
					</xsl:element>
				</xsl:when>
				<xsl:otherwise>
					<!-- non; peut-on appliquer une règle de l'ensemble signe? -->
					<xsl:variable name="trans" select="nat:matchRules($motMin,$signesRules,$signesRulesBr,$signesDeriv,$signesDerivBr,1)" as="xs:string*"/>
					<!--<xsl:message select="('*****base',$trans[2])"/>-->
					<xsl:choose>
						<!-- on est bien sur un signe mais il ne faut pas abréger -->
						<xsl:when test="$nAbrnAbr and functx:is-value-in-sequence($trans[2],$nAbr)">
							<xsl:element name="mot" inherit-namespaces="no">
								<xsl:attribute name="abreger" select="'false'"/>
								<xsl:copy-of select="$noeud/@*"/>
								<xsl:value-of select="$noeud"/>
							</xsl:element>
						</xsl:when>
						<xsl:when test="not($trans[1] = ' ' or $pbSuivant or $pbPrec)"><!-- pas besoin de or $pbSuivPonct): les règles sur ces ensembles ajoutent un pt haut ">-->
							<xsl:element name="mot" inherit-namespaces="no">
								<xsl:attribute name="abr" select="$trans[1]"/>
								<xsl:attribute name="ori" select="$noeud"/>
								<xsl:copy-of select="$noeud/@*"/>
								<xsl:value-of select="$noeud"/>
							</xsl:element>
						</xsl:when>
						<xsl:otherwise>
							<xsl:copy-of select="$noeud" copy-namespaces="no"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="bijection">
	<xsl:param name="mot" as="xs:string"/>
	<xsl:param name="PN" as="xs:integer" select='-10'/>
	<xsl:call-template name="symbolesComposes">
		<xsl:with-param name="mot" select="lower-case($mot)"/>
		<xsl:with-param name="PN" select="$PN"/>
	</xsl:call-template>
</xsl:template>

<xsl:template name="symbolesComposes">
	<xsl:param name="mot" as="xs:string"/>
	<xsl:param name="PN" as="xs:integer"/>
	<xsl:param name="tiret" select='0' as="xs:integer"/><!-- = 1 si il faut tester le tiret simple; 2 si on est en fin de mot-->
	<xsl:param name="ivb" select="true()" as="xs:boolean" tunnel="yes"/><!-- faux si il ne faut pas appliquer l'ivb -->
	<!--<xsl:message select="('pass2:',.)"/>-->
	<xsl:if test="$dbg"><xsl:message select="('traitement symboles composés',$mot,.)"/></xsl:if>
	<!-- détection des préfixages majuscule -->
	<xsl:if test="matches($mot, '^(&pt25;)?&pt46;')">
		<!-- Fred à Bruno : mais à quoi ça peut bien servir ??? -->
		<xsl:value-of select="replace($mot,'^((&pt25;)?[&pt46;]+).*$','$1')"/>
	</xsl:if>
	<!-- Fred à Bruno : et pourquoi tu enlèves le multiprefixemaj ici ??? -->
	<xsl:variable name="mot" select="translate($mot,'&pt25;&pt46;','')"/>
	<xsl:variable name="resu">
		<xsl:choose>
			<xsl:when test="@abr"><!-- le mot a déjà été abrégé -->
				<xsl:value-of select="concat($stopCoup,@abr)"/>
			</xsl:when>
			<xsl:when test="@doSpace='false' and $mot='-'">
				<xsl:text>&pt36;</xsl:text><!--<xsl:message select="' * trait d''union'"/>-->
			</xsl:when>
			<!-- mot à ne pas abréger -->
			<xsl:when test="../@abreger='false' or @abreger='false'">
				<xsl:if test="$dbg"><xsl:message select="'   **',$mot, ' ne doit pas être abrégé'"/></xsl:if>
				<xsl:if test="@ivb='true' and $doIVB">
					<xsl:if test="$dbg"><xsl:message select="('*Application de l''ivb pour ',$mot)"/></xsl:if>
					<xsl:text>&pt56;</xsl:text>
				</xsl:if><!-- ajout de l'ivb si besoin -->
				<xsl:call-template name="symbCompAbr">
					<xsl:with-param name="mot" select="$mot"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="rezu">
					<!-- est-ce une unité à préfixer? -->
					<!--<xsl:text>rez**</xsl:text>-->
					<xsl:choose>
						<!-- premier cas simple: on est sûr qu'il s'agit d'une unité -->
						<!-- le deuxième test avec strats-with sert à ne pas préfixer plusieurs fois, ce qui se produit si l'unité contient des majuscules -->
						<xsl:when test="functx:is-value-in-sequence(.,$unites) and starts-with(.,$mot)">
							<!--<xsl:text>nu**</xsl:text>-->
							<xsl:value-of select="'&pt6;'"/>
							<xsl:call-template name="symbCompAbr">
								<xsl:with-param name="mot" select="lower-case($mot)"/>
							</xsl:call-template>
						</xsl:when>
						<!-- est-ce une unité avec ambiguïté? -->
						<xsl:when test="functx:is-value-in-sequence(.,($unitesAmb,$unitesAmbSimple)) and starts-with(.,$mot)">
							<!-- on essaie de résoudre la question avec des heuristiques -->
							<!--<xsl:text>amb**</xsl:text>-->
							<xsl:if test="$dbg"><xsl:message select="(('*********************',nat:getPrecedingValues(.,2)))"/></xsl:if>
							<xsl:choose>
								<xsl:when test="matches(nat:getPrecedingValues(.,2),'\d')">
									<xsl:if test="$dbg"><xsl:message select="('*** Ambiguïté sur unité résolue par le contexte ***')"/></xsl:if>
									<xsl:value-of select="'&pt6;'"/>
									<xsl:call-template name="symbCompAbr">
										<xsl:with-param name="mot" select="lower-case($mot)"/>
									</xsl:call-template>
								</xsl:when>
								<xsl:when test="functx:is-value-in-sequence(.,$unitesAmbSimple)">
									<xsl:call-template name="symboleFond">
										<xsl:with-param name="mot" select="lower-case($mot)"/>
									</xsl:call-template>
								</xsl:when>
								<xsl:otherwise>
									<!-- génération d'une ambiguïté -->
									<xsl:variable name="siUnite">
										<xsl:call-template name="symbCompAbr">
											<xsl:with-param name="mot" select="lower-case($mot)"/>
										</xsl:call-template>
									</xsl:variable>
									<xsl:variable name="pasUnite">
										<xsl:call-template name="symboleFond">
											<xsl:with-param name="mot" select="lower-case($mot)"/>
										</xsl:call-template>
									</xsl:variable>
									<!--
									<xsl:element name="ambiguity">
										<amb_text>Unité (1) ou autre (2)?</amb_text>
										<context>
											<preceding><xsl:value-of select="nat:getPrecedingValues(.,6)"/></preceding>
											<amb_expr><xsl:value-of select="nat:getOriginalValue(.)"/></amb_expr>
											<following><xsl:value-of select="nat:getFollowingValues(.,6)"/></following>
										</context>
										<value id="1"><xsl:value-of select="concat('&pt6;',$siUnite)"/></value>
										<value id="2"><xsl:value-of select="$pasUnite"/></value>
									</xsl:element>-->
									<!--<xsl:value-of select="concat('[Amb:Unité (1) ou non (2)?|',nat:getPrecedingValues(.,6),'|',nat:getOriginalValue(.),'|',nat:getFollowingValues(.,6),'|&pt6;',$siUnite,'|',$pasUnite,']')"/>-->
									<xsl:value-of select="concat('[Amb:Unité (1) ou non (2)?|',nat:getPrecedingValues(.,6),'|',nat:getOriginalValue(.),'|',nat:getFollowingValues(.,6),'|&pt6;',$siUnite,'|',$pasUnite,']')"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						<xsl:otherwise>
							<!--<xsl:text>sf**</xsl:text>-->
							<xsl:call-template name="symboleFond">
								<xsl:with-param name="mot" select="lower-case($mot)"/>
							</xsl:call-template>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:value-of select="$rezu"/>
				<!--<xsl:message select="concat('mot:', $mot, 'rezu:', $rezu)"/>-->
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:choose>
		<!-- écriture des ambiguïtés -->
		<xsl:when test="@ambNP">
			<xsl:if test="$dbg"><xsl:message select="('*** AMBIGUITE ***')"/></xsl:if>
			<xsl:variable name="alternative">
				<xsl:call-template name="symbCompAbr">
					<xsl:with-param name="mot" select="@ambNP"/>
				</xsl:call-template>
			</xsl:variable>
			<!--<xsl:variable name="amb">
				<xsl:element name="amb">
					<xsl:attribute name="alternate" select="$alternative"/>
					<xsl:value-of select="$resu"/>
				</xsl:element>
			</xsl:variable>
			<xsl:copy-of select="$amb"/>-->
			<!--<xsl:message select="$amb"/>-->
			<!--<xsl:value-of select="concat('[Amb:Mot français (1) ou Nom propre (2)?|',nat:getPrecedingValues(.,6),'|',nat:getOriginalValue(.),'|',nat:getFollowingValues(.,6),'|',$resu,'|',concat($stopCoup,$alternative),']')"/>-->
			<!--
			<xsl:element name="ambiguity">
				<amb_text>Mot français (1) ou Nom propre (2)?</amb_text>
				<context>
					<preceding><xsl:value-of select="nat:getPrecedingValues(.,6)"/></preceding>
					<amb_expr><xsl:value-of select="nat:getOriginalValue(.)"/></amb_expr>
					<following><xsl:value-of select="nat:getFollowingValues(.,6)"/></following>
				</context>
				<value id="1"><xsl:value-of select="$resu"/></value>
				<value id="2"><xsl:value-of select="concat($stopCoup,$alternative)"/></value>
			</xsl:element>
			-->
			<xsl:value-of select="concat('[Amb:Mot français (1) ou Nom propre (2)?|',nat:getPrecedingValues(.,6),'|',nat:getOriginalValue(.),'|',nat:getFollowingValues(.,6),'|',$resu,'|',concat($stopCoup,$alternative),']')"/>
			<!--<amb>
				<def><xsl:text>Nom propre ou mot français?</xsl:text></def>
				<origine><xsl:value-of select="."/></origine>
				<p1><xsl:value-of select="$alternative"/></p1>
				<p2><xsl:value-of select="$resu"/></p2>
			</amb>-->
		</xsl:when>
		<xsl:when test="@ambIVB">
			<xsl:if test="$dbg"><xsl:message select="('*** AMBIGUITE ***')"/></xsl:if>
			<xsl:variable name="alternative">
				<xsl:call-template name="symbCompAbr">
					<xsl:with-param name="mot" select="@ambIVB"/>
				</xsl:call-template>
			</xsl:variable>
			<!--<xsl:variable name="amb">
				<xsl:element name="amb">
					<xsl:attribute name="alternate" select="$alternative"/>
					<xsl:value-of select="$resu"/>
				</xsl:element>
			</xsl:variable>
			<xsl:copy-of select="$amb"/>-->
			<!--<xsl:message select="$amb"/>-->
			<xsl:value-of select="concat('[Amb:Appliquer IVB (1) ou pas d''IVB (2)?|',nat:getPrecedingValues(.,6),'|',nat:getOriginalValue(.),'|',nat:getFollowingValues(.,6),'|',concat('&pt56;',$alternative),'|',$resu,']')"/>
			<!--
			<xsl:element name="ambiguity">
				<amb_text>Appliquer IVB (1) ou pas d''IVB (2)?</amb_text>
				<context>
					<preceding><xsl:value-of select="nat:getPrecedingValues(.,6)"/></preceding>
					<amb_expr><xsl:value-of select="nat:getOriginalValue(.)"/></amb_expr>
					<following><xsl:value-of select="nat:getFollowingValues(.,6)"/></following>
				</context>
				<value id="1"><xsl:value-of select="concat('&pt56;',$alternative)"/></value>
				<value id="2"><xsl:value-of select="$resu"/></value>
			</xsl:element>
			-->
			<!--<amb>
				<def><xsl:text>Nom propre ou mot français?</xsl:text></def>
				<origine><xsl:value-of select="."/></origine>
				<p1><xsl:value-of select="$alternative"/></p1>
				<p2><xsl:value-of select="$resu"/></p2>
			</amb>-->
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$resu"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="symboleFond">
	<xsl:param name="mot"/><!-- il est en minuscule à partir d'ici -->
	<xsl:param name="ivb" select="true()" as="xs:boolean" tunnel="yes"/>
	<!--<xsl:message select="($symbs,$mot)"/>-->
	<xsl:if test="$dbg"><xsl:message select="('traitement des symboles fondamentaux pour:',$mot)"/></xsl:if>
	<xsl:variable name="pos" as="xs:integer?" select="fn:index-of(($symbs,$symbsNC),$mot)"/>
	<xsl:choose>
		<xsl:when test="$pos > 0"><!--oui-->
			<!--<xsl:text>sf2**</xsl:text>-->
			<xsl:variable name="trans" select="($symbsBr,$symbsNCBr)[$pos]" as="xs:string"/>
			<!--<xsl:message select="' * Symb fond:',$trans, ' ', preceding-sibling::*[1]"/>-->
			<xsl:variable name="pbPrec" as="xs:boolean" select="preceding-sibling::*[1]/@doSpace='false' and
						ends-with(translate(preceding-sibling::*[1],'-,;:.?!&quot;«»“”‘’&lsquo;&rsquo;&acute;&prime;()¡¿''',
						'&pt36;&pt2;&pt23;&pt25;&pt256;&pt26;&pt235;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt3;&pt3;&pt3;&pt3;&pt236;&pt356;&pt235;&pt26;&pt3;'),
						substring($trans,1,1))"/>
			<xsl:variable name="suivant" select="following-sibling::*[1]" as="element()?"/>
			<xsl:variable name="pbSuivant" as="xs:boolean" select="ends-with(($symbsBr,$symbsNCBr)[$pos],substring(
				if(local-name($suivant)='ponctuation' or (@doSpace='false')) then 
				translate($suivant,'-,;:.?!&quot;«»“”‘’)',
					'&pt36;&pt2;&pt23;&pt25;&pt256;&pt26;&pt235;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt356;')
					else 'A',1,1))"/>
<!-- '-,;:.?!&quot;«»“”‘’&lsquo;&rsquo;&acute;&prime;()¡¿''',
'&pt36;&pt2;&pt23;&pt25;&pt256;&pt26;&pt235;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt356;')
-->
			<!--<xsl:message select="'trans=',($symbsBr,$symbsNCBr)[$pos],' suivant:', $suivant, ' debSuiv=',substring(
				if(local-name($suivant)='ponctuation' or (@doSpace='false')) then 
				translate($suivant,'-,;:.?!&quot;«»“”‘’&lsquo;&rsquo;&acute;&prime;()¡¿''',
				'&pt36;&pt2;&pt23;&pt25;&pt256;&pt26;&pt235;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt3;&pt3;&pt3;&pt3;&pt236;&pt356;&pt235;&pt26;&pt3;')
				else 'A',1,1)"/>-->
			<!--<xsl:message select="' * Signe: pbSuiv', $pbSuivant, 'suiv=',$suivant"/>-->
			<xsl:choose>
				<xsl:when test="$pbPrec or $pbSuivant">
					<xsl:call-template name="casGeneral">
						<xsl:with-param name="mot" select="$mot"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="concat($stopCoup,$trans)"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		<xsl:otherwise>
			<!--<xsl:text>nosf**</xsl:text>-->
			<!-- est-ce que le mot contient un symbole dérivable -->
			<xsl:choose>
				<xsl:when test="functx:contains-any-of($mot,$symbs) or nat:starts-with-any-of($mot,$symbsNC)">
					<!--extraction du symbole -->
					<!--<xsl:message select="('resultat-match:',replace('destinations', '.*(destination|nation).*','patt:$1'))"/>-->
					<xsl:variable as="xs:string" name="symbPref">
						<xsl:variable name="racine" as="xs:string*">
							<xsl:analyze-string select="$mot" regex="{concat('(',string-join(($symbs,$symbsNC),'|'),')')}">
								<xsl:matching-substring>
									<xsl:value-of select="regex-group(1)"/>
								</xsl:matching-substring>
							</xsl:analyze-string>
						</xsl:variable>
						<xsl:value-of select="$racine[count($racine)]"/>
						<xsl:if test="$dbg"><xsl:message select="('Racines possibles:',for $m in $racine return $m)"/></xsl:if>
					</xsl:variable>
					<xsl:variable name="indexAmbSymb" select="max((0,index-of($l_amb_comp_connues,$symbPref)))" as="xs:integer?"/>
					<!--<xsl:message select="('index ambiguïté:',$indexAmbSymb)"/>-->
					<!-- est-ce que le symbole est composable? (ambiguïté) fraction/action par ex-->
					<xsl:choose>
						<!-- soit direct le symbole, soit un composé possible, soit un dérivé -->
						<xsl:when test="$indexAmbSymb = 0 or ($indexAmbSymb &gt; 0 and matches($mot,$l_amb_comp_resol[$indexAmbSymb]))">
							<!--<xsl:variable name="symb" as="xs:string?" select="replace($mot,concat('.*(',string-join($symbs,'|'),')'),'$1')"/>-->
							<!--<xsl:message select="(' *** COMPOSé? prefixe:',substring-before($mot,$symbPref),';mot:',$mot,'symb:',$symbPref)"/>-->
							<xsl:variable name="prefixe" as="xs:string?" select="substring-before($mot,$symbPref)"/>
							<xsl:variable name="reste" select="if(string-length($prefixe)=0) then $mot else substring-after($mot,$prefixe)" as="xs:string"/>
							<xsl:if test="$dbg"><xsl:message select="('Composé?',' m:',$mot,' p:',$prefixe,' s:',$symbPref,' r:',$reste)"/></xsl:if>
							<xsl:variable name="prefBr">
								<xsl:if test="not($reste = $mot)"><!-- il y a un préfixe et le mot est composable-->
									<xsl:call-template name="casGeneral">
										<xsl:with-param name="mot" select="$prefixe"/><!-- à améliorer: pb pour tout ce qui match au début en cas gen -->
										<xsl:with-param name="termin" select="false()"/>
										<xsl:with-param name="l_suiv" select="if (matches($reste, '^[bçcdfghjklmnpqrstvwxz]')) then 'C' else 'V'"/>
									</xsl:call-template>
								</xsl:if>
							</xsl:variable>
							<!--<xsl:value-of select="$prefBr"/>-->
							<!-- dérivé? -->
							<!-- non; peut-on appliquer une règle de l'ensemble symbole si besoin? -->
							<xsl:variable name="trans" select="if ($symbPref = $reste) then $symbsBr[index-of($symbs,$symbPref)] else nat:matchRules($reste,$symbsRules,$symbsRulesBr,($symbs,$symbsNC),($symbsBr,$symbsNCBr),1)" as="xs:string*"/>
							<!--<xsl:message select="('prefixe: ', $prefBr, ';trans=',$trans)"/>-->
							<xsl:choose>
								<xsl:when test="$nAbrnAbr and functx:is-value-in-sequence($symbPref,$nAbr) and not($trans[1] = ' ')">
									<xsl:if test="$dbg"><xsl:message select="('Pas d''abrégement pour ',$symbPref)"/></xsl:if>
									<xsl:value-of select="$prefBr"/>
									<xsl:call-template name="symbCompAbr">
										<xsl:with-param name="mot" select="$reste"/>
									</xsl:call-template>
								</xsl:when>
								<!-- est-ce que le mot précédent est collé à celui-ci, et dans ce cas est-ce qu'il n'y a pas deux signes identiques qui se suivent? 
									Mais deux lettres identiques peuvent se suivre si elles sont identiques en noir: ex: ac couple => acc[pl] (acc6)
								-->
								<xsl:when test="(preceding-sibling::*[1]/@doSpace='false' or string-length($prefBr) > 0) and
									ends-with(translate(if(string-length($prefBr) > 0) then $prefBr else preceding-sibling::*[1],
									'-,;:.?!&quot;«»“”‘’&lsquo;&rsquo;&acute;&prime;()¡¿''', '&pt36;&pt2;&pt23;&pt25;&pt256;&pt26;&pt235;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt3;&pt3;&pt3;&pt3;&pt236;&pt356;&pt235;&pt26;&pt3;'),
									substring($trans[1],1,1)) and not(ends-with($prefixe, substring($reste, 1,1)))"><!--if(string-length($prefBr)>0) then $prefBr else $trans,1,1-->
									<xsl:call-template name="casGeneral">
										<xsl:with-param name="mot" select="$mot"/>
									</xsl:call-template>
								</xsl:when>
								<xsl:when test="not($trans[1] = ' ')">
									<xsl:value-of select="concat($prefBr,$stopCoup,$trans[1])"/>
								</xsl:when>
								<xsl:otherwise>
									<!--<xsl:message select="'essai pass 2'"/>-->
									<xsl:variable name="indexSymb" select="index-of(($symbs,$symbsNC),$symbPref[1])[1]" as="xs:integer?"/>
									<!-- règles de pass2 -->
									<xsl:variable name="trans2" select="nat:matchRulesP2(fn:replace($reste,$symbPref[1],($symbsBr,$symbsNCBr)[$indexSymb]),$symbsRulesP2,$symbsRulesP2Br,($symbsBr,$symbsNCBr),1)" as="xs:string?"/>
									<!-- utilisation ou non de l'abréviation du symbole, pour abrégé progressif-->
									<xsl:variable name="trans3" select="if(functx:is-value-in-sequence($symbPref,$nAbr))then nat:matchRulesP2($reste,$symbsRulesP2,$symbsRulesP2Br,($symbPref),1) else $trans2"/>
									<!--<xsl:message select="'index:', $indexSymb,'symb',$symbPref[1],'racine:',$symbPref,'br',($symbsBr,$symbsNCBr)[$indexSymb],'trans:',$trans2"/>-->
									<xsl:choose>
										<xsl:when test="not($trans3 = ' ')">
											<xsl:if test="$dbg"><xsl:message select="('Règle pass 2 possible ',$symbPref)"/></xsl:if>
											<xsl:choose>
												<!-- si il y a une différence entre trans 2 et trans 3=> on est en abrégé partiel, et il y a une règle qui a matché -->
												<xsl:when test="$trans2 = $trans3">
													<xsl:value-of select="concat($prefBr,$stopCoup,$trans3)"/>
												</xsl:when>
												<xsl:otherwise>
													<xsl:variable name="racineAbr" as="xs:string?">
														<xsl:call-template name="symbCompAbr">
															<xsl:with-param name="mot" select="$symbPref"/>
														</xsl:call-template>
													</xsl:variable>
													<xsl:value-of select="concat($prefBr,$stopCoup,replace($trans3, $symbPref, $racineAbr))"/>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:when>
										<xsl:otherwise>
											<xsl:call-template name="casGeneral">
												<xsl:with-param name="mot" select="$mot"/>
											</xsl:call-template>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						<!-- le mot n'a pas de préfixe mais fait partie de la liste des ambigUîtés de composés -->
						<xsl:when test="substring-before($mot,$symbPref)='' and $indexAmbSymb &gt; 0">
							<!-- non; peut-on appliquer une règle de l'ensemble symbole si besoin? -->
							<xsl:variable name="trans" select="nat:matchRules($mot,$symbsRules,$symbsRulesBr,($symbs,$symbsNC),($symbsBr,$symbsNCBr),1)" as="xs:string*"/>
							<!--<xsl:message select="('prefixe: ', $prefBr, ';trans=',$trans)"/>-->
							<xsl:choose>
								<!-- est-ce que le mot précédent est collé à celui-ci, et dans ce cas est-ce qu'il n'y a pas deux signes identiques qui se suivent? 
									Mais deux lettres identiques peuvent se suivre si elles sont identiques en noir: ex: ac couple => acc[pl] (acc6)
								-->
								<xsl:when test="$nAbrnAbr and functx:is-value-in-sequence($trans[2],$nAbr)">
									<xsl:if test="$dbg"><xsl:message select="('Pas d''abrègement pour ',$trans[2])"/></xsl:if>
									<xsl:call-template name="symbCompAbr">
										<xsl:with-param name="mot" select="$mot"/>
									</xsl:call-template>
								</xsl:when>
								<xsl:when test="(preceding-sibling::*[1]/@doSpace='false') and
									ends-with(translate(preceding-sibling::*[1],
									'-,;:.?!&quot;«»“”‘’&lsquo;&rsquo;&acute;&prime;()¡¿''', '&pt36;&pt2;&pt23;&pt25;&pt256;&pt26;&pt235;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt3;&pt3;&pt3;&pt3;&pt236;&pt356;&pt235;&pt26;&pt3;'),
									substring($trans[1],1,1))"><!--if(string-length($prefBr)>0) then $prefBr else $trans,1,1-->
									<xsl:call-template name="casGeneral">
										<xsl:with-param name="mot" select="$mot"/>
									</xsl:call-template>
								</xsl:when>
								<xsl:when test="not($trans[1] = ' ')">
									<xsl:value-of select="concat($stopCoup,$trans[1])"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:if test="$dbg"><xsl:message select="'Essai d''une règle de deuxième passe', (($symbs,$symbsNC),$symbPref[1])[1], '**', $symbPref[1]"/></xsl:if>
									<!--<xsl:message select="'essai pass 2'"/>-->
									<xsl:variable name="indexSymb" select="index-of(($symbs,$symbsNC),$symbPref[1])[1]" as="xs:integer?"/>
									<!-- règles de pass2 -->
									<xsl:variable name="trans2" select="nat:matchRulesP2(fn:replace($mot,$symbPref[1],($symbsBr,$symbsNCBr)[$indexSymb]),$symbsRulesP2,$symbsRulesP2Br,($symbsBr,$symbsNCBr),1)" as="xs:string?"/>
									<!--<xsl:message select="'index:', $indexSymb,'symb',$symbPref[1],'br',($symbsBr,$symbsNCBr)[$indexSymb],'trans:',$trans2"/>-->
									<xsl:choose>
										<xsl:when test="not($trans2 = ' ')">
											<xsl:value-of select="concat($stopCoup,$trans2)"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:call-template name="casGeneral">
												<xsl:with-param name="mot" select="$mot"/>
											</xsl:call-template>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						<xsl:otherwise><!-- le symbole n'est pas composable (ambiguïté)-->
							<xsl:call-template name="casGeneral">
								<xsl:with-param name="mot" select="$mot"/>
							</xsl:call-template>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:otherwise>
					<!--<xsl:text>CG**</xsl:text>-->
					<xsl:call-template name="casGeneral">
						<xsl:with-param name="mot" select="$mot"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="casGeneral">
	<xsl:param name="mot"/>
	<xsl:param name="termin" select="true()" as="xs:boolean"/>
	<xsl:param name="l_suiv" select="''" as="xs:string"/>
	<xsl:param name="ivb" select="true()" as="xs:boolean" tunnel="yes"/>
	<xsl:if test="$dbg"><xsl:message select="('arrivée cas Général:',$mot)"/></xsl:if>
	
	<xsl:variable name="mev" select="@mev" as="xs:string?"/>
	<xsl:choose>
		<!-- le mot ne doit pas être abrégé (il y a une mise en évidence partielle) -->
		<xsl:when test="$emph_mix and (
		(@doSpace='false' and not(nat:ends-with-any-of($mot,('''','-'))) and exists(following-sibling::*[1][not(string(@mev)=string($mev)) and not(self::echap) and not(.='-')])) or
		(exists(preceding-sibling::*[1][@doSpace='false' and not(self::echap) and not(string(@mev)=string($mev)) and not(nat:ends-with-any-of(.,($punctAllBl,'-')))]))
		)">
		<!-- autres conditions qui avaient l'air utiles mais ptête pas : or
		(following-sibling::*[1]/@doSpace='false' and (following-sibling::*[1]/@mev!=@mev)) or -->
		<!--<xsl:message select="preceding-sibling::*[1], $mot, '$mev',$mev,'@mev',@mev,'egal?',string(@mev)=string($mev), exists(preceding-sibling::*[1][@doSpace='false' and not(self::echap) and not(@mev=$mev) and not(nat:ends-with-any-of(.,($punctAllBl,'-')))])" />-->
			<xsl:call-template name="symbCompAbr">
				<xsl:with-param name="mot" select="$mot"/>
			</xsl:call-template>
		</xsl:when>
		<!-- ivb -->
		<xsl:when test="functx:is-value-in-sequence($mot,$l_ivb) or ($doIVB and $ivb and matches($mot,$l_ivb_rules) and $termin and not(preceding-sibling::*[1]/@doSpace='false' or @doSpace='false'))">
			<!--<xsl:element name="mot" inherit-namespaces="no">
				<xsl:attribute name="abreger" select="'false'"/>
				<xsl:attribute name="ivb" select="'true'"/>
				<xsl:copy-of copy-namespaces="no" select="$noeud"/>
			</xsl:element>-->
			<xsl:if test="$dbg"><xsl:message select="(' - utilisation de l''ivb (d''après ',if(functx:is-value-in-sequence($mot,$l_ivb)) then 'fichier' else 'règles',') ')"/></xsl:if>
			<xsl:text>&pt56;</xsl:text>
			<xsl:call-template name="symbCompAbr">
				<xsl:with-param name="mot" select="$mot"/>
			</xsl:call-template>
		</xsl:when>
		<xsl:otherwise>
			<!-- caractère de fin du mot précédent s'il est collé -->
			<xsl:variable name="finPrec" as="xs:string" select="if(preceding-sibling::*[1]/@doSpace='false')
					then translate(substring(preceding-sibling::*[1],string-length(preceding-sibling::*[1]),1),
							'-,;:.?!&quot;«»“”‘’&lsquo;&rsquo;&acute;&prime;()¡¿''',
							'&pt36;&pt2;&pt23;&pt25;&pt256;&pt26;&pt235;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt3;&pt3;&pt3;&pt3;&pt236;&pt356;&pt235;&pt26;&pt3;')
					else ('&pt;')"/>
			<!-- ponctuation suivante si elle est colléé -->
			<!--<xsl:message select="$l_suiv"/>-->
			<xsl:variable name="ponctSuiv" as="xs:string" select="if($l_suiv='' and (following-sibling::*[1]/@doSpace='false' or local-name(following-sibling::*[1])='ponctuation'))
					then translate(following-sibling::*[1],
							'-,;:.?!&quot;«»“”‘’)',
							'&pt36;&pt2;&pt23;&pt25;&pt256;&pt26;&pt235;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt356;')
					else ('&pt;')"/>
			<!--<xsl:message select="('pct suiv', $ponctSuiv)"/>-->
			
			<!-- définition des jeu de règles à utiliser -->
			<xsl:variable name="rules" as="xs:string*" select="if ($termin) then ($cgenFinRules,$cgenRules,$cgenPass2Rules) else $cgenRules"/>
			<xsl:variable name="rulesBr" as="xs:string*" select="if ($termin) then ($cgenFinRulesBr,$cgenRulesBr,$cgenPass2RulesBr) else $cgenRulesBr"/>

			<!-- liste des syllabes à abréger, génération des éléments syllabes -->
			<xsl:variable name="motAbr" as="element()*" select="nat:matchGeneralRules(concat($mot,$l_suiv),$rules,$rulesBr,1)"/>
			<xsl:if test="$dbg"><xsl:message select="('Liste des séquences:', $motAbr)"/></xsl:if>
			<!--<xsl:message select="('mot abr:', $motAbr, ':' ,  string-join($motAbr/noir,''), ':', string-join($motAbr/abr,''))"/>-->
			<!-- tri des séquences et filtrage des abréviations interdites-->
			<!-- la suppression des abréviations avec les ponctuations est une interprétation de la norme -->
			<xsl:variable name="motAbrSort" as="element()*">
				<xsl:for-each select="$motAbr">
					<xsl:sort select="./deb" data-type="number"/>
					<xsl:sort select="string-length(./noir)" order="descending" data-type="text"/>
					<!-- le mot se termine par un assemblage = à la ponctuation suivante 
						ou commence par un assemblage = précédent
					-->
					<xsl:if test="not((ends-with(./abr,$ponctSuiv) and string-length($mot) = string-length(./noir)+./deb) or 
						(starts-with(./abr,$finPrec) and ./deb='0'))">
						<xsl:copy-of select="."/>
					</xsl:if>
				</xsl:for-each>
			</xsl:variable>
			<xsl:if test="$dbg"><xsl:message select="('Liste des séquences triées:', $motAbrSort, ':' ,  string-join($motAbrSort/noir,''), ':', string-join($motAbrSort/abr,''))"/></xsl:if>
			<!-- liste du dernier indice des contractions dans le mot, le premier indice est 0: exemPLe => 4+2= 6-->
			<xsl:variable name="lg" as="xs:double*" select="(for $i in $motAbrSort return $i/deb + string-length($i/noir))"/>
			<!--<xsl:message select="('lg:',$lg)"/>-->
			<xsl:variable name="pbPrec" as="xs:boolean" select="if($motAbrSort[1]/deb=0 and $motAbrSort[1]/abr = $finPrec) then true() else false()"/>
			<xsl:variable name="noeud" select="."/>
			<xsl:choose>
				<!-- il n'y a pas d'empiètement partiel dans les abréviations ET pas d'abréviation générant une ambiguïté sans recouvrement-->
				<xsl:when test="not(nat:matches-any-of($mot,$l_amb_connues)) and
					(every $i in 1 to (count($lg) - 1) satisfies 
						(every $j in 1 to (count($lg) - 1) satisfies ($lg[$i] &lt;= number($motAbrSort[$j]/deb)) or 
						($lg[$i] >= $lg[$i+1] and number($motAbrSort[$i]/deb) &lt;= number($motAbrSort[$i+1]/deb)))
						and not($lg[$i] = number($motAbrSort[$i+1]/deb) and $motAbrSort[$i]/abr = $motAbrSort[$i+1]/abr 
						and not($motAbrSort[$i]/noir = $motAbrSort[$i+1]/noir))) ">
					<xsl:if test="$dbg"><xsl:message select="'- - pas d''empiètement dans les séquences - -'"/></xsl:if>
					<xsl:variable name="fin">
						<xsl:choose>
							<xsl:when test="count(($motAbrSort/noir))=0">
								<xsl:value-of select="$mot"/>
							</xsl:when>
							<xsl:otherwise>
								<!--écriture des abréviations -->			
								<!-- il peut y avoir une inclusion avec le >= (ex: ellement et ent), mais dans ce cas le replace du ent final sera vide et non effectué 
									PLUS maintenant => suppression des abr recouvertes
								-->
								<xsl:variable name="motAbrSort2" select="if($pbPrec) then $motAbrSort[position()>1] else ($motAbrSort)"/>
								<xsl:variable name="motAbrSort3" select="for $i in $motAbrSort2 return
									if($i/deb + string-length($i/noir) = string-length($mot) or not(some $j in $motAbrSort2[not(. = $i)] satisfies (number($j/deb) &lt;= number($i/deb) and $j/deb + string-length($j/noir) &gt;= $i/deb + string-length($i/noir)))) then $i else()"/>
								<xsl:if test="$dbg"><xsl:message select="('Liste des séquences filtrées avant abréviation', $motAbrSort3, ':' ,  string-join($motAbrSort3/noir,''), ':', string-join($motAbrSort3/abr,''))"/></xsl:if>
								<!--écriture des abréviations -->

								<!-- due to https://saxonica.plan.io/issues/6434?pn=1#change-26137 $motAbrSort3/subnode order is random-->
<!--								<xsl:variable name="chSyll" as="xs:string*" select="$motAbrSort3/noir"/>&lt;!&ndash; syllabes noires abrégées &ndash;&gt;-->
<!--								<xsl:variable name="chSyllAbr" as="xs:string*" select="$motAbrSort3/abr"/>&lt;!&ndash; syllabes abrégées &ndash;&gt;-->
<!--								<xsl:variable name="chDeb" as="xs:integer*" select="$motAbrSort3/deb"/>&lt;!&ndash; débuts des syllabes abrégées &ndash;&gt;-->

								<xsl:variable name="chSyll" as="xs:string*" select="for $i in $motAbrSort3 return ($i/noir)"/><!-- syllabes noires abrégées -->
								<xsl:variable name="chSyllAbr" as="xs:string*" select="for $i in $motAbrSort3 return ($i/abr)"/><!-- syllabes abrégées -->
								<xsl:variable name="chDeb" as="xs:integer*" select="for $i in $motAbrSort3 return ($i/deb)"/><!-- débuts des syllabes abrégées -->

								<xsl:variable name="finAbr">
									<xsl:variable name="apresAbr" select="nat:replace-first-multi($mot,$chSyll,$chSyllAbr,$chDeb)"/>
									<xsl:value-of select="if ((functx:contains-any-of($apresAbr, $l_pt_sup) 
										or (string-length($apresAbr) &lt; 2 and not (preceding-sibling::*[1]/@doSpace = 'false' and preceding-sibling::*[1][self::ponctuation]))) 
										or not($termin)) then $apresAbr 
										else (nat:replace-first-multi($mot,$chSyll[position()>1],$chSyllAbr[position()>1],$chDeb[position()>1]))"/>
								</xsl:variable>
								<xsl:value-of select="$finAbr"/>
								<xsl:if test="$dbg"><xsl:message select="('Liste des syllabes abrégées: ', $chSyll, 'finAbr=' , $finAbr)"/></xsl:if>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					
					<xsl:if test="$dbg"><xsl:message select="('mot abrégé: ',$fin)"/></xsl:if>
					<!-- si le mot ne contient pas d'abréviations ET qu'il y a confusion avec un symbole -->
					<xsl:if test="$doIVB and not(matches($mot,('(^[-])?\d.*'))) and string-length($mot) &gt; 1
						and not(number($mot)) and count($motAbrSort/noir)=0 
						and (functx:is-value-in-sequence(nat:toBrUTF8(if (ends-with($mot,'s')) then substring($mot, 1, string-length($mot) - 1) else ''), 
							if (ends-with($mot,'s')) then ($signesDerivBr,$symbsBr, $symbsNCBr) else ($loc3Br, $loc2Br, $loc1Br, $signesBr,$symbsBr, $symbsNCBr)) or functx:is-value-in-sequence(nat:toBrUTF8($mot), ($loc3Br, $loc2Br, $loc1Br, $signesBr,$symbsBr, $symbsNCBr)))
						and $termin and not(preceding-sibling::*[1]/@doSpace='false' or @doSpace='false')">
						<xsl:if test="$dbg"><xsl:message select="' - ivb (d''après appréciation par NAT)'"/></xsl:if>
						<xsl:text>&pt56;</xsl:text>
					</xsl:if>
					<xsl:call-template name="symbCompAbr">
						<xsl:with-param name="mot" select="$fin"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<!-- il y a soit empiètement partiel (ex: ien + ent, prompt -> pro + pr + om => pr + om et pas pro + m); on doit décider quelle abréviation conserver 
						- soit c'est une ambiguïté recouvrante (pas d'empiètement partiel) : les édITIONs ou nous éditIONs 
					-->
					<xsl:if test="$dbg"><xsl:message select="'- - empiètement dans les séquences ou ambiguïté sans empiètement partiel- -'"/></xsl:if>
					<xsl:variable as="element()*" name="motAbrSort2">
						<!--<saxon:iterate select="1 to count($motAbrSort)" xmlns:saxon="http://saxon.sf.net/" xsl:extension-element-prefixes="saxon">-->
						<xsl:for-each select="1 to count($motAbrSort)">
							<!--<xsl:param name="erase" as="xs:boolean" select="false()"/> 
									On peut se passer du erase: ma séquence n'arrivera pas à se déclencher car elle sera détruite par la précédente
									Vérifier quand même dans le cas d'une utilisation de deux séquences genre dissitrucasse, ss
								-->
							<xsl:variable name="i" select="." as="xs:integer"/>
							<!-- liste des syllabes en conflit de recouvrement avec la syllabe examinée-->
							<xsl:variable name="conflit" as="element()*" 
								select="for $j in $i + 1 to count($lg) return
								if ($lg[$i] &gt;= number($motAbrSort[$j]/deb)) then $motAbrSort[$j] else()"/>
							<!-- si la syllabe appartient peut-être aux ambiguîtés non résolues connues -->
							<xsl:variable name="regexAmbi" select="$l_amb_nResol[matches(.,$motAbrSort[$i]/noir)][1]" as="xs:string?"/>
							<xsl:variable name="regexG2" select="replace($regexAmbi,'\(.*\)','')"/>
							<!-- on vérifie d'une part si le mot contient une ambiguïté et d'autre part que la syllable fait bien partie de partie ambigue -->
							<xsl:variable name="ambiNR" as="element()?" select="if(nat:matches-any-of($mot, $regexAmbi) and contains($regexG2,$motAbrSort[$i]/noir)) then $motAbrSort[$i] else ()"/>
							<!--<xsl:message select="('%%%%%%%%%%%%%%%%%%%%%',$regexAmbi,'-',$regexG2,'-',$ambiNR,'-',$motAbrSort[$i])"/>-->
							<xsl:if test="$dbg"><xsl:message select="if(count($conflit) &gt;0) then (' - - en conflit avec ', $motAbrSort[$i], ' :', $conflit) else ('Pas de conflits')"/></xsl:if>
							<xsl:if test="$dbg"><xsl:message select="(if(not(empty($ambiNR))) then (' - - ambigüité connue non resolvable pour ', $motAbrSort[$i]) else ('Pas d''ambiguité'),'ambiNR:',$ambiNR)"/></xsl:if>
							<xsl:variable name="syllabe" as="element()?">
								<xsl:choose>
									<xsl:when test="position()=last() or count($conflit)=0 ">
										<xsl:choose>
											<!-- si l'abréviation suivante est la même mais que le noir est différent => on ne garde pas l'assemblage -->
											<xsl:when test="$lg[$i] = number($motAbrSort[$i + 1]/deb) and $motAbrSort[$i]/abr = $motAbrSort[$i + 1]/abr and not($motAbrSort[$i]/noir = $motAbrSort[$i + 1]/noir)">
												<!--<xsl:message select=" ('  * Séquence d''assemblages non compatible:', $motAbrSort[$i]/noir, ' et ' , $motAbrSort[$i + 1]/noir)"/>-->
											</xsl:when>
											<!-- séquence à supprimer car doublon -->
											<xsl:when test="$motAbrSort[$i + 1]/abr = $motAbrSort[$i]/abr and $motAbrSort[$i + 1]/noir = $motAbrSort[$i]/noir
											and $motAbrSort[$i + 1]/deb = $motAbrSort[$i]/deb"/>
											<xsl:otherwise>
												<xsl:copy-of select="$motAbrSort[$i]"/><!--<xsl:message select="'dernier'"/>-->
											</xsl:otherwise>
										</xsl:choose>
									</xsl:when>
									<xsl:otherwise>
										<!-- diagnostic pour savoir si on conserve l'abréviation -->
										<xsl:variable as="xs:integer*" name="valide">
											<!-- si ambiguïté -->
											<xsl:choose>
												<xsl:when test="not(empty($ambiNR))"><xsl:value-of select="2"/></xsl:when>
												<xsl:otherwise>
													<xsl:for-each select="$conflit">
														<xsl:variable as="xs:integer" select="position()" name="j"/>
														<!--<xsl:message select="' conflits:'"/>-->
														<!--<xsl:message select="('lg $i:', $lg[$i], ' $conflit[$j]/deb:', number($conflit[$j]/deb),
															' $motAbrSort[$i]/abr:', $motAbrSort[$i]/abr, ' $conflit[$j]/abr:', $conflit[$j]/abr, ' noirs:', $motAbrSort[$i]/noir, ' ',$conflit[$j]/noir)"/>-->
														<xsl:choose>
															<!-- ne pas utiliser l'abréviation -->
															<!--<xsl:when test="$erase" />-->
															<!-- si l'abréviation suivante est la même mais que le noir est différent => on ne garde pas l'assemblage -->
															<xsl:when test="$lg[$i] = number($conflit[$j]/deb) and $motAbrSort[$i]/abr = $conflit[$j]/abr and not($motAbrSort[$i]/noir = $conflit[$j]/noir)">
																<!--<xsl:message select=" ('  * Séquence d''assemblages non compatible:', $motAbrSort[$i]/noir, ' et ' , $conflit[$j]/noir)"/>-->
																<xsl:value-of select="0"/>
															</xsl:when>
															<!-- pas de pb avec la syllable actuelle: pas de recouvrement -->
															<xsl:when test="$lg[$i] &lt;= number($conflit[$j]/deb) or ($lg[$i] >= $lg[$i+$j] 
																and number($motAbrSort[$i]/deb) &lt;= number($conflit[$j]/deb))">
																<xsl:value-of select="1000"/>
															</xsl:when>
															<!-- il y a un recouvrement, mais l'abréviation en conflit fait partie de la liste des pass2 -->
															<xsl:when test="functx:is-value-in-sequence($conflit[$j]/abr, $cgenPass2RulesBr)">
																<xsl:value-of select="1000"/>
															</xsl:when>
															<!-- la sequence fait partie des pass2 et il y a recouvrement -->
															<xsl:when test="$i > 1 and functx:is-value-in-sequence($motAbrSort[$i]/abr, $cgenPass2RulesBr) and
																	$lg[$i - 1] > $lg[$i]">
																<xsl:value-of select="0"/>
															</xsl:when>
															<!-- la séquence n'appartient pas à une ambiguité connue-->
															<xsl:when test="every $ch in $l_amb_connues satisfies not(contains($ch,$motAbrSort[$i]/noir)) ">
																<!--<xsl:for-each select="$l_amb_connues">
																	-<xsl:message select="(not(contains($motAbrSort[$i]/noir,.)), 'val:', ., 'Abr:', $motAbrSort[$i]/noir)"/>
																</xsl:for-each>-->
																<!--<xsl:message select="' EMPIET:',$motAbrSort[$i],' SUIV:',$motAbrSort[$i+$j], ' conflit:', $conflit[$j]"/>-->
																<xsl:variable name="motDecoup" as="xs:string" select="nat:toBlack(nat:hyphenate(nat:toBrUTF8($mot),$coupe))"/>
																<!--<xsl:message select="('  - mot decoup:', $motDecoup)"/>-->
																<!-- première tentative: on conserve la première abréviation uniquement si elle recouvre une syllable -->
																<xsl:variable name="decoupChars" select="functx:chars($motDecoup)" as="xs:string*"/>
																<!--<xsl:message select="('  - decoupCh:', $decoupChars)"/>-->
																<!--<xsl:message select="'sous-chaine', substring($motDecoup,1,string-length(replace(substring($motDecoup,1,$lg[$i]),concat('[^',$coupe,']'),'')) + $lg[$i] +1),translate(substring($motDecoup,1,$lg[$i]),$coupe,'')"/>-->
																<!-- c'est là qu'il faut prioriser les abréviations -->
																<xsl:value-of select="if (ends-with(substring($motDecoup,1,string-length(replace(substring($motDecoup,1,$lg[$i]),concat('[^',$coupe,']'),'')) + $lg[$i] +1),$coupe)) then 1000 else(0)"/>
															</xsl:when>
															<xsl:otherwise>
																<xsl:value-of select="2"/>
															</xsl:otherwise>
														</xsl:choose>
													</xsl:for-each>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:variable>
										<!--<xsl:message select="$valide"/>-->
										<!-- la séquence ne doit pas être supprimée -->
										<xsl:if test="not(functx:is-value-in-sequence(0,$valide))">
											<xsl:choose>
												<!-- cest une ambigüité -->
												<xsl:when test="functx:is-value-in-sequence(2,$valide)">
													<xsl:if test="$dbg"><xsl:message select="'résolution automatique de l''ambiguïté'"/></xsl:if>
													<xsl:variable name="indexAmb" select="index-of($l_amb_connues,$l_amb_connues[contains(.,$motAbrSort[$i]/noir)][1])"/>
													<xsl:variable name="regLevAmb" select="$l_amb_resol[$indexAmb * 2 - 1]"/>
													<xsl:variable name="regLevAmbNR" select="$l_amb_nResol[$indexAmb * 2 - 1]"/>
													<xsl:if test="$dbg"><xsl:message select="($indexAmb,' regex solution:',$regLevAmb)"/></xsl:if>
													<xsl:if test="$dbg"><xsl:message select="('resu:',matches($mot,$regLevAmb),' resol:', $l_amb_resol[2 * $indexAmb], ' abr=', $motAbrSort[$i]/abr)"/></xsl:if>
													<xsl:choose>
														<xsl:when test="matches($mot,$regLevAmb) and $l_amb_resol[2 * $indexAmb]=$motAbrSort[$i]/abr">
															<xsl:copy-of select="$motAbrSort[$i]"/>
														</xsl:when>
														<!-- c'est une ambiguïté connue -->
														<xsl:when test="matches($mot,$regLevAmbNR)">
															<xsl:if test="$dbg"><xsl:message select="'** ambiguïté non résolvable connue: génération'"/></xsl:if>
															<!--<xsl:message select="$motAbrSort[$i]"/>-->
															<!-- TODO bourrin -->
															<xsl:variable name="message" select="if($mot='convient') then $l_amb_nResol_conv[2] else $l_amb_nResol_mes[$indexAmb * 2]"/>
															<xsl:element name="syll">
																<xsl:element name="deb"><xsl:value-of select="$motAbrSort[$i]/deb"/></xsl:element>
																<xsl:element name="noir"><xsl:value-of select="$l_amb_connues[$indexAmb]"/></xsl:element>
																<xsl:element name="abr"><xsl:value-of select="('[', $l_amb_nResol[2 * $indexAmb], ']')"/></xsl:element>
																<!--
																<xsl:element name="ambiguity">
																<amb_text><xsl:value-of select="$message"/></amb_text>
																<context>
																	<preceding><xsl:value-of select="nat:getPrecedingValues($noeud,6)"/></preceding>
																	<amb_expr><xsl:value-of select="nat:getOriginalValue($noeud)"/></amb_expr>
																	<following><xsl:value-of select="nat:getFollowingValues($noeud,6)"/></following>
																</context>
																<value id="1"><xsl:value-of select="$l_amb_nResol[2 * $indexAmb]"/></value>
																-->
																<xsl:element name="amb_text">
																	<xsl:value-of select="concat('[Amb:',$message,'|',
																	nat:getPrecedingValues($noeud,6),'|',nat:getOriginalValue($noeud),'|',nat:getFollowingValues($noeud,6),'|',$l_amb_nResol[2 * $indexAmb],']')"/>
																</xsl:element>
															</xsl:element>
														</xsl:when>
														<xsl:otherwise>
															<xsl:if test="$dbg"><xsl:message select="'** ambiguïté non connue: suppression de l''abréviation'"/></xsl:if>
														</xsl:otherwise>
													</xsl:choose>
												</xsl:when>
												<!-- conservation de la séquence -->
												<xsl:otherwise><xsl:copy-of select="$motAbrSort[$i]"/></xsl:otherwise>
											</xsl:choose>
										</xsl:if>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:variable>
							<xsl:copy-of select="$syllabe"/>
							<!--<xsl:message select="'   conservé:',$syllabe"/>-->
							<!--<saxon:continue/>
								<xsl:with-param name="erase" select="if($syllabe/@eraseNext='yes') then true() else (false())"/>
							</saxon:continue>-->
						</xsl:for-each>
						<!--</saxon:iterate>-->
					</xsl:variable>
					<xsl:if test="$dbg"><xsl:message select="('Liste des séquences avant abréviation:', $motAbrSort2, ':' ,  string-join($motAbrSort2/noir,''), ':', string-join($motAbrSort2/abr,''))"/></xsl:if>
					<!--<xsl:message select="('- - Avant Abréviation: nb=', count($motAbrSort2),',abrS2=',$motAbrSort2,count($motAbrSort2/ambiguity),$motAbrSort2/noir)"/>-->
					<xsl:choose>
						<xsl:when test="count($motAbrSort2/ambiguity)=0">
							<xsl:variable name="fin">
								<xsl:choose>
									<xsl:when test="count(($motAbrSort2/noir))=0">
										<xsl:value-of select="$mot"/>
									</xsl:when>
									<xsl:otherwise>
										<!--écriture des abréviations -->			
										<!-- suppression des séquences recouvertes -->
										<xsl:variable name="motAbrSort3" select="for $i in $motAbrSort2 return
											if($i/deb + string-length($i/noir) = string-length($mot) or not(some $j in $motAbrSort2[not(. = $i)] satisfies (number($j/deb) &lt;= number($i/deb) and $j/deb + string-length($j/noir) &gt;= $i/deb + string-length($i/noir)))) then $i else()"/>
										<xsl:if test="$dbg"><xsl:message select="('Liste des séquences filtrées avant abréviation', $motAbrSort3, ':' ,  string-join($motAbrSort3/noir,''), ':', string-join($motAbrSort3/abr,''))"/></xsl:if>
										<xsl:variable name="chSyll" as="xs:string*" select="($motAbrSort3/noir)"/><!-- syllabes noires abrégées -->
										<xsl:variable name="chSyllAbr" as="xs:string*" select="($motAbrSort3/abr)"/><!-- syllabes abrégées -->
										<xsl:variable name="debuts" as="xs:integer*" select="($motAbrSort3/deb)"/>

										<xsl:variable name="finAbr"><!-- il faut supprimer ici les recouvrements inutiles -->
											<xsl:variable name="apresAbr" select="nat:replace-first-multi($mot,$chSyll,$chSyllAbr,$debuts)"/>
											<!-- désabréviation de la première séquence s'il n'y a que des points inférieurs -->
											<xsl:value-of select="if ((functx:contains-any-of($apresAbr, $l_pt_sup) or string-length($apresAbr) &lt; 2) 
											or not($termin) or preceding-sibling::*[1]/@doSpace = 'false' or upper-case(substring(.,1,1)) = substring(.,1,1)) then $apresAbr 
												else (nat:replace-first-multi($mot,$chSyll[position()>1],$chSyllAbr[position()>1],$debuts[position()>1]))"/>
										</xsl:variable>
										<xsl:message select="('voilà',$finAbr)"/>
										<xsl:value-of select="$finAbr"/>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:variable>
							<!--<xsl:message select="('séquence recouvertes:',$chSyll, ';', $chSyllAbr)"/>-->
							<xsl:call-template name="symbCompAbr">
								<xsl:with-param name="mot" select="$fin"/>
							</xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
							<xsl:message select="('eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee',$motAbrSort2)"/>
							<xsl:element name="mot">
							<xsl:copy-of select="$motAbrSort2"/><!-- copie des syllabes telles quel; TODO Bruno: garder juste les amb et transmettre le reste pour symbCompAbr ou bijection-->
							</xsl:element>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="symbCompAbr">
	<xsl:param name="mot"/><!-- as="xs:string"/>-->
	<xsl:param name="ivb" select="true()" as="xs:boolean" tunnel="yes"/>
	<xsl:message select="('MOTDeb',$mot)"/>
	<xsl:choose>
		<!-- il y a une ambiguïté dans un assemblage -->
		<xsl:when test="contains($mot,'[Amb')">
			<xsl:variable name="avant" select="functx:substring-before-match($mot,'\[Amb')"/>
			<xsl:call-template name="symbCompAbr">
				<xsl:with-param name="mot" select="$avant"/>
			</xsl:call-template>
			<xsl:value-of select="concat('[Amb',substring-after($mot,'[Amb'))"/>
		</xsl:when>
		<xsl:when test="functx:contains-any-of($mot,$symbCompBl)">
			<!--<xsl:if test="starts-with($mot,'-')"><xsl:message select="'mot ',$mot,'tiret ',$tiret,'subs ',substring($mot,2,1)" /></xsl:if>-->
			<xsl:call-template name="finBijection">
				<xsl:with-param name="mot" select="functx:replace-multi($mot,$symbCompRegBl,$symbCompBr)"/>
			</xsl:call-template>
		</xsl:when>
		<xsl:otherwise>
			<xsl:call-template name="finBijection">
				<xsl:with-param name="mot" select="$mot"/>
			</xsl:call-template>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>


</xsl:stylesheet>
