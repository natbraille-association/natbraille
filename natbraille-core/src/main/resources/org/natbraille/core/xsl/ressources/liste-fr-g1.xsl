<?xml version='1.1' encoding="UTF-8" ?>
<!--
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
-->

<!-- version: 1.0 -->
<!DOCTYPE xsl:stylesheet SYSTEM "nat://system/xsl/mmlents/windob.dtd">
<xsl:stylesheet version="3.0" xmlns:xsl='http://www.w3.org/1999/XSL/Transform' 
xmlns:saxon='http://icl.com/saxon'
xmlns:xs='http://www.w3.org/2001/XMLSchema'
xmlns:fn='http://www.w3.org/2005/xpath-functions'
xmlns:fo='http://www.w3.org/1999/XSL/Format'
xmlns:m='http://www.w3.org/1998/Math/MathML'
xmlns:functx='http://www.functx.com'
xmlns:lit='espacelit'
xmlns:doc='espaceDoc'>

<!--TODO NBA faire une map (XSL 3)-->

<!-- tag name conversion: to be removed if convertissor use the new terminology -->
<xsl:variable name="tagConversion" select="map {
	'phrase' : 'par',
	'titre' : 'title'
	}" as="map(xs:string, xs:string)"/>

<!-- 
	Préfixes
-->
<xsl:variable name="capsMap" select="map {
	'name' : 'caps',
	'pref' : '&pt46;',
	'word': '&pt46;&pt46;',
	'multiStart': '&pt25;&pt46;',
	'multiLast': '&pt46;',
	'multiEnd': ''	}" as="map(xs:string, xs:string)"/>

<xsl:variable name="numMap" select="map {
	'name' : 'num',
	'pref' : '&pt6;',
	'word': '&pt6;',
	'multiStart': '',
	'multiLast': '',
	'multiEnd': ''	}" as="map(xs:string, xs:string)"/>

<xsl:variable name="capPref" as="xs:string" select="'&pt46;'"/>
<xsl:variable name="capPrefWord" as="xs:string" select="'&pt46;&pt46;'"/>
<xsl:variable name="capPrefMulti" as="xs:string" select="'&pt25;&pt46;'"/>
<xsl:variable name="capPrefMultiLast" as="xs:string" select="'&pt46;'"/>

<xsl:variable name="emphPref" as="xs:string" select="'&pt456;'"/>
<xsl:variable name="emphPrefMulti" as="xs:string" select="'&pt25;&pt456;'"/>

<xsl:variable name="numPref" as="xs:string" select="'&pt6;'"/>
<xsl:variable name="numSep" as="xs:string" select="'&pt3;'"/>

<xsl:variable name="ivbPref" as="xs:string" select="'&pt56;'"/>

<xsl:variable name="codePref" as="xs:string" select="'&pt6;&pt3;'"/>

<xsl:variable name="expPref" as="xs:string" select="'&pt4;'"/>

<xsl:variable name="indPref" as="xs:string" select="'&pt26;'"/>

<xsl:variable name="noteCall" as="xs:string" select="'&pt346;'"/>

<!--
	constantes contenu
-->
<xsl:variable name="listPuce" as="xs:string" select="'&pt36;&pt36;'"/>
<xsl:variable name="listOrdered" as="xs:string" select="'&pt36;&pt36;'"/>
<!--
	Caractères
-->
<xsl:variable name="hyphenBr" as="xs:string" select="'&pt36;'"/>
<xsl:variable name="aposBr" as="xs:string" select="'&pt3;'"/>

<!-- 
	Alphabets 
-->
<xsl:variable name="mapPuncts" select="map {
	',' : '&pt2;',
	';' : '&pt23;',
	':' : '&pt25;',
	'.' : '&pt256;',
	'?' : '&pt26;',
	'!' : '&pt235;',
	'&quot;' : '&pt2356;',
	'«' : '&pt2356;',
	'»' : '&pt2356;',
	'“' : '&pt2356;',
	'”' : '&pt2356;',
	'a‘' : '&pt2356;',
	'a’' : '&pt2356;',
	'&lsquo;' : '&pt3;',
	'&rsquo;' : '&pt3;',
	'&acute;' : '&pt3;',
	'&prime;' : '&pt3;',
	'(' : '&pt236;',
	')' : '&pt356;',
	'¡' : '&pt235;',
	'¿' : '&pt26;'
 }" as="map(xs:string, xs:string)"/>

<!-- ponctuation 1 signe braille -->
<xsl:variable name="punct1br" as="xs:string">
	<xsl:text>&pt2;&pt23;&pt25;&pt256;&pt26;&pt235;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt3;&pt3;&pt3;&pt3;&pt236;&pt356;&pt235;&pt26;</xsl:text>
</xsl:variable>
<!-- ponctuation 1 signe noir -->
<xsl:variable name="punct1bl" as="xs:string" select="',;:.?!&quot;«»“”‘’&lsquo;&rsquo;&acute;&prime;()¡¿'"/>

<!-- ponctuation n signes noir -->
<xsl:variable name="punctNbl" as="xs:string*" select="('...','…','&mldr;','&vellip;','&hellip;','&dtdot;','&ldots;','&ctdot;','&utdot;','''','\[','\]','\{','\}','\*')"/>

<!-- ajout Fred liste de toutes les ponctuations -->
<xsl:variable name="punctAllBl" as="xs:string*" select="(functx:chars($punct1bl),$punctNbl)" />

<xsl:variable name="punctNbr" as="xs:string*" select="('&pt256;&pt256;&pt256;','&pt256;&pt256;&pt256;','&pt256;&pt256;&pt256;','&pt256;&pt256;&pt256;','&pt256;&pt256;&pt256;','&pt256;&pt256;&pt256;','&pt256;&pt256;&pt256;','&pt256;&pt256;&pt256;','&pt256;&pt256;&pt256;','&pt3;','&pt45;&pt236;','&pt356;&pt12;','&pt6;&pt6;&pt236;','&pt356;&pt3;&pt3;','&pt5;&pt35;')"/>

<xsl:variable name="noSpacePunctEnd" select="'¡¿([{«“‘&lsquo;'" as="xs:string"/>
<xsl:variable name="noSpacePunctOpen" select="'&quot;:([{'" as="xs:string"/>

<!-- symboles composés -->
<xsl:variable name="symbCompRegBl" as="xs:string*" select="('^-|-$','&ndash;','&mdash;','©','°','§','®','&trade;','&amp;','~','\*','#','%','‰','_','\\','¢','€','£','\$','¥','&lt;','&gt;','≤','≥','²','³','&sup2;','&sup3;',$lGrecBl,$l_phonBl)"/>

<xsl:variable name="symbCompBl" as="xs:string*" select="('-','&ndash;','&mdash;','©','°','§','®','&trade;','&amp;','~','*','#','%','‰','_','\','¢','€','£','$','¥','&lt;','&gt;','≤','≥','²','³','&sup2;','&sup3;',$lGrecBl,$l_phonBl)"/>

<xsl:variable name="symbCompBr" as="xs:string*" select="('&pt36;&pt36;','&pt36;&pt36;','&pt36;&pt36;','&pt5;&pt14;','&pt5;&pt135;','&pt5;&pt1234;','&pt5;&pt1235;','&pt5;&pt2345;','&pt5;&pt123456;','&pt5;&pt26;','&pt5;&pt35;','&pt5;&pt3456;','&pt5;&pt346;','&pt5;&pt346;&pt346;','&pt5;&pt36;','&pt5;&pt34;','&pt45;&pt14;','&pt45;&pt15;','&pt45;&pt123;','&pt45;&pt234;','&pt45;&pt13456;','&pt5;&pt126;','&pt5;&pt345;','&pt45;&pt126;','&pt45;&pt345;','&pt4;&pt126;','&pt4;&pt146;','&pt4;&pt126;','&pt4;&pt146;',$lGrecBr,$l_phonBr)"/>

<!--ligatures -->
<!-- TODO NBA add other ligatures, see //docs/misc/ligatures.txt -->
<xsl:variable name="ligatures" select="'ﬀ','ﬁ','ﬂ','ﬃ','ﬄ','ﬅ','ﬆ'" as="xs:string*"/>
<xsl:variable name="sansLigatures" select="'ff','fi','fl','ffi','ffl','ft','st'" as="xs:string*"/>

<!-- phonétique -->
<xsl:variable name="l_phonBl" as="xs:string*" select="('ʈ','ɖ')"/>
<xsl:variable name="l_phonBr" as="xs:string*" select="('&pt256;&pt2345;','&pt256;&pt145;')"/>

<!--GREC-->
<xsl:variable name="lGrecBl" as="xs:string*" select="('&alpha;','&beta;','&gamma;','&delta;','&epsi;','&epsiv;','&zeta;','&eta;','&theta;','&thetav;','&iota;','&kappa;','&lambda;','&mu;','&nu;','&xi;','&omicron;','&pi;','&piv;','&rho;','&rhov;','&sigma;','&sigmav;','&tau;','&upsilon;','&phi;','&phiv;','&chi;','&psi;','&omega;','&Agr;','&Bgr;','&Gamma;','&Delta;','&Egr;','&Zgr;','&EEgr;','&Theta;','&Igr;','&Kgr;','&Lambda;','&Mgr;','&Ngr;','&Xi;','&Ogr;','&Pi;','&Rgr;','&Sigma;','&Tgr;','&Upsilon;','&Phi;','&KHgr;','&Chi;','&Psi;','&Omega;')"/>

<xsl:variable name="lGrecBr" as="xs:string*" select="('&pt45;&pt1;','&pt45;&pt12;','&pt45;&pt1245;','&pt45;&pt145;','&pt45;&pt15;','&pt45;&pt15;','&pt45;&pt1356;','&pt45;&pt125;','&pt45;&pt245;','&pt45;&pt245;','&pt45;&pt24;','&pt45;&pt13;','&pt45;&pt123;','&pt45;&pt134;','&pt45;&pt1345;','&pt45;&pt1346;','&pt45;&pt135;','&pt45;&pt1234;','&pt45;&pt12456;','&pt45;&pt1235;','&pt45;&pt1235;','&pt45;&pt234;','&pt45;&pt234;','&pt45;&pt2345;','&pt45;&pt136;','&pt45;&pt124;','&pt45;&pt124;','&pt45;&pt12345;','&pt45;&pt13456;','&pt45;&pt2456;','&pt46;&pt45;&pt1;','&pt46;&pt45;&pt12;','&pt46;&pt45;&pt1245;','&pt46;&pt45;&pt145;','&pt46;&pt45;&pt15;','&pt46;&pt45;&pt1356;','&pt46;&pt45;&pt125;','&pt46;&pt45;&pt245;','&pt46;&pt45;&pt24;','&pt46;&pt45;&pt13;','&pt46;&pt45;&pt123;','&pt46;&pt45;&pt134;','&pt46;&pt45;&pt1345;','&pt46;&pt45;&pt1346;','&pt46;&pt45;&pt135;','&pt46;&pt45;&pt1234;','&pt46;&pt45;&pt1235;','&pt46;&pt45;&pt234;','&pt46;&pt45;&pt2345;','&pt46;&pt45;&pt136;','&pt46;&pt45;&pt124;','&pt46;&pt45;&pt12345;','&pt46;&pt45;&pt12345;','&pt46;&pt45;&pt13456;','&pt46;&pt45;&pt2456;')"/>

<xsl:variable name="l_grec_min" as="xs:string">&alpha;&beta;&gamma;&delta;&epsi;&epsiv;&zeta;&eta;&theta;&thetav;&iota;&kappa;&lambda;&mu;&nu;&xi;&omicron;&pi;&piv;&rho;&rhov;&sigma;&sigmav;&tau;&upsilon;&phi;&phiv;&chi;&psi;&omega;ϵ</xsl:variable>
<xsl:variable name="l_grec_maj" as="xs:string">&Agr;&Bgr;&Gamma;&Delta;&Egr;&Zgr;&EEgr;&Theta;&Igr;&Kgr;&Lambda;&Mgr;&Ngr;&Xi;&Ogr;&Pi;&Rgr;&Sigma;&Tgr;&Upsilon;&Phi;&KHgr;&Chi;&Psi;&Omega;</xsl:variable>
<xsl:variable name="l_alphanumgrec" as="xs:string" select="concat($l_alphabet,$l_chiffres,$l_grec_min,$l_grec_maj)" />

<xsl:variable name="l_maj" as="xs:string">ABCDEFGHIJKLMNOPQRSTUVWXYZÀÂÉÈÊËÎÏÔÙÛÜÁÍÓÚÑÌÒÄÖ&Ccedil;&AElig;&OElig;</xsl:variable>
<xsl:variable name="seqMaj" as="xs:string*" select="('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','À','Â','É','È','Ê','Ë','Î','Ï','Ô','Ù','Û','Ü','Á','Í','Ó','Ú','Ñ','Ì','Ò','Ä','Ö','&Ccedil;','&AElig;','&OElig;')"/>
<xsl:variable name="l_maj_A" as="xs:string">AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA</xsl:variable>
<xsl:variable name="l_min" as="xs:string">abcdefghijklmnopqrstuvwxyzàâéèêëîïôùûüáíóúñìòäö&cedil;&aelig;&oelig;</xsl:variable>
<xsl:variable name="seqMin" as="xs:string*" select="('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','à','â','é','è','ê','ë','î','ï','ô','ù','û','ü','á','í','ó','ú','ñ','ì','ò','ä','ö','&ccedil;','&aelig;','&oelig;')"/>
<xsl:variable name="l_min_a" as="xs:string">aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa</xsl:variable>
<xsl:variable name="l_num" as="xs:string">0123456789+-×÷=,.&sup2;&sup3;</xsl:variable>
<xsl:variable name="seqNum" as="xs:string*" select="('0','1','2','3','4','5','6','7','8','9','+','×','÷','=','&sup2;','&sup3;')"/>
<xsl:variable name="l_amb" as="xs:string">ÂÊÎÔÛËÏÜŒWâêîôûëïüœ"</xsl:variable>
<xsl:variable name="l_amb_ahat" as="xs:string">ââââââââââââââââââââ</xsl:variable>
<xsl:variable name="l_alphabet" as="xs:string">abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ</xsl:variable>
<xsl:variable name="l_chiffres" as="xs:string">1234567890,.</xsl:variable>
<xsl:variable name="seqDigit" as="xs:string*" select="('1','2','3','4','5','6','7','8','9','0')"/>
<xsl:variable name="seq_plusminus" select="('+','-','&plus;','&minus;','&dash;','&ndash;','&mdash;')"/>

<!--
	STYLES
-->
<xsl:variable name="noteTrName" as="xs:string" select="'noteTr'"/>
<xsl:variable name="styleNote" as="xs:string*" select="($noteTrName,'Footnote','Endnote')"/>

<xsl:variable name="chaine_vide" as="xs:string" select="''"/>

</xsl:stylesheet>
