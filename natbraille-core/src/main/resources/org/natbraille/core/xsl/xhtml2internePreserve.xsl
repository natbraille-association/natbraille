<?xml version='1.1' encoding="UTF-8" ?>
<!DOCTYPE xsl:stylesheet SYSTEM "nat://system/xsl/mmlents/windob.dtd">
<!--
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
-->

<!-- version 1.5 -->

<!-- IMPORTANT : dans cette xsl, tout ce qui s'appelle "style(s)" fait référence aux styles css du doc xhtml,
tout ce qui s'appelle stylist fait référence au fichier xml stylist -->

<xsl:stylesheet version="2.0" 

xmlns:xsl='http://www.w3.org/1999/XSL/Transform' 
xmlns:nat='http://natbraille.free.fr/xsl' 
xmlns:saxon='http://icl.com/saxon'
xmlns:fn='http://www.w3.org/2005/xpath-functions'
xmlns:xs='http://www.w3.org/2001/XMLSchema'
xmlns:xhtml="http://www.w3.org/1999/xhtml" 
xmlns:m='http://www.w3.org/1998/Math/MathML'
xmlns:functx='http://www.functx.com'
xmlns:doc='espaceDoc'> 

<xsl:import href="nat://system/xsl/functions/functx.xsl" /> <!-- functx functions -->
<xsl:param name="dtd" select="'xsl/mmlents/windob.dtd'" as="xs:string"/>
<xsl:param name="processImage" select="false()" as="xs:boolean"/>
<xsl:param name="chemistryStyle" select="'chimie'" as="xs:string"/>
<xsl:param name="insertIndexBraille" select="true()"/>
<xsl:param name="indexTableName" select="'Table des matières'"/>
<xsl:param name="stylistFile" select="'./ressources/styles_default.xml'" as="xs:string" />
<xsl:param name="preserveTag" select="true()" as="xs:boolean"/>

<xsl:variable as="node()*" name="stylist" select="document($stylistFile)/styles/*"/>
<xsl:variable name="tableLitStyle" as="xs:string" select="'Table'"/>
<xsl:variable name="stylesChimie" as="xs:string*" select="$stylist[@special=('chimie','chemistry')]/@name" />
<xsl:variable name="stylesG0" as="xs:string*" select="$stylist[@special=('g0','G0')]/@name" />
<xsl:variable name="stylesG1" as="xs:string*" select="$stylist[@special=('g1','G1')]/@name" />
<xsl:output method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="no"/>


<xsl:strip-space elements = "xhtml:p xhtml:head xhtml:meta xhtml:title xhtml:link xhtml:style xhtml:ul xhtml:ol xhtml:dl xhtml:dd xhtml:li m:* m:math m:semantics m:mrow xhtml:table xhtml:td xhtml:tbody xhtml:tr xhtml:thead xhtml:th" />

<xsl:variable name="l_ponct">¡¿”&quot;’,.:;!?»…)]}\}«“‘([{}&lsquo;&rsquo;&acute;&prime;&mldr;&vellip;&hellip;&dtdot;&ldots;&ctdot;&utdot;</xsl:variable>
<xsl:variable name="style_Standard" as="xs:string" select="'Standard'" />



<xsl:template match="/">
<!--
	vivien : même modif que faite par frédéric sur xhtml2interne pour empêcher le
	 When 'standalone' or 'doctype-system' is specified, the document must be well-formed; but this document contains a top-level text node
; SystemID: nat://system/xsl/xhtml2internePreserve.xsl; Line#: 65; Column#: -1
65
	<xsl:text disable-output-escaping="yes">
		&lt;!DOCTYPE doc:doc SYSTEM "
	</xsl:text>
	<xsl:value-of select="$dtd"/>
	<xsl:text disable-output-escaping="yes">"&gt;</xsl:text>
 -->
	<!--<xsl:for-each select="$styles">-->
		<!--<xsl:message select="."/>-->
		<!--<xsl:message select="'********'"/>
	</xsl:for-each>-->
	<doc:doc xmlns:doc="espaceDoc">
	<xsl:apply-templates select="*|text()|processing-instruction()" />
	</doc:doc>
</xsl:template>

<!--math recopiés tels quel-->
<xsl:template match="m:math">
	<phrase>
	<xsl:choose>
		<xsl:when test="some $d in ancestor::* satisfies $d/@class=$stylesChimie">
			<chimie>
				<xsl:copy-of select="./*"/>
			</chimie>
		</xsl:when>
		<xsl:otherwise>
			<xsl:copy-of select="."/>
		</xsl:otherwise>
	</xsl:choose>
	</phrase>
</xsl:template>

<!-- balises à recopier sans transcription -->
<xsl:template match="xhtml:script">
	<xsl:copy-of select="."/>
</xsl:template>

<xsl:template match="xhtml:head"/>

<xsl:template match="xhtml:html">
	<xsl:apply-templates/>
</xsl:template>


<xsl:template match="*">
	<xsl:copy>
		<xsl:copy-of select="@*"/>

					<xsl:apply-templates select="*|text()"/>
	</xsl:copy>
</xsl:template>

<xsl:template match="text()">
	<phrase>
		<lit>
			<xsl:call-template name="remplaceEspace">
				<xsl:with-param name="chaine" select="."/>
			</xsl:call-template>
		</lit>
	</phrase>
</xsl:template>


<xsl:template name="remplaceEspace">
	<xsl:param name="chaine" />
	<xsl:param name="doSpace" as="xs:boolean" select="true()" tunnel="yes" />
	<xsl:param name="attrValues" as="xs:string*" select="('0','','true')" tunnel="yes"/>
	<xsl:param name="appel" as="xs:boolean" select="false()" tunnel="yes"/>
	
	<xsl:variable name="chaine2">
		<!-- on remplace insécables, tabulation, et saut de ligne par des espaces, on vire les espaces du début -->
		<xsl:variable name="tmp" select="fn:replace(translate($chaine,'&nbsp;	&#10;&#13;','    '),'^[ ]+','')"/>
		<!-- on remplace les apostrophes pourris (non sécables) et les tirets nuls, et les saisies erronées des oeu et oei	-->
		<xsl:value-of select="replace(replace(replace($tmp,'‑','-'),'(.)[’](.)','$1''$2'),'oe(i|u)','œ$1')"/>
	</xsl:variable>
	
	<!--<xsl:variable name="dernier" select="position()=last()" as="xs:boolean" />-->

	<!-- on découpe la chaine par espaces et on garde que les non vides -->
	<xsl:variable name="chaines" as="xs:string*" select="tokenize($chaine2,'\s')[string(.)]"/>
	<xsl:for-each select="$chaines">
		<xsl:choose>
			<!-- ponctuation seule -->
			<xsl:when test=". =''"/>
			<xsl:when test="string-length(.)=1 and (contains(concat($l_ponct, '-'),substring(.,1,1)))">
				<ponctuation>
					<xsl:value-of select="." />
				</ponctuation>
			</xsl:when>
			<xsl:when test="string-length(.)=1 and starts-with(.,'''')">
				<ponctuation>&apos;</ponctuation>
			</xsl:when>
			<xsl:when test="string-length(.)=3 and substring(.,1,3)='...'">
				<ponctuation>...</ponctuation>
			</xsl:when>
			<!-- ponctuation au début du mot sans espace -->
			<xsl:when test="contains($l_ponct,substring(.,1,1))">
				<ponctuation>
				<xsl:value-of select="substring(., 1, 1)" />
				</ponctuation>
				<xsl:call-template name="remplaceEspace">
					<xsl:with-param name="chaine">
						<xsl:value-of select="substring(.,2,string-length(.))" />
					</xsl:with-param>
				</xsl:call-template>
			</xsl:when>
			<!-- ponctuation à la fin du mot sans espace -->
			<xsl:when test="string-length(.)&gt;2 and substring(., string-length(.)-2, string-length(.))='...'">
				<xsl:call-template name="remplaceEspace">
					<xsl:with-param name="chaine">
						<xsl:value-of select="substring(.,1,string-length(.)-3)" />
					</xsl:with-param>
				</xsl:call-template>
				<ponctuation>
				<xsl:text>...</xsl:text>
				</ponctuation>
			</xsl:when>
			<xsl:when test="contains($l_ponct,substring(., string-length(.), string-length(.)))">
				<xsl:call-template name="remplaceEspace">
					<xsl:with-param name="chaine">
						<xsl:value-of select="substring(.,1,string-length(.)-1)" />
					</xsl:with-param>
				</xsl:call-template>
				<ponctuation>
				<xsl:value-of select="substring(., string-length(.), string-length(.))" />
				</ponctuation>
			</xsl:when>
			<xsl:otherwise>
				<mot>
					<!-- <xsl:if test="not(string($attrname)='')">
						<xsl:attribute name="{string($attrname)}">
							<xsl:value-of select="string($attrvalue)"/>
						</xsl:attribute>
					</xsl:if> -->
					<xsl:value-of select="."/>
				</mot>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:for-each>
</xsl:template>



<!-- template qui renvoie vrai s'il faut mettre un espace après le noeud passé en paramètre.
utile pour les mev à l'intérieur de mots, les bugs style <span>mick</span><span>aelig;</span><span>l</span> -->
<xsl:template name="doSpace" as="xs:boolean">
	<!-- node() match les noeuds text() et element() donc c'est lui qu'il faut utiliser ci-dessous -->
	<!-- voir http://www.w3.org/TR/xpath-functions/#flags pour le flag 'm' de fn:matches -->
	<xsl:value-of select="fn:matches(.,'\s$') or fn:matches(string(following::text()[1][not(../@class=$stylesG0)]),'^\s')" />
	<!--<xsl:message select="concat('***',string(.),'***',replace(following::text()[1],' ','X'),'***',replace(string(following::node()[1]),' ','X'),'***',fn:matches(.,'\s$'),fn:matches(string(following::node()[1]),'^\s'))"/>-->
</xsl:template>





</xsl:stylesheet>