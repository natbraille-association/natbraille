<?xml version='1.0' encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
xmlns:xsl='http://www.w3.org/1999/XSL/Transform' 
xmlns:saxon='http://icl.com/saxon'
xmlns:m='http://www.w3.org/1998/Math/MathML'
xmlns:lit='espacelit'
xmlns:score-partwise='http://www.musicxml.org/'
xmlns:doc='espaceDoc'>
<!--
   n'est plus utilisé car on prend directement les altérations graphiques de 
   musicxml
  -->

<!--
   nat:pitch_is_in
   - prend un <pitch> et un set de <pitch>
   - renvoie vrai si l'<alter> du <pitch> est la même que celle
   de l'<alter> du <pitch> correspondant dans le set de <pitch>s
  -->

<xsl:function name="nat:pitch_is_in" as="xs:boolean" >
  
  <xsl:param name="pitch" as="node()"/>
  <xsl:param name="gamme" as="node()"/>
  
  <xsl:variable name="c_step" select="$pitch/step" />
  <xsl:variable name="c_alter" select="if ($pitch/alter) then ($pitch/alter) else (0)"/>   
  
  <xsl:variable name="alter_of_it">
    <xsl:for-each select="$gamme/pitch" >
      <xsl:if test="contains(./step,$c_step)" >
	<xsl:value-of select="./alter" />
      </xsl:if>
    </xsl:for-each>
  </xsl:variable>
  
  <xsl:value-of select="if ($alter_of_it = $c_alter) then (fn:true()) else (fn:false())" />

</xsl:function>


<!--
   nat:pitch_set_add
   - prend set de <pitch> (gamme) et un <pitch>
   - renvoie un nouveau set de <pitch> id. à celui d'entrée
   avec l'alter du pitch passé en paramètre
  -->

<xsl:function name="nat:pitchset_set_pitch" as="node()*" >
  
  <xsl:param name="gamme" as="node()"/>
  <xsl:param name="pitch" as="node()"/>
  
  <xsl:variable name="c_step" select="$pitch/step" />
  <xsl:variable name="c_alter" select="if ($pitch/alter) then ($pitch/alter) else (0)"/>   
  
  <xsl:for-each select="$gamme/pitch" >
    <xsl:element name="pitch">
      <xsl:element name="step">
	<xsl:value-of select="./step" />
      </xsl:element>
      <xsl:element name="alter">
	<xsl:choose>
	  <xsl:when test="./step = $c_step" >
	    <xsl:value-of select="$c_alter" />
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:value-of select="./alter" />
	  </xsl:otherwise>
	</xsl:choose>
      </xsl:element>
    </xsl:element>
  </xsl:for-each>
</xsl:function>



<!-- is the (potential) alteration accidental ? -->
<!--
   <xsl:variable name="alter_in_acci" 
		 select="if ($c_is_note)
			 then (nat:pitch_is_in(*[$in_num_child]/pitch,$in_accidentals_set))
			 else (fn:true())" />
   -->


<!--	
   on prend maintenant l'altération noire sans calcul
   <xsl:with-param name="in_accidentals_set"  
		   select="if ($c_is_measure_bar or $c_is_attribute_key) 
			   then ($c_armure) 
			   else ($out_accidentals_set)" />
   -->
</xsl:stylesheet>