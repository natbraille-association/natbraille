#!/usr/bin/perl
use strict;
use warnings;

my @all = 
(
 'pt',
 'pt1',
 'pt12',
 'pt123',
 'pt1234',
 'pt12345',
 'pt123456',
 'pt12346',
 'pt1235',
 'pt12356',
 'pt1236',
 'pt124',
 'pt1245',
 'pt12456',
 'pt1246',
 'pt125',
 'pt1256',
 'pt126',
 'pt13',
 'pt134',
 'pt1345',
 'pt13456',
 'pt1346',
 'pt135',
 'pt1356',
 'pt136',
 'pt14',
 'pt145',
 'pt1456',
 'pt146',
 'pt15',
 'pt156',
 'pt16',
 'pt2',
 'pt23',
 'pt234',
 'pt2345',
 'pt23456',
 'pt2346',
 'pt235',
 'pt2356',
 'pt236',
 'pt24',
 'pt245',
 'pt2456',
 'pt246',
 'pt25',
 'pt256',
 'pt26',
 'pt3',
 'pt34',
 'pt345',
 'pt3456',
 'pt346',
 'pt35',
 'pt356',
 'pt36',
 'pt4',
 'pt45',
 'pt456',
 'pt46',
 'pt5',
 'pt56',
 'pt6',
);

my %r;
my @filenames = `find . -name '*ent'`;
foreach my $filename (@filenames){
    chomp $filename;
    open A, $filename;
    foreach (<A>){
	if ($_ =~ /.*ENTITY.*(pt\d*)\s+"(.*)"/){
	    $r{$1}->{$filename} = $2;
	}

    }
    close A;
}

my @lines;
my %ucuse;
foreach my $pt ('header',@all){
    my @line;
    if ($pt eq 'header'){
	push @line, "filename";
    } else {
	push @line, $pt;
    }
    foreach my $filename (@filenames){
	if ($pt eq 'header'){
	    push @line, $filename;
	} else {
	    my $uc = $r{$pt}->{$filename};
	    push @line, $uc;
	    $ucuse{$uc}++;
	}
    }
    push @lines, \@line;
}

foreach (@lines){
    print join("\t",@$_)."\n";
}

# foreach (sort keys %ucuse){
#     print $_." ".$ucuse{$_}."\n";
# }

# &amp; 3   U+0026 (38)
# &apos; 7 U+0027 (39)
# &gt; 6 U+003E (62)
# &lt; 5 U+003C (60)
# &quot; 7 U+0022 (34)

