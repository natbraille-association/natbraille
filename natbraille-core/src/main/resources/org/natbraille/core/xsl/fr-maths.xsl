﻿<?xml version='1.1' encoding="UTF-8" ?>
<!--
* NAT - An universal Translator
* Copyright (C) 2005 Bruno Mascret
* Contact: bmascret@free.fr

* former authors: Frédéric Schwebel, Bruno Mascret, Ouarda Yassa
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
-->

<!-- version 2.4 -->

<!DOCTYPE xsl:stylesheet SYSTEM "nat://system/xsl/mmlents/windob.dtd">

<xsl:stylesheet version="3.0"
xmlns:xsl='http://www.w3.org/1999/XSL/Transform' 
xmlns:saxon='http://icl.com/saxon'
xmlns:m='http://www.w3.org/1998/Math/MathML'
xmlns:xs='http://www.w3.org/2001/XMLSchema'
xmlns:fn='http://www.w3.org/2005/xpath-functions'
xmlns:functx='http://www.functx.com'
xmlns:lit='espacelit'
xmlns:doc='espaceDoc'
xmlns:nat='http://natbraille.free.fr/xsl'>

	<xsl:import href="nat://system/xsl/fr-maths-pass1.xsl" />  <!-- pour le preprocessing concernant les sinus etc -->
	
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<xsl:variable name="dbk">&pt56;</xsl:variable>
	<xsl:variable name="fbk">&pt23;</xsl:variable>
	
	<xsl:variable name="abregez">1</xsl:variable> <!-- non utlisé sf pour tests -->
	
	<xsl:variable name="charspt6" as="xs:string">
		<!-- caracteres auxquels il faut rajouter un pt6 selon qu'on est en abrégé ou pas -->
		<xsl:choose>
			<xsl:when test="$abrege"><xsl:text>abcdefghijklmnopqrstuvwxyz1234567890,.'&rarr;&larr;&uarr;&darr;</xsl:text></xsl:when>
			<xsl:otherwise><xsl:text>1234567890,.'&rarr;&larr;&uarr;&darr;</xsl:text></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

<!-- ******************** PROGRAMME ****************** -->

<!-- *******************************************
On met les chars de debut et fin de document
******************************************* -->
<!--
<xsl:template match="/">
	<xsl:value-of select="$debut" />
	<xsl:apply-templates select="@*|*|text()|processing-instruction()"/>
	<xsl:value-of select="$fin" />
</xsl:template>-->

<!-- *******************************************
Tout ce qui n'est pas traité est ressorti tel quel (attention à  l'encodage)
******************************************* -
<xsl:template match="@*|*|text()|processing-instruction()" priority="-1">
	<xsl:copy>
		<xsl:apply-templates select="@*|*|text()|processing-instruction()"/>
	</xsl:copy>
</xsl:template>
-->
<!-- on vire les commentaires et les annotations mathtype -->
<xsl:template match="comment()|m:annotation">
</xsl:template>

<!-- en mode 'appel' sur les mrow, on apply la suite en mode appel aussi -->
<xsl:template match="m:mrow" mode="appel">
	<xsl:apply-templates mode="appel" />
</xsl:template>

<xsl:template match="m:math[not(@chemistry='true')][*[1][not(self::m:semantics)]]|m:math[not(@chemistry='true')]/m:semantics">
	<xsl:message>
		Allé les mathent
	</xsl:message>
	<xsl:element name="math">
		<xsl:element name="braille">
		<!-- ********************** phase de preprocessing *********************** -->
		<xsl:variable name="math-preprocessed">
			<xsl:element name="mrow" namespace="http://www.w3.org/1998/Math/MathML">
				<xsl:apply-templates mode="phase1" />
			</xsl:element>
		</xsl:variable>
		<!--<xsl:if test="$dbg"><xsl:message select="'original',count(.//*[@mathvariant='normal']),'apres ',count($math-preprocessed//*[@mathvariant='normal']),
				'proutos',count($math-preprocessed//*[@proutos])"/></xsl:if>-->
		<xsl:variable name="enfants" as="xs:integer">
			<xsl:variable name="newm" as="element()" select="$math-preprocessed/m:mrow" />
			<xsl:choose>
				<xsl:when test="(count($newm/child::*)=1) and (local-name($newm/child::*[1])='mrow')">
					<xsl:value-of select="count($newm/m:mrow/descendant::*)" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="count($newm/descendant::*)" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		
		<xsl:variable name="equation">
			<xsl:apply-templates select="$math-preprocessed" />
			<!-- <xsl:apply-templates /> -->
			<!-- <xsl:copy-of select="$math-preprocessed" /> -->
			<!-- GA<xsl:value-of select="string(fn:local-name($math-preprocessed[2]))"  />GA -->
		</xsl:variable>
		
		<!-- on enleve les espaces, les line feed et tabs de l'equation -->
		<!-- on remplace les saut à générer par de vrai sauts -->
		<xsl:variable name="eqn2" as="xs:string">
			<xsl:value-of select="translate(string($equation),'&#13;&#10;	 ','')" />
		</xsl:variable>
		<!--
		<xsl:variable name="longeqn"> <!- longueur effective de l'equation ->
			<xsl:value-of select="string-length(translate($eqn2,'&coupe;',''))" />
		</xsl:variable>-->
		
		<!-- ****** Affichage final de l'expression ****** -->
		<xsl:variable name="tabSeul" as="xs:boolean" select="count(*[not(self::m:annotation)])=1 and
				*[1][self::m:mtable or (self::m:mrow and count(*)=1 and m:mtable)]"/>
		<xsl:value-of select="if (not($tabSeul)) then $debMath else ''"/>

		<!-- regle de prefixage : rien, pt6 ou pt6pt3 ? -->
		<xsl:choose>
		
			<!-- on est dans un tableau, on préfixe pas -->
			<xsl:when test="count(ancestor::tableau) &gt; 0">
			</xsl:when>
			
			<!-- si l'option préfixe est activée, on met direct le préfixe -->
			<xsl:when test="$forceMathPrefix">
				<xsl:text>&pt6;&pt3;</xsl:text>
			</xsl:when>
			
			
			<!-- cas d'une lettre, symbole ou chiffre isolé -->
			<xsl:when test="$enfants=1">
				<xsl:if test="contains($charspt6,substring(child::*[1],1,1)) or fn:matches(string($math-preprocessed),'.*([A-Z]{2})+.*')">
					<xsl:text>&pt6;</xsl:text> <!-- pt6 si abrege et si la 1ere lettre de l'unite est minuscule ou chiffre seul-->
				</xsl:if>
			</xsl:when>
			
			<!-- si il n'y a que des maths dans la phrase, on ne préfixe pas = si le nombre d'enfants non vides de phrase dont le nom est math est egal au nombre d'enfants non vides -->
			<xsl:when test="count(ancestor::phrase/child::*[string(.)]) = count(ancestor::phrase/child::*[(string(.)) and (local-name(.)='math')])">
			</xsl:when>
			
			<!-- quand on a que des lettres, on met pas de pt6 sauf si ça commence par une minuscule et qu'on est en abrégé, ou qu'il y a une suite de majuscules 
			OU qu'il y a autre chose que des mi, mo, mtext, mrow par exemple a indice p-->
			<xsl:when test="(string-length(translate(string(.),concat($l_alphabet,$l_grec_min,$l_grec_maj,'°'),''))=0) and not(contains($eqn2,$dbk))">
				<xsl:if test="contains($charspt6,substring(string(.),1,1)) or fn:matches(string($math-preprocessed),'.*([A-Z]{2})+.*')
					or (descendant::*[not(self::m:mi or self::m:mo or self::m:mrow or self::m:mtext)] and not(string($math-preprocessed)='A°'))">
					<xsl:text>&pt6;</xsl:text>
				</xsl:if>
			</xsl:when>
			
			<!-- TODO : cas particulier chiffre + unite -->
			
			<!-- cas général : on met un point 6 et s'il y a des blocs dans l'expression on rajoute un point 3 -->
			<xsl:otherwise>
				<!-- il y a des trucs avant sur la même ligne qui ne sont pas vides (le premier élément non vide de la phrase est ce noeud math ) TODO: tester aussi la suite de la phrase-->
				<xsl:text>&pt6;</xsl:text><!-- TODO test si unité, etc... -->
				<xsl:if test="contains($eqn2,$dbk)" >
					<!-- c'est des maths avec blocs -->
					<xsl:text>&pt3;</xsl:text>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>
		
		<!-- bra <xsl:value-of disable-output-escaping="yes" select="$avant"/> -->
		<!-- je vire le traitement de la coupure qui se fera dans base.xsl -->
		<xsl:value-of select="translate($eqn2,$espaceSecable,'&pt;')" />
		<!--
		<xsl:choose>
			<xsl:when test="$longeqn>$longueur and $coupons='1'">
				<xsl:variable name="eqn3">
					<xsl:call-template name="coupure">
						<xsl:with-param name="equation" select="$eqn2" />
					</xsl:call-template>
				</xsl:variable>
				<xsl:value-of select="translate($eqn3,'&espace;&coupe;&#13;&#10; ',' &#10;')" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="translate($eqn2,'&espace;&coupe;',' ')" />
			</xsl:otherwise>
		</xsl:choose>
		-->
		<!-- bra <xsl:value-of select="$apres"/> -->
		<xsl:value-of select="if (not($tabSeul)) then $finMath else ''"/>
		<xsl:if test="following-sibling::*[1] or (self::m:semantics and parent::*[1]/following-sibling::*[1])">
			<!-- il y a des trucs après sur la même ligne -->
			<xsl:call-template name="espace"/>
		</xsl:if>
		<!-- ****** Fin Affichage final ****** -->
		</xsl:element>
	</xsl:element>
</xsl:template>

<!-- la suite était commentée (espaces) -->
<xsl:template match="m:ms">
	<xsl:choose>
		<xsl:when test=".='&thinsp;' or .='&ThickSpace;'"/>
		<xsl:otherwise>
			<xsl:apply-templates />
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="none|m:mspace">
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="m:mrow|m:mtr|m:mtd">
	<xsl:apply-templates />
</xsl:template>

<!-- espaces transformés en pt3 si entre deux chiffres et laissés sinon-->
<xsl:template match="m:mtext">
	<!-- Bruno: TODO vérifier ce template, il y a des cas non prévus (texte direct dans le mtext, espaces avec ponctuations en utilisant la feuille g1 ou g2, etc...-->
	<xsl:choose>
		<xsl:when test="@bijection='true'">
			<xsl:call-template name="bijectionMath">
				<xsl:with-param name="mot" select="."/>
			</xsl:call-template>
		</xsl:when>
		<xsl:when test="not(count(child::*)=0)">
			<!-- le noeud texte est pas vide alors on applique les templates filles et alea jacta est -->
			<!--<xsl:value-of select="text()" />-->
			<xsl:call-template name="communMoMi">
				<xsl:with-param name="chaine" select="."/>
			</xsl:call-template>
			<xsl:apply-templates />
		</xsl:when>
		<xsl:otherwise><!-- Djidjo à Bruno : TODO vérifier que la première condition est pas complètement foireuse-->
			<xsl:choose>
				<!-- Bruno: si c'est une suite de nombre, on les sépare par des pt3 et pas des espaces-->
				<xsl:when test="(local-name(preceding::*[1])='mn') and (local-name(following::*[1])='mn')">
					<!-- <xsl:value-of select="translate(string(.),'&ThickSpace;&quad; &thinsp;','&pt3;&pt3;&pt3;&pt3;')" /> -->
					<xsl:value-of select="translate(string(.),'&ThickSpace;&quad; &thinsp;','&pt3;&pt3;&pt3;&pt3;')" />
				</xsl:when>
				<!-- a priori plus besoin de ça: ponct en lit SAUF si dbt -->
				<xsl:when test="string-length(translate(string(following::*[1]),'?!;:.%',''))=0">
					<!-- on a un espace de mise en page avant une ponctuation finale qui disparait en braille -->
					<xsl:call-template name="communMoMi">
						<xsl:with-param name="chaine" select="translate(string(.),'&ThickSpace;&quad; &thinsp;','')" />
					</xsl:call-template>
				</xsl:when>
				<xsl:when test="string-length(translate(string(preceding::*[1]),'?!;:.+-&plus;&minus;&ndash;',''))=0">
					<!-- on a un espace de mise en page après un signe, ça disparait mais on les garde pour =, etc. -->
					<xsl:call-template name="communMoMi">
						<xsl:with-param name="chaine" select="translate(string(.),'&#xE897;&ThickSpace;&quad; &thinsp;','')" />
					</xsl:call-template>
				</xsl:when>
				<xsl:when test="..[self::m:msub or self::m:msup] and translate(string(.),'&ThickSpace;&quad; &thinsp;','')=''">
					<!-- c'est un espace généré pour la mise en page d'indice ou d'exposant donc on l'ignore -->
				</xsl:when>
				<xsl:otherwise>
					<xsl:variable name="m" select="translate(normalize-space(.),'&ThickSpace;&quad; &thinsp;',concat($espace,$espace,$espace))" /><!-- TODO SERVEUR c'est ici les pb espaces-->
					<xsl:call-template name="communMoMi">
						<xsl:with-param name="chaine" 
						select="functx:replace-multi($m,('([A-Z])','&sup2;','&sup3;'),('&pt46;$1','&pt4;&pt126;','&pt4;&pt146;'))" />
					</xsl:call-template>
					<!-- Fred : cas des unités :  garder espace et rajouter un pt6 devant si abrégé et si le suivant est une lettre minuscule -->
					<xsl:choose>
						<xsl:when test="$forceMathPrefix and matches(.,'[&ThickSpace;&quad; &thinsp;]+')">
							<xsl:text>&pt6;&pt3;</xsl:text>
						</xsl:when>
						<xsl:when test="following::text()[1][..[self::m:mn and not (@fonc or @membreFonc)]] and not(.=('(',')','{','}','[',']'))">
							<!-- si on a une suite de différents nbres+unités -->
							<xsl:text>&pt6;</xsl:text>
						</xsl:when>
						<xsl:when test="$abrege and following::*[1][not(@fonc or @membreFonc)
							and (self::m:mi or self::m:mo or self::m:msup) and (translate(substring(text(),1,1),$charspt6,'')='')]">
							<xsl:text>&pt6;</xsl:text>
						</xsl:when>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="m:mfenced">
	<!-- parenthèses et autres gros trucs d'encadrements -->
	<!-- TODO encadrements plusieurs parenthèses et crochets avec pt5-->
	<!-- 5 ans après (2008-2013) : le TODO ci-dessus n'est plus à faire ! -->
	
	<xsl:variable name="clef" as="xs:string">
		<xsl:variable name="open" select="@open" />
		<xsl:variable name="close" select="@close" />
		<xsl:choose>
			<xsl:when test="*[1][self::m:mtable] or *[1][self::m:mrow and m:mtable]"><!-- matrice ou systeme -->
				<xsl:text>&pt45;</xsl:text>
			</xsl:when>
			<xsl:when test="$open=('(','[') and descendant::*[(string(.)=$open) or (@open=$open and count(descendant::m:mtd)=(0,1))]">
				<!-- imbrication de parenthèses et/ou crochets -->
				<xsl:text>&pt5;</xsl:text>
			</xsl:when>
			<xsl:otherwise><xsl:text/></xsl:otherwise><!-- pour pas que la séquence soit vide -->
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="prefixe" as="xs:string*">
		<xsl:choose>
			<xsl:when test="@open = '('">
				<xsl:sequence select="(concat($clef,'&pt236;'),'&pt236;')"/>
			</xsl:when>
			<xsl:when test="@open = '['">
				<xsl:sequence select="(concat($clef,'&pt12356;'),'&pt12356;')"/>
			</xsl:when>
			<xsl:when test="@open = '{'">
				<xsl:choose>
					<xsl:when test="$clef='&pt45;'"> <!-- systeme equations -->
						<xsl:sequence select="('&pt456;&pt236;','&pt46;&pt236;')"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:sequence select="('&pt46;&pt236;','&pt46;&pt236;')"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<!-- problème: les attributrs ne sont pas interprétés et arrivent sous la forme &amp;NOM;-->
			<xsl:when test="@open = ('&amp;lobrk;','&lobrk;','&#x27E6;')"><!--grand crochet ouvrant avec barre (crochet double)-->
				<xsl:sequence select="('&pt46;&pt12356;','&pt46;&pt12356;')" />
			</xsl:when>
			<xsl:when test="@open = ')'">
				<xsl:sequence select="(concat($clef,'&pt356;'),'&pt356;')"/>
			</xsl:when>
			<xsl:when test="@open = ']'">
				<xsl:sequence select="(concat($clef,'&pt23456;'),'&pt23456;')"/>
			</xsl:when>
			<xsl:when test="@open = '}'">
				<xsl:choose>
					<xsl:when test="$clef='&pt45;'"> <!-- systeme equations -->
						<xsl:sequence select="('&pt456;&pt356;','&pt46;&pt356;')"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:sequence select="('&pt46;&pt356;','&pt46;&pt356;')"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<!-- problème: les attributrs ne sont pas interprétés et arrivent sous la forme &amp;NOM;-->
			<xsl:when test="@open = ('&amp;robrk;','&robrk;','&#x27E7;')"><!--grand crochet fermant avec barre( crochet double)-->
				<xsl:sequence select="('&pt46;&pt23456;','&pt46;&pt23456;')"/>
			</xsl:when>
			<xsl:when test="@open = ('&amp;mid;','&mid;','|')"><!-- barre vert. -->
				<xsl:sequence select="('&pt456;&pt123456;','&pt123456;')" />
			</xsl:when>
			<xsl:when test="@open = ('&par;','&Verbar;')"><!-- double barre vert. -->
				<xsl:sequence select="('&pt46;&pt123456;','&pt45;&pt123456;')"/>
			</xsl:when>
			<xsl:otherwise><xsl:text/></xsl:otherwise><!-- pour que la variable ne soit pas vide -->
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="suffixe" as="xs:string*">
		<xsl:choose>
			<xsl:when test="@close = ')'">
				<xsl:sequence select="(concat($clef,'&pt356;'),'&pt356;')"/>
			</xsl:when>
			<xsl:when test="@close = ']'">
				<xsl:sequence select="(concat($clef,'&pt23456;'),'&pt23456;')"/>
			</xsl:when>
			<xsl:when test="@close = '}'">
				<xsl:choose>
					<xsl:when test="$clef='&pt45;'"> <!-- systeme equations -->
						<xsl:sequence select="('&pt456;&pt356;','&pt46;&pt356;')"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:sequence select="('&pt46;&pt356;','&pt46;&pt356;')"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<!-- problème: les attributrs ne sont pas interprétés et arrivent sous la forme &amp;NOM;-->
			<xsl:when test="@close = ('&amp;robrk;','&robrk;','&#x27E7;')"><!--grand crochet fermant avec barre( crochet double)-->
				<xsl:sequence select="('&pt46;&pt23456;','&pt46;&pt23456;')"/>
			</xsl:when>
			<xsl:when test="@close = '('">
				<xsl:sequence select="(concat($clef,'&pt236;'),'&pt236;')"/>
			</xsl:when>
			<xsl:when test="@close = '['">
				<xsl:sequence select="(concat($clef,'&pt12356;'),'&pt12356;')"/>
			</xsl:when>
			<xsl:when test="@close = '{'">
				<xsl:choose>
					<xsl:when test="$clef='&pt45;'"> <!-- systeme equations -->
						<xsl:sequence select="('&pt456;&pt236;','&pt46;&pt236;')"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:sequence select="('&pt46;&pt236;','&pt46;&pt236;')"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<!-- problème: les attributrs ne sont pas interprétés et arrivent sous la forme &amp;NOM;-->
			<xsl:when test="@close = ('&amp;lobrk;','&lobrk;','&#x27E6;')"><!--grand crochet ouvrant avec barre (crochet double)-->
				<xsl:sequence select="('&pt46;&pt12356;','&pt46;&pt12356;')" />
			</xsl:when>
			<xsl:when test="@close = ('&amp;mid;','&mid;','|')"><!-- barre vert. -->
				<xsl:sequence select="('&pt456;&pt123456;','&pt123456;')" />
			</xsl:when>
			<xsl:when test="@close = ('&par;','&Verbar;')"><!-- double barre vert. -->
				<xsl:sequence select="('&pt46;&pt123456;','&pt45;&pt123456;')"/>
			</xsl:when>
			<xsl:otherwise><xsl:text/></xsl:otherwise><!-- pour que la variable ne soit pas vide -->
		</xsl:choose>
	</xsl:variable>
	
	<xsl:choose>
		<xsl:when test="*[1][self::m:mtable] or *[1][self::m:mrow and m:mtable]"><!-- c'est une structure 2D -->
			<xsl:apply-templates mode="appel">
				<xsl:with-param name="prefixe" select="$prefixe" as="xs:string*" tunnel="yes"/>
				<xsl:with-param name="suffixe" select="$suffixe" as="xs:string*" tunnel="yes"/>
			</xsl:apply-templates>
		</xsl:when>
		<xsl:when test="$clef='&pt5;'">
			<!-- imbrication -->
			<xsl:value-of select="$prefixe[1]"/>
			<xsl:apply-templates/>
			<xsl:value-of select="$suffixe[1]"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$prefixe[2]"/>
			<xsl:apply-templates/>
			<xsl:value-of select="$suffixe[2]"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>
<!-- *******************************************
Template entités communes mi et mo
****************************************** -->
<!-- Majuscules, Lettres  ques, ponctuation, espaces, ensembles, autres types de lettres -->
<xsl:template name="communMoMi">
	<xsl:param name="chaine"/>
	<xsl:param name="type" /><!-- mo ou mi -->
	
	<!-- forcissation de cle majuscule Bruno: ancien algo pas bon (pour le cas de xX par exemple-->
	<!-- Bruno: pour avoir le double préfixe (on s'en fout en math, faudra virer ça et le faire marcher)-->
	<!-- Bruno: réglé -->
	<!-- TODO majuscule dans une expresison -->
	<xsl:choose>
		<xsl:when test="string-length($chaine)=0"/>
		<xsl:when test="contains('ABCDEFGHIJKLMNOPQRSTUVWXYZ',substring(string($chaine),1,1))">
			<xsl:text>&pt46;</xsl:text>
			<!-- Bruno: appel à un seul template de transcription
					<xsl:value-of select="translate($chaine,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','&pt1;&pt12;&pt14;&pt145;&pt15;&pt124;&pt1245;&pt125;&pt24;&pt245;&pt13;&pt123;&pt134;&pt1345;&pt135;&pt1234;&pt12345;&pt1235;&pt234;&pt2345;&pt136;&pt1236;&pt2456;&pt1346;&pt13456;&pt1356;')" />-->
			<xsl:call-template name="bijectionMath">
				<xsl:with-param name="mot" select="$chaine"/>
			</xsl:call-template>
		</xsl:when>
		<!-- flèches openoffice -->
		<xsl:when test=".='↘'"> <!-- flèche décroissante -->
			<xsl:text>&pt46;&pt356;</xsl:text>
		</xsl:when>
		<xsl:when test=".='↗'"> <!-- flèche croissante -->
			<xsl:text>&pt45;&pt356;</xsl:text>
		</xsl:when>
		<xsl:when test=".='&map;'"> <!-- Fleche associe -->
			<xsl:text>&pt5;&pt156;</xsl:text>
		</xsl:when>
		<!--Bruno: ajout ² et cube -->
		<xsl:when test="$chaine='&sup2;'">
			<xsl:text>&pt4;&pt126;</xsl:text>
		</xsl:when>
		<xsl:when test="$chaine='&sup3;'">
			<xsl:text>&pt4;&pt146;</xsl:text>
		</xsl:when>
		
		<xsl:when test="$chaine=('&part;','&npart;')"> <!-- derivee partielle -->
			<xsl:text>&pt5;&pt145;</xsl:text>
		</xsl:when>
		
		<xsl:when test="$chaine='&compfn;'"> <!-- f rond g -->
			<xsl:text>&pt456;&pt3456;</xsl:text>
		</xsl:when>
		
		<xsl:when test="$chaine=('°','&deg;')">
			<xsl:choose>
				<xsl:when test="$type='mi'"> <!-- dans un mi c'est un degré -->
					<xsl:text>&pt5;&pt135;</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$coupe"/> <!-- dans un mo c'est rond -->
					<xsl:text>&pt456;&pt3456;</xsl:text> <!-- f rond g -->
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		
		<!-- l'apostrophe est obligée d'être ici car elle ne peut être transcrite correctement dans le translate de bijectionMath car ça ferme la chaîne -->
		<xsl:when test='$chaine="&apos;"'>
			<xsl:text>&pt3;</xsl:text>
		</xsl:when>
		<!-- pts de suspension -->
		<xsl:when test="$chaine=('&mldr;','&vellip;','&hellip;','&dtdot;','&ldots;','&ctdot;','&utdot;')"> <!-- Bruno NAT: ajout de certaines entités ici -->
			<xsl:text>&pt256;&pt256;&pt256;</xsl:text>
		</xsl:when>
		
		<!-- mars-avril 2013 : fred ; ensemble d'entités chelou de la norme -->
		<xsl:when test=".='&nequiv;'"><!-- non congru à ≢ &#x2262;-->
			<xsl:text>&pt46;&pt2356;&pt2356;</xsl:text>
		</xsl:when>
		<xsl:when test=".='&ape;'"><!-- double équivalence souligné ≊ &#x224A;-->
			<xsl:text>&pt456;&pt5;&pt2356;</xsl:text>
		</xsl:when>
		<xsl:when test=".='&wedgeq;'"> <!-- signe correspond à (égal avec chapeau) &#x2259;-->
			<xsl:text>&pt25;&pt2356;</xsl:text>
		</xsl:when>
		<xsl:when test=".=('&lobrk;','&#x27E6;')"> <!-- crochet double ouvrant ⟦ ; étrange qqfois nom non reconnu -->
		<!-- cf http://infohost.nmt.edu/tcc/help/pubs/docbook43/iso9573/web/by-full-name-index.html : num différent de isotech.ent -->
			<xsl:text>&pt46;&pt12356;</xsl:text>
		</xsl:when>
		<xsl:when test=".=('&robrk;','&#x27E7;')"> <!-- crochet double fermant ⟧ ; étrange qqfois nom non reconnu-->
		<!-- cf http://infohost.nmt.edu/tcc/help/pubs/docbook43/iso9573/web/by-full-name-index.html : num différent de isotech.ent -->
			<xsl:text>&pt46;&pt23456;</xsl:text>
		</xsl:when>
		<xsl:when test=".=('&#x227B;','&sc;')"> <!-- succède à -->
			<xsl:text>&pt46;&pt46;&pt345;</xsl:text>
		</xsl:when>
		<xsl:when test=".=('&#x227A;','&pr;')"> <!-- précède à -->
			<xsl:text>&pt46;&pt46;&pt126;</xsl:text>
		</xsl:when>
		<xsl:when test=".=('&#x227D;','&sccue;')"> <!-- suit au sens large -->
			<xsl:text>&pt45;&pt45;&pt345;</xsl:text>
		</xsl:when>
		<xsl:when test=".=('&#x227C;','&prcue;')"> <!-- précède au sens large -->
			<xsl:text>&pt45;&pt45;&pt126;</xsl:text>
		</xsl:when>
		
		<xsl:when test=".=('%','&percent;')">
			<xsl:text>&pt5;&pt346;</xsl:text>
			<!-- pt6 au debut mais ptete qu'il en faut pas -->
		</xsl:when>
		
		<!-- on vire les espaces -->
		<xsl:when test="$chaine='&ZeroWidthSpace;'">
			<xsl:text></xsl:text>
		</xsl:when>
		<xsl:when test="$chaine='&ThickSpace;'">
			<xsl:text></xsl:text>
		</xsl:when>
		<!-- Ensembles RZCQND infini vide -->
		<xsl:when test="contains('&Copf;&Ropf;&Nopf;&Qopf;&Zopf;&Dopf;',$chaine)"> <!-- complexes -->
			<xsl:text>&pt46;&pt46;</xsl:text> <!--<xsl:text>&pt46;&pt46;c</xsl:text>-->
			<xsl:value-of select="translate($chaine,'&Copf;&Ropf;&Nopf;&Qopf;&Zopf;&Dopf;','&pt14;&pt1235;&pt1345;&pt12345;&pt1356;&pt145;')"/>
		</xsl:when>
		
		<!-- clef &pt45; : lettres grecques minuscules + qq signes -->
		<xsl:when test="contains(concat('&infin;&emptyv;',$l_grec_min),$chaine)">
			<xsl:text>&pt45;</xsl:text>
			<xsl:value-of select="translate($chaine,concat('&infin;&emptyv;',$l_grec_min),'&pt14;&pt3456;&pt1;&pt12;&pt1245;&pt145;&pt15;&pt15;&pt1356;&pt125;&pt245;&pt245;&pt24;&pt13;&pt123;&pt134;&pt1345;&pt1346;&pt135;&pt1234;&pt12456;&pt1235;&pt1235;&pt234;&pt234;&pt2345;&pt136;&pt124;&pt124;&pt12345;&pt13456;&pt2456;&pt15;')"/>
		</xsl:when>
		<!-- clef &pt46;&pt45; : lettres grecques majuscules-->
		<xsl:when test="contains($l_grec_maj,$chaine)">
			<xsl:text>&pt46;&pt45;</xsl:text>
			<xsl:value-of select="translate($chaine, $l_grec_maj,'&pt1;&pt12;&pt1245;&pt145;&pt15;&pt1356;&pt125;&pt245;&pt24;&pt13;&pt123;&pt134;&pt1345;&pt1346;&pt135;&pt1234;&pt1235;&pt234;&pt2345;&pt136;&pt124;&pt12345;&pt12345;&pt13456;&pt2456;')"/>
		</xsl:when>

		<!-- lettres rondes utilisées essentiellement en géométrie (police Euclid math one et NOUVEAU : Math12); Bruno NAT: Je rentre toutes les lettres -->
		<!-- ajout d'autres caractères unicodes de la palge &#x1D49N; attention, il n'y a pas tout l'alphabet!)-->
		<!--clef &pt46; &pt5; -->
		<xsl:when test="contains('𝒜&Ascr;&Bscr;𝒞&Cscr;𝒟&Dscr;&Escr;&Fscr;𝒢&Gscr;&Hscr;&Iscr;𝒥&Jscr;𝒦&Kscr;&Lscr;&Mscr;𝒩&Nscr;𝒪&Oscr;𝒫&Pscr;&wp;&weierp;𝒬&Qscr;ℛ&Rscr;𝒮&Sscr;𝒯&Tscr;𝒰&Uscr;𝒱&Vscr;𝒲&Wscr;𝒳&Xscr;𝒴&Yscr;𝒵&Zscr;',$chaine)">
			<xsl:text>&pt46;&pt5;</xsl:text>
			<xsl:value-of select="translate($chaine, '𝒜&Ascr;&Bscr;𝒞&Cscr;𝒟&Dscr;&Escr;&Fscr;𝒢&Gscr;&Hscr;&Iscr;𝒥&Jscr;𝒦&Kscr;&Lscr;&Mscr;𝒩&Nscr;𝒪&Oscr;𝒫&Pscr;&wp;&weierp;𝒬&Qscr;ℛ&Rscr;𝒮&Sscr;𝒯&Tscr;𝒰&Uscr;𝒱&Vscr;𝒲&Wscr;𝒳&Xscr;𝒴&Yscr;𝒵&Zscr;','&pt1;&pt1;&pt12;&pt14;&pt14;&pt145;&pt145;&pt15;&pt124;&pt1245;&pt1245;&pt125;&pt24;&pt245;&pt245;&pt13;&pt13;&pt123;&pt134;&pt1345;&pt1345;&pt135;&pt135;&pt1234;&pt1234;&pt1234;&pt1234;&pt12345;&pt12345;&pt1235;&pt1235;&pt234;&pt234;&pt2345;&pt2345;&pt136;&pt136;&pt1236;&pt1236;&pt2456;&pt2456;&pt1346;&pt1346;&pt13456;&pt13456;&pt1356;&pt1356;')"/>
		</xsl:when>
		<!-- clef &pt5; lettres rondes minuscules -->
		<xsl:when test="contains('𝒶&ascr;𝒷&bscr;𝒸&cscr;𝒹&dscr;&escr;𝒻&fscr;&gscr;𝒽&hscr;𝒾&iscr;𝒿&jscr;𝓀&kscr;&ell;𝓁&lscr;𝓂&mscr;𝓃&nscr;&oscr;𝓅&pscr;𝓆&qscr;𝓇&rscr;𝓈&sscr;𝓉&tscr;𝓊&uscr;𝓋&vscr;𝓌&wscr;𝓍&xscr;𝓎&yscr;𝓏&zscr;',$chaine)">
			<xsl:text>&pt5;</xsl:text>
			<xsl:value-of select="translate($chaine, '𝒶&ascr;𝒷&bscr;𝒸&cscr;𝒹&dscr;&escr;𝒻&fscr;&gscr;𝒽&hscr;𝒾&iscr;𝒿&jscr;𝓀&kscr;&ell;𝓁&lscr;𝓂&mscr;𝓃&nscr;&oscr;𝓅&pscr;𝓆&qscr;𝓇&rscr;𝓈&sscr;𝓉&tscr;𝓊&uscr;𝓋&vscr;𝓌&wscr;𝓍&xscr;𝓎&yscr;𝓏&zscr;','&pt1;&pt1;&pt12;&pt12;&pt14;&pt14;&pt145;&pt145;&pt15;&pt124;&pt124;&pt1245;&pt125;&pt125;&pt24;&pt24;&pt245;&pt245;&pt13;&pt13;&pt123;&pt123;&pt123;&pt134;&pt134;&pt1345;&pt1345;&pt135;&pt1234;&pt1234;&pt12345;&pt12345;&pt1235;&pt1235;&pt234;&pt234;&pt2345;&pt2345;&pt136;&pt136;&pt1236;&pt1236;&pt2456;&pt2456;&pt1346;&pt1346;&pt13456;&pt13456;&pt1356;&pt1356;')"/>
		</xsl:when>
		<!-- clef &pt45; &pt45; lettres hébraïques TODO vérifier ces lettres bizarres (diff. avec norme)-->
		<!-- entités n'existants pas avec leur code associé: &he;&#x05D4;&vav;&#x05D5;&zayin;&#x05D6;&het;&#x05D7;&tet;&#x05D8;&yod;&#x05D9;&khaf;&#x05DA;&kaf;&#x05DB;&lamed;&#x05DC;&mem;&#x05DD;&#x05DE;&nun;&#x05DF;&#x05E0;&samekh;&#x05E1;&ayin;&#x05E2;&pe;&#x05E4;&fe;&#x05E3;&tsadi;&#x05E5;&#x05E6;&qof;&#x05E7;&resh;&#x05E8;&shin;&#x05E9;&sin;&tav;&#x05EA;&vet;-->
		<xsl:when test="contains('&aleph;&beth;&#x05D1;&gimel;&#x05D2;&daleth;&#x05D3;&#x05D4;&#x05D5;&#x05D6;&#x05D7;&#x05D8;&#x05D9;&#x05DA;&#x05DB;&#x05DC;&#x05DD;&#x05DE;&#x05DF;&#x05E0;&#x05E1;&#x05E2;&#x05E4;&#x05E3;&#x05E5;&#x05E6;&#x05E7;&#x05E8;&#x05E9;&#x05EA;&sin;&vet;',$chaine)">
			<xsl:text>&pt45;&pt45;</xsl:text>
			<xsl:value-of select="translate($chaine, '&aleph;&beth;&#x05D1;&gimel;&#x05D2;&daleth;&#x05D3;&#x05D4;&#x05D5;&#x05D6;&#x05D7;&#x05D8;&#x05D9;&#x05DA;&#x05DB;&#x05DC;&#x05DD;&#x05DE;&#x05DF;&#x05E0;&#x05E1;&#x05E2;&#x05E4;&#x05E3;&#x05E5;&#x05E6;&#x05E7;&#x05E8;&#x05E9;&#x05EA;&sin;&vet;','&pt1;&pt12;&pt12;&pt1245;&pt1245;&pt145;&pt145;&pt125;&pt2456;&pt1356;&pt1346;&pt2345;&pt245;&pt16;&pt13;&pt123;&pt134;&pt134;&pt1345;&pt1345;&pt234;&pt1246;&pt1234;&pt124;&pt2346;&pt2346;&pt12345;&pt1235;&pt146;&pt1256;&pt156;&pt1236;')"/>
		</xsl:when>
		<!-- ********************************* Entités considérées comme mi par oo ********* -->
		<xsl:when test="$chaine='&gl;' or $chaine= '&gtrless;' or $chaine='&GreaterLess;'">
			<xsl:value-of select="$coupeEsth"/>
			<xsl:text>&pt5;&pt345;&pt5;&pt126;</xsl:text>
		</xsl:when>
		<xsl:when test="$chaine=('&minus;','-','&ndash;')">
			<xsl:text>&pt36;</xsl:text>
		</xsl:when>
		<xsl:otherwise><!-- bruno, 23/11/08: je rajoute un translate pour coder les alphanumériques avec la table braille -->
			<!-- utilisation d'une variable (pb des '")-->
			<!-- factorisation des translate de bijection 05/07/08-->
			<xsl:call-template name="bijectionMath">
				<xsl:with-param name="mot" select="$chaine"/>
			</xsl:call-template>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>
<!-- *******************************************
Template match mi
******************************************* -->
<xsl:template match="m:mi">
	<!-- Bruno: les mi se retrouvent toutes dans les mo (=ce mathml oo et mathtype) et sont traitées dans le template commun mo/mi; ce template pourrait être optimisé -->
	<!-- Problème avec l'ancienne solution: il y a des fois des expressions contenant un seul tag mi contenant plusieurs entités (suivant la manière d'utiliser mathml); du coup on découpe le . en sous-chaine de longueur 1 sauf si il s'agit de fonctions à écriture spéciale (sin, cos, etc.)-->

	<xsl:choose>
		<!-- Euro fait à  l'arrache car l'entité &euro; n'est pas dans la DTD mathml -->
		<xsl:when test=".='euro'">
			<xsl:text>&pt45;&pt15;</xsl:text>
		</xsl:when>
		<xsl:when test=".='&euro;'">
			<xsl:text>&pt45;&pt15;</xsl:text>
		</xsl:when>
		<!-- signe de degré qui veut dire degré dans un mi et rond (f rond g) dans un mo-->
		<xsl:when test=".=('&deg;','°')">
			<xsl:text>&pt5;&pt135;</xsl:text>
		</xsl:when>
		<!-- <xsl:when test='.="&apos;" or .="&prime;"'>
			<xsl:text>Z</xsl:text>
		</xsl:when> -->
		<xsl:otherwise>
			<xsl:call-template name="decoupeChaine">
				<xsl:with-param name="type" select="mi" />
				<xsl:with-param name="chaine" select="." />
			</xsl:call-template>
		</xsl:otherwise>
	</xsl:choose>
	
</xsl:template>

<xsl:template name="decoupeChaine">
	<xsl:param name="type"/>
	<xsl:param name="chaine"/>
	<xsl:if test="string-length($chaine) &gt; 0">
		<xsl:call-template name="communMoMi">
			<xsl:with-param name="type" select="$type" />
			<xsl:with-param name="chaine" select="substring($chaine, 1,1)"/>
		</xsl:call-template>
		<xsl:call-template name="decoupeChaine">
			<xsl:with-param name="type" select="$type" />
			<xsl:with-param name="chaine" select="substring($chaine, 2)" />
		</xsl:call-template>
	</xsl:if>
</xsl:template>

<!-- *******************************************
	  Template match mn (chiffres ou expression du type 2x)
	  ******************************************* -->

<xsl:template match="m:mn"><!-- TODO -->
	
	<xsl:variable name="dernier" select="substring(.,string-length(.))" />
	<!-- probleme avec codeUS o pt3=' ça fait planter le translate donc
		  solution provisoire qui le restera certainement longtemps : utiliser une variable -->	
	<xsl:variable name="pt3">&pt256;</xsl:variable><!-- point => 256 en 2007-->
	<!--<xsl:variable name="v2">
		<xsl:text>&pt1;&pt12;&pt14;&pt145;&pt15;&pt124;&pt1245;&pt125;&pt24;&pt245;&pt13;&pt123;&pt134;&pt1345;&pt135;&pt1234;&pt12345;&pt1235;&pt234;&pt2345;&pt136;&pt1236;&pt2456;&pt1346;&pt13456;&pt1356;&pt12356;&pt16;&pt123456;&pt2346;&pt126;&pt1246;&pt146;&pt12456;&pt1456;&pt23456;&pt156;&pt1256;&pt12346;&pt345;&pt246;&pt1;&pt12;&pt14;&pt145;&pt15;&pt124;&pt1245;&pt125;&pt24;&pt245;&pt13;&pt123;&pt134;&pt1345;&pt135;&pt1234;&pt12345;&pt1235;&pt234;&pt2345;&pt136;&pt1236;&pt2456;&pt1346;&pt13456;&pt1356;&pt3456;&pt16;&pt126;&pt146;&pt1456;&pt156;&pt1246;&pt12456;&pt1256;&pt246;&pt12356;&pt16;&pt123456;&pt2346;&pt126;&pt1246;&pt146;&pt12456;&pt1456;&pt23456;&pt156;&pt1256;&pt12346;&pt345;&pt246;&pt345;&pt235;&pt36;&pt35;&pt25;&pt2356;&pt;&pt12356;&pt23456;&pt12346;&pt123456;&pt123456;&pt2;&pt23;&pt25;&pt256;&pt26;&pt235;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt3;&pt3;&pt3;&pt3;&pt236;&pt356;</xsl:text>
	</xsl:variable>-->

	<xsl:choose>
		<xsl:when test="string-length(translate($dernier,'.:;?!',''))= 0">
			<xsl:call-template name="decoupeChaine">
				<xsl:with-param name="chaine" select="substring(.,1,string-length(.)-1)"/>
			</xsl:call-template>
			<xsl:if test="$dernier=';'"><xsl:value-of select="$coupeEsth"/></xsl:if>
			<xsl:value-of select="translate($dernier,'.:;?!','&pt256;&pt25;&pt23;&pt26;&pt235;')" />
		</xsl:when>
		<xsl:otherwise>
			<xsl:call-template name="decoupeChaine">
				<xsl:with-param name="chaine" select="."/>
			</xsl:call-template>
		</xsl:otherwise>
	</xsl:choose>

</xsl:template>

<!-- *******************************************
Template match mo
******************************************* -->

<xsl:template match="m:mo">

	<xsl:choose>
		<!-- Bruno: nat 02-03-2006 :j'ajoute en tant que mo la plupart des entités mi (pb avec le mathml openoffice)-->
		<!-- Bruno: nat 16-11-2006 : je regroupe dans le template communMoMi les entités communes -->
		<!-- ajout 10 mars 2004 -->
		<!-- ce tilde peut être aussi "sinusoà¯de" pt4pt2356 mais c'est rare alors zut -->
		<xsl:when test=".='&tilde;'"> <!-- signe de fct polynomiale -->
			<xsl:text>&pt5;&pt456;&pt25;</xsl:text>
		</xsl:when>
		
		<!--Ajout 17 fevrier 2004 -->
		<xsl:when test=".='&nabla;'"> <!-- nabla = delta à  l'envers -->
			<xsl:value-of select="$coupe"/>
			<xsl:text>&pt45;&pt4;&pt145;</xsl:text><!--<xsl:text>&pt45;&pt4;d</xsl:text>-->
		</xsl:when>
		
		<!-- Ajout de qq oublis 25 juin 2002 -->
		<xsl:when test=".='&triangleright;'">
			<xsl:text>tridroite</xsl:text>
		</xsl:when>
		
		<xsl:when test=".='&triangleleft;'">
			<xsl:text>trigauche</xsl:text>
		</xsl:when>
		
		<!--mis à  jours par Ouarda -->	
		<xsl:when test=".='&permil;'">
			<xsl:text>&pt5;&pt346;&pt346;</xsl:text>
			<!-- pt6 au debut mais ptete qu'il en faut pas -->
		</xsl:when>
		
		<!-- ** NN les signes de ponctuation sont à  faire précéder par un pt6-->
		<!-- ponctuation traitée: ; -->
		<xsl:when test=".=';'">
			<xsl:text>&pt23;</xsl:text>
			<xsl:value-of select="$coupeEsth" />
		</xsl:when>
		
		<!-- composition de fonctions MathType utilise la bonne entité mais OpenOffice utilise ° dans mo -->
		<xsl:when test=".=('&compfn;','°')">
			<xsl:value-of select="$coupe"/>
			<xsl:text>&pt456;&pt3456;</xsl:text> <!-- f rond g -->
		</xsl:when>
		
		<!-- ** géométrie *************************************** -->
		<!--mis à  jour par ouarda-->
		<xsl:when test=".='&par;'"> <!-- parallele a ou double barre exclusion (erreur de mathtype) Ux2225 formula-->
			<xsl:choose>
				<xsl:when test="nat:is-variation-table(ancestor::m:mtable)">
					<xsl:text>&pt123456;</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>&pt456;&pt1256;</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		
		<xsl:when test=".='&perp;'"> <!-- perpendiculaire a -->
			<xsl:text>&pt45;&pt1256;</xsl:text>
		</xsl:when>
		
		<xsl:when test=".='&frown;'"> <!-- signe d'arc -->
			<xsl:text>&pt4;&pt25;</xsl:text>
		</xsl:when>
		
		<xsl:when test=".=('^','&ang;','&hat;','&Hat;','&angsph;','&#710;','&circ;')"> <!-- signes d'angles Bruno NAT angsph-->
			<xsl:text>&pt45;&pt25;</xsl:text>
		</xsl:when>
		
		
		<xsl:when test=".='&comp;'">
			<xsl:text>&pt46;&pt14;</xsl:text>
		</xsl:when>
		
		<!-- une truc qui ressemble à  un petit delta majuscule: je le traduis pareil que delta-->
		<xsl:when test=".='&bigtriangleup;'">
			<xsl:text>&pt4;&pt145;</xsl:text>
		</xsl:when>
		
		<!-- </A INCLURE FAUTE MAJUSCULE> -->
	
	<!-- ** Flèches *************************************** -->
	
	<!-- pour les fleches vers la droite, symbole different selon si c'est
		  une fleche "normale" ou un vecteur -->
	
		<xsl:when test=".='&rarr;' or .='&xrarr;' or .='&#x20D7;'"> <!-- Fleche horiz. droite rarr=openoffice xrarr=mathtype-->
			<xsl:choose>
				<xsl:when test="local-name(parent::*)='mover'">
					<!-- est-ce une fleche de vecteur ? -->
					<xsl:text>&pt46;&pt25;</xsl:text>
					<!-- <xsl:value-of select="'WW&xrarr;XX&rarr;'" /> -->
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$coupeEsth"/>
					<xsl:text>&pt456;&pt156;</xsl:text> <!-- fleche normale -->
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		
		<xsl:when test=".='&larr;' or .='&xlarr;'"> <!-- Fleche horiz. gauche -->
			<!-- rien... -->
			<xsl:text>&pt456;&pt246;</xsl:text><!-- Bruno NAT -->
		</xsl:when>
		<!-- mis à  jour par ouarda-->
		<xsl:when test=".='&uarr;'"> <!-- Fleche verticale haut-->
			<xsl:text>&pt45;&pt12456;</xsl:text><!-- Bruno NAT -->
		</xsl:when>
		<!-- mis à  jour par ouarda-->	
		<xsl:when test=".='&darr;'"> <!-- Fleche verticale bas-->
			<xsl:text>&pt46;&pt12456;</xsl:text><!-- Bruno NAT -->
		</xsl:when>

		
		<xsl:when test=".='&nearr;'"> <!-- Fleche croissant Ux2197 formula -->
			<xsl:text>&pt45;&pt156;</xsl:text>
		</xsl:when>
		
		<xsl:when test=".='&searr;'"> <!-- Fleche decroissant Ux2198 formula -->
			<xsl:text>&pt46;&pt156;</xsl:text>
		</xsl:when>
		
		<xsl:when test=".='&nwarr;'"> <!-- flèche croissante à gauche -->
			<xsl:text>&pt45;&pt246;</xsl:text>
		</xsl:when>
		
		<xsl:when test=".='&swarr;'"> <!-- flèche décroissante à gauche -->
			<xsl:text>&pt46;&pt246;</xsl:text>
		</xsl:when>
		
		<xsl:when test=".='&rlarr;' or .='&rlhar;'"> <!-- double flèche gauche droite -->
			<xsl:value-of select="$coupeEsth"/>
			<xsl:text>&pt456;&pt12456;</xsl:text>
		</xsl:when>
		
		<xsl:when test=".='&rArr;'"> <!-- Fleche implique -->
			<xsl:text>&pt25;&pt2;</xsl:text>
		</xsl:when>
		
		<xsl:when test=".='&lArr;'"> <!-- Fleche implique gauche-->
			<xsl:text>&pt5;&pt25;</xsl:text>
		</xsl:when>
		
		<xsl:when test=".='&harr;'"> <!-- flèche gd simple, Bruno-->
			<xsl:text>&pt5;&pt12456;</xsl:text>
		</xsl:when>
		
		<xsl:when test=".=('&cdot;','&sdot;','&bullet;')"> <!-- produit scalaire -->
			<xsl:text>&pt35;&pt35;</xsl:text>
		</xsl:when>
		
		<xsl:when test=".='.'"> <!-- multiplication ou unite -->
			<!-- si dernière position, point de ponctuation pt6pt3 -->
			<xsl:choose>
				<xsl:when test="count(following-sibling::*)=0">
					<xsl:text>&pt256;</xsl:text>
				</xsl:when>
				<xsl:when test="matches(preceding-sibling::*[1],'[A-z]') and matches(following-sibling::*[1],'[A-z]')">
					<!-- c'est un point d'unité comme par exemple m.s² -->
					<xsl:text>&pt35;</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$coupe"/>
					<xsl:text>&pt35;&pt35;</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		
		<xsl:when test=".='?'">
			<!-- à  priori il s'agit uniquement d'une ponctuation de fin donc point6 -->
			<xsl:text>&pt6;&pt26;</xsl:text>
		</xsl:when>
		
		
		<xsl:when test=".='!'"><!-- a vérif -->
			<!-- si dernière position avec espace avant, point d'exclamation -->
			<!-- factorielle ou "unique" si "il existe" avant-->
			<xsl:choose>
				<xsl:when test="contains ('&ThickSpace;&quad; &thinsp;',preceding::*[1]) and count(following-sibling::*)=0">
					<xsl:text>&pt56;&pt235;</xsl:text>
				</xsl:when>
				<xsl:when test="preceding-sibling::*[1]='&exist;'">
					<xsl:text>&pt235;</xsl:text>
				</xsl:when>
				<xsl:otherwise> <!-- factorielle -->
					<xsl:text>&pt456;&pt35;</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		
		<xsl:when test=".='&wedge;'"> <!-- produit vectoriel, prod. extérieur, pgcd, conjonction, wedge -->
			<xsl:value-of select="$coupe"/>
			<xsl:text>&pt45;&pt35;</xsl:text>
		</xsl:when>
		<xsl:when test=".='&Wedge;'"> <!-- grand produit vectoriel -->
			<xsl:value-of select="$coupe"/>
			<xsl:text>&pt45;&pt45;&pt35;</xsl:text>
		</xsl:when>
		
		<!-- ya un truc que je capte pas ici: Msg erreur:name expected (found "<") -->
			<!-- j'ai enfin capté certaine entity ne peuvent être mise sous la forme "&#nombre;" ex &amp -->
		
		<xsl:when test=".='&Verbar;'"> <!-- double barre vert. -->
			<!--<xsl:message><xsl:copy-of select=".." /></xsl:message>-->
			<xsl:choose>
				<!-- dans un tableau de variation -->
				<xsl:when test="nat:is-variation-table(ancestor::m:mtable)">
					<xsl:text>&pt123456;</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:if test="parent::*[self::m:mrow and count(*[string(.)])=(2,3)]"> <!-- la double barre encadre un truc alors préfixe 45 -->
						<xsl:text>&pt45;</xsl:text>
						</xsl:if>
						<xsl:text>&pt123456;</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		
		<xsl:when test=".='&forall;'"> <!-- quelque soit -->
			<xsl:text>&pt456;&pt34;</xsl:text>
		</xsl:when>
		<xsl:when test=".='&exist;'"> <!-- il existe -->
			<xsl:text>&pt456;&pt16;</xsl:text>
		</xsl:when>
		<xsl:when test=".='&hArr;'"> <!-- equivalent a TODO verif-->
			<xsl:value-of select="$coupeEsth"/>
			<xsl:text>&pt5;&pt25;&pt2;</xsl:text>
		</xsl:when>
		
		<xsl:when test=".='&sum;'"> <!-- somme algébrique-->
			<xsl:text>&pt46;&pt45;&pt234;</xsl:text><!-- ancienne norme <xsl:text>&pt4;s</xsl:text>-->
		</xsl:when>
		<xsl:when test=".='&prod;'"> <!-- produit -->
			<xsl:text>&pt46;&pt45;&pt1234;</xsl:text><!-- anicenne norme <xsl:text>&pt4;p</xsl:text>-->
		</xsl:when>
		
		<!--    <xsl:when test=".='&PI;'">
			<xsl:text>&pt4;p</xsl:text>
		</xsl:when>
		Un jour cette entite fut generee par mathtype. Paix a son ame.
		-->
		<xsl:when test=".='('"> <!-- parenthèse gauche , matrice -->
			<xsl:variable name="suffixe" as="xs:string*">
				<xsl:call-template name="choose_suffixe">
					<xsl:with-param name="position" select="2" />
				</xsl:call-template>
			</xsl:variable>
			<xsl:variable name="lasuite" as="xs:string" select="string-join(following::text(),'')"/>
			<xsl:choose>
				<xsl:when test="local-name(following-sibling::*[1])='mtable' or local-name(following-sibling::*[1]/child::*[1])='mtable'"> <!-- matrice-->
					<xsl:apply-templates select="following-sibling::*[1]" mode="appel">
						<xsl:with-param name="prefixe" select="('&pt45;&pt236;','&pt236;')" tunnel="yes"/>
						<xsl:with-param name="suffixe" select="$suffixe" tunnel="yes"/>
					</xsl:apply-templates>
				</xsl:when>
				<xsl:when test="matches($lasuite,'^[^\)]*\(.*\)')">
					<!-- imbrication -->
					<xsl:text>&pt5;&pt236;</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>&pt236;</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		
		<xsl:when test=".=')'"> <!-- parenthèse droite , matrice -->
			<xsl:variable name="lavant" as="xs:string" select="string-join(preceding::text(),'')"/>
			<xsl:choose>
				<xsl:when test="local-name(preceding-sibling::*[1])='mtable' or local-name(preceding-sibling::*[1]/*[1])='mtable'"/> <!-- matrice, déjà traité -->
				<xsl:when test="matches(functx:reverse-string($lavant),'^[^\(]*\).*\(')">
					<!-- imbrication -->
					<xsl:text>&pt5;&pt356;</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>&pt356;</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		
		<xsl:when test=".='['"> <!-- crochet gauche , matrice -->
			<xsl:variable name="suffixe" as="xs:string*">
				<xsl:call-template name="choose_suffixe">
					<xsl:with-param name="position" select="2" />
				</xsl:call-template>
			</xsl:variable>
			<xsl:variable name="lasuite" as="xs:string" select="string-join(following::text(),'')"/>
			<!--<xsl:message select="'**',translate($lasuite,'&#59543;',''),string(matches($lasuite,'^[^\]]*\[')),'**'" />-->
			<xsl:choose>
				<xsl:when test="local-name(following-sibling::*[1])='mtable' or local-name(following-sibling::*[1]/*[1])='mtable'"> <!-- matrice-->
					<xsl:apply-templates select="following-sibling::*[1]" mode="appel">
						<xsl:with-param name="prefixe" select="('&pt45;&pt12356;','&pt12356;')" tunnel="yes"/>
						<xsl:with-param name="suffixe" select="$suffixe" tunnel="yes"/>
					</xsl:apply-templates>
				</xsl:when>
				<xsl:when test="matches($lasuite,'^[^\]]*\[.*\]')">
					<!-- imbrication -->
					<xsl:text>&pt5;&pt12356;</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>&pt12356;</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		
		<xsl:when test=".=']'"> <!-- crochet droit , matrice -->
			<xsl:variable name="lavant" as="xs:string" select="string-join(preceding::text(),'')"/>
			<xsl:choose>
				<xsl:when test="local-name(preceding-sibling::*[1])='mtable' or local-name(preceding-sibling::*[1]/*[1])='mtable'"/> <!-- matrice-->
				<xsl:when test="matches(functx:reverse-string($lavant),'^[^\[]*\].*\[')">
					<!-- imbrication -->
					<xsl:text>&pt5;&pt23456;</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>&pt23456;</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		
		<xsl:when test=".='{'"> <!-- accolade gauche , système d'équation-->
			<xsl:variable name="suffixe" as="xs:string*">
				<xsl:call-template name="choose_suffixe">
					<xsl:with-param name="position" select="2" />
				</xsl:call-template>
			</xsl:variable>
			<xsl:choose>
				<xsl:when test="($suffixe=('','')) and
					(local-name(following-sibling::*[1])='mtable' or local-name(following-sibling::*[1]/*[1])='mtable')">
					<!-- c'est l'accolade d'un système d'équations car pas de fermant -->
					<xsl:text>&pt456;&pt236;</xsl:text><!-- le système est la seule exception où on a l'accolade multilignes même en non linéarisation -->
					<xsl:apply-templates select="following-sibling::*[1]" mode="appel">
						<xsl:with-param name="prefixe" select="('','')" tunnel="yes"/>
						<xsl:with-param name="suffixe" select="$suffixe" tunnel="yes"/>
					</xsl:apply-templates>
				</xsl:when>
				<xsl:when test="local-name(following-sibling::*[1])='mtable' or local-name(following-sibling::*[1]/*[1])='mtable'"> <!-- matrice grande accolade ou systeme -->
					<xsl:apply-templates select="following-sibling::*[1]" mode="appel">
						<xsl:with-param name="prefixe" select="('&pt456;&pt236;','&pt46;&pt236;')" tunnel="yes"/>
						<xsl:with-param name="suffixe" select="$suffixe" tunnel="yes"/>
					</xsl:apply-templates>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>&pt46;&pt236;</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		
		<xsl:when test=".='}'"> <!-- accolade droite -->
			<xsl:choose>
				<xsl:when test="local-name(preceding-sibling::*[1])='mtable' or local-name(preceding-sibling::*[1]/*[1])='mtable'"/> <!-- matrice, déjà traité -->
				<xsl:otherwise>
					<xsl:text>&pt46;&pt356;</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		
		<xsl:when test=".=('&mid;','|')"> <!-- barre verticale -->
			<!--<xsl:message select="(.,';',./following-sibling::*[1],';',nat:is-variation-table(.))"/>-->
			<xsl:variable name="suffixe" as="xs:string*">
				<xsl:call-template name="choose_suffixe">
					<xsl:with-param name="position" select="2" />
				</xsl:call-template>
			</xsl:variable>
			<xsl:choose>
				<xsl:when test="local-name(following-sibling::*[1])='mtable' or local-name(following-sibling::*[1]/*[1])='mtable'"> <!-- matrice-->
					<xsl:apply-templates select="following-sibling::*[1]" mode="appel">
						<xsl:with-param name="prefixe" select="('&pt456;&pt123456;','&pt123456;')" tunnel="yes"/>
						<xsl:with-param name="suffixe" select="$suffixe" tunnel="yes"/>
					</xsl:apply-templates>
				</xsl:when>
				<xsl:when test="local-name(preceding-sibling::*[1])='mtable' or local-name(preceding-sibling::*[1]/*[1])='mtable'"/> <!-- matrice, déjà traité-->
				<xsl:otherwise>
					<xsl:text>&pt123456;</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		
		<!-- **** Ensembles ************************************ -->
		<xsl:when test=".='&cup;'"> <!-- union -->
			<xsl:value-of select="$coupe"/>
			<xsl:text>&pt456;&pt235;</xsl:text>
		</xsl:when>
		
		<!-- grande union -->
		<!-- xplus? -->
		<!-- grande intersection -->
		
		<xsl:when test=".='&cap;'"> <!-- intersection -->
			<xsl:value-of select="$coupe"/>
			<xsl:text>&pt45;&pt235;</xsl:text>
		</xsl:when>
		
		<xsl:when test=".=('&isin;','&isinv;')"> <!-- appartient a -->
			<xsl:text>&pt45;&pt16;</xsl:text>
		</xsl:when>
		
		<xsl:when test=".='&notin;'"> <!-- n appartient pas -->
			<xsl:text>&pt45;&pt34;</xsl:text>
		</xsl:when>
		
		<xsl:when test=".='&ni;'"> <!-- symétrique de appartient a &owns;?-->
			<xsl:text>&pt46;&pt45;&pt16;</xsl:text>
		</xsl:when>
		
		<xsl:when test=".='&sub;'"> <!-- inclus dans (sens strict)-->
			<xsl:text>&pt46;&pt16;</xsl:text>
		</xsl:when>
		
		<xsl:when test=".='&vnsub;' or .='&nsub;'"> <!-- bruno nat: +nsub :n'est pas inclus dans (sens strict)-->
			<xsl:text>&pt46;&pt34;</xsl:text>
		</xsl:when>
		
		<!--modifié par ouarda -->
		<xsl:when test=".='&sube;'"> <!-- inclus dans (sens large)-->
			<xsl:text>&pt456;&pt46;&pt16;</xsl:text>
		</xsl:when>
		
		<!-- mis à  jour par ouarda-->
		<xsl:when test=".='&nsube;'"> <!--bruno nat: ni un sous-ensemble ni égal à -->
			<xsl:text>&pt456;&pt46;&pt34;</xsl:text>
		</xsl:when>
		
		<xsl:when test=".='&nsupe;'"> <!--bruno nat?: ni un sur-ensemble ni égal à -->
			<xsl:text>(ni_un_sur-ensemble_ni_égal_à (ndnn))</xsl:text>
		</xsl:when>
		<!-- mis à  jour par ouarda -->
		<xsl:when test=".='&nsup;'"> <!-- bruno nat:inclus dans (sens strict)-->
			<xsl:text>&pt5;&pt34;</xsl:text>
		</xsl:when>
		
		<xsl:when test=".='&setminus;'"> <!-- différence d'ensemble--><!--bruno nat? -->
			<xsl:text>différence</xsl:text>
		</xsl:when>
		
		<!--<xsl:when test=".='&vnsube;'">  !!!n'est pas inclus dans (sens large); cette entité n'est pas dans mathtype bruno nat: mais bien dans oo (nsube)
			<xsl:text>&pt4;&pt46;&pt34;</xsl:text>
		</xsl:when>
		-->
		
		<!-- **** PB PB PB les 2 entités suivantes-->
		
		<!-- modifié¡°ar ouarda -->
		
		<xsl:when test=".='&sup;'"> <!-- inclus dans (sens strict)-->
			<xsl:text>&pt5;&pt16;</xsl:text>
		</xsl:when>
		
		<xsl:when test=".='&supe;'"> <!-- contenant dans (sens large)-->
			<xsl:text>(contenant_au_sens_large(ndnn))</xsl:text>
		</xsl:when>
			
			<!-- la norme ne donne pas la traduction pour supe et sup (a inclus (large) b; a inclus b (strict) -->
			
			<!-- remember the vase of quotient d'ensembles p.21 &pt4;&pt256;-->
			
			<!-- </A INCLURE> -->
		
		<!-- **** équations-inéquations-systèmes ************************ -->
		
		<xsl:when test=".='&ne;' or .='&neq;'"> <!-- non egal -->
			<xsl:value-of select="$coupeEsth"/>
			<xsl:text>&pt46;&pt2356;</xsl:text>
		</xsl:when>
		
		<xsl:when test=".=('&sim;','~')"> <!-- equivalent à -->
			<xsl:text>&pt45;&pt2356;</xsl:text>
		</xsl:when>
		
		<xsl:when test=".='&cong;'"> <!-- equivalence soulignée deux fois -->
			<xsl:text>&pt456;&pt2356;</xsl:text>
		</xsl:when>
		
		<!-- bruno NAT: je rajoute pas mal d'entités ici -->
		<xsl:when test=".='&approx;' or .='&simeq;'"> <!-- environ egal -->
			<xsl:value-of select="$coupeEsth"/>
			<xsl:text>&pt5;&pt2356;</xsl:text>
		</xsl:when>
		<xsl:when test=".='&ape;' or .='&approxeq;'"> <!-- presque égal ou egalà  -->
			<xsl:value-of select="$coupeEsth"/>
			<xsl:text>&pt456;&pt5;&pt2356;</xsl:text>
		</xsl:when>
		
		<xsl:when test=".=('&le;','&leq;','&les;','&#xE2FA;','&#x2A7D;','&#x2264;') "> <!-- inferieur ou egal , &lesqlant;?-->
			<xsl:value-of select="$coupeEsth"/>
			<xsl:text>&pt45;&pt126;</xsl:text>
		</xsl:when>
		
		<xsl:when test=".=('&ge;','&geq;','&ges;','&#x2265;','&#x2A7E;','&#xE2F6;')"> <!-- superieur ou egal -->
			<xsl:value-of select="$coupeEsth"/>
			<xsl:text>&pt45;&pt345;</xsl:text>
		</xsl:when>
		<!-- Bruno: c'est bon les &amp;lt; maintenant -->
		<xsl:when test=".='&lt;' or .='&amp;lt;'"> <!-- strict. inferieur -->
			<xsl:value-of select="$coupeEsth"/>
			<xsl:text>&pt5;&pt126;</xsl:text>
		</xsl:when>
		<!-- mis à  jour par ouarda-->	
		<xsl:when test=".='&gt;'"> <!-- strict. superieur -->
			<xsl:value-of select="$coupeEsth"/>
			<xsl:text>&pt5;&pt345;</xsl:text>
		</xsl:when>
		
		<xsl:when test=".='&equiv;'"> <!-- = a trois barres, relation de congruence-->
			<xsl:value-of select="$coupeEsth"/>
			<xsl:text>&pt2356;&pt2356;</xsl:text> <!--CHIMIE pt123456-->
		</xsl:when>
		
		<xsl:when test=".=('&Gt;','&#x226B;')"> <!-- Bruno Nat: très supérieur à -->
			<xsl:value-of select="$coupeEsth"/>
			<xsl:text>&pt5;&pt5;&pt345;</xsl:text>
		</xsl:when>
		
		<!-- modifié par ouarda-->
		<xsl:when test=".=('&Lt;','&#x226A;')"> <!-- Bruno Nat: très inférieur à -->
			<xsl:value-of select="$coupeEsth"/>
			<xsl:text>&pt5;&pt5;&pt126;</xsl:text>
		</xsl:when>
		
		<xsl:when test=".=('&gl;','&gtrless;','&GreaterLess;','&#x2277;','&lg;','&#x2276;')">
			<xsl:value-of select="$coupeEsth"/>
			<xsl:text>&pt5;&pt345;&pt5;&pt126;</xsl:text>
		</xsl:when>
		<!-- ***** barre horizontale, derivees ****** -->
		
		<xsl:when test=".=('&horbar;','&macr;','&amp;macr;','ˉ')">
			<xsl:text>&pt456;&pt25;</xsl:text>
		</xsl:when>
		
		<!-- l'apostrophe (caractere 39) est change en .
				pour les derivees (point 3) 
				pris en charge normalement par communmomi
		<xsl:when test='.="&apos;" or .="&prime;"'>
			<xsl:text>&pt3;</xsl:text>
		</xsl:when>
		<xsl:when test='.="&apos;&apos;" or .="&prime;&prime;"'>
			<xsl:text>&pt3;&pt3;</xsl:text>
		</xsl:when>
		<xsl:when test='.="&apos;&apos;&apos;" or .="&prime;&prime;&prime;"'>
			<xsl:text>&pt3;&pt3;&pt3;</xsl:text>
		</xsl:when> -->

		<!-- **** Opérateurs NN****************************************** -->
		
		<!--pour les symboles divise et multiplie, on regarde si l'operateur
					precedent est une fraction auquel cas il faut doubler
					le signe dans l'ancienne norme -->
		
		<xsl:when test=".='&times;'">
			<xsl:value-of select="$coupe"/>
			<xsl:text>&pt35;</xsl:text>
		</xsl:when>
		
		<xsl:when test=".='&divide;'">
			<xsl:value-of select="$coupe"/>
			<xsl:text>&pt25;</xsl:text>
		</xsl:when>
		
		<xsl:when test=".='/'">
			<xsl:value-of select="$coupe"/>
			<xsl:text>&pt34;</xsl:text>
		</xsl:when>
			
	<!-- avant on avait ça mais pour la table WebCBFr1252 o on a plusieurs caractères pour = ça chie, d'où la séparation    
		<xsl:when test=".='=' or .='+' or .='-'">
			<xsl:text>&coupe;</xsl:text><xsl:value-of select="translate(string(.),'=+-','&pt2356;&pt235;&pt36;')" />
		</xsl:when> -->
		
		<xsl:when test=".='=' or .='&equals;'">
			<xsl:value-of select="$coupeEsth"/>
			<xsl:text>&pt2356;</xsl:text>
		</xsl:when>
		
		<xsl:when test=".='+' or .='&plus;'">
			<xsl:value-of select="$coupe"/>
			<xsl:text>&pt235;</xsl:text>
		</xsl:when>
		<!--<xsl:when test=".='&Plus;'">existe pas?
			<xsl:text>&coupe;&pt5;&pt235;</xsl:text>
		</xsl:when>-->
		<xsl:when test=".='&Circleplus;' or .='&oplus;' or .='&xoplus;'">
			<xsl:value-of select="$coupe"/>
			<xsl:text>&pt46;&pt235;</xsl:text>
		</xsl:when>
		
		<xsl:when test=".='-' or .='&minus;'">
			<xsl:value-of select="$coupe"/>
			<xsl:text>&pt36;</xsl:text>
		</xsl:when>
		
		<xsl:when test=".='&plusmn;' or .='&PlusMinus;'">
			<xsl:value-of select="$coupe"/>
			<xsl:text>&pt235;&pt36;</xsl:text>
		</xsl:when>
		<!-- bruno -->
		<xsl:when test=".='&mnplus;' or .='&MinusPlus;'">
			<xsl:value-of select="$coupe"/>
			<xsl:text>&pt36;&pt235;</xsl:text>
		</xsl:when>
		
		<xsl:when test=".='*' or .='&lowast;'">
			<xsl:value-of select="$coupe"/>
			<xsl:text>&pt5;&pt35;</xsl:text>
			<!-- L'ETOILE N'EST PAS UNE MULTIPLICATION, MAIS UN SYMBOLE D'ENSEMBLE ou un produit de convolution ex. R* -->
		</xsl:when>
		
		<xsl:when test=".='&otimes;'">
			<xsl:value-of select="$coupe" />
			<xsl:text>&pt46;&pt35;</xsl:text>
		</xsl:when>
		
		<!-- ********************************************************************************* 
				**************************************************************************************
				*					A Verififer 
				*
				*******************************************************************************************-->

		<xsl:when test=".='&or;'"> <!-- opérateur ou logique-->
			<xsl:value-of select="$coupe"/>
			<xsl:text>&pt45;&pt26;</xsl:text><!-- page 6 old pareil que ppcm-->
		</xsl:when>
		<xsl:when test=".='&nmid;'"> <!-- barre verticale barrée: f|barre x x ne divise pas f ndnn-->
			<xsl:text>&pt1345;&pt135;&pt1345;&pt123456;</xsl:text>
		</xsl:when>
		<xsl:when test=".='&not;'"> <!-- signe not (trait horizontal se terminant en petit angle droit-->
			<xsl:text>&pt5;&pt36;</xsl:text><!-- page 6 old pareil que ppcm; trouvé dans 85 p22-->
		</xsl:when>
		<xsl:when test=".='&prop;'"> <!-- signe proportionnel à  mais utilisé comme précède, antérieur à -->
			<xsl:text>&pt46;&pt46;&pt126;</xsl:text><!-- page 6 old pareil que ppcm-->
		</xsl:when>
		<xsl:when test=".='&lang;'"> <!-- signe de chevron gauche (<) -->
				<xsl:text>(chevron_gauche_ndnn)</xsl:text>
		</xsl:when>
		<xsl:when test=".='&rang;'"> <!-- signe de chevron droit (>) -->
			<xsl:text>(chevron_droit_ndnn)</xsl:text>
		</xsl:when>
		<xsl:when test=".='&coprod;'"> <!-- coproduit (pi à  l'envers) NDNN-->
			<xsl:text>&pt14;&pt135;&pt4;&pt1234;</xsl:text>
		</xsl:when>
		<xsl:when test=".='&Int;'"> <!-- intégrale double-->
			<xsl:text>&pt12346;&pt12346;</xsl:text>
		</xsl:when>
		<xsl:when test=".='&tint;'"> <!-- intégrale triple-->
			<xsl:text>&pt12346;&pt12346;&pt12346;</xsl:text>
		</xsl:when>
		<xsl:when test=".='&conint;'"> <!-- intégrale de contour-->
			<xsl:text>&pt46;&pt12346;</xsl:text>
		</xsl:when>
		<xsl:when test=".='&Conint;'"> <!-- intégrale de surface-->
			<xsl:text>&pt46;&pt12346;&pt12346;</xsl:text>
		</xsl:when>
		<xsl:when test=".='&Cconint;'"> <!-- intégrale de volume-->
			<xsl:text>&pt46;&pt12346;&pt12346;&pt12346;</xsl:text>
		</xsl:when>
		<xsl:when test=".='&real;'"> <!-- R gothique (partie réelle)-->
			<xsl:text>&pt45;&pt46;&pt1235;</xsl:text>
		</xsl:when>
		<xsl:when test=".='&image;'"> <!-- I gothique (partie imaginaire)-->
			<xsl:text>&pt45;&pt46;&pt24;</xsl:text>
		</xsl:when>
		<xsl:when test=".='&hbar;' or .='ℏ'"> <!-- h barré constante de planck sur 2Pi, non normé-->
			<xsl:text>&pt456;&pt36;&pt125;</xsl:text>
		</xsl:when>
		<xsl:when test=".='&hlambdabar;' or .='ƛ'"> <!-- h barré constante de planck sur 2Pi, non normé-->
			<xsl:text>&pt456;&pt36;&pt123;</xsl:text>
		</xsl:when>
		<xsl:when test=".='&image;'"> <!-- I gothique (partie imaginaire)-->
			<xsl:text>&pt45;&pt46;&pt24;</xsl:text>
		</xsl:when>
		<xsl:when test=".='&ring;'"> <!-- petit rond suscrit (ou en exposant) p20 old-->
			<xsl:text>petit_rond<!--&pt45;&pt3456;--></xsl:text>
		</xsl:when>
		<xsl:when test=".='&ogon;'"> <!-- angle rentrant p34 old-->
			<xsl:text>&pt456;&pt45;&pt25;</xsl:text>
		</xsl:when>
		<xsl:when test=".='&breve;'"> <!-- arc de cercle suscrit p.35 old-->
			<xsl:text>&pt4;&pt4;&pt25;</xsl:text>
		</xsl:when>
		<xsl:when test=".='&acute;'"> <!-- accent aigu -->
			<xsl:text>accent_aigu</xsl:text>
		</xsl:when>
		<xsl:when test=".='&grave;'"> <!-- accent grave -->
			<xsl:text>accent_grave</xsl:text>
		</xsl:when>
		<xsl:when test=".='&dot;'"> <!-- point suscrit-->
			<xsl:text>&pt46;&pt256;</xsl:text>
		</xsl:when>
		<xsl:when test=".='&Dot;'"> <!-- double point suscrit-->
			<xsl:text>&pt46;&pt256;&pt46;&pt256;</xsl:text>
		</xsl:when>
		<xsl:when test=".='&tdot;'"> <!-- triple point suscrit-->
			<xsl:text>&pt46;&pt256;&pt46;&pt256;&pt46;&pt256;</xsl:text>
		</xsl:when>
		<xsl:when test=".='&UnderBar;'"> <!-- trait horizontal souscrit (souligné -->
			<xsl:text>&pt456;&pt36;</xsl:text>
		</xsl:when>
		<!-- **** là  c'est les trucs auxquels on peut faire correspondre directement un pt123456 NN************* -->
		<xsl:otherwise>
			<xsl:call-template name="communMoMi">
				<xsl:with-param name="type" select="mo"/>
				<xsl:with-param name="chaine" select="."/>
			</xsl:call-template>
		</xsl:otherwise>
	</xsl:choose>
	
</xsl:template>

<!-- *********************************************************************************************************
		Racines carrees et nieme NN
		******************************************* -->
<xsl:template match="m:msqrt">
	<xsl:variable name="autreRacine" as="xs:boolean" 
		select="following-sibling::*[1][self::m:msqrt or self::m:mroot] or preceding-sibling::*[1][self::m:msqrt or self::m:mroot]" />
		
	<xsl:if test="$autreRacine"><xsl:value-of select="$dbk"/></xsl:if>
	
	<xsl:text>&pt345;</xsl:text><!-- signe de racine -->
	<xsl:choose>
		<xsl:when test="count(child::*[1]/descendant::*) &lt; 2 or (count(child::*[1]/child::*) &lt; 3 and (local-name(child::*[1]/child::*[1])='mo' ))">
			<xsl:apply-templates/> <!-- racine simple -->
		</xsl:when>
		<xsl:when test="count(child::*[1]/child::*) &lt; 2 and local-name(child::*[1]/child::*[1])='mn'">
		<!-- cas d'un chiffre avec virgule à l'intérieur -->
			<xsl:apply-templates/> <!-- racine simple -->
		</xsl:when>

		<xsl:otherwise> <!-- racine complexe -->
			<xsl:value-of select="$dbk" /> <!-- debut de bloc -->
			<xsl:apply-templates>
				<xsl:with-param name="bloc_deja_ouvert" select="true()" tunnel="yes"/>
			</xsl:apply-templates>
			<xsl:call-template name="finblock_ptvirgule" />
		</xsl:otherwise>
	</xsl:choose>
	
	<xsl:if test="$autreRacine"><xsl:call-template name="finblock_ptvirgule" /></xsl:if>
</xsl:template>

<xsl:template match="m:mroot">
<!-- signe nieme -->

<xsl:variable name="autreRacine" as="xs:boolean" 
		select="following-sibling::*[1][self::m:msqrt or self::m:mroot] or preceding-sibling::*[1][self::m:msqrt or self::m:mroot]" />

<xsl:if test="$autreRacine"><xsl:value-of select="$dbk"/></xsl:if>
<xsl:text>&pt4;</xsl:text>

<xsl:apply-templates select="child::*[2]"/> <!-- exposant de la racine -->
<xsl:text>&pt345;</xsl:text><!-- signe de racine -->
<xsl:choose>
	<xsl:when test="count(child::*[1]/descendant::*) &lt; 2 or (count(child::*[1]/child::*) = 2 and local-name(child::*[1]/child::*[1])='mo')">
		<xsl:apply-templates select="child::*[1]"/> <!-- racine simple -->
	</xsl:when>
	<xsl:when test="count(child::*[1]/child::*) &lt; 2 and local-name(child::*[1]/child::*[1])='mn'">
		<!-- cas d'un chiffre avec virgule à l'intérieur -->
			<xsl:apply-templates select="child::*[1]"/> <!-- racine simple -->
		</xsl:when>
	<xsl:otherwise> <!-- racine complexe -->
		<xsl:value-of select="$dbk" /> <!-- debut de bloc -->
		<xsl:apply-templates select="child::*[1]">
			<xsl:with-param name="bloc_deja_ouvert" select="true()" tunnel="yes"/>
		</xsl:apply-templates>
		<xsl:call-template name="finblock_ptvirgule" />
	</xsl:otherwise>
</xsl:choose>
<xsl:if test="$autreRacine"><xsl:call-template name="finblock_ptvirgule" /></xsl:if>
</xsl:template>

<!-- *******************************************
	  Fractions et trucs dessus-dessous NN
	  ******************************************* -->

<xsl:template match="m:mfrac">
	<!-- TODO Ajouter les autres symboles (inégalités, etc.) dans nonComplx -->
	<xsl:variable name="NonComplx">&rarr;&xrarr;&map;&larr;&xlarr;&lArr;&rArr;+-=;&minus;&ndash;&plus;&equals;| &verbar;&Verbar;&mid;&lt;&gt;&le;&ge;&ne;&neq;&approx;&asymp;&simeq;&cong;&sim;&ape;&approxeq;&leq;&Gt;&Lt;</xsl:variable>
	<xsl:variable name="NonComplxGauche" select="concat($NonComplx,'([{')" />
	<xsl:variable name="NonComplxDroite" select="concat($NonComplx,')]}')" />
	<xsl:variable name="ancetreSubSup" select="ancestor::*[local-name(.)=('msup','msub','msubsup','munder','mover','munderover')]" />

	
	<!-- GROS TRUC BALEZE : a cause d'openoffice qui est fantaisiste sur les mrow, on va déterminer les "vrais" preceding and following siblings 
	Probablement devenu inutile avec la suppression des mrow en trop pendant la 1ere passe -->
	<xsl:variable name="vrai-following-sibling">
		<xsl:choose>
			<xsl:when test="following-sibling::*[not(local-name(.)='mtext')]">
				<xsl:value-of select="string(following-sibling::*[not(local-name(.)='mtext')][1])" />
			</xsl:when>
			<xsl:when test="local-name(..)='mrow'"> <!-- on a un parent mrow sans following sibling -->
				<!-- on remonte au premier mrow qui n'a pas un seul fils mrow -->
				<xsl:value-of select="string(ancestor::*[local-name(.)='mrow'][count(child::*[not(local-name(.)='mrow')])>0][1]/following-sibling::*[not(local-name(.)='mtext')][1])" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="'none'"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	
	<xsl:variable name="vrai-preceding-sibling">
		<xsl:choose>
			<xsl:when test="preceding-sibling::*[not(local-name(.)='mtext')]">
				<xsl:value-of select="string(preceding-sibling::*[not(local-name(.)='mtext')][1])" />
			</xsl:when>
			<xsl:when test="local-name(..)='mrow'"> <!-- on a un parent mrow sans preceding sibling -->
				<!-- on remonte au premier mrow qui n'a pas un seul fils mrow -->
				<xsl:value-of select="string(ancestor::*[local-name(.)='mrow'][count(child::*[not(local-name(.)='mrow')])>0][1]/preceding-sibling::*[not(local-name(.)='mtext')][1])" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="'none'"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	
	<xsl:variable name="complexe">
		<!-- la fraction necessite-t-elle des blocs ? ajout : éviter les espaces mtext dans les siblings ;-->
		<xsl:choose>
			<xsl:when test="parent::*[local-name(.)='mfenced'] and count (../child::*)=1"> <!--openoffice : la fraction est seule entourée de fenced -->
				<xsl:text>non3</xsl:text>
			</xsl:when>
			<xsl:when test="(($vrai-following-sibling='none') or translate(substring($vrai-following-sibling,1,1), $NonComplxDroite,'')='' or ($vrai-following-sibling='&amp;lt;'))
				and (($vrai-preceding-sibling='none') or translate(substring($vrai-preceding-sibling,1,1), $NonComplxGauche,'')='' or ($vrai-preceding-sibling='&amp;lt;'))">
				<xsl:text>non1</xsl:text> <!-- destiné uniquement aux tests -->
			</xsl:when>
			<xsl:when test="($ancetreSubSup) and ((parent::*=$ancetreSubSup) or 
				((parent::*=$ancetreSubSup/m:mrow) and not((following-sibling::*) or (preceding-sibling::*))))">
				<!-- si la frac est descendante seule (avec evt un mrow) d'un sup,sub,underover, etc c'est pas elle qui met ses blocs, c'est l'ancetre -->
				<xsl:text>non2</xsl:text> <!-- destiné uniquement aux tests -->
			</xsl:when>
			<xsl:when test="(substring($vrai-following-sibling,1,1)='[' and preceding::text()=';') or
							(substring($vrai-preceding-sibling,1,1)=']' and following::text()=';')">
				<!-- cas des intervalles : pas de blocs meme si ] a gauche ou [ a droite -->
				<xsl:text>non4</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>oui</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	
	<!-- nouvelle norme : fraction encadree par des blocs si * ou / apres -->
	<xsl:if test="$complexe='oui'">
		<xsl:value-of select="$dbk" />
	</xsl:if>
	
	<!-- uniquement pour test -->
	<!--<xsl:if test="not($complexe='oui')">
		<xsl:value-of select="$complexe" />
	</xsl:if> -->
	
	<xsl:variable name="operateurs" select="functx:chars('+-×÷&times;&divide;&plus;&minus;&ndash;')" />
	<!--<xsl:message>
		<xsl:value-of select="trace(local-name(child::*[1]),'name child1 ')" />
		<xsl:value-of select="trace(local-name(child::*[2]),'name child2 ')" />
		<xsl:value-of select="trace(string-join(child::*[1]/child::*[contains($operateurs,.) and not(@membreFonc)],'_'),'child1 ')" />
		<xsl:value-of select="trace(string-join(child::*[2]/child::*[contains($operateurs,.) and not(preceding-sibling::*[.='('] or parent::*=m:mfenced)],'_'),'child2 ')" />
	</xsl:message> -->
	<xsl:choose>
		<xsl:when test="count(child::*[1]/child::*[functx:contains-any-of(.,$operateurs) and not
			(count(preceding-sibling::*[.='(' or .='[']) >  count(preceding-sibling::*[.=']' or .=')']) or parent::*[self::m:mfenced])])=0">
			<!-- NEW : s'il n'y a aucun +-*div au numerateur qui ne soit pas entre parenthese ou crochets, pas de bloc -->
			<xsl:apply-templates select="child::*[1]"/>
		</xsl:when>
		<xsl:when test="*[1][self::m:mrow and *[1][@fonc]] and matches(string(*[1]),'^[A-z]*\(.*\)$')">
			<!-- cas d'une fonction genre ln(x+2) -->
			<xsl:apply-templates select="child::*[1]"/>
		</xsl:when>
		<xsl:when test="((count(child::*[1]/child::*) &lt; 2) and (not(local-name(child::*[1]/child::*[1])='mfrac')))
				or (count(child::*[1]/child::*)=2 and contains('+-',child::*[1]/child::*[1]) and not(local-name(child::*[1]/child::*[2])='mfrac'))
				or ((translate (string(child::*[1]),concat($l_alphanumgrec,'()'),'')='') 
					and (count(child::*[1]/descendant::*)=count(child::*[1]/descendant::*[local-name(.)='mn' or local-name(.)='mi' or local-name(.)='mo' or local-name(.)='mrow'])))">
			<!-- TODO : optimiser ce test (le second est certainement devenu inutile depuis le troisième) -->
			<!-- signification du troisième test : s'il n'y a que des chiffres et des lettres et que des mi mo mn mrow comme descendants, c'est simple -->
			<!-- numerateur simple donc pas de bloc -->
			<xsl:apply-templates select="child::*[1]"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$dbk" />
				<xsl:apply-templates select="child::*[1]">
					<xsl:with-param name="bloc_deja_ouvert" select="true()" tunnel="yes"/>
				</xsl:apply-templates>
			<xsl:call-template name="finblock_ptvirgule" />
		</xsl:otherwise>
	</xsl:choose>
	<xsl:value-of select="$coupe"/>
	<xsl:text>&pt34;</xsl:text> <!-- signe de division -->
	<xsl:choose> 
		<xsl:when test="count(child::*[2]/child::*[functx:contains-any-of(.,$operateurs) and not
			(count(preceding-sibling::*[.='(' or .='[']) >  count(preceding-sibling::*[.=']' or .=')']) or parent::*[self::m:mfenced])])=0">
			<!-- NEW and DANGEROUS : s'il n'y a aucun +-*div au numerateur, pas de bloc -->
			<xsl:apply-templates select="child::*[2]"/>
		</xsl:when>
		<xsl:when test="(count(child::*[2]/child::*) &lt; 2 and not(local-name(child::*[2]/child::*[1])='mfrac'))
				or (count(child::*[2]/child::*)=2 and local-name(child::*[2]/child::*[1])='mo' and not(local-name(child::*[2]/child::*[2])='mfrac'))
				or ((translate (string(child::*[2]),concat($l_alphanumgrec,'()'),'')='') 
					and (count(child::*[2]/descendant::*)=count(child::*[2]/descendant::*[local-name(.)='mn' or local-name(.)='mi' or local-name(.)='mo' or local-name(.)='mrow'])))">
			<!-- TODO : optimiser ce test (le second est certainement devenu inutile depuis le troisième) -->
			<!-- denominateur simple donc pas de bloc -->
			<xsl:apply-templates select="child::*[2]"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$dbk" />
			<xsl:apply-templates select="child::*[2]">
				<xsl:with-param name="bloc_deja_ouvert" select="true()" tunnel="yes"/>
			</xsl:apply-templates>
			<xsl:call-template name="finblock_ptvirgule">
				<xsl:with-param name="bloc_deja_ouvert" select="$complexe='oui'" tunnel="yes"/>
			</xsl:call-template>
		</xsl:otherwise>
	</xsl:choose>
	<!-- fraction encadree par des blocs ou parentheses si * ou / apres -->
	<xsl:if test="$complexe='oui'">
		<xsl:call-template name="finblock_ptvirgule" />
	</xsl:if>
</xsl:template>

<xsl:template match="m:mover">
	<xsl:choose>
		<xsl:when test="local-name(parent::*)='mtext'">
			<!-- experimental ! si le parent est un mtext alors c'est différent mais c'est à  déconseiller -->
			<xsl:apply-templates select="child::*[1]"/>
			<xsl:choose><!-- TODO appliquer une règle de majuscule sur le mot entier -->
				<xsl:when test="contains('ABCDEFGHIJKLMNOPQRSTUVWXYZ',normalize-space(text()))">
					<xsl:text>&pt46;</xsl:text>
					<xsl:call-template name="bijectionMath">
						<xsl:with-param name="mot" select="text()"/>
					</xsl:call-template>
					<!--<xsl:value-of select="translate(text(),'ABCDEFGHIJKLMNOPQRSTUVWXYZ','&pt1;&pt12;&pt14;&pt145;&pt15;&pt124;&pt1245;&pt125;&pt24;&pt245;&pt13;&pt123;&pt134;&pt1345;&pt135;&pt1234;&pt12345;&pt1235;&pt234;&pt2345;&pt136;&pt1236;&pt2456;&pt1346;&pt13456;&pt1356;')" />-->
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="bijectionMath">
						<xsl:with-param name="mot" select="text()"/>
					</xsl:call-template>
					<!--<xsl:value-of select="text()"/>-->
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		<xsl:when test="string(.)='A°' or string(.)='A&ring;'">
			<!-- cas particulier de l'angstrom -->
			<xsl:text>&pt46;&pt1;&pt4;&pt135;</xsl:text>
		</xsl:when>
		<xsl:otherwise>
			<!-- mover classique et éprouvé -->
			<xsl:if test="preceding-sibling::*[1][@fonc]"><xsl:value-of select="$dbk" /></xsl:if>
			<xsl:apply-templates select="*[2]"/>
			<xsl:variable name="simple" as="xs:boolean">
				<!-- Note : en xpath child:: peut être sous-entendu -->
				<!-- signes d'angle (pas utilisés mais peut servir) : '&hat;','&Hat;','&angsph;','&#710;' -->
				<xsl:value-of select="(count(*[1]/*) &lt; 2 or (count(*[1]/*)=2 and string(*[1]/*[1])=$seq_plusminus))
					or (string(*[2])=('&breve;','&rarr;','&xrarr;','&horbar;','ˉ','&macr;','&amp;macr;', '&ang;','&hat;','&Hat;','&angsph;','&#710;','&circ;','&frown;') 
					  and not(functx:contains-any-of(string(*[1]),('+','&plus;','-','&minus;','&ndash;','*','&times;','/','&divide;','÷'))))" />
				<!-- 1 seul truc sous le signe OU (vecteur ET pas de signe d'opérateur dedans) -->
			</xsl:variable>
			<xsl:choose>
				<xsl:when test="$simple">
								<!-- or (count(child::*[1]/child::*[not(local-name(.)='mi')])=0)"> Bloc enlevé en 2008 mais rajouté en 2010 -->
					<!-- ce qu'il y a dessous est simple (ou il n'y a que des lettres NON plus vrai depuis 2010) -->
					<xsl:apply-templates select="child::*[1]"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$dbk" />
					<xsl:apply-templates select="child::*[1]">
						<xsl:with-param name="bloc_deja_ouvert" select="true()" tunnel="yes"/>
					</xsl:apply-templates>
					<xsl:call-template name="finblock_ptvirgule">
						<xsl:with-param name="bloc_deja_ouvert" select="fn:exists(preceding-sibling::*[1][@fonc])" tunnel="yes"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:if test="preceding-sibling::*[1][@fonc]"><xsl:call-template name="finblock_ptvirgule" /></xsl:if>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>
	
<xsl:template match="m:munder">
	<xsl:apply-templates select="child::*[1]"/>
	<!-- signe d'indice souscrit -->
	<xsl:text>&pt26;&pt26;</xsl:text>
	
	<xsl:choose>
		<xsl:when test="count(child::*[2]/child::*) &lt; 2 or (count(child::*[2]/child::*)=2 and string(*[2]/*[1])=$seq_plusminus)">
			<!-- indice simple -->
			<xsl:apply-templates select="child::*[2]"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$dbk" />
			<xsl:apply-templates select="child::*[2]">
				<xsl:with-param name="bloc_deja_ouvert" select="true()" tunnel="yes"/>
			</xsl:apply-templates>
			<xsl:call-template name="finblock_ptvirgule" />
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- <xsl:template match="m:munderover">
	<xsl:when test="child::*[1]='&int;'"> integrale NN
		<xsl:apply-templates select="child::*[1]"/>
		<xsl:text>&pt26;</xsl:text>
		<xsl:apply-templates select="child::*[2]"/>
		<xsl:text>&pt34;</xsl:text>
		<xsl:apply-templates select="child::*[3]"/>
	</xsl:template> -->
	
<xsl:template match="m:munderover">
	<xsl:apply-templates select="child::*[1]"/>
	<!-- signe d'indice souscrit -->
	<xsl:text>&pt26;&pt26;</xsl:text>
	<xsl:choose>
		<xsl:when test="count(child::*[2]/child::*) &lt; 2 or (count(child::*[2]/child::*)=2 and string(*[2]/*[1])=$seq_plusminus)">
			<!-- indice simple -->
			<xsl:apply-templates select="child::*[2]"/>
		</xsl:when>
		<xsl:otherwise> <!-- indice complexe -->
			<xsl:value-of select="$dbk" />
			<xsl:apply-templates select="child::*[2]">
				<xsl:with-param name="bloc_deja_ouvert" select="true()" tunnel="yes"/>
			</xsl:apply-templates>
			<xsl:call-template name="finblock_ptvirgule" />
		</xsl:otherwise>
	</xsl:choose>
	<!-- signe d'indice suscrit
	on encadre l'indice par des blocs ou une fin d'indice
	s'il est complexe OU s'il appartient au même sous-ensemble que ce qui suit (chiffres, lettres, etc.)-->
	<xsl:text>&pt4;&pt4;</xsl:text>
	<!-- c'est comme le truc pour msubsup mais ça fait planter saxon !!!! alors je le fais en xsl plutot qu'en xpath -->
	<xsl:variable name="indice" select="*[3]/text()" />
	<xsl:variable name="typeExpM" as="xs:string*">
		<xsl:for-each select="($l_min, $l_chiffres, $l_grec_min)">
			<xsl:if test="contains(.,$indice)">
				<xsl:value-of select="." />
			</xsl:if>
		</xsl:for-each>
	</xsl:variable>
	<xsl:variable name="typeExp" as="xs:string?" select="if (count($typeExpM)=1) then $typeExpM else ()" />
<!--<xsl:message select=".,'following', following::*,'&#10;******&#10;',string-join((.,following::*,$typeExp),'%'),'&#10;********************&#10;'" />-->
	<xsl:choose>
		<xsl:when test="(count(*[3]/*) &lt; 2 or (count(*[3]/*)=2 and local-name(*[3]/*[1])='mo'))
			and (not(following::*) or not(contains($typeExp,following::*[1]/text())) or $typeExp=$l_min)">
			<!-- exposant simple et non suivi d'un même truc simple -->
			<xsl:apply-templates select="*[3]"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$dbk" />
				<xsl:apply-templates select="*[3]">
					<xsl:with-param name="bloc_deja_ouvert" select="true()" tunnel="yes"/>
				</xsl:apply-templates>
			<xsl:call-template name="finblock_ptvirgule" />
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>
		
<!-- *******************************************
		Exposants et indices NN
		******************************************* -->
<xsl:template match="m:msup">
	<xsl:param name="fonction" as="xs:boolean" select="false()" />
	
	<xsl:apply-templates select="child::*[1]"/>
	<!-- on met pas de signe d'exposant si on a un degré ou un prime-->
	<xsl:if test="not(child::*[2]=('''','&prime;','°','&deg;'))">
		<xsl:text>&pt4;</xsl:text>
	</xsl:if>
	<!--<xsl:if test="*[1]='e'"><xsl:message><xsl:copy-of select="."/></xsl:message></xsl:if>-->
	<xsl:choose>
		<xsl:when test="((count(child::*[2]/child::*) &lt; 2) and (not(local-name(child::*[2]/child::*[1])='mfrac')))
		   or (count(*[2]/*)=2 and string(*[2]/*[1])=$seq_plusminus and 
										(functx:is-a-number(translate(*[2]/*[2],',','.')) or string-length(string(*[2]/*[2]))=1))
			 or matches(string(*[2]),'^\([^)]*\)$') or string(*[2])=('ème','er','ième')">
			<!-- exposant simple qui n'est pas une fraction
			ou un doublon genre -x ou -25,12 mais pas -5x ; oo met <mn>5x</mn> le con
			ou déjà avec des parenthèses -->
			<xsl:apply-templates select="child::*[2]"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$dbk" />
			<xsl:apply-templates select="child::*[2]">
				<xsl:with-param name="bloc_deja_ouvert" select="true()" tunnel="yes"/>
			</xsl:apply-templates>
			<xsl:call-template name="finblock_ptvirgule" />
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="m:msub">
	<xsl:apply-templates select="child::*[1]"/>
	<xsl:if test="not(*[1]/text()='|')">
		<!-- sinon on est dans le cas "fonction restreinte" cf norme math p 40 -->
		<xsl:text>&pt26;</xsl:text><!-- signe d'indice -->
	</xsl:if>
	
	<xsl:choose>
		<xsl:when test="count(child::*[2]/child::*) &lt; 2 or (count(child::*[2]/child::*)=2 and string(*[2]/*[1])=$seq_plusminus)
			or matches(string(*[2]),'^\([^)]*\)$')">
			<!-- indice simple ou déjà avec des parenthèses -->
			<xsl:apply-templates select="child::*[2]"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$dbk" />
			<xsl:apply-templates select="child::*[2]">
				<xsl:with-param name="bloc_deja_ouvert" select="true()" tunnel="yes"/>
			</xsl:apply-templates>
			<xsl:call-template name="finblock_ptvirgule" />
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>
		
<xsl:template match="m:msubsup">
	<!-- maj 10/11/04 : selon le bouton choisi pour le subsup dans MathType, des fois c'est l'exposant
			le deuxieme enfant, des fois c'est l'indice on est oblige de declarer les variables chacune au
			niveau le plus eleve (donc de faire 2 choose au lieu d'un) sinon saxon voit plus les variables -->
	<xsl:variable name="indic" as="xs:integer">
		<xsl:choose>
			<xsl:when test="local-name(*[1])='mstyle'">
				<xsl:value-of select="3" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="2" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	
	<xsl:variable name="exposant" as="xs:integer">
		<xsl:choose>
			<xsl:when test="local-name(*[1])='mstyle'">
				<xsl:value-of select="2" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="3" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	
	<xsl:apply-templates select="child::*[1]"/>
	<xsl:text>&pt26;</xsl:text><!-- signe d'indice -->
			
	<xsl:choose>
		<xsl:when test="count(*[number($indic)]/*) &lt; 2 or (count(*[number($indic)]/*)=2 and string(*[number($indic)]/*[1])=$seq_plusminus)">
			<!-- indice simple -->
			<xsl:apply-templates select="*[number($indic)]"/>
		</xsl:when>
		<xsl:otherwise> <!-- indice complexe -->
			<xsl:value-of select="$dbk" />
			<xsl:apply-templates select="*[number($indic)]">
				<xsl:with-param name="bloc_deja_ouvert" select="true()" tunnel="yes"/>
			</xsl:apply-templates>
			<xsl:call-template name="finblock_ptvirgule">
				<xsl:with-param name="bloc_deja_ouvert" select="true()" tunnel="yes"/> <!-- car on a le bloc qui suit pour l'exposant -->
			</xsl:call-template>
		</xsl:otherwise>
	</xsl:choose>
	<!-- signe d'exposant
			on encadre l'exposant par des blocs ou une fin d'exposant
			s'il est complexe OU s'il appartient au même sous-ensemble que ce qui suit (chiffres, lettres, etc.) OU si les deux c'est des lettres min-->
	<xsl:text>&pt4;</xsl:text>
	<xsl:variable name="typeExpM" as="xs:string*" select="for $t in ($l_min, $l_chiffres, $l_grec_min) 
													return (if (contains($t,*[$exposant]/text())) then $t else ())" />
	<xsl:variable name="typeExp" as="xs:string?" select="if (count($typeExpM)=1) then $typeExpM else ()" />
<!--<xsl:message select=".,'following', following::*,'&#10;******&#10;',string-join((.,following::*,$typeExp),'%'),'&#10;********************&#10;'" />-->
	<!--<xsl:message select="local-name(*[number($exposant)]),'**',string-join(for $i in *[number($exposant)]/* return local-name($i),'*')" />-->
	<xsl:choose>
		<xsl:when test="((count(*[number($exposant)]/*) &lt; 2 and not (*[number($exposant)]/m:mfrac))
			or (count(*[number($exposant)]/*)=2 and local-name(*[number($exposant)]/*[1])='mo')
			or (string(*[number($exposant)]/*[1])=('(','[','{') and string(*[number($exposant)]/*[last()])=(')',']','}')))
			and (not(following::*) or not(contains($typeExp,following::*[1]/text())) or $typeExp=$l_min)">
			<!-- exposant simple et non suivi d'un même truc simple -->
			<xsl:apply-templates select="*[number($exposant)]"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$dbk" />
				<xsl:apply-templates select="*[number($exposant)]">
					<xsl:with-param name="bloc_deja_ouvert" select="true()" tunnel="yes"/>
				</xsl:apply-templates>
			<xsl:call-template name="finblock_ptvirgule" />
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="m:mmultiscripts">
<!-- tag pour indices et exposants à gauche -->
<!-- 2 notations differentes selon chimie ou maths -->
<!-- TODO : option chimie dans interface qui doit ensuite être prise en compte ici -->

	<xsl:variable name="blocs_necessaires" as="xs:boolean">
		<xsl:value-of select="following-sibling::*[1][self::m:mmultiscripts] or preceding-sibling::*[1][self::m:mmultiscripts]"/>
	</xsl:variable>
	
	<xsl:if test="$blocs_necessaires"><xsl:value-of select="$dbk" /></xsl:if>
	
	<xsl:if test="string(child::*[3])"> <!-- présence d'un indice -->
		<xsl:text>&pt26;</xsl:text> <!-- signe indice -->
		<xsl:choose>
			<xsl:when test="count(child::*[3]/child::*) &lt; 2 or (count(child::*[3]/child::*)=2 and string(*[3]/*[1])=$seq_plusminus)">
				<!-- indice simple -->
				<xsl:apply-templates select="child::*[3]"/>
			</xsl:when>
			<xsl:otherwise> <!-- indice complexe -->
				<xsl:value-of select="$dbk" />
				<xsl:apply-templates select="child::*[3]">
					<xsl:with-param name="bloc_deja_ouvert" select="true()" tunnel="yes"/>
				</xsl:apply-templates>
				<xsl:call-template name="finblock_ptvirgule" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:if>
	
	<xsl:if test="string(child::*[4])"> <!-- présence d'un exposant -->
		<xsl:text>&pt4;</xsl:text> <!-- signe exposant -->
		<xsl:choose>
			<xsl:when test="count(child::*[4]/child::*) &lt; 2 or (count(child::*[4]/child::*)=2 and string(*[4]/*[1])=$seq_plusminus)">
				<!-- exposant simple -->
				<xsl:apply-templates select="child::*[4]"/>
			</xsl:when>
			<xsl:otherwise> <!-- exposant complexe -->
				<xsl:value-of select="$dbk" />
				<xsl:apply-templates select="child::*[4]">
					<xsl:with-param name="bloc_deja_ouvert" select="true()" tunnel="yes"/>
				</xsl:apply-templates>
				<xsl:call-template name="finblock_ptvirgule">
					<xsl:with-param name="bloc_deja_ouvert" select="true()" tunnel="yes"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:if>
	
	<xsl:apply-templates select="child::*[1]"/> <!-- membre principal -->
	
	<xsl:if test="$blocs_necessaires"><xsl:call-template name="finblock_ptvirgule" /></xsl:if>
	
</xsl:template>
		
<!-- **************************** systèmes d'équations, matices, déterminants, etc.. ********************* -->
<xsl:template match="m:mtable"><!--mtr/mtd"-->
	<xsl:param name="bloc_deja_ouvert" select="false()" as="xs:boolean" tunnel="yes" />
	<xsl:if test="not(translate(string(preceding::*[1]),'([|{','AAAA')='A')">
	<!-- pour que les structures ne soient pas répétées car elles ont déjà été appelées en mode "appel"-->
		<xsl:choose>
			<xsl:when test="..[self::m:munder] or ../..[self::m:munder]">
				<!-- cas d'une limite avec plusieurs conditions cf p44 norme math 2ème exemple -->
				<xsl:if test="not($bloc_deja_ouvert)"><xsl:value-of select="$dbk"/></xsl:if>
				<xsl:for-each select="m:mtr">
					<xsl:apply-templates />
					<xsl:if test="not(position()=last())"><xsl:text>&pt2;</xsl:text></xsl:if>
				</xsl:for-each>
				<xsl:if test="not($bloc_deja_ouvert)"><xsl:call-template name="finblock_ptvirgule" /></xsl:if>
			</xsl:when>
			<xsl:when test="$linearise_table">
				<xsl:call-template name="tableMath">
					<xsl:with-param name="lineaire" select="true()"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="tableMath"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:if>
</xsl:template>

<xsl:template match="m:mtable" mode="appel"><!--mtr/mtd"-->
	<xsl:choose>
		<xsl:when test="$linearise_table">
			<xsl:call-template name="tableMath">
				<xsl:with-param name="lineaire" select="true()"/>
			</xsl:call-template>
		</xsl:when>
		<xsl:otherwise>
			<xsl:call-template name="tableMath"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="tableMath">
	<xsl:param name="prefixe" select="('','')" as="xs:string*" tunnel="yes"/>
	<xsl:param name="suffixe" select="('','')" as="xs:string*" tunnel="yes"/>
	<!-- les deux params ci-dessus ont le premier multilignes pour linéaire et le second simple ligne pour 2D -->
	<xsl:param name="lineaire" select="false()" as="xs:boolean"/><!-- false: ne pas linéariser; true: linéariser -->
	
	<xsl:variable name="tableau_variation" as="xs:boolean" select="nat:is-variation-table(.)"/>

	<!-- dans quels cas linéarise-t-on toujours? -->

	<xsl:variable name="lineariser" as="xs:boolean">
		<xsl:choose>
			<xsl:when test="count(./m:mtr/m:mtd) &gt;= $min_cell_lin">
				<xsl:value-of select="false()"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="true()"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<!--<xsl:value-of select="trace($tableau_variation,'Tableau:')"/>-->
	
	<xsl:variable name="prefixeOK" select="if ($lineaire or $lineariser) then $prefixe[1] else $prefixe[2]"/>
	<xsl:variable name="suffixeOK" select="if ($lineaire or $lineariser) then $suffixe[1] else $suffixe[2]"/>
	
	<xsl:choose>
		<xsl:when test="$lineaire or $lineariser">
			<!-- TODO : peut être certainement optimisé avec moins de cas -->
			<xsl:value-of select="$prefixeOK"/>
			<xsl:choose>
				<xsl:when test="not (./m:mtr/m:mtd)"> 
				<!-- openoffice ne fait pas de mtd si une seule colonne A PRIORI OUTDATED PAR fr-maths-pass1 qui rajoute mtr -->
					<xsl:for-each select="m:mtr">
						<xsl:variable name="resu">
							<xsl:apply-templates/>
						</xsl:variable>
						<xsl:choose>
							<xsl:when test="$resu=''">
								<xsl:text>&pt5;&pt2;</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$resu"/>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:if test="not(position()=last())">
							<xsl:value-of select="$espace" />
							<xsl:value-of select="$coupe" />
							<xsl:text>&pt6;&pt345;</xsl:text>
							<xsl:value-of select="$espace" />
							<xsl:value-of select="$coupe" />
							<xsl:value-of select="$coupe"/>
						</xsl:if>
					</xsl:for-each>
				</xsl:when>
				<xsl:when test="string(preceding::*[1])='{' or empty(m:mtr[count(m:mtd) > 1])">
				<!-- c'est un systeme mathtype ou un tableau à une seule colonne-->
					<xsl:for-each select="m:mtr/m:mtd">
						<xsl:apply-templates/>
						<xsl:if test="not(position()=last()) ">
							<xsl:value-of select="$coupe"/>
							<xsl:value-of select="$espace" />
							<xsl:value-of select="$coupe" />
							<xsl:text>&pt6;&pt345;</xsl:text>
							<xsl:value-of select="$espace" />
							<xsl:value-of select="$coupe" />
						</xsl:if>
					</xsl:for-each>
				</xsl:when>
				<xsl:otherwise>
					<!-- <xsl:when test="preceding::*[1]='('"> c'est une matrice ou un tableau -->
					<!-- <xsl:text>&pt45;&pt236;</xsl:text> -->
					<xsl:for-each select="m:mtr">
						<xsl:for-each select="m:mtd">
							<xsl:variable name="resu">
								<xsl:apply-templates/>
							</xsl:variable>
							<xsl:choose>
								<xsl:when test="$resu='' or $resu='&pt;'">
									<xsl:text>&pt5;&pt2;</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="$resu"/>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:if test="not(position()=last())">
								<!--<xsl:if test="not($tableau_variation and string(.)='')">
									<!- évitons le double espace dans un tableau de variation ->
									<xsl:value-of select="$espace" /> BRUNO: remplacé par pt5pt2
								</xsl:if>-->
								<xsl:value-of select="$espace" />
							</xsl:if>
						</xsl:for-each>
						<xsl:if test="not(position()=last()) ">
							<xsl:value-of select="$coupe"/>
							<xsl:value-of select="$espace" />
							<xsl:value-of select="$coupe" />
							<xsl:text>&pt6;&pt345;</xsl:text>
							<xsl:value-of select="$espace" />
							<xsl:value-of select="$coupe" />
						</xsl:if>
					</xsl:for-each>
					<!-- <xsl:text>&pt45;&pt356;</xsl:text> fin de matrice -->
				</xsl:otherwise>
			</xsl:choose>
			<xsl:value-of select="$suffixeOK"/>
		</xsl:when>
		<!-- représentation non linéaire-->
		<xsl:otherwise>
			<xsl:variable name="col" as="xs:integer">
				<xsl:value-of select="count(m:mtr[1]/child::m:mtd)"/>
			</xsl:variable>
			<xsl:variable name="lignes" as="xs:integer">
				<xsl:value-of select="count(child::m:mtr)"/>
			</xsl:variable>
			<xsl:variable name="table" as="xs:string*">
				<xsl:for-each select="child::m:mtr">
					<xsl:for-each select="child::m:mtd">
						<xsl:variable name="cell" as="xs:string*">
							<xsl:if test="position() = 1">
								<xsl:value-of select="$prefixeOK"/>
							</xsl:if>
							<xsl:apply-templates/>
						</xsl:variable>
						<xsl:value-of select="translate(string-join($cell,''),concat($coupeEsth,$coupe,$debMath,$finMath),'')"/>
					</xsl:for-each>
				</xsl:for-each>
			</xsl:variable>
			<xsl:variable name="initTailles" as="xs:integer*">
				<xsl:for-each select="child::m:mtr[1]/m:mtd">
					<xsl:value-of select="0"/>
				</xsl:for-each>
			</xsl:variable>
			<xsl:variable name="tableMef" as="xs:string*">
				<xsl:call-template name="retourneTable">
					<xsl:with-param name="table" select="$table" tunnel="yes"/>
					<xsl:with-param name="nbLigne" select="$lignes" tunnel="yes"/>
					<xsl:with-param name="nbCol" select="$col" tunnel="yes"/>
					<xsl:with-param name="tailles" select="$initTailles"/>
					<xsl:with-param name="tabVar" select="$tableau_variation" tunnel="yes"/>
					<xsl:with-param name="suffixe" select="$suffixeOK" tunnel="yes"/>
					<xsl:with-param name="prefixe" select="$prefixeOK" tunnel="yes"/>
				</xsl:call-template>
			</xsl:variable>
			<xsl:choose>
				<xsl:when test="$tableMef='NULL'">
					<xsl:call-template name="tableMath">
						<xsl:with-param name="lineaire" select="true()"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<!--****** mise en forme ********** -->
					<!-- ajout d'une ligne supplémentaire pour les tableaux -->
					<!-- ajout des sauts de ligne -->
					<!--<xsl:message select="for $i in $tableMef return concat('num ',position(),' : ',nat:toTbfr($i),'**&#10;')" />-->
					<xsl:choose>
						<xsl:when test="$tableau_variation">
							<xsl:value-of select="$debTable"/><!-- pour indiquer qu'il y a une table -->
							<xsl:variable name="coupeCol" as="xs:integer*" select="doc:donneCoupeCol($tableMef[position()&lt;= $col],1,0,$col)"/>
							<!--<xsl:message><xsl:value-of select="$tableMef"/></xsl:message>-->
							<!--<xsl:message select="'coupeCol',$coupeCol" />-->
							<xsl:for-each select="$coupeCol"><!-- [position()&lt;last()]"> -->
								<!--<xsl:message><xsl:value-of select="."/></xsl:message>-->
								<xsl:variable name="limColMax" as="xs:integer" select="."/>
								<xsl:variable name="min" as="xs:integer" select="position()"/>
								<xsl:variable name="limColMin" as="xs:integer">
									<xsl:choose>
										<!-- c'est la première position -->
										<xsl:when test="$min=1"><xsl:value-of select="1"/></xsl:when>
										<!-- c'est une position suivante -->
										<xsl:otherwise><xsl:value-of select="$coupeCol[$min - 1]"/></xsl:otherwise>
									</xsl:choose>
								</xsl:variable>
								<!--<xsl:message><xsl:value-of select="$limColMin"/></xsl:message>-->
								<xsl:variable name="ligne1" as="xs:string*"><!---->
									<!-- la première case -->
									<xsl:value-of select="concat(substring($tableMef[1],1,string-length($tableMef[1])-1),'&pt456;',$coupe)"/>
									<!-- les cases suivantes -->
									<xsl:for-each select="$tableMef[position()&lt; $limColMax and position() &gt;= $limColMin and position()&gt;1]">
										<xsl:choose>
											<!-- c'est la dernière colonne de la série à afficher: on vire l'espace -->
											<xsl:when test="($min = 1 and position() = $limColMax - $limColMin -1) or
												($min &gt;1 and position() = $limColMax - $limColMin)">
												<xsl:value-of select="concat(functx:substring-before-last-match(.,$espace),$coupe)"/>
											</xsl:when>
											<xsl:otherwise><xsl:value-of select="concat(.,$coupe)"/></xsl:otherwise>
										</xsl:choose>
										<xsl:if test="position() mod $col = 0 and not(position()=last())">
											<xsl:value-of select="$sautAGenerer"/>
										</xsl:if>
									</xsl:for-each>
								</xsl:variable>
								<!--{string-join($ligne1,'') || functx:re/>-->
								<xsl:value-of select="$sautAGenerer"/>
								<!-- ligne de séparation de la première case -->
								<xsl:call-template name="rempli">
									<xsl:with-param name="nb" select="string-length(translate($ligne1[1],$coupe,'')) -1" />
									<xsl:with-param name="motif" select="'&pt25;'"/>
								</xsl:call-template>
								<!-- séparateur de la première colonne -->
								<xsl:value-of select="'&pt2456;'"/>
								<!-- ligne de séparation des autres cases -->
								<xsl:call-template name="rempli">
									<xsl:with-param name="nb" select="string-length(translate(string-join($ligne1,''),$coupe,'')) - string-length(translate($ligne1[1],$coupe,''))" />
									<xsl:with-param name="motif" select="'&pt25;'"/>
								</xsl:call-template>
								<xsl:value-of select="$sautAGenerer"/>
								<!-- les autres lignes (ou fragments de lignes) -->
								<xsl:for-each select="$tableMef[position()&gt; $col]">
									<xsl:choose>
										<!-- colonne entete -->
										<xsl:when test="position() mod $col = 1">
											<xsl:value-of select="concat(substring(.,1,string-length(.)-1),'&pt456;',$coupe)"/>
										</xsl:when>
										<xsl:otherwise>
											<!--<xsl:message><xsl:value-of select="concat(nat:toTbfr(.),';',position(),';')"/></xsl:message>-->
											<xsl:if test="(((position() mod $col) &gt;= $limColMin) and ((position() mod $col) &lt; $limColMax)) 
													or ($min = count($coupeCol) and (position() mod $col) = 0)">
												<xsl:choose>
													<!-- c'est la dernière colonne de la série à afficher: on vire l'espace -->
													<xsl:when test="($min = 1 and position() mod $col = $limColMax - $limColMin)
														or (position() mod $col = $limColMax - 1) or (position()=last())">
														<xsl:value-of select="concat(functx:substring-before-last-match(.,$espace),$coupe)"/>
													</xsl:when>
													<!-- ni 1ère ni dernière -->
													<xsl:otherwise><xsl:value-of select="concat(.,$coupe)"/></xsl:otherwise>
												</xsl:choose>
											</xsl:if>
										</xsl:otherwise>
									</xsl:choose>
									
									<xsl:if test="position() mod $col = 0  and not($min = count($coupeCol) and position() = last())">
										<xsl:value-of select="$sautAGenerer"/> <!-- pas de saut à la fin du tableau, c'est la mise en page qui s'en charge -->
									</xsl:if>
								</xsl:for-each>
								<xsl:if test="not(position() = last())"><xsl:value-of select="concat($finTable,$debTable)"/></xsl:if>
							</xsl:for-each>
							<xsl:value-of select="$finTable"/><!-- pour indiquer qu'il y a une table -->
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="concat($coupe,$debTable)"/><!-- pour indiquer qu'il y a une table -->
							<xsl:for-each select="$tableMef">
								<xsl:value-of select="concat(.,$coupe)"/>
								<xsl:if test="position() mod $col = 0 and not(position()=last())">
									<xsl:value-of select="$sautAGenerer"/>
								</xsl:if>
							</xsl:for-each>
							<xsl:value-of select="concat($finTable,$coupe)"/><!-- pour indiquer qu'il y a une table -->
						</xsl:otherwise>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
			<!--<xsl:value-of select="$tableMef"/>-->
		</xsl:otherwise>
	</xsl:choose>
	<!--<xsl:if test="not($linearise_table)">
		<!- pour indiquer la fin de la table ->
		<xsl:value-of select="$finTable" />
	</xsl:if>-->
</xsl:template>

<xsl:function name="doc:donneCoupeCol" as="xs:integer*" xmlns:doc="espaceDoc">
	<xsl:param name="arg" as="xs:string*"/> 
	<xsl:param name="pos" as="xs:integer"/>
	<xsl:param name="used" as="xs:integer"/>
	<xsl:param name="col" as="xs:integer"/>
	<xsl:choose>
		<xsl:when test="$pos &gt; $col"><xsl:value-of select="$pos"/></xsl:when>
		<xsl:when test="$used + string-length($arg[$pos]) &gt; $longueur">
			<xsl:sequence select="xs:integer($pos),(doc:donneCoupeCol($arg, $pos + 1, string-length($arg[1]) + string-length($arg[$pos]),$col))"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:sequence select="doc:donneCoupeCol($arg, $pos + 1, $used + string-length($arg[$pos]),$col)"/>
		</xsl:otherwise>
	</xsl:choose>
   
</xsl:function>


<xsl:template name="retourneTable">
	<xsl:param name="nbCol" as="xs:integer" tunnel="yes"/>
	<xsl:param name="table" as="xs:string*" tunnel="yes"/>
	<xsl:param name="nbLigne" as="xs:integer" tunnel="yes"/>
	<xsl:param name="tailles" as="xs:integer*"/>
	<xsl:param name="i" as="xs:integer" select="0" tunnel="yes"/>
	<xsl:param name="tabVar" as="xs:boolean" tunnel="yes"/>
	<xsl:param name="suffixe" as="xs:string?" select="''" tunnel="yes"/>
	<xsl:param name="prefixe" select="''" as="xs:string?" tunnel="yes"/>
	<!--<xsl:value-of select="trace($nbCol,'name col ')" />
	<xsl:value-of select="trace($nbLigne,'name ligne ')" />
	<xsl:value-of select="trace($table,'name table ')" />
	<xsl:value-of select="trace($tailles,'name tailles ')" />
	<xsl:value-of select="trace($i,'name i ')" />-->
	<xsl:choose>
		<!-- calcul des tailles de colonnes -->
		<xsl:when test="$i &lt; count($table)">
			<xsl:choose>
				<xsl:when test="$tailles[$i mod $nbCol + 1] &lt; string-length($table[($i+1)])">
					<!--<xsl:value-of select="trace($i mod $nbCol + 1,'name modulo ')" />-->
					<xsl:variable name="lesTailles" as="xs:integer*">
						<xsl:for-each select="$tailles">
							<xsl:choose>
								<xsl:when test="not($i mod $nbCol + 1 = position())">
									<xsl:value-of select="."/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="string-length($table[$i + 1])"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</xsl:variable>
					<!--<xsl:value-of select="trace($lesTailles,'name taille après ')" />-->
					<xsl:call-template name="retourneTable">
						<xsl:with-param name="tailles" select="$lesTailles"/>
						<xsl:with-param name="i" select="$i + 1" tunnel="yes"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="retourneTable">
						<xsl:with-param name="tailles" select="$tailles"/>
						<xsl:with-param name="i" select="$i + 1" tunnel="yes"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		<!-- nouvelles tailles des cellules -->
		<xsl:otherwise>
			<!-- est-ce qu'on va pouvoir représenter la structure en 2D?-->
			<xsl:variable name="notLinOk" as="xs:boolean">
				<xsl:choose>
					<xsl:when test="$tabVar and (max($tailles[position() &gt; 1]) + $tailles[1] + 1 + string-length($suffixe)) &gt; $longueur"><!-- +1 pour les séparateurs -->
						<xsl:value-of select="false()"/>
					</xsl:when>
					<xsl:when test="not($tabVar) and (sum($tailles) + string-length(concat($prefixe,$suffixe))) &gt; $longueur">
						<xsl:value-of select="false()"/>
						<!--<xsl:message>
							<xsl:value-of select="concat(sum($tailles),' ',count($tailles),' ',string-length(concat($prefixe,$suffixe)),' ',sum($tailles) + count($tailles) + string-length(concat($prefixe,$suffixe)))"/>
						</xsl:message> -->
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="true()"/>
						<!--<xsl:message>
							<xsl:value-of select="concat(sum($tailles),' ',count($tailles),' ',string-length(concat($prefixe,$suffixe)),' ',sum($tailles) + count($tailles) + string-length(concat($prefixe,$suffixe)))"/>
						</xsl:message> -->
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<!--<xsl:value-of select="$notLinOk"/>-->
			<xsl:choose>
				<xsl:when test="$notLinOk">
					<xsl:for-each select="$table">
						<xsl:variable name="pos" select="position()" as="xs:integer"/>
						<!--<xsl:value-of select="trace(position(),'position')"/>
						<xsl:value-of select="trace($tailles[($pos -1) mod $nbCol + 1],'taille')"/>-->
						<xsl:variable name="sep" as="xs:integer">
							<xsl:choose>
								<xsl:when test="$pos mod $nbCol = 0"><!-- pas de séparateur pour la dernière colonne-->
									<xsl:value-of select="0"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="derniere" as="xs:string">
							<xsl:choose>
								<xsl:when test="($pos -1) mod $nbCol + 1 = count($tailles)">
									<xsl:value-of select="$suffixe"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="''"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:choose>
							<xsl:when test="string-length(.) &lt;= $tailles[($pos -1) mod $nbCol + 1]">
								<xsl:if test="$tabVar">
									<xsl:value-of select="nat:fill_center(.,$tailles[($pos -1) mod $nbCol + 1] - string-length(.) + $sep, $espace) || $derniere"/>
											<!-- le  + sep c'est pour le séparateur entre cellules-->
								</xsl:if>
								<xsl:if test="not($tabVar)">
									<xsl:value-of select="functx:repeat-string($espace,$tailles[($pos -1) mod $nbCol + 1] - string-length(.) + $sep) || $derniere"/>
								</xsl:if>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$espace || $derniere"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</xsl:when>
				<!-- ça va pas tenir, il faut linéariser -->
				<xsl:otherwise>
					<xsl:value-of select="'NULL'"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="bijectionMath">
	<xsl:param name="mot"/>
	<!-- utilisation d'une variable (pb des '")-->
	<xsl:variable name="v2">
		<xsl:text>&pt1;&pt12;&pt14;&pt145;&pt15;&pt124;&pt1245;&pt125;&pt24;&pt245;&pt13;&pt123;&pt134;&pt1345;&pt135;&pt1234;&pt12345;&pt1235;&pt234;&pt2345;&pt136;&pt1236;&pt2456;&pt1346;&pt13456;&pt1356;&pt12356;&pt16;&pt123456;&pt2346;&pt126;&pt1246;&pt146;&pt12456;&pt1456;&pt23456;&pt156;&pt1256;&pt12346;&pt345;&pt246;&pt1;&pt12;&pt14;&pt145;&pt15;&pt124;&pt1245;&pt125;&pt24;&pt245;&pt13;&pt123;&pt134;&pt1345;&pt135;&pt1234;&pt12345;&pt1235;&pt234;&pt2345;&pt136;&pt1236;&pt2456;&pt1346;&pt13456;&pt1356;&pt3456;&pt16;&pt126;&pt146;&pt1456;&pt156;&pt1246;&pt12456;&pt1256;&pt246;&pt12356;&pt16;&pt123456;&pt2346;&pt126;&pt1246;&pt146;&pt12456;&pt1456;&pt23456;&pt156;&pt1256;&pt12346;&pt345;&pt246;&pt345;&pt235;&pt36;&pt35;&pt25;&pt2356;&pt;&pt12346;&pt123456;&pt123456;&pt2;&pt23;&pt25;&pt256;&pt26;&pt235;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt3;&pt3;&pt3;&pt3;&pt236;&pt356;&pt12356;&pt23456;</xsl:text>
	</xsl:variable>
	<xsl:variable name="resu_sans_coupe" as="xs:string?">
		<xsl:choose>
			<xsl:when test="@type='mi'">
				<xsl:value-of select="translate(normalize-space($mot),'ABCDEFGHIJKLMNOPQRSTUVWXYZÀÂÉÈÊËÎÏÔÙÛÜ&Ccedil;&AElig;&OElig;abcdefghijklmnopqrstuvwxyz0123456789àâéèêëîïôùûüçæœ@+-×÷=&nbsp;&int;|&verbar;,;:.?!&quot;«»“”‘’&lsquo;&rsquo;&acute;&prime;()[]',string($v2))" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="translate($mot,'ABCDEFGHIJKLMNOPQRSTUVWXYZÀÂÉÈÊËÎÏÔÙÛÜ&Ccedil;&AElig;&OElig;abcdefghijklmnopqrstuvwxyz0123456789àâéèêëîïôùûüçæœ@+-×÷=&nbsp;&int;|&verbar;,;:.?!&quot;«»“”‘’&lsquo;&rsquo;&acute;&prime;()[]',string($v2))" />
				<!-- prime,x,divisé,/,=,*,+,-,[,],intégrale,|,barre verticale simple -->
				<!-- l'apostrophe (caractere 39) est change en . pour les derivees (point 3)-->
				<!-- la barre | (aussi appelee &verbar;) est changée en & pour les modules de cplx -->
				<!-- : est traduit par pt25 ("tel que") -->
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:value-of select="functx:replace-multi($resu_sans_coupe,('&pt23;','\*','&ThickSpace;'),(concat('&pt23;',$coupe),'&pt5;&pt35;',concat($espace,$coupeEsth)))"/>
</xsl:template>

<!-- ********* TRAITEMENT DES TRIGO ET AUTRE AVEC GROSSE PRIORITE ***** -->
          
<xsl:template match="m:mi[@fonc]" priority="2">
	<xsl:if test="preceding-sibling::*[1][@fonc]">
		<xsl:value-of select="$dbk" />
	</xsl:if>
	<xsl:choose>
		<xsl:when test="$trigoSpecif and @fonc='trigo'">
			<xsl:choose>
				<xsl:when test=".='sin'">
					<xsl:text>&pt346;</xsl:text>
				</xsl:when>
				<xsl:when test=".='cos'">
					<xsl:text>&pt46;&pt346;</xsl:text>
				</xsl:when>
				<xsl:when test=".='tan'">
					<xsl:text>&pt2346;</xsl:text>
				</xsl:when>
				<xsl:when test=".='cot' or .='cotan'">
					<xsl:text>&pt46;&pt2346;</xsl:text>
				</xsl:when>
				<xsl:when test=".='arcsin'">
					<xsl:text>&pt45;&pt346;</xsl:text>
				</xsl:when>
				<xsl:when test=".='arccos'">
					<xsl:text>&pt45;&pt46;&pt346;</xsl:text>
				</xsl:when>
				<xsl:when test=".='arctan'">
					<xsl:text>&pt45;&pt2346;</xsl:text>
				</xsl:when>
				<xsl:when test=".='arccot' or .='arccotan'">
					<xsl:text>&pt45;&pt46;&pt2346;</xsl:text>
				</xsl:when>
			</xsl:choose>
		</xsl:when>
		<xsl:otherwise>
			<xsl:call-template name="bijectionMath">
				<xsl:with-param name="mot" select="functx:replace-multi(string(.),('([A-Z])','&sup2;','&sup3;'),('&pt46;$1','&pt4;&pt126;','&pt4;&pt146;'))" />
			</xsl:call-template>
		</xsl:otherwise>
	</xsl:choose>

	<!-- le nom de la fonction a été mis, maintenant il faut des blocs ou pas dans certains cas précis -->
	<!-- les membres de cette fct sont le 1er et evt 2eme following dont le preceding[@fonc] est celle-ci -->
	<xsl:variable name="this" select="." />
	<xsl:variable name="membresFonction" select="following::*[@membreFonc and (functx:is-node-in-sequence(preceding::*[@fonc][1],$this))]" />
	
	<xsl:choose>
		<xsl:when test="count($membresFonction)=0"/>
		<!-- on fait rien : parenthèses qui suivent -->
		<xsl:when test="parent::*[self::m:msup or self::m:msub or self::m:mrow[(parent::m:msup or parent::m:msub) and count(*)=1]]" />
		<!-- on fait rien : parenthèses ou cos². dans le cas du cos² ou log indice 7, c'est le msup ou msub qui s'en charge -->
		<xsl:when test="$trigoSpecif and @fonc='trigo' and (
				(count($membresFonction)=1 and string-length(string($membresFonction[1]))=1) or
				(count($membresFonction)=2 and string($membresFonction[2])=('&deg;','°')))">
				<!-- cas de pas de bloc : trigospecif=1 et un seul char ou nombre+°  -->
			<xsl:apply-templates select="$membresFonction">
				<xsl:with-param name="forceAffiche" select="1" />
			</xsl:apply-templates>
		</xsl:when>
		<xsl:otherwise> <!-- on met des blocs -->
			<xsl:value-of select="$dbk" />
			<xsl:apply-templates select="$membresFonction">
				<xsl:with-param name="forceAffiche" select="1" />
				<xsl:with-param name="bloc_deja_ouvert" select="true()" tunnel="yes"/>
			</xsl:apply-templates>
			<xsl:call-template name="finblock_ptvirgule">
				<xsl:with-param name="noeud" select="$membresFonction[position()=last()]" />
			</xsl:call-template>
			<!--<xsl:value-of select="concat('**',following::*[not(@membreFonc)][1]/text(),'**',@membreFonc,'**')"/>-->
		</xsl:otherwise>
	</xsl:choose>
	<xsl:if test="preceding-sibling::*[1][@fonc]">
		<xsl:value-of select="$fbk" />
	</xsl:if>
</xsl:template>

<xsl:template match="m:*[@membreFonc]" priority="2">
	<xsl:param name="forceAffiche" select="0" />
	
	<xsl:if test="$forceAffiche=1">
		<xsl:next-match />
	</xsl:if>
</xsl:template>

<xsl:template match="m:msup[descendant::text()[1][parent::*[@fonc]]]|m:msub[descendant::text()[1][parent::*[@fonc]]]" priority="2">
	<xsl:variable name="this" select="descendant::text()[1][parent::*[@fonc]]/parent::*" />
	<!-- on stocke dans la variable this le noeud fonction mère -->
	<xsl:next-match>
		<xsl:with-param name="fonction" select="true()" />
	</xsl:next-match>
	<xsl:variable name="membresFonction" select="following::*[@membreFonc and (functx:is-node-in-sequence(preceding::*[@fonc][1],$this))]" />
	<xsl:if test="count($membresFonction) > 0">
		<xsl:value-of select="$dbk" />
		<xsl:apply-templates select="$membresFonction">
			<xsl:with-param name="forceAffiche" select="1" />
			<xsl:with-param name="bloc_deja_ouvert" select="true()" tunnel="yes"/>
		</xsl:apply-templates>
		<xsl:call-template name="finblock_ptvirgule" />
	</xsl:if>
</xsl:template>

<!-- ********* AJOUT D'un pt6 entre fermeture de bloc et pt virgule Nouvelle norme ***** -->
<xsl:template name="finblock_ptvirgule">
	<xsl:param name="noeud" as="element()" select="." />
	<xsl:param name="bloc_deja_ouvert" as="xs:boolean" select="false()" tunnel="yes" />
	<xsl:value-of select="$fbk" />
	<xsl:if test="not($bloc_deja_ouvert) and ($noeud/following::*[not(ancestor-or-self::*/@membreFonc)][1]/text()=';')">
		<xsl:text>&pt6;</xsl:text>
	</xsl:if>
</xsl:template>

<!-- ***** template de choix d'un suffixe selon les parenthèses/accolades/crochets encadrant un mtable -->
<xsl:template name="choose_suffixe">
	<xsl:param name="position" select="1" />
	<xsl:choose>
		<xsl:when test="')'=(following-sibling::*[$position],..[self::m:mrow]/following-sibling::*[$position])">
			<xsl:sequence select="('&pt45;&pt356;','&pt356;')" />
		</xsl:when>
		<xsl:when test="']'=(following-sibling::*[$position],..[self::m:mrow]/following-sibling::*[$position])">
			<xsl:sequence select="('&pt45;&pt23456;','&pt23456;')" />
		</xsl:when>
		<xsl:when test="'}'=(following-sibling::*[$position],..[self::m:mrow]/following-sibling::*[$position])">
			<xsl:sequence select="('&pt456;&pt356;','&pt46;&pt356;')" />
		</xsl:when>
		<xsl:when test="'|'=(following-sibling::*[$position],..[self::m:mrow]/following-sibling::*[$position])">
			<xsl:sequence select="('&pt456;&pt123456;','&pt123456;')" />
		</xsl:when>
		<xsl:when test="'&mid;'=(following-sibling::*[$position],..[self::m:mrow]/following-sibling::*[$position])">
			<xsl:sequence select="('&pt456;&pt123456;','&pt123456;')" />
		</xsl:when>
		<xsl:otherwise>
			<xsl:sequence select="('','')" />
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

</xsl:stylesheet>
