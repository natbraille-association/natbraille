<?xml version='1.1' encoding="UTF-8" ?>                                     
<!--                                                                        
 * NAT - An universal Translator                                            
 * Copyright (C) 2005 Bruno Mascret                                         
 * Contact: bmascret@free.fr                                                
 *                                                                          
 * This program is free software; you can redistribute it and/or            
 * modify it under the terms of the GNU General Public License              
 * as published by the Free Software Foundation; either version 2           
 * of the License.                                                          
 *                                                                          
 * This program is distributed in the hope that it will be useful,          
 * but WITHOUT ANY WARRANTY; without even the implied warranty of           
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            
 * GNU General Public License for more details.                             
 *                                                                          
 * You should have received a copy of the GNU General Public License        
 * along with this program; if not, write to the Free Software              
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
-->
<!DOCTYPE xsl:stylesheet SYSTEM "nat://system/xsl/mmlents/windob.dtd"
[
   <!ENTITY % table_tbfr PUBLIC "table embosseuse" "nat://system/xsl/tablesUsed/TbFr2007.ent">
	%table_tbfr;
]>
                                                                               
<xsl:stylesheet version="3.0"                                                     
xmlns:xsl='http://www.w3.org/1999/XSL/Transform'                                  
xmlns:nat='http://natbraille.free.fr/xsl'                                         
xmlns:fn='http://www.w3.org/2005/xpath-functions'                                 
xmlns:xs='http://www.w3.org/2001/XMLSchema'
xmlns:m='http://www.w3.org/1998/Math/MathML'
xmlns:functx='http://www.functx.com'
expand-text="yes">                                      
<!-- version: 1.0.1 -->                                                           
<!-- <xsl:output method="text" encoding="UTF-8" indent="no"/> -->
<xsl:import href="nat://system/xsl/functions/functx.xsl" /> <!-- functx functions -->

<!-- TODO NBA organize functions in librairies as package (xsl 3 new feature) -->
<xsl:variable name="ptBraille" as="xs:string">
	<xsl:text>&pt;&pt1;&pt12;&pt123;&pt1234;&pt12345;&pt123456;&pt12346;&pt1235;&pt12356;&pt1236;&pt124;&pt1245;&pt12456;&pt1246;&pt125;&pt1256;&pt126;&pt13;&pt134;&pt1345;&pt13456;&pt1346;&pt135;&pt1356;&pt136;&pt14;&pt145;&pt1456;&pt146;&pt15;&pt156;&pt16;&pt2;&pt23;&pt234;&pt2345;&pt23456;&pt2346;&pt235;&pt2356;&pt236;&pt24;&pt245;&pt2456;&pt246;&pt25;&pt256;&pt26;&pt3;&pt34;&pt345;&pt3456;&pt346;&pt35;&pt356;&pt36;&pt4;&pt45;&pt456;&pt46;&pt5;&pt56;&pt6;</xsl:text>
</xsl:variable>
<xsl:variable name="ptEmbos" as="xs:string">
	<xsl:text>&pte;&pte1;&pte12;&pte123;&pte1234;&pte12345;&pte123456;&pte12346;&pte1235;&pte12356;&pte1236;&pte124;&pte1245;&pte12456;&pte1246;&pte125;&pte1256;&pte126;&pte13;&pte134;&pte1345;&pte13456;&pte1346;&pte135;&pte1356;&pte136;&pte14;&pte145;&pte1456;&pte146;&pte15;&pte156;&pte16;&pte2;&pte23;&pte234;&pte2345;&pte23456;&pte2346;&pte235;&pte2356;&pte236;&pte24;&pte245;&pte2456;&pte246;&pte25;&pte256;&pte26;&pte3;&pte34;&pte345;&pte3456;&pte346;&pte35;&pte356;&pte36;&pte4;&pte45;&pte456;&pte46;&pte5;&pte56;&pte6;</xsl:text>
</xsl:variable>

<!-- returns true if the arg element is part of a variation table (maths)-->
<xsl:function name="nat:is-variation-table" as="xs:boolean">
	<xsl:param name="arg" as="element()?"/>
	<xsl:choose>
		<!-- Bruno: il faut aussi que preceding::*[1] existe -->
		<xsl:when test="translate(string($arg/preceding::*[1]),'([{|','')='' and $arg/preceding::*[1]">
			<xsl:value-of select="false()"/>
		</xsl:when>
		<xsl:when test="$arg/ancestor::*[self::m:mfenced]">
			<xsl:value-of select="false()"/>
		</xsl:when>
		<xsl:when test="empty($arg/m:mtr[count(m:mtd) > 1])">
			<xsl:value-of select="false()"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="true()"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:function>
<!-- returns the DIRECT text value of a node, not the text value of children -->
<xsl:function name="nat:direct-text" as="xs:string?">
	<xsl:param name="arg" as="element()"/>
	<xsl:variable name="sequence_text" as="xs:string*">
		<xsl:for-each select="$arg/text()">
			<xsl:value-of select="."/>
		</xsl:for-each>
	</xsl:variable>
	<xsl:value-of select="string-join($sequence_text,'')" />
</xsl:function>

<!-- inserts the char $char after each char of $arg if not a '^' or a '$'-->                                      
<xsl:function name="nat:insert-char" as="xs:string">                   
        <xsl:param name="arg" as="xs:string?"/>                                                                   
        <xsl:param name="char" as="xs:string?"/>
        <xsl:value-of select="string-join(
                for $ch in string-to-codepoints($arg)
                return if (not(codepoints-to-string($ch)='^' or codepoints-to-string($ch)='$'))
                then concat(codepoints-to-string($ch),$char)
                else codepoints-to-string($ch),'')
        "/>
</xsl:function>
<!-- insert the char $char after each chars of $arg except for digits and supresses '$' and '^' -->
<xsl:function name="nat:insert-char-if-not-digit" as="xs:string">
        <xsl:param name="arg" as="xs:string?"/>
        <xsl:param name="char" as="xs:string?"/>

        <xsl:variable name="tab" select="string-to-codepoints($arg)" as="xs:integer*"/>
        <xsl:variable name="retour" as="xs:string*">
                <xsl:for-each select="$tab">
                        <xsl:variable name="i" select="position()" as="xs:integer"/>
                        <!--<xsl:message select="concat('position:',position(),' actu:',.,' next:',$tab[$i + 1])"/>-->
                        <xsl:value-of select="
                                if (not(string-to-codepoints('^')=. or string-to-codepoints('$')=.))
                                then
                                if (. &lt; string-to-codepoints('9') and . &gt; string-to-codepoints('0')  or
                                        ($tab[$i + 1] &lt; string-to-codepoints('9') and $tab[$i + 1] &gt; string-to-codepoints('0')))
                                then codepoints-to-string(.)
                                else concat(codepoints-to-string(.),$char)
                                else ''
                        "/>
                </xsl:for-each>
        </xsl:variable>
        <!-- retour sous forme de string -->
        <!--<xsl:message select="string-join($retour,'')"/>-->
        <xsl:value-of select="string-join($retour,'')"/>
</xsl:function>
<!-- 
*** Fonction nat:starts-with-any-of ***
* retour: renvoie vrai si $arg commence par une des chaînes de startStrings
* return: true if $arg starts with any of startStrings 
*
* d'après functx:contains-any-of de Priscilla Walmsley-->
<xsl:function name="nat:starts-with-any-of" as="xs:boolean">
  <xsl:param name="arg" as="xs:string?"/> 
  <xsl:param name="startStrings" as="xs:string*"/> 
 
  <xsl:sequence select=" 
   some $startString in $startStrings
   satisfies starts-with($arg,$startString)
 "/>
   
</xsl:function>

<!-- 
*** Fonction nat:ends-with-any-of ***
* retour: renvoie vrai si $arg se termine par une des chaînes de endStrings
* return: true if $arg ends with any of endStrings 
*
* d'après functx:contains-any-of de Priscilla Walmsley-->
<xsl:function name="nat:ends-with-any-of" as="xs:boolean">
  <xsl:param name="arg" as="xs:string?"/> 
  <xsl:param name="endStrings" as="xs:string*"/> 
 
  <xsl:sequence select=" 
   some $endString in $endStrings
   satisfies ends-with($arg,$endString)
 "/>
   
</xsl:function>

<!--
  lowerizes the first character of a string 

 d'après functx:capitalize-first de Priscilla Walmsley
 @param   $arg the word or phrase to capitalize 
--> 
<xsl:function name="nat:lowerize-first" as="xs:string?">
  <xsl:param name="arg" as="xs:string?"/> 
 
  <xsl:sequence select="     concat(lower-case(substring($arg,1,1)),              substring($arg,2))  "/>
   
</xsl:function>

<!-- 
  Escapes only 2 regex special characters : $ and \
@param   $arg the string to escape 
* d'après functx:contains-any-of de Priscilla Walmsley-->
<xsl:function name="nat:escape-for-regex2" as="xs:string">
  <xsl:param name="arg" as="xs:string?"/> 
 
  <xsl:sequence select="
  replace($arg,'(\\|\$)','\\$1')  "/>
</xsl:function>

<!--
    transcrit une chaine en braille utf8 vers le braille tbfr2007
    utile pour la lecture à l'écran des fichiers temporaires -->
<xsl:function name="nat:toTbfr" as="xs:string">
  <xsl:param name="arg" as="xs:string?"/>
  
  <!--<xsl:value-of select="translate($arg,concat(string($ptBraille),$carcoupure,$espace),concat(string($ptEmbos),'ECMNTU '))"/>-->
	<xsl:value-of select="translate($arg,concat($ptBraille,'&#x2d34;'),concat($ptEmbos,' '))"/>
</xsl:function>

<!-- transcription g0 vers BrailleUTF8 -->
<xsl:function name="nat:toBrUTF8" as="xs:string">
  <xsl:param name="arg" as="xs:string?"/>
  
  <xsl:variable name="in" as="xs:string" select="'áíóúñìòäöabcdefghijklmnopqrstuvwxyz0123456789àâéèêëîïôùûüçæœ@+-×÷=.,&nbsp;/&#8209;’‘:'''"/>
  <xsl:variable name="out" as="xs:string">
	<xsl:text>&pt12356;&pt34;&pt346;&pt23456;&pt12456;&pt34;&pt346;&pt345;&pt246;&pt1;&pt12;&pt14;&pt145;&pt15;&pt124;&pt1245;&pt125;&pt24;&pt245;&pt13;&pt123;&pt134;&pt1345;&pt135;&pt1234;&pt12345;&pt1235;&pt234;&pt2345;&pt136;&pt1236;&pt2456;&pt1346;&pt13456;&pt1356;&pt3456;&pt16;&pt126;&pt146;&pt1456;&pt156;&pt1246;&pt12456;&pt1256;&pt246;&pt12356;&pt16;&pt123456;&pt2346;&pt126;&pt1246;&pt146;&pt12456;&pt1456;&pt23456;&pt156;&pt1256;&pt12346;&pt345;&pt246;&pt345;&pt235;&pt36;&pt35;&pt25;&pt2356;&pt256;&pt2;&pt;&pt34;&pt36;&pt3;&pt3;&pt25;&pt3;</xsl:text>
  </xsl:variable>
  <!--<xsl:value-of select="translate($arg,concat(string($ptBraille),$carcoupure,$espace),concat(string($ptEmbos),'ECMNTU '))"/>-->
	<xsl:value-of select="translate($arg,$in,$out)"/>
</xsl:function>

<!-- detranscription g0 de BrailleUTF8 vers noir-->
<xsl:function name="nat:toBlack" as="xs:string">
  <xsl:param name="arg" as="xs:string?"/>
  
  <xsl:variable name="out" as="xs:string" select="'áíóúñìòäöabcdefghijklmnopqrstuvwxyz0123456789àâéèêëîïôùûüçæœ@+-×÷=.,&nbsp;/&#8209;’‘:'''"/>
  <xsl:variable name="in" as="xs:string">
	<xsl:text>&pt12356;&pt34;&pt346;&pt23456;&pt12456;&pt34;&pt346;&pt345;&pt246;&pt1;&pt12;&pt14;&pt145;&pt15;&pt124;&pt1245;&pt125;&pt24;&pt245;&pt13;&pt123;&pt134;&pt1345;&pt135;&pt1234;&pt12345;&pt1235;&pt234;&pt2345;&pt136;&pt1236;&pt2456;&pt1346;&pt13456;&pt1356;&pt3456;&pt16;&pt126;&pt146;&pt1456;&pt156;&pt1246;&pt12456;&pt1256;&pt246;&pt12356;&pt16;&pt123456;&pt2346;&pt126;&pt1246;&pt146;&pt12456;&pt1456;&pt23456;&pt156;&pt1256;&pt12346;&pt345;&pt246;&pt345;&pt235;&pt36;&pt35;&pt25;&pt2356;&pt256;&pt2;&pt;&pt34;&pt36;&pt3;&pt3;&pt25;&pt3;</xsl:text>
  </xsl:variable>
  <!--<xsl:value-of select="translate($arg,concat(string($ptBraille),$carcoupure,$espace),concat(string($ptEmbos),'ECMNTU '))"/>-->
	<xsl:value-of select="translate($arg,$in,$out)"/>
</xsl:function>

<!--
    renvoie la taille des colonnes du tableau tab composé de nCol passé en paramètre -->
<xsl:function name="nat:getColSize" as="xs:integer*">
	<xsl:param name="tab" as="xs:string*"/>
	<xsl:param name="nCol" as="xs:integer"/>
	
	<xsl:sequence select="for $i in 1 to $nCol
  return max(for $c in 1 to count($tab) return 
		if($c mod($nCol) = $i mod($nCol)) then string-length($tab[$c]) else() )"/>
</xsl:function>

<!-- fonctions pour les styles -->
<!-- transforme une chaine de cars de style p6p23 en &pt6;&pt23; renvoie vide sinon -->
<xsl:function name="nat:pToPt" as="xs:string?">
	<xsl:param name="c" as="xs:string?" />
	
	<!-- position > 1 car tokenize renvoie une chaine vide en premier vu que ça commence par p -->
	<xsl:variable name="pbraille" select="tokenize($c,'p')[position()>1]" />
	<!-- position > 1 car le 1er c'est l'espace et il correspond à une chaine vide pour $pbraille -->
	<xsl:variable name="charBraille" select="functx:chars($ptBraille)[position()>1]" />
	<xsl:variable name="resu" as="xs:string*">
		<xsl:if test="matches($c,'^(p(1?2?3?4?5?6?))+$')">
			<xsl:for-each select="$pbraille">
				<xsl:value-of select="if (string-length(.)=0)then '&pt;' else functx:replace-multi(.,
					('^1$','^12$','^123$','^1234$','^12345$','^123456$','^12346$','^1235$','^12356$','^1236$','^124$',
					'^1245$','^12456$','^1246$','^125$','^1256$','^126$','^13$','^134$','^1345$','^13456$','^1346$',
					'^135$','^1356$','^136$','^14$','^145$','^1456$','^146$','^15$','^156$','^16$','^2$','^23$','^234$',
					'^2345$','^23456$','^2346$','^235$','^2356$','^236$','^24$','^245$','^2456$','^246$','^25$','^256$',
					'^26$','^3$','^34$','^345$','^3456$','^346$','^35$','^356$','^36$','^4$','^45$','^456$','^46$','^5$','^56$','^6$'),
				$charBraille)"/>
			</xsl:for-each>
		</xsl:if>
	</xsl:variable>
	<xsl:value-of select="string-join($resu,'')" />
</xsl:function>

<!-- fonctions pour l'abrégé -->
<!--
	renvoie un espace si on n'a pas réussi à appliquer de règles
	ou la transcription complète du mot si on a pu appliquer une règle et la base utilisée
-->
<xsl:function name="nat:matchRules" as="xs:string*">
	<!-- mot à tester -->
	<xsl:param name="mot" as="xs:string"/> 
	<!-- liste des règles -->
	<xsl:param name="liste" as="xs:string*"/>
	<!-- liste des remplacements -->
	<xsl:param name="listeBr" as="xs:string*"/>
	<!-- liste des signes/symboles sur qui on peu appliquer la règle -->
	<xsl:param name="listePos" as="xs:string*"/>
	<!-- transcription de la liste des possibles -->
	<xsl:param name="listePosBr" as="xs:string*"/>
	<!-- position dans la liste -->
	<xsl:param name="pos" as="xs:integer"/>
	<xsl:choose>
		<!-- c'est fini, on a rien trouvé -->
		<xsl:when test="$pos > count($liste)">
			<xsl:value-of select="' '"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:variable name="resu" as="xs:string*">
				<!--<xsl:message select="('règle:',$liste[$pos])"/>-->
				<!-- test de la règle -->
				<xsl:analyze-string select="$mot" regex="{$liste[$pos]}">
					<xsl:matching-substring>
				<!--<xsl:message select="(regex-group(1),';',regex-group(2),';',regex-group(3))"/>-->
						<xsl:choose>
							<!-- on vérifie que le groupe qui match fait bien partie de la liste des symboles/signes possibles -->
							<xsl:when test="functx:is-value-in-sequence(regex-group(1),$listePos)">
								<xsl:variable name="pos2" as="xs:integer?" select="fn:index-of($listePos,regex-group(1))"/>
								<!--<xsl:message select="($pos2,' ',$mot)"/>-->
								<xsl:value-of select="translate(fn:replace(fn:replace($mot,$liste[$pos],$listeBr[$pos]),'\(.*\)',$listePosBr[$pos2]),'s','&pt234;')"/>
								<xsl:value-of select="regex-group(1)"/>
							</xsl:when>
							<xsl:otherwise/>
						</xsl:choose>
					</xsl:matching-substring>
				</xsl:analyze-string>
			</xsl:variable>
			<!--<xsl:message select="('******resu',$resu)"/>-->
			<!--<xsl:message select="concat('resu:',$resu)"/>-->
			<xsl:sequence select="if(string-length($resu[1])=0) then nat:matchRules($mot,$liste,$listeBr,$listePos,$listePosBr,$pos+1) else $resu"/>
			<!--<xsl:value-of select="fn:index-of($liste,$mot)"/>-->
		</xsl:otherwise>
	</xsl:choose>
</xsl:function>

<!--
  Whether a string matches any of a sequence of patterns 

 @author  Bruno Mascret, thanks to contains-any-of of Priscilla Walmsley, Datypic 
 @param   $arg the string to test 
 @param   $patterns the pattern strings to look for 
--> 
<xsl:function name="nat:matches-any-of" as="xs:boolean" xmlns:functx="http://natbraille.free.fr/xsl">
  <xsl:param name="arg" as="xs:string?"/> 
  <xsl:param name="patterns" as="xs:string*"/> 
 
  <xsl:sequence select="     some $pat in $patterns   satisfies matches($arg,$pat)  "/>
   
</xsl:function>

<!--
	Whether a word match a rule for symbols for contracted braille
	@author  Bruno Mascret
	@param   $mot the word to test
	@param   $liste the rule list
	@param   $listeBr the replacement list
	@param   $pos the current position tested (recursive function)

-->
<xsl:function name="nat:matchRulesP2" as="xs:string?" 
              xmlns:nat="http://natbraille.free.fr/xsl" >
	<!-- mot à tester -->
	<xsl:param name="mot" as="xs:string"/> 
	<!-- liste des règles -->
	<xsl:param name="liste" as="xs:string*"/>
	<!-- liste des remplacements -->
	<xsl:param name="listeBr" as="xs:string*"/>
	<!-- liste des mots racines -->
	<xsl:param name="words" as="xs:string*"/>
	<!-- position dans la liste -->
	<xsl:param name="pos" as="xs:integer"/>
	<xsl:choose>
		<!-- c'est fini, on a rien trouvé -->
		<xsl:when test="$pos > count($liste)">
			<xsl:value-of select="' '"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:variable name="resu" as="xs:string?">
				<!--<xsl:message select="('règle:',$liste[$pos])"/>-->
				<!-- test de la règle -->
				<xsl:analyze-string select="$mot" regex="{$liste[$pos]}">
					<xsl:matching-substring>
						<!--<xsl:message select="(regex-group(1),';',regex-group(2),';',regex-group(3))"/>-->
						<xsl:if test="index-of($words,regex-group(1)) > 0">
							<xsl:value-of select="fn:replace($mot,$liste[$pos],$listeBr[$pos])"/>
						</xsl:if>
					</xsl:matching-substring>
				</xsl:analyze-string>
			</xsl:variable>
			<!--<xsl:message select="concat('resu:',$resu)"/>-->
			<xsl:value-of select="if(string-length($resu)=0) then nat:matchRulesP2($mot,$liste,$listeBr,$words,$pos+1) else $resu"/>
			<!--<xsl:value-of select="fn:index-of($liste,$mot)"/>-->
		</xsl:otherwise>
	</xsl:choose>
</xsl:function>

<!-- 
	replace the first occurence of every patterns once
	@author Bruno Mascret , thanks to functx:replace-multi of Priscilla Walmsley, Datypic 
	@param   $arg the input string
	@param   $changeFrom the input list
	@param   $changeTo the output list
	@param   $debs to be checked: memorize the initial positions???

-->
<xsl:function name="nat:replace-first-multi" as="xs:string?" 
              xmlns:nat="http://natbraille.free.fr/xsl" >
	<xsl:param name="arg" as="xs:string?"/> 
	<xsl:param name="changeFrom" as="xs:string*"/> 
	<xsl:param name="changeTo" as="xs:string*"/>
	<xsl:param name="debs" as="xs:integer*"/>
	
	<xsl:variable name="pref" as="xs:string" select="substring($arg,1,max((0,$debs[1])))"/>
	<!--<xsl:message select="($debs[1],':', max((0,$debs[1])))"/>-->
	<xsl:sequence select=" 
		if (count($changeFrom) > 0)
		then nat:replace-first-multi(
					concat($pref,functx:replace-first(substring-after($arg,$pref), $changeFrom[1],
											functx:if-absent($changeTo[1],''))),
					$changeFrom[position() > 1],
					$changeTo[position() > 1],
					for $i in $debs[position() > 1] return $i - (string-length($changeFrom[1]) - string-length($changeTo[1])))
		else $arg
 "/>
   
</xsl:function>

<!-- 

	Whether a word match a general rule for contracted braille
	@author  Bruno Mascret
	@param   $mot the word to test
	@param   $liste the rule list
	@param   $listeBr the replacement list
	@param   $pos the current position tested (recursive function)

-->
<xsl:function name="nat:matchGeneralRules" as="element()*" 
              xmlns:nat="http://natbraille.free.fr/xsl" >
	<!-- mot à tester -->
	<xsl:param name="mot" as="xs:string"/> 
	<!-- liste des règles -->
	<xsl:param name="liste" as="xs:string*"/>
	<!-- liste des remplacements -->
	<xsl:param name="listeBr" as="xs:string*"/>
	<!-- position dans la liste -->
	<xsl:param name="pos" as="xs:integer"/>
	<xsl:choose>
		<!-- c'est fini, on a rien trouvé -->
		<xsl:when test="$pos > count($liste)"/>
			<!--<xsl:element name="syll" inherit-namespaces="no">
				<xsl:element name="noir" inherit-namespaces="no"><xsl:value-of select="$mot"/></xsl:element>
				<xsl:element name="abr" inherit-namespaces="no"><xsl:value-of select="$mot"/></xsl:element>
			</xsl:element>
		</xsl:when>-->
		<xsl:otherwise>
			<xsl:variable name="resu">
				<!-- test de la règle -->
				<xsl:element name="sequences" inherit-namespaces="no" >
					<xsl:analyze-string select="$mot" regex="{$liste[$pos]}">
						<xsl:matching-substring>
							<xsl:element name="abrege" inherit-namespaces="no">
								<!--<xsl:message select="('match-subst:',.)"/>-->
								<!--<xsl:message select="('match:',regex-group(1),';',regex-group(2),';',regex-group(3))"/>-->
								<!--<xsl:sequence select="(regex-group(1),$listeBr[$pos],regex-group(3),regex-group(2))"/>-->
								<xsl:element name="noir" inherit-namespaces="no"><xsl:value-of select="regex-group(2)"/></xsl:element>
								<xsl:element name="abr" inherit-namespaces="no"><xsl:value-of select="$listeBr[$pos]"/></xsl:element>
								<xsl:element name="pref" inherit-namespaces="no"><xsl:value-of select="regex-group(1)"/></xsl:element>
								<xsl:element name="suff" inherit-namespaces="no"><xsl:value-of select="regex-group(3)"/></xsl:element>
							</xsl:element>
						</xsl:matching-substring>
						<xsl:non-matching-substring>
							<xsl:element name="nabr" inherit-namespaces="no">
								<xsl:sequence select="."/>
							</xsl:element>
						</xsl:non-matching-substring>
					</xsl:analyze-string>
				</xsl:element>
			</xsl:variable>
			<!--<xsl:if test="$resu/sequences/abrege"><xsl:message select="$resu/sequences"/></xsl:if>-->
			<!--<xsl:message select="($resu, ' 1:',$resu[1],' 2:',$resu[2],' 3:',$resu[3])"/>-->
			<!--<xsl:message select="(' - nb matches pour la règle: ', count($resu))"/>-->
			<xsl:for-each select="$resu/sequences/abrege">
				<xsl:if test="./abr">
					<xsl:element name="syll" inherit-namespaces="no">
						<xsl:element name="deb" inherit-namespaces="no">
							<xsl:value-of select="string-length(string-join((./pref,./preceding-sibling::nabr,./preceding-sibling::abrege/noir,./preceding-sibling::abrege/suff,./preceding-sibling::abrege/pref),''))"/>
							<!--<xsl:message select="(./pref,./preceding-sibling::nabr,./preceding-sibling::abrege/noir,'lg=',string-length(string-join((./pref,./preceding-sibling::nabr,./preceding-sibling::abrege/noir,./preceding-sibling::abrege/suff,./preceding-sibling::abrege/pref),'')))"/>-->
						</xsl:element>
						<xsl:copy-of select="./noir"/>
						<xsl:copy-of select="./abr"/>
					</xsl:element>
				</xsl:if>
				<!-- Avant:
				<xsl:variable name="i" select="4*(. - 1)" as="xs:integer"/>
				<!-<xsl:message select="(count($resu),':',$i,':',$resu[$i+2])"/>->
				<xsl:if test="string-length($resu[$i+2])>0">
					<xsl:element name="syll" inherit-namespaces="no">
						<xsl:element name="deb" inherit-namespaces="no">
							<!-<xsl:message select="('positionDeb:', string-join(tokenize($mot, $resu[$i+4]),''),'mot:', string-join(tokenize($mot, $resu[$i+4])[position() &lt;= $i div 4  + 1],''))"/>->
							<xsl:value-of select="if (string-length($resu[$i+1]) > 0) then
								string-length($resu[$i+1]) else ($i div 4 * string-length($resu[$i+4]) + string-length(string-join(tokenize($mot, $resu[$i+4])[position() &lt;= $i div 4  + 1],'')))"/>
						</xsl:element>
						<xsl:element name="noir" inherit-namespaces="no"><xsl:value-of select="$resu[$i+4]"/></xsl:element>
						<xsl:element name="abr" inherit-namespaces="no"><xsl:value-of select="$resu[$i+2]"/></xsl:element>
					</xsl:element>
				</xsl:if>-->
			</xsl:for-each>
			<xsl:sequence select="nat:matchGeneralRules($mot,$liste,$listeBr,$pos+1)"/>
			<!--<xsl:sequence select="if(string-length($resu[2])=0) 
					then nat:matchGeneralRules($mot,$liste,$listeBr,$pos+1) else ($resu)"/>-->
			<!--<xsl:value-of select="fn:index-of($liste,$mot)"/>-->
		</xsl:otherwise>
	</xsl:choose>
</xsl:function>

<!-- 
	return true if the contracted word doesn't begin with the preceding word, if there is no space between them
	@author Bruno Mascret
	@param: $word the word to test, in braille
	@param: $prec the preceding word without space
 -->
<xsl:function name="nat:isContractableDeb" as="xs:boolean" 
              xmlns:nat="http://natbraille.free.fr/xsl" >
	<xsl:param name="word" as="xs:string"/>
	<xsl:param name="prec" as="xs:string"/>
	<xsl:choose>
		<xsl:when test="string-length($prec)=0"><xsl:value-of select="true()"/></xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="not(starts-with($word, substring($prec,string-length($prec),1)))"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:function>

<!-- 
	return a string containing node value replaced by @ori attribute if present
	@author Bruno Mascret
	@param: $noeud the current node
 -->
<xsl:function name="nat:getOriginalValue" as="xs:string?" 
              xmlns:nat="http://natbraille.free.fr/xsl" >
	<xsl:param name="noeud" as="element()"/>
	<xsl:choose>
		<xsl:when test="$noeud/@ori"><xsl:value-of select="$noeud/@ori"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="$noeud"/></xsl:otherwise>
	</xsl:choose>
</xsl:function>

<!-- 
	return a string containing the n preceding sibling tags;
	the preceding sibling value is replaced by @ori if present
	@author Bruno Mascret
	@param: $noeud the current node
	@param: $n the number of preceding-sibling nodes to add
 -->
<xsl:function name="nat:getPrecedingValues" as="xs:string?" 
              xmlns:nat="http://natbraille.free.fr/xsl" >
	<xsl:param name="noeud" as="element()"/>
	<xsl:param name="n" as="xs:integer"/>
	<xsl:variable name="prec" select="$noeud/preceding-sibling::*[position() &lt; $n]"/>

	<xsl:variable name="retour">
		<xsl:for-each select="$prec">
			<xsl:choose>
				<xsl:when test="./@ori"><xsl:value-of select="./@ori"/></xsl:when>
				<xsl:otherwise><xsl:value-of select="."/></xsl:otherwise>
			</xsl:choose>
			<xsl:text> </xsl:text>
		</xsl:for-each>
	</xsl:variable>
	<xsl:value-of select="string-join($retour,'')"/>
</xsl:function>

<!-- 
	return a string containing the n following sibling tags;
	the following sibling value is replaced by @ori if present
	@author Bruno Mascret
	@param $noeud the current node
	@param $n the number of following-sibling nodes to add
 -->
<xsl:function name="nat:getFollowingValues" as="xs:string?" 
              xmlns:nat="http://natbraille.free.fr/xsl" >
	<xsl:param name="noeud" as="element()"/>
	<xsl:param name="n" as="xs:integer"/>
	<xsl:variable name="prec" select="$noeud/following-sibling::*[position() &lt; $n]"/>

	<xsl:variable name="retour">
		<xsl:for-each select="$prec">
			<xsl:choose>
				<xsl:when test="./@ori"><xsl:value-of select="./@ori"/></xsl:when>
				<xsl:otherwise><xsl:value-of select="."/></xsl:otherwise>
			</xsl:choose>
			<xsl:text> </xsl:text>
		</xsl:for-each>
	</xsl:variable>
	<xsl:value-of select="string-join($retour,'')"/>
</xsl:function>
 
<!--
  return a centered string filled left and right with $motif char
  on a $nb char length line.
	@author Bruno Mascret
	@param $expr the string to be centered
	@param $nb the number of following-sibling nodes to add
	@param $motif the char to fill with
-->
<xsl:function name="nat:fill_center" as="xs:string?"
              xmlns:nat="http://natbraille.free.fr/xsl" >		
	<xsl:param name="expr" as="xs:string" />
	<xsl:param name="nb" as="xs:integer"/>
	<xsl:param name="motif" as="xs:string"/>
	<xsl:value-of select="(if ($nb>0) then functx:repeat-string($motif,xs:integer(floor(($nb div 2)))) else '' , $expr , 	if ($nb>0) then functx:repeat-string($motif,xs:integer($nb -floor(($nb div 2)))) else '') => string-join()"/>	
</xsl:function>

<!--
TODO NBA 
 return a sequence of words tagged with prefixe for mev 
	@author Bruno Mascret
	@since NBA
	@todo
-->
<xsl:function name="nat:doMevPrefixe" as="element()*">
	<xsl:param name="expr" as="element()*"/>
	<xsl:copy-of select="$expr"/>
</xsl:function>
 
 
 <!--
TODO NBA 
 return a sequence of words tagged with prefixe for mev 
	@author Bruno Mascret
	@since NBA
	@todo
-->
<xsl:function name="nat:tagContent" as="element()*">
	<xsl:param name="expr" as="element()*"/>
	<xsl:param name="regex" as="xs:string"/>
	<xsl:param name="map" as="map(*)"/>
	<xsl:param name="funct" as="function(*)" />
	<xsl:variable name="a" select="fn:analyze-string(string-join($expr,''),$regex)" as="element()*"/>
	<xsl:choose>
		<!-- no target content -->
		<xsl:when test="count($a/fn:match)=0">	<xsl:copy-of select="$expr"/></xsl:when>
		<!-- full content-->
		<xsl:when test="count($a/fn:match)=1 and count($a/fn:non-match)=0">
			<prefixe type="{$map?name}"><xsl:value-of select="if(string-length($expr)>1) then $map?word else $map?pref"/></prefixe>
			<mot><xsl:value-of select="$a/fn:match => $funct()"/></mot>
		</xsl:when>
		<!-- mixed content -->
		<!-- only the first char -->
		<xsl:when test="count($a/fn:match/following-sibling::*)=1 and string-length($a/fn:match)=1">
			<prefixe type="{$map?name}">{$map?pref}</prefixe>
			<mot><xsl:value-of select="$expr => $funct()"/></mot>
		</xsl:when>
		<!-- other mixed content cases -->
		<xsl:otherwise>
			<xsl:for-each select="$a/*">
				<xsl:choose>
					<xsl:when test="self::fn:non-match">
						<syll><xsl:value-of select="$funct(.)"/></syll>
					</xsl:when>
					<xsl:when test="position()=last() and string-length(.)>1">
						<prefixe type="{$map?name}">{$map?word}</prefixe>
						<syll><xsl:value-of select="$funct(.)"/></syll>
					</xsl:when>
					<xsl:otherwise>
						<xsl:variable name="chars" as="xs:string*" select="functx:chars(.)"/>
						<xsl:for-each select="$chars">
							<prefixe type="{$map?name}">{$map?pref}</prefixe>
							<syll><xsl:value-of select="$funct(.)"/></syll>
						</xsl:for-each>
					</xsl:otherwise>
				</xsl:choose>	
			</xsl:for-each>
		</xsl:otherwise>
	</xsl:choose>
</xsl:function>

<xsl:function name="nat:ivb" as="element()*">
	<xsl:param name="expr" as="element()*"/>
	<xsl:copy-of select="$expr"/>
</xsl:function>
</xsl:stylesheet>


