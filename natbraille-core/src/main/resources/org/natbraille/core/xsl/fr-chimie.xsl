<?xml version='1.1' encoding="UTF-8" ?>
<!--
* NAT - An universal Translator
* Copyright (C) 2005 Bruno Mascret
* Contact: bmascret@free.fr

* former authors: Frédéric Schwebel, Bruno Mascret, Ouarda Yassa
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
-->

<!-- version 2.4 -->

<!DOCTYPE xsl:stylesheet SYSTEM "nat://system/xsl/mmlents/windob.dtd">

<xsl:stylesheet version="2.0"
xmlns:xsl='http://www.w3.org/1999/XSL/Transform' 
xmlns:saxon='http://icl.com/saxon'
xmlns:m='http://www.w3.org/1998/Math/MathML'
xmlns:xs='http://www.w3.org/2001/XMLSchema'
xmlns:fn='http://www.w3.org/2005/xpath-functions'
xmlns:functx='http://www.functx.com'
xmlns:lit='espacelit'
xmlns:doc='espaceDoc'
xmlns:nat='http://natbraille.free.fr/xsl'>

<xsl:output method="xml" encoding="UTF-8" indent="yes"/>

<xsl:variable name="blackChars" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZÀÂÉÈÊËÎÏÔÙÛÜ&Ccedil;&AElig;&OElig;abcdefghijklmnopqrstuvwxyz0123456789àâéèêëîïôùûüçæœ@+-×÷=&nbsp;&int;|&verbar;,;:.?!&quot;«»“”‘’&lsquo;&rsquo;&acute;&prime;()−&equiv;\∖/''∥'" />
<xsl:variable name="brailleChars" as="xs:string">	<xsl:text>&pt1;&pt12;&pt14;&pt145;&pt15;&pt124;&pt1245;&pt125;&pt24;&pt245;&pt13;&pt123;&pt134;&pt1345;&pt135;&pt1234;&pt12345;&pt1235;&pt234;&pt2345;&pt136;&pt1236;&pt2456;&pt1346;&pt13456;&pt1356;&pt12356;&pt16;&pt123456;&pt2346;&pt126;&pt1246;&pt146;&pt12456;&pt1456;&pt23456;&pt156;&pt1256;&pt12346;&pt345;&pt246;&pt1;&pt12;&pt14;&pt145;&pt15;&pt124;&pt1245;&pt125;&pt24;&pt245;&pt13;&pt123;&pt134;&pt1345;&pt135;&pt1234;&pt12345;&pt1235;&pt234;&pt2345;&pt136;&pt1236;&pt2456;&pt1346;&pt13456;&pt1356;&pt3456;&pt16;&pt126;&pt146;&pt1456;&pt156;&pt1246;&pt12456;&pt1256;&pt246;&pt12356;&pt16;&pt123456;&pt2346;&pt126;&pt1246;&pt146;&pt12456;&pt1456;&pt23456;&pt156;&pt1256;&pt12346;&pt345;&pt246;&pt345;&pt235;&pt36;&pt35;&pt25;&pt2356;&pt;&pt12346;&pt36;&pt36;&pt2;&pt23;&pt25;&pt256;&pt26;&pt235;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt3;&pt3;&pt3;&pt3;&pt236;&pt356;&pt36;&pt123456;&pt36;&pt36;&pt34;&pt3;&pt2356;</xsl:text>
</xsl:variable>

<!--<xsl:template match="(chimie|math[@chemistry='true'])[*[1][not(self::semantics)]]|(chimie|math[@chemistry='true'])/semantics">-->
<xsl:template match="(chimie|m:math[@chemistry='true'])[*[1][not(self::semantics)]]|(chimie|m:math[@chemistry='true'])/semantics">
	<xsl:message>
		Allé la chimit.
	</xsl:message>
	<xsl:variable name="chimie-preprocess">
		<xsl:apply-templates mode="phase1"/>
	</xsl:variable>
	<!--<xsl:message>
		<xsl:copy-of select="$chimie-preprocess" copy-namespaces="no" />
	</xsl:message>-->
	<xsl:value-of select="$debMath"/>
	<xsl:value-of select="'&pt6;&pt3;'"/>
	<xsl:apply-templates mode="chimie" select="$chimie-preprocess"/>
	<xsl:value-of select="$finMath"/>
	<xsl:if test="following-sibling::*[1] or (self::m:semantics and parent::*[1]/following-sibling::*[1])">
		<!-- il y a des trucs après sur la même ligne -->
		<xsl:call-template name="espace"/>
	</xsl:if>
</xsl:template>

<xsl:template match="m:annotation" mode="chimie"/>

<xsl:template match="m:mmultiscripts|m:mrow|m:mtd" mode="chimie"><!-- molécule avec infos gauche -->
	<xsl:apply-templates mode="chimie"/>
</xsl:template>

<xsl:template match="m:mi" mode="chimie">
	<xsl:choose>
		<xsl:when test=".='|' or .='&mid;'"/>
		<!-- c'est une représentation de Lewis qui a déjà été réalisée -->
		<!-- harpons ou flèches superposés: réaction réversible -->
		<!-- flèche droite: donne -->
		<xsl:when test=".='&rarr;'">
			<xsl:value-of select="$coupeEsth"/>
			<xsl:text>&pt456;&pt156;</xsl:text>
		</xsl:when>
		<!-- harpons ou flèches superposés: réaction réversible -->
		<xsl:when test=".='&rlarr;' or .='&rlhar;'">
			<xsl:value-of select="$coupeEsth"/>
			<xsl:text>&pt456;&pt12456;</xsl:text>
		</xsl:when>
		<!-- mésomérie, flèche double pointes -->
		<xsl:when test=".='&harr;'">
			<xsl:value-of select="$coupeEsth"/>
			<xsl:text>&pt5;&pt12456;</xsl:text>
		</xsl:when>
		<xsl:when test=".=';'"><!-- il faut préfixer avec p6 -->
			<xsl:value-of select="concat('&pt6;',nat:trChimie(.))"/>
		</xsl:when>
		<xsl:when test=".='+' and not(local-name(..)='msup' or (local-name(..)='mrow' and local-name(../..)='msup'))">
			<xsl:value-of select="$coupeEsth"/>
			<xsl:value-of select="nat:trChimie(.)"/>
		</xsl:when>
		<!-- clef &pt45; : lettres grecques minuscules + qq signes -->
		<xsl:when test="contains(concat('&infin;&emptyv;',$l_grec_min),.)">
			<xsl:text>&pt45;</xsl:text>
			<xsl:value-of select="translate(.,concat('&infin;&emptyv;',$l_grec_min),'&pt14;&pt3456;&pt1;&pt12;&pt1245;&pt145;&pt15;&pt15;&pt1356;&pt125;&pt245;&pt245;&pt24;&pt13;&pt123;&pt134;&pt1345;&pt1346;&pt135;&pt1234;&pt12456;&pt1235;&pt1235;&pt234;&pt234;&pt2345;&pt136;&pt124;&pt124;&pt12345;&pt13456;&pt2456;&pt15;')"/>
		</xsl:when>
		<!-- clef &pt46;&pt45; : lettres grecques majuscules-->
		<xsl:when test="contains($l_grec_maj,.)">
			<xsl:text>&pt46;&pt45;</xsl:text>
			<xsl:value-of select="translate(., $l_grec_maj,'&pt1;&pt12;&pt1245;&pt145;&pt15;&pt1356;&pt125;&pt245;&pt24;&pt13;&pt123;&pt134;&pt1345;&pt1346;&pt135;&pt1234;&pt1235;&pt234;&pt2345;&pt136;&pt124;&pt12345;&pt12345;&pt13456;&pt2456;')"/>
		</xsl:when>
		<xsl:when test="(local-name(..)='mover' and preceding-sibling::*[1]='&rarr;') or 
						((local-name(..),local-name(../..))=('mrow','mover') and ../preceding-sibling::*[1]='&rarr;')">
			<!-- c'est une réaction commentée -->
			<xsl:value-of select="nat:trChimie(.)"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="nat:trChimie(.)"/>
			<!-- est-ce une représentation de Lewis?-->
			<xsl:variable name="n" as="element()" select="if (@mathvariant='italic' and ../self::m:mrow and not(following-sibling::*)) then .. else ." />
			<!-- par Fred : on est obligés de faire ça car en pass1 pour les unités en maths je sépare en pass1 <mi>Cu</mi> en <mi>C</mi><mi>u</mi> et d'ailleurs ça devrait du coup
			donner une meilleure compatibilité sous Word+MathType pour la chimie car MathType met jamais plus d'une lettre dans les mi -->
			<xsl:if test="$n/following-sibling::*[1]=('|','&mid;') or $n/preceding-sibling::*[1]=('|','&mid;') or local-name($n/..)=('munder','mover')">
				<xsl:variable name="nbDoublets" as="xs:integer">
					<xsl:choose>
						<xsl:when test="local-name($n/..)=('munder','mover')">
							<xsl:value-of select="count($n/../../../following-sibling::*[1][.=('|','&mid;')]|$n/../../../preceding-sibling::*[1][.=('|','&mid;')])+
							count($n/../m:mo | $n/../../m:mo)"/>
						</xsl:when>
						<xsl:otherwise><!-- il y a un seul doublet vertical-->
							<xsl:value-of select="count($n/following-sibling::*[1][.=('|','&mid;')]|$n/preceding-sibling::*[1][.=('|','&mid;')])"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:value-of select="concat('&pt56;',nat:trChimie(xs:string($nbDoublets)),'&pt145;&pt23;')"/>
			</xsl:if>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="m:mo" mode="chimie">
	<xsl:choose>
		<!-- flèche droite: donne -->
		<xsl:when test=".='&rarr;'">
			<xsl:value-of select="$coupeEsth"/>
			<xsl:text>&pt456;&pt156;</xsl:text>
		</xsl:when>
		<xsl:when test="local-name(..)='munder' or local-name(..)='mover'"/><!-- représentation de Lewis déjà traitée -->
		<xsl:when test=".='|' or .='&mid;'"/>
		<!-- c'est une représentation de Lewis qui a déjà été réalisée -->
		<!-- harpons ou flèches superposés: réaction réversible -->
		<xsl:when test=".='&rlarr;' or .='&rlhar;'">
			<xsl:value-of select="$coupeEsth"/>
			<xsl:text>&pt456;&pt12456;</xsl:text>
		</xsl:when>
		<!-- mésomérie, flèche double pointes -->
		<xsl:when test=".='&harr;'">
			<xsl:value-of select="$coupeEsth"/>
			<xsl:text>&pt5;&pt12456;</xsl:text>
		</xsl:when>
		<xsl:when test=".=';'"><!-- il faut préfixer avec p6 -->
			<xsl:value-of select="concat('&pt6;',nat:trChimie(.))"/>
		</xsl:when>
		<xsl:when test=".='+' and not(local-name(..)='msup' or (local-name(..)='mrow' and local-name(../..)='msup'))">
			<xsl:value-of select="$coupeEsth"/>
			<xsl:value-of select="nat:trChimie(.)"/>
		</xsl:when>
		<!-- clef &pt45; : lettres grecques minuscules + qq signes -->
		<xsl:when test="contains(concat('&infin;&emptyv;',$l_grec_min),.)">
			<xsl:text>&pt45;</xsl:text>
			<xsl:value-of select="translate(.,concat('&infin;&emptyv;',$l_grec_min),'&pt14;&pt3456;&pt1;&pt12;&pt1245;&pt145;&pt15;&pt15;&pt1356;&pt125;&pt245;&pt245;&pt24;&pt13;&pt123;&pt134;&pt1345;&pt1346;&pt135;&pt1234;&pt12456;&pt1235;&pt1235;&pt234;&pt234;&pt2345;&pt136;&pt124;&pt124;&pt12345;&pt13456;&pt2456;&pt15;')"/>
		</xsl:when>
		<!-- clef &pt46;&pt45; : lettres grecques majuscules-->
		<xsl:when test="contains($l_grec_maj,.)">
			<xsl:text>&pt46;&pt45;</xsl:text>
			<xsl:value-of select="translate(., $l_grec_maj,'&pt1;&pt12;&pt1245;&pt145;&pt15;&pt1356;&pt125;&pt245;&pt24;&pt13;&pt123;&pt134;&pt1345;&pt1346;&pt135;&pt1234;&pt1235;&pt234;&pt2345;&pt136;&pt124;&pt12345;&pt12345;&pt13456;&pt2456;')"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="nat:trChimie(.)"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="m:msup" mode="chimie">
	<!-- écriture ionique -->
	<xsl:apply-templates select="child::*[1]" mode="chimie"/>
	<xsl:value-of select="'&pt4;'"/>
	<xsl:variable name="bloc" as="xs:boolean" select="count(child::*[2]/child::*) &gt; 1 or child::*[2]=following-sibling::*[1]"/>
	<xsl:if test="$bloc"><xsl:value-of select="'&pt56;'"/></xsl:if>
	<xsl:apply-templates select="child::*[2]" mode="chimie"/>
	<xsl:if test="$bloc"><xsl:value-of select="'&pt23;'"/></xsl:if>
</xsl:template>

<xsl:template match="m:msubsup" mode="chimie">
	<!-- écriture ionique -->
	<xsl:apply-templates select="child::*[1]" mode="chimie"/>
	<xsl:apply-templates select="child::*[2]" mode="chimie"/>

	<xsl:value-of select="'&pt4;'"/>
	<xsl:variable name="bloc" as="xs:boolean" select="count(child::*[3]/child::*) &gt; 1 or child::*[3]=following-sibling::*[1]"/>
	<xsl:if test="$bloc"><xsl:value-of select="'&pt56;'"/></xsl:if>
	<xsl:apply-templates select="child::*[3]" mode="chimie"/>
	<xsl:if test="$bloc"><xsl:value-of select="'&pt23;'"/></xsl:if>
</xsl:template>

<xsl:template match="m:munderover" mode="chimie">
	<xsl:choose>
		<xsl:when test="child::*[1]='&rlarr;' or child::*[1]='&rlhar;'">
		<!-- est-ce une réaction réversible commentée?-->
			<xsl:apply-templates select="child::*[1]" mode="chimie"/>
			<xsl:variable name="dessus">
				<xsl:apply-templates select="child::*[3]" mode="chimie"/>
			</xsl:variable>
			<xsl:variable name="dessous">
				<xsl:apply-templates select="child::*[2]" mode="chimie"/>
			</xsl:variable>
			<xsl:value-of select="concat('&pt236;',translate($dessus,'&pt236;,&pt356;',''),'&pt23;',translate($dessous,'&pt236;,&pt356;',''),'&pt356;')"/>
		</xsl:when>
	</xsl:choose>
</xsl:template>

<xsl:template match="m:mtext" mode="chimie">
	<xsl:if test="not(.='|' or .='&mid;')"><!-- pas de doublets de Lewis -->
		<xsl:value-of select="nat:trChimie(.)"/>
	</xsl:if>
</xsl:template>

<xsl:template match="m:mn" mode="chimie">
	<xsl:choose>
		<xsl:when test="local-name(..)='mmultiscripts' or local-name(../..)='mmultiscripts'"><!-- c'est une représentation symbolique-->
			<xsl:value-of select="'&pt6;'"/>
			<xsl:choose>
				<xsl:when test="count(preceding-sibling::m:mn)=0 and count(preceding-sibling::m:mmultiscripts|preceding-sibling::m:munder)=0"><!-- c'est le premier indice -->
					<xsl:value-of select="'&pt26;'"/>
					<xsl:value-of select="nat:trChimie(.)"/>
				</xsl:when>
				<xsl:when test="count(preceding-sibling::m:mn)=1 and local-name(../..)='mmultiscripts'"><!-- deuxième indice -->
					<xsl:value-of select="'&pt26;&pt26;'"/>
					<xsl:value-of select="nat:trChimie(.)"/>
				</xsl:when>
				<xsl:otherwise><!-- c'est l'exposant gauche -->
					<xsl:value-of select="'&pt4;'"/>
					<xsl:value-of select="nat:trChimie(.)"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		<xsl:otherwise><!-- représentation moléculaire -->
			<xsl:value-of select="nat:trChimie(.)"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="m:munder|m:mover" mode="chimie">
	<xsl:choose>
		<xsl:when test="child::*[2]='&#x2934;'"><!-- dégagement -->
			<xsl:apply-templates select="child::*[1]" mode="chimie"/>
			<xsl:value-of select="'&pt45;&pt156;'"/>
		</xsl:when>
		<xsl:when test="child::*[2]='⤵'"><!-- précipité, &#x2935; ou &curdarrr -->
			<xsl:apply-templates select="child::*[1]" mode="chimie"/>
			<xsl:value-of select="'&pt46;&pt156;'"/>
		</xsl:when>
		<xsl:when test="child::*[1]='&rarr;'">
			<xsl:apply-templates select="child::*[1]" mode="chimie"/>
			<xsl:variable name="commentaire">
				<xsl:apply-templates select="child::*[2]" mode="chimie"/>
			</xsl:variable>
			<xsl:if test="not($commentaire='')">
				<xsl:value-of select="concat('&pt236;',$commentaire,'&pt356;')"/>
			</xsl:if>
		</xsl:when>
		<xsl:when test="child::*[1]='&rlarr;' or child::*[1]='&rlhar;'"><!-- réaction réversible avec 1 seul commentaire -->
			<xsl:apply-templates select="child::*[1]" mode="chimie"/>
			<xsl:variable name="commentaire">
				<xsl:apply-templates select="child::*[2]" mode="chimie"/>
			</xsl:variable>
			<xsl:value-of select="concat('&pt236;',if(local-name(.)='munder') then '&pt5;&pt2;' else translate($commentaire,'&pt236;,&pt356;',''),'&pt23;',if(local-name(.)='munder') then translate($commentaire,'&pt236;,&pt356;','') else '&pt5;&pt2;','&pt356;')"/>
		</xsl:when>
		<xsl:otherwise><!--notation de lewis -->
			<xsl:apply-templates mode="chimie"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="m:mtable" mode="chimie">
	<!-- c'est une formule semi développée -->
	
	<xsl:variable name="formule" as="element()*">
		<xsl:for-each select="m:mtr">
			<xsl:for-each select="m:mtd">
				<xsl:copy-of select="."/>
				<!--<xsl:message select="concat(string(.),';')"/>-->
			</xsl:for-each>
			<!--<xsl:message select="'*******'"/>-->
		</xsl:for-each>
	</xsl:variable>
	<!--<xsl:message select="$formule"/>-->
	<xsl:variable name="nbLignes" as="xs:integer" select="count(m:mtr)"/>
	<!--<xsl:message select="('nbLignes:',$nbLignes)"/>-->
	<xsl:variable name="nbCol" as="xs:integer" select="count(m:mtr[1]/m:mtd)"/>
	<!--<xsl:message select="('nbcol:',$nbCol)"/>-->
	
	<!-- on cherche la ligne principale: celle où il y a les liaisons horizontales -->
	<xsl:variable name="indPrinc" as="xs:integer" select="nat:lPrinc($formule,$nbCol,$nbLignes,1)"/>
	<!--écriture de la formule -->
	<xsl:for-each select="$formule[position() &gt; ($indPrinc - 1)*$nbCol and position() &lt;= $indPrinc*$nbCol]">
		<xsl:if test="not(.='')">
			<xsl:variable name="pos" select="position()" as="xs:integer"/>
			<xsl:variable name="base" as="xs:string*">
				<xsl:variable name="resuBase">
					<xsl:apply-templates select="." mode="chimie"/>
				</xsl:variable>
				<xsl:sequence select="tokenize($resuBase,'&pt46;')"/>
			</xsl:variable>
			<xsl:value-of select="string-join($base[position()&lt;3],'&pt46;')"/><!-- on suppose que si il y a une liaison satellite, elle porte sur le premier élément -->
			<xsl:if test="not(.='-' or .='=' or .='&equiv;')">
				<!-- molécules en indice -->
				<xsl:if test="$indPrinc + 1 &lt; $nbLignes">
					<xsl:choose>
						<xsl:when test="$pos=3 and string($formule[position()= ($indPrinc - 1) * $nbCol + $pos - 2]) = ''">
							<xsl:variable name="liaison" select="$formule[position() = ($indPrinc + 1) * $nbCol + $pos - 2]" as="element()?"/>
							<xsl:if test="not(string($liaison)='')">
								<xsl:value-of select="'&pt26;'"/>
								<!-- la liaison -->
								<xsl:value-of select="nat:trChimie(translate(string($formule[position() = $nbCol + $pos - 1]),'∖','-'))"/>
								<xsl:value-of select="'&pt56;'"/>
								<!-- la molécule -->
								<xsl:apply-templates select="$liaison" mode="chimie"/>
								<xsl:value-of select="'&pt23;'"/>
							</xsl:if>
						</xsl:when>
						<xsl:when test="$pos = last() - 2 and string($formule[position()= ($indPrinc - 1) * $nbCol + $pos + 2]) = ''">
							<xsl:variable name="liaison" select="$formule[position() = ($indPrinc + 1) * $nbCol + $pos + 2]" as="element()?"/>
							<xsl:if test="not(string($liaison)='')">
								<xsl:value-of select="'&pt26;'"/>
								<!-- la liaison -->
								<xsl:value-of select="nat:trChimie(string($formule[position() = $indPrinc * $nbCol + $pos + 1]))"/>
								<xsl:value-of select="'&pt56;'"/>
								<!-- la molécule -->
								<xsl:apply-templates select="$liaison" mode="chimie"/>
								<xsl:value-of select="'&pt23;'"/>
							</xsl:if>
						</xsl:when>
						<xsl:otherwise>
							<xsl:variable name="liaison" select="$formule[position() = ($indPrinc + 1) * $nbCol + $pos]" as="element()?"/>
							<xsl:if test="not(string($liaison)='')">
								<xsl:value-of select="'&pt26;'"/>
								<!-- la liaison -->
								<xsl:value-of select="nat:trChimie(string($formule[position() = $indPrinc * $nbCol + $pos]))"/>
								<xsl:value-of select="'&pt56;'"/>
								<!-- la molécule -->
								<xsl:apply-templates select="$liaison" mode="chimie"/>
								<xsl:value-of select="'&pt23;'"/>
							</xsl:if>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:if>
				<!-- molécules en exposant -->
				<xsl:if test="not($indPrinc = 1)">
					<xsl:choose>
						<xsl:when test="$pos=3 and string($formule[($indPrinc - 1) * $nbCol + $pos - 2]) = ''">
							<xsl:variable name="liaison" select="$formule[1]" as="element()?"/>
							<xsl:if test="not(string($liaison)='')">
								<xsl:value-of select="'&pt4;'"/>
								<!-- la liaison -->
								<xsl:value-of select="nat:trChimie(translate(string($formule[$nbCol + 2]),'\&bslash;','--'))"/>
								<xsl:value-of select="'&pt56;'"/>
								<!-- la molécule -->
								<xsl:apply-templates select="$liaison" mode="chimie"/>
								<xsl:value-of select="'&pt23;'"/>
							</xsl:if>
						</xsl:when>
						<xsl:when test="$pos = last() - 2 and string($formule[position()= ($indPrinc - 1) * $nbCol + $pos + 2]) = ''">
							<xsl:variable name="liaison" select="$formule[position() = ($indPrinc - 3) * $nbCol + $pos + 2]" as="element()?"/>
							<xsl:if test="not(string($liaison)='')">
								<xsl:value-of select="'&pt4;'"/>
								<!-- la liaison -->
								<xsl:value-of select="nat:trChimie(translate(string($formule[position() = ($indPrinc - 2) * $nbCol + $pos + 1]),'/&slash;','--'))"/>
								<xsl:value-of select="'&pt56;'"/>
								<!-- la molécule -->
								<xsl:apply-templates select="$liaison" mode="chimie"/>
								<xsl:value-of select="'&pt23;'"/>
							</xsl:if>
						</xsl:when>
						<xsl:otherwise>
							<xsl:variable name="liaison" select="$formule[position() = ($indPrinc - 3) * $nbCol + $pos]" as="element()?"/>
							<xsl:if test="not(string($liaison)='')">
								<xsl:value-of select="'&pt4;'"/>
								<!-- la liaison -->
								<xsl:value-of select="nat:trChimie(string($formule[position() = ($indPrinc - 2) * $nbCol + $pos]))"/>
								<xsl:value-of select="'&pt56;'"/>
								<!-- la molécule -->
								<xsl:apply-templates select="$liaison" mode="chimie"/>
								<xsl:value-of select="'&pt23;'"/>
							</xsl:if>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:if>
			</xsl:if>
			<xsl:value-of select="if (count($base[position() &gt; 2]) &gt; 0) then '&pt46;' else ()"/>
			<xsl:value-of select="concat(string-join($base[position() &gt; 2],'&pt46;'),$coupeEsth)"/>
		</xsl:if>
	</xsl:for-each>
</xsl:template>

<!-- translate pour la chimie -->
<xsl:function name="nat:trChimie" as="xs:string?" xmlns:nat="http://natbraille.free.fr/xsl">
	<xsl:param name="arg" as="xs:string"/>
	<xsl:variable name="chaine" select="functx:chars($arg)" as="xs:string*"/>
	<xsl:variable name="resu" as="xs:string*">
		<xsl:for-each select="$chaine">
			<xsl:if test="not(lower-case(.) = .)"><!-- c'est une majuscule -->
				<xsl:value-of select="'&pt46;'"/>
			</xsl:if>
			<xsl:value-of select="translate(.,$blackChars,$brailleChars)"/>
		</xsl:for-each>
	</xsl:variable>
	<xsl:value-of select="string-join($resu,'')"/>
</xsl:function>

<xsl:function name="nat:lPrinc" as="xs:integer" xmlns:nat="http://natbraille.free.fr/xsl">
	<xsl:param name="arg" as="element()*"/>
	<xsl:param name="nbCol" as="xs:integer"/>
	<xsl:param name="nbLignes" as="xs:integer"/>
	<xsl:param name="i" as="xs:integer"/>
	
	<xsl:choose>
		<xsl:when test="$i &gt; $nbLignes"><xsl:text>0</xsl:text></xsl:when>
		<xsl:otherwise>
			<xsl:variable name="ligne" select="$arg[position() &gt; $i - 1 and position() &lt;= $i*$nbCol]" as="xs:string*"/>
			<xsl:choose>
				<xsl:when test="functx:contains-any-of(string-join($ligne,''),('-','=','&equiv;'))">
					<xsl:value-of select="$i"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="nat:lPrinc($arg,$nbCol,$nbLignes,$i+1)"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:otherwise>
	</xsl:choose>
</xsl:function>

</xsl:stylesheet>
