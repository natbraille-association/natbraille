<?xml version='1.1' encoding="UTF-8" ?>
<!--
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
-->

<!-- version: 1.0 -->
<!DOCTYPE xsl:stylesheet SYSTEM "nat://system/xsl/mmlents/windob.dtd">
<xsl:stylesheet version="3.0" xmlns:xsl='http://www.w3.org/1999/XSL/Transform' 
xmlns:saxon='http://icl.com/saxon'
xmlns:xs='http://www.w3.org/2001/XMLSchema'
xmlns:fn='http://www.w3.org/2005/xpath-functions'
xmlns:fo='http://www.w3.org/1999/XSL/Format'
xmlns:m='http://www.w3.org/1998/Math/MathML'
xmlns:functx='http://www.functx.com'
xmlns:lit='espacelit'
xmlns:doc='espaceDoc'>

<!-- liste des unités ne présentant pas de confusion possible -->
<xsl:variable name="unites" as="xs:string*" select="('hk','mam','km','hm','dam','dm','cm','mm','ca','kL','hL','daL','dL','cL','mL','kg','hg','dag','dg','cg','mg','th','mth','cal','kJ','kgm','eV','kW','sn','mb','pz','lm','lx','rad','gr','sr','min','dB','rd','ua')"/>
<!-- liste des unités présentant de possibles confusions -->
<xsl:variable name="unitesAmb" as="xs:string*" select="('da','ha','cv','bar','cd','ph','pc')"/>

<!-- liste des unités présentant de possibles confusions mais étant tellement fréquent qu'on ne génère pas l'ambiguïté-->
<xsl:variable name="unitesAmbSimple" as="xs:string*" select="('ma','a','k','h','d','c','m','n','p','f','t','q','g','s')"/>

<xsl:variable name="voyelles" as="xs:string" select="'aeiouyàâéèêëîïôùûüæœ'"/>

<!-- liste des caractères braille avec points supérieurs et des principaux caractères noirs associés -->
<xsl:variable name="l_pt_sup" as="xs:string*" select="('&pt1;','&pt12;','&pt13;','&pt14;','&pt15;','&pt16;',
	'&pt123;','&pt124;','&pt125;','&pt126;','&pt1234;','&pt1235;','&pt1236;','&pt12345;','&pt1245;','&pt12346;','&pt12356;','&pt1246;','&pt1256;','&pt12456;','&pt123456;',
	'&pt134;','&pt135;','&pt136;','&pt135;','&pt136;','&pt1345;','&pt1346;','&pt1356;','&pt13456;',
	'&pt14;','&pt15;','&pt16;','&pt145;','&pt146;','&pt1456;','&pt156;',
	'&pt2345;','&pt245;','&pt23456;','&pt234;','&pt246;','&pt2456;',
	'&pt345;','&pt24;','&pt3456;','&pt346;',
	'&pt4;','&pt45;','&pt46;','&pt456;',
	'a','e','i','o','u','y','à','â','é','è','ê','ë','î','ï','ô','ù','û','ü','æ','œ',
	'b','c','d','f','g','h','j','k','l','m','n','p','q','r','s','t','v','w','x','z','ç')"/>

<xsl:variable name="l_ivb_rules" as="xs:string" select="'â$|â[aeiouyàâéèêëîïôùûüáíóúìòäö&aelig;&oelig;]|ê$|ê[aeiouyàâéèêëîïôùûüáíóúìòäö&aelig;&oelig;]|î$|î[aeiouyàâéèêëîïôùûüáíóúìòäö&aelig;&oelig;]|ô$|ô[aeiouyàâéèêëîïôùûüáíóúìòäö&aelig;&oelig;]|^û|û$|ë[aeiouyàâéèêëîïôùûüáíóúìòäö&aelig;&oelig;]|ï[aeiouyàâéèêëîïôùûüáíóúìòäö&aelig;&oelig;]|^ï$|ü|&oelig;[bmp]|&oelig;$|w$|w[bçcdfghjklmnpqrstvwxz]|[aeiouyàâéèêëîïôùûüáíóúìòäö&aelig;&oelig;]w[aeiouyàâéèêëîïôùûüáíóúìòäö&aelig;&oelig;]|k[bçcdfghjklmnpqrstvwxz]|q[^u]|[^aeiouyàâéèêëîïôùûüáíóúìòäö&aelig;&oelig;]z$|^z$|ç$|ç[bçcdfghjklmnpqrstvwxz]|[aeiouyàâéèêëîïôùûüáíóúìòäö&aelig;&oelig;]è[aeiouyàâéèêëîïôùûüáíóúìòäö&aelig;&oelig;]|^ù$|^c$|^d$|^f$|^g$|^h$|^i$|^j$|^l$|^m$|^n$|^o$|^q$|^r$|^s$|^t$|^u$|^v$|^x$|^è$|^ù$|^ë$|^é$'"/>
<!-- â$|â[aeiouyàâéèêëîïôùûüáíóúìòäö&aelig;&oelig;]|ê$|ê[aeiouyàâéèêëîïôùûüáíóúìòäö&aelig;&oelig;]|î$|î[aeiouyàâéèêëîïôùûüáíóúìòäö&aelig;&oelig;]|ô$|ô[aeiouyàâéèêëîïôùûüáíóúìòäö&aelig;&oelig;]|^û|û$|ë[aeiouyàâéèêëîïôùûüáíóúìòäö&aelig;&oelig;]|ï[aeiouyàâéèêëîïôùûüáíóúìòäö&aelig;&oelig;]|^ï$|ü|&oelig;[bmp]|&oelig;$|w$|w[bçcdfghjklmnpqrstvwxz]|[aeiouyàâéèêëîïôùûüáíóúìòäö&aelig;&oelig;]w[aeiouyàâéèêëîïôùûüáíóúìòäö&aelig;&oelig;]|k|q[^u]|q$|[^e]z$|^z$|ç$|ç[bçcdfghjklmnpqrstvwxz]|[aeiouyàâéèêëîïôùûüáíóúìòäö&aelig;&oelig;]è[aeiouyàâéèêëîïôùûüáíóúìòäö&aelig;&oelig;]|^ù$|^b$|^c$|^d$|^f$|^g$|^h$|^i$|^j$|^l$|^m$|^n$|^o$|^p$|^q$|^r$|^s$|^t$|^u$|^v$|^x$|^z$|^è$|^ù$|^ë$|^é$ -->
<!-- pas besoin  car n'existe pas
	[^e]x[cons.]|^x[cons]|
	à$|
? => /?!"(*)'-§
-->


<xsl:variable name="l_amb_connues" as="xs:string*" select="('ient$','itions$')"/>

<xsl:variable name="l_amb_resol" as="xs:string*" select="(
'^(((abs|(ré)?appar|con|dé|entre|main|(ré)?ob|re|sou)?t|(ad|(cir|dis)+con|contre|(re)?de|(ré)?inter|par|pré|pro|re|(res)?sou|sub|sur)?v|cl|coeffic|défic|effic|esc|excip|grad|impat|inconvén|ingréd|obéd|omnisc|or|pat|presc|quot|récip)ient(s?)$)','&pt256;',
'','&pt146;'
	)"/>
<xsl:variable name="l_amb_nResol" as="xs:string*" select="('^(conv|expéd|émoll|obv)ient$','&pt24;&pt126;|&pt256;&pt2345;','^(aud|(co|ré)?éd|inquis)itions$','&pt246;&pt234;|&pt24;&pt2345;&pt3456;&pt234;','NC|V')"/>
<xsl:variable name="l_amb_nResol_mes" as="xs:string*" select="('^(expéd|émoll|obv)ient$','Verbe (1) ou nom/adjectif (2)?','^(aud|(co|ré)?éd|inquis)itions$','Nom commun pluriel (1) ou verbe à la première personne du pluriel (2)?')"/>
<xsl:variable name="l_amb_nResol_conv" as="xs:string*" select="('^convient$', 'Verbe convier (1) ou convenir (2)?')"/>
<xsl:variable name="l_amb_comp_connues" as="xs:string*" select="('action','faut','merci','père','rôle','route','nature','mère')"/>

<xsl:variable name="l_amb_comp_resol" as="xs:string*" select="('^(ex|in|ré|red|rétro|trans)action(s)?','^(dé)faut(s)?','^merci(s)?','^(com)père(s)?','^(en)?rôle(s)?','^(dé|auto|bi|resto|restau)route(s)?','[^g]nature(s)?','^(com)mère(s)?')"/>



</xsl:stylesheet>
