<?xml version="1.1" encoding="utf-8"?>

<!DOCTYPE xsl:stylesheet SYSTEM "nat://system/xsl/mmlents/windob.dtd">
<xsl:stylesheet version="2.0"
		xmlns:xs='http://www.w3.org/2001/XMLSchema'
		xmlns:xsl='http://www.w3.org/1999/XSL/Transform'
		xmlns:functx='http://www.functx.com' 
		xmlns:fn='http://www.w3.org/2005/xpath-functions'
		xmlns:nat='http://natbraille.free.fr/xsl'
		xmlns:namu='http://natbraille.free.fr/xsl/namu'	>

  <xsl:output method="xml" indent="yes" />

  
  <!-- 
       nat:brl_lyrics 
       - takes a <part> node
       - returns braille <phrase>
       *
       attention, ça ne marchera pas si les notes
       ne sont pas triées dans la partition (présence 
       des éléments backup ou forward)
       *
  -->
  <xsl:function name="nat:brl_lyrics" as ="node()">
    <xsl:param name="ly" as="node()" />

    <!-- 
	 1.
	 sorts lyrics according to lyric@number 
	 and copies note's/notation's/articulation's/breath-mark 
	 element in each lyric
    -->
    <xsl:variable name="sortedLyrics">
      <xsl:for-each select="$ly/measure/note/lyric">
	<xsl:sort select="@number"/>
	<xsl:element name="lyric">
	  <xsl:copy-of select="@*"/>
	  <xsl:copy-of select="./*"/>
	  <xsl:copy-of select="../notations/articulations/breath-mark"/>
	</xsl:element>
      </xsl:for-each>
    </xsl:variable>


    <xsl:message>
      <xsl:copy-of select="$sortedLyrics"/>
    </xsl:message>
    <!--
	2. brl translation
	<phrase>
	<syl>&brlpts</syl>
	</phrase>
    -->
    <phrase>
      <xsl:for-each select="$sortedLyrics/lyric">

	<xsl:variable name="syllabes" select="nat:toTbfr(nat:hyphenate(nat:toBrUTF8(string-join(text,'')),'-'))"/>
	<xsl:variable name="nbSyllabes" select="functx:word-count($syllabes)"/>
	<xsl:variable name="stopSignExists" select="fn:matches(lyric,'[,]')"/>
	<xsl:variable name="nextText" select="following-sibling::lyric[1][./@number = @number]/text"/>

	<xsl:variable name="cLy">
	  <xsl:choose>
	    <xsl:when test="$nbSyllabes = 1">
	      <!-- normal -->
	      <xsl:value-of select="nat:toBrUTF8(string-join(text,''))"/>
	    </xsl:when>
	    <xsl:otherwise>
	      <xsl:choose>              		
		<!-- syllabe muette élidée sur la syllabe suivante
		     pt3 après la dernière syllabe -->
		<xsl:when test="fn:matches($nextText,'^[a|e|i|o|u]')">
		  <xsl:value-of select="concat(text,'&pt3;')"/>
		</xsl:when>
		<!-- syllabe muette affectée à aucune note 
		     muet : pt6 avant la dernière syllabe -->
		<xsl:otherwise> 		  
		  <xsl:value-of select="replace($syllabes,'-','&pt6;')"/>
		</xsl:otherwise>
	      </xsl:choose>
	    </xsl:otherwise>
	  </xsl:choose>
	</xsl:variable>

	<syl>
	  <!--
	      <xsl:attribute name="orig" select="text"/>
	      <xsl:attribute name="syllabic" select="syllabic"/>
	  -->
	  <!-- translation -->
	  <xsl:value-of select="nat:toBrUTF8(string-join($cLy,''))"/>
	  <xsl:if test="(syllabic = 'single') or (syllabic = 'end')" >
	    <xsl:value-of select="'&pt;'" />    
	  </xsl:if>

	  <!-- breath mark with pt6p t34-->
	  <xsl:if test="breath-mark" >
	    <xsl:value-of select="'&pt6;&pt34;'" />    
	  </xsl:if>
	</syl>

	<!-- change of verse in lyrics -->
	<xsl:if test="not(@number = following-sibling::*[1]/@number)">
	  <end/>
	</xsl:if>

	

      </xsl:for-each>

    </phrase>
  </xsl:function>


</xsl:stylesheet>