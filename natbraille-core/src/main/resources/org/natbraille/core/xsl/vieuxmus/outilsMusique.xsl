<?xml version='1.0' encoding="UTF-8" ?>
<!--
 * NAT - An universal Translator
 * Copyright (C) 2007  Didier Erin
 * Contact: natbraille@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
-->

<!-- Cette feuille offre tous les outils standards qui permettront de transcrire un partition -->

<!-- version: 0.1 -->

<!DOCTYPE xsl:stylesheet SYSTEM "nat://system/xsl/mmlents/windob.dtd">

<xsl:stylesheet version="1.0"
xmlns:xsl='http://www.w3.org/1999/XSL/Transform' 
xmlns:saxon='http://icl.com/saxon'
xmlns:m='http://www.w3.org/1998/Math/MathML'
xmlns:lit='espacelit'
xmlns:score-partwise='http://www.musicxml.org/'
xmlns:doc='espaceDoc'>

	<!-- Didier ERIN : transcription d'un note -->
	<xsl:template name="transcrireNote">
		
		<xsl:param name="step"/>
		<xsl:param name="duration"/>
		
		<xsl:choose>
			<xsl:when test="$step = 'A' or $step = 'a'">
				<xsl:choose>
					<xsl:when test="$duration = 'whole' or $duration = '1' or $duration = '16th'">
						<xsl:text>&pt2346;</xsl:text>
					</xsl:when>
					<xsl:when test="$duration = 'half' or $duration = '2' or $duration = '32th'">
						<xsl:text>&pt234;</xsl:text>
					</xsl:when>
					<xsl:when test="$duration = 'quarter' or $duration = '4' or $duration = '64th'">
						<xsl:text>&pt246;</xsl:text>
					</xsl:when>
					<xsl:when test="$duration = 'eighth' or $duration = '8' or $duration = '128th'">
						<xsl:text>&pt24;</xsl:text>
					</xsl:when>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$step = 'B' or $step = 'b'">
				<xsl:choose>
					<xsl:when test="$duration = 'whole' or $duration = '1' or $duration = '16th'">
						<xsl:text>&pt23456;</xsl:text>
					</xsl:when>
					<xsl:when test="$duration = 'half' or $duration = '2' or $duration = '32th'">
						<xsl:text>&pt2345;</xsl:text>
					</xsl:when>
					<xsl:when test="$duration = 'quarter' or $duration = '4' or $duration = '64th'">
						<xsl:text>&pt2456;</xsl:text>
					</xsl:when>
					<xsl:when test="$duration = 'eighth' or $duration = '8' or $duration = '128th'">
						<xsl:text>&pt245;</xsl:text>
					</xsl:when>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$step = 'C' or $step = 'c'">
				<xsl:choose>
					<xsl:when test="$duration = 'whole' or $duration = '1' or $duration = '16th'">
						<xsl:text>&pt13456;</xsl:text>
					</xsl:when>
					<xsl:when test="$duration = 'half' or $duration = '2' or $duration = '32th'">
						<xsl:text>&pt1345;</xsl:text>
					</xsl:when>
					<xsl:when test="$duration = 'quarter' or $duration = '4' or $duration = '64th'">
						<xsl:text>&pt1456;</xsl:text>
					</xsl:when>
					<xsl:when test="$duration = 'eighth' or $duration = '8' or $duration = '128th'">
						<xsl:text>&pt145;</xsl:text>
					</xsl:when>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$step = 'D' or $step = 'd'">
				<xsl:choose>
					<xsl:when test="$duration = 'whole' or $duration = '1' or $duration = '16th'">
						<xsl:text>&pt1356;</xsl:text>
					</xsl:when>
					<xsl:when test="$duration = 'half' or $duration = '2' or $duration = '32th'">
						<xsl:text>&pt135;</xsl:text>
					</xsl:when>
					<xsl:when test="$duration = 'quarter' or $duration = '4' or $duration = '64th'">
						<xsl:text>&pt156;</xsl:text>
					</xsl:when>
					<xsl:when test="$duration = 'eighth' or $duration = '8' or $duration = '128th'">
						<xsl:text>&pt15;</xsl:text>
					</xsl:when>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$step = 'E' or $step = 'e'">
				<xsl:choose>
					<xsl:when test="$duration = 'whole' or $duration = '1' or $duration = '16th'">
						<xsl:text>&pt12346;</xsl:text>
					</xsl:when>
					<xsl:when test="$duration = 'half' or $duration = '2' or $duration = '32th'">
						<xsl:text>&pt1234;</xsl:text>
					</xsl:when>
					<xsl:when test="$duration = 'quarter' or $duration = '4' or $duration = '64th'">
						<xsl:text>&pt1246;</xsl:text>
					</xsl:when>
					<xsl:when test="$duration = 'eighth' or $duration = '8' or $duration = '128th'">
						<xsl:text>&pt124;</xsl:text>
					</xsl:when>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$step = 'F' or $step = 'f'">
				<xsl:choose>
					<xsl:when test="$duration = 'whole' or $duration = '1' or $duration = '16th'">
						<xsl:text>&pt123456;</xsl:text>
					</xsl:when>
					<xsl:when test="$duration = 'half' or $duration = '2' or $duration = '32th'">
						<xsl:text>&pt12345;</xsl:text>
					</xsl:when>
					<xsl:when test="$duration = 'quarter' or $duration = '4' or $duration = '64th'">
						<xsl:text>&pt12456;</xsl:text>
					</xsl:when>
					<xsl:when test="$duration = 'eighth' or $duration = '8' or $duration = '128th'">
						<xsl:text>&pt1245;</xsl:text>
					</xsl:when>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$step = 'G' or $step = 'g'">
				<xsl:choose>
					<xsl:when test="$duration = 'whole' or $duration = '1' or $duration = '16th'">
						<xsl:text>&pt12356;</xsl:text>
					</xsl:when>
					<xsl:when test="$duration = 'half' or $duration = '2' or $duration = '32th'">
						<xsl:text>&pt1235;</xsl:text>
					</xsl:when>
					<xsl:when test="$duration = 'quarter' or $duration = '4' or $duration = '64th'">
						<xsl:text>&pt1256;</xsl:text>
					</xsl:when>
					<xsl:when test="$duration = 'eighth' or $duration = '8' or $duration = '128th'">
						<xsl:text>&pt125;</xsl:text>
					</xsl:when>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$step = 'rest'">
				<xsl:choose>
					<!-- Didier ERIN : tester si il ne s'agit pas d'une pause dans une mesure à 3 tps : 
						dans ce cas, duration sera compris entre 1 et 2
					-->
					<xsl:when test="$duration = 'whole' or $duration = '1' or 
							($duration &gt; 1 and $duration &lt; 2) or $duration = '16th'">
						<xsl:text>&pt134;</xsl:text>
					</xsl:when>
					<xsl:when test="$duration = 'half' or $duration = '2' or $duration = '32th'">
						<xsl:text>&pt136;</xsl:text>
					</xsl:when>
					<xsl:when test="$duration = 'quarter' or $duration = '4' or $duration = '64th'">
						<xsl:text>&pt1236;</xsl:text>
					</xsl:when>
					<xsl:when test="$duration = 'eighth' or $duration = '8' or $duration = '128th'">
						<xsl:text>&pt1346;</xsl:text>
					</xsl:when>
				</xsl:choose>
			</xsl:when>
		</xsl:choose>
		
	</xsl:template>

	<xsl:template name="determinerSymboleAlteration">
		<xsl:param name="step" />
		<xsl:param name="alter" />
		<xsl:param name="armure" />
		
		<xsl:choose>
			<xsl:when test="contains($armure,$step)">
				<xsl:choose>
					<xsl:when test="starts-with($armure,'B')">
						<xsl:choose>
							<xsl:when test="$alter = '-2'">
								<xsl:call-template name="transcrireAlteration">
									<xsl:with-param name="alter" select="'-1'" />
								</xsl:call-template>
							</xsl:when>
							<xsl:when test="$alter = '-1'">
							</xsl:when>
							<xsl:when test="not($alter)">
								<xsl:call-template name="transcrireAlteration">
									<xsl:with-param name="alter" select="'0'" />
								</xsl:call-template>
							</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="transcrireAlteration">
									<xsl:with-param name="alter" select="$alter" />
								</xsl:call-template>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:choose>
							<xsl:when test="$alter = '2'">
								<xsl:call-template name="transcrireAlteration">
									<xsl:with-param name="alter" select="'1'" />
								</xsl:call-template>
							</xsl:when>
							<xsl:when test="$alter = '1'">
							</xsl:when>
							<xsl:when test="not($alter)">
								<xsl:call-template name="transcrireAlteration">
									<xsl:with-param name="alter" select="'0'" />
								</xsl:call-template>
							</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="transcrireAlteration">
									<xsl:with-param name="alter" select="$alter" />
								</xsl:call-template>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="transcrireAlteration">
					<xsl:with-param name="alter" select="$alter" />
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
		
	</xsl:template>

	<!-- Didier ERIN : transcription de la liaison de prolongation -->
	<xsl:template name="transcrireLiaisonDeProlongation">
		<xsl:param name="tie" />
		
		<xsl:value-of select="$tie"/>
		<xsl:if test="$tie[@type='start']">
			<xsl:text>&pt4;&pt14;</xsl:text>
		</xsl:if>
		
	</xsl:template>

	<!-- Didier ERIN : Transcription de la clé -->
	<xsl:template name="transcrireClef">
		<xsl:param name="sign" />
		<xsl:param name="line" />
		<xsl:param name="tone" />

		<xsl:choose>
			<xsl:when test="$tone = '1'">
				<xsl:text>&pt145;</xsl:text>
			</xsl:when>
			<xsl:when test="$tone = '2'">
				<xsl:text>&pt145;&pt145;</xsl:text>
			</xsl:when>
			<xsl:when test="$tone = '3'">
				<xsl:text>&pt145;&pt145;&pt145;</xsl:text>
			</xsl:when>
			<xsl:when test="$tone = '4'">
				<xsl:text>&pt3456;&pt145;&pt145;</xsl:text>
			</xsl:when>
			<xsl:when test="$tone = '5'">
				<xsl:text>&pt3456;&pt15;&pt145;</xsl:text>
			</xsl:when>
			<xsl:when test="$tone = '6'">
				<xsl:text>&pt3456;&pt124;&pt145;</xsl:text>
			</xsl:when>
			<xsl:when test="$tone = '7'">
				<xsl:text>&pt3456;&pt1245;&pt145;</xsl:text>
			</xsl:when>
			<xsl:when test="$tone = '-1'">
				<xsl:text>&pt126;</xsl:text>
			</xsl:when>
			<xsl:when test="$tone = '-2'">
				<xsl:text>&pt126;&pt126;</xsl:text>
			</xsl:when>
			<xsl:when test="$tone = '-3'">
				<xsl:text>&pt126;&pt126;&pt126;</xsl:text>
			</xsl:when>
			<xsl:when test="$tone = '-4'">
				<xsl:text>&pt3456;&pt145;&pt126;</xsl:text>
			</xsl:when>
			<xsl:when test="$tone = '-5'">
				<xsl:text>&pt3456;&pt15;&pt126;</xsl:text>
			</xsl:when>
			<xsl:when test="$tone = '-6'">
				<xsl:text>&pt3456;&pt124;&pt126;</xsl:text>
			</xsl:when>
			<xsl:when test="$tone = '-7'">
				<xsl:text>&pt3456;&pt1245;&pt126;</xsl:text>
			</xsl:when>
		</xsl:choose>
		
		<xsl:choose>
			<xsl:when test="$sign = 'G'">
				<xsl:choose>
					<xsl:when test="$line = 2">
						<xsl:text>&pt345;&pt34;&pt123;</xsl:text>
					</xsl:when>
					<xsl:when test="$line = 1">
						<xsl:text>&pt345;&pt34;&pt4;&pt123;</xsl:text>
					</xsl:when>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$sign = 'F'">
				<xsl:choose>
					<xsl:when test="$line = 5">
						<xsl:text>&pt345;&pt3456;&pt46;&pt123;</xsl:text>
					</xsl:when>
					<xsl:when test="$line = 4">
						<xsl:text>&pt345;&pt3456;&pt123;</xsl:text>
					</xsl:when>
					<xsl:when test="$line = 3">
						<xsl:text>&pt345;&pt3456;&pt456;&pt123;</xsl:text>
					</xsl:when>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$sign = 'C'">
				<xsl:choose>
					<xsl:when test="$line = 5">
						<xsl:text>&pt345;&pt346;&pt46;&pt123;</xsl:text>
					</xsl:when>
					<xsl:when test="$line = 4">
						<xsl:text>&pt345;&pt346;&pt5;&pt123;</xsl:text>
					</xsl:when>
					<xsl:when test="$line = 3">
						<xsl:text>&pt345;&pt346;&pt123;</xsl:text>
					</xsl:when>
					<xsl:when test="$line = 2">
						<xsl:text>&pt345;&pt346;&pt45;&pt123;</xsl:text>
					</xsl:when>
					<xsl:when test="$line = 1">
						<xsl:text>&pt345;&pt346;&pt4;&pt123;</xsl:text>
					</xsl:when>
				</xsl:choose>
			</xsl:when>
		</xsl:choose>

	</xsl:template>

	<!-- Didier ERIN : Transcription du temps de la mesure -->
	<xsl:template name="transcrireTempsMesure">
		<xsl:param name="beats" />
		<xsl:param name="beat-type" />
		
		<xsl:text>&pt;&pt3456;</xsl:text>

		<xsl:choose>
			<xsl:when test="$beats = 2">
				<xsl:text>&pt12;</xsl:text>
			</xsl:when>
			<xsl:when test="$beats = 3">
				<xsl:text>&pt14;</xsl:text>
			</xsl:when>
			<xsl:when test="$beats = 4">
				<xsl:text>&pt145;</xsl:text>
			</xsl:when>
			<xsl:when test="$beats = 6">
				<xsl:text>&pt124;</xsl:text>
			</xsl:when>
			<xsl:when test="$beats = 9">
				<xsl:text>&pt24;</xsl:text>
			</xsl:when>
			<xsl:when test="$beats = 12">
				<xsl:text>&pt1;&pt12;</xsl:text>
			</xsl:when>
		</xsl:choose>
		
		<xsl:choose>
			<xsl:when test="$beat-type = 2">
				<xsl:text>&pt25;&pt;</xsl:text>
			</xsl:when>
			<xsl:when test="$beat-type = 4">
				<xsl:text>&pt256;&pt;</xsl:text>
			</xsl:when>
			<xsl:when test="$beat-type = 8">
				<xsl:text>&pt236;&pt;</xsl:text>
			</xsl:when>
			<xsl:when test="$beat-type = 16">
				<xsl:text>&pt2;&pt235;&pt;</xsl:text>
			</xsl:when>
		</xsl:choose>

	</xsl:template>

	<!-- Didier ERIN : Retrouver la division correspondant à la mesure courante -->
	<xsl:template name="retrouverDivision">
		<xsl:param name="measure" />
		
		<xsl:choose>
			<xsl:when test="$measure/attributes/divisions" >
				<xsl:value-of select="$measure/attributes/divisions" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$measure/preceding-sibling::measure/attributes/division" />
			</xsl:otherwise>
		</xsl:choose>

	</xsl:template>

	<!-- Didier ERIN : Retrouver la tonalité qui s'applique à la mesure en cours -->
	<xsl:template name="retrouverTonalite">
		<xsl:param name="measure" />

		<xsl:choose>
			<xsl:when test="$measure/attributes/key/fifths" >
				<xsl:value-of select="$measure/attributes/key/fifths" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$measure/preceding-sibling::measure/attributes/key/fifths" />
			</xsl:otherwise>
		</xsl:choose>

	</xsl:template>

	<!-- Didier ERIN : affiche le symbole d'altération demandé -->
	<xsl:template name="transcrireAlteration">
		<xsl:param name="alter" />

		<xsl:choose>
			<xsl:when test="$alter = 2">
				<xsl:text>&pt146;&pt146;</xsl:text>
			</xsl:when>
			<xsl:when test="$alter = 1">
				<xsl:text>&pt146;</xsl:text>
			</xsl:when>
			<xsl:when test="$alter = 0">
				<xsl:text>&pt16;</xsl:text>
			</xsl:when>
			<xsl:when test="$alter = -1">
				<xsl:text>&pt126;</xsl:text>
			</xsl:when>
			<xsl:when test="$alter = -2">
				<xsl:text>&pt126;&pt126;</xsl:text>
			</xsl:when>
		</xsl:choose>

	</xsl:template>

	<!-- Didier ERIN : Retourne la liste des notes altérés selon la tonalité -->
	<xsl:template name="trouverArmure">
		<xsl:param name="tone" />

		<xsl:choose>
			<xsl:when test="$tone = '1'">
				<xsl:text>F</xsl:text>
			</xsl:when>
			<xsl:when test="$tone = '2'">
				<xsl:text>FC</xsl:text>
			</xsl:when>
			<xsl:when test="$tone = '3'">
				<xsl:text>FCG</xsl:text>
			</xsl:when>
			<xsl:when test="$tone = '4'">
				<xsl:text>FCGD</xsl:text>
			</xsl:when>
			<xsl:when test="$tone = '5'">
				<xsl:text>FCGDA</xsl:text>
			</xsl:when>
			<xsl:when test="$tone = '6'">
				<xsl:text>FCGDAE</xsl:text>
			</xsl:when>
			<xsl:when test="$tone = '7'">
				<xsl:text>FCGDAEB</xsl:text>
			</xsl:when>
			<xsl:when test="$tone = '-1'">
				<xsl:text>B</xsl:text>
			</xsl:when>
			<xsl:when test="$tone = '-2'">
				<xsl:text>BE</xsl:text>
			</xsl:when>
			<xsl:when test="$tone = '-3'">
				<xsl:text>BEA</xsl:text>
			</xsl:when>
			<xsl:when test="$tone = '-4'">
				<xsl:text>BEAD</xsl:text>
			</xsl:when>
			<xsl:when test="$tone = '-5'">
				<xsl:text>BEADG</xsl:text>
			</xsl:when>
			<xsl:when test="$tone = '-6'">
				<xsl:text>BEADGC</xsl:text>
			</xsl:when>
			<xsl:when test="$tone = '-7'">
				<xsl:text>BEADGCF</xsl:text>
			</xsl:when>
		</xsl:choose>

	</xsl:template>

	<!-- Didier ERIN : transcription des barres de mesures -->
	<xsl:template name="transcrireBarreDeMesure">
		<xsl:param name="barline" />
		
		<xsl:choose>
			<xsl:when test="not($barline)">
				<xsl:text>&pt;</xsl:text>
			</xsl:when>
			<xsl:when test="$barline/bar-style = 'light-heavy'">
				<xsl:text>&pt126;&pt13;&pt;</xsl:text>
			</xsl:when>
			<xsl:when test="$barline/bar-style = 'light-light'">
				<xsl:text>&pt126;&pt13;&pt3;&pt;</xsl:text>
			</xsl:when>
		</xsl:choose>
		
	</xsl:template>

	<!-- Didier ERIN : transcription de l'octave -->
	<xsl:template name="transcrireOctave">
		<xsl:param name="octave" />
		
		<xsl:choose>
			<xsl:when test="$octave = 1">
				<xsl:text>&pt4;</xsl:text>
			</xsl:when>
			<xsl:when test="$octave = 2">
				<xsl:text>&pt45;</xsl:text>
			</xsl:when>
			<xsl:when test="$octave = 3">
				<xsl:text>&pt456;</xsl:text>
			</xsl:when>
			<xsl:when test="$octave = 4">
				<xsl:text>&pt5;</xsl:text>
			</xsl:when>
			<xsl:when test="$octave = 5">
				<xsl:text>&pt46;</xsl:text>
			</xsl:when>
			<xsl:when test="$octave = 6">
				<xsl:text>&pt56;</xsl:text>
			</xsl:when>
			<xsl:when test="$octave = 7">
				<xsl:text>&pt6;</xsl:text>
			</xsl:when>
		</xsl:choose> 
		
	</xsl:template>
	
	<!-- Didier ERIN : template permettant de savoir si oui ou non il faut indiquer l'octave et de l'afficher si c'est le cas -->
	<xsl:template name="indiquerOctave">
		<xsl:param name="noteCourante" />
		<xsl:param name="notePrecedante" />
		<xsl:param name="octavePrecedant" />


		<xsl:variable name="gammeMontante" select="'CDEFGAB'" />
		<xsl:variable name="gammeDescendante" select="'BAGFEDC'" />
		<xsl:variable name="gammeMontante2" select="'CDEFGABCDEFGAB'" />
		<xsl:variable name="gammeDescendante2" select="'BAGFEDCBAGFEDC'" />
		
		<xsl:variable name="intervalleMontant">
			<xsl:value-of select="substring-before(
				substring-after( $gammeMontante, substring-before( $gammeMontante, $notePrecedante ) ),
				substring-after(
				substring-after( $gammeMontante, substring-before( $gammeMontante, $notePrecedante ) ),
				$noteCourante/step
				)
				)" />
		</xsl:variable>
		<xsl:variable name="intervalleDescendant">
			<xsl:value-of select="substring-before(
				substring-after( $gammeDescendante, substring-before( $gammeDescendante, $notePrecedante ) ),
				substring-after(
				substring-after( $gammeDescendante, substring-before( $gammeDescendante, $notePrecedante ) ),
				$noteCourante/step
				)
				)" />
		</xsl:variable>
		<xsl:variable name="intervalleMontant2">
			<xsl:value-of select="substring-before(
				substring-after( $gammeMontante2, substring-before($gammeMontante2,$notePrecedante) ),
				substring-after(
				substring-after( $gammeMontante2, substring-before( $gammeMontante2, $notePrecedante ) ),
				$noteCourante/step
				)
				)" />
		</xsl:variable>
		<xsl:variable name="intervalleDescendant2">
			<xsl:value-of select="substring-before(
				substring-after( $gammeDescendante2, substring-before($gammeDescendante2,$notePrecedante) ),
				substring-after(
				substring-after( $gammeDescendante2, substring-before( $gammeDescendante2, $notePrecedante ) ),
				$noteCourante/step
				)
				)" />
		</xsl:variable>
		
		<xsl:variable name="intervalle">
			<xsl:choose>
				<xsl:when test="$notePrecedante and $noteCourante/octave = $octavePrecedant and $noteCourante/step = $notePrecedante">
					<xsl:value-of select="$noteCourante/step" />
				</xsl:when>
				<xsl:when test="$notePrecedante and $noteCourante/octave = $octavePrecedant and string-length($intervalleMontant) > 0">
					<xsl:value-of select="$intervalleMontant" />
				</xsl:when>
				<xsl:when test="$notePrecedante and $noteCourante/octave = $octavePrecedant and string-length($intervalleDescendant) > 0">
					<xsl:value-of select="$intervalleDescendant" />
				</xsl:when>
				<xsl:when test="$notePrecedante and $noteCourante/octave > $octavePrecedant and $noteCourante/octave - $octavePrecedant = 1">
					<xsl:value-of select="$intervalleMontant2" />
				</xsl:when>
				<xsl:when test="$notePrecedante and $octavePrecedant > $noteCourante/octave and $octavePrecedant - $noteCourante/octave = 1">
					<xsl:value-of select="$intervalleDescendant2" />
				</xsl:when>
			</xsl:choose>
		</xsl:variable>
		
		<xsl:choose>
			<xsl:when test="not($notePrecedante)">
				<xsl:call-template name="transcrireOctave">
					<xsl:with-param name="octave" select="$noteCourante/octave" />
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="string-length($intervalle) >= 6">
				<xsl:call-template name="transcrireOctave">
					<xsl:with-param name="octave" select="$noteCourante/octave" />
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="string-length($intervalle) >= 4 and 5 >= string-length($intervalle)">
				<xsl:if test="$noteCourante/octave != $octavePrecedant">
					<xsl:call-template name="transcrireOctave">
						<xsl:with-param name="octave" select="$noteCourante/octave" />
					</xsl:call-template>
				</xsl:if>
			</xsl:when>
		</xsl:choose>

	</xsl:template>

	<!-- Didier ERIN : template retournant le nombre de notes impliquées dans un phrasé -->
	<xsl:template name="nbNoteSuivPhrase" >
		<xsl:param name="note" />
		
		<xsl:choose>
			<xsl:when test="$note//slur[ @type = 'start' ]" >
				<xsl:variable name="x">
					<xsl:call-template name="nbNoteSuivPhrase">
						<xsl:with-param name="note" select="$note/following::note[1]" />
					</xsl:call-template>
				</xsl:variable>
				<xsl:value-of select="1 + $x"/>
			</xsl:when>
			<xsl:when test="$note//slur[ @type = 'stop' ]" >
				<xsl:value-of select="1"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="x">
					<xsl:call-template name="nbNoteSuivPhrase">
						<xsl:with-param name="note" select="$note/following::note[1]" />
					</xsl:call-template>
				</xsl:variable>
				<xsl:value-of select="1 + $x"/>
			</xsl:otherwise>
		</xsl:choose>
	
	</xsl:template>
	
	<!-- Didier ERIN : template retournant le nombre de notes impliquées dans un phrasé -->
	<xsl:template name="nbNotePrecPhrase" >
		<xsl:param name="note" />
		
		<xsl:choose>
			<xsl:when test="$note//slur[ @type = 'stop' ]" >
				<xsl:variable name="x">
					<xsl:call-template name="nbNotePrecPhrase">
						<xsl:with-param name="note" select="$note/preceding::note[1]" />
					</xsl:call-template>
				</xsl:variable>
				<xsl:value-of select="1 + $x"/>
			</xsl:when>
			<xsl:when test="$note//slur[ @type = 'start' ]" >
				<xsl:value-of select="1"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="x">
					<xsl:call-template name="nbNotePrecPhrase">
						<xsl:with-param name="note" select="$note/preceding::note[1]" />
					</xsl:call-template>
				</xsl:variable>
				<xsl:value-of select="1 + $x"/>
			</xsl:otherwise>
		</xsl:choose>
		
	</xsl:template>
	
	<!-- Didier ERIN : template retournant si une note appartient à une phrasé ou pas -->
	<xsl:template name="dansPhrase">
		<xsl:param name="note"/>
		
		<xsl:variable name="suiv" select="$note/following::note//slur[last()]"/>
		<xsl:choose>
			<xsl:when test="$suiv[@type = 'stop']">
				<xsl:text>true</xsl:text>				
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>false</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<!--<xsl:value-of select="$note/preceding::note//slur[1 and  @type='start']"/>-->
		
	</xsl:template>
	
		
	
</xsl:stylesheet>
