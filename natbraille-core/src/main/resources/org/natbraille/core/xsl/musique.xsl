<?xml version="1.1" encoding="utf-8"?>

<!DOCTYPE xsl:stylesheet SYSTEM "nat://system/xsl/mmlents/windob.dtd">
<xsl:stylesheet version="2.0"
		xmlns:xs='http://www.w3.org/2001/XMLSchema'
		xmlns:xsl='http://www.w3.org/1999/XSL/Transform'
		xmlns:functx='http://www.functx.com' 
		xmlns:fn='http://www.w3.org/2005/xpath-functions'
		xmlns:nat='http://natbraille.free.fr/xsl'
		xmlns:namu='http://natbraille.free.fr/xsl/namu'	>

  <xsl:output method="xml" indent="yes" />

  <xsl:include href="mu_measure.xsl" /> 
  <xsl:include href="mu_items.xsl" /> 
  <xsl:include href="mu_sep.xsl" /> 
  <xsl:include href="mu_lyric.xsl" />  
<!--  <xsl:include href="mu_time.xsl" />  -->
  <xsl:include href="functions/functx.xsl" />
<!--  <xsl:variable name="mdbg" select="fn:true()" />-->
  <xsl:variable name="mdbg" select="fn:false()" />
  <xsl:strip-space elements="*" />

  <!-- 
       Score-partwise 
  -->
  <xsl:template match="score-partwise">
    <xsl:element name="musix">
      <xsl:element name="titre">
	<xsl:attribute name="niveauBraille" select="1" />
	<xsl:variable name="exprLit">
	  <xsl:copy-of select="nat:plaintxt2lit(work/work-title)"/>
	  <xsl:copy-of select="nat:plaintxt2lit(credit/credit-words)" />
	</xsl:variable>
	<xsl:apply-templates select="$exprLit"/>
      </xsl:element>
      <xsl:apply-templates/>
    </xsl:element>
  </xsl:template>

  <!-- 
       Part
  -->
  <xsl:template match="part">
    <xsl:element name="{local-name(.)}" >
      <xsl:copy-of select="@*"/>   
      <xsl:apply-templates />
      <xsl:if test="measure/note/lyric">
	<xsl:copy-of select="nat:brl_lyrics(.)" />
      </xsl:if>
    </xsl:element>
  </xsl:template>
  
  <!-- 
       Measure
  -->
  <xsl:template match="measure">
    <!-- 
	 adds a @start-time and @end-time as attribute of <note> 
    --> 
    <xsl:variable name="mesuretimee">
      <xsl:element name="{local-name(.)}" >
	<xsl:copy-of select="@*"/>

	<!-- -->
	<xsl:for-each select="note|attributes|direction|barline">
<!--	  <xsl:variable name="position" select="position()"/>
	  <xsl:message>
	    <o><xsl:value-of select="$position"/></o>
	  </xsl:message>
-->
	  <xsl:variable name="tDuration" 
			select="if (number(./duration)) then (number(./duration)) else (0)"/>
	  <xsl:variable name="tChordedNotes"
			select="sum(preceding-sibling::note[./following-sibling::note[1]/chord]/duration)"/>
	  <xsl:variable name="tNotes" 
			select="sum(preceding-sibling::note/duration)"/> 
	  <xsl:variable name="tBackups"
			select="sum(preceding-sibling::backup)"/> 
	  <xsl:variable name="tForwards"
			select="sum(preceding-sibling::forward)"/> 
	  <xsl:element name="{local-name(.)}" >
	    <xsl:copy-of select="@*"/>
	    <xsl:attribute name="start-time">
	      <xsl:value-of select="($tNotes + $tForwards -$tBackups - $tChordedNotes)" />
	    </xsl:attribute>
	    <xsl:attribute name="end-time">
	      <xsl:value-of select="($tNotes + $tForwards -$tBackups - $tChordedNotes + $tDuration)" />
	    </xsl:attribute>
	    <xsl:copy-of copy-namespaces="no" select="./*"/>
	  </xsl:element>
	  
	</xsl:for-each>
	<!-- -->
	
      </xsl:element>
    </xsl:variable>

    <!-- 
	 sort by note/voice ascending and note/@start-time;
    --> 
    <xsl:variable name="mesuretimee2">
      <xsl:for-each select="$mesuretimee">
	<xsl:sort order="descending" select="nat:pitches_to_interval_class(./note/pitch)" />
	<xsl:sort select="./note/pitch/octave" />
	<xsl:sort select="./note/voice" />
	<xsl:sort select="./*/@start-time" />
	<xsl:copy-of select="." />
      </xsl:for-each>
    </xsl:variable>

    <!-- 
	 add "copule" between voice 
    -->    
    <xsl:variable name="readyfortraduc">
      <xsl:element name="{local-name(.)}" >
	<xsl:copy-of select="@*"/>
	<xsl:for-each select="$mesuretimee2/*/*">
	  <xsl:if test="(./voice) 
			and (preceding-sibling::note/voice)
			and (not(voice = preceding-sibling::note/voice))">
	    <xsl:element name="copule"/>
	  </xsl:if>
	  <xsl:copy-of select='.' />
	</xsl:for-each>
      </xsl:element>
    </xsl:variable>
    
    <!--
        translate measure
    -->
    <xsl:for-each select="$readyfortraduc/*"> <!-- false foreach -->
      <xsl:element name="{local-name(.)}" >
	<xsl:copy-of select="@*"/>
	<xsl:call-template name="trAduc">
	  <xsl:with-param name="in_num_child" select="1" />
	  <xsl:with-param name="in_max_num_child" select="count(*)" /> 
	  <xsl:with-param name="in_attributes" >
	    <xsl:call-template name="getAttributeDefautValues" />
	  </xsl:with-param>
	  <xsl:with-param name="in_comparation_pitch" />
	</xsl:call-template>
	<xsl:if test="not(./barline[@location='right'])">
	  <xsl:value-of select="nat:brl_bar('regular')"/>
	</xsl:if>
      </xsl:element>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="identification|defaults|credit|part-list"/>

</xsl:stylesheet>

<!--

A section B section C           // A > B > C en succession
A copule B copule C             // A|B|C simultanés
A section B copulepartielle C   // A > B|C

(A,B,C) = notes|accords

<measure>
<section>A</section>
<section>B</section>
<section>C</section>
</measure>

<measure>
<copule>A</copule>
<copule>B</copule>
<copule>C</copule>
</measure>

<measure>
<section>A</section>
<section>
<copule>B</copule>         -> copule partielle
<copule>C</copule>
</section>
</measure>


======================== 

<measure>
A
<section/>
B
<section/>
C
</measure>

<measure>
A
<copule/>
B
<copule/>
C
</measure>

<measure>
A
<section/>
B
<copule/>                       -> copule partielle
C
</measure>






-->