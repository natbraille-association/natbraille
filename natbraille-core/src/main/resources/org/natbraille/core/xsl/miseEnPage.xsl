<?xml version='1.1' encoding="UTF-8" ?>
<!--
* NAT - An universal Translator
* Copyright (C) 2009 Bruno Mascret
* Contact: bmascret@free.fr
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
-->

<!DOCTYPE xsl:stylesheet SYSTEM "nat://system/xsl/mmlents/windob.dtd">

<xsl:stylesheet version="3.0"
xmlns:xsl='http://www.w3.org/1999/XSL/Transform' 
xmlns:saxon='http://icl.com/saxon'
xmlns:m='http://www.w3.org/1998/Math/MathML'
xmlns:fn='http://www.w3.org/2005/xpath-functions'
xmlns:functx='http://www.functx.com'
xmlns:xs='http://www.w3.org/2001/XMLSchema'
xmlns:map='http://www.w3.org/2005/xpath-functions/map'
xmlns:xhtml='http://www.w3.org/1999/xhtml'
xmlns:nat='http://natbraille.free.fr/xsl'
xmlns:layout='http://natbraille.free.fr/layout'
xmlns:lit='espacelit'
xmlns:doc='espaceDoc'>
	<xsl:param name="numStartingPage" as="xs:integer" select="1" />
	<xsl:param name="numStartingLine" as="xs:integer" select="1" />
	<xsl:param name="doNotAddEmptyLinesAtEnd" as="xs:boolean" select="false()" />
					
	<xsl:include href="nat://system/xsl/coupures.xsl"/>
<!-- pas mal de variables globales utilisées dans cette feuille sont dans coupures.xsl -->
<!-- rappel : 
$modeLigneVide 	= 0 -> conserver lignes d'origine 
			= 1 -> personnaliser 
			= 2 -> supprimer tout
			= 3 -> braille aere
			= 4 -> braille compact 
			= 5 -> double interligne -->
<!-- $interligne nombre de ligne d'interligne (0 la plupart des cas, 1 si modeLigneVide="double interligne" -->
			
<!-- cette variable est une ligne vide uniquement si NAT prend complètement en charge la mep 
  gestion des lignes vides : norme compacte ou aérée -->
<xsl:variable name="sautConditionnel" as="node()?">
	<xsl:if test="$modeLigneVide=(3,4)"><xsl:element name="layout:lineBreak"/></xsl:if><!--TODO NAT3 test -->
</xsl:variable>

<!-- ensemble des styles à l'envers -->
<xsl:variable name="stylesEnvers" as="xs:string*" select="$styles[@upsidedown='yes']/@name" />


<xsl:accumulator name="a" initial-value="0">
   <xsl:accumulator-rule match="braille | prefix |suffix" select="concat($value,string(.))"/>
</xsl:accumulator>

<xsl:output method="xml" encoding="UTF-8" indent="no"/>

<xsl:template match="doc:doc">
	<doc:doc xmlns:doc="espaceDoc">
	<xsl:apply-templates select="@*" />
	<xsl:call-template name="miseEnPage">
	</xsl:call-template>
	<!-- <xsl:if test="$formFeedEnd">
		<xsl:text>&#12;</xsl:text>
	</xsl:if> -->
	</doc:doc>
</xsl:template>

<!-- TEMPLATE PRINCIPAL DE MISE EN PAGE -->
<xsl:template name="miseEnPage">
            
<!-- <saxon:iterate select="*" xmlns:saxon="http://saxon.sf.net/" xsl:extension-element-prefixes="saxon">
 -->	
		<xsl:param name="numNoeud" as="xs:integer" select="1" />
		<xsl:param name="numPage" as="xs:integer" select="$numStartingPage" />
		<xsl:param name="numLigne" as="xs:integer" select="$numStartingLine" />
		<xsl:param name="tableMatiere" as="element()*"/>
	
		<xsl:variable name="currentNode" select="*[$numNoeud]"/>
		<xsl:variable name="nbNoeuds" select="count(*)" />
<!-- 		<xsl:message>
			RPI:		<xsl:copy-of select='name($currentNode)'/>
			RPI:		<xsl:value-of select='nat:toTbfr($currentNode)'/>
		</xsl:message>-->
 
		<!-- 
		<xsl:message>RPFFFFFFFFFFFFFFFF</xsl:message>
		 -->
		 <!-- <xsl:value-of select="concat('PROUT',count(*[$numNoeud][self::titre and preceding-sibling::*[string(.)][1][not(self::titre)]]))" /> -->
		<xsl:variable name="texteMisEnPage">
			<xsl:for-each select="$currentNode">
				<xsl:choose>
					<!-- saut de page -->
					<xsl:when test="self::page-break and $keepPageBreak and $numNoeud>1">
						<!--<xsl:message select="OUI"/>-->
						<xsl:value-of select="translate(doc:sautePage(true(),$numLigne,$numPage),concat($sautAGenerer,$espace),'&#10;&pt;')"/>
					</xsl:when>
					<xsl:when test="not(string(.))"><!-- le fils est vide -->
						<xsl:call-template name="gestionLignesVides">
							<xsl:with-param name="numLigne" select="$numLigne" />
							<xsl:with-param name="numPage" select="$numPage" />
						</xsl:call-template>
					</xsl:when>
					<xsl:when test="self::echap">
						<xsl:variable name="echappe" select="concat(.,doc:sautAGenerer($numLigne,$numPage,string-length(.)))" />
						<xsl:value-of select="translate($echappe,concat($espace,$sautAGenerer),'&pt;&#10;')" />
					</xsl:when>
					<xsl:when test="self::doc:ul or self::doc:ol or self::doc:dl"> <!-- liste à puces -->
						<xsl:call-template name="MEPliste">
							<xsl:with-param name="numPage" select="$numPage" tunnel="yes"/>
							<xsl:with-param name="numLigne" select="$numLigne" tunnel="yes" />
						</xsl:call-template>
					</xsl:when>
					<xsl:when test="self::par or self::table or self::math">
					<!--  
						<xsl:message>TOTOTOTOTO phrase</xsl:message>
					-->
						<xsl:call-template name="MEPphrase">
							<xsl:with-param name="numPage" select="$numPage" />
							<xsl:with-param name="numLigne" select="$numLigne" />
							<xsl:with-param name="center" select="@center" />
						</xsl:call-template>
					</xsl:when>
					<xsl:when test="self::titre and
						(preceding-sibling::*[string(.)][1][not(self::titre)] or ../titre[1]=. or $modeLigneVide=(0,1,2,5))">
					<!-- si un titre a un preceding-sibling titre non vide, et que le choix de glv est norme braille aérée ou compacte il a déjà été traité donc on l'ignore -->
						<xsl:call-template name="MEPtitre">
							<xsl:with-param name="numPage" select="$numPage" />
							<xsl:with-param name="numLigne" select="$numLigne" />
						</xsl:call-template>
					</xsl:when>
					<xsl:when test="self::musix">
						<xsl:call-template name="MEPmusique">
							<xsl:with-param name="numPage" select="$numPage" />
							<xsl:with-param name="numLigne" select="$numLigne" />
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise><xsl:text></xsl:text></xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</xsl:variable>

		<!-- ******** affichage du texte ********* -->


		<xsl:variable name="texteMisEnPageJoint" select="string-join($texteMisEnPage,'')" as="xs:string" />
		<xsl:copy select="$currentNode">
			<xsl:copy-of select="@*"/>
			<xsl:element name="layout">
				<xsl:copy-of select="$texteMisEnPage"/>
			</xsl:element>
		</xsl:copy>
	
	<!--<xsl:element name="mep">
		<xsl:copy-of select="$currentNode"/>
		<xsl:element name="layout">
			<xsl:copy-of select="$texteMisEnPageJoint" />
		</xsl:element>
	</xsl:element>-->

	<!-- calcul pages et lignes -->
		<xsl:variable name="nouvPage" as="xs:integer" select="doc:nouvellePage($texteMisEnPageJoint,$numPage)" />
		<xsl:variable name="nouvLigne" as="xs:integer" select="doc:nouvelleLigne($texteMisEnPageJoint,$numLigne)" />
		<!-- appel final pour num de page et saut de page -->
		<xsl:if test="$mise_en_page and $numNoeud=$nbNoeuds and not($insertIndexBraille and count($tableMatiere)&gt;1)">
                <xsl:if test="not($doNotAddEmptyLinesAtEnd)">
			<xsl:variable name="completeLastPage" as="xs:string" select="doc:sautePage(true(),$nouvLigne,$nouvPage)" />
			<xsl:value-of select="translate($completeLastPage,concat($sautAGenerer,$espace),'&#10;&pt;')" />
		</xsl:if>
                </xsl:if>
		<xsl:variable name="titres" as="element()*">
			<xsl:for-each select="$currentNode">
				<xsl:choose>
					<xsl:when test="self::titre">
						<xsl:variable name="leTitre" as="element()">
							<xsl:copy>
								<xsl:attribute name="page" select="$nouvPage"/>
								<xsl:attribute name="niveauOrig" select="@niveauOrig"/>
								<!-- on ajoute la chaine permettant de transcrire le numéro en fin de ligne, adaptera ensuite -->
								<xsl:value-of select="concat(.,'&pt3;&pt3;','&pt6;',$nouvPage)"/>
							</xsl:copy>
						</xsl:variable>
						<xsl:sequence select="($tableMatiere,$leTitre)"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:sequence select="$tableMatiere"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</xsl:variable>
		<!-- ********** affichage table matieres *************-->
		<xsl:if test="$insertIndexBraille and $numNoeud=$nbNoeuds">
			<!--<xsl:message select="$tableMatiere"/>-->
			<xsl:value-of select="doc:writeIndex($titres,'&pt3;',$nouvLigne)"/>
		</xsl:if>		
		
	<!-- si on est pas à la dernière phrase, on continue -->
<!--	<xsl:if test="($numNoeud &lt; $nbNoeuds)">
		<xsl:call-template name="miseEnPage">
			<xsl:with-param name="numNoeud" as="xs:integer" select="($numNoeud + 1)" />
			<xsl:with-param name="numPage" as="xs:integer" select="$nouvPage" />
			<xsl:with-param name="numLigne" as="xs:integer" select="$nouvLigne" />
			<xsl:with-param name="tableMatiere" as="element()*" select="$titres"/>
		</xsl:call-template>
	</xsl:if>-->
        <xsl:choose>
            <xsl:when test="($numNoeud &lt; $nbNoeuds)">
		<xsl:call-template name="miseEnPage">
			<xsl:with-param name="numNoeud" as="xs:integer" select="($numNoeud + 1)" />
			<xsl:with-param name="numPage" as="xs:integer" select="$nouvPage" />
			<xsl:with-param name="numLigne" as="xs:integer" select="$nouvLigne" />
			<xsl:with-param name="tableMatiere" as="element()*" select="$titres"/>
		</xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <!-- END HERE -->
				<!-- was added for fragment translation -->
<!--                <xsl:value-of select="'|'" />-->
<!--                <xsl:value-of select="$nouvPage" />-->
<!--                <xsl:value-of select="'|'" />-->
<!--                <xsl:value-of select="$nouvLigne" />-->
<!--                <xsl:value-of select="'|'" />-->
<!--                <xsl:value-of select="$numNoeud" />-->
<!--                <xsl:value-of select="'|'" />-->
            </xsl:otherwise>
        </xsl:choose>
</xsl:template>

<xsl:function name="doc:writeIndex" as="xs:string*">
	<xsl:param name="table" as="element()*" />
	<xsl:param name="separator" as="xs:string"/>
	<xsl:param name="startLigne" as="xs:integer"/>
	
	<xsl:choose>
		<xsl:when test="count($table) &gt; 1">
			<xsl:variable name="tree">
				<xsl:for-each select="$table">
					<xsl:call-template name="afficheItem">
						<xsl:with-param name="numPage" select="1" tunnel="yes"/>
						<xsl:with-param name="niveauItem" select="./@niveauOrig"/>
						<xsl:with-param name="numLigne" select="1" tunnel="yes"/>
					</xsl:call-template>
				</xsl:for-each>
			</xsl:variable>
			<xsl:variable name="resu" select="tokenize($tree, $sautAGenerer)"/>
			<xsl:variable name="tableMEF">
				<xsl:for-each select="$resu">
					<xsl:variable name="fin" select="tokenize(.,'&pt3;&pt3;&pt6;')"/>
					<xsl:choose>
						<xsl:when test="count($fin) &gt; 1">
							<xsl:variable name="listeNumbers" as="xs:string">
								<xsl:text>&pt3456;&pt16;&pt126;&pt146;&pt1456;&pt156;&pt1246;&pt12456;&pt1256;&pt246;</xsl:text>
							</xsl:variable>
							<xsl:variable name="libre" select="$longueur - string-length(.)"/>
							<xsl:value-of select="concat($fin[1],functx:repeat-string('&pt3;',$libre),'&pt3;&pt3;&pt6;',translate($fin[2],'0123456789',$listeNumbers))"/>
						</xsl:when>
						<xsl:otherwise><xsl:value-of select="."/></xsl:otherwise>
					</xsl:choose>
					<xsl:value-of select="$sautAGenerer"/>
					<xsl:if test="(position() + ($startLigne - 1)) mod $lignesParPage = 0"><xsl:value-of select="'&#12;'"/></xsl:if>
				</xsl:for-each>
				<xsl:value-of select="doc:sautePage(true(),($startLigne + count($resu)) mod $lignesParPage,0)" />
			</xsl:variable>
			<xsl:value-of select="translate($tableMEF,concat($sautAGenerer,$espace),'&#10;&pt;')" />
		</xsl:when>
		<xsl:otherwise/>
	</xsl:choose>
</xsl:function>

<!-- <xsl:template match="tableau">
	<xsl:value-of select="translate(.,concat($espace,$sautAGenerer,$carcoup),'&pt;&#10;')"/>
</xsl:template> -->

<xsl:template name="MEPliste">
<!-- ce template met en page les listes avec différents retraits possibles -->
	<xsl:param name="numPage" as="xs:integer" tunnel="yes"/>
	<xsl:param name="numLigne" as="xs:integer" tunnel="yes"/>
	<xsl:param name="niveauListe" as="xs:integer" select="1"/>
	
	<xsl:variable name="sautMepAeree" as="xs:string?">
		<xsl:if test="$modeLigneVide=3 and $niveauListe=1 and not(preceding-sibling::*[string(.)][1][self::titre or self::doc:ul or self::doc:ol or self::doc:dl])">
			<xsl:value-of select="if ($numLigne > 1 and $numLigne &lt;= $lignesParPage) then doc:sautAGenerer($numLigne,$numPage,0) else ''" /><!-- -->
		</xsl:if>
	</xsl:variable>
	<xsl:variable name="resultat">
		<xsl:for-each select="*[1]"> <!-- juste pour changer le contexte -->
			<xsl:if test="not(self::page-break)"><xsl:value-of select="translate($sautMepAeree,concat($espace,$sautAGenerer),'&pt;&#10;')" /></xsl:if>
			<xsl:choose>
				<xsl:when test="self::page-break">
					<xsl:value-of select="translate(doc:sautePage(true(),$numLigne,$numPage),concat($sautAGenerer,$espace),'&#10;&pt;')"/>
					<xsl:for-each select="following-sibling::*[1]"> <!-- juste pour changer le contexte -->
						<xsl:if test="self::doc:li">
							<xsl:call-template name="afficheItem">
								<xsl:with-param name="niveauItem" select="$niveauListe" />
								<xsl:with-param name="numLigne" select="1" tunnel="yes" />
								<xsl:with-param name="numPage" select="$numPage+1" tunnel="yes" />
							</xsl:call-template>
						</xsl:if>
						<xsl:if test="self::doc:ul or self::doc:ol or self::doc:dl">
							<xsl:call-template name="MEPliste">
								<xsl:with-param name="niveauListe" select="$niveauListe+1" />
								<xsl:with-param name="numLigne" select="1" tunnel="yes" />
								<xsl:with-param name="numPage" select="$numPage+1" tunnel="yes" />
							</xsl:call-template>
						</xsl:if>
					</xsl:for-each>
				</xsl:when>
				<xsl:when test="self::doc:li">
					<xsl:call-template name="afficheItem">
						<xsl:with-param name="niveauItem" select="$niveauListe" />
						<xsl:with-param name="numLigne" select="$numLigne + xs:integer(string-length($sautMepAeree)>0)" tunnel="yes" />
					</xsl:call-template>
				</xsl:when>
				<xsl:when test="self::doc:ul or self::doc:ol or self::doc:dl">
					<xsl:call-template name="MEPliste">
						<xsl:with-param name="niveauListe" select="$niveauListe+1" />
						<xsl:with-param name="numLigne" select="$numLigne + xs:integer(string-length($sautMepAeree)>0)" tunnel="yes" />
					</xsl:call-template>
				</xsl:when>
			</xsl:choose>
		</xsl:for-each>
	</xsl:variable>
	<xsl:value-of select="translate($resultat,concat($espace,$sautAGenerer),'&pt;&#10;')"/>
</xsl:template>

<!-- template affichant les listes et rappelant mepliste pour les listes imbriquées -->
<xsl:template name="afficheItem">
	<xsl:param name="numPage" as="xs:integer" tunnel="yes"/>
	<xsl:param name="numLigne" as="xs:integer" tunnel="yes"/>
	<xsl:param name="niveauItem" as="xs:integer" />
	<xsl:variable name="retraitMepCompacte" as="xs:integer"
		select="if (($modeLigneVide = (2,4) and not(../../self::doc:ul[@style='tableau'])) or 
		(string(ancestor::*[self::doc:ul or self::doc:ol or self::doc:dl][position()=last()]/preceding-sibling::*[1])) and $modeLigneVide = (0,1,5)) then 2 else 0" />
		<!-- 2 de plus pour retrait si pas de ligne vide avant -->
	<xsl:variable name="retraitSousListe" as="xs:integer" select="if (doc:ul|doc:ol|doc:dl) then 2 else 0" /><!-- 2 de plus pour retrait 2de ligne si sous-liste -->
	<xsl:variable name="retraitGeneral" as="xs:integer" select="xs:integer(1+2*($niveauItem -1))+$retraitMepCompacte" />
	<xsl:variable name="modeBraille" as="xs:string" 
		select="concat(string($retraitGeneral),'-',string($retraitGeneral + 2 + $retraitSousListe))" /><!-- calcul du retrait -->
	
	<!--<xsl:message select="$modeBraille" /> -->
	
	<xsl:for-each select="(*|text())[1]"><!-- car une liste peut avoir des sous-listes -->
		<xsl:choose>
			<xsl:when test="self::doc:ul or self::doc:ol or self::doc:dl">
				<xsl:call-template name="MEPliste">
					<xsl:with-param name="niveauListe" select="$niveauItem+1" />
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="self::text()"><!-- and string(translate(.,'&pt;&#10;&#13; &#9;','')) tout le reste est le traitement des noeud texte -->
				<xsl:variable name="pasVide" as="xs:boolean" select="string-length(translate(.,'&pt;&#10;&#13; &#9;','')) > 0" />
				<xsl:variable name="prefixe" as="xs:string?">
					<xsl:choose>
						<xsl:when test="../../self::doc:ul[@style='tableau'] or lower-case(../@styleOrig)= lower-case($poetryStyle)"/><!-- pas de puce affichée -->
						<xsl:when test="../../self::doc:ul"><xsl:text>&pt246;&pt135;&pt;</xsl:text></xsl:when>
						<xsl:when test="../../self::doc:ol  or ../../self::doc:dl">
							<!--<xsl:variable name="numComp">
								<xsl:call-template name="convNumListe" />
							</xsl:variable>
							<xsl:variable name="numero_liste">
								<xsl:number level="multiple" count="doc:li"	format="{$numComp}"/>
							</xsl:variable>
							<xsl:value-of select="concat('&pt6;',translate($numero_liste,'0123456789. ','&pt3456;&pt16;&pt126;&pt146;&pt1456;&pt156;&pt1246;&pt12456;&pt1256;&pt246;&pt256;&pt;'))" />-->
						</xsl:when>
						<!--table des matieres-->
						<xsl:when test="../self::titre"/>
						<xsl:otherwise><xsl:text>&pt36;&pt36;&pt;</xsl:text></xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="item_multi" as="xs:string*">
					<!-- concatenage puce et item -->
					<xsl:call-template name="coupure">
						<xsl:with-param name="phrase" select="tokenize(concat($prefixe,.),concat('&pt;|',$sautAGenerer))" />
						<xsl:with-param name="firstLine" select="true()" />
						<xsl:with-param name="modeBraille" select="$modeBraille" tunnel="yes"/>
						<xsl:with-param name="numPage" select="$numPage" tunnel="yes" />
						<xsl:with-param name="numLigne" select="$numLigne" tunnel="yes" />
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="item" as="xs:string" select="string-join($item_multi,'')" />
				
				<xsl:variable name="sousListeMulti" as="xs:string*">
					<xsl:if test="$pasVide"><!-- on n'affiche une puce et son noeud que si le noeud n'est pas vide -->
						<xsl:value-of select="$item" />
					</xsl:if>
					<xsl:if test="following-sibling::*[1][self::doc:ul or self::doc:ol or self::doc:dl]"><!-- cas d'une liste imbriquée précédée par un texte -->
						<xsl:for-each select="following-sibling::*[1]">
							<xsl:call-template name="MEPliste">
								<xsl:with-param name="niveauListe" select="$niveauItem+1" />
								<xsl:with-param name="numPage" select="if ($pasVide) then doc:nouvellePage($item,$numPage) else $numPage" tunnel="yes"/>
								<xsl:with-param name="numLigne" select="if ($pasVide) then doc:nouvelleLigne($item,$numLigne) else $numLigne" tunnel="yes"/>
							</xsl:call-template>
						</xsl:for-each>
					</xsl:if>
				</xsl:variable>
				
				<xsl:variable name="sousListe" select="string-join($sousListeMulti,'')"/>
				<xsl:variable name="sousListeNonVide" as="xs:boolean" select="string-length($sousListe) > 0"/>
				<xsl:if test="$sousListeNonVide"><xsl:value-of select="$sousListe"/></xsl:if>
				<xsl:if test="../following-sibling::*[1][self::doc:li]">
					<xsl:for-each select="../following-sibling::*[1]"> <!-- juste pour changer le contexte -->
						<xsl:call-template name="afficheItem">
							<xsl:with-param name="numPage" select="if ($sousListeNonVide) then doc:nouvellePage($sousListe,$numPage) else $numPage" tunnel="yes"/>
							<xsl:with-param name="numLigne" select="if ($sousListeNonVide) then doc:nouvelleLigne($sousListe,$numLigne) else $numLigne" tunnel="yes"/>
							<xsl:with-param name="niveauItem" select="$niveauItem" />
						</xsl:call-template>
					</xsl:for-each>
				</xsl:if>
			</xsl:when>
			<xsl:otherwise></xsl:otherwise>
		</xsl:choose>
	</xsl:for-each>
</xsl:template>
	
<xsl:template name="MEPtitre">
<!-- ce template affiche d'un coup tous les titres qui se suivent pour ne pas sauter de page après un titre -->
	<xsl:param name="numPage" as="xs:integer"/>
	<xsl:param name="numLigne" as="xs:integer" />
	
	<xsl:variable name="titresPreMisEnPage" as="xs:string*">
		<xsl:call-template name="recursionTitre">
			<xsl:with-param name="premier" select="true()" />
			<xsl:with-param name="debutPage" select="$numLigne = 1" />
		</xsl:call-template>
	</xsl:variable>
	
	<!-- on prend pas le dernier car comme l'expression se finit par un caractère délimiteur le dernier est une chaîne vide en trop
	en plus tokenize bug si on met un value-of select plutôt que le select direct dans la variable -->
	<xsl:variable name="titresMisEnPage" as="xs:string*" 
		select="tokenize(string-join($titresPreMisEnPage,''),$sautAGenerer)[position() &lt; last()]" />
	
	<xsl:choose>
		<xsl:when test="count($titresMisEnPage) > ($lignesParPage - xs:integer(not($numerosDePage='nn')))">
		<!-- cas particulier où tous les titres enchaînés sont plus longs qu'une page, donc on envoie direct sans saut de page -->
			<xsl:call-template name="envoieLignesBrut">
				<xsl:with-param name="lignes" select="$titresMisEnPage[not(.='ligneEnPlus')]" />
				<xsl:with-param name="numPage" select="$numPage"/>
				<xsl:with-param name="numLigne" select="$numLigne" />
			</xsl:call-template>
		</xsl:when>
		<xsl:when test="$numLigne = 1 ">
		<!-- on est en début de page, on checke le numéro de page puis on envoie -->
			<xsl:if test="$numerosDePage=('hs','hb') and not($numPage &lt; $startNumberingPage)"><!-- and not($numberFirstPage))">-->
				<!-- si on doit mettre un numéro de page en haut, on écrit rien sur cette ligne -->
				<xsl:value-of select="translate(doc:sautAGenerer(1,$numPage,0),concat($espace,$sautAGenerer),'&pt;&#10;')" />
			</xsl:if>
			<xsl:for-each select="$titresMisEnPage[not(.='ligneEnPlus')]">
				<!-- on vire les lignes vides avant le titre -->
				<xsl:variable name="pos" select="position()" />
				<xsl:if test="string(.) or exists($titresMisEnPage[(position() &lt; $pos) and string(.)])">
					<xsl:value-of select="translate(.,$espace,'&pt;')" />
					<xsl:text>&#10;</xsl:text>
				</xsl:if>
			</xsl:for-each>
		</xsl:when>
		<!-- si tout tient dans la page on envoie direct (-1 car on est déjà sur la ligne numligne) -->
		<xsl:when test="(count($titresMisEnPage) + $numLigne -1) &lt;= ($lignesParPage - $ligneEnMoins)">
			<xsl:for-each select="$titresMisEnPage[not(.='ligneEnPlus')]">
				<xsl:value-of select="translate(.,$espace,'&pt;')" />
				<xsl:text>&#10;</xsl:text>
			</xsl:for-each>
		</xsl:when>
		<!-- sinon on saute une page  -->
		<xsl:otherwise>
			<!-- <xsl:value-of select="concat ($numLigne,' ',count($titresMisEnPage),' ',$lignesParPage,' ',$ligneEnMoins)" /> -->
			<xsl:variable name="sautePage" as="xs:string*">
				<xsl:value-of select="doc:sautePage(true(),$numLigne,$numPage)"/>
				<!-- si le numero de page est hs ou hb alors on garde une ligne pour le numéro de page -->
				<xsl:if test="$numerosDePage='hs' or $numerosDePage='hb'">
					<xsl:value-of select="doc:sautAGenerer(1,$numPage+1,0)" />
				</xsl:if>
			</xsl:variable>
			<xsl:value-of select="translate(string-join($sautePage,''), concat($espace,$sautAGenerer),'&pt;&#10;')" />
			<xsl:for-each select="$titresMisEnPage[not(.='ligneEnPlus')]">
				<!-- on vire les lignes vides avant le titre -->
				<xsl:variable name="pos" select="position()" />
				<xsl:if test="string(.) or exists($titresMisEnPage[(position() &lt; $pos) and string(.)])">
					<xsl:value-of select="translate(.,$espace,'&pt;')" />
					<xsl:text>&#10;</xsl:text>
				</xsl:if>
			</xsl:for-each>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="MEPphrase">

	<xsl:param name="numPage" as="xs:integer"/>
	<xsl:param name="numLigne" as="xs:integer" />
	<xsl:param name="center" as="xs:string?" />
	<xsl:variable name="stringPhrase" select="translate(string-join(descendant::*[local-name(.)='braille'],$espace),' &#10;','')" as="xs:string"/>
	<xsl:message select="('Phrase à MEP:',$stringPhrase)"/>

	<xsl:variable name="phrase" select="if (@styleOrig=$stylesEnvers) then translate (functx:reverse-string($stringPhrase),
'&pt;&pt1;&pt12;&pt123;&pt1234;&pt12345;&pt123456;&pt12346;&pt1235;&pt12356;&pt1236;&pt124;&pt1245;&pt12456;&pt1246;&pt125;&pt1256;&pt126;&pt13;&pt134;&pt1345;&pt13456;&pt1346;&pt135;&pt1356;&pt136;&pt14;&pt145;&pt1456;&pt146;&pt15;&pt156;&pt16;&pt2;&pt23;&pt234;&pt2345;&pt23456;&pt2346;&pt235;&pt2356;&pt236;&pt24;&pt245;&pt2456;&pt246;&pt25;&pt256;&pt26;&pt3;&pt34;&pt345;&pt3456;&pt346;&pt35;&pt356;&pt36;&pt4;&pt45;&pt456;&pt46;&pt5;&pt56;&pt6;',
'&pt;&pt6;&pt56;&pt456;&pt3456;&pt23456;&pt123456;&pt13456;&pt2456;&pt12456;&pt1456;&pt356;&pt2356;&pt12356;&pt1356;&pt256;&pt1256;&pt156;&pt46;&pt346;&pt2346;&pt12346;&pt1346;&pt246;&pt1246;&pt146;&pt36;&pt236;&pt1236;&pt136;&pt26;&pt126;&pt16;&pt5;&pt45;&pt345;&pt2345;&pt12345;&pt1345;&pt245;&pt1245;&pt145;&pt35;&pt235;&pt1235;&pt135;&pt25;&pt125;&pt15;&pt4;&pt34;&pt234;&pt1234;&pt134;&pt24;&pt124;&pt14;&pt3;&pt23;&pt123;&pt13;&pt2;&pt12;&pt1;')
	else $stringPhrase" />
	
	<xsl:variable name="sautMepAeree" as="xs:string*">
		<xsl:if test="$modeLigneVide=3 and not(preceding-sibling::*[string(.)][1][self::titre]) and not(ancestor::m:mtable)">
			<!-- si numerosdepage='hs' on fait le saut mepaere que à partir de la liggggne > 2 -->
			<xsl:value-of select="if (($numLigne > (1 + xs:integer($numerosDePage='hs'))) and (($numLigne &lt;= $lignesParPage) or ($lignesParPage=0))) 
				then doc:sautAGenerer($numLigne,$numPage,0) else ''" />
		</xsl:if>
	</xsl:variable>
	<xsl:value-of select="translate($sautMepAeree,concat($espace,$sautAGenerer),'&pt;&#10;')" />
	
	<xsl:variable name="phraseSansEspaceMath" as="node()*">
		<xsl:choose>
			<xsl:when test="$center='true' and not(ancestor::m:mtable)">
			<!-- si la phrase est centrée, on fait comme un titre sauf qu'il n'y a pas 3 espaces à gauche et à droite de chaque ligne -->
				<xsl:variable name="nbLignesIdeal" as="xs:integer" select="xs:integer(ceiling(string-length(translate($phrase,concat($carcoup,$stopCoup),'')) div $longueur))" />
				<xsl:variable name="longueurIdeale" as="xs:integer" select="xs:integer(round(string-length(translate($phrase,concat($carcoup,$stopCoup),'')) div $nbLignesIdeal))"/>
				<xsl:variable name="phraseCentree" as="xs:string" select="string-join(doc:centreTitre(tokenize($phrase,'&pt;'), $longueurIdeale,$longueur,''),'')" />

				<!-- on vire des espace rajoutés par un bug de tokenize et les lignes vides nécessaires -->
				<xsl:variable name="lignesPhrase" as="xs:string*" select="tokenize(translate($phraseCentree,' ',''),$sautAGenerer)" />
				<xsl:variable name="lignesPhraseCentrees" as="xs:string*">
					<!-- insertion des lignes vides entre les lignes si double (ou plus) interligne -->
					<xsl:choose>
						<xsl:when test="$interligne &gt;0">
							<xsl:for-each select="$lignesPhrase">
								<xsl:value-of select="concat(functx:repeat-string('&pt;',xs:integer(floor(($longueur - string-length(.)) div 2))),.)" />
								<xsl:sequence select="for $i in 1 to $interligne return ''" />
							</xsl:for-each>
							<!--<xsl:message select="'double inter',$lignesPhrase[1],'taille',count($lignesPhrase)"/>-->
						</xsl:when>
						<xsl:otherwise>
							<xsl:sequence select="for $l in $lignesPhrase return
							concat(functx:repeat-string('&pt;',xs:integer(floor(($longueur - string-length($l)) div 2))),$l)" />
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:call-template name="envoieLignesBrut">
					<xsl:with-param name="lignes" select="$lignesPhraseCentrees" />
					<xsl:with-param name="numPage" select="$numPage" />
					<xsl:with-param name="numLigne" select="$numLigne + xs:integer(string-length($sautMepAeree)>0)"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="styleOrig" select="upper-case(@styleOrig)" />
				<xsl:variable name="modeStylist" select="$styles[upper-case(@name)=$styleOrig]/@mode" />
				<xsl:variable name="modeBraille" as="xs:string" select="if (@modeBraille != '') then @modeBraille else if ($modeStylist)
						then $modeStylist else '3-1'"/>
				<xsl:call-template name="coupure">
					<!-- ajout d'un espace en fin de phrase (il sera supprimé) pour ne pas avoir à tester partout dans coupure si le mot est le dernier de la phrase -->
					<xsl:with-param name="phrase" select="tokenize(string-join($phrase,''),concat('&pt;|',$sautAGenerer))"/>
					<xsl:with-param name="used" select="0"/>
					<xsl:with-param name="firstLine" select="true()"/>
					<xsl:with-param name="numPage" select="$numPage" tunnel="yes" />
					<xsl:with-param name="modeBraille" select="$modeBraille" tunnel="yes"/>
					<xsl:with-param name="numLigne" select="$numLigne + xs:integer(string-length($sautMepAeree)>0)" tunnel="yes" />
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	
	<!-- affichage définitif de la phrase avec un retour à la ligne à la fin -->
	<!--<xsl:if test="not(translate(string($phraseSansEspaceMath),concat($espace,'&pt;'),'')='')">-->
		<!--<xsl:element name="MepPhase">-->
		<xsl:for-each select="$phraseSansEspaceMath">
			<xsl:choose>
				<xsl:when test=". instance of text()">
					<xsl:value-of select="translate(string(.),concat($espace,$sautAGenerer),'&pt;&#10;')" />
				</xsl:when>
				<xsl:otherwise><!--text-->
					<xsl:copy-of select="." />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
				<!--<xsl:value-of select="translate(string($phraseSansEspaceMath),concat($espace,$sautAGenerer),'&pt;&#10;')" />-->
		<xsl:if test="not(nat:ends-with-any-of($phraseSansEspaceMath[position()=last()],($sautAGenerer,'&#12;','&#10;')))">
			<xsl:element name="layout:lineBreak"/>
			<!--<xsl:value-of select="'&#10;'" />-->
		</xsl:if>
		<!--</xsl:element>-->
		<!--	</xsl:if>-->
</xsl:template>
	
<xsl:template name="gestionLignesVides">
	<xsl:param name="numLigne" as="xs:integer" />
	<xsl:param name="numPage" as="xs:integer" />
	
	<!-- on met des lignes vides que jusqu'à la fin de la page -->
	<xsl:variable name="maxLignesVides" as="xs:integer" select="if ($lignesParPage=0) then 1 else $lignesParPage - $ligneEnMoins - $numLigne + 1" />

	<xsl:choose>
		<xsl:when test="$generatePageBreak and string-join(preceding-sibling::*[position() &lt; $minPageBreak],'')=''
			and string(following-sibling::*[1])">
			<xsl:value-of select="translate(doc:sautePage(true(),$numLigne,$numPage),concat($sautAGenerer,$espace),'&#10;&pt;')" />
		</xsl:when>
		<!-- mode 2 à 4 : suppression de toutes les lignes vides du doc d'origine 
		5 à rajouter si nécessaire -->
		<xsl:when test="$modeLigneVide=(2,3,4)"/>		
		<!-- si la phrase n'est pas vide (càd avec autre chose que des espaces) on l'affiche -->
		<xsl:when test="string(.)='' and $modeLigneVide=1">
			<xsl:choose>
				<!-- la phrase et la suivante sont vides: on supprime la ligne-->
				<xsl:when test="not(string(following-sibling::*[1]))"/>
				<!-- on est au début du document: on vire -->
				<!-- <xsl:when test="position()=1 "/> -->
				<!-- la phrase est vide mais pas la suivante -->
				<xsl:otherwise>
					<xsl:choose>
						<!-- le minimum pour mettre 3 lignes est atteint -->
						<xsl:when test="string-join(preceding-sibling::*[position() &lt; $min3ligne],'')=''">
							<xsl:value-of select="functx:repeat-string('&#10;', min ((3, $maxLignesVides)))"/>
						</xsl:when>
						<!-- le minimum pour mettre 2 lignes est atteint -->
						<xsl:when test="string-join(preceding-sibling::*[position() &lt; $min2ligne],'')=''">
							<xsl:value-of select="functx:repeat-string('&#10;', min ((2, $maxLignesVides)))"/>
						</xsl:when>
						<!-- le minimum pour mettre 1 ligne est atteint -->
						<xsl:when test="string-join(preceding-sibling::*[position() &lt; $min1ligne],'')=''">
							<xsl:value-of select="functx:repeat-string('&#10;', min ((1, $maxLignesVides)))"/>
						</xsl:when>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		<xsl:otherwise><!-- mode 0 = conserver les lignes du doc d'origine ou 5 aussi pour double interligne : 1 lv origine = 1 lv braille -->
			<xsl:value-of select="functx:repeat-string('&#10;', min ((1, $maxLignesVides)))"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- fonction de nouvelle page et nouvelle ligne après un envoi de texte -->
<xsl:function name="doc:nouvellePage" as="xs:integer">
	<xsl:param name="texte" as="xs:string" />
	<xsl:param name="numPage" as="xs:integer" />
	
	<!-- number-of-matches renvoie -1 quand texteMisEnPage est vide(bug)  -->
	<xsl:variable name="pagesEnPlus" as="xs:integer" select="functx:number-of-matches($texte,'&#12;')" />
	<xsl:value-of select="$numPage + xs:integer(number(string-length($texte)>0)*$pagesEnPlus)" />
</xsl:function>

<xsl:function name="doc:nouvelleLigne" as="xs:integer">
	<xsl:param name="texte" as="xs:string" />
	<xsl:param name="numLigne" as="xs:integer" />
	
	<!-- number-of-matches renvoie -1 quand $texte est vide(bug)  et substring-after-last ne marche pas pour une
	chaîne sur plusieurs lignes, d'où les deux variables suivantes et l'emploi feintu et bidouilleur de $sautAGenerer pour un autre role -->
	<xsl:variable name="lignesEnPlus" as="xs:integer" 
		select="functx:number-of-matches(functx:substring-after-last(translate($texte,'&#10;',$sautAGenerer),'&#12;'),$sautAGenerer)" />
	<xsl:choose>
		<xsl:when test="ends-with($texte,'&#12;')">
			<xsl:value-of select="1" /><!-- contournement de bug de substring-after-last -->
		</xsl:when>
		<xsl:when test="contains($texte,'&#12;')">
			<xsl:value-of select="$lignesEnPlus+1" /><!-- +1 car on est déjà au début de la prochaine ligne -->
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$numLigne+xs:integer(number(string-length($texte)>0)*$lignesEnPlus)" />
		</xsl:otherwise>
	</xsl:choose>
</xsl:function>

<!-- fonction de mise en ligne des titres avant centrage -->
<xsl:function name="doc:centreTitre" as="xs:string*">
	<xsl:param name="mots" as="xs:string*" />
	<xsl:param name="longueurIdeale" as="xs:integer" />
	<xsl:param name="longueurMax" as="xs:integer" />
	<xsl:param name="dejaFait" as="xs:string?" />
	
	<xsl:variable name="derniereLigne" as="xs:string" select='functx:substring-after-last($dejaFait,$sautAGenerer)' />
	<xsl:variable name="motCourant" as="xs:string" select="translate($mots[1],concat($carcoup,$stopCoup),'')" />
	<xsl:variable name="longueurIdealeCourante" as="xs:integer">
		<xsl:choose>
			<xsl:when test="($longueurIdeale + string-length($motCourant) + 1) &lt;= $longueurMax">
				<xsl:value-of select="xs:integer($longueurIdeale + round(string-length($motCourant) div 2))" />
			</xsl:when>
			<xsl:otherwise><xsl:value-of select="$longueurIdeale" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="derniereLigneAvecMot" as="xs:string" select="concat($derniereLigne,$espace,$motCourant)" />
	<xsl:variable name="finAvecMot" as="xs:string" select="translate(string-join($mots,$espace),concat($carcoup,$stopCoup),'')" />
	
	<xsl:variable name="motMisEnPage" as="xs:string">
		<xsl:choose>
			<!-- cas 0 : un mot plus long que la longueur max (idéale) -->
			<xsl:when test="string-length($motCourant) > $longueurMax">
				<xsl:choose>
					<!-- le mot rentre dans la longueur max (de la ligne), on le coupe pas -->
					<xsl:when test="string-length($motCourant) &lt;= $longueur">
						<xsl:variable name="motSansCoupe">
							<xsl:if test="string-length($derniereLigne) > 0"><xsl:value-of select="$sautAGenerer" /></xsl:if>
							<xsl:value-of select="$motCourant" />
						</xsl:variable>
						<xsl:value-of select="string-join($motSansCoupe,'')" />
					</xsl:when>
					<!-- sinon mode sagouin et zut -->
					<xsl:otherwise>
						<xsl:variable name="motSansCoupe" select="substring($motCourant,1,
								$longueurMax - string-length($derniereLigne)-1-xs:integer(string-length($derniereLigne) > 0))" />
						<!--<xsl:message select="$motSansCoupe" />-->
						<!--<xsl:message select="$longueurMax - string-length($derniereLigne)-1" />-->
						<xsl:variable name="motCoupeSagouin">
							<xsl:if test="string-length($derniereLigne) > 0">
								<xsl:value-of select="$espace" />
							</xsl:if>
							<xsl:value-of select="$motSansCoupe" /><!-- -->
							<xsl:if test="string-length($motSansCoupe) > 0">
								<xsl:choose>
									<xsl:when test="starts-with($mots[1],$debMath)"><xsl:text>&pt5;</xsl:text></xsl:when>
									<xsl:otherwise><xsl:text>&pt36;</xsl:text></xsl:otherwise>
								</xsl:choose>
							</xsl:if>
							<xsl:value-of select="$sautAGenerer" />
						</xsl:variable>
						<xsl:variable name="motCoupeSagouin2">
							<xsl:value-of select="$motCoupeSagouin" />
							<xsl:value-of select="doc:centreTitre(substring-after($motCourant,$motSansCoupe), 
								$longueurIdeale, $longueurMax, concat(functx:trim($dejaFait),functx:trim(string-join($motCoupeSagouin,''))))" />
						</xsl:variable>
						<xsl:value-of select="string-join($motCoupeSagouin2,'')" />
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<!-- cas 1 : on est en début de ligne -->
			<xsl:when test="string-length($derniereLigne)=0">
				<xsl:value-of select="$motCourant" />
			</xsl:when>
			<!-- cas 2 : le mot rentre dans la ligne de (longueur ideale + moitie longueur mot courant) -->
			<xsl:when test="string-length($derniereLigneAvecMot) &lt;= $longueurIdealeCourante">
				<xsl:value-of select="concat($espace,$motCourant)" />
			</xsl:when>
			<!-- cas 3 : faut passer à la ligne -->
			<xsl:otherwise>
				<xsl:value-of select="concat($sautAGenerer,$motCourant)" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	
	<xsl:value-of select="functx:trim($motMisEnPage)" />
	
	<xsl:if test="(count($mots) > 1) and not($motMisEnPage = concat($espace,$finAvecMot))">
		<xsl:value-of select="doc:centreTitre($mots[position() > 1], $longueurIdeale, $longueurMax,	concat(functx:trim($dejaFait),functx:trim($motMisEnPage)))" />
	</xsl:if>
</xsl:function>

<!-- template recursive pour mettre en page tout un bloc de titres ; on met pas de numero de page sur la même ligne qu'un titre -->
<xsl:template name="recursionTitre" as="xs:string*">
	<xsl:param name="premier" as="xs:boolean" select="false()" />
	<xsl:param name="debutPage" as="xs:boolean" select="false()" />
	<xsl:param name="ligneVideAvant" as="xs:boolean" select="false()" />
	
	<xsl:if test="string(.)"> <!-- on ne fait ça que pour les noeud non vides -->
		<xsl:if test="not($debutPage) and $premier">
		<!-- on ne met les lignes vides avant le titre que si c'est le premier titre qu'on traite -->
			<xsl:value-of select="$sautConditionnel" /><!-- 1 ligne vide avant si titre 2 a 5-->
			<xsl:if test="@niveauBraille=1">
				<xsl:value-of select="$sautConditionnel" /><!-- deux lignes vides avant si titre 1 -->
			</xsl:if>
		</xsl:if>

		<xsl:choose>
			<xsl:when test="not(string(.))" /> <!-- noeud vide dans le cas par exemple d'une phrase vide entre deux titres -->
			<xsl:when test="@niveauBraille=1 or @niveauBraille=2">
			<!-- titres centrés avec 1 ou 2 lignes vides avant, 1 ou 2 lignes vides après et 2 lignes mini de braille ensuite -->
				<xsl:variable name="longueur2" as="xs:integer" select="$longueur - 6" /> <!-- car 3 chars mini à gauche et à droite -->
				<xsl:variable name="nbLignesIdeal" as="xs:integer" select="xs:integer(ceiling(string-length(translate(.,concat($carcoup,$stopCoup),'')) div $longueur2))" />
				<xsl:variable name="longueurIdeale" as="xs:integer" select="xs:integer(round(string-length(translate(.,concat($carcoup,$stopCoup),'')) div $nbLignesIdeal))"/>
				<xsl:variable name="titreMisEnPage" as="xs:string" select="string-join(doc:centreTitre(tokenize(.,'&pt;'), $longueurIdeale,$longueur2,''),'')" />
				
				<!-- affichage du titre -->
				<!-- on vire des espace rajoutés par un bug de tokenize et les lignes vides nécessaires -->
				<xsl:variable name="lignesTitre" as="xs:string*" select="tokenize(translate($titreMisEnPage,' ',''),$sautAGenerer)" />
				<xsl:for-each select="$lignesTitre">
					<xsl:value-of select="functx:repeat-string('&pt;',3 + xs:integer(floor(($longueur2 - string-length(.)) div 2)))" />
					<xsl:value-of select="concat(.,functx:repeat-string($sautAGenerer,$interligne+1))" />
				</xsl:for-each>
				<!-- <xsl:value-of select="concat('nbligideal ',$nbLignesIdeal,' longideale ',$longueurIdeale)" /> -->
				<xsl:value-of select="$sautConditionnel" /><!-- 1 ligne vide après si titre 2 ou titre 1 suivi d'un autre titre -->
				<xsl:if test="not(following-sibling::*[string(.)][1][self::titre])">
					<xsl:if test="@niveauBraille=1">
						<xsl:value-of select="$sautConditionnel" /><!-- 2 lignes vides pour niv 1 -->
					</xsl:if>
					<xsl:value-of select="concat('ligneEnPlus',$sautAGenerer,'ligneEnPlus',$sautAGenerer)" /><!-- 2 lignes braille en plus -->
				</xsl:if>
			</xsl:when>
			<xsl:otherwise> <!-- titres niveau 345 -->
				<xsl:if test="not($ligneVideAvant) and not($premier)">
					<xsl:value-of select="$sautConditionnel" /><!-- ligne vide avant titre si pas de titre niv 1 ou 2 avant -->
				</xsl:if>
				<xsl:call-template name="coupure">
					<xsl:with-param name="phrase" select="tokenize(.,concat('&pt;|',$sautAGenerer))" />
					<xsl:with-param name="firstLine" select="true()" />
					<xsl:with-param name="modeBraille" tunnel="yes">
						<xsl:choose>
							<xsl:when test="@niveauBraille=3"><xsl:value-of select="'5'" /></xsl:when>
							<xsl:when test="@niveauBraille=4"><xsl:value-of select="'1'" /></xsl:when>
							<xsl:when test="@niveauBraille=5"><xsl:value-of select="'3-1'" /></xsl:when>
							<xsl:otherwise><xsl:value-of select="'3-1'" /></xsl:otherwise>
						</xsl:choose>
					</xsl:with-param>
					<xsl:with-param name="numPage" select="0" tunnel="yes" />
					<xsl:with-param name="numLigne" select="-1000" tunnel="yes" />
					<!-- valeurs bidon pour que coupure coupe pas les pages -->
				</xsl:call-template>
				<xsl:value-of select="concat('ligneEnPlus',$sautAGenerer)" /><!-- 1 ligne braille en plus -->
			</xsl:otherwise>
		</xsl:choose>
		
		<!-- <xsl:value-of select="concat(.,' suivi de ',count(following-sibling::*[string(.)][1][self::titre]),$sautAGenerer)" /> -->
	</xsl:if>
	
	<!-- on quitte le for-each pour rappeler le template sinon le contexte est changé -->
	<xsl:if test="following-sibling::*[string(.)][1][self::titre] and $sautConditionnel">
		<!-- s'il y a conservation des lignes vides on fait pas de récursion -->
		<xsl:variable name="nivBraille" select="@niveauBraille" />
		<xsl:for-each select="following-sibling::*[1]"><!-- changement de contexte -->
			<xsl:call-template name="recursionTitre">
				<xsl:with-param name="ligneVideAvant" as="xs:boolean" 
					select="($nivBraille=(1,2) or $ligneVideAvant) and not($nivBraille=(3,4,5))" />
			</xsl:call-template>
		</xsl:for-each>
	</xsl:if>
</xsl:template>

<!-- template pour envoyer des lignes sur des pages avec controle des numeros mais pas
sur la même ligne. Sert pour les titres et pourra servir pour les tableaux 
SI NBLIGNESPARPAGES=0, on s'en fout des sauts de page -->
<xsl:template name="envoieLignesBrut">
	<xsl:param name="lignes" as="xs:string*" select="''" />
	<xsl:param name="numPage" as="xs:integer"/>
	<xsl:param name="numLigne" as="xs:integer" />
	
	<xsl:choose>
		<xsl:when test="($numLigne > $lignesParPage) and ($lignesParPage > 0)">
		<!-- ce cas produit assez rarement mais ça arrive -->
			<xsl:value-of select="doc:sautePage(false(),0,0)" />
			<xsl:call-template name="envoieLignesBrut">
				<xsl:with-param name="lignes" select="$lignes" />
				<xsl:with-param name="numPage" select="$numPage + 1"/>
				<xsl:with-param name="numLigne" select="1" />
			</xsl:call-template>
		</xsl:when>
		<xsl:when test="($lignesParPage > 0) and (($numLigne=1 and substring($numerosDePage,1,1)='h')
			or ($numLigne=$lignesParPage and substring($numerosDePage,1,1)='b'))">
			<!-- gestion des numéros de page -->
			<xsl:value-of select="translate(doc:sautAGenerer($numLigne, $numPage, 0),concat($espace,$sautAGenerer),'&pt;&#10;')" />
			<xsl:if test="$numLigne=$lignesParPage"><xsl:value-of select="doc:sautePage(false(),0,0)" /></xsl:if>
			<xsl:call-template name="envoieLignesBrut">
				<xsl:with-param name="lignes" select="$lignes" />
				<xsl:with-param name="numPage" select="$numPage + xs:integer($numLigne>=$lignesParPage)"/>
				<xsl:with-param name="numLigne" select="1+xs:integer($numLigne*number($numLigne&lt;$lignesParPage))" />
			</xsl:call-template>
		</xsl:when>
		<xsl:otherwise>
			<!-- cas général -->
			<xsl:value-of select="translate(concat($lignes[1],$sautAGenerer),concat($espace,$sautAGenerer),'&pt;&#10;')" />
			<xsl:if test="$numLigne=$lignesParPage"><xsl:value-of select="doc:sautePage(false(),0,0)" /></xsl:if>
			<xsl:if test="count($lignes) > 1">
				<xsl:call-template name="envoieLignesBrut">
					<xsl:with-param name="lignes" select="$lignes[position()>1]" />
					<xsl:with-param name="numPage" select="$numPage + xs:integer($numLigne=$lignesParPage)"/>
					<xsl:with-param name="numLigne" select="1+xs:integer($numLigne*number($numLigne&lt;$lignesParPage))"/>
				</xsl:call-template>
			</xsl:if>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- template de mise en page de la musique; juste pour avoir un 
     affichage correct de la musique, pas de prise en compte des 
     pages et de leur dimension-->

<xsl:template name="MEPmusique">
  <xsl:param name="numPage" as="xs:integer" />
  <xsl:param name="numLigne" as="xs:integer" />
  

  <!-- title -->
  <xsl:for-each select="titre">
    <xsl:value-of select="concat('&pt;&pt;',.)"/>
    <xsl:value-of select="'&#xA;'"/>
  </xsl:for-each>

  <!-- part --> 
  <xsl:for-each select="part">

    <!-- music -->
    <xsl:variable name="partHasLyrics" select="phrase" />        

    <xsl:if test="$partHasLyrics">
      <xsl:value-of select="concat('&pt6;','&pt3;')"/>
    </xsl:if>    

    <xsl:for-each select="./*">
      <xsl:if test="self::measure"> 
	<xsl:value-of select="."/>
      </xsl:if>
    </xsl:for-each>
    
    <xsl:value-of select="'&#xA;'"/>
    
    <!-- lyrics -->
    <xsl:value-of select="concat('&pt56;','&pt23;')"/>
    <xsl:for-each select="phrase"> 
      <xsl:for-each select="./*">
<!--	<xsl:message>
	  <xsl:variable name="nextIsEnd" select="parent::self"/>
	  <xsl:copy-of select="$nextIsEnd"/>
	</xsl:message>-->
	<xsl:choose>
	  <xsl:when test="self::syl" >
	    <xsl:copy-of select="."/>
	  </xsl:when>
	  <xsl:when test="self::end" >
	    <xsl:copy-of select="'&#xA;'"/>
	  </xsl:when>
	</xsl:choose>
      </xsl:for-each>
      <xsl:value-of select="'&#xA;'"/>
    </xsl:for-each>
    
  </xsl:for-each>    
 


</xsl:template>

<!-- conversion des numéros de listes
<xsl:template name="convNumListe" as="xs:string">
	<xsl:variable name="numComplet" as="xs:string*">
		<xsl:for-each select="ancestor::ol">
			<xsl:value-of select="functx:replace-multi(@list-style-type,
				('decimal','lower-roman','upper-roman','lower-alpha','upper-alpha','lower-latin','upper-latin'),('1','i','I','a','A','a','A'))"/>
		</xsl:for-each>
	</xsl:variable>
	<xsl:value-of select="string-join(($numComplet,'&pt;'),'.')"/>
</xsl:template>  -->
</xsl:stylesheet>
