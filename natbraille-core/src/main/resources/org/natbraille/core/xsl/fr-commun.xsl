<?xml version='1.1' encoding="UTF-8" ?>
<!--
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
-->

<!-- version: 1.3-b3 -->

<!DOCTYPE xsl:stylesheet SYSTEM "nat://system/xsl/mmlents/windob.dtd">


<xsl:stylesheet version="3.0" xmlns:xsl='http://www.w3.org/1999/XSL/Transform' 
xmlns:saxon='http://icl.com/saxon'
xmlns:xs='http://www.w3.org/2001/XMLSchema'
xmlns:fn='http://www.w3.org/2005/xpath-functions'
xmlns:fo='http://www.w3.org/1999/XSL/Format'
xmlns:m='http://www.w3.org/1998/Math/MathML'
xmlns:functx='http://www.functx.com'
xmlns:lit='espacelit'
xmlns:doc='espaceDoc'
xmlns:nat='http://natbraille.free.fr/xsl'
expand-text="yes">

<xsl:output method="xml" encoding="UTF-8" indent="yes"/>

<xsl:template match="ponctuation[@type='puce']">
	<xsl:element name="bullet">{$listPuce}</xsl:element>	
</xsl:template>

<xsl:template match="ponctuation[@type='ordre']">
	<xsl:element name="pref"><xsl:attribute name="type" select="'num'"/>{$numPref}</xsl:element>
	<xsl:call-template name="bijection">
		<xsl:with-param name="mot" select="@num" />
	</xsl:call-template>
	<xsl:element name="ordered">{$listOrdered}</xsl:element>		
</xsl:template>

<xsl:template match="ponctuation">
	<xsl:element name="punctuation">
		<xsl:copy-of select="@*"/>
		<xsl:element name="black">
			<xsl:value-of select="."/>
		</xsl:element>
		<xsl:element name="braille">
			<xsl:choose>
				<xsl:when test=".='-'">
					<!-- est-ce le signe mathématique - ou un tiret? -->
					<xsl:choose>
						<!-- il y a des numériques avant et/ou après, ou il y a d'autres opérateurs mathématiques (+ ou = ) dans la phrase-->
						<xsl:when test="(number(translate(string(preceding-sibling::*[1]),',0','.1')) or number(translate(string(following-sibling::*[1]),',0','.1'))) or contains(translate(string-join((preceding-sibling::*[position() &lt; 3],following-sibling::*[position() &lt; 3]),''),'+','='),'=')"><!-- string(..), ça fait trop --><!-- conversion des , en . et des 0 en 1 car number(0) et number(NNN,NNN) renvoie faux -->
							<xsl:value-of select="$numPref"/>
						</xsl:when>
						<xsl:otherwise><!-- test=".=../child::*[1]">-->
							<xsl:value-of select="$hyphenBr"/>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:value-of select="$hyphenBr"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:variable name="punctM" select="functx:replace-multi(.,$punctNbl,$punctNbr)"/>
					<xsl:value-of select="translate($punctM,$punct1bl,$punct1br)"/>
				</xsl:otherwise>
			</xsl:choose><!--
			<xsl:if test="not (local-name(following::*[1])='phrase') and not(contains('([{«“‘&lsquo;',.)) and not(local-name(following::*[1])='ponctuation')"> RAF:à améliorer pour les guillemets et apos ouvrantssimples 
				<xsl:text> </xsl:text>
			</xsl:if>-->
			<xsl:if test="not(local-name(following::*[1])='phrase' ) and not(contains($noSpacePunctEnd,.)) and not(local-name(following-sibling::*[1])='ponctuation' and not(contains($noSpacePunctOpen,.)) and following-sibling::*[1]='&quot;')"><!-- RAF:à améliorer pour apos ouvrantssimples -->
				<xsl:call-template name="espace"/>
			</xsl:if>
		</xsl:element>
	</xsl:element>
</xsl:template>

<xsl:template match="mot">
	<xsl:element name="word">
		<xsl:attribute name="fragmentId" select="@fragmentId"/>
		<xsl:attribute name="source" select="'black'"/>
		<xsl:element name="black">
			<xsl:value-of select="if(@ori) then @ori else ." />
		</xsl:element>
		<xsl:element name="info">
			<xsl:copy-of select="./*|@*|text()" />
		</xsl:element>
		<!-- TODO Bruno, voir si doit rester-->
		<!--<xsl:if test="syll/ambiguity">
			<xsl:element name="mot_syll">
				<xsl:copy-of select="*"/>
			</xsl:element>-->
		<!--</xsl:if>-->
		<xsl:element name="braille">
			<xsl:attribute name="fragmentId" select="@fragmentId"/>
			<xsl:if test="string-length(.) > 0">
				<!-- ****************** Exposant? *************************-->
				<xsl:if test="@hauteur = 'exp' and not(@appel)">
					<xsl:element name="prefixe">
						<xsl:attribute name="type" select="'exp'"/>
						<xsl:value-of select="$expPref"/>
					</xsl:element>
				</xsl:if>
				<!-- note call -->
				<xsl:if test="@appel">
					<xsl:element name="note_call">
						<xsl:value-of select="concat($espace,$noteCall)"/>
					</xsl:element>
				</xsl:if>
				<!-- indice -->
				<xsl:if test="@hauteur = 'ind'">
					<xsl:element name="prefixe">
						<xsl:attribute name="type" select="'ind'"/>
						<xsl:value-of select="$indPref"/>
					</xsl:element>
				</xsl:if>
				
				<xsl:variable name="afterMev" select="nat:doMevPrefixe(.)" as="element()*"/><!--TODO NBA: create function to tag mev sequence-->
				<!-- suppression des ligatures -->
				<xsl:variable name="deLigature" select="functx:replace-element-values($afterMev,for $p in $afterMev return functx:replace-multi($p,$ligatures,$sansLigatures))" as="element()*"/>

				<xsl:choose>
					<!-- mot vide: pas de traitement -->
					<xsl:when test="string-length(normalize-space(.))=0"/>
					<xsl:otherwise>
					<xsl:variable name="end" as="element()* " select=". => nat:tagContent('[-\+]?[0-9]+(,[0-9]+)?',$numMap,fn:function-lookup(xs:QName('xs:string'),1)) => nat:tagContent('[A-Z]+',$capsMap,fn:function-lookup(xs:QName('fn:lower-case'),1))=> nat:ivb()"/>
					<xsl:variable name="befSComp">
						<xsl:for-each select="$end">
							<xsl:choose><!--TODO diff entre syll et mot; conserver syll et origine de syll-->
								<xsl:when test="local-name(.)='mot'">
									<!--<xsl:element name="{local-name(.)}">
										<xsl:copy-of select="./@*"/>-->
										<xsl:call-template name="symbolesComposes">
											<xsl:with-param name="mot" select="string(.)"/>
											<xsl:with-param name="PN" select="-10"/>
										</xsl:call-template>
									<!--</xsl:element>-->
								</xsl:when>
								<xsl:when test="local-name(.)='syll'">
									<xsl:element name="{local-name(.)}">
										<xsl:copy-of select="./@*"/>
										<xsl:attribute name="ori" select="."/>
										<xsl:call-template name="symbolesComposes">
											<xsl:with-param name="mot" select="string(.)"/>
											<xsl:with-param name="PN" select="-10"/>
										</xsl:call-template>
									</xsl:element>
								</xsl:when>
								<xsl:otherwise>
									<xsl:copy-of select="."/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</xsl:variable>
					<xsl:copy-of select="$befSComp"/>
					<xsl:message select="$end" terminate="no"/>
					<!--NUMERIQUE -->
						<!--MAJUSCULE-->
						</xsl:otherwise>
				</xsl:choose>
				
				
			</xsl:if><!-- du if du tout départ car si le mot est vide on fait rien -->
		</xsl:element>
	</xsl:element>
</xsl:template>



<xsl:template name="finBijection">
	<xsl:param name="mot" as="xs:string"/>
	<xsl:param name="PN" as="xs:integer" select='-10'/>
	<xsl:choose>
		<!-- ajout de règles de coupure -->
		<xsl:when test="contains($mot, '@')">
			<xsl:call-template name="bijection">
				<xsl:with-param name="mot" select="substring-before($mot,'@')"/>
			</xsl:call-template>
			<xsl:text>&pt345;</xsl:text><!--<xsl:value-of select='$coupe'/>-->
			<xsl:call-template name="bijection">
				<xsl:with-param name="mot" select="substring-after($mot,'@')"/>
			</xsl:call-template>			
		</xsl:when>
		<xsl:when test="contains($mot, '/')">
			<xsl:call-template name="bijection">
				<xsl:with-param name="mot" select="substring-before($mot,'/')"/>
			</xsl:call-template>
			<xsl:text>&pt34;</xsl:text><!--<xsl:value-of select='$coupe'/>-->
			<xsl:call-template name="bijection">
				<xsl:with-param name="mot" select="substring-after($mot,'/')"/>
			</xsl:call-template>
		</xsl:when>
		<xsl:when test='$mot="&apos;"'>
			<xsl:text><!--&pt2356;-->&pt3;</xsl:text>
		</xsl:when>
		<xsl:otherwise>
			<!-- utilisation d'une variable (pb des '")-->
			<!-- NEW XSL2 : '' ou "" permet d'obtenir le symbole choisi sans pb -->
			<xsl:variable name="tousLesSymboles" as="xs:string">
				<xsl:value-of select="concat('ÁÍÓÚÑÌÒÄÖáíóúñìòäöABCDEFGHIJKLMNOPQRSTUVWXYZÀÂÉÈÊËÎÏÔÙÛÜ&Ccedil;&AElig;&OElig;abcdefghijklmnopqrstuvwxyz0123456789àâéèêëîïôùûüçæœ@+-×÷=.,&nbsp;/&#8209;’‘:',$apos)"/>
			</xsl:variable>
			
			<xsl:variable name="v2">
				<!-- <xsl:choose>
					là, c'est vraiment se casser la tête pour pas grand chose, c'est vrai... 
					Fred : d'ailleurs je le mets en commentaire pour optimisation
					<xsl:when test="ancestor::*[@lang='es']">
						<xsl:text>&pt12356;&pt34;&pt346;&pt23456;&pt12456;&pt34;&pt346;&pt345;&pt246;&pt12356;&pt34;&pt346;&pt23456;&pt12456;&pt34;&pt346;&pt345;&pt246;&pt1;&pt12;&pt14;&pt145;&pt15;&pt124;&pt1245;&pt125;&pt24;&pt245;&pt13;&pt123;&pt134;&pt1345;&pt135;&pt1234;&pt12345;&pt1235;&pt234;&pt2345;&pt136;&pt1236;&pt2456;&pt1346;&pt13456;&pt1356;&pt12356;&pt16;&pt2346;&pt2346;&pt126;&pt1246;&pt146;&pt12456;&pt1456;&pt23456;&pt156;&pt1256;&pt12346;&pt345;&pt246;&pt1;&pt12;&pt14;&pt145;&pt15;&pt124;&pt1245;&pt125;&pt24;&pt245;&pt13;&pt123;&pt134;&pt1345;&pt135;&pt1234;&pt12345;&pt1235;&pt234;&pt2345;&pt136;&pt1236;&pt2456;&pt1346;&pt13456;&pt1356;&pt3456;&pt16;&pt126;&pt146;&pt1456;&pt156;&pt1246;&pt12456;&pt1256;&pt246;&pt12356;&pt16;&pt2346;&pt2346;&pt126;&pt1246;&pt146;&pt12456;&pt1456;&pt23456;&pt156;&pt1256;&pt12346;&pt345;&pt246;&pt345;&pt235;&pt36;&pt35;&pt25;&pt2356;&pt256;&pt2;&pt;&pt34;&pt36;&pt3;&pt3;&pt25;&pt3;</xsl:text>
						: &#8209; = trait d'union insécable 
					</xsl:when>
					<xsl:otherwise> -->
						<xsl:text>&pt12356;&pt34;&pt346;&pt23456;&pt12456;&pt34;&pt346;&pt345;&pt246;&pt12356;&pt34;&pt346;&pt23456;&pt12456;&pt34;&pt346;&pt345;&pt246;&pt1;&pt12;&pt14;&pt145;&pt15;&pt124;&pt1245;&pt125;&pt24;&pt245;&pt13;&pt123;&pt134;&pt1345;&pt135;&pt1234;&pt12345;&pt1235;&pt234;&pt2345;&pt136;&pt1236;&pt2456;&pt1346;&pt13456;&pt1356;&pt12356;&pt16;&pt123456;&pt2346;&pt126;&pt1246;&pt146;&pt12456;&pt1456;&pt23456;&pt156;&pt1256;&pt12346;&pt345;&pt246;&pt1;&pt12;&pt14;&pt145;&pt15;&pt124;&pt1245;&pt125;&pt24;&pt245;&pt13;&pt123;&pt134;&pt1345;&pt135;&pt1234;&pt12345;&pt1235;&pt234;&pt2345;&pt136;&pt1236;&pt2456;&pt1346;&pt13456;&pt1356;&pt3456;&pt16;&pt126;&pt146;&pt1456;&pt156;&pt1246;&pt12456;&pt1256;&pt246;&pt12356;&pt16;&pt123456;&pt2346;&pt126;&pt1246;&pt146;&pt12456;&pt1456;&pt23456;&pt156;&pt1256;&pt12346;&pt345;&pt246;&pt345;&pt235;&pt36;&pt35;&pt25;&pt2356;&pt256;&pt2;&pt;&pt34;&pt36;&pt3;&pt3;&pt25;&pt3;</xsl:text>
						<!-- ne rien mettre pour &nbsp-->
					<!-- </xsl:otherwise>
				</xsl:choose> -->
			</xsl:variable>
			<xsl:value-of select="translate($mot,$tousLesSymboles,$v2)" />
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>
</xsl:stylesheet>
