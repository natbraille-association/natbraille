﻿<?xml version='1.1' encoding="UTF-8" ?>
<!--
* NAT - An universal Translator
* Copyright (C) 2005 Bruno Mascret
* Contact: bmascret@free.fr

* former authors: Frédéric Schwebel, Bruno Mascret, Ouarda Yassa
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
-->

<!-- TODO : virer les mrow qui ont un seul child et simplifier fr-maths
MAIS PAS AVANT UN JEU DE TESTS DE NON-REGRESSION  -->

<!DOCTYPE xsl:stylesheet SYSTEM "nat://system/xsl/mmlents/windob.dtd">

<xsl:stylesheet version="2.0"
xmlns:xsl='http://www.w3.org/1999/XSL/Transform' 
xmlns:saxon='http://icl.com/saxon'
xmlns:m='http://www.w3.org/1998/Math/MathML'
xmlns:xs='http://www.w3.org/2001/XMLSchema'
xmlns:fn='http://www.w3.org/2005/xpath-functions'
xmlns:doc='espaceDoc'
xmlns:functx='http://www.functx.com'>

	<xsl:import href="nat://system/xsl/functions/functx.xsl" />
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	
<!-- ******************** Phase de preprocessing ****************** -->

<xsl:variable name="functionList1" as="xs:string">
	<xsl:value-of select="'|sin|cos|tan|cot|cotan|arcsin|arccos|arctan|arccotan|arccot|sec|csc|sinh|cosh|th|tanh|coth|arcsinh|arccosh|arctanh|arccoth|sech|csch|ln|log|sh|ch|exp|Card|arg|'" />
</xsl:variable>

<xsl:variable name="flCarreCube" as="xs:string*">
	<xsl:variable name="flToken" as="xs:string*" select="tokenize($functionList1,'\|')[string(.)]"/>
	<xsl:for-each select="$flToken">
		<xsl:value-of select="concat(.,'&sup2;')"/>
		<xsl:value-of select="concat(.,'&sup3;')"/>
	</xsl:for-each>
</xsl:variable>

<xsl:variable name="functionList" as="xs:string">
	<xsl:value-of select="concat($functionList1,string-join($flCarreCube,'|'),'|')"/>
</xsl:variable>

<xsl:variable name="trigoList" as="xs:string">
	<xsl:value-of select="'|sin|cos|tan|cot|cotan|arcsin|arccos|arctan|arccotan|arccot|'" />
</xsl:variable>

<xsl:variable name="noBlocList" as="xs:string">
	<!-- les mfrac mettent leur bloc elles-mêmes et les mover (angles) pas besoin -->
	<xsl:value-of select="'|mfrac|mover|'" />
</xsl:variable>

<xsl:variable name="blocList" as="xs:string">
	<xsl:value-of select="'|msub|msup|msqrt|mroot|mover|munder|munderover|msubsup|'" />
</xsl:variable>

<xsl:variable name="l_alphanumgrec_ext" as="xs:string">
	<xsl:value-of select="concat($l_alphanumgrec,'°&prime;''&deg;')"/>
</xsl:variable>

<!-- liste de tous les noeuds de fonctions du document -->
<xsl:variable name="listeFonctions" select="//*[(self::m:mo|self::m:mi)[fn:contains($functionList,concat('|',string(.),'|'))]]" />

<!-- transfo identité par défaut avec controle pour fctions -->
<xsl:template match="@*|*|text()|processing-instruction()" priority="-1" mode="phase1">
	<xsl:copy>
		<xsl:apply-templates select="@*|*|text()|processing-instruction()" mode="phase1" />
	</xsl:copy>
</xsl:template>

<!--<xsl:template match="//m:mi" priority="5" mode="phase1">-->
<!--<xsl:if test="contains(.,'°')"><xsl:message select="concat(local-name(.),' ',string(.))" /></xsl:if>
	<xsl:next-match />
</xsl:template>-->

<!-- on vire les annotations -->
<xsl:template match="m:annotation" mode="phase1">
</xsl:template>

<!-- on vire les mrow inutiles (dont le parent est mrow)  de openoffice et de mathtype(un peu)-->
<xsl:template match="m:mrow[parent::*[self::m:mrow and count(*)=1]]" priority="0" mode="phase1">
	<xsl:apply-templates select="@*|*|text()|processing-instruction()" mode="phase1" />
</xsl:template>

<!-- bruno 26/11/18 ajout de mrow dans le cas des bloclist si il y a plusieurs fils et pas un simple mrow -->
<xsl:template match="m:msqrt" priority="1" mode="phase1"><!-- m:msub|m:msup|m:msqrt|m:mroot|m:mover|m:munder|m:munderover|m:msubsup-->
	<xsl:copy>
		<xsl:choose>
			<xsl:when test="count(child::*) &gt; 1">
				<m:mrow>
					<xsl:apply-templates select="@*|*|text()|processing-instruction()" mode="phase1" />
				</m:mrow>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates select="@*|*|text()|processing-instruction()" mode="phase1" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:copy>
</xsl:template>

<!-- openoffice met pas de mtd dans un mtr quand y'a qu'une colonne comme les systèmes, c'est chiant, on en met -->
<xsl:template match="m:mtable/m:mtr[not(m:mtd)]" mode="phase1">
	<xsl:copy><m:mtd>
		<xsl:apply-templates select="@*|*|text()|processing-instruction()" mode="phase1" />
	</m:mtd></xsl:copy>
</xsl:template>

<!-- quand on a des objets pas maths dans un tableau word, il les met en phrase, on les met en bijectionmaths direct pour fr-maths grâce à un attribut -->
<xsl:template match="m:mtable/m:mtr/m:mtd/phrase" mode="phase1">
	<xsl:choose>
		<xsl:when test="string-length(normalize-space(.))=0"></xsl:when>
		<xsl:when test="functx:contains-any-of(string(.),('0','1','2','3','4','5','6','7','8','9'))">
			<xsl:for-each select="lit/*">
				<xsl:choose>
					<xsl:when test="functx:is-a-number(.)">
						<m:mn><xsl:value-of select="." /></m:mn>
					</xsl:when>
					<xsl:otherwise>
						<m:mo><xsl:value-of select="." /></m:mo>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</xsl:when>
		<xsl:otherwise>
			<m:mtext>
				<xsl:attribute name="bijection" select="'true'" />
				<xsl:value-of select="string-join(lit/*,$espace)" />
			</m:mtext>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- openoffice met des fois des mtext pour des lettres au lieu de mi ; on corrige ça 
NON : on fait plutôt une bijection dans la seconde passe -->
<!--<xsl:template match="m:mtext[not(*)]" mode="phase1">
	<xsl:for-each select="functx:chars(.)">
		<xsl:choose>
			<xsl:when test="contains(concat($l_alphabet,$l_grec_min,$l_grec_maj),.)">
				<m:mi><xsl:value-of select="." /></m:mi>
			</xsl:when>
			<xsl:otherwise>
				<m:mtext><xsl:value-of select="." /></m:mtext>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:for-each>
</xsl:template>-->

<!-- openoffice génère des mi vides quand on met des espaces on les remplace par des thickspace pour faire comme dans mathtype pour les unités -->
<!-- mais des fois aussi pour les cases vides des matrices alors là on les vire -->
<xsl:template match="m:mi[not(node())]" priority="2" mode="phase1">
	<xsl:choose>
		<xsl:when test="parent::m:mtd or parent::m:mrow/parent::m:mtd" />
		<xsl:otherwise>
			<m:mtext bijection="true">&ThickSpace;</m:mtext>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- traitements des mo, mi, mn membres de fonctions en pas following-sibling à cause des mrow de con -->
<xsl:template match="m:mn[parent::*[self::m:mrow and not(fn:contains($blocList,concat('|',local-name(parent::*),'|')))]]" mode="phase1">
	<xsl:call-template name="traitementMembreFonction" />
</xsl:template>

<!-- oo 3.2 met plein de nouveaux signes en mi mathvariant, il faut que ce soit des mo 
la priorité c'est pour la laisser d'abord au match du dessous -->
<xsl:template match="m:mi[@mathvariant='normal']" priority="-0.6" mode="phase1">
	<m:mo>
		<xsl:attribute name="proutos" select="1" />
		<xsl:value-of select="."/>
	</m:mo>
</xsl:template>

<!-- des fois plusieurs lettres (cas d'unités) sont mis dans un seul mi par OO, pas bon -->
<xsl:template match="m:mi[@mathvariant='italic']" priority="-0.6" mode="phase1">
	<xsl:variable name="lettres" select="functx:chars(text())" />
	<xsl:variable name="n" select="." />
	<m:mrow>
		<xsl:for-each select="$lettres">
			<xsl:choose>
				<xsl:when test="matches(.,'[A-z]')">
					<m:mi><xsl:copy-of select="$n/@*" /><xsl:value-of select="." /></m:mi>
				</xsl:when>
				<xsl:when test="matches(.,'[0-9]')">
					<m:mn><xsl:copy-of select="$n/@*" /><xsl:value-of select="." /></m:mn>
				</xsl:when>
				<xsl:otherwise>
					<m:mo><xsl:copy-of select="$n/@*" /><xsl:value-of select="." /></m:mo>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</m:mrow>
</xsl:template>

<xsl:template match="m:mi[parent::*[self::m:mrow and not(fn:contains($blocList,concat('|',local-name(parent::*),'|')))]]" mode="phase1">
	<!--<xsl:message><xsl:copy-of select="."/></xsl:message>-->
	<xsl:choose>
		<xsl:when test="@mathvariant='normal'">
			<!--<xsl:message select="'GAGNE'"/>-->
			<m:mo>
				<xsl:attribute name="proutos" select="1" />
				<xsl:call-template name="traitementMembreFonctionSub" />
			</m:mo>
		</xsl:when>
		<xsl:otherwise>
			<!--<xsl:message select="'PERDU'"/>-->
			<xsl:call-template name="traitementMembreFonction" />
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="m:mo[parent::*[self::m:mrow and not(fn:contains($blocList,concat('|',local-name(parent::*),'|')))]]" mode="phase1">
	<xsl:call-template name="traitementMembreFonction" />
</xsl:template>

<!-- pour virer les &amp; des noms d'entites 
<xsl:template match="text()" mode="phase1">
	<xsl:choose>
		<xsl:when test="(string-length(.) > 1) and not (.='&lt;')">
			<xsl:value-of select="." disable-output-escaping="yes" />
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="." />
		</xsl:otherwise>
	</xsl:choose>
</xsl:template> -->

<!-- pour virer les , et . enfants de mn et les mettre dedans (conar de MathType) -->
<xsl:template match="m:mo[parent::*[self::m:mn]]" mode="phase1">
	<xsl:value-of select="string(.)" />
</xsl:template>

<!-- *********** 1er test pour les fonctions simples ************ -->
<xsl:template match="m:*[*[(self::m:mo|self::m:mi)[fn:contains($functionList,concat('|',string(.),'|'))]]]" mode="phase1">
<!-- <xsl:template match="m:*[*[self::m:mo[fn:matches(string(.),$functionList)]]]"> ce serait mieux -->
	
	<xsl:copy>
		<xsl:apply-templates select="@*" mode="phase1"/>

		<xsl:for-each select="*">
			<xsl:call-template name="traitementFonctions">
				<xsl:with-param name="listeFonctions" select="$listeFonctions" />
			</xsl:call-template>
		</xsl:for-each>
	</xsl:copy>
</xsl:template>

<!-- *********** 2eme test pour les fonctions exp genre cos² ************ -->
<!-- dans ce cas c'est le msup qui a l'attribut fonction et pas le mi -->
<!-- EBAUCHE, NE SERT PAS 
<xsl:template match="m:msup[1][*[(self::m:mo|self::m:mi)[fn:contains($functionList,concat('|',string(.),'|'))]]]" priority="-11" mode="phase1">
	<xsl:copy>
		<xsl:choose>
			<xsl:when test="fn:contains($trigoList,concat('|',string(*[1]),'|'))">
				<xsl:attribute name="fonc" select="'trigo'" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:attribute name="fonc" select="'autre'" />
			</xsl:otherwise>
		</xsl:choose>
		
		<xsl:element name="mi" namespace="http://www.w3.org/1998/Math/MathML"> pour que les mo fonctions deviennent mi 
			<xsl:apply-templates select="@*[1]|*[1]" mode="phase1" />
		</xsl:element>
		
		<xsl:apply-templates select="*[2]" mode="phase1"/>
		
	</xsl:copy>
</xsl:template>-->


<!-- *********** template de traitement d'une série d'enfants avec au moins 1 fct ******* -->
<xsl:template name="traitementFonctions">
	<xsl:param name="listeFonctions" />	
	
	<xsl:variable name="ps1" select="if (preceding-sibling::*[1][self::m:msup or self::m:msub]) then
					if (preceding-sibling::*[1]/*[1][self::m:mrow]) then preceding-sibling::*[1]/*[1]/*[1]
						else preceding-sibling::*[1]/*[1]
					else preceding-sibling::*[1]" />
	<xsl:variable name="ps2" select="if (preceding-sibling::*[2][self::m:msup or self::m:msub]) then
					if (preceding-sibling::*[2]/*[1][self::m:mrow]) then preceding-sibling::*[2]/*[1]/*[1]
						else preceding-sibling::*[2]/*[1]
					else preceding-sibling::*[2]" />
	
	<xsl:choose>
		<xsl:when test="functx:is-node-in-sequence(.,$listeFonctions)"> <!-- on est sur une fonction -->
			<xsl:element name="mi" namespace="http://www.w3.org/1998/Math/MathML"> <!-- pour que les mo fonctions deviennent mi -->
				<xsl:choose>
					<xsl:when test="fn:contains($trigoList,concat('|',string(.),'|'))">
						<xsl:attribute name="fonc" select="'trigo'" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:attribute name="fonc" select="'autre'" />
					</xsl:otherwise>
				</xsl:choose>
				<xsl:apply-templates select="@*|*|text()" mode="phase1"/>
			</xsl:element>
		</xsl:when>
		<xsl:otherwise> <!-- tests pour les membres -->
			<xsl:copy>
				<!-- le not(string(.)='d') c'est pour éviter les cas genre sin xdx ou ln xdx avec des blocs comprenant le d -->
				<xsl:if test="not(fn:contains($noBlocList,concat('|',local-name(.),'|')))
				and (
					(functx:is-node-in-sequence($ps1,$listeFonctions) and fn:translate(string(.),$l_alphanumgrec,'')='' and not(..[self::m:msup or self::m:msub])) or
					(functx:is-node-in-sequence($ps1,$listeFonctions) and fn:contains($blocList,concat('|',local-name(.),'|'))) or
					(functx:is-node-in-sequence($ps2,$listeFonctions) and fn:translate(concat(string($ps1),string(.)),$l_alphanumgrec_ext,'')=''
						and string-length(concat(string($ps1),string(.))) &lt; 3 and not(string(.)='d')) or
					(functx:is-node-in-sequence($ps2,$listeFonctions) and (string(.)=('&circ;','°','''','&prime;') and local-name($ps1)='mn'))
				)
				">
					<xsl:attribute name="membreFonc" select="'yes'" />
				</xsl:if>
				
				<xsl:choose>
					<xsl:when test="local-name(.)='mrow' and
						(*[(self::m:mo|self::m:mi)[fn:contains($functionList,concat('|',string(.),'|'))]])">
						<!-- il faut vérifier si ses enfants sont fonctions et si en plus y'a pas de fct
						dans un preceding d'avant -->
						
						<xsl:variable name="liste2" select="*[(self::m:mo|self::m:mi)[fn:contains($functionList,concat('|',string(.),'|'))]]" />
						<xsl:variable name="avantFirstFunc" select="count(*[following-sibling::*[$liste2[1]]])" />
						
						<xsl:apply-templates select="*[position() &lt; ($avantFirstFunc+1)]" mode="phase1"/>
						
						<xsl:for-each select="*[position() > $avantFirstFunc]">
							<xsl:call-template name="traitementFonctions">
								<xsl:with-param name="listeFonctions" select="$liste2" />
							</xsl:call-template>
						</xsl:for-each>
						
					</xsl:when>
					<xsl:otherwise>
						<xsl:apply-templates select="@*|*|text()" mode="phase1"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:copy>
		</xsl:otherwise>
	</xsl:choose>

</xsl:template>

<!-- controle complementaire basé sur les preceding -->

<xsl:template name="traitementMembreFonction">
	<xsl:copy>
		<xsl:call-template name="traitementMembreFonctionSub" />
	</xsl:copy>
</xsl:template>

<xsl:template name="traitementMembreFonctionSub">
		<xsl:apply-templates select="@*" mode="phase1"/>
		<xsl:if test="fn:translate(string(.),$l_alphanumgrec,'')='' and (
		(preceding::text()[1][fn:contains($functionList,concat('|',string(.),'|'))])
		 or (preceding::text()[2][fn:contains($functionList,concat('|',string(.),'|')) 
				and (ancestor::*[2][self::m:msup or self::m:msub or self::m:mrow[(parent::m:msup or parent::m:msub) and count(*)=1]])]))
			and not(ancestor::m:mfenced/preceding::text()[1][fn:contains($functionList,concat('|',string(.),'|'))] = preceding::text()[1][fn:contains($functionList,concat('|',string(.),'|'))])
		">
			<!-- le dernier test est pour les membres des cos² dont le cos est dans un msup ou un msup/mrow -->
			<xsl:attribute name="membreFonc" select="'yes'" />
		</xsl:if>
		<!-- bonus pour les degrés -->
		<xsl:if test="fn:translate(string(.),concat('°','&deg;','&circ;'),'')='' and 
		(preceding::text()[2][fn:contains($functionList,concat('|',string(.),'|'))])">
			<xsl:attribute name="membreFonc" select="'yes'" />
		</xsl:if>
		<xsl:apply-templates select="*|text()|processing-instruction()" mode="phase1"/>
</xsl:template>

</xsl:stylesheet>
