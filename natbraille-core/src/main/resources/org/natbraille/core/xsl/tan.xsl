<?xml version='1.1' encoding="UTF-8" ?>
<!--
 * NAT - An universal Translator
 * Copyright (C) 2005 Raphaël Mina
 * Contact: raphael.mina@gmail.com
 *          natbraille.Free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
-->

<!-- version: 1.3-b3 -->
<!DOCTYPE xsl:stylesheet SYSTEM "nat://system/xsl/mmlents/windob.dtd">

<xsl:stylesheet version="2.0"
xmlns='http://www.w3.org/1999/xhtml'
xmlns:xsl='http://www.w3.org/1999/XSL/Transform'
xmlns:saxon='http://icl.com/saxon'
xmlns:xs='http://www.w3.org/2001/XMLSchema'
xmlns:fn='http://www.w3.org/2005/xpath-functions'
xmlns:fo='http://www.w3.org/1999/XSL/Format'
xmlns:m='http://www.w3.org/1998/Math/MathML'
xmlns:lit='espacelit'
xmlns:functx='http://www.functx.com'
xmlns:doc='espaceDoc'
xmlns:nat='http://natbraille.free.fr/xsl'>

<xsl:import href="nat://system/xsl/functions/functx.xsl" /> <!-- functx functions -->
<xsl:import href="nat://system/xsl/tanMaths.xsl"/><!-- transcription braille mathématique-->

<xsl:character-map name="remplacementMap">
	<xsl:output-character character="&lt;" string="&#60;"/>
	<xsl:output-character character="&gt;" string="&#62;"/>
</xsl:character-map>

<xsl:output
	method="xhtml"
	doctype-public="-//W3C//DTD XHTML 1.1 plus MathML 2.0//EN"
	indent="yes"
	encoding="UTF-8"
	media-type="text/html"
/>

<!-- this variable is used to get ambiguity BUT is defined in xsl.xsl-->
<!--<xsl:variable name="blackToBraille" select="false()" as="xs:boolean"/>-->
<!-- On passe les symboles sémantiques braille par une variable pour éviter les problèmes de "unmatched quote"-->
<xsl:variable name="symboleMev"><xsl:text>&pt456;</xsl:text></xsl:variable>
<xsl:variable name="symboleFinPassage"><xsl:text>&pt6;&pt3;</xsl:text></xsl:variable>
<xsl:variable name="symboleMaj"><xsl:text>&pt46;</xsl:text></xsl:variable>
<xsl:variable name="symboleMajDouble"><xsl:text>&pt46;&pt46;</xsl:text></xsl:variable>
<xsl:variable name="symboleCleMaj"><xsl:text>&pt25;&pt46;</xsl:text></xsl:variable>
<xsl:variable name="symboleCleMev"><xsl:text>&pt25;&pt456;</xsl:text></xsl:variable>
<xsl:variable name="symboleNumerique"><xsl:text>&pt6;</xsl:text></xsl:variable>
<xsl:variable name="symboleFinNumerique"><xsl:text>&pt56;</xsl:text></xsl:variable>
<xsl:variable name="virgule"><xsl:text>&pt2;</xsl:text></xsl:variable>
<xsl:variable name="ptInterro"><xsl:text>&pt26;</xsl:text></xsl:variable>
<xsl:variable name="ptVirgule"><xsl:text>&pt23;</xsl:text></xsl:variable>
<xsl:variable name="point"><xsl:text>&pt256;</xsl:text></xsl:variable>
<xsl:variable name="deuxPoints"><xsl:text>&pt25;</xsl:text></xsl:variable>
<xsl:variable name="slash"><xsl:text>&pt34;</xsl:text></xsl:variable>
<xsl:variable name="ptExcla"><xsl:text>&pt235;</xsl:text></xsl:variable>
<xsl:variable name="prefixeFormule"><xsl:text>&pt6;&pt3;</xsl:text></xsl:variable>
<xsl:variable name="finLigneTableau"><xsl:text>&pt6;&pt345;</xsl:text></xsl:variable>
<xsl:variable name="celluleVide"><xsl:text>&pt5;&pt2;</xsl:text></xsl:variable>

<xsl:template match="/">
	<html xml:lang="FR">
		<body>
			<xsl:apply-templates/>
		</body>
	</html>
</xsl:template>


<xsl:template match="doc:doc">
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="page-break">
	<span style="page-break-after:always"/>
</xsl:template>

<xsl:template match="phrase">
	<p>
		<xsl:apply-templates/>
	</p>
</xsl:template>

<xsl:template match="lit">
	<xsl:apply-templates select="*[1]">
		<xsl:with-param name="fin" select="true()" tunnel="yes"/>
		<xsl:with-param name="passageMaj" select="false()" tunnel="yes"/>
		<xsl:with-param name="passageMev" select="false()" tunnel="yes"/>
	</xsl:apply-templates>
</xsl:template>

<xsl:template match="mot">
	<xsl:param name="fin" as="xs:boolean" tunnel="yes"/>
	<xsl:param name="passageMaj" as="xs:boolean" tunnel="yes"/>
	<xsl:param name="passageMev" as="xs:boolean" tunnel="yes"/>

	<xsl:if test="preceding-sibling::* and string(.)"><!-- TODO application règles des espaces de puncts, BRUNO -->
		<xsl:text> </xsl:text>
	</xsl:if>
	<!-- brancher ici abrégé -->
	<xsl:variable name="mot">
		<!--<xsl:message select="."/>-->
		<xsl:choose>
			<xsl:when test="starts-with(functx:substring-after-match(.,'^[&pt;]+'),'&pt6;&pt3;')">
				<xsl:value-of select="functx:substring-after-match(.,'^[&pt;]+')"/>
			</xsl:when>
			<xsl:when test="$abrege">
				<!-- on retire les ouvrants -->
				<xsl:variable name="sansOuvrant" select="if(not(translate(.,'&pt236;&pt2356;','')='')) then functx:substring-after-match(.,'^[&pt236;&pt2356;]') else ."/>
				<xsl:variable name="ouvrant" select="substring-before(.,$sansOuvrant)"/>
				<xsl:message select="'ouvrant:',$ouvrant,'+',$sansOuvrant"/>
				<!-- remplacement des &pt3; qui ne sont pas en début par des apostrophes -->

				<!-- cas particulier des locutions sur deux mots contenant un apostrophe -->
				<xsl:variable name="sansLocApos" select="functx:replace-multi(functx:substring-after-match($sansOuvrant,'^[&pt;]+'),('⠉⠄⠑⠤⠷⠤⠙(.)?','⠅⠄⠓(.)?$','⠙⠄⠁([^&pt235;^&pt1235;])?$'),('c''est-à-dire','aujourd''hui$1','d''abord$1'))"/>
				<!-- TODO verif quand majuscule ou autre préfixe-->
				<xsl:variable name="sansApos" select="functx:replace-multi($sansLocApos,('^([&pt46;]+[^&pt46;]+)&pt3;','^([^&pt46;]+)&pt3;'),('$1''','$1'''))"/>
				<!-- decoupage sur vraies apostrophes -->
				<xsl:variable name="decoup" select="tokenize($sansApos,'''')" as="xs:string*"/>
				<!-- si présence d'apostrophe(s), premier terme non désabrégé si une seule lettre TODO -->
				<!--<xsl:value-of select="if(count($decoup) &gt; 1 and string-length($decoup[1])=1 and not($decoup[1]='&pt12345;')) then $decoup[1] else nat:desabrege($decoup[1],'')"/>-->
				<xsl:variable name="resu">
					<xsl:for-each select="$decoup">
						<xsl:value-of select="concat(if(position()=1) then '' else '&pt3;', nat:desabrege(.,''))"/>
					</xsl:for-each>
				</xsl:variable>
				<xsl:value-of select="concat($ouvrant,functx:replace-multi($resu,('([cdjlmnstu])e&pt3;','(t)e&pt36;'),('$1&pt3;','$1&pt36;')))"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="functx:substring-after-match(.,'^[&pt;]+')"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:call-template name="debrailler">
		<xsl:with-param name="contenu" select="$mot"/>
		<xsl:with-param name="maj" select="false()" tunnel="yes"/>
		<xsl:with-param name="num" select="false()" tunnel="yes"/>
	</xsl:call-template>
</xsl:template>

<!--
<xsl:template match="ponctuation">
  <xsl:param name="fin" as="xs:boolean" tunnel="yes"/>
  <xsl:param name="passageMaj" as="xs:boolean" tunnel="yes"/>
  <xsl:param name="passageMev" as="xs:boolean" tunnel="yes"/>

    <xsl:if test="not(contains(translate(.,$point,$virgule),$virgule))">
      <xsl:text> </xsl:text>
    </xsl:if>

    <xsl:call-template name="bijection">
	    <xsl:with-param name="contenu" select="."/>
	    <xsl:with-param name="maj" select="false()" tunnel="yes"/>
	    <xsl:with-param name="num" select="false()" tunnel="yes"/>
    </xsl:call-template>

</xsl:template>-->

<xsl:template name="debrailler">
	<xsl:param name="contenu" as="xs:string"/>
	<xsl:param name="maj" as="xs:boolean" tunnel="yes"/>
	<xsl:param name="passageMaj" as="xs:boolean" tunnel="yes"/>
	<xsl:param name="passageMev" as="xs:boolean" tunnel="yes"/>
	<xsl:param name="num" as="xs:boolean" tunnel="yes"/>
	<xsl:param name="fin" as="xs:boolean" tunnel="yes"/>

	<xsl:choose>
		<!--<xsl:when test="string-length($contenu) = 0" />-->
		<!-- Formule mathématique ?-->
		<xsl:when test="substring($contenu,1,2)=$prefixeFormule">
			<!-- contenu2D est l'indice du premier bloc multiligne ouvrant et donc indique qu'on a du 2D linéarisé -->
			<xsl:variable name="firstBloc2D" as="xs:integer?" select="functx:index-of-match-first($contenu,string-join($seqOuvrantMultilignes,'|'))" />
			<xsl:variable name="tabVariation" as="xs:boolean" select="following-sibling::*[.='&pt6;&pt345;'] and not(following-sibling::*[starts-with(.,$prefixeFormule)])"/>
			<!--<xsl:message select="'****',nat:toTbfr(string-join(../../preceding-sibling::*[1]/*,' '))" />-->
			<!--<xsl:message select="boolean(following-sibling::*[.='&pt6;&pt345;']),' ',boolean(following-sibling::*[starts-with(.,$prefixeFormule)]),'**',$firstBloc2D,'**'" />-->
			<xsl:variable name="finMath" as="element()*">
			<!-- si on a des structures 2D linéarisées, faut savoir jusqu'où elle va -->
				<xsl:choose>
					<xsl:when test="($tabVariation) or ($firstBloc2D)">
						<xsl:call-template name="debraille2D">
							<xsl:with-param name="listeBlocs2D" select="if ($firstBloc2D) then (substring($contenu, $firstBloc2D,2)) else ('')" tunnel="yes"/>
							<!-- <xsl:with-param name="structure2D" select="(0)" tunnel="yes"/>-->
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise>
						<fin>0</fin>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<!--<xsl:if test="$tabVariation">
				-<xsl:message>
					<xsl:for-each select="$finMath">
						<xsl:copy copy-namespaces="no">
							<xsl:value-of select="nat:toTbfr(.)" />
						</xsl:copy>
						<!{1}**<xsl:if test="position()=$pos"><xsl:text>********</xsl:text><xsl:value-of select="nat:toTbfr(text())"/></xsl:if>**{1}>
						<xsl:text>&#10;</xsl:text>
					</xsl:for-each>
				</xsl:message>
			</xsl:if>-->
			<m:math>
                <xsl:variable name="mathml-content" as="element()*">
					<xsl:call-template name="parser">
						<xsl:with-param name="contenu" select="substring($contenu,3)"/>
						<xsl:with-param name="inside2D" select="false()" tunnel="yes" />
						<xsl:with-param name="tab2D" select="$finMath" />
					</xsl:call-template>
				</xsl:variable>
				<xsl:choose>
					<xsl:when test="$MODE_DETRANS_MATH_USE_BRAILLE_SEMANTICS_ANNOTATION">
                        <!-- no braille mathml annotation -->
                        <m:semantics>
                            <m:mrow>
                                <xsl:copy-of select="$mathml-content"/>
                            </m:mrow>
                            <m:annotation>
                                <xsl:attribute name="encoding">application/x-braille;lang=fr</xsl:attribute>
                                <xsl:value-of select="substring($contenu,3)"/>
                            </m:annotation>
                        </m:semantics>
					</xsl:when>
					<xsl:otherwise>
                        <!-- use braille mathml annotation -->
                        <xsl:copy-of select="$mathml-content"/>
					</xsl:otherwise>
				</xsl:choose>
			</m:math>
			<!--<xsl:apply-templates select="following-sibling::*[not($finLigneTableau=(text(),preceding-sibling::*[1]/text()))][1]">-->
			<xsl:apply-templates select="following-sibling::*[number($finMath[position()=last()]) +1]">
				<xsl:with-param name="fin" select="true()" tunnel="yes"/>
			</xsl:apply-templates>
		</xsl:when>
		<!--Contient une clé de mise en évidence ?-->
		<xsl:when test="contains($contenu,$symboleCleMev)">
			<xsl:call-template name="debrailler">
				<xsl:with-param name="contenu" select="substring-before($contenu,$symboleCleMev)"/>
				<xsl:with-param name="fin" select="false()" tunnel="yes"/>
			</xsl:call-template>
			<xsl:text>&lt;em&gt;</xsl:text>
			<xsl:call-template name="debrailler">
				<xsl:with-param name="contenu" select="substring-after($contenu,$symboleCleMev)"/>
				<xsl:with-param name="passageMev" select="true()" tunnel="yes"/>
			</xsl:call-template>
		</xsl:when>
		<!--Contient une clé majuscule-->
		<xsl:when test="contains($contenu,$symboleCleMaj)">
			<xsl:call-template name="debrailler">
				<xsl:with-param name="contenu" select="substring-before($contenu,$symboleCleMaj)"/>
				<xsl:with-param name="fin" select="false()" tunnel="yes"/>
			</xsl:call-template>
			<xsl:call-template name="debrailler">
				<xsl:with-param name="contenu" select="substring-after($contenu,$symboleCleMaj)"/>
				<xsl:with-param name="passageMaj" select="true()" tunnel="yes"/>
			</xsl:call-template>
		</xsl:when>
		<!--Contient une partie en exposant ?-->
		<!-- TODO important de peut-être remonter l'exposant pour traiter le cas ou les
		exposant contiennent des majuscules-->
		<xsl:when test="contains($contenu,'&pt4;')">
			<xsl:choose>
				<!-- Les caractères qui interrompent la mise en indice sont le trait d'union,
				la barre oblique et les signes numériques. Il convient donc de traiter le cas
				où on est en passage numérique et l'autre cas.-->
				<xsl:when test="$num">
					<!--Premiere partie du mot, avant la mise en exposant-->
					<xsl:call-template name="debrailler">
								<xsl:with-param name="contenu" select="substring-before($contenu,'&pt4;')"/>
								<xsl:with-param name="fin" select="false()" tunnel="yes"/>
					</xsl:call-template>
					<xsl:variable name="exp" select="functx:substring-before-if-contains(translate(substring-after($contenu,'&pt4;'),
					'&pt235;&pt36;&pt35;&pt25;&pt2356;','&pt34;&pt34;&pt34;&pt34;'),'&pt34;')" />
					<sup>
						<!--Seconde partie du mot, avant l'éventuelle fin de mise en exposant-->
						<xsl:if test="string-length($exp)>0 and not($exp='&pt4;')">
							<xsl:call-template name="debrailler">
								<xsl:with-param name="contenu" select="$exp"/>
								<xsl:with-param name="fin" select="false()" tunnel="yes"/>
							</xsl:call-template>
						</xsl:if>
					</sup>
					<!--Dernière éventuelle partie du mot, apres l'éventuelle fin de mise en exposant-->
					<xsl:call-template name="debrailler">
						<xsl:with-param name="contenu" select="if (not(ends-with(substring-after($contenu,$exp),'&pt4;'))) then
								substring-after($contenu,$exp) else ''"/><!-- sans ça récursion infinie -->
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<!--Premiere partie du mot, avant la mise en exposant-->
					<xsl:call-template name="debrailler">
						<xsl:with-param name="contenu" select="substring-before($contenu,'&pt4;')"/>
						<xsl:with-param name="fin" select="false()" tunnel="yes"/>
					</xsl:call-template>
					<xsl:variable name="exp" select="functx:substring-before-if-contains(translate(substring-after($contenu,'&pt4;'),'&pt36;','&pt34;'),'&pt34;')" />
					<sup>
						<!--Seconde partie du mot, avant l'éventuelle fin de mise en exposant-->
						<xsl:if test="string-length($exp)>0 and not($exp='&pt4;')">
							<xsl:call-template name="debrailler">
								<xsl:with-param name="contenu" select="$exp"/>
								<xsl:with-param name="fin" select="false()" tunnel="yes"/>
							</xsl:call-template>
						</xsl:if>
					</sup>
					<!--Dernière éventuelle partie du mot, après l'éventuelle fin de mise en exposant-->

						<xsl:call-template name="debrailler">
							<xsl:with-param name="contenu" select="if (not(ends-with(substring-after($contenu,$exp),'&pt4;'))) then
								substring-after($contenu,$exp) else ''"/> <!-- sans ça récursion infinie -->
							<xsl:with-param name="fin" select="(string-length($exp)=0)" tunnel="yes"/>
						</xsl:call-template>

				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		<!--Contient un double préfixe majuscule-->
		<xsl:when test="contains($contenu,$symboleMajDouble)">
			<xsl:call-template name="debrailler">
				<xsl:with-param name="contenu" select="substring-before($contenu,$symboleMajDouble)"/>
				<xsl:with-param name="fin" select="false()" tunnel="yes"/>
			</xsl:call-template>
			<xsl:call-template name="debrailler">
				<xsl:with-param name="contenu" select="functx:substring-before-if-contains(substring-after($contenu,$symboleMajDouble),$symboleFinPassage)"/>
				<xsl:with-param name="maj" select="true()" tunnel="yes"/>
				<xsl:with-param name="fin" select="false()" tunnel="yes"/>
			</xsl:call-template>
			<xsl:call-template name="debrailler">
				<xsl:with-param name="contenu" select="substring-after($contenu,$symboleFinPassage)"/>
				<xsl:with-param name="maj" select="false()" tunnel="yes"/>
			</xsl:call-template>
		</xsl:when>
		<!--Contient un seul préfixe majuscule-->
		<xsl:when test="contains($contenu,$symboleMaj)">
			<xsl:call-template name="debrailler">
				<xsl:with-param name="contenu" select="substring-before($contenu,$symboleMaj)"/>
				<xsl:with-param name="fin" select="false()" tunnel="yes"/>
			</xsl:call-template>
			<xsl:choose>
				<!-- Un symbole de majuscule dans un passage en majuscule indique la fin de ce passage-->
				<xsl:when test="$passageMaj">
					<xsl:call-template name="debrailler">
						<xsl:with-param name="contenu" select="substring-after($contenu,$symboleMaj)"/>
						<xsl:with-param name="maj" select="true()" tunnel="yes"/>
						<xsl:with-param name="passageMaj" select="false()" tunnel="yes"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="debrailler">
						<xsl:with-param name="contenu" select="substring($contenu,functx:index-of-string-first($contenu,$symboleMaj)+1,1)"/>
						<xsl:with-param name="maj" select="true()" tunnel="yes"/>
						<xsl:with-param name="fin" select="false()" tunnel="yes"/>
					</xsl:call-template>
					<xsl:call-template name="debrailler">
						<xsl:with-param name="contenu" select="substring($contenu,functx:index-of-string-first($contenu,$symboleMaj)+2)"/>
						<xsl:with-param name="maj" select="false()" tunnel="yes"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		<!-- Contient un signe de mise en évidence ? -->
		<xsl:when test="contains($contenu,$symboleMev)">
			<xsl:choose>
				<!-- Si on est dans un passage en évidence, le pt456 signifie une fin de passage -->
				<xsl:when test="$passageMev">
					<xsl:call-template name="debrailler">
						<xsl:with-param name="contenu" select="substring-before($contenu,$symboleMev)"/>
						<xsl:with-param name="fin" select="false()" tunnel="yes"/>
					</xsl:call-template>
					<xsl:call-template name="debrailler">
						<xsl:with-param name="contenu" select="substring-after($contenu,$symboleMev)"/>
						<xsl:with-param name="passageMev" select="false()" tunnel="yes"/>
						<xsl:with-param name="fin" select="false()" tunnel="yes"/>
					</xsl:call-template>
					<!-- On est obligé de passer par la character map pour afficher la balise, car
					sinon le parseur signale que la balise <em> ouvrante n'existe pas dans ce bloc.-->
					<xsl:text>&lt;/em&gt;</xsl:text>
					<!-- Appel du template uniquement pour passer au mot suivant. Malin non ?-->
					<!-- t'es trop fort Raph... -->
					<xsl:call-template name="debrailler">
						<xsl:with-param name="contenu">
							<xsl:value-of select="''"/>
						</xsl:with-param>
						<xsl:with-param name="passageMev" select="false()" tunnel="yes"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<!-- Cas où le caractere pt456 marque le début d'un passage en évidence.-->
					<xsl:call-template name="debrailler">
						<xsl:with-param name="contenu" select="substring-before($contenu,$symboleMev)"/>
						<xsl:with-param name="fin" select="false()" tunnel="yes"/>
					</xsl:call-template>
					<em>
						<xsl:call-template name="debrailler">
							<xsl:with-param name="contenu" select="functx:substring-before-if-contains(substring-after($contenu,$symboleMev),$symboleFinPassage)"/>
							<xsl:with-param name="passageMev" select="false()" tunnel="yes"/>
							<xsl:with-param name="fin" select="false()" tunnel="yes"/>
						</xsl:call-template>
					</em>
					<xsl:call-template name="debrailler">
						<xsl:with-param name="contenu" select="substring-after($contenu,$symboleFinPassage)"/>
						<xsl:with-param name="passageMev" select="false()" tunnel="yes"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		<!--Contient le symbole numérique-->
		<xsl:when test="contains($contenu,$symboleNumerique)">
			<xsl:call-template name="debrailler">
				<xsl:with-param name="contenu" select="substring-before($contenu,$symboleNumerique)"/>
				<xsl:with-param name="fin" select="false()" tunnel="yes"/>
			</xsl:call-template>
			<xsl:call-template name="debrailler">
				<xsl:with-param name="contenu" select="functx:substring-before-if-contains(substring-after($contenu,$symboleNumerique),$symboleFinNumerique)"/>
				<xsl:with-param name="num" select="true()" tunnel="yes"/>
				<xsl:with-param name="fin" select="false()" tunnel="yes"/>
			</xsl:call-template>
			<xsl:call-template name="debrailler">
				<xsl:with-param name="contenu" select="substring-after($contenu,$symboleFinNumerique)"/>
				<xsl:with-param name="num" select="false()" tunnel="yes"/>
			</xsl:call-template>
		</xsl:when>
		<xsl:otherwise>
		<!--Si on arrive jusqu'ici, c'est que le mot ne contient pas ou plus de chose remarquables.
			On opère alors une bijection de remplacement grâce aux paramètres glanés précédemment-->
			<xsl:call-template name="bijection">
			<xsl:with-param name="contenu" select="$contenu"/>
			</xsl:call-template>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="bijection">
	<xsl:param name="contenu" as="xs:string"/>
	<xsl:param name="maj" as="xs:boolean" tunnel="yes"/>
	<xsl:param name="num" as="xs:boolean" tunnel="yes"/>
	<xsl:param name="passageMev" as="xs:boolean" tunnel="yes"/>
	<xsl:param name="passageMaj" as="xs:boolean" tunnel="yes"/>
	<xsl:param name="fin" as="xs:boolean" tunnel="yes"/>

	<!--On définit l'alphabet de remplacement par partie, en fonction des paramètres majuscule et numérique-->
	<xsl:variable name="seqAlphabet" as="xs:string*">
		<xsl:choose>
			<!-- En mode majuscule, l'alphabet de remplacement est en majuscule-->
			<xsl:when test="$maj or $passageMaj">
				<xsl:value-of select="'ABCDEFGHIJKLMNOPQRSTUVWXYZÇÉÀÈÙ'"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="'abcdefghijklmnopqrstuvwxyzçéàèù'"/>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:choose>
			<!-- En mode numérique, on remplace par des nombres, sinon par les lettres de la quatrième série
			et d'un espace, le 0 n'ayant pas d'équivalent-->
			<xsl:when test="$num">
				<xsl:text> 1234567890</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="$maj">
						<xsl:text>'ÂÊÎÔÛËÏÜŒ</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>'âêîôûëïüœ</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
				<!--Sortie d'une chaîne vide pour compenser le caractère 0-->
				<xsl:text> </xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<!-- partie commune : parenthèses, etc...-->
		<xsl:text>,?;.:/!"()@-</xsl:text>
	</xsl:variable>
	<!--On concatène les différentes partie de l'alphabet de remplacement-->
	<xsl:variable name="alphabetRemplacement" select="string-join($seqAlphabet,'')"/>
	<xsl:variable name="alphabetBraille">
		<xsl:text>&pt1;&pt12;&pt14;&pt145;&pt15;&pt124;&pt1245;&pt125;&pt24;&pt245;&pt13;&pt123;&pt134;&pt1345;&pt135;&pt1234;&pt12345;&pt1235;&pt234;&pt2345;&pt136;&pt1236;&pt2456;&pt1346;&pt13456;&pt1356;&pt12346;&pt123456;&pt12356;&pt2346;&pt23456;&pt3;&pt16;&pt126;&pt146;&pt1456;&pt156;&pt1246;&pt12456;&pt1256;&pt246;&pt3456;&pt2;&pt26;&pt23;&pt256;&pt25;&pt34;&pt235;&pt2356;&pt236;&pt356;&pt345;&pt36;</xsl:text>
	</xsl:variable>
	<!-- caractère codé sur deux caractères braille -->
	<xsl:variable name="multiChar" as="xs:string*" select="('&ndash;','&mdash;','©','°','§','®','&trade;','&amp;','~','*',
		'#','‰','%','_','\\','¢','€','£','\$','¥',
		'&lt;','&gt;','≤','≥','²','³')"/>
	<xsl:variable name="multiCharBr" as="xs:string*" select="('&pt36;&pt36;','&pt36;&pt36;','&pt5;&pt14;','&pt5;&pt135;','&pt5;&pt1234;','&pt5;&pt1235;','&pt5;&pt2345;','&pt5;&pt123456;','&pt5;&pt26;','&pt5;&pt35;',
		'&pt5;&pt3456;','&pt5;&pt346;&pt346;','&pt5;&pt346;','&pt5;&pt36;','&pt5;&pt34;','&pt45;&pt14;','&pt45;&pt15;','&pt45;&pt123;','&pt45;&pt234;','&pt45;&pt13456;',
		'&pt5;&pt126;','&pt5;&pt345;','&pt45;&pt126;','&pt45;&pt345;','&pt4;&pt126;','&pt4;&pt146;')"/>
	<xsl:variable name="contenu2" select="functx:replace-multi($contenu,$multiCharBr,$multiChar)"/>
	<xsl:value-of select="translate(if($maj or $passageMaj) then upper-case($contenu2) else $contenu2,$alphabetBraille,$alphabetRemplacement)"/>
	<xsl:if test="$fin">
		<xsl:apply-templates select="following-sibling::*[1]">
			<xsl:with-param name="fin" select="true()" tunnel="yes"/>
		</xsl:apply-templates>
	</xsl:if>
</xsl:template>

<!-- restructure une expression mathématique en 2D -->
<xsl:template name="debraille2D" as="element()*">
	<xsl:param name="listeBlocs2D" as="xs:string*" select="()" tunnel="yes" />
	<xsl:param name="structure2D" as="element()*" select="()" />
	<xsl:param name="position" as="xs:integer" select="0" />

	<xsl:variable name="nc" as="element()?" select="if ($position=0) then
		functx:replace-element-values(current(),for $p in current() return substring($p,3)) else following-sibling::*[$position]" />
	<!-- bloc fermant correspondant au dernier bloc ouvert -->
	<xsl:variable name="blocFermantSiOuvert" select="$seqFermantMultilignes[index-of($seqOuvrantMultilignes,$listeBlocs2D[last()])]"/>

	<!--<xsl:message select="$listeBlocs2D"/>
	<xsl:message select="$structure2D"/>-->
	<xsl:choose>
		<xsl:when test="empty($nc)">
			<!-- on a une structure 2D qui se ferme et pas de nouvelle : fin de l'expression -->
			<xsl:variable name="cel" as="element()">
					<!-- cas d'un système ou d'un tableau qui ne se finit pas par un fermant -->
					<fin><xsl:value-of select="$position -1" /></fin>
			</xsl:variable>
			<xsl:copy-of select="($structure2D,$cel)" />
			<!--<xsl:message select="($structure2D,$cel)" />-->
		</xsl:when>
		<xsl:when test="not(functx:contains-any-of($nc/text(),($seqOuvrantMultilignes,$seqFermantMultilignes))) and not($position=0)">
			<!-- aucun bloc fermant ni ouvrant :  on continue à parcourir ; si pos=0 c'est premier passage donc faut commencer le tableau de variation au xsl:when position=0-->
			<xsl:variable name="cel" as="element()">
				<xsl:choose>
					<xsl:when test="$nc/text()='&pt6;&pt345;'">
						<line-break />
					</xsl:when>
					<xsl:otherwise>
						<cel><xsl:value-of select="$nc/text()"/></cel>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:call-template name="debraille2D">
				<xsl:with-param name="position" select="$position+1" />
				<xsl:with-param name="structure2D" select="($structure2D,$cel)"/>
			</xsl:call-template>
		</xsl:when>
		<!-- obligé de compter les blocs de la liste car pour les barres verticales on sait pas si ça ouvre ou ferme -->
		<xsl:when test="$blocFermantSiOuvert and (contains($nc/text(),$blocFermantSiOuvert) and (count($listeBlocs2D) mod 2)=1)
			and functx:contains-any-of(substring-after($nc/text(),$blocFermantSiOuvert),$seqOuvrantMultilignes)">
			<!-- on a une structure 2D qui se ferme et une nouvelle qui commence ! -->
			<xsl:variable name="suite" select="substring-after($nc/text(),$blocFermantSiOuvert)" />
			<xsl:variable name="nextOpenBlock"
				select="substring($suite,functx:index-of-match-first($suite,string-join($seqOuvrantMultilignes,'|')),2)"/>

			<!-- les 5(!) éléments à insérer -->
			<xsl:variable name="elems" as="element()*">
				<cel><xsl:value-of select="substring-before($nc/text(),$blocFermantSiOuvert)" /></cel>
				<fermant><xsl:value-of select="$blocFermantSiOuvert" /></fermant>
				<expr1D><xsl:value-of select="substring-before($suite,$nextOpenBlock)"/></expr1D>
				<ouvrant><xsl:value-of select="$nextOpenBlock" /></ouvrant>
				<cel><xsl:value-of select="substring-after($suite,$nextOpenBlock)"/></cel>
			</xsl:variable>

			<xsl:call-template name="debraille2D">
				<xsl:with-param name="position" select="$position+1" />
				<xsl:with-param name="listeBlocs2D" select="($listeBlocs2D,$blocFermantSiOuvert,$nextOpenBlock)" tunnel="yes"/>
				<xsl:with-param name="structure2D" select="($structure2D,$elems)" />
			</xsl:call-template>
		</xsl:when>
		<!-- Bruno: ajout du test modulo car dans le cas des grandes barres verticales, ça rentre aussi ici pour les fermants -->
		<xsl:when test="functx:contains-any-of($nc/text(),$seqOuvrantMultilignes) and count($listeBlocs2D) mod 2 = 0 or ($position=0)">
			<!-- on a juste un bloc ouvert, c'est le cas du premier passage uniquement
			auquel on a déjà donné la liste de bloc en paramètre donc on le rajoute pas dans la récursion -->
			<xsl:variable name="indiceOuvr" as="xs:integer?"
				select="functx:index-of-match-first($nc/text(),string-join($seqOuvrantMultilignes,'|'))" />
			<xsl:variable name="nextOpenBlock" select="if ($indiceOuvr) then substring($nc/text(),$indiceOuvr,2) else ''"/>
			<!-- les 2-3 éléments à insérer -->
			<xsl:variable name="elems" as="element()*">
				<xsl:if test="substring-before($nc/text(),$nextOpenBlock)">
					<expr1D><xsl:value-of select="substring-before($nc/text(),$nextOpenBlock)"/></expr1D>
				</xsl:if>
				<ouvrant><xsl:value-of select="$nextOpenBlock" /></ouvrant>
				<cel><xsl:value-of select="substring-after($nc/text(),$nextOpenBlock)"/></cel>
			</xsl:variable>
			<xsl:call-template name="debraille2D">
				<xsl:with-param name="position" select="$position+1" />
				<xsl:with-param name="listeBlocs2D" select="($listeBlocs2D)" tunnel="yes"/>
				<xsl:with-param name="structure2D" select="($structure2D,$elems)" />
			</xsl:call-template>
		</xsl:when>
		<xsl:when test="$blocFermantSiOuvert and (contains($nc/text(),$blocFermantSiOuvert) and (count($listeBlocs2D) mod 2)=1)">
			<!-- on a une structure 2D qui se ferme et pas de nouvelle : fin de l'expression -->
			<!--<xsl:message select="'Fin normale ! ',count($listeBlocs2D) mod 2, nat:toTbfr($nc)"/>-->
			<!-- les 4 éléments à insérer -->
			<xsl:variable name="elems" as="element()*">
				<cel><xsl:value-of select="substring-before($nc/text(),$blocFermantSiOuvert)"/></cel>
				<fermant><xsl:value-of select="$blocFermantSiOuvert" /></fermant>
				<expr1D><xsl:value-of select="substring-after($nc/text(),$blocFermantSiOuvert)"/></expr1D>
				<fin><xsl:value-of select="$position" /></fin>
			</xsl:variable>
			<xsl:copy-of select="($structure2D,$elems)" />
		</xsl:when>
		<xsl:otherwise>
			<xsl:text>Erreur</xsl:text>
			<xsl:if test="$dbg"><xsl:message select="'Erreur FinMaths ! ',count($listeBlocs2D) mod 2, nat:toTbfr($nc)"/></xsl:if>
			<!--<xsl:message select="$structure2D, nat:toTbfr(string-join($listeBlocs2D,'**'))" />-->
			<xsl:copy-of select="$structure2D" />
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- debut test abrege -->
<xsl:param name="is_abrege" select="true()"/>
<xsl:variable name="invariantsBr" select="($loc3Br,$loc2Br,$loc1Br,$signesInvBr,$signesDerivBr)"/>
<xsl:variable name="invariants" select="($loc3,$loc2,$loc1,$signesInv,$signesDeriv)"/>
<xsl:variable name="puncts" select="('&pt2;','&pt23;','&pt25;','&pt256;','&pt26;','&pt235;','&pt2356;','&pt2356;','&pt2356;','&pt2356;','&pt2356;','&pt2356;','&pt2356;','&pt3;','&pt3;','&pt3;','&pt3;','&pt356;','&pt235;','&pt26;')"/>
<!-- tests désabrégé
 -->
<xsl:function name="nat:desabrege" as="xs:string" xmlns:nat="http://natbraille.free.fr/xsl">
	<xsl:param name="arg" as="xs:string?"/>
	<xsl:param name="suff" as="xs:string?"/>
	<xsl:choose>
		<!-- ivb -->
		<xsl:when test="starts-with($arg, '&pt56;')">
			<xsl:message select="'ivb:',$arg,$suff"/>
			<xsl:value-of select="concat(substring-after($arg,'&pt56;'),$suff)"/>
		</xsl:when>
		<!-- clefs majuscule, mise en évidence, etc. -->
		<xsl:when test="string-length($arg) &gt; 1 and starts-with($arg,'&pt46;')">
			<xsl:value-of select="concat('&pt46;',nat:desabrege(substring-after($arg,'&pt46;'),''))"/>
		</xsl:when>
		<xsl:when test="string-length($arg) &gt; 1 and starts-with($arg,'&pt25;&pt46;')">
			<xsl:value-of select="concat('&pt25;&pt46;',nat:desabrege(substring-after($arg,'&pt25;&pt46;'),''))"/>
		</xsl:when>
		<!-- ponctuations -->
		<xsl:when test="$suff='' and string-length($arg) &gt; 1 and nat:ends-with-any-of($arg,$puncts)">
			<xsl:message select="'ponctuation finale',$arg"/>
			<xsl:value-of select="nat:desabrege(substring($arg,1,string-length($arg)-1),substring($arg,string-length($arg),1))"/>
		</xsl:when>
		<!-- locutions (pour virer &pt36;) -->
		<xsl:when test="functx:is-value-in-sequence($arg,($loc1Br,$loc2Br,$loc3Br))">
			<xsl:message select="'locution'"/>
			<xsl:value-of select="concat(($loc1,$loc2,$loc3)[index-of(($loc1Br,$loc2Br,$loc3Br),$arg)],$suff)"/>
		</xsl:when>
		<!-- découpage sur tirets TODO règle sur re|in + com en dur...-->
		<xsl:when test="matches($arg,'^.+&pt36;.+') and not(matches($arg,'^(&pt35;|&pt3;)&pt36;'))">
			<xsl:variable name="decoup" select="tokenize($arg, '&pt36;')" as="xs:string*" />
			<!--<xsl:value-of select="nat:desabrege($decoup[1],$suff)"/>-->
			<xsl:variable name="suff1" select="if (count($decoup)=1) then $suff else()"/>
			<xsl:variable name="resu">
				<xsl:for-each select="$decoup[position()&gt;1]">
					<xsl:value-of select="concat('&pt36;',nat:desabrege(.,if(count($decoup)=position()+1) then $suff else ''))"/>
				</xsl:for-each>
			</xsl:variable>
			<xsl:value-of select="concat(nat:desabrege($decoup[1],$suff1),string-join($resu,''))"/>
		</xsl:when>
		<!-- invariants -->
		<xsl:when test="functx:is-value-in-sequence($arg,$invariantsBr)">
			<xsl:message select="'invariant'"/>
			<xsl:value-of select="concat($invariants[index-of($invariantsBr,$arg)],$suff)"/>
		</xsl:when>
		<!-- invariants avec la ponctuation finale-->
		<xsl:when test="not($suff='') and functx:is-value-in-sequence(concat($arg,$suff),$invariantsBr)">
			<xsl:message select="'invariant + ponct'"/>
			<xsl:value-of select="$invariants[index-of($invariantsBr,concat($arg,$suff))]"/>
		</xsl:when>
		<!-- symboles simples -->
		<xsl:when test="functx:is-value-in-sequence($arg,($symbsBr,$symbsNCBr))">
			<xsl:message select="'symbole'"/>
			<xsl:value-of select="concat(($symbs,$symbsNC)[index-of(($symbsBr,$symbsNCBr),$arg)[1]],$suff)"/>
		</xsl:when>
		<!-- règles sur signes -->
		<xsl:otherwise>
			<!-- peut-on appliquer une règle de l'ensemble signe? -->
			<xsl:variable name="trans" select="if(nat:starts-with-any-of($arg,$signesDerivBr)) then nat:matchRules($arg,$signesBrRules,$signesBrRulesBlack,$signesDerivBr,$signesDeriv,1) else ' '" as="xs:string*"/>
			<xsl:choose>
				<xsl:when test="not($trans[1]=' ')">
					<xsl:message select="'signe + règle'"/>
					<xsl:value-of select="concat($trans[1],$suff)"/>
				</xsl:when>
				<xsl:otherwise>
				<!-- règle sur les symboles -->
					<xsl:variable name="transSymb" select="if(nat:starts-with-any-of($arg,($symbsBr,$symbsNCBr))) then nat:matchRules($arg,$symbsBrRules,$symbsBrRulesBlack,($symbsBr,$symbsNCBr),($symbs,$symbsBr),1) else ' '"/>
					<xsl:choose>
						<!-- TODO a améliorer: test féminin ee -->
						<xsl:when test="not($transSymb[1]=' ') and not(ends-with($transSymb[1],'ee'))">
							<xsl:message select="'symbole + règle'"/>
							<xsl:value-of select="concat($transSymb[1],$suff)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:message select="'cas général',$arg"/>
							<!-- on enlève la ponctuation -->
							<xsl:variable name="transGen" as="xs:string?" select="concat(functx:replace-multi($arg,$cgenBrRules,$cgenBrRulesBlack),$suff)"/>
							<!-- on ajoute la ponctuation -->
							<xsl:variable name="transGen2" as="xs:string?" select="functx:replace-multi($transGen,$cgenBrRules,$cgenBrRulesBlack)"/>
							<xsl:variable name="transGen3" as="xs:string?" select="functx:replace-multi($transGen2,$cgenBrPass2Rules,$cgenBrPass2RulesBlack)"/>

							<xsl:value-of select="$transGen3"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:otherwise>
	</xsl:choose>
</xsl:function>
</xsl:stylesheet>
