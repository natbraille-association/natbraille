<?xml version='1.1' encoding="UTF-8" ?>
<!--
* NAT - An universal Translator
* Copyright (C) 2009 Bruno Mascret
* Contact: bmascret@free.fr
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
-->

<!DOCTYPE xsl:stylesheet SYSTEM "nat://system/xsl/mmlents/windob.dtd">

<xsl:stylesheet version="2.0"
xmlns:nat='http://natbraille.free.fr/xsl' 
xmlns:xsl='http://www.w3.org/1999/XSL/Transform' 
xmlns:saxon='http://icl.com/saxon'
xmlns:m='http://www.w3.org/1998/Math/MathML'
xmlns:fn='http://www.w3.org/2005/xpath-functions'
xmlns:functx='http://www.functx.com'
xmlns:xs='http://www.w3.org/2001/XMLSchema'
xmlns:lit='espacelit'
xmlns:doc='espaceDoc'>

<xsl:import href="nat://system/xsl/functions/functx.xsl" /><!-- "functx functions -->
<xsl:include href="nat://system/xsl/nat-functs.xsl" />

<!-- indices du tableau de rajouts -->
<xsl:variable name="rajoutDebDoc" select="1" />
<xsl:variable name="rajoutFinDoc" select="2" />
<xsl:variable name="rajoutDebPar" select="3" />
<xsl:variable name="rajoutFinPar" select="4" />
<xsl:variable name="rajoutDebLigne" select="5" />
<xsl:variable name="rajoutFinLigne" select="6" />
<xsl:variable name="rajoutDebMaths" select="7" />
<xsl:variable name="rajoutFinMaths" select="8" />
<xsl:variable name="rajoutDebLit" select="9" />
<xsl:variable name="rajoutFinLit" select="10" />
<xsl:variable name="rajoutDebMusique" select="11" />
<xsl:variable name="rajoutFinMusique" select="12" />

<xsl:output method="xml" encoding="UTF-8"/><!-- doctype-public="-//W3C//DTD XHTML 1.1//EN" doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd" indent="no"/>-->
<xsl:variable name="depuis" select="($debMath,$finMath,$sautAGenerer,$espace,$debTable,$finTable,$coupe,$coupeEsth,$stopCoup)" />
<xsl:variable name="vers_intro" select="($rajouts[$rajoutDebMaths], $rajouts[$rajoutFinMaths],concat('&#10;',$rajouts[$rajoutFinLigne]),'&pt;')" />
<xsl:variable name="vers" select="(for $i in $vers_intro return nat:escape-for-regex2($i))" />

<xsl:template match="doc:doc">
	<xsl:value-of select="$rajouts[$rajoutDebDoc]" />
	<xsl:apply-templates/>
	<xsl:value-of select="$rajouts[$rajoutFinDoc]" />
	<!-- <xsl:if test="$formFeedEnd">
		<xsl:text>&#12;</xsl:text>
	</xsl:if> -->
</xsl:template>

<xsl:template match="phrase|titre">
	<xsl:value-of select="functx:replace-multi(.,('&amp;',$depuis),('&amp;amp;',$vers))" />
	<!--<xsl:apply-templates/>-->
</xsl:template>

<xsl:template match="*">
	<xsl:copy>
		<xsl:copy-of select="@*"/>
					<xsl:apply-templates/>
	</xsl:copy>
</xsl:template>



<!--<xsl:template match="text()">
	<xsl:value-of select="functx:replace-multi(.,('&amp;',$depuis),('&amp;amp;',$vers))" />
</xsl:template>-->

</xsl:stylesheet>
