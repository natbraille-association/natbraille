<?xml version='1.1' encoding="UTF-8" ?>
<!--
 * NAT - An universal Translator
 * Copyright (C) 2005 Raphaël Mina
 * Contact: raphael.mina@gmail.com
 *          natbraille.Free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
-->

<!-- version: 1.3-b3 -->
<!DOCTYPE xsl:stylesheet SYSTEM "nat://system/xsl/mmlents/windob.dtd">
<!--
<!DOCTYPE xsl:stylesheet SYSTEM "nat://system/xsl/mmlents/windob.dtd"
[
  <!ENTITY % table_braille PUBLIC "table braille" "./tablesBraille/Brltab.ent">
  %table_braille;
  
]>-->


<xsl:stylesheet version="2.0"
xmlns:xsl='http://www.w3.org/1999/XSL/Transform' 
xmlns:saxon='http://icl.com/saxon'
xmlns:xs='http://www.w3.org/2001/XMLSchema'
xmlns:fn='http://www.w3.org/2005/xpath-functions'
xmlns:fo='http://www.w3.org/1999/XSL/Format'
xmlns:m='http://www.w3.org/1998/Math/MathML'
xmlns:lit='espacelit'
xmlns:functx='http://www.functx.com'
xmlns:doc='espaceDoc'
xmlns:nat='http://natbraille.free.fr/xsl'>

<xsl:import href="nat://system/xsl/nat-functs.xsl" /> <!-- NAT functions -->
<xsl:import href="nat://system/xsl/functions/functx.xsl" /> <!-- functx functions -->
<xsl:import href="nat://system/xsl/outilsMaths.xsl" /> <!-- Signes braille et autres-->
<xsl:output
   method="xhtml"
   doctype-public="-//W3C//DTD XHTML 1.1 plus MathML 2.0//EN"
   indent="yes"
   encoding="UTF-8"
   media-type="text/html"
/>

<!--    <xsl:function name="nat:compareCI">
    <xsl:param name="start"/>
    <xsl:param name="end"/>
    <xsl:variable name="positionAttributesBidon">
        <bidon>
            <xsl:attribute name="oso" select="$start"/>
            <xsl:attribute name="oeo" select="$end"/>
        </bidon>
    </xsl:variable>        
    
    <xsl:copy-of select="$positionAttributesBidon//@*"/>
</xsl:function>-->
<xsl:function name="nat:originalTextOffsetAttributes" as="attribute()*">
    <xsl:param name="start" as="xs:integer?"/>
    <xsl:param name="end" as="xs:integer?"/>
    <xsl:if test="$MODE_DETRANS_KEEP_SOURCE_OFFSETS">
        <xsl:attribute name="oso" select="$start"/>
        <xsl:attribute name="oeo" select="$end"/>
    </xsl:if>
</xsl:function>

<xsl:template name="parser">
	<xsl:param name="contenu" as="xs:string"/>
	<xsl:param name="inside2D" as="xs:boolean" select="false()" tunnel="yes"/>
	<xsl:param name="tab2D" as="element()*" select="()"/>
	<xsl:param name="first" as="xs:boolean" select="true()" tunnel="yes"/>
        
        <xsl:param name="originStartOffset" as="xs:integer" select="0"/>
        <xsl:variable name="originEndOffset" as="xs:integer" select="$originStartOffset+string-length($contenu)"/>
        
	<xsl:variable name="premierGroupe" select="substring($contenu,1,nat:finPremierGroupe($contenu,1))"/>
	<xsl:variable name="premierElement" select="substring($contenu,1,nat:indexFinElement($contenu))"/>

<!--        <xsl:if test="$KEEP_SOURCE_OFFSETS">
        <trace-parser><xsl:copy-of select="nat:originalTextOffsetAttributes($originStartOffset,$originEndOffset)"/></trace-parser>
        </xsl:if>-->
	<!--<xsl:message select="($premierGroupe,$premierElement)"/>-->
<!--        <xsl:if test="$KEEP_SOURCE_OFFSETS">
            <trace-parser><xsl:copy-of select="nat:originalTextOffsetAttributes($originStartOffset,$originEndOffset)"/></trace-parser>
        </xsl:if>-->

	<xsl:choose>
		<!-- cas d'une cellule contenant juste la barre d'exclusion -->
		<xsl:when test="$contenu = '&pt123456;' and $inside2D and $first">
			<xsl:call-template name="sortiePremierElement">
                                <!-- TODO position -->
				<xsl:with-param name="contenu" select="$doubleBarre"/>
			</xsl:call-template>
		</xsl:when>
		<xsl:when test="not($contenu) and count($tab2D)=0"/>
		<!-- si on a des indices de blocs 2D : on est dans une structure 2D -->
		<xsl:when test="not($inside2D) and count($tab2D) > 1">
			<xsl:call-template name="structure2D">
                                <!-- TODO position -->
				<xsl:with-param name="tab2D" select="$tab2D" />
			</xsl:call-template>
                </xsl:when>
		<!--Si l'opération du premier bloc est une division-->
		<xsl:when test="substring($contenu,string-length($premierGroupe)+1,1) = $barreFraction">
			<xsl:call-template name="fraction">
                                <xsl:with-param name="originStartOffset" select="$originStartOffset"/>    
				<xsl:with-param name="numerateur" select="$premierGroupe"/>
				<xsl:with-param name="denominateurEtSuite" select="substring(substring-after($contenu,$premierGroupe),2)"/>
			</xsl:call-template>
		</xsl:when>
		<!--Lorsque la chaîne commence par le caractère racine-->
		<xsl:when test="starts-with($contenu,$racine)">
		<!-- Sortie de la partie dans la racine-->
                        <xsl:variable name="endposition" as="xs:integer" select="nat:indexFinElement(substring($contenu,2))"/>
			<m:msqrt>
                                <xsl:copy-of select="nat:originalTextOffsetAttributes($originStartOffset,$originStartOffset+string-length($racine))"/>
				<xsl:call-template name="parser">
                                        <xsl:with-param name="originStartOffset" select="$originStartOffset+1"/>    
					<xsl:with-param name="contenu" select="substring(substring($contenu,2),1,$endposition)"/>
				</xsl:call-template>
			</m:msqrt>
	  <!-- Sortie de la partie après la racine-->
			<xsl:call-template name="parser">
                                <xsl:with-param name="originStartOffset" select="$originStartOffset+$endposition+1"/>    
				<xsl:with-param name="contenu" select="substring(substring($contenu,2),$endposition+1)"/>
			</xsl:call-template>
		</xsl:when>
		<!--Si la chaîne commence par un caractère surscrit-->
		<!-- mais pas avec un caractère ambigü: vercteur et n'implique pas par exemple -->
		<xsl:when test="nat:starts-with-any-of($contenu,$seqSurscrit) and not(nat:starts-with-any-of($contenu,$seqNonSurscrit))">
			<xsl:call-template name="surscrit">
                                <xsl:with-param name="originStartOffset" select="$originStartOffset"/>    
				<xsl:with-param name="contenu" select="$contenu"/>
			</xsl:call-template>
		</xsl:when>
		<!--Si la première partie de la chaîne à parser termine un caractère d'indice ou d'exposant-->
		<xsl:when test="functx:contains-any-of(substring($contenu,string-length($premierElement)+1,1),$caractereBrailleSeq) or nat:starts-with-any-of($contenu,$caractereBrailleSeq)">
			<xsl:variable name="signe" as="xs:string" select="substring($contenu,functx:index-of-string-first(translate($contenu,$caractereBraille,$remplacement),$exposant),1)"/>
			<xsl:choose>
				<!--Le signe détecté est l'indice -->
				<xsl:when test="$signe=$indice">
					<xsl:choose>
						<!-- Dans le cas ou on détecte un second signe d'indice, on passe en mode souscrit-->
						<xsl:when test="substring(substring-after($contenu,$signe),1,1)=$indice">
							<xsl:variable name="double" as="xs:boolean">
								<xsl:choose>
									<xsl:when test="nat:starts-with-any-of(substring-after($contenu,concat($signe,$signe)),($plus,$moins))">
										<xsl:choose>
											<xsl:when test="substring(substring(substring-after($contenu,concat($indice,$indice)),2),nat:indexFinElement(substring(substring-after($contenu,concat($indice,$indice)),2))+1,2)=concat($exposant,$exposant) or starts-with(substring(substring-after($contenu,concat($indice,$indice)),2),concat($exposant,$exposant))">
												<xsl:value-of select="true()"/>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="false()"/>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:when>
									<xsl:otherwise>
										<xsl:choose>
											<xsl:when test="substring(substring-after($contenu,concat($indice,$indice)),nat:indexFinElement(substring-after($contenu,concat($indice,$indice)))+1,2)=concat($exposant,$exposant)">
												<xsl:value-of select="true()"/>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="false()"/>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:variable>
							<xsl:choose>
							<!--Le bloc est il suivi d'un double signe d'exposant ? Dans ce cas, on est en mode souscrit et surscrit à la fois-->
								<xsl:when test="$double">
									<m:munderover>
										<xsl:call-template name="indice">                                                                                       
                                                                                        <xsl:with-param name="originStartOffset" select="$originStartOffset"/>
											<xsl:with-param name="contenu" select="$contenu"/>
											<xsl:with-param name="double" select="true()"/>
											<xsl:with-param name="signe" select="concat($signe,$signe)"/>
										</xsl:call-template>
									</m:munderover>
									<!--On sort la partie après ce qui est surscrit-->
                                                                        <xsl:variable name="x" as="xs:string" select="nat:postIndice($contenu,concat($exposant,$exposant))"/>
									<xsl:call-template name="parser">
                                                                                <xsl:with-param name="originStartOffset" select="$originStartOffset+string-length($contenu)-string-length($x)"/>
										<xsl:with-param name="contenu" select="$x"/>
									</xsl:call-template>
								</xsl:when>
								<!--Sinon, on est dans le cas d'un souscrit simple-->
								<xsl:otherwise>
									<m:munder>
										<xsl:call-template name="indice">
                                                                                        <xsl:with-param name="originStartOffset" select="$originStartOffset"/>
											<xsl:with-param name="contenu" select="$contenu"/>
											<xsl:with-param name="double" select="false()"/>
											<xsl:with-param name="signe" select="concat($signe,$signe)"/>
										</xsl:call-template>
									</m:munder>
									<!--On sort la partie après ce qui est souscrit-->
                                                                        <xsl:variable name="x" as="xs:string" select="nat:postIndice($contenu,concat($indice,$indice))"/>
									<xsl:call-template name="parser">
                                                                                <xsl:with-param name="originStartOffset" select="$originStartOffset+string-length($contenu)-string-length($x)"/>                                                                              
										<xsl:with-param name="contenu" select="$x"/>                                                                                
									</xsl:call-template>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						<!-- Sinon, on est dans le cas d'un indice simple-->
						<xsl:otherwise>
							<xsl:variable name="double" as="xs:boolean">
								<xsl:choose>
									<xsl:when test="nat:starts-with-any-of(substring-after($contenu,$signe),($plus,$moins))">
										<xsl:choose>
											<xsl:when test="substring(substring(substring-after($contenu,$signe),2),nat:indexFinElement(substring(substring-after($contenu,$signe),2))+1,1)=$exposant or starts-with(substring(substring-after($contenu,$signe),2),$exposant)">
												<xsl:value-of select="true()"/>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="false()"/>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:when>
									<xsl:otherwise>
										<xsl:choose>
											<xsl:when test="substring(substring-after($contenu,$indice),nat:indexFinElement(substring-after($contenu,$indice))+1,1)=$exposant">
												<xsl:value-of select="true()"/>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="false()"/>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:variable>
							<xsl:choose>
								<!--Le bloc est il suivi d'un signe d'exposant ? Dans ce cas, on est en mode msupsub: indice et exposant à la fois-->
								<xsl:when test="$double">
                                                                        <xsl:variable name="postIndice" as="xs:string" select="nat:postIndice($contenu,$exposant)"/>
									<m:msubsup>
										<xsl:call-template name="indice">
                                                                                        <xsl:with-param name="originStartOffset" select="$originStartOffset"/> 
											<xsl:with-param name="contenu" select="$contenu"/>
											<xsl:with-param name="double" select="true()"/>
											<xsl:with-param name="signe" select="$signe"/>
										</xsl:call-template>
									</m:msubsup>
									<!--On sort la partie après ce qui est en exposant-->
									<xsl:call-template name="parser">
                                                                                <xsl:with-param name="originStartOffset" select="$originStartOffset+string-length($contenu)-string-length($postIndice)"/>    											
										<xsl:with-param name="contenu" select="$postIndice"/>
									</xsl:call-template>
								</xsl:when>
								<!--Sinon, on est dans le cas d'un indice simple-->
								<xsl:otherwise>
									<xsl:choose>
										<xsl:when test="starts-with($contenu,$indice)">
											<xsl:variable name="contenuApresSigne" select="substring($contenu,2,nat:indexFinElement(substring($contenu,2)))"/>
											<m:msub>                                                                                               
												<m:mi></m:mi>
												<xsl:call-template name="parser">
                                                                                                        <xsl:with-param name="originStartOffset" select="$originStartOffset+string-length($indice)"/>
													<xsl:with-param name="contenu" select="$contenuApresSigne"/>
												</xsl:call-template>
											</m:msub>
											<xsl:call-template name="parser">
                                                                                                <xsl:with-param name="originStartOffset" select="$originStartOffset+string-length($contenuApresSigne)+string-length($indice)"/>
												<xsl:with-param name="contenu" select="substring($contenu,string-length($contenuApresSigne)+2)"/>
											</xsl:call-template>
										</xsl:when>
										<xsl:otherwise>
                                                                                        <xsl:variable name="postIndice" as="xs:string" select="nat:postIndice($contenu,$indice)"/>
											<m:msub>
												<xsl:call-template name="indice">
                                                                                                        <xsl:with-param name="originStartOffset" select="$originStartOffset"/>    
													<xsl:with-param name="contenu" select="$contenu"/>
													<xsl:with-param name="double" select="false()"/>
													<xsl:with-param name="signe" select="$signe"/>
												</xsl:call-template>
											</m:msub>
											<!--On sort la partie après ce qui est en indice-->
											<xsl:call-template name="parser">
                                                                                                <xsl:with-param name="originStartOffset" select="$originStartOffset+string-length($contenu)-string-length($postIndice)"/>                                                                                               
												<xsl:with-param name="contenu" select="$postIndice"/>
											</xsl:call-template>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<!--Le signe détecté est le signe d'exposant-->
				<xsl:when test="$signe=$exposant">
					<!--Plusieurs cas possibles : exposant simple, surscrit, ou puissance de racine-->
					<xsl:choose>
						<!-- Si le caractère suivant le signe d'exposant est un signe d'exposant, surscrit-->
						<xsl:when test="starts-with(substring-after($contenu,$exposant),$exposant)">
							<m:mover>
								<xsl:call-template name="indice">
                                                                        <xsl:with-param name="originStartOffset" select="$originStartOffset"/>    
									<xsl:with-param name="contenu" select="$contenu"/>
									<xsl:with-param name="double" select="false()"/>
									<xsl:with-param name="signe" select="concat($signe,$signe)"/>
								</xsl:call-template>
							</m:mover>
							<!--On sort la partie après ce qui est en surscrit-->
                                                        <xsl:variable name="x" as="xs:string" select="nat:postIndice($contenu,concat($exposant,$exposant))"/>
							<xsl:call-template name="parser">
                                                                <xsl:with-param name="originStartOffset" select="$originStartOffset+string-length($contenu)-string-length($x)"/>    							
								<xsl:with-param name="contenu" select="$x"/>
							</xsl:call-template>
						</xsl:when>
						<!--Si le contenu en exposant est suivi d'une racine, on est dans le cas d'une racine autre que carrée, ou d'un produit d'une puissance par une racine.
						On respecte dans ce cas les priorités braille-->
						<xsl:when test="substring(substring-after($contenu,$signe),nat:indexFinElement(substring-after($contenu,$signe))+1,1) = $racine">
                                                        <xsl:variable name="x" as="xs:string" select="substring-after($contenu,$racine)"/>
                                                        <xsl:variable name="y" as="xs:string" select="substring-before($contenu,$racine)"/>
							<xsl:choose>
								<!--Si la chaîne en cours début par le caractère d'exposant, on est dans le cas d'une puissance de racine-->
								<xsl:when test="starts-with($contenu,$signe)">
									<m:mroot>  
                                                                                <!--Sortie du contenu en racine-->
										<xsl:call-template name="parser">
                                                                                        <xsl:with-param name="originStartOffset" select="$originStartOffset+string-length($y)+string-length($racine)"/>
											<xsl:with-param name="contenu" select="substring($x,1,nat:indexFinElement($x))"/>
										</xsl:call-template>
										<!--Sortie de l'index de la racine-->
										<xsl:call-template name="sortiePremierElement">
                                                                                        <xsl:with-param name="originStartOffset" select="$originStartOffset+string-length($exposant)"/>
											<xsl:with-param name="contenu" select="substring-after($y,$exposant)"/>
										</xsl:call-template>
									</m:mroot>
									<!--On sort la partie après ce qui est dans la racine-->
                                                                        <xsl:variable name="z" as="xs:string" select="substring($x,nat:indexFinElement($x)+1)"/>
									<xsl:call-template name="parser">
                                                                                <xsl:with-param name="originStartOffset" select="$originStartOffset+string-length($contenu)-string-length($z)"/>
										<xsl:with-param name="contenu" select="$z"/>
									</xsl:call-template>
								</xsl:when>
								<xsl:otherwise>
                                                                        <!-- TODO write test case -->
									<xsl:call-template name="parser">
                                                                                <!-- TODO position -->
                                                                                <!--<xsl:with-param name="originStartOffset" select="$originStartOffset"/>-->
										<xsl:with-param name="contenu" select="$y"/>
									</xsl:call-template>
									<xsl:call-template name="parser">
                                                                                <!-- TODO position -->
                                                                                <!--<xsl:with-param name="originStartOffset" select="$originStartOffset+string-length($y)"/>-->
										<xsl:with-param name="contenu" select="substring-after($contenu,$y)"/>
									</xsl:call-template>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						<!--Si la chaîne commence par le caractère d'exposant, c'est que le début de la chaîne est un caractère en exposant.-->
						<xsl:when test="starts-with($contenu,$exposant)">
							<xsl:variable name="contenuApresSigne" select="substring($contenu,2)"/>
							<m:msup>
								<m:mi></m:mi>
									<xsl:choose>
									<!--Si la partie en indice ou en souscrit commence par un moins, on l'encadre par des row-->
									<xsl:when test="starts-with($contenuApresSigne,$moins)">
										<m:mrow>
										<!-- Sortie de la partie en indice-->
											<xsl:call-template name="parser">
                                                                                                <xsl:with-param name="originStartOffset" select="$originStartOffset+1"/>
												<xsl:with-param name="contenu" select="substring($contenuApresSigne,1,nat:indexFinElement($contenuApresSigne))"/>
											</xsl:call-template>
										</m:mrow>
									</xsl:when>
									<xsl:otherwise>
										<xsl:call-template name="parser">
                                                                                        <xsl:with-param name="originStartOffset" select="$originStartOffset+1"/>
											<xsl:with-param name="contenu" select="substring($contenuApresSigne,1,nat:indexFinElement($contenuApresSigne))"/>
										</xsl:call-template>
									</xsl:otherwise>
								</xsl:choose>
							</m:msup>
                                                        <xsl:variable name="x" as="xs:string" select="substring($contenuApresSigne,nat:indexFinElement($contenuApresSigne)+1)" />
							<xsl:call-template name="parser">
                                                                <xsl:with-param name="originStartOffset" select="$originStartOffset+string-length($contenu)-string-length($x)"/>
								<xsl:with-param name="contenu" select="$x"/>
							</xsl:call-template>
						</xsl:when>
						<!-- Sinon, il s'agit d'un exposant standard-->
						<xsl:otherwise>
                                                        <xsl:variable name="postIndice" as="xs:string" select="nat:postIndice($contenu,$exposant)"/>
							<m:msup>
								<xsl:call-template name="indice">
                                                                        <xsl:with-param name="originStartOffset" select="$originStartOffset"/>    
									<xsl:with-param name="contenu" select="$contenu"/>
									<xsl:with-param name="double" select="false()"/>
									<xsl:with-param name="signe" select="$signe"/>
								</xsl:call-template>
							</m:msup>
							<!--On sort la partie après ce qui est en exposant-->
							<xsl:call-template name="parser">
                                                                <xsl:with-param name="originStartOffset" select="$originStartOffset+string-length($contenu)-string-length($postIndice)"/>    
								<xsl:with-param name="contenu" select="$postIndice"/>
							</xsl:call-template>
						</xsl:otherwise>
					</xsl:choose>     
				</xsl:when>
				<xsl:otherwise/>
			</xsl:choose>
		</xsl:when>
		<xsl:otherwise>      
			<xsl:call-template name="sortiePremierElement">
                                <xsl:with-param name="originStartOffset" select="$originStartOffset"/>
				<xsl:with-param name="contenu" select="$contenu"/>
			</xsl:call-template>
		</xsl:otherwise>
	</xsl:choose>
        <!--</trace-parser>-->
</xsl:template>


<!--Renvoie l'index de fin d'une chaîne en appliquant les règles de fin propres aux indices et exposants notamment-->
<xsl:function name="nat:indexFinElement" as="xs:integer" xmlns:nat="http://natbraille.free.fr/xsl">
	<xsl:param name="contenu" as="xs:string"/>

	<xsl:choose>
		<xsl:when test="nat:starts-with-any-of($contenu,$seqAlphabet)">
			<xsl:value-of select="1"/>
		</xsl:when>
		<!--Si un début de bloc double débute la chaîne-->
		<xsl:when test="functx:contains-any-of(substring($contenu,1,2),$seqOuvrantDouble)">
			<xsl:variable name="caractereDebutBloc" select ="$seqOuvrantDouble[index-of($seqOuvrantDouble,substring($contenu,1,2))]" />
			<xsl:variable name="caractereFinBloc" select ="$seqFermantDouble[index-of($seqOuvrantDouble,substring($contenu,1,2))]" />
			<!-- Index de fin du bloc -->
			<xsl:value-of select="nat:finBloc($contenu,$caractereDebutBloc,$caractereFinBloc,0,1)"/>
			<!--TODO : optimisation en dessus : inverser les valeurs de 0 et 1-->
		</xsl:when>
		<!--Si un début de bloc simple débute la chaîne-->
		<xsl:when test="functx:contains-any-of(substring($contenu,1,1),$seqOuvrantSimple)">
			<xsl:variable name="caractereDebutBloc" select ="$seqOuvrantSimple[index-of($seqOuvrantSimple,substring($contenu,1,1))]" />
			<xsl:variable name="caractereFinBloc" select ="$seqFermantSimple[index-of($seqOuvrantSimple,substring($contenu,1,1))]" />
			<!-- Index de fin du bloc-->
			<xsl:value-of select="nat:finBloc($contenu,$caractereDebutBloc,$caractereFinBloc,0,1)"/>
		</xsl:when>
		<!--Si un chiffre débute la chaine-->
		<xsl:when test="matches(substring($contenu,1,1),concat('[',$chaineChiffres,']'))">
			<xsl:value-of select="nat:finNombre($contenu)"/>		 
		</xsl:when>
		<xsl:when test="nat:starts-with-any-of($contenu,$seqOperatorSansPrefixe)">
			<xsl:choose>
				<xsl:when test="nat:starts-with-any-of(substring($contenu,1,2),$seqOperatorSansPrefixe)">
					<xsl:value-of select="2"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="3"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		<!--Contient un operator spécial-->
		<xsl:when test="nat:starts-with-any-of($contenu,$seqOperatorSpeciaux)">
			<xsl:choose>
				<xsl:when test="functx:is-value-in-sequence(substring($contenu,1,4),$seqOperatorSpeciaux)">
					<xsl:value-of select="4"/>
				</xsl:when>
				<xsl:when test="functx:is-value-in-sequence(substring($contenu,1,3),$seqOperatorSpeciaux)">
					<xsl:value-of select="3"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="2"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		<!--Contient un operator simple-->
		<xsl:when test="nat:starts-with-any-of($contenu,$seqOperatorSimple)">
			<xsl:value-of select="1"/>
		</xsl:when>
		<!-- Commence par un préfixe (opérateur ou majuscule derrière)-->
		<xsl:when test="nat:starts-with-any-of($contenu,$seqPrefixe)">
			<xsl:value-of select="nat:finPrefixe($contenu)"/>
		</xsl:when>
		<!-- Sinon, a priori c'est une lettre -->
		<xsl:otherwise>
			<xsl:value-of select="1"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:function>


<!-- Donne la fin du bloc passé en paramètre, à partir de son premier caractère-->
<xsl:function name = "nat:finBloc" as="xs:integer" xmlns:nat="http://natbraille.free.fr/xsl">
	<xsl:param name = "contenu"/>
	<xsl:param name = "caractereDebutBloc"/>
	<xsl:param name = "caractereFinBloc"/>
	<xsl:param name = "profondeur" />
	<xsl:param name = "position" />

	<xsl:variable name = "retrait " select="string-length($caractereDebutBloc)-1"/>
	<xsl:choose>
		<!-- Fin trouvé, qui n'est pas dans un noeud. C'est terminé -->
		<xsl:when test = "substring($contenu, 1, 1 + $retrait) = $caractereFinBloc and $profondeur = 1">
			<xsl:value-of select = "$position+$retrait"/>
		</xsl:when>
		<!-- Début trouvé. On incrémente le compteur de noeuds et on continue. -->
		<xsl:when test = "substring($contenu, 1, 1 + $retrait) = $caractereDebutBloc">
			<xsl:value-of select = "nat:finBloc(substring($contenu, 2), $caractereDebutBloc, $caractereFinBloc, $profondeur + 1, $position+1)"/>
		</xsl:when>
		<!-- On a trouvé une fermeture à l'intérieur d'un noeud. On décrément et on continue. -->
		<xsl:when test = "substring($contenu, 1, 1 + $retrait) = $caractereFinBloc">
			<xsl:value-of select = "nat:finBloc(substring($contenu, 2), $caractereDebutBloc, $caractereFinBloc, $profondeur - 1, $position+1)"/>
		</xsl:when>
		<!-- Fin du texte trouvé sans bloc ouvrant correspondant. Problème en entrée -->
		<xsl:when test = "$contenu = '' and $profondeur != 0">
			<xsl:value-of select="string-length($caractereDebutBloc)"/>
		</xsl:when>
		<!-- Dans les autres cas, on scanne le reste de la chaîne. -->
		<xsl:otherwise>
			<xsl:value-of select="nat:finBloc(substring($contenu, 2), $caractereDebutBloc, $caractereFinBloc, $profondeur, $position + 1)"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:function>

<!-- Donne la position de la fin du premier élément du chaîne, lorsqu'elle débute par un "préfixe".-->
<xsl:function name="nat:finPrefixe" as="xs:integer" xmlns:nat="http://natbraille.free.fr/xsl">
	<xsl:param name="contenu"/>

	<xsl:choose>
		<xsl:when test="starts-with($contenu,$point46)">
			<xsl:choose>
				<xsl:when test="nat:starts-with-any-of(substring($contenu,2),$seqAlphabet)">
					<xsl:value-of select="2"/>
				</xsl:when>
				<xsl:when test="functx:contains-any-of(substring($contenu,2,1),($point45,$point46,$point5))">
					<xsl:choose>
						<xsl:when test="functx:contains-any-of(substring($contenu,3,1),$seqAlphabet)">
							<xsl:value-of select="3"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="nat:finOperatorPrefixe($contenu)"/>
						</xsl:otherwise>
					</xsl:choose>	  
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="nat:finOperatorPrefixe($contenu)"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		<xsl:when test="starts-with($contenu,$point45)">
			<xsl:choose>
				<xsl:when test="substring($contenu,2,1)=$point45 and functx:contains-any-of(substring($contenu,3,1),$seqAlphabet)">
					<xsl:value-of select="3"/>
				</xsl:when>
				<xsl:when test="functx:contains-any-of(substring($contenu,2,1),$seqAlphabet)">
					<xsl:value-of select="2"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="nat:finOperatorPrefixe($contenu)"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		<xsl:when test="starts-with($contenu,$point5) and functx:contains-any-of(substring($contenu,2,1),$seqAlphabet)">
			<xsl:value-of select="2"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="nat:finOperatorPrefixe($contenu)"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:function>

<!-- Donne la position de la fin d'un caractère préfixé qui débute une chaîne, et qui n'est pas une lettre spéciale.-->
<xsl:function name="nat:finOperatorPrefixe" as="xs:integer" xmlns:nat="http://natbraille.free.fr/xsl">
	<xsl:param name="contenu"/>

	<xsl:variable name="seqRetenue" as="xs:string*">
		<xsl:choose>
			<xsl:when test="starts-with($contenu,$point46)">
				<xsl:sequence select="$seqOperatorPt46"/>
			</xsl:when>
			<xsl:when test="starts-with($contenu,$point456)">
				<xsl:sequence select="$seqOperatorPt456"/>
			</xsl:when>
			<xsl:when test="starts-with($contenu,$point45)">
				<xsl:sequence select="$seqOperatorPt45"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:sequence select="$seqOperatorPt5"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:choose>
		<xsl:when test="functx:is-value-in-sequence(substring($contenu,1,4),$seqRetenue)">
			<xsl:value-of select="4"/>
		</xsl:when>
		<xsl:when test="functx:is-value-in-sequence(substring($contenu,1,3),$seqRetenue)">
			<xsl:value-of select="3"/>
		</xsl:when>
		<xsl:when test="functx:is-value-in-sequence(substring($contenu,1,2),$seqRetenue)">
			<xsl:value-of select="2"/>
		</xsl:when>
		<!--Cas où le préfixe n'a pas de sens trouvé. Il est ignoré-->
		<xsl:otherwise>
			<xsl:value-of select="1"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:function>

<!-- Donne la position de la fin d'un caractère préfixé qui début une chaîne-->
<xsl:function name="nat:finNombre" as="xs:integer" xmlns:nat="http://natbraille.free.fr/xsl">
	<xsl:param name="contenu"/>
	<xsl:value-of select="string-length($contenu) - string-length(functx:substring-after-match($contenu,concat('[',$chaineChiffres,']+[',$virgule,']?[',$chaineChiffres,']*')))"/>
</xsl:function>

<xsl:template name="sortiePremierElement">
	<xsl:param name="contenu" as="xs:string"/>
        <xsl:param name="originStartOffset" as="xs:integer" select="0"/>
        <xsl:variable name="originEndOffset" as="xs:integer" select="$originStartOffset+string-length($contenu)"/>
<!--        <trace-sortiePremierElement><xsl:copy-of select="nat:originalTextOffsetAttributes($originStartOffset,$originEndOffset)"/>
        <trace-sortiePremierElement>-->

	<xsl:variable name="premierElement" as="xs:string" select="substring($contenu,1,nat:indexFinElement($contenu))"/>
	<xsl:choose>
		<!--Si la chaîne commence par un bloc double, elle n'est probablement pas atomique.
			On l'encadre donc de la balise <m:mrow>-->
		<xsl:when test="nat:starts-with-any-of($contenu,$seqOuvrantDouble)">
			<m:mrow>
				<!--Sortie du bloc ouvrant-->
				<m:mo>                                   
                                    <xsl:copy-of select="nat:originalTextOffsetAttributes($originStartOffset,$originStartOffset+2)"/>
                                    <xsl:value-of select="$seqOuvrantDoubleMML[index-of($seqOuvrantDouble,substring($contenu,1,2))]"/>
                                </m:mo>
				<!-- Sortie du contenu entre bloc de deux caractères-->
				<xsl:call-template name="parser">
                                        <xsl:with-param name="originStartOffset" select="$originStartOffset+2"/>    
					<xsl:with-param name="contenu" select="substring($premierElement,3,string-length($premierElement)-3)"/>
				</xsl:call-template>
				<!--Sortie du bloc fermant-->
				<m:mo>
                                    <xsl:copy-of select="nat:originalTextOffsetAttributes($originStartOffset+string-length($premierElement)-2,$originStartOffset+string-length($premierElement))"/>                                    
                                    <xsl:value-of select="$seqFermantDoubleMML[index-of($seqFermantDouble,substring($premierElement,string-length($premierElement)-1,2))]"/>
                                </m:mo>
			</m:mrow>
		</xsl:when>
		<!--Si la chaîne commence par un bloc simple, elle n'est probablement pas atomique.
			On l'encadre donc de la balise <m:mrow>-->
		<xsl:when test="nat:starts-with-any-of($contenu,$seqOuvrantSimple)">
			<m:mrow>
				<!--Sortie du bloc ouvrant-->
				<xsl:variable name="sorBO" select="$seqOuvrantSimpleMML[index-of($seqOuvrantSimple,substring($contenu,1,1))]"/>
				<xsl:if test="not($sorBO='' or empty($sorBO))">
                                    <m:mo>
                                        <xsl:copy-of select="nat:originalTextOffsetAttributes($originStartOffset,$originStartOffset+1)"/>
                                        <xsl:value-of select="$sorBO"/>
                                    </m:mo>
                                </xsl:if>
				<!-- Sortie du contenu entre bloc de deux caractères-->
				<xsl:call-template name="parser">
                                        <xsl:with-param name="originStartOffset" select="$originStartOffset+1"/>    
					<xsl:with-param name="contenu" select="substring($premierElement,2,string-length($premierElement)-2)"/>
				</xsl:call-template>
				<!--Sortie du bloc fermant-->
				<xsl:variable name="sorBF" select="$seqFermantSimpleMML[index-of($seqFermantSimple,substring($premierElement,string-length($premierElement),1))]"/>
				<xsl:if test="not($sorBF='' or empty($sorBF))">
                                    <m:mo>
                                        <xsl:copy-of select="nat:originalTextOffsetAttributes($originStartOffset+string-length($premierElement)-1,$originStartOffset+string-length($premierElement))"/>                                    
                                        <xsl:value-of select="$sorBF"/>
                                    </m:mo>
                                </xsl:if>
			</m:mrow>
		</xsl:when>
		<!--Si un chiffre débute la chaine-->
		<xsl:when test="matches(substring($contenu,1,1),concat('[',$chaineChiffres,']'))">                    
			<m:mn>
                                <xsl:copy-of select="nat:originalTextOffsetAttributes($originStartOffset,$originStartOffset+string-length($premierElement))"/>
				<xsl:value-of select="translate($premierElement,$alphabetBraille,$alphabetNoir)"/>
			</m:mn>
		</xsl:when>
		<!--Si la chaîne commence par un opérator sans préfixe-->
		<xsl:when test="nat:starts-with-any-of($contenu,$seqOperatorSansPrefixe)">
			<m:mo>
                                <xsl:copy-of select="nat:originalTextOffsetAttributes($originStartOffset,$originStartOffset+string-length($premierElement))"/>
				<xsl:value-of select="$seqOperatorSansPrefixeMML[index-of($seqOperatorSansPrefixe,$premierElement)]"/>
			</m:mo>
		</xsl:when>
		<!--Si la chaîne commence par un opérator spécial (opérateur pouvant poser problème lors de la détranscription car mélangeant des caractères de préfixe)-->
		<xsl:when test="nat:starts-with-any-of($contenu,$seqOperatorSpeciaux)">
			<m:mo>
                                <xsl:copy-of select="nat:originalTextOffsetAttributes($originStartOffset,$originStartOffset+string-length($premierElement))"/>
				<xsl:value-of select="$seqOperatorSpeciauxMML[index-of($seqOperatorSpeciaux,$premierElement)]"/>
			</m:mo>
		</xsl:when>
		<!--Si la chaîne commence par un opérator d'un caractère-->
		<xsl:when test="nat:starts-with-any-of($contenu,$seqOperatorSimple)">
			<m:mo>
                                <xsl:copy-of select="nat:originalTextOffsetAttributes($originStartOffset,$originStartOffset+1)"/>
				<xsl:value-of select="$seqOperatorSimpleMML[index-of($seqOperatorSimple,$premierElement)]"/>
			</m:mo>
		</xsl:when>
		<!--Si la chaîne commence par un préfixe d'operateur-->
		<xsl:when test="nat:starts-with-any-of($contenu,$seqPrefixe)">
			<xsl:call-template name="sortieOperator">
                                <xsl:with-param name="originStartOffset" select="$originStartOffset"/>    
				<xsl:with-param name="contenu" select="$premierElement"/>
			</xsl:call-template>
		</xsl:when>
		<!-- Sinon, on peut se passer des row-->
		<xsl:otherwise>
			<m:mi>
                              <xsl:copy-of select="nat:originalTextOffsetAttributes($originStartOffset,$originStartOffset+string-length($premierElement))"/>
                              <xsl:value-of select="translate($premierElement,$alphabetBraille,$alphabetNoir)"/>
			</m:mi>
		</xsl:otherwise>
	</xsl:choose>
	<xsl:call-template name="parser">           
                <xsl:with-param name="originStartOffset" select="$originStartOffset+string-length($premierElement)"/>                    
		<xsl:with-param name="contenu" select="substring-after($contenu,$premierElement)"/>
		<xsl:with-param name="first" select="false()" tunnel="yes"/>
	</xsl:call-template>
        <!--</trace-sortiePremierElement>-->
</xsl:template>

<!-- Ce template extrait une fraction dont on lui passe le numérateur, et une chaine dont le début est le dénominateur-->
<xsl:template name="fraction">
	<xsl:param name="numerateur"/>
	<xsl:param name="denominateurEtSuite"/>
        <xsl:param name="originStartOffset" as="xs:integer" select="0"/>

	<xsl:variable name="denominateur">
		<!-- Extraction du dénominateur. 
		S'il commence par un bloc, alors il se termine à la fermeture de ce bloc. Sinon, on utilise la fonction finPremierGroupe -->
		<xsl:choose>
			<xsl:when test="nat:starts-with-any-of($denominateurEtSuite,$seqOuvrantSimple)">
				<xsl:value-of select="substring($denominateurEtSuite,1,nat:finBloc($denominateurEtSuite,substring($denominateurEtSuite,1,1),$seqFermantSimple[index-of($seqOuvrantSimple,substring($denominateurEtSuite,1,1))],0,1))"/>
			</xsl:when>
			<xsl:when test="functx:contains-any-of(substring($denominateurEtSuite,1,2),$seqOuvrantDouble)">
				<xsl:value-of select="substring($denominateurEtSuite,1,nat:finBloc($denominateurEtSuite,substring($denominateurEtSuite,1,2),$seqFermantDouble[index-of($seqOuvrantDouble,substring($denominateurEtSuite,1,1))],0,1))"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="substring($denominateurEtSuite,1,nat:finPremierGroupe($denominateurEtSuite,1))"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<!--On détermine si le dénominateur est suivi d'un signe de fraction. En ce cas, numérateur et dénominateur forment un nouveau numérateur pour
	une nouvelle fraction-->
	<xsl:choose>
		<xsl:when test="functx:contains-any-of(substring(substring-after($denominateurEtSuite,$denominateur),1,1),($barreFraction,$signeDivision))">
			<xsl:call-template name="fraction">
                                <xsl:with-param name="originStartOffset" select="$originStartOffset"/>
				<xsl:with-param name="numerateur" select="concat($numerateur,$barreFraction,$denominateur)"/>
				<xsl:with-param name="denominateurEtSuite" select="substring(substring-after($denominateurEtSuite,$denominateur),2)"/>
			</xsl:call-template>
		</xsl:when>
		<!--Si le dénominateur n'est pas suivi d'un signe de fraction, on a atteint la fin de la fraction. On la présente puis on passe au reste de la chaîne-->
		<xsl:otherwise>
			<m:mfrac>
                                <xsl:copy-of select="nat:originalTextOffsetAttributes($originStartOffset,$originStartOffset+string-length($numerateur)+string-length($denominateur)+1)"/>
				<m:mrow>
                                        <xsl:copy-of select="nat:originalTextOffsetAttributes($originStartOffset,$originStartOffset+string-length($numerateur))"/>    
					<xsl:call-template name="parser">
                                                <xsl:with-param name="originStartOffset" select="$originStartOffset"/>
						<xsl:with-param name="contenu" select="$numerateur"/>
					</xsl:call-template>
				</m:mrow>
				<m:mrow>
                                        <xsl:copy-of select="nat:originalTextOffsetAttributes($originStartOffset+string-length($numerateur)+1,$originStartOffset+string-length($numerateur)+string-length($denominateur)+1)"/>    
					<xsl:call-template name="parser">
                                                <xsl:with-param name="originStartOffset" select="$originStartOffset+string-length($numerateur)+1"/>    
						<xsl:with-param name="contenu" select="$denominateur"/>
					</xsl:call-template>
				</m:mrow>
			</m:mfrac>
			<xsl:call-template name="parser">
                                <xsl:with-param name="originStartOffset" select="$originStartOffset+string-length($numerateur)+string-length($denominateur)+1"/>    
				<xsl:with-param name="contenu" select="substring-after($denominateurEtSuite,$denominateur)"/>
			</xsl:call-template>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="indice">
	<xsl:param name="contenu" as="xs:string"/>
	<xsl:param name="signe" as="xs:string"/>
	<xsl:param name="double" as="xs:boolean"/>        
        <xsl:param name="originStartOffset" as="xs:integer" select="0"/>
        <xsl:variable name="originEndOffset" as="xs:integer" select="$originStartOffset+string-length($contenu)"/>

	<!--On divise la chaine à traiter en deux : avant et apres le signe (éventuellement double) d'indice-->
	<xsl:variable name="contenuAvantSigne" select="substring-before($contenu,$signe)"/>
	<xsl:variable name="contenuApresSigne" select="substring-after($contenu,$signe)"/>
	<!--Sortie de la partie ayant un indice-->
	<m:mrow>
                <xsl:copy-of select="nat:originalTextOffsetAttributes($originStartOffset,$originStartOffset+string-length($contenuAvantSigne))"/>
		<xsl:call-template name="sortiePremierElement">
                        <xsl:with-param name="originStartOffset" select="$originStartOffset"/>    
			<xsl:with-param name="contenu" select="$contenuAvantSigne"/>
		</xsl:call-template>
	</m:mrow>
	<!--Sortie du reste-->
	<xsl:choose>
		<!-- Si on doit sortir une partie en exposant ou en surscrit en plus de celle en indice-->
		<xsl:when test="$double=true()">
			<xsl:choose>
				<!--Si la chaîne commence par un moins ou un plus, on l'encadre par des row-->
				<xsl:when test="nat:starts-with-any-of(substring-before($contenuApresSigne,$exposant),($moins,$plus))">
					<m:mrow>
						<!-- Sortie de la partie en indice-->
						<xsl:call-template name="parser">
                                                        <xsl:with-param name="originStartOffset" select="$originStartOffset+string-length($contenu)-string-length($contenuApresSigne)"/>    
							<xsl:with-param name="contenu" select="substring-before($contenuApresSigne,$exposant)"/>
						</xsl:call-template>
					</m:mrow>
				</xsl:when>
				<xsl:otherwise>
					<!-- Sortie de la partie en indice-->
					<xsl:call-template name="parser">
                                                <xsl:with-param name="originStartOffset" select="$originStartOffset+string-length($contenu)-string-length($contenuApresSigne)"/>    
						<xsl:with-param name="contenu" select="substring-before($contenuApresSigne,$exposant)"/>
					</xsl:call-template>
				</xsl:otherwise>
			<!--Fin de la sortie de l'indice en mode double-->
			</xsl:choose>
			<!--Sortie de la partie en exposant (en mode double)-->

                        <xsl:variable name="x" as="xs:string" select="functx:substring-after-match($contenuApresSigne,concat($exposant,'+'))"/>
			<xsl:choose>
				<!--Si la chaîne commence par un moins ou un plus, on l'encadre par des row-->
				<xsl:when test="nat:starts-with-any-of(substring($x,1),($plus,$moins))">
					<m:mrow>
						<!--Sortie du plus ou du moins-->
						<xsl:call-template name="sortiePremierElement">
                                                        <xsl:with-param name="originStartOffset" select="$originStartOffset+string-length($contenu)-string-length($x)"/>    
							<xsl:with-param name="contenu" select="substring($x,1,1)"/>
						</xsl:call-template>					<!-- Sortie du reste de la partie en exposant ou surscrit-->
						<xsl:call-template name="parser">
                                                        <xsl:with-param name="originStartOffset" select="$originStartOffset+string-length($contenu)-string-length($x)+1"/>    
							<xsl:with-param name="contenu" select="substring($x,2,nat:indexFinElement(substring($x,2)))"/>
						</xsl:call-template>
					</m:mrow>
				</xsl:when>
				<xsl:otherwise>
					<!-- Sortie de la partie en exposant ou souscrit-->
                                        <xsl:variable name="x" as="xs:string" select="functx:substring-after-match($contenuApresSigne,concat($exposant,'+'))"/>
					<xsl:call-template name="parser">
                                                <xsl:with-param name="originStartOffset" select="$originStartOffset+string-length($contenu)-string-length($x)"/>    
						<xsl:with-param name="contenu" select="substring($x,1,nat:indexFinElement($x))"/>
					</xsl:call-template>
				</xsl:otherwise>
				<!--Fin de la sortie de l'exposant en mode double-->
			</xsl:choose>
		</xsl:when>
		<!-- S'il n'y a pas de partie en exposant ou surscrit (double=false)-->
		<xsl:otherwise>
			<xsl:choose>
				<!--Si la partie en indice ou en souscrit commence par un moins, on l'encadre par des row--> 
				<xsl:when test="nat:starts-with-any-of($contenuApresSigne,($plus,$moins))">
					<m:mrow>
						<!--Sortie du plus ou du moins-->
						<xsl:call-template name="sortiePremierElement">
                                                        <xsl:with-param name="originStartOffset" select="$originStartOffset+string-length($contenu)-string-length($contenuApresSigne)"/>    
							<xsl:with-param name="contenu" select="substring($contenuApresSigne,1,1)"/>
						</xsl:call-template>
						<!-- Sortie de la partie en indice ou souscrit-->
						<xsl:call-template name="parser">
                                                        <xsl:with-param name="originStartOffset" select="$originStartOffset+string-length($contenu)-string-length($contenuApresSigne)+1"/>    
							<xsl:with-param name="contenu" select="substring($contenuApresSigne,2,nat:indexFinElement(substring($contenuApresSigne,2)))"/>
						</xsl:call-template>
					</m:mrow>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="parser">
                                                <xsl:with-param name="originStartOffset" select="$originStartOffset+string-length($contenu)-string-length($contenuApresSigne)"/>    
						<xsl:with-param name="contenu" select="substring($contenuApresSigne,1,nat:indexFinElement($contenuApresSigne))"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:otherwise>
	</xsl:choose>
<!--Fin du template indice-->
</xsl:template>

<!--Renvoie la dernière position avant un opérateur dans une chaîne. Utile pour extraire les numérateurs et dénominateurs-->
<xsl:function name="nat:finPremierGroupe" as="xs:integer" xmlns:doc="espaceDoc">
	<xsl:param name="contenu" as="xs:string"/>
	<xsl:param name="position" as="xs:integer"/>

	<xsl:choose>
		<xsl:when test="not(substring($contenu,$position))">
			<xsl:value-of select="$position - 1"/>
		</xsl:when>
		<!--Si on trouve un bloc ouvrant simple, on saute directement à la position du bloc fermant correspondant-->
		<xsl:when test="functx:contains-any-of(substring($contenu,$position,1),$seqOuvrantSimple)">
			<xsl:value-of select="nat:finPremierGroupe($contenu,$position+nat:finBloc(substring($contenu,$position),substring($contenu,$position,1),$seqFermantSimple[index-of($seqOuvrantSimple,substring($contenu,$position,1))],0,1))"/>
		</xsl:when>
		<!--Idem avec un bloc double-->
		<xsl:when test="functx:contains-any-of(substring($contenu,$position,2),$seqOuvrantDouble)">
			<xsl:value-of select="nat:finPremierGroupe($contenu,$position+nat:finBloc(substring($contenu,$position),substring($contenu,$position,2),$seqFermantDouble[index-of($seqOuvrantDouble,substring($contenu,$position,2))],0,1))"/>
		</xsl:when>
		<!--Si ou trouve un signe d'indice ou d'exposant-->
		<xsl:when test="substring($contenu,$position,1)=$indice or substring($contenu,$position,1)=$exposant">
			<!--On stocke dans cette variable un éventuel décalage au cas où deux signes d'indice et d'exposant de suivent"-->
			<xsl:variable name="retrait" as="xs:integer">
				<xsl:choose>
					<!--Si le caractère se répète (cas du souscrit ou surscrit)-->
					<xsl:when test="substring($contenu,$position+1,1)=substring($contenu,$position,1)">
						<xsl:value-of select="2"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="1"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:value-of select="nat:finPremierGroupe($contenu,nat:indexFinElement(substring($contenu,$position + $retrait))+$position+$retrait)"/>
		</xsl:when>
		<!--Si le caractère en cours est le début d'un caractère sans préfixe, alors on a trouvé la fin de la chaîne-->
		<xsl:when test="functx:contains-any-of(substring($contenu,$position,1),$seqDebutSansPrefixe)">
			<xsl:value-of select="$position - 1"/>
		</xsl:when>
		<!--Si le caractère en cours est un préfixe de symbole-->
		<xsl:when test="functx:contains-any-of(substring($contenu,$position,1),$seqPrefixe)">
			<!--On sélectionne la séquence d'opérateur et d'identifier qui correspondent au caractère actuel-->
			<xsl:variable name="premierCaractereOperateur" select="substring($contenu,$position,1)"/>
			<xsl:variable name="sequenceRetenue" as="xs:string*">
				<xsl:choose>
					<xsl:when test="$premierCaractereOperateur=$point45">
						<xsl:sequence select="$seqOperatorPt45"/>
					</xsl:when>
					<xsl:when test="$premierCaractereOperateur=$point456">
						<xsl:sequence select="$seqOperatorPt456"/>
					</xsl:when>
					<xsl:when test="$premierCaractereOperateur=$point46">
						<xsl:sequence select="$seqOperatorPt46"/>
					</xsl:when>
					<xsl:when test="$premierCaractereOperateur=$point5">
						<xsl:sequence select="$seqOperatorPt5"/>
					</xsl:when>
				</xsl:choose>
			</xsl:variable>
			<!--<xsl:message select="($premierCaractereOperateur,'(',$sequenceRetenue,')')"/>-->
			<xsl:choose>
				<!--Si la chaine constitué du caractère courant et des deux suivants contient un opérateur ou un identifier de la liste,
				on a trouvé la fin de la chaîne-->
				<xsl:when test="functx:contains-any-of(substring($contenu,$position,3),$sequenceRetenue)">
						<xsl:value-of select="$position - 1"/>
				</xsl:when>
				<!--Dans le cas contraire, le symbole signifie autre chose (lettre grecque, majuscule...)-->
				<xsl:otherwise>
					<xsl:value-of select="nat:finPremierGroupe($contenu,$position+1)"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		<!--Si le caractère en cours est une lettre ou un chiffre, ou n'importe quel autre caractère qui n'est pas un opérateur-->
		<xsl:otherwise>
			<xsl:value-of select="nat:finPremierGroupe($contenu,$position+1)"/>
		</xsl:otherwise>  
	</xsl:choose>
</xsl:function>

<!--Ce template effectue la sortie des operator ainsi que des lettres élaborées.-->
<xsl:template name="sortieOperator">
	<xsl:param name="contenu"/>
        <xsl:param name="originStartOffset" as="xs:integer" select="0"/>    
        <xsl:variable name="originEndOffset" as="xs:integer" select="$originStartOffset+string-length($contenu)"/>
        <xsl:variable name="originalTextOffsetAttributes" as="attribute()*" select="nat:originalTextOffsetAttributes($originStartOffset,$originEndOffset)"/>

	<xsl:choose>
		<!--Lettre majuscule-->
		<xsl:when test="starts-with($contenu,$point46) and nat:starts-with-any-of(substring($contenu,2),$seqAlphabet)">
			<m:mi>
                                <xsl:copy-of select="$originalTextOffsetAttributes"/>
				<xsl:value-of select="$seqAlphabetNoirMajuscule[index-of($seqAlphabet,substring($contenu,2,1))]"/>
			</m:mi>
		</xsl:when>
		<!--Lettre ronde minuscule-->
		<xsl:when test="starts-with($contenu,$point5) and nat:starts-with-any-of(substring($contenu,2),$seqAlphabet)">
			<m:mi>
                                <xsl:copy-of select="$originalTextOffsetAttributes"/>
				<xsl:value-of select="$seqRondeMinuscule[index-of($seqAlphabet,substring($contenu,2,1))]"/>
			</m:mi>
		</xsl:when>
		<!--Lettre ronde majuscule-->
		<xsl:when test="starts-with($contenu,concat($point46,$point5)) and functx:contains-any-of(substring($contenu,3,1),$seqAlphabet)">
			<m:mi>
                                <xsl:copy-of select="$originalTextOffsetAttributes"/>
				<xsl:value-of select="$seqRondeMajuscule[index-of($seqAlphabet,substring($contenu,3,1))]"/>
			</m:mi>      
		</xsl:when>
		<!--Lettre éclairée-->
		<xsl:when test="starts-with($contenu,concat($point46,$point46)) and functx:contains-any-of(substring($contenu,3,1),$seqAlphabet)">
			<m:mi>
                                <xsl:copy-of select="$originalTextOffsetAttributes"/>
				<xsl:value-of select="$seqEclairee[index-of($seqAlphabet,substring($contenu,3,1))]"/>
			</m:mi>      
		</xsl:when>
		<!--Lettre grecque minuscule-->
		<xsl:when test="starts-with($contenu,$point45) and functx:contains-any-of(substring($contenu,2,1),$seqAlphabet)">
			<m:mi>
                                <xsl:copy-of select="$originalTextOffsetAttributes"/>
				<xsl:value-of select="$seqGrecqueMinuscule[index-of($seqAlphabet,substring($contenu,2,1))]"/>
			</m:mi>      
		</xsl:when>
		<!--Lettre grecque majuscule-->
		<xsl:when test="starts-with($contenu,concat($point46,$point45)) and functx:contains-any-of(substring($contenu,3,1),$seqAlphabet)">
			<m:mi>
                                <xsl:copy-of select="$originalTextOffsetAttributes"/>
				<xsl:value-of select="$seqGrecqueMajuscule[index-of($seqAlphabet,substring($contenu,3,1))]"/>
			</m:mi>      
		</xsl:when>
		<!--Lettre barre-->
		<xsl:when test="starts-with($contenu,concat($point456,$point36)) and functx:contains-any-of(substring($contenu,3,1),$seqAlphabet)">
			<m:mi>
                                <xsl:copy-of select="$originalTextOffsetAttributes"/>
				<xsl:value-of select="$seqBarreLettersMML[index-of($seqAlphabet,substring($contenu,3,1))]"/>
			</m:mi>      
		</xsl:when>
		<!--Lettre hébraique-->
		<xsl:when test="nat:starts-with-any-of($contenu,$seqHebraique)">
			<m:mi>
                                <xsl:copy-of select="$originalTextOffsetAttributes"/>
				<xsl:value-of select="$seqHebraiqueMML[index-of($seqHebraique,substring($contenu,1,3))]"/>
			</m:mi>      
		</xsl:when>
		<xsl:otherwise>
			<!--Identifie la séquence de caractères braille appropriée-->
			<xsl:variable name="seqRetenue" as="xs:string*">
				<xsl:choose>
					<!--Commence par le préfixe d'opérateur pt45-->
					<xsl:when test="starts-with($contenu,$point45)">
						<xsl:sequence select="$seqOperatorPt45"/>
					</xsl:when>
					<xsl:when test="starts-with($contenu,$point456)">
						<xsl:sequence select="$seqOperatorPt456"/>
					</xsl:when>
					<xsl:when test="starts-with($contenu,$point46)">
						<xsl:sequence select="$seqOperatorPt46"/>
					</xsl:when>
					<xsl:when test="starts-with($contenu,$point5)">
						<xsl:sequence select="$seqOperatorPt5"/>
					</xsl:when>
				</xsl:choose>
			</xsl:variable>
			<!--Identifie la séquence de caractères mathML appropriée-->
			<xsl:variable name="seqRetenueMML" as="xs:string*">
				<xsl:choose>
					<xsl:when test="starts-with($contenu,$point45)">
						<xsl:sequence select="$seqOperatorPt45MML"/>
					</xsl:when>
					<xsl:when test="starts-with($contenu,$point456)">
						<xsl:sequence select="$seqOperatorPt456MML"/>
					</xsl:when>
					<xsl:when test="starts-with($contenu,$point46)">
						<xsl:sequence select="$seqOperatorPt46MML"/>
					</xsl:when>
					<xsl:when test="starts-with($contenu,$point5)">
						<xsl:sequence select="$seqOperatorPt5MML"/>
					</xsl:when>
				</xsl:choose>
			</xsl:variable>
			<!--<xsl:message select="('contenu:',$contenu)"/> 
			<xsl:message select="('index:',index-of($seqRetenue,$contenu))"/>-->
			<m:mo>
                                <xsl:copy-of select="$originalTextOffsetAttributes"/>
				<xsl:value-of select="$seqRetenueMML[index-of($seqRetenue,$contenu)]"/>
			</m:mo>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="surscrit">
	<xsl:param name="contenu" as="xs:string"/>
        <xsl:param name="originStartOffset" as="xs:integer" select="0"/>

	<xsl:choose>
		<!--Si la chaîne commence par un symbole de tenseur, le traitement est différent, car il faut intégrer l'ordre du tenseur à la partie surscrite-->
		<xsl:when test="starts-with($contenu,$tenseur)">
                        <!-- TODO write test case -->
			<xsl:variable name="ordre" select="substring($contenu,4,nat:indexFinElement(substring($contenu,4)))"/>
			<xsl:variable name="ayantSurscrit" select="substring(substring-after($contenu,$ordre),1,nat:finPremierGroupe(substring-after($contenu,$ordre),1))"/>
			<m:mover>
				<m:mrow>
					<xsl:call-template name="parser">
                                                <xsl:with-param name="originStartOffset" select="$originStartOffset+4+string-length($ordre)"/> 
						<xsl:with-param name="contenu" select="$ayantSurscrit"/>
					</xsl:call-template>
				</m:mrow>
				<m:mrow>
					<m:mo><xsl:value-of select="$flecheEstMML"/></m:mo>
					<m:mi>
						<xsl:call-template name="parser">
                                                        <xsl:with-param name="originStartOffset" select="$originStartOffset+4"/> 
							<xsl:with-param name="contenu" select="$ordre"/>
						</xsl:call-template>
					</m:mi>
				</m:mrow>
			</m:mover>
                        <xsl:variable name="x" as="xs:integer" select="4 + string-length($ordre) + string-length($ayantSurscrit)"/>
			<xsl:call-template name="parser">
                                <xsl:with-param name="originStartOffset" select="$originStartOffset+$x"/> 
				<xsl:with-param name="contenu" select="substring($contenu,$x)"/>
			</xsl:call-template>
		</xsl:when>
		<xsl:otherwise>
			<!--On capture le caractère annonçant quelle est la partie surscrite-->
			<xsl:variable name="signeSurscrit">
				<xsl:choose>
					<xsl:when test="nat:starts-with-any-of(substring($contenu,1,2),$seqSurscrit)">
						<xsl:value-of select="substring($contenu,1,2)"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="substring($contenu,1,3)"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:variable name="ayantSurscrit" select="substring(substring-after($contenu,$signeSurscrit),1,nat:finPremierGroupe(substring-after($contenu,$signeSurscrit),1))"/>
			<m:mover>
				<m:mrow>
					<xsl:call-template name="parser">
                                                <xsl:with-param name="originStartOffset" select="$originStartOffset+string-length($contenu)-string-length($ayantSurscrit)-string-length($signeSurscrit)"/> 
						<xsl:with-param name="contenu" select="$ayantSurscrit"/>
					</xsl:call-template>
				</m:mrow>
				<m:mo stretchy='true'>
                                        <xsl:copy-of select="nat:originalTextOffsetAttributes($originStartOffset,$originStartOffset+string-length($signeSurscrit))"/>                                    
					<xsl:value-of select="$seqSurscritMML[index-of($seqSurscrit,$signeSurscrit)]"/>
				</m:mo>
			</m:mover>
			<xsl:call-template name="parser">
                                <xsl:with-param name="originStartOffset" select="$originStartOffset+string-length($ayantSurscrit)+string-length($signeSurscrit)"/> 
				<xsl:with-param name="contenu" select="substring-after($contenu,$ayantSurscrit)"/>
			</xsl:call-template>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!--Fonction qui la partie post indice d'une chaîne-->
<xsl:function name="nat:postIndice" as="xs:string" xmlns:nat="http://natbraille.free.fr/xsl">
	<xsl:param name="contenu" as="xs:string"/>
	<!-- Le paramètre signe doit recevoir le signe mettant fin à l'indice (exposant, double exposant, indice ou double indice)-->
	<xsl:param name="signe" as="xs:string"/>

	<xsl:choose>
		<xsl:when test="nat:starts-with-any-of(substring-after($contenu,$signe),($plus,$moins))">
			<xsl:value-of select="substring(substring-after($contenu,$signe),nat:indexFinElement(substring(substring-after($contenu,$signe),2))+2)"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="substring(substring-after($contenu,$signe),nat:indexFinElement(substring-after($contenu,$signe))+1)"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:function>

<xsl:template name="structure2D">
	<xsl:param name="tab2D" as="element()*" />

	<xsl:for-each select="$tab2D">
		<xsl:choose>
			<xsl:when test="local-name(.)='expr1D'">
				<xsl:call-template name="parser">
                                        <!-- TODO position -->
					<xsl:with-param name="contenu" select="."/>
					<xsl:with-param name="inside2D" select="true()" tunnel="yes"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="local-name(.)='cel'">
				<m:mtd>
					<xsl:if test="not(.='&pt5;&pt2;')">
                                                <!-- TODO position -->
						<xsl:call-template name="parser">
							<xsl:with-param name="contenu" select="."/>
							<xsl:with-param name="inside2D" select="true()" tunnel="yes"/>
						</xsl:call-template>
					</xsl:if>
				</m:mtd>
			</xsl:when>
			<xsl:when test="local-name(.)='ouvrant'">
				<xsl:variable name="pos" select="position()" />
				<xsl:variable name="ouvrantNoir" select="$seqOuvrantMultilignesMML[index-of($seqOuvrantMultilignes,$tab2D[$pos])]"/>
				<!--<xsl:message>
					<xsl:for-each select="$tab2D">
						<xsl:copy copy-namespaces="no">
							<xsl:value-of select="nat:toTbfr(.)" />
						</xsl:copy>
						<xsl:if test="position()=$pos"><xsl:text>********</xsl:text><xsl:value-of select="nat:toTbfr(text())"/></xsl:if>
						<xsl:text>&#10;</xsl:text>
					</xsl:for-each>
				</xsl:message>-->
				<xsl:variable name="indiceFermant" select="$tab2D[local-name(.)='fermant' and position()>$pos][1]" />
				<xsl:variable name="fermantNoir"
					select="if ($indiceFermant) then $seqFermantMultilignesMML[index-of($seqFermantMultilignes,$indiceFermant)] else ''"/>
				<!--<xsl:text disable-output-escaping="yes">&lt;m:mfenced open="</xsl:text>-->
				<xsl:value-of select="concat('&lt;m:mfenced open=&quot;',$ouvrantNoir,'&quot; close=&quot;',$fermantNoir,'&quot;&gt;&#10;')"/>
				<xsl:value-of select="'&lt;m:mtable&gt;&#10;&lt;m:mtr&gt;'" />
			</xsl:when>
			<xsl:when test="local-name(.)='fermant'">
				<xsl:value-of select="'&lt;/m:mtr&gt;&#10;&lt;/m:mtable&gt;&#10;'" />
				<xsl:value-of select="'&lt;/m:mfenced&gt;&#10;'"/>
			</xsl:when>
			<xsl:when test="local-name(.)='line-break'">
				<xsl:value-of select="'&#10;&lt;/m:mtr&gt;&#10;&lt;m:mtr&gt;'"/>
			</xsl:when>
			<xsl:when test="local-name(.)='fin'">
				<!-- dans le cas d'un système d'équation, on a pas de fermant donc c'est à
				fin de tout fermer -->
				<xsl:if test="local-name($tab2D[position()=last()-1])='cel'">
					<xsl:value-of select="'&lt;/m:mtr&gt;&#10;&lt;/m:mtable&gt;&#10;'" />
					<xsl:value-of select="'&lt;/m:mfenced&gt;&#10;'"/>
				</xsl:if>
			</xsl:when>
			<xsl:otherwise>
				<xsl:if test="$dbg"><xsl:message select="'ERREUR Debraillage tableau'" /></xsl:if>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:for-each>
</xsl:template>

<xsl:template name="structure2Dold">
	<xsl:param name="lignes" as="xs:string*"/>
	<xsl:param name="indicesBlocs2D" as="xs:integer*" />

	<m:mrow>
		<xsl:variable name="ouvrant">
			<xsl:if test="nat:starts-with-any-of($lignes[1],$seqOuvrantDouble)">
				<!--Sortie du bloc ouvrant-->
				<xsl:value-of select="$seqOuvrantDoubleMML[index-of($seqOuvrantDouble,substring($lignes[1],1,2))]"/>
			</xsl:if>
		</xsl:variable>
		<xsl:variable name="fermant">
			<xsl:if test="nat:ends-with-any-of($lignes[last()],$seqFermantDouble)">
				<!--Sortie du bloc fermant-->
				<xsl:value-of select="$seqFermantDoubleMML[index-of($seqFermantDouble,substring($lignes[last()],string-length($lignes[last()])-1,2))]"/>
			</xsl:if>
		</xsl:variable>
		<!-- traitement du tableau -->
		<m:mfenced open="{$ouvrant}" close="{$fermant}">
			<m:mtable>
				<xsl:for-each select="$lignes">
					<m:mtr>
						<m:mtd>
							<xsl:call-template name="parser">
                                                                <!-- TODO position -->
								<xsl:with-param name="contenu" select="if ((position()=1) and nat:starts-with-any-of(.,$seqOuvrantDouble))
									then substring(.,3) else ."/>
								<xsl:with-param name="inside2D" select="true()" tunnel="yes"/>
							</xsl:call-template>
						</m:mtd>
					</m:mtr>
				</xsl:for-each>
			</m:mtable>
		</m:mfenced>
	</m:mrow>
</xsl:template>
</xsl:stylesheet>
