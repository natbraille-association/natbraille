<?xml version='1.0' encoding="UTF-8" ?>
<!--
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
-->

<!-- version: 1.3-b3 -->

<!DOCTYPE xsl:stylesheet SYSTEM "nat://system/xsl/mmlents/windob.dtd">


<xsl:stylesheet version="2.0"
xmlns:xsl='http://www.w3.org/1999/XSL/Transform' 
xmlns:saxon='http://icl.com/saxon'
xmlns:xs='http://www.w3.org/2001/XMLSchema'
xmlns:fn='http://www.w3.org/2005/xpath-functions'
xmlns:fo='http://www.w3.org/1999/XSL/Format'
xmlns:m='http://www.w3.org/1998/Math/MathML'
xmlns:lit='espacelit'
xmlns:doc='espaceDoc'>

<xsl:output method="xml" encoding="UTF-8" indent="yes"/>

<xsl:template match="lit">
	<!--<xsl:text>debLit</xsl:text>-->
	<xsl:apply-templates select="@*|*|text()|processing-instruction()" />
	<!--<xsl:text>FinLit</xsl:text>-->
</xsl:template>

<xsl:template match="ponctuation">
	<xsl:value-of select="." />
	<xsl:if test="not (local-name(following::*[1])='phrase') and not(contains('¡¿([{«“‘&lsquo;',string(.)))"><!-- RAF:à améliorer pour apos ouvrantssimples -->
		<xsl:call-template name="espace"/>
	</xsl:if>
</xsl:template>

<xsl:template match="mot">
	<xsl:value-of select="." />
	<xsl:call-template name="espace"/>
</xsl:template>

</xsl:stylesheet>
