<?xml version='1.1' encoding="UTF-8" ?>
<!--
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
-->

<!DOCTYPE xsl:stylesheet SYSTEM "nat://system/xsl/mmlents/windob.dtd">

<xsl:stylesheet version="3.0"
xmlns:xsl='http://www.w3.org/1999/XSL/Transform' 
xmlns:saxon='http://icl.com/saxon'
xmlns:m='http://www.w3.org/1998/Math/MathML'
xmlns:fn='http://www.w3.org/2005/xpath-functions'
xmlns:functx='http://www.functx.com'
xmlns:xs='http://www.w3.org/2001/XMLSchema'
xmlns:map='http://www.w3.org/2005/xpath-functions/map'
xmlns:xhtml='http://www.w3.org/1999/xhtml'
xmlns:nat='http://natbraille.free.fr/xsl'
xmlns:lit='espacelit'
xmlns:doc='espaceDoc'>
	
	<xsl:import href="nat://system/xsl/nat-functs.xsl" />
	<xsl:include href="nat://system/xsl/outils.xsl"/>
	<xsl:include href="nat://system/xsl/ressources/liste-fr-g1.xsl"/>
	<!-- <xsl:include href="hyphenation.xsl"/> -->

	<xsl:output method="xml" encoding="UTF-8" indent="yes" doctype-system="nat://system/xsl/mmlents/windob.dtd"/>

	<xsl:param name="preserveTag" as="xs:boolean" select="false()"/>
	
	<xsl:strip-space elements = "tableau col ligne lit phrase math li ol ul tr th td vide chimie doc:ul doc:li doc:ol" />

	<xsl:template match="xhtml:*">
		<xsl:copy>
			<xsl:copy-of select="@*"/>
			<xsl:apply-templates/>
		</xsl:copy>
	</xsl:template>
	<!-- en attendant que ça soit un param -->
	<!--<xsl:variable name="styles" select="document('./ressources/styles_default.xml')/styles/*" />-->

	<xsl:template match="doc:doc">
		<doc:doc>
			<xsl:apply-templates select="@*|*|text()|processing-instruction()" />
		</doc:doc>
	</xsl:template>

	<xsl:template match="page-break">
		<xsl:copy/>
	</xsl:template>

	<xsl:template match="lit/echap">
		<xsl:element name="unformated">
			<xsl:copy-of select="node()|@*"/>
		</xsl:element>
	</xsl:template>

	<!--Bruno TODO NBA remplacer par une fonction et virer les appels au template (fr-maths entre autres)-->
	<xsl:template name="rempli">
		<xsl:param name="nb" select="-10" as="xs:integer"/>
		<xsl:param name="motif" as="xs:string"/>
		<!--<xsl:element name="fill">
			<xsl:copy-of select="@*"/>-->
			<xsl:value-of select="((1 to $nb) ! $motif) => string-join()"/>
		<!--</xsl:element>-->
	</xsl:template>
	
	<xsl:template match="*:ol|*:ul|*:li|*:dl" priority="1"> <!-- listes -->
			<xsl:variable name="fragmentId" select="@fragmentId" as="xs:string"/>
			<xsl:element name="{'doc:' || local-name(.)}">
				<xsl:copy-of select="@*"/>
				<xsl:attribute name="fragmentId" select="$fragmentId"/>
				<xsl:apply-templates select="functx:change-element-ns-deep(child::*, '','doc')"/>
			</xsl:element>			
	</xsl:template>
	
	<xsl:template match="titre|phrase|tableau">
		<!-- est-ce un début de note de transcripteur (ou n'importe quel prefixe) ? -->
		<xsl:variable name="styleOrig" select="upper-case(@styleOrig)" />
		<xsl:variable name="prefixe" as="xs:string?" select="nat:pToPt($styles[upper-case(@name)=$styleOrig]/@prefixe)" />
		<!-- est-ce une fin de note de transcripteur (ou n'importe quel suffixe)? -->
		<xsl:variable name="suffixe" as="xs:string?" select="nat:pToPt($styles[upper-case(@name)=$styleOrig]/@suffixe)" />
		
		<xsl:variable name="expr" >
			<xsl:apply-templates/>
		</xsl:variable>
		
		<!-- NBA -->
		<xsl:if test="not($prefixe='')">
			<xsl:element name="prefix">
				<xsl:attribute name="type" select="'style'"/>
				<xsl:value-of select="$prefixe"/>
			</xsl:element>
		</xsl:if>
		<xsl:message select="$tagConversion(name())"/>
		<xsl:variable name="nom" as="xs:string" select="if (contains ($styles[upper-case(@name)=$styleOrig]/@mode,'title')) then 'title' else 
			if(map:contains($tagConversion,name())) then $tagConversion(name()) else name()" />
		<xsl:element name="{$nom}">
			<xsl:copy-of select="@*" /> <!-- recopie des attributs -->
			<xsl:variable name="modeStyle" select="$styles[upper-case(@name)=$styleOrig]/@mode" />
			<xsl:choose>
				<xsl:when test="self::titre or $nom='titre'">
					<xsl:variable name="nivBra" as="xs:integer" select="if (self::titre) then @niveauOrig else
						xs:integer(functx:if-empty(substring-after($modeStyle,'title'),'1'))" />
					<xsl:attribute name="niveauBraille" select="$listeTitres[$nivBra]" />
					<!-- changement des niveaux de titre entre original et braille -->
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="modeBraille" select="if (matches($modeStyle,'^\d(-\d)?$'))
						then $modeStyle else '3-1'"/>
				</xsl:otherwise>
			</xsl:choose>
			<!-- si la phrase n'est pas vide (càd avec autre chose que des espaces) on la garde --><!-- BM 2023 :je vire pour le moment, a priori plus besoin si gestion tag plus loin-->
			<!--<xsl:if test="not(translate(string-join($phrasePretePourMep,''),concat($espace,'&pt;'),'')='')">
				<xsl:value-of select="string-join($phrasePretePourMep,'')"/>
			</xsl:if>-->
			<xsl:copy-of select="$expr"/>
		</xsl:element>
		<xsl:if test="not($suffixe='')">
			<xsl:element name="suffix">
					<xsl:attribute name="type" select="'style'"/>
					<xsl:value-of select="$suffixe"/>
				</xsl:element>
			</xsl:if>
	</xsl:template>
		
	<!-- TODO NBA-->
	<xsl:template match="tableau[@type='math_var']">
		<xsl:copy-of select="."/>
	</xsl:template>
	
	<xsl:template match="tableau[@type='lit']">
		<xsl:apply-templates mode="ligne" select="."/>
	</xsl:template>

	<xsl:template match="tableau" mode="ligne">
		<xsl:variable name="nbCol" select="count(tr[1]/th)" as="xs:integer"/><!-- nombre de colonnes du tableau -->
		<!-- transcription des données du tableau -->
		<!-- entètes -->
		<xsl:variable name="entetes" as="xs:string*">
			<xsl:for-each select="tr[th]">
				<xsl:for-each select="th">
					<xsl:variable name="resu">
						<xsl:apply-templates/>
					</xsl:variable>
					<xsl:value-of select="$resu"/><!--concat($resu,if (not(position() mod ($nbCol) =0)) then $espace else ())"/>-->
				</xsl:for-each>
			</xsl:for-each>
		</xsl:variable>
		<!-- corps du tableau -->
		<xsl:variable name="nblEnt" select="count(tr[child::th])" as="xs:integer"/><!-- nombre de lignes d'entête -->
		<!--<xsl:message select="$nblEnt"/>-->
		<!--<xsl:message select="$nbCol"/>-->
		<xsl:variable name="corps" as="xs:string*">
			<xsl:for-each select="tr[child::td]">
				<xsl:for-each select="td">
					<xsl:apply-templates/>
					<!--<xsl:message select="."/>
					<xsl:value-of select="$espace"/>-->
				</xsl:for-each>
				<!--<xsl:value-of select="$sautAGenerer"/>-->
			</xsl:for-each>
		</xsl:variable>

		<xsl:if test="$nblEnt &gt;1">
			<titre niveauBraille="3">
			<!-- écriture des entêtes non répétées (les premières)-->
			<!--<xsl:message select="'nbEntetes'"/>-->
			<!--<xsl:message select="$nblEnt;"/>-->
				<xsl:for-each select="for $c in 1 to (($nblEnt -1) * $nbCol) return concat($entetes[$c],'&pt;')">
					<xsl:value-of select="."/>
				</xsl:for-each>
			</titre>
		</xsl:if>
		<doc:ul style='tableau'>
			<!-- entetes et corps du tableau -->
			<!--<tableau mep="ligne">-->
			<xsl:variable as="xs:string" name="nomCol1" select="translate($entetes[($nblEnt -1) * $nbCol + 1],'&pt; ','')"/>
			<xsl:if test="not(string-length($nomCol1)=0)">
				<doc:li>
					<xsl:value-of select="concat($espace,$espace,$nomCol1)"/>
				</doc:li>
			</xsl:if>
			<!-- entetes à répéter -->
			<xsl:variable name="entetesRep" as="xs:string*">
				<xsl:for-each select="$entetes[position() &gt; ($nblEnt -1) * $nbCol]">
					<xsl:value-of select="."/>
				</xsl:for-each>
			</xsl:variable>
			<!-- nom des entetes -->
			<doc:li>
				<xsl:value-of select="string-join($entetesRep,'&pt23;')"/>
			</doc:li>
			<doc:li>
			<xsl:for-each select="$corps">
				<xsl:variable name="pos" as="xs:integer" select="position()"/>
				<xsl:variable name="nomCol" select="$entetesRep[($pos -1) mod $nbCol + 1]"/>
					<xsl:value-of select="if($nomCol='')  then concat(.,'&pt23;&pt;') else concat($nomCol,'&pt25;&pt;',.,'&pt23;&pt;')"/>
					<!-- dernière colonne -->
					<xsl:if test="position() mod $nbCol = 0 and not(position() = last())"><xsl:text disable-output-escaping="yes">&lt;/li&gt;&#10;			&lt;li&gt;</xsl:text></xsl:if>
			</xsl:for-each>
			</doc:li>
		</doc:ul>
	</xsl:template>


	<xsl:template match="tableau" mode="colonne">
		
		<tableau mep="col">
			<xsl:value-of select="$debTable"/>
			
			<xsl:variable name="nbCol" select="count(tr[1]/th)" as="xs:integer"/><!-- nombre de colonnes du tableau -->
			<!-- transcription des données du tableau -->
			<!-- entètes -->
			<xsl:variable name="entetes" as="xs:string*">
				<xsl:for-each select="tr[child::th]">
					<xsl:for-each select="th">
						<xsl:variable name="resu">
							<xsl:apply-templates/>
						</xsl:variable>
						<xsl:value-of select="concat($resu,if (position() mod ($nbCol) =0) then $sautAGenerer else $espace)"/>
					</xsl:for-each>
				</xsl:for-each>
			</xsl:variable>
			<!--<xsl:message select="$entetes"/>-->
			<!-- corps du tableau -->
			<xsl:variable name="nblEnt" select="count(tokenize(string-join($entetes,''),$sautAGenerer)) -1" as="xs:integer"/><!-- nombre de lignes d'entête -->
			<!--<xsl:message select="$nblEnt"/>-->
			<!--<xsl:message select="$nbCol"/>-->
			<xsl:variable name="corps" as="xs:string*">
				<xsl:for-each select="tr[child::td]">
					<xsl:for-each select="td">
						<xsl:apply-templates/>
						<!--<xsl:message select="."/>
						<xsl:value-of select="$espace"/>-->
					</xsl:for-each>
					<!--<xsl:value-of select="$sautAGenerer"/>-->
				</xsl:for-each>
			</xsl:variable>

			<xsl:variable name="lgColEnt" as="xs:integer*" select="nat:getColSize($entetes,$nbCol)"/>
			<xsl:variable name="lgColCorps" as="xs:integer*" select="nat:getColSize($corps, $nbCol)"/>
			<!--<xsl:message select="$lgColEnt"/>-->
			<!--<xsl:message select="$lgColCorps"/>-->

			<xsl:for-each select="$entetes">
				<xsl:value-of select="."/>
			</xsl:for-each>
			<xsl:for-each select="$corps">
				<xsl:value-of select="."/>
				<xsl:value-of select="if (position() mod ($nbCol) =0) then $sautAGenerer else $espace"/>
			</xsl:for-each>
			<xsl:value-of select="$finTable"/>
		</tableau>
	</xsl:template>

	<xsl:template match="td | tr | th">
		<xsl:apply-templates/>
	</xsl:template>
	
	<xsl:template match="th">
		<xsl:apply-templates/>
	</xsl:template>

	<xsl:template match="vide"/><!--<xsl:text>A</xsl:text></xsl:template>-->

<xsl:template name="espace">
	<xsl:param name="position" select="1" as="xs:integer" />
	
	<xsl:element name="spacing"><!-- TODO supprimer les tags vides -->
		<!--<xsl:message select="('ESP ''',.,'''',@doSpace, 'suiv', following::*[$position])"/>-->
		<xsl:choose>
			<xsl:when test="@doSpace='false'"/>
			<!--<xsl:when test="not(following::*[1]=text() or following::*[1]=node())">
			</xsl:when>-->
			<!--<xsl:when test="string(following::*[$position]) ='' or local-name(following::*[$position])='lit'">-->
			<xsl:when test="local-name(following::*[$position])='lit' or following::*[$position][not(string(.))]">
				<xsl:call-template name="espace">
					<xsl:with-param name="position" select="$position + 1"/>
				</xsl:call-template>
			</xsl:when>
			<!--<xsl:when test="following::*[$position]='-' and count(preceding-sibling::ponctuation[.='-']) mod 2 =1 "/>-->
			<xsl:when test="following::*[$position]='-' and local-name(following::*[$position+1])='ponctuation'"/>
			<!--<xsl:when test="number(.) and number(following-sibling::*[$position])"/>-->
			<xsl:when test="local-name(following::*[$position])=('mot','math','chimie','echap') or following::*[$position]='-'">
				<xsl:if test="not(following::*[$position]/@hauteur=('exp','ind') or 
					(.='&quot;' and (count(preceding-sibling::ponctuation[.='&quot;']) mod 2)=0))">
					<!--or (.='-' and (count(preceding-sibling::ponctuation[.='-']) mod 2)=0))"><!- TODO ET qu'on est pas avec une locution et en abrégé -->
					<xsl:text>&pt;</xsl:text>
				</xsl:if>
			</xsl:when>
			<!-- espaces avec les ponctuations -->
			<xsl:when test="local-name(following::*[$position])='ponctuation'">
				<xsl:if test="contains('¡¿([{«“‘&lsquo;',following::*[$position])" >
					<xsl:text>&pt;</xsl:text>
				</xsl:if>
				<!--  cas particulier des guillements non orientés -->
				<xsl:if test="following::*[$position]='&quot;'">
					<xsl:variable name="ouvrant" as="xs:integer">
						<xsl:value-of select="count(preceding-sibling::ponctuation[.='&quot;']) mod 2"/>
					</xsl:variable>
					<!-- Espace si le nombre de guillements précédents est pair (ouverture) -->
					<xsl:if test="$ouvrant = 0">
						<xsl:text>&pt;</xsl:text>
					</xsl:if>
				</xsl:if>
				<!--  cas particulier des tirets -->
				<xsl:if test="following::*[$position]='-'">
					<xsl:variable name="ouvrant" as="xs:integer">
						<xsl:value-of select="count(preceding-sibling::ponctuation[.='-']) mod 2"/>
					</xsl:variable>
					<!-- Espace si le nombre de guillements précédents est pair (ouverture) -->
					<xsl:if test="$ouvrant = 0">
						<xsl:text>&pt;</xsl:text>
					</xsl:if>
				</xsl:if>
			</xsl:when>
		</xsl:choose>
	</xsl:element>
</xsl:template>
	
</xsl:stylesheet>
