<?xml version='1.1' encoding="UTF-8" ?>
<!--
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
-->

<!-- version: 1.3-b3 -->

<!DOCTYPE xsl:stylesheet SYSTEM "nat://system/xsl/mmlents/windob.dtd">


<xsl:stylesheet version="2.0"
xmlns:xsl='http://www.w3.org/1999/XSL/Transform' 
xmlns:saxon='http://icl.com/saxon'
xmlns:xs='http://www.w3.org/2001/XMLSchema'
xmlns:fn='http://www.w3.org/2005/xpath-functions'
xmlns:fo='http://www.w3.org/1999/XSL/Format'
xmlns:m='http://www.w3.org/1998/Math/MathML'
xmlns:functx='http://www.functx.com'
xmlns:lit='espacelit'
xmlns:doc='espaceDoc'
xmlns:nat='http://natbraille.free.fr/xsl'>

<xsl:output method="xml" encoding="UTF-8" indent="yes"/>

<xsl:include href="nat://system/xsl/fr-commun.xsl"/>

<!-- traitement particulier sur le littéraire; en intégral: rien -->
<xsl:template match="lit">
	<!--<xsl:text>debLit</xsl:text>-->
	<xsl:apply-templates select="@*|*|text()|processing-instruction()" />
	<!--<xsl:text>FinLit</xsl:text>-->
</xsl:template>
<!-- ***************************************************
		SYMBOLES COMPOSES
	*************************************************
	Les entités de coupures ont été placées un peu au pif, en fonction de ce que je pensais probable
	Règles utilisées:
	1) il faut empêcher une coupure de symbole composé => utiliser au minimu un symbole de coupure
	dans l'idéal, encadrer le symbole par deux entités de coupure.
	2) certains symboles ne doivent pas être séparés de ce qui les précède (€, °...) ou de ce qui les suit
	(pas trouvé d'exemple; le $, si on respecte la notation americaine: $3 et non pas 3$).
	Dans ce cas, une seule entité de coupure est ajoutée après le symbole.
-->

<xsl:template name="symbolesComposes">
	<xsl:param name="mot" as="xs:string"/>
	<xsl:param name="PN" as="xs:integer"/>
	<xsl:param name="tiret" select='0' as="xs:integer"/><!-- = 1 si il faut tester le tiret simple; 2 si on est en fin de mot-->
	<!-- on teste ici s'il s'agit d'un nom propre pour ne pas le couper plus tard -->
	<xsl:choose>
		<xsl:when test="functx:is-value-in-sequence(.,$l_noms_propres) or @abrege='false'">
			<xsl:value-of select='$stopCoup'/>
		</xsl:when>
		<!-- génération d'une ambiguïté sur le caractère d'arrêt de coupure uniquement (pas sur le mot en cours de transcription), c'est différent en abrégé-->
		<xsl:when test='functx:is-value-in-sequence(.,$l_noms_propres_amb)'>
			<xsl:value-of select="concat('[Amb:Mot français (1) ou Nom propre (2)?|',nat:getPrecedingValues(.,6),'|',nat:getOriginalValue(.),'|',nat:getFollowingValues(.,6),'|','','|',$stopCoup,']')"/>
		</xsl:when>
	</xsl:choose>
	<xsl:choose>
		<xsl:when test="functx:contains-any-of($mot,$symbCompBl)">
			<!--<xsl:if test="starts-with($mot,'-')"><xsl:message select="'mot ',$mot,'tiret ',$tiret,'subs ',substring($mot,2,1)" /></xsl:if>-->
			<xsl:choose>
				<xsl:when test="not($tiret=2) and starts-with($mot,'-') and (matches(substring($mot,2,1),'\d') or $tiret=1)">
					<xsl:text>&pt36;</xsl:text>
					<xsl:if test="string-length($mot) &gt; 1">
						<xsl:choose>
							<xsl:when test="string-length($mot) = 2">
								<xsl:call-template name="symbolesComposes">
									<xsl:with-param name="mot" select="substring($mot,2,string-length($mot))"/>
									<xsl:with-param name="PN" select="$PN"/>
									<xsl:with-param name="tiret" select='2'/>
								</xsl:call-template>
							</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="symbolesComposes">
									<xsl:with-param name="mot" select="substring($mot,2,string-length($mot))"/>
									<xsl:with-param name="PN" select="$PN"/>
									<xsl:with-param name="tiret" select='1'/>
								</xsl:call-template>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:if>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="bijection">
						<xsl:with-param name="mot" select="functx:replace-multi($mot,$symbCompRegBl,$symbCompBr)"/>
						<xsl:with-param name="PN" select="$PN"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		<xsl:otherwise>
			<xsl:call-template name="bijection">
				<xsl:with-param name="mot" select="$mot"/>
				<xsl:with-param name="PN" select="$PN"/>
			</xsl:call-template>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="bijection">
	<xsl:param name="mot" as="xs:string"/>
	<xsl:param name="PN" as="xs:integer" select='-10'/>
	<xsl:call-template name="finBijection">
		<xsl:with-param name="mot" select="$mot"/>
		<xsl:with-param name="PN" select="$PN"/>
	</xsl:call-template>
</xsl:template>

</xsl:stylesheet>
