﻿<?xml version='1.1' encoding="UTF-8" ?>
<!--
* NAT - An universal Translator
* Copyright (C) 2005 Bruno Mascret
* Contact: bmascret@free.fr

* former authors: Frédéric Schwebel, Bruno Mascret, Ouarda Yassa
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
-->

<!-- version 2.4 -->

<!DOCTYPE xsl:stylesheet SYSTEM "nat://system/xsl/mmlents/windob.dtd">

<xsl:stylesheet version="2.0"
xmlns:xsl='http://www.w3.org/1999/XSL/Transform' 
xmlns:saxon='http://icl.com/saxon'
xmlns:m='http://www.w3.org/1998/Math/MathML'
xmlns:xs='http://www.w3.org/2001/XMLSchema'
xmlns:fn='http://www.w3.org/2005/xpath-functions'
xmlns:doc='espaceDoc'
xmlns:set="http://exslt.org/sets"
extension-element-prefixes="set">

	<xsl:import href="set.xsl" />
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	
	<xsl:variable name="l_alphabet">abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ</xsl:variable>
	<xsl:variable name="l_chiffres">1234567890,.</xsl:variable>
	<xsl:variable name="l_grec_min">&alpha;&beta;&gamma;&delta;&epsi;&epsiv;&zeta;&eta;&theta;&thetav;&iota;&kappa;&lambda;&mu;&nu;&xi;&omicron;&pi;&piv;&rho;&rhov;&sigma;&sigmav;&tau;&upsilon;&phi;&phiv;&chi;&psi;&omega;ϵ</xsl:variable>
	<xsl:variable name="l_grec_maj">&Agr;&Bgr;&Gamma;&Delta;&Egr;&Zgr;&EEgr;&Theta;&Igr;&Kgr;&Lambda;&Mgr;&Ngr;&Xi;&Ogr;&Pi;&Rgr;&Sigma;&Tgr;&Upsilon;&Phi;&KHgr;&Chi;&Psi;&Omega;</xsl:variable>
	<xsl:variable name="l_alphanumgrec" select="fn:concat($l_alphabet,$l_chiffres,$l_grec_min,$l_grec_maj)" />
	
<!-- ******************** Phase de preprocessing ****************** -->

<xsl:variable name="functionList" as="xs:string">
	<xsl:value-of select="'|sin|cos|tan|cot|cotan|arcsin|arccos|arctan|arccotan|arccot|sec|csc|sinh|cosh|tanh|coth|arcsinh|arccosh|arctanh|arccoth|sech|csch|ln|log|sh|ch|exp|Card|'" />
</xsl:variable>
	
<xsl:template match="@*|*|processing-instruction()" priority="-1">
	<xsl:copy>
		<xsl:apply-templates select="@*|*|text()|processing-instruction()"/>
	</xsl:copy>
</xsl:template>

<xsl:template match="text()">
	<!-- pour virer les &amp; des noms d'entites -->
	<xsl:value-of select="." disable-output-escaping="yes" />
</xsl:template>

<xsl:template match="m:*[*[(self::m:mo|self::m:mi)[fn:contains($functionList,concat('|',string(.),'|'))]]]">
<!-- <xsl:template match="m:*[*[self::m:mo[fn:matches(string(.),$functionList)]]]"> ce serait mieux -->
	<xsl:variable name="listeFonctions" select="*[(self::m:mo|self::m:mi)[fn:contains($functionList,concat('|',string(.),'|'))]]" />
	
	<xsl:copy>
		<xsl:apply-templates select="@*" />

		<xsl:for-each select="*">
		
			<xsl:variable name="ps1" select="preceding-sibling::*[1]" />
			<xsl:variable name="ps2" select="preceding-sibling::*[2]" />
			
			<xsl:choose>
				<xsl:when test="set:has-same-node(.,$listeFonctions)">
					<xsl:copy>
						<xsl:attribute name="fonc" select="'yes'" />
						<xsl:apply-templates select="@*|*|text()" />
					</xsl:copy>
				</xsl:when>
				<xsl:otherwise> <!-- tests pour les membres -->
					<xsl:copy>
						<xsl:if test="not(local-name(parent::*)='msup') and (
						(set:has-same-node($ps1,$listeFonctions) and fn:translate(string(.),$l_alphanumgrec,'')='') or
						(set:has-same-node($ps2,$listeFonctions) and fn:translate(string($ps1),$l_alphanumgrec,'')=''
							and string-length(concat(string($ps1),string($ps2))) &lt; 3) or
						(set:has-same-node($ps2,$listeFonctions) and (string(.)='&circ;' or string(.)='&amp;circ;') and local-name($ps1)='mn')
						)">
							<xsl:attribute name="membreFonc" select="'yes'" />
						</xsl:if>
						<xsl:apply-templates select="@*|*|text()" />
					</xsl:copy>
				</xsl:otherwise>
			</xsl:choose>
		
		</xsl:for-each>
	</xsl:copy>
</xsl:template>

<!-- <xsl:template name="traitementFonctions">
	<xsl:param name="positionCourante" />
	<xsl:param name="positionFonction" />
	<xsl:param name="positionFinFonction" />
	
	<xsl:if test="fn:exists(*[position()=number($positionCourante)])">
	<xsl:copy-of select="*[position()=number($positionCourante)]">
		<xsl:choose>
			<xsl:when test="fn:contains($functionList,fn:concat('|',string(.),'|'))">
				<xsl:apply-templates />
				<xsl:call-template name="traitementFonctions">
					<xsl:with-param name="positionCourante" select="$positionCourante+1" />
					<xsl:with-param name="positionFonction" select="$positionCourante" />
					<xsl:with-param name="positionFinFonction" select="number(1)" />
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="$positionFinFonction=1">
						<xsl:attribute name="membreFonc">1</xsl:attribute>
						<xsl:apply-templates />
						<xsl:call-template name="traitementFonctions">
							<xsl:with-param name="positionCourante" select="$positionCourante+1" />
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="traitementFonctions">
							<xsl:with-param name="positionCourante" select="$positionCourante+1" />
						</xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
			
	</xsl:copy-of>
	</xsl:if>
	
</xsl:template>-->

</xsl:stylesheet>
