<?xml version='1.1' encoding="UTF-8" ?>
<!DOCTYPE xsl:stylesheet SYSTEM "nat://system/xsl/mmlents/windob.dtd">
<!--
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
-->

<!-- version 1.5 -->

<!-- IMPORTANT : dans cette xsl, tout ce qui s'appelle "style(s)" fait référence aux styles css du doc xhtml,
tout ce qui s'appelle stylist fait référence au fichier xml stylist -->

<xsl:stylesheet version="2.0" 

xmlns:xsl='http://www.w3.org/1999/XSL/Transform' 
xmlns:nat='http://natbraille.free.fr/xsl' 
xmlns:saxon='http://icl.com/saxon'
xmlns:fn='http://www.w3.org/2005/xpath-functions'
xmlns:xs='http://www.w3.org/2001/XMLSchema'
xmlns:xhtml="http://www.w3.org/1999/xhtml" 
xmlns:m='http://www.w3.org/1998/Math/MathML'
xmlns:functx='http://www.functx.com'
xmlns:doc='espaceDoc'
xmlns:dtb="http://www.daisy.org/z3986/2005/dtbook/"> 

<xsl:import href="nat://system/xsl/functions/functx.xsl" /> <!-- functx functions -->
<xsl:param name="dtd" select="'xsl/mmlents/windob.dtd'" as="xs:string"/>
<xsl:param name="processImage" select="false()" as="xs:boolean"/>
<xsl:param name="chemistryStyle" select="'chimie'" as="xs:string"/>
<xsl:param name="insertIndexBraille" select="true()"/>
<xsl:param name="indexTableName" select="'Table des matières'"/>
<xsl:param name="stylistFile" select="'./ressources/styles_default.xml'" as="xs:string" />

<!-- dtdbook parameters-->
<xsl:param name="printInfo" as="xs:boolean" select="true()"/><!-- display information on frontmatter-->

<xsl:variable as="node()*" name="stylist" select="document($stylistFile)/styles/*"/>
<xsl:variable name="tableLitStyle" as="xs:string" select="'Table'"/>
<xsl:variable name="stylesChimie" as="xs:string*" select="$stylist[@special=('chimie','chemistry')]/@name" />
<xsl:variable name="stylesG0" as="xs:string*" select="$stylist[@special=('g0','G0')]/@name" />
<xsl:variable name="stylesG1" as="xs:string*" select="$stylist[@special=('g1','G1')]/@name" />
<xsl:output method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="no"/>


<xsl:strip-space elements = "dtb:dtbook dtb:head dtb:frontmatter dtb:bodymatter dtb:div dtb:part dtb:rearmatter dtb:book dtb:p dtb:epigraph xhtml:head xhtml:meta xhtml:title xhtml:link xhtml:style xhtml:ul xhtml:ol xhtml:li m:* m:math m:semantics m:mrow xhtml:table xhtml:td xhtml:tbody xhtml:tr xhtml:thead xhtml:th" />

<xsl:variable name="l_ponct">¡¿”&quot;’,.:;!?»…)]}\}«“‘([{}&lsquo;&rsquo;&acute;&prime;&mldr;&vellip;&hellip;&dtdot;&ldots;&ctdot;&utdot;</xsl:variable>
<xsl:variable name="style_Standard" as="xs:string" select="'Standard'" />

<!-- constantes pour attributs -->
<xsl:variable name="mev" select="1" as="xs:integer" />
<xsl:variable name="hauteur" select="2" as="xs:integer" />
<xsl:variable name="integral" select="3" as="xs:integer" />
<xsl:variable name="attrNames" select="('mev','hauteur','abrege')" as="xs:string*" />

<xsl:variable name="styles" as="xs:string*">
	<!-- cette variable va contenir tous les styles du doc d'origine, un par ligne, en commençant par le nom du style si le css est inclus-->
	<xsl:for-each select="functx:lines(string(/xhtml:html/xhtml:head/xhtml:style/text()))">
		<xsl:variable name="s" select="replace(.,'\s','')" />
		<xsl:if test="string-length($s) >0">
			<xsl:value-of select="$s" />
		</xsl:if>
	</xsl:for-each>
</xsl:variable>

<xsl:template match="dtb:head|dtb:meta" mode="#all"/>
<xsl:template match="dtb:noteref" mode="#all">
	<ponctuation>(</ponctuation><mot>*</mot><ponctuation>)</ponctuation>
</xsl:template>

<xsl:template match="dtb:dtbook|dtb:bodymatter">
	<xsl:apply-templates select="*|text()|processing-instruction()"/>
</xsl:template>

<xsl:template match="/">
	<xsl:text disable-output-escaping="yes">
		&lt;!DOCTYPE doc:doc SYSTEM "
	</xsl:text>
	<xsl:value-of select="$dtd"/>
	<xsl:text disable-output-escaping="yes">"&gt;</xsl:text>

	<!--<xsl:for-each select="$styles">-->
		<!--<xsl:message select="."/>-->
		<!--<xsl:message select="'********'"/>
	</xsl:for-each>-->
	<doc:doc xmlns:doc="espaceDoc">
	<xsl:apply-templates select="dtb:dtbook" />
	<xsl:if test="$insertIndexBraille and (descendant::dtb:level1 or descendant::dtb:level2 or descendant::dtb:level3)">
		<page-break/>
		<titre niveauOrig="1">
			<lit>
				<xsl:for-each select="tokenize($indexTableName,' ')">
					<mot><xsl:value-of select="."/></mot>
				</xsl:for-each>
			</lit>
		</titre>
	</xsl:if>
	</doc:doc>
</xsl:template>

<!-- page de garde, traitement sépcial -->
<xsl:template match="dtb:frontmatter">
	<xsl:apply-templates select="dtb:doctitle|dtb:docauthor"/>
</xsl:template>

<!--<xsl:template match="dtb:p[@class='p']" priority="10"/>-->
<xsl:template match="dtb:bodymatter|dtb:rearmatter|dtb:part|dtb:level|dtb:epigraph">
	<xsl:apply-templates select="*"/>
</xsl:template>

<xsl:template match="dtb:doctitle|dtb:docauthor">
	<titre niveauOrig='1' niveauBraille='1'>
		<!-- pour simplifier, on crée forcement un tag lit suivi d'un tag mot, s'ils sont vides, on les supprimera dans la feuille de style de transcription -->
		<xsl:call-template name="createGlobalAttributes" />
		<!--<xsl:variable name="miseEnEv" select="nat:miseEnEvidence(.)" />-->
		<lit>
			<xsl:call-template name="remplaceEspace">
				<xsl:with-param name="chaine">
					<xsl:value-of disable-output-escaping="yes" select="text()" />
				</xsl:with-param>
			</xsl:call-template>
		</lit>
	</titre>
</xsl:template>

<!-- numéro de pages en noirs => ajout d'un attribut -->
<xsl:template match="dtb:pagenum" mode="#all"/>
	<!--<xsl:attribute name="numBlack"><xsl:value-of select="."/></xsl:attribute>-->
<!--
<xsl:template match="xhtml:table|xhtml:tr|xhtml:div|xhtml:html|xhtml:body">
	<xsl:apply-templates/>
</xsl:template>-->
<xsl:template match="*[@class=$stylesG0]" mode="#all">
	<!-- il faut mettre un dospace=false si ce qui suit n'a pas d'espace -->
<!--	<xsl:variable name="sibz" as="node()*" select="../node()" />
	<xsl:variable name="nc" as="node()" select="." />
	<xsl:variable name="pos" select="index-of($sibz,$nc)" />-->
	<xsl:variable name="doSpace" as="xs:boolean" select="fn:matches(string(following::text()[1]),'^\s')" />
	<echap>
		<xsl:if test="not($doSpace)">
			<xsl:attribute name="doSpace" select="$doSpace" />
		</xsl:if>
		<xsl:value-of select='.'/>
	</echap>
</xsl:template>


<xsl:template match="dtb:br" mode="#all">
	<line-break />
</xsl:template>

<xsl:template match="dtb:div|xhtml:body">
	<xsl:apply-templates/>
</xsl:template>
<!-- Je commente pour l'instant -->
<xsl:template match="xhtml:*[contains(@style,'break-before:page') and not(self::p)] | xhtml:p[contains(@style,'break-before:page') and not(local-name(..)='li')]" priority="2" mode="#all">
	<page-break/>
	<xsl:next-match/>
</xsl:template>

<xsl:template match="xhtml:*[contains(@style,'break-after:page') and not(self::p)] | xhtml:p[contains(@style,'break-after:page') and not(local-name(..)='li')]" priority="1.5" mode="#all">
	<xsl:next-match/>
	<page-break/>
</xsl:template>

<xsl:template match="xhtml:thead|xhtml:tbody|xhtml:table">
	<xsl:choose>
		<xsl:when test="not(xhtml:tbody or xhtml:thead or count(descendant::*[@class=$tableLitStyle])&gt;0) and (count(descendant::*[local-name()='math'])&gt;0)">
			<!--<tableau type="math">-->
<phrase><math xmlns="http://www.w3.org/1998/Math/MathML">
			<mtable>
				<xsl:for-each select="xhtml:tr|xhtml:th">
					<mtr>
						<xsl:for-each select="xhtml:td">
							<mtd>
								<xsl:for-each select="xhtml:p">
									<xsl:choose>
										<xsl:when test="child::m:math">
											<xsl:copy-of select="child::m:math/*"/>
										</xsl:when>
										<!--<xsl:when test="string-length(normalize-space(.)) &gt; 0"> TODO trouver pourquoi il fait des mi vides
											<mi><xsl:value-of select="."/></mi>
										</xsl:when>-->
									</xsl:choose>
								</xsl:for-each>
							</mtd>
						</xsl:for-each>
					</mtr>
				</xsl:for-each>
			</mtable>
			</math>
			</phrase>
			<!--</tableau>-->
		</xsl:when>
		<xsl:otherwise>
			<!-- c'est un tableau littéraire -->
			<tableau type="lit">
				<!-- dimensions réelles du tableau -->
				<xsl:variable name="nbC" as="xs:integer" select="xs:integer(sum(for $r in descendant::xhtml:tr[position() = 1]/xhtml:td return if($r/@rowspan) then $r/@rowspan else 1))"/>
				<xsl:variable name="nbL" as="xs:integer" select="xs:integer(sum(for $r in descendant::xhtml:tr/xhtml:td return  max(($r/@colspan,1))*max((1,$r/@rowspan))) div $nbC)"/>
				<!--<xsl:message select="$nbC, $nbL" />-->
				<!--<xsl:message select="concat('colonnes', $nbC, ' lignes:',$nbL)"/>-->
				<!-- taille des colonnes en cellules suivie du nb de lignes fusionnées -->
				<xsl:variable as="xs:integer*" name="tCol" select="for $c in 1 to $nbC return 0"/>
				<!--<xsl:message select="concat('indice',string-join(for $c in $tCol return string($c),' '))"/>-->
				<!-- réécriture du tableau -->
				<xsl:variable name="tabComplet" >
					<xsl:call-template name="writeTabLn">
						<xsl:with-param name="nbC" select="$nbC"/>
						<xsl:with-param name="tCol" select="$tCol"/>
					</xsl:call-template>
				</xsl:variable>

				<!--<xsl:message select="'tableau:'"/>-->
				<!--<xsl:message select="$tabComplet"/>-->
				<!-- récupération du nombre de ligne des entêtes: nb ligne de la 1ère cellule -->
				<xsl:variable name="nblEntetes" as="xs:integer" select="max((1,xs:integer(xhtml:tr[1]/xhtml:td/@rowspan)))"/><!-- nombre de ligne des entêtes -->

				<xsl:for-each select="$tabComplet/*">
					<xsl:variable name="pos" select="position()"/>
					<xsl:if test="$pos mod $nbC = 1">
						<xsl:text disable-output-escaping="yes">&lt;tr&gt;</xsl:text>
					</xsl:if>
					<xsl:choose>
						<xsl:when test="$pos &lt;= $nbC * $nblEntetes">
							<th>
								<xsl:variable name="spanCol" select="if(following-sibling::phrase[1]) then 
									functx:index-of-node($tabComplet/*[position() &gt; $pos],following-sibling::phrase[1]) - $pos
									else 0"/>
								<xsl:if test="$spanCol &gt; 0">
									<xsl:attribute name="col" select="$spanCol + 1"/>
								</xsl:if>
								<xsl:copy-of select="."/>
							</th>
						</xsl:when>
						<xsl:otherwise>
							<td>
								<!-- disable output escaping est ignoré dans un temporary tree, donc les &lt;lit&gt; tout ça vont pas;
								on ignore le contenu littéraire si y'a des maths et pis c'est tout -->
								<xsl:choose>
									<xsl:when test="lit and count(*)=1 and descendant::m:math">
										<phrase>
											<xsl:copy-of select="descendant::m:math"/>
										</phrase>
									</xsl:when>
									<xsl:otherwise>
										<xsl:copy-of select="."/>
									</xsl:otherwise>
								</xsl:choose>
								
							</td>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:if test="$pos mod $nbC = 0">
						<xsl:text disable-output-escaping="yes">&lt;/tr&gt;</xsl:text>
					</xsl:if>
				</xsl:for-each>
			</tableau>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- écris les cellules en tenant compte des fusions, complète les trous avec des cellules vides -->
<xsl:template name="writeTabLn">
	<xsl:param name="nbC" as="xs:integer"/><!-- nombre de colonnes du tableau -->
	<xsl:param name="tCol" as="xs:integer*"/> <!-- liste contenant pour chaque case un entier; si >0, signifie qu'il faut écrire une case vide à cause des fusions -->
	<xsl:param name="col" as="xs:integer" select="1"/><!-- la colonne actuelle -->
	<xsl:param name="row" as="xs:integer" select="1"/><!-- la ligne actuelle -->
	<xsl:param name="pos" as="xs:integer" select="1"/><!-- position du prochain enfant de tr à traiter -->

	<!-- calcul de la prochaine cellule à traiter -->
	<xsl:variable name="nextCol" as="xs:integer" select="$col mod $nbC + 1"/>
	<xsl:variable name="nextRow" as="xs:integer" select="if ($nextCol = 1) then ($row + 1) else $row"/>
	<!--<xsl:message select="concat('*nextC:', $nextCol, ' nextR:', $nextRow)"/>-->
	<xsl:choose>
		<xsl:when test="($tCol[$col] &gt; 0)"><!-- c'est une cellule vide -->
			<vide/>
			<!-- reconstruction de tCol -->
			<xsl:variable name="nextTCol" as="xs:integer*" select="for $c in 1 to $nbC return 
				if ($nextRow &gt; $row) then max(($tCol[$c] -1,0))
				else $tCol[$c]"/>
			<!--<xsl:message select="concat('indice ',string-join(for $c in $nextTCol return string($c),' '))"/>-->
			<xsl:call-template name="writeTabLn">
				<xsl:with-param name="nbC" select="$nbC"/>
				<xsl:with-param name="tCol" select="$nextTCol"/>
				<xsl:with-param name="col" select="$nextCol"/>
				<xsl:with-param name="row" select="$nextRow" />
				<xsl:with-param name="pos" select="if ($nextCol = 1) then 1 else $pos" />
			</xsl:call-template>
		</xsl:when>
		<xsl:otherwise><!-- c'est une cellule à lire -->
			<xsl:if test="descendant::xhtml:tr[$row]/descendant::xhtml:td[$pos]"><!-- faux si il n'y a plus de cellules à traiter (fin du tableau)-->
				<xsl:variable name="cellule" select="descendant::xhtml:tr[$row]/descendant::xhtml:td[$pos]"/>
				<xsl:variable name="cellTr">
					<xsl:apply-templates select="$cellule"/>
				</xsl:variable>
				<xsl:apply-templates select="$cellule"/><!-- TODO : remplacer par cellTr -->
				<!-- reconstruction de tCol, PLUS COMPLIQUÉE car il faut prendre en compte les colspan et les rowspan de la cellule actuelle -->
				<xsl:variable name="nextTCol" as="xs:integer*" select="for $c in 1 to $nbC return 
					if ($nextRow &gt; $row) then 
						if (($c >= $col) and ($col +  $cellule/@colspan > $c) or $c = $col)
						then xs:integer(max(($cellule/@rowspan -1,0)))
						else max(($tCol[$c] -1,0))
					else
						if (($c >= $col) and ($col +  $cellule/@colspan > $c) or $c = $col)
						then xs:integer(max(($cellule/@rowspan,1)))
						else $tCol[$c]"/>
				<!--<xsl:message select="concat('indice',string-join(for $c in $nextTCol return string($c),' '))"/>-->
				<xsl:call-template name="writeTabLn">
					<xsl:with-param name="nbC" select="$nbC"/>
					<xsl:with-param name="tCol" select="$nextTCol"/>
					<xsl:with-param name="col" select="$nextCol"/>
					<xsl:with-param name="row" select="$nextRow" />
					<xsl:with-param name="pos" select="if ($nextCol = 1) then 1 else $pos + 1" />
				</xsl:call-template>
			</xsl:if>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="retourneTableau">
	<xsl:param name="nbCol"/>
	<xsl:param name="colonne"/>
	
	<xsl:if test="not($colonne &gt; $nbCol)">
		<col>
		<xsl:for-each select="child::xhtml:tr">
			<xsl:for-each select="child::xhtml:td[position()=$colonne]">
				<ligne>
				<xsl:if test="(not(substring-before(substring-after(@style, 'border-right:'),';')='none' or substring-before(substring-after(@style, 'border-right:'),';')='') or not(substring-before(substring-after(following-sibling::xhtml:td[1]/@style, 'border-left:'),';')='none' or substring-before(substring-after(following-sibling::xhtml:td[1]/@style, 'border-left:'),';')='')) or not(substring-before(substring-after(@style, 'border:'),';')='none' or substring-before(substring-after(@style, 'border:'),';')='') or not(substring-before(substring-after(following-sibling::xhtml:td[1]/@style, 'border:'),';')='none' or substring-before(substring-after(following-sibling::xhtml:td[1]/@style, 'border:'),';')='')">
					<xsl:attribute name="b-vert">1</xsl:attribute>
				</xsl:if>
				<xsl:if test="not(substring-before(substring-after(@style, 'border-bottom:'),';')='none')">
					<xsl:attribute name="b-hor">1</xsl:attribute>
				</xsl:if>
				<xsl:apply-templates/>
				</ligne>
			</xsl:for-each>
		</xsl:for-each>
		</col>
		<xsl:call-template name="retourneTableau">
			<xsl:with-param name="nbCol" select="$nbCol"/>
			<xsl:with-param name="colonne" select="$colonne + 1"/>
		</xsl:call-template>
	</xsl:if>
</xsl:template>
<!--*****************************************************************
*              TITRES 
********************************************************************** -->

			<!-- quel est le niveau du titre?
			<xsl:attribute name="niveauOrig">
				<xsl:value-of select="min((xs:double(count(parent::dtb:level)+1),5))"/>
			</xsl:attribute>-->
<!-- titre de la table des matières -->
<!--<xsl:template match="dtb:level">
	<xsl:apply-templates/>
</xsl:template>-->

<!-- un titre -->
<xsl:template match="dtb:p[@class='tit']">
	<titre>
		<xsl:call-template name="createGlobalAttributes" />
		<!-- quel est le niveau du titre?-->
		<xsl:attribute name="niveauOrig">
			<xsl:choose>
				<xsl:when test="ancestor::dtb:level9"><xsl:text>9</xsl:text></xsl:when>
				<xsl:when test="ancestor::dtb:level8"><xsl:text>8</xsl:text></xsl:when>
				<xsl:when test="ancestor::dtb:level7"><xsl:text>7</xsl:text></xsl:when>
				<xsl:when test="ancestor::dtb:level6"><xsl:text>6</xsl:text></xsl:when>
				<xsl:when test="ancestor::dtb:level5"><xsl:text>5</xsl:text></xsl:when>
				<xsl:when test="ancestor::dtb:level4"><xsl:text>4</xsl:text></xsl:when>
				<xsl:when test="ancestor::dtb:level3"><xsl:text>3</xsl:text></xsl:when>
				<xsl:when test="ancestor::dtb:level2"><xsl:text>2</xsl:text></xsl:when>
				<xsl:when test="ancestor::dtb:level1"><xsl:text>1</xsl:text></xsl:when>
				<xsl:otherwise><xsl:text>9</xsl:text></xsl:otherwise>
			</xsl:choose>
		</xsl:attribute>
		<xsl:if test="dtb:pagenum">
			<xsl:attribute name="numNoir">
				<xsl:value-of select="dtb:pagenum"/>
			</xsl:attribute>
		</xsl:if>
		<lit>
			<xsl:call-template name="remplaceEspace">
				<!-- on ne passe pas les infos de mev à un titre car il n'est jamais mis en évidence -->
				<xsl:with-param name="chaine">
					<xsl:value-of disable-output-escaping="yes" select="*[not(self::dtb:pagenum)]|text()" />
				</xsl:with-param>
			</xsl:call-template>
		</lit>
	</titre>
</xsl:template>

<!-- un sous titre -->
<xsl:template match="dtb:p[@class='stit']">
	<titre>
		<xsl:call-template name="createGlobalAttributes" />
		<!-- quel est le niveau du titre?-->
		<xsl:attribute name="niveauOrig">
			<xsl:choose>
				<xsl:when test="ancestor::dtb:level8"><xsl:text>9</xsl:text></xsl:when>
				<xsl:when test="ancestor::dtb:level7"><xsl:text>8</xsl:text></xsl:when>
				<xsl:when test="ancestor::dtb:level6"><xsl:text>7</xsl:text></xsl:when>
				<xsl:when test="ancestor::dtb:level5"><xsl:text>6</xsl:text></xsl:when>
				<xsl:when test="ancestor::dtb:level4"><xsl:text>5</xsl:text></xsl:when>
				<xsl:when test="ancestor::dtb:level3"><xsl:text>4</xsl:text></xsl:when>
				<xsl:when test="ancestor::dtb:level2"><xsl:text>3</xsl:text></xsl:when>
				<xsl:when test="ancestor::dtb:level1"><xsl:text>2</xsl:text></xsl:when>
				<xsl:otherwise><xsl:text>9</xsl:text></xsl:otherwise>
			</xsl:choose>
		</xsl:attribute>
		<lit>
			<xsl:call-template name="remplaceEspace">
				<!-- on ne passe pas les infos de mev à un titre car il n'est jamais mis en évidence -->
				<xsl:with-param name="chaine">
					<xsl:value-of disable-output-escaping="yes" select="." />
				</xsl:with-param>
			</xsl:call-template>
		</lit>
	</titre>
</xsl:template>

<xsl:template match="xhtml:li" mode="#all">
	<xsl:if test="contains(xhtml:p[1]/@style,'break-before:page')"><page-break /></xsl:if>
	<xsl:variable name="puceOrNum">
		<xsl:choose>
			<xsl:when test="..[self::xhtml:ol]">
				<xsl:variable name="numComp">
					<xsl:call-template name="convNumListe" />
				</xsl:variable>
				<xsl:number level="multiple" count="xhtml:li"	format="{$numComp}"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="()" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<doc:li>
		<xsl:for-each select="*|text()">
			<xsl:choose>
				<xsl:when test="self::xhtml:ul or self::xhtml:ol">
					<xsl:apply-templates select="." mode="phrase"/>
				</xsl:when>
				<xsl:when test="self::*">
					<lit>
						<mot><xsl:value-of select="functx:trim(string-join($puceOrNum,''))" /></mot>
						<xsl:apply-templates select="." mode="phrase"/>
					</lit>
				</xsl:when>
				<xsl:otherwise>
					<lit>
						<mot><xsl:value-of select="functx:trim(string-join($puceOrNum,''))" /></mot>
						<xsl:call-template name="remplaceEspace">
							<xsl:with-param name="chaine">
								<xsl:value-of disable-output-escaping="yes" select="." />
							</xsl:with-param>
							<xsl:with-param name="doSpace" tunnel="yes">
								<xsl:call-template name="doSpace" />
							</xsl:with-param>
						</xsl:call-template>
					</lit>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</doc:li>
	<xsl:if test="contains(xhtml:p[last()]/@style,'break-after:page')"><page-break /></xsl:if>
</xsl:template>

<xsl:template match="xhtml:ul|xhtml:ol" mode="#all">
	<xsl:element name="{local-name(.)}">
		<xsl:call-template name="createGlobalAttributes" />
		<xsl:apply-templates />
	</xsl:element>
</xsl:template>

<xsl:template match="dtb:p|dtb:author|xhtml:td|xhtml:pre|xhtml:p|xhtml:title">
	<!-- je vire le test du champ vide car c'est important dans certain cas (cellules de tableaux vides) en fait... -->
	<!--<xsl:if test="(not(string(.)=' ') and not(string(.)=''))">-->
	<!-- je vire pour garder les lignes vides <xsl:if test="not(string(.)='')">-->
	<xsl:choose>
		<!-- si c'est une @ de bas de page: déjà traité -->
		<xsl:when test="@class='Footnote'"/>
		<!-- s'il y a du mathml venant de word -->
		<xsl:when test="*[@class='MTConvertedEquation']">
			<xsl:call-template name="convertMaths" />
		</xsl:when>
		<xsl:when test="translate(string(.),'&nbsp; 	','')='' and not(dtb:img)">
			<xsl:if test="empty((preceding-sibling::*[1]/xhtml:span[@class='MTConvertedEquation'], following-sibling::*[1]/xhtml:span[@class='MTConvertedEquation']))">
				<phrase />
			</xsl:if>
		</xsl:when>
		<xsl:when test="self::xhtml:td and dtb:p">
			<!-- la cellule est composée de paragraphes donc on apply -->
			<xsl:apply-templates />
		</xsl:when>
		<xsl:otherwise>
			<phrase>
				<!-- le contenu n'est pas vide; pour simplifier, on crée forcement un tag lit suivi d'un tag mot, s'ils sont vides, on les supprimera dans la feuille de style de transcription -->
				<xsl:call-template name="createGlobalAttributes" />
				
				<!-- si c'est une note de fin -->
				<xsl:if test="*[@class='endnotereference']">
					<xsl:attribute name="styleOrig">Endnote</xsl:attribute>
				</xsl:if>
				<xsl:variable name="miseEnEv" select="nat:miseEnEvidence(.)" />
				<xsl:variable name="hauteurTexte" select="nat:miseEnHauteur(.)"/>
				
				<lit>
					<xsl:for-each select="xhtml:img|*|text()">
						<!--<xsl:sort select="position()"/>-->
						<!--<xsl:value-of select="."/>-->
						<!-- rappel : dans un for-each, le noeud local devient le fils d'où le ../@class -->
						<xsl:choose>
							<xsl:when test="node()|self::xhtml:img">
								<xsl:apply-templates select="." mode="phrase">
									<xsl:with-param name="attrValues" select="(string($miseEnEv),$hauteurTexte,string(not(../@class=$stylesG1)))" tunnel="yes"/>
								</xsl:apply-templates>
							</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="remplaceEspace">
									<xsl:with-param name="chaine">
										<xsl:value-of disable-output-escaping="yes" select="." />
									</xsl:with-param>
									<xsl:with-param name="attrValues" select="(string($miseEnEv),$hauteurTexte, string(not(../@class=$stylesG1)))" tunnel="yes"/>
									<xsl:with-param name="doSpace" tunnel="yes">
										<xsl:call-template name="doSpace" />
									</xsl:with-param>
								</xsl:call-template>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</lit>
			</phrase>
		</xsl:otherwise>
	</xsl:choose>
	<!-- gestion des notes de bas de page -->
	<xsl:for-each select="dtb:noteref">
		<phrase styleOrig='Footnote'>
			<lit>
				<xsl:call-template name="remplaceEspace">
					<xsl:with-param name="chaine">
						<xsl:value-of disable-output-escaping="yes" select="." />
					</xsl:with-param>
					<xsl:with-param name="doSpace" tunnel="yes">
						<xsl:call-template name="doSpace" />
					</xsl:with-param>
				</xsl:call-template>
			</lit>
		</phrase>
	</xsl:for-each>
</xsl:template>

<xsl:template match="dtb:p|dtb:author|xhtml:pre|xhtml:p|xhtml:h1|xhtml:h2|xhtml:h3|xhtml:h4|xhtml:h5|xhtml:h6|xhtml:h7|xhtml:h8|xhtml:h9|xhtml:title" mode="phrase">
	<!-- je vire le test du champ vide car c'est important dans certain cas (cellules de tableaux vides) en fait... -->
	<!-- A PRIORI CE TEMPLATE MATCH NE SERT JAMAIS... mais ptête je me trompe -->
	<!--<xsl:if test="(not(string(.)=' ') and not(string(.)=''))">-->
	<xsl:if test="not(string(.)='')">
		<xsl:if test="local-name(.)='li'">
			<xsl:choose>
				<xsl:when test="local-name(parent::*[1])='ul'">
					<ponctuation type="puce">-</ponctuation>
				</xsl:when>
				<xsl:when test="local-name(parent::*[1])='ol'">
					<ponctuation>
						<xsl:attribute name="type">ordre</xsl:attribute>
						<xsl:attribute name="num">
							<xsl:value-of select="count(preceding-sibling::*[name()='li'])+1"/>
						</xsl:attribute>
						<xsl:text>-</xsl:text>
					</ponctuation>
				</xsl:when>
			</xsl:choose>
		</xsl:if>
		<xsl:for-each select="*|text()|xhtml:img">
			<!--<xsl:sort select="position()"/>-->
			<!--<xsl:value-of select="."/>-->
			<xsl:choose>
				<xsl:when test="self::*|self::xhtml:img">
					<xsl:apply-templates select="." mode="phrase"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="remplaceEspace">
						<xsl:with-param name="chaine">
							<xsl:value-of disable-output-escaping="yes" select="." />
						</xsl:with-param>
						<xsl:with-param name="doSpace" tunnel="yes">
							<xsl:call-template name="doSpace" />
						</xsl:with-param>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</xsl:if>
</xsl:template>

<xsl:template match="xhtml:span" mode="phrase">
	<xsl:param name="attrValues" as="xs:string*" select="('0','','true')" tunnel="yes"/>
	<xsl:choose>
		<!-- c'est des maths convertis par openoffice -->
		<xsl:when test="@class='MTConvertedEquation'">
			<xsl:variable name="chimie" as="xs:boolean" select="../@class=$chemistryStyle"/>
			<xsl:variable name="expr">
				<xsl:choose>
					<xsl:when test="matches(string(.),'&lt;[^/]*math')">
						<xsl:choose>
							<xsl:when test="$chimie">
								<xsl:text disable-output-escaping="yes">&lt;chimie&gt;</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text disable-output-escaping="yes">&lt;math xmlns="http://www.w3.org/1998/Math/MathML"</xsl:text>
								<xsl:value-of select="substring-after(string(.),'math')" />
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="if($chimie) then replace(replace(string(.),'/math','/chimie'),
							'&lt;semantics','&lt;semantics xmlns=&quot;http://www.w3.org/1998/Math/MathML&quot;')
							else string(.)"/>
					</xsl:otherwise>
					<!-- xsl:value-of select="text()" ça marche des fois pas car on peut avoir un fils span !!!! -->
				</xsl:choose>
			</xsl:variable>
			<xsl:value-of select="$expr" disable-output-escaping="yes"/>
		</xsl:when>
		
		<xsl:when test="not(string(.)=' ') and not(string(.)='') or xhtml:img ">
		<!-- Attention au caractère d'espace précédent, il faut le mettre dans le bon jeu de caractère sinon le test ne marche pas!!!! -->
			<xsl:variable name="miseEnEv" select="nat:chgtMiseEnEvidence(.,xs:integer($attrValues[$mev]))" />
			<xsl:variable name="hauteurTexte" select="nat:chgtMiseEnHauteur(.,$attrValues[$hauteur])"/>
			<xsl:for-each select="*|text()">
			<!-- rappel : dans un for-each, le noeud local devient le fils d'où le ../@class -->
				<xsl:choose>
					<xsl:when test="self::*">
						<xsl:apply-templates select="." mode="phrase">
							<xsl:with-param name="attrValues" select="(string($miseEnEv),$hauteurTexte, string(not(../@class=$stylesG1)))" tunnel="yes"/>
						</xsl:apply-templates>
					</xsl:when>
					<xsl:otherwise>
						<xsl:variable name="chaine" select="translate(.,'&nbsp;',' ')" />
						<xsl:call-template name="remplaceEspace">
							<xsl:with-param name="chaine" select="$chaine" />
							<xsl:with-param name="attrValues" select="(string($miseEnEv),$hauteurTexte, string(not(../@class=$stylesG1)))" tunnel="yes"/>
							<!-- node() match les noeuds text() et element() donc c'est lui qu'il faut utiliser ci-dessous -->
							<!-- voir http://www.w3.org/TR/xpath-functions/#flags pour le flag 'm' de fn:matches -->
							<xsl:with-param name="doSpace" tunnel="yes">
								<xsl:call-template name="doSpace" />
							</xsl:with-param>
						</xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</xsl:when>
	</xsl:choose>
</xsl:template>

<xsl:template match="xhtml:tt|xhtml:cite|xhtml:dfn|xhtml:samp|xhtml:code|xhtml:var" mode="phrase">
	<xsl:if test="not(string(.)=' ') and not(string(.)='')">
	<!-- Attention au caractère d'espace précédent, il faut le mettre dans le bon jeu de caractère sinon le test ne marche pas!!!! -->
		<xsl:for-each select="*|text()">
			<xsl:choose>
				<xsl:when test="self::*">
					<xsl:apply-templates select="." mode="phrase"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="remplaceEspace">
						<xsl:with-param name="chaine" select="translate(.,'&nbsp;',' ')" />
						<xsl:with-param name="doSpace" tunnel="yes">
							<xsl:call-template name="doSpace" />
						</xsl:with-param>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</xsl:if>
</xsl:template>

<xsl:template match="xhtml:a" mode="phrase">
	<!--<xsl:if test="text()">-->
	<!-- on vire ça pour l'instant -->
	<!--<xsl:call-template name="remplaceEspace">
		<xsl:with-param name="chaine">
				<xsl:value-of select="concat(.,'(',@href,')')" />
		</xsl:with-param>
		<xsl:with-param name="doSpace" tunnel="yes">
			<xsl:call-template name="doSpace" />
		</xsl:with-param>
	</xsl:call-template>-->
	<xsl:call-template name="remplaceEspace">
		<!-- text() si pas de href, href vide, note de fin, href=text()
		sinon, concat de . et ajout de href entre parenthèse -->
		<xsl:with-param name="chaine" select="if(starts-with(@href,'#') or @href='' or not(@href) or @class=('EndnoteSymbol','FootnoteSymbol') or string(.)=string(@href)) then text() else concat(.,'(',@href,')')" />
		<!-- node() match les noeuds text() et element() donc c'est lui qu'il faut utiliser ci-dessous -->
		<!-- voir http://www.w3.org/TR/xpath-functions/#flags pour le flag 'm' de fn:matches -->
		<xsl:with-param name="doSpace" tunnel="yes">
			<xsl:call-template name="doSpace" />
		</xsl:with-param>
		<xsl:with-param name="appel" select="if (../@class=('Endnoteanchor','Footnoteanchor')) then true() else false()" tunnel="yes"/>
	</xsl:call-template>
</xsl:template>

<xsl:template match="xhtml:img" mode="phrase">
	<xsl:if test="not(@alt=' ' or @alt='')">
		<xsl:call-template name="remplaceEspace">
			<xsl:with-param name="chaine"><!-- espace après le [ pour que le [ soit bien considéré comme une ponctuation-->
					<xsl:value-of select="concat(.,'[ ',@alt,']')" />
			</xsl:with-param>
			<xsl:with-param name="doSpace" tunnel="yes">
				<xsl:call-template name="doSpace" />
			</xsl:with-param>
		</xsl:call-template>
	</xsl:if>
	<xsl:if test="$processImage">
		<xsl:call-template name="remplaceEspace">
			<xsl:with-param name="chaine"><!-- espace après le [ pour que le [ soit bien considéré comme une ponctuation-->
					<xsl:value-of select="concat('Conversion de l''image en Braille en annexe ',count(preceding::xhtml:img)+1)" />
			</xsl:with-param>
		</xsl:call-template>
	</xsl:if>
</xsl:template>

<xsl:template match="xhtml:img">
	<phrase>
		<lit>
			<xsl:if test="not(@alt=' ' or @alt='')">
				<xsl:call-template name="remplaceEspace">
					<xsl:with-param name="chaine"><!-- espace après le [ pour que le [ soit bien considéré comme une ponctuation-->
							<xsl:value-of select="concat(.,'[ ',@alt,']')" />
					</xsl:with-param>
				</xsl:call-template>
			</xsl:if>
			<xsl:if test="$processImage">
				<xsl:call-template name="remplaceEspace">
					<xsl:with-param name="chaine"><!-- espace après le [ pour que le [ soit bien considéré comme une ponctuation-->
							<xsl:value-of select="concat('Conversion de l''image en Braille en annexe ',count(preceding::xhtml:img)+1)" />
					</xsl:with-param>
				</xsl:call-template>
			</xsl:if>
		</lit>
	</phrase>
</xsl:template>

<xsl:template match="dtb:sup" mode="phrase">
	<xsl:param name="attrValues" as="xs:string*" select="('0','')" tunnel="yes"/>
	<xsl:if test="not(string(.)=' ') and not(string(.)='')">
	<!-- Attention au caractère d'espace précédent, il faut le mettre dans le bon jeu de caractère sinon le test ne marche pas!!!! -->
		<xsl:for-each select="*|text()">
			<xsl:choose>
				<xsl:when test="self::*">
					<xsl:apply-templates select="." mode="phrase"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="remplaceEspace">
						<xsl:with-param name="chaine" select="translate(.,'&nbsp;',' ')" />
						<xsl:with-param name="attrValues" select="($attrValues[$mev],'exp')" tunnel="yes"/>
						<xsl:with-param name="doSpace" tunnel="yes">
							<xsl:call-template name="doSpace" />
						</xsl:with-param>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</xsl:if>
</xsl:template>

<xsl:template match="dtb:sub" mode="phrase">
	<xsl:param name="attrValues" as="xs:string*" select="('0','')" tunnel="yes"/>
	<xsl:if test="not(string(.)=' ') and not(string(.)='')">
		<xsl:for-each select="*|text()">
			<xsl:choose>
				<xsl:when test="self::*">
					<xsl:apply-templates select="." mode="phrase"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="remplaceEspace">
						<xsl:with-param name="chaine" select="translate(.,'&nbsp;',' ')" />
						<xsl:with-param name="attrValues" select="($attrValues[$mev],'ind')" tunnel="yes"/>
						<xsl:with-param name="doSpace" tunnel="yes">
							<xsl:call-template name="doSpace" />
						</xsl:with-param>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</xsl:if>
</xsl:template>

<xsl:template match="dtb:em" mode="phrase">
	<xsl:param name="attrValues" as="xs:string*" select="('0','')" tunnel="yes"/>
	<!-- on suppose qu'on est forcement dans une phrase -->
	<xsl:if test="not(string(.)=' ') and not(string(.)='')">
	<!-- Attention au caractère d'espace précédent, il faut le mettre dans le bon jeu de caractère sinon le test ne marche pas!!!! -->
		<xsl:for-each select="*|text()">
			<xsl:choose>
				<xsl:when test="self::*">
					<xsl:apply-templates select="." mode="phrase"/>
				</xsl:when>
				<xsl:when test="true() or not(../following-sibling::* or ../preceding-sibling::*)"><!-- TODO -->
					<xsl:call-template name="remplaceEspace">
						<xsl:with-param name="chaine"><xsl:value-of disable-output-escaping="yes" select="." />"</xsl:with-param>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="remplaceEspace">
						<xsl:with-param name="chaine" select="translate(.,'&nbsp;',' ')" />
						<xsl:with-param name="attrValues" select="('2',$attrValues[$hauteur])" tunnel="yes"/>
						<xsl:with-param name="doSpace" tunnel="yes">
							<xsl:call-template name="doSpace" />
						</xsl:with-param>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</xsl:if>
</xsl:template>

<xsl:template match="dtb:strong" mode="phrase">
	<xsl:param name="attrValues" as="xs:string*" select="('0','')" tunnel="yes"/>
	<xsl:if test="not(string(.)=' ') and not(string(.)='')">
	<!-- Attention au caractère d'espace précédent, il faut le mettre dans le bon jeu de caractère sinon le test ne marche pas!!!! -->
		<xsl:for-each select="*|text()">
			<xsl:choose>
				<xsl:when test="self::*">
					<xsl:apply-templates select="." mode="phrase"/>
				</xsl:when>
				<xsl:when test="true() or not(../following-sibling::* or ../preceding-sibling::*)"><!-- TODO -->
					<xsl:call-template name="remplaceEspace">
						<xsl:with-param name="chaine" select="translate(.,'&nbsp;',' ')" />
						<xsl:with-param name="doSpace" tunnel="yes">
							<xsl:call-template name="doSpace" />
						</xsl:with-param>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="remplaceEspace">
						<xsl:with-param name="chaine" select="translate(.,'&nbsp;',' ')" />
						<xsl:with-param name="attrValues" select="('1',$attrValues[$hauteur])" tunnel="yes"/>
						<xsl:with-param name="doSpace" tunnel="yes">
							<xsl:call-template name="doSpace" />
						</xsl:with-param>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</xsl:if>
</xsl:template>

<!-- musique recopé tel quel -->
<xsl:template match="score-partwise">
		<xsl:copy-of select="."/>
</xsl:template>

<!--math recopiés tels quel-->
<xsl:template match="m:math" mode="phrase">
	<xsl:text disable-output-escaping="yes">&lt;/lit&gt;</xsl:text>
	<xsl:choose>
		<xsl:when test="some $d in ancestor::* satisfies $d/@class=$stylesChimie">
			<chimie>
				<xsl:copy-of select="./*"/>
			</chimie>
		</xsl:when>
		<xsl:otherwise>
			<xsl:copy-of select="."/>
		</xsl:otherwise>
	</xsl:choose>
<!--
		<xsl:copy><!- disable-output-escaping="yes">->
			<xsl:apply-templates select="*|text()|processing-instruction()"/>
		</xsl:copy>-->
	<xsl:text disable-output-escaping="yes">&lt;lit&gt;</xsl:text>
</xsl:template>



<xsl:template name="remplaceEspace">
	<xsl:param name="chaine" />
	<xsl:param name="doSpace" as="xs:boolean" select="true()" tunnel="yes" />
	<xsl:param name="attrValues" as="xs:string*" select="('0','','true')" tunnel="yes"/>
	<xsl:param name="appel" as="xs:boolean" select="false()" tunnel="yes"/>
	
	<xsl:variable name="chaine2">
		<!-- on remplace insécables, tabulation, et saut de ligne par des espaces, on vire les espaces du début -->
		<xsl:variable name="tmp" select="fn:replace(translate($chaine,'&nbsp;	&#10;&#13;','    '),'^[ ]+','')"/>
		<!-- on remplace les apostrophes pourris (non sécables) et les tirets nuls, et les saisies erronées des oeu et oei	-->
		<xsl:value-of select="replace(replace(replace($tmp,'‑','-'),'(.)[’](.)','$1''$2'),'oe(i|u)','œ$1')"/>
	</xsl:variable>
	
	<!--<xsl:variable name="dernier" select="position()=last()" as="xs:boolean" />-->

	<!-- on découpe la chaine par espaces et on garde que les non vides -->
	<xsl:variable name="chaines" as="xs:string*" select="tokenize($chaine2,'\s')[string(.)]"/>
	<xsl:for-each select="$chaines">
		<xsl:choose>
			<!-- ponctuation seule -->
			<xsl:when test=". =''"/>
			<xsl:when test="string-length(.)=1 and (contains(concat($l_ponct, '-'),substring(.,1,1)))">
				<ponctuation>
					<xsl:value-of select="." />
				</ponctuation>
			</xsl:when>
			<xsl:when test="string-length(.)=1 and starts-with(.,'''')">
				<ponctuation>&apos;</ponctuation>
			</xsl:when>
			<xsl:when test="string-length(.)=3 and substring(.,1,3)='...'">
				<ponctuation>...</ponctuation>
			</xsl:when>
			<!-- ponctuation au début du mot sans espace -->
			<xsl:when test="contains($l_ponct,substring(.,1,1))">
				<ponctuation>
				<xsl:value-of select="substring(., 1, 1)" />
				</ponctuation>
				<xsl:call-template name="remplaceEspace">
					<xsl:with-param name="chaine">
						<xsl:value-of select="substring(.,2,string-length(.))" />
					</xsl:with-param>
				</xsl:call-template>
			</xsl:when>
			<!-- ponctuation à la fin du mot sans espace -->
			<xsl:when test="string-length(.)&gt;2 and substring(., string-length(.)-2, string-length(.))='...'">
				<xsl:call-template name="remplaceEspace">
					<xsl:with-param name="chaine">
						<xsl:value-of select="substring(.,1,string-length(.)-3)" />
					</xsl:with-param>
				</xsl:call-template>
				<ponctuation>
				<xsl:text>...</xsl:text>
				</ponctuation>
			</xsl:when>
			<xsl:when test="contains($l_ponct,substring(., string-length(.), string-length(.)))">
				<xsl:call-template name="remplaceEspace">
					<xsl:with-param name="chaine">
						<xsl:value-of select="substring(.,1,string-length(.)-1)" />
					</xsl:with-param>
				</xsl:call-template>
				<ponctuation>
				<xsl:value-of select="substring(., string-length(.), string-length(.))" />
				</ponctuation>
			</xsl:when>
			<xsl:otherwise>
				<mot>
					<!-- <xsl:if test="not(string($attrname)='')">
						<xsl:attribute name="{string($attrname)}">
							<xsl:value-of select="string($attrvalue)"/>
						</xsl:attribute>
					</xsl:if> -->
					<xsl:for-each select="$attrNames">
						<xsl:variable name="i" select="position()" />						
						<xsl:if test="$i &lt; 3 and not($attrValues[$i]=('0','')[$i])">
							<xsl:attribute name="{$attrNames[$i]}" select="$attrValues[$i]" />
						</xsl:if>
						<xsl:if test="$i=3 and $attrValues[$i]='false'">
							<xsl:attribute name="{$attrNames[$i]}" select="'false'" />
						</xsl:if>
					</xsl:for-each>
					<xsl:if test="not($doSpace) and (position()=last())">
						<xsl:attribute name="doSpace" select="$doSpace" />
					</xsl:if>
					<xsl:if test="$appel">
						<xsl:attribute name="appel"/>
					</xsl:if>
					<xsl:value-of select="."/>
				</mot>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:for-each>
</xsl:template>

<!-- fonction renvoyant la valeur d'une propriété de style de @class en vérifiant qu'elle n'est
pas annulée par l'attribut @style local 
exemple pour list-style-type : decimal ou roman ou alpha, upper-alpha...-->
<xsl:function name="nat:styleValue" as="xs:string?">
	<xsl:param name="n" as="element()" />
	<xsl:param name="attribName" as="xs:string" />
	<xsl:variable name="attributeName" as="xs:string" select="concat($attribName,':')" />
	
	<xsl:choose>
		<xsl:when test="contains($n/@style,$attributeName)">
			<xsl:value-of select="functx:substring-before-match(substring-after($n/@style,$attributeName),'[;}]')" />
		</xsl:when>
		<!-- si le tag n n'a pas de style sous forme d'attribut, on va le chercher dans la liste de départ -->
		<xsl:otherwise>
			<xsl:variable name="styleComplet" as="xs:string*" select="$styles[starts-with(.,concat(local-name($n),'.',string($n/@class),'{'))]" />
			<xsl:variable name="styleDansListe" as="xs:string*" select="
				if ($n[@class]) then
					if (string-length($styleComplet) > 0) then $styleComplet
					else ($styles[starts-with(.,concat('.',string($n/@class),'{'))])
				else $styles[starts-with(.,concat(local-name($n),'{'))]" />
			<xsl:value-of select="functx:substring-before-match(substring-after($styleDansListe,$attributeName),'[;}]')" />
		</xsl:otherwise>
	</xsl:choose>
</xsl:function>

<!-- fonction donnant vrai ou faux sur une propriété de style de @class en vérifiant qu'elle n'est
pas annulée par l'attribut @style local 
exemple pour gras : attrName = font-weight, attrYes = (bold,bolder), attrNo=(normal,lighter)-->
<xsl:function name="nat:styleChecker" as="xs:boolean">
	<xsl:param name="n" as="element()" />
	<xsl:param name="attributeName" as="xs:string" />
	<xsl:param name="attribYes" as="xs:string*" />
	<xsl:param name="attribNo" as="xs:string*" />
	
	<xsl:variable name="attributeYes" as="xs:string*">
		<xsl:sequence select="for $a in $attribYes return concat($attributeName,':',$a)" />
	</xsl:variable>
	<xsl:variable name="attributeNo" as="xs:string*">
		<xsl:sequence select="for $a in $attribNo return concat($attributeName,':',$a)" />
	</xsl:variable>
	
	<xsl:choose>
		<xsl:when test="exists($attributeYes) and functx:contains-any-of($n/@style,$attributeYes)">
			<xsl:value-of select="true()" />
		</xsl:when>
		<xsl:when test="exists($attributeNo) and functx:contains-any-of($n/@style,$attributeNo)">
			<xsl:value-of select="false()" />
		</xsl:when>
		<!-- si le tag n n'a pas de style sous forme d'attribut, on va le chercher dans la liste de départ -->
		<xsl:otherwise>
			<xsl:variable name="styleComplet" as="xs:string*" select="$styles[starts-with(.,concat(local-name($n),'.',string($n/@class),'{'))]" />
			<xsl:variable name="styleDansListe" as="xs:string*" select="
				if ($n[@class]) then
					if (string-length($styleComplet) > 0) then $styleComplet
					else ($styles[starts-with(.,concat('.',string($n/@class),'{'))])
				else $styles[starts-with(.,concat(local-name($n),'{'))]" />
			<xsl:value-of select="exists($attributeYes) and (functx:contains-any-of($styleDansListe,$attributeYes))" />
		</xsl:otherwise>
	</xsl:choose>
</xsl:function>

<!-- fonction renvoyant 1 si gras, 2 si italique 3 si souligné, 0 sinon 
pourra être étendu pour mev de niv 2 et 3 ; ne prend pas en compte les
mev multiples (ex : gras/souligné) -->
<xsl:function name="nat:miseEnEvidence" as="xs:integer">
	<xsl:param name="n" as="element()" />
	
	<!-- les xsl:message ne marchent pas à l'intérieur des functions-->
	<!--<xsl:message select="('klmj',$styleDansListe)" />-->
	<xsl:choose>
		<!-- gras -->
		<xsl:when test="nat:styleChecker($n,'font-weight',('bold','bolder'),('normal','lighter'))">
			<xsl:value-of select="1" />
		</xsl:when>
		<!-- italique -->
		<xsl:when test="nat:styleChecker($n,'font-style',('italic','oblique'),('normal'))">
			<xsl:value-of select="2" />
		</xsl:when>
		<!-- souligné -->
		<xsl:when test="nat:styleChecker($n,'text-decoration',('underline'),('none'))">
			<xsl:value-of select="3" />
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="0" />
		</xsl:otherwise>
	</xsl:choose>
</xsl:function>

<!-- fonction détectant les changements de mev -->
<xsl:function name="nat:chgtMiseEnEvidence" as="xs:integer">
	<xsl:param name="n" as="element()" />
	<xsl:param name="avant" as="xs:integer" />
	
	<xsl:choose>
		<xsl:when test="$avant=0">
			<xsl:value-of select="nat:miseEnEvidence($n)"/>
		</xsl:when>
		<!-- arrêt du gras donc renvoi de l'éventuelle nouvelle mev -->
		<xsl:when test="$avant=1 and nat:styleChecker($n,'font-weight',('normal','lighter'), ('bold','bolder'))">
			<xsl:value-of select="nat:miseEnEvidence($n)" />
		</xsl:when>
		<!-- arrêt du italique donc renvoi de l'éventuelle nouvelle mev -->
		<xsl:when test="$avant=2 and nat:styleChecker($n,'font-style',('normal'),('italic','oblique'))">
			<xsl:value-of select="nat:miseEnEvidence($n)" />
		</xsl:when>
		<!-- arrêt du souligné donc renvoi de l'éventuelle nouvelle mev -->
		<xsl:when test="$avant=3 and nat:styleChecker($n,'text-decoration',('none'),('underline'))">
			<xsl:value-of select="nat:miseEnEvidence($n)" />
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$avant" />
		</xsl:otherwise>
	</xsl:choose>
</xsl:function>

<!-- fonction renvoyant exp si exposant, ind si indice, '' autrement -->
<xsl:function name="nat:miseEnHauteur" as="xs:string">
	<xsl:param name="n" as="element()" />
	
	<xsl:choose>
		<!-- exposant -->
		<xsl:when test="nat:styleChecker($n,'vertical-align',('super','top','text-bottom','+'),('middle'))">
			<xsl:value-of select="'exp'" />
		</xsl:when>
		<!-- indice -->
		<xsl:when test="nat:styleChecker($n,'vertical-align',('sub','bottom','text-top','baseline','-'),('middle'))">
			<xsl:value-of select="'ind'" />
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="''" />
		</xsl:otherwise>
	</xsl:choose>
</xsl:function>

<!-- fonction détectant les changements de mise en hauteur -->
<xsl:function name="nat:chgtMiseEnHauteur" as="xs:string">
	<xsl:param name="n" as="element()" />
	<xsl:param name="avant" as="xs:string" />
	
	<xsl:choose>
		<!-- exposant -->
		<xsl:when test="nat:styleChecker($n,'vertical-align',('super','top','text-bottom','+'),('middle'))">
			<xsl:value-of select="'exp'" />
		</xsl:when>
		<!-- indice -->
		<xsl:when test="nat:styleChecker($n,'vertical-align',('sub','bottom','text-top','baseline','-'),('middle'))">
			<xsl:value-of select="'ind'" />
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$avant" />
		</xsl:otherwise>
	</xsl:choose>
</xsl:function>

<!-- template créant les attributs pour les tags de premier niveau : titres, listes, paragraphes -->
<xsl:template name="createGlobalAttributes">
	<xsl:if test="@xml:lang='es-ES'">
		<xsl:attribute name="lang">
			<xsl:value-of select="string('es')"/>
		</xsl:attribute>
	</xsl:if>
	<xsl:if test="@class">
		<xsl:attribute name="styleOrig" select="@class"  />
	</xsl:if>
	<xsl:if test="@class=$stylesG1">
		<xsl:attribute name="abrege" select="'false'" />
	</xsl:if>
	<xsl:if test="nat:styleChecker(.,'text-align',('center'),(''))">
		<xsl:attribute name="center" select="true()"  />
	</xsl:if>
	<xsl:if test="self::xhtml:ul or self::xhtml:ol">
		<xsl:attribute name="list-style-type" select="nat:styleValue(.,'list-style-type')" />
	</xsl:if>
</xsl:template>

<!-- template qui renvoie vrai s'il faut mettre un espace après le noeud passé en paramètre.
utile pour les mev à l'intérieur de mots, les bugs style <span>mick</span><span>aelig;</span><span>l</span> -->
<xsl:template name="doSpace" as="xs:boolean">
	<!-- node() match les noeuds text() et element() donc c'est lui qu'il faut utiliser ci-dessous -->
	<!-- voir http://www.w3.org/TR/xpath-functions/#flags pour le flag 'm' de fn:matches -->
	<xsl:value-of select="fn:matches(.,'\s$') or fn:matches(string(following::text()[1][not(../@class=$stylesG0)]),'^\s')" />
	<!--<xsl:message select="concat('***',string(.),'***',replace(following::text()[1],' ','X'),'***',replace(string(following::node()[1]),' ','X'),'***',fn:matches(.,'\s$'),fn:matches(string(following::node()[1]),'^\s'))"/>-->
</xsl:template>

<!-- template qui traite les enfants texte et élément d'un noeud -->
<xsl:template name="processLitteral">
	<xsl:param name="miseEnEv" as="xs:integer" select="nat:miseEnEvidence(..)"/>
	<xsl:param name="hauteurTexte" as="xs:string" select="nat:miseEnHauteur(..)" />
	
	<xsl:choose>
		<xsl:when test="self::*|self::xhtml:img">
			<xsl:apply-templates select="." mode="phrase">
				<xsl:with-param name="attrValues" select="(string($miseEnEv),$hauteurTexte)" tunnel="yes"/>
			</xsl:apply-templates>
		</xsl:when>
		<xsl:otherwise><!-- noeud texte -->
			<xsl:call-template name="remplaceEspace">
				<xsl:with-param name="chaine">
					<xsl:value-of disable-output-escaping="yes" select="." />
				</xsl:with-param>
				<xsl:with-param name="attrValues" select="(string($miseEnEv),$hauteurTexte)" tunnel="yes"/>
				<xsl:with-param name="doSpace" tunnel="yes">
					<xsl:call-template name="doSpace" />
				</xsl:with-param>
			</xsl:call-template>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- conversion des numéros de listes -->
<xsl:template name="convNumListe" as="xs:string">
	<xsl:variable name="numComplet" as="xs:string*">
		<xsl:for-each select="ancestor::xhtml:ol">
			<xsl:value-of select="functx:replace-multi(nat:styleValue(.,'list-style-type'),
				('decimal','lower-roman','upper-roman','lower-alpha','upper-alpha','lower-latin','upper-latin'),('1','i','I','a','A','a','A'))"/>
		</xsl:for-each>
	</xsl:variable>
	<xsl:value-of select="string-join(($numComplet,' '),'.')"/>
</xsl:template>

<!-- conversion des maths au niveau du <p> ou <h1> ou <h2> etc qui contient les span @class=mtconvertedequation -->
<xsl:template name="convertMaths">
	<xsl:variable name="nbm" select="count(*[@class='MTConvertedEquation'])" />
	<xsl:choose>
		<xsl:when test="(count(text()[matches(.,'\S')]|*[not(@class='MTConvertedEquation') and matches(.,'\S')]) > 0) or $nbm > 1">
			<!-- Trois cas de figure : littéraire + <math> ou </math> + littéraire ou </math> + littéraire + <math> -->
			<xsl:if test="*[@class='MTConvertedEquation'][1]/preceding-sibling::node()">
			<!-- on a du contenu avant, donc on ouvre la phrase -->
				<xsl:text disable-output-escaping="yes">&lt;phrase </xsl:text>
				<xsl:variable name="att" as="element()">
					<xsl:element name="attribs">
						<xsl:call-template name="createGlobalAttributes" />
					</xsl:element>
				</xsl:variable>
				<xsl:for-each select="$att/@*">
					<xsl:value-of select="concat(local-name(.),'=&quot;',.,'&quot; ')"/>
				</xsl:for-each>
				<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
			</xsl:if>
			<xsl:variable name="miseEnEv" select="nat:miseEnEvidence(.)" />
			<xsl:variable name="hauteurTexte" select="nat:miseEnHauteur(.)"/>
			<xsl:for-each select="*[@class='MTConvertedEquation'][1]">
			<!-- for-each juste pour changer le contexte -->
				<xsl:for-each select="preceding-sibling::node()[matches(.,'\S')]">
					<xsl:if test="position()=1">
						<xsl:text disable-output-escaping="yes">&#10;	&lt;lit&gt;&#10;</xsl:text>
					</xsl:if>
					<xsl:call-template name="processLitteral">
						<xsl:with-param name="miseEnEv" select="$miseEnEv" />
						<xsl:with-param name="hauteurTexte" select="$hauteurTexte"/>
					</xsl:call-template>
					<xsl:if test="position()=last()">
						<xsl:text disable-output-escaping="yes">&#10;	&lt;/lit&gt;&#10;</xsl:text>
					</xsl:if>
				</xsl:for-each>
				<xsl:apply-templates select="." mode="phrase" />
				<xsl:for-each select="following-sibling::node()[not(@class='MTConvertedEquation') and matches(.,'\S')]">
					<!-- le matches c'est pour pas compter les noeuds texte vides -->
					<xsl:if test="position()=1">
						<xsl:text disable-output-escaping="yes">&#10;	&lt;lit&gt;&#10;</xsl:text>
					</xsl:if>
					<xsl:call-template name="processLitteral">
						<xsl:with-param name="miseEnEv" select="$miseEnEv" />
						<xsl:with-param name="hauteurTexte" select="$hauteurTexte"/>
					</xsl:call-template>
					<xsl:if test="position()=last()">
						<xsl:text disable-output-escaping="yes">&#10;	&lt;/lit&gt;&#10;</xsl:text>
						<xsl:if test="$nbm=1"><!-- on est sûr d'être à la fin de la phrase -->
							<xsl:text disable-output-escaping="yes">&lt;/phrase&gt;&#10;</xsl:text>
						</xsl:if>
					</xsl:if>
				</xsl:for-each>
				
				<xsl:if test="matches(string(.),'/.*math') and not(following-sibling::node()[matches(.,'\S')])">
					<xsl:text disable-output-escaping="yes">&lt;/phrase&gt;&#10;</xsl:text>
				</xsl:if>
			</xsl:for-each>
			<xsl:apply-templates select="*[@class='MTConvertedEquation'][2]" mode="phrase" />
		</xsl:when>
		<xsl:otherwise><!-- on a un seul span dans le p -->
			<xsl:if test="matches(string(.),'&lt;[^/]*math')">
				<xsl:text disable-output-escaping="yes">&lt;phrase </xsl:text>
				<xsl:variable name="att" as="element()">
					<xsl:element name="attribs">
						<xsl:call-template name="createGlobalAttributes" />
					</xsl:element>
				</xsl:variable>
				<xsl:for-each select="$att/@*">
					<xsl:value-of select="concat(local-name(.),'=&quot;',.,'&quot; ')"/>
				</xsl:for-each>
				<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
			</xsl:if>
			<xsl:apply-templates mode="phrase" />
			<xsl:if test="matches(string(.),'/.*math')">
				<xsl:text disable-output-escaping="yes">&lt;/phrase&gt;&#10;</xsl:text>
			</xsl:if>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

</xsl:stylesheet>