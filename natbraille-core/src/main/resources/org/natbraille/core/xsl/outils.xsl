<?xml version='1.1' encoding="UTF-8" ?>
<!--
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
-->

<!-- version: 1.1 -->

<!DOCTYPE xsl:stylesheet SYSTEM "nat://system/xsl/mmlents/windob.dtd" >


<xsl:stylesheet version="2.0"
xmlns:xsl='http://www.w3.org/1999/XSL/Transform' 
xmlns:saxon='http://icl.com/saxon'
xmlns:m='http://www.w3.org/1998/Math/MathML'
xmlns:lit='espacelit'
xmlns:doc='espaceDoc'
xmlns:functx='http://www.functx.com'
xmlns:xs='http://www.w3.org/2001/XMLSchema'>

<xsl:output method="xml" encoding="UTF-8" indent="yes"/>

<!-- bcp de variables (seqmin, seqmaj, ... sont définies dans liste-fr-g1.xsl -->

<!-- ****************************************************
	Outils pour les majuscules
************************************* -->
<xsl:template name="donnePosition1MotMaj">
	<!-- 
		renvoie 0 si le mot n'est pas le premier d'un passage en majuscule
		renvoie 1 si le mot est le premier d'un passage en majuscule
		FAUX : renvoie 2 si le mot est le dernier d'un passage en majuscule
		FAUX : renvoie 3 si le mot précédent est en majuscule
		ON SAIT DEJA QUE LE MOT A DES MAJUSCULES-->
	<xsl:param name="position" select="1"/>
	<!--<xsl:value-of select="trace(concat(position(),' ', $position, ' ', string(.),';'),'Voila:')"/>-->
	<xsl:choose>
		<!-- on cherche le premier mot en majuscule précédent si il existe, ou si il y a un mot en minuscule avant -->
		<xsl:when test="position() = 1 or $position = position()">
		<!-- c'est le debut du paragraphe -->
			<xsl:value-of select="1"/>
		</xsl:when>
		<xsl:when test="not(local-name(preceding::*[$position])='mot') or (string(preceding::*[$position]) = $chaine_vide)
				or (preceding::*[$position]/@doSpace='false')">
		<!--le précédent n'est pas un mot ou alors une chaine vide ; on regarde avant -->
			<xsl:call-template name="donnePosition1MotMaj">
				<xsl:with-param name="position" select="$position + 1"/>
			</xsl:call-template>
		</xsl:when>
		<!--<xsl:when test="contains(translate(preceding::*[$position],'°&sup2;&sup3;0123456789abcdefghijklmnopqrstuvwxyzàâéèêëîïôùûü&ccedil;&aelig;&oelig;','aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'),'a')">-->
		<xsl:when test="functx:contains-any-of(string(preceding::*[$position]),$seqMin)">
		<!-- mot non vide et au moins une minuscule : le mot examiné  est donc le premier avec une majuscule -->
 			<xsl:value-of select="1"/>
		</xsl:when>
		<xsl:when test="functx:contains-any-of(string(preceding::*[$position]),$seqMaj)">
		<!-- mot non vide et au moins une majuscule : le mot examiné  n'est donc pas le premier avec une majuscule -->
 			<xsl:value-of select="0"/>
		</xsl:when>
		<xsl:otherwise>
			<!-- mot précédent non vide, sans maj ni min (nombre) : on continue à remonter -->
			<!--<xsl:value-of select="0"/>-->
			<xsl:call-template name="donnePosition1MotMaj">
				<xsl:with-param name="position" select="$position + 1"/>
			</xsl:call-template>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="donnePositionDMotMaj">
	<!-- 
		renvoie 0 si le mot n'est pas le dernier d'un passage en majuscule
		renvoie 1 si le mot est le dernier d'un passage en majusculee -->
	<xsl:param name="position"/>
	<xsl:param name="doSpaceAPrendreEnCompte" as="xs:boolean" select="false()" tunnel="yes" />
	
	<!--<xsl:message select="'mot',string(.),'pos',$position,'exam',string(following::*[$position])" />-->
	<xsl:choose>
		<!-- on cherche le premier mot en majuscule suivant si il existe, ou si il y a un mot en minuscule avant -->
		<xsl:when test="$position + position() &gt; last()">
		<!-- c'est la fin du document -->
			<xsl:value-of select="1"/>
		</xsl:when>
		<xsl:when test="not(local-name(following::*[$position])='mot') or (string(following::*[$position]) = $chaine_vide)
				or (following::*[$position]/@doSpace='false')">
		<!--le suivant n'est pas un mot ou alors une chaine vide ; on regarde après  -->
			<xsl:call-template name="donnePositionDMotMaj">
				<xsl:with-param name="position" select="$position + 1"/>
			</xsl:call-template>
		</xsl:when>
		<xsl:when test="functx:contains-any-of(string(following::*[$position]),$seqMin)">
		<!-- mot non vide et au moins une minuscule : le mot examiné  est donc le dernier avec une majuscule -->
 			<xsl:value-of select="1"/>
		</xsl:when>
		<xsl:when test="functx:contains-any-of(string(following::*[$position]),$seqMaj)">
		<!-- mot non vide et au moins une majuscule : le mot examiné n'est donc pas le dernier avec une majuscule 
		SAUF s'il a un @doSpace='false' car c'est une partie de mot auquel cas il faut relancer la recherche une fois -->
			<xsl:choose>
				<xsl:when test="$doSpaceAPrendreEnCompte">
					<xsl:call-template name="donnePositionDMotMaj">
						<xsl:with-param name="position" select="$position + 1"/>
						<xsl:with-param name="doSpaceAPrendreEnCompte" select="false()" tunnel="yes"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="0"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		<xsl:otherwise>
			<!-- mot précédent non vide, sans maj ni min (nombre) : on continue à parcourir -->
			<!--<xsl:value-of select="0"/>-->
			<xsl:call-template name="donnePositionDMotMaj">
				<xsl:with-param name="position" select="$position + 1"/>
			</xsl:call-template>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="estSuiviMaj">
	<xsl:param name="nb"/>
	<xsl:param name="position"/>
	<!-- renvoie 1 si il y a nb mot en majuscule au moins après le mot -->
	<xsl:choose>
		<xsl:when test="$nb = 0">
			<xsl:value-of select="1"/>
		</xsl:when>
		<xsl:when test="$position + position() + $nb - 1 &gt; last()">
		<!-- c'est la fin du paragraphe -->
			<xsl:value-of select="0"/>
		</xsl:when>
		<xsl:when test="not(local-name(following::*[$position])='mot') or (string(following::*[$position]) = $chaine_vide) or
				following::*[$position]/@doSpace='false'">
		<!-- soit pas un mot soit une chaine vide soit un "bout de mot" avec @dospace=false donc on continue -->
			<xsl:call-template name="estSuiviMaj">
				<xsl:with-param name="position" select="$position + 1"/>
				<xsl:with-param name="nb" select="$nb"/>
			</xsl:call-template>
		</xsl:when>
		<xsl:when test="functx:contains-any-of(string(following::*[$position]),$seqMin)">
			<!-- mot avec au moins une minsucule -->
 			<xsl:value-of select="0"/>
		</xsl:when>
		<xsl:when test="functx:contains-any-of(string(following::*[$position]),$seqMaj)">
			<!-- mot avec au moins une majuscule et pas de minuscule : il compte -->
 			<xsl:call-template name="estSuiviMaj">
				<xsl:with-param name="position" select="$position + 1"/>
				<xsl:with-param name="nb" select="$nb - 1"/>
			</xsl:call-template>
		</xsl:when>
		<xsl:otherwise>
			<!-- mot sans majuscule ni minuscule (nombre...) : il compte pas mais on continue -->
			<xsl:call-template name="estSuiviMaj">
				<xsl:with-param name="position" select="$position + 1"/>
				<xsl:with-param name="nb" select="$nb"/>
			</xsl:call-template>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="estPrecedeMaj">
	<xsl:param name="nb"/>
	<xsl:param name="position"/>
	<!-- renvoie 1 si il y a nb mot en majuscule au moins après le mot -->
	<xsl:choose>
		<xsl:when test="$nb = 0">
			<xsl:value-of select="1"/>
		</xsl:when>
		<xsl:when test="position() - $position - $nb &lt; 0">
		<!-- c'est le début du document -->
			<xsl:value-of select="0"/>
		</xsl:when>
		<xsl:when test="not(local-name(preceding::*[$position])='mot') or (string(preceding::*[$position]) = $chaine_vide) or
		preceding::*[$position]/@doSpace='false'">
		<!-- soit pas un mot soit une chaine vide soit un "bout de mot" avec @dospace=false donc ça compte pas -->
			<xsl:call-template name="estPrecedeMaj">
				<xsl:with-param name="position" select="$position + 1"/>
				<xsl:with-param name="nb" select="$nb"/>
			</xsl:call-template>
		</xsl:when>
		<xsl:when test="functx:contains-any-of(string(preceding::*[$position]),$seqMin)">
			<!-- mot avec au moins une minsucule -->
 			<xsl:value-of select="0"/>
		</xsl:when>
		<xsl:when test="functx:contains-any-of(string(preceding::*[$position]),$seqMaj)">
			<!-- mot avec au moins une majuscule et pas de minuscule : il compte -->
 			<xsl:call-template name="estPrecedeMaj">
				<xsl:with-param name="position" select="$position + 1"/>
				<xsl:with-param name="nb" select="$nb - 1"/>
			</xsl:call-template>
		</xsl:when>
		<xsl:otherwise>
			<!-- mot sans majuscule ni minuscule (nombre...) : il compte pas mais on continue -->
			<xsl:call-template name="estPrecedeMaj">
				<xsl:with-param name="position" select="$position + 1"/>
				<xsl:with-param name="nb" select="$nb"/>
			</xsl:call-template>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- **************************************************
		Outils pour les mises en evidence
	**********************************************
-->
<xsl:template name="donnePosition1MotME">
	<!-- 
		renvoie 0 si le mot n'est le 1er d'un passage en évidence
		renvoie 1 si le mot est le premier d'un passage en évidence
	-->
	<xsl:param name="position"/>
	<xsl:param name="prec" as="element()?"/>
	<!--<xsl:choose>
		<!{1}** on cherche le premier mot en évidence précédent si il existe, ou si il y a un mot pas en évidence avant **{1}>
		<xsl:when test="position() - $position + 1 = 1">
		<!{1}** c'est le debut du þ **{1}>
			<xsl:value-of select="1"/>
		</xsl:when>
		<xsl:when test="not(local-name(preceding::*[$position])='mot') or (local-name(preceding::*[$position])='mot' and string(preceding::*[$position]) = $chaine_vide)">
		<!{1}**pas intéressant**{1}>
			<xsl:call-template name="donnePosition1MotME">
				<xsl:with-param name="position" select="$position + 1"/>
			</xsl:call-template>
		</xsl:when>
		<!{1}** intéressant, en évidence? **{1}>
		<xsl:when test="preceding::*[$position]/@mev=('1','2','3')">
 			<xsl:value-of select="0"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="1"/>
		</xsl:otherwise>
	</xsl:choose>-->
	
	<!--<xsl:value-of select="if (preceding-sibling::*[self::mot][1]/@mev=@mev) then 0 else 1" />-->
	<xsl:value-of select="if ($prec/@mev=@mev) then 0 else 1" />
	
</xsl:template>

<xsl:template name="donnePositionDMotME">
	<!-- 
		renvoie 0 si le mot n'est pas le dernier d'un passage en évidence
		renvoie 1 si le mot est le dernier d'un passage en évidence -->
	<xsl:param name="position"/>
	<!--<xsl:choose>
		<!{1}** on cherche le premier mot en évidence suivant si il existe, ou si il y a un mot pas en évidence avant **{1}>
		<xsl:when test="$position + position() &gt; last()">
		<!{1}** c'est la fin du document **{1}>
			<xsl:value-of select="1"/>
		</xsl:when>
		<xsl:when test="not(local-name(following::*[$position])='mot') or (local-name(following::*[$position])='mot' and string(following::*[$position]) = $chaine_vide)">
		<!{1}**pas intéressant**{1}>
			<xsl:call-template name="donnePositionDMotME">
				<xsl:with-param name="position" select="$position + 1"/>
			</xsl:call-template>
		</xsl:when>
		<!{1}** intéressant, en évidence? **{1}>
		<xsl:when test="following::*[$position]/@mev=('1','2','3')">
 			<xsl:value-of select="0"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="1"/>
		</xsl:otherwise>
	</xsl:choose>-->
	<!-- le mot suivant varie en abrégé car il y a un découpage en mots sur ' et - et pas en intégral 
	MAIS faut aussi bidouiller en intégral car des fois les mots ont des dospace=false-->
	<xsl:variable name="motsuivant" as="xs:integer">
		<xsl:choose>
			<xsl:when test="$abrege and (@doSpace='false') and (following-sibling::*[self::mot][1][@doSpace='false']='-')
				and exists(following-sibling::*[self::mot][2])">3</xsl:when>
			<xsl:when test="$abrege and (@doSpace='false') and ends-with(.,'''') and exists(following-sibling::*[self::mot][1])">2</xsl:when>
			<xsl:when test="@doSpace='false' and exists(following-sibling::mot[(not(@doSpace='false') and not(@hauteur)) or (@hauteur)][2])">
				<!-- le vrai mot suivant est le deuxième qui n'a pas de dospace false car le 1er est celui qui finit le mot actuel -->
				<!-- l'expression peut certainement se simplifier mais franchement entre les dospace et les hauteur qu'il faut pas compter j'ai pas trouvé plus simple -->
				<xsl:value-of select="index-of(following-sibling::mot,following-sibling::mot[(not(@doSpace='false') and not(@hauteur)) or (@hauteur)][2])[1]" />
			</xsl:when>
			<xsl:otherwise>1</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<!--<xsl:message select="('mot',string(.),'suiv',string(following-sibling::*[self::mot][$motsuivant]))"/>-->
	<xsl:value-of select="if ((following-sibling::*[self::mot][$motsuivant]/@mev=@mev)
		or (count(following-sibling::mot)=0 and ../following-sibling::lit[1]/mot[1]/@mev=@mev)) 
		then 0 else 1" />
</xsl:template>

<xsl:template name="estSuiviME">
	<!-- renvoie 1 si il y a nb mot en évidence au moins après le mot -->
	<xsl:param name="nb"/>
	<xsl:param name="position"/>
	<!-- la bidouille du père c'est à cause de la première passe d'abrégé qui effectue les traitements en ayant perdu le père phrase -->
	<!-- de plus on est obligés d'utiliser deep-equal sinon ça bugge lors de la détection du père -->
	<xsl:variable name="pere" select="if(../..) then ../.. else ''"/>
	<xsl:variable name="lesSuiv" select="
		following::mot[not(@hauteur=('ind','exp')) and deep-equal((if(../..) then ../.. else ''),$pere) and not(@doSpace='false')][position() &lt; $nb+1]"/>
	<!-- renvoie 1 si il y a nb mot en évidence au moins après le mot -->
	<xsl:choose>
		<xsl:when test="count($lesSuiv)=$nb and (every $m in $lesSuiv satisfies $m/@mev=('1','2','3'))">
			<xsl:value-of select="1"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="0"/>
		</xsl:otherwise>
	</xsl:choose>
	
	<!--<xsl:message select="string-join((.,'suivants :',for $i in 1 to count($lesSuiv) return string($lesSuiv[$i]),'pere',local-name($pere)),' ')" />-->
	<!--
	<xsl:choose>
		<xsl:when test="$nb = 0">
			<xsl:value-of select="1"/>
		</xsl:when>
		<xsl:when test="$position + position() + $nb &gt; last()+1">-->
		<!-- c'est la fin du document -->
		<!--	<xsl:value-of select="0"/>
		</xsl:when>
		<xsl:when test="not(local-name(following::*[$position])='mot') or
			(string(following::*[$position]) = $chaine_vide or following::*[$position]/@hauteur=('ind','exp'))">-->
		<!--pas intéressant-->
			<!--<xsl:choose>
				<xsl:when test="local-name(following::*[$position])='mot' and string(following::*[$position]) = $chaine_vide">
				<!{1}** on ajoute un mot pour ignorer le mot vide **{1}>
					<xsl:call-template name="estSuiviME">
						<xsl:with-param name="position" select="$position + 1"/>
						<xsl:with-param name="nb" select="$nb"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:when test="local-name(following::*[$position])='ponctuation'">
				<!{1}** on ajoute un mot pour ignorer le mot vide **{1}>
					<xsl:call-template name="estSuiviME">
						<xsl:with-param name="position" select="$position + 1"/>
						<xsl:with-param name="nb" select="$nb"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="estSuiviME">
						<xsl:with-param name="position" select="$position + 1"/>
						<xsl:with-param name="nb" select="$nb"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>-->
			<!--<xsl:call-template name="estSuiviME">
				<xsl:with-param name="position" select="$position + 1"/>
				<xsl:with-param name="nb" select="$nb"/>
			</xsl:call-template>
		</xsl:when>-->
		<!-- intéressant, en évidence? -->
		<!--<xsl:when test="following::*[$position]/@mev=('1','2','3')"> 
 			<xsl:call-template name="estSuiviME">
				<xsl:with-param name="position" select="$position + 1"/>
				<xsl:with-param name="nb" select="$nb - 1"/>
			</xsl:call-template>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="0"/>
		</xsl:otherwise>
	</xsl:choose>-->
</xsl:template>

<xsl:template name="estPrecedeME">
	<!-- renvoie 1 si il y a nb mot en évidence au moins avant le mot -->
	<xsl:param name="nb"/>
	<xsl:param name="position"/>
	<xsl:variable name="pere" select="if(../..) then ../.. else ''"/>
	<!-- la bidouille du père c'est à cause de la première passe d'abrégé qui effectue les traitements en ayant perdu le père phrase -->
	<!-- de plus on est obligés d'utiliser deep-equal sinon ça bugge lors de la détection du père -->
	<xsl:variable name="lesPrec" select="
	preceding::mot[not(@hauteur=('ind','exp')) and deep-equal((if(../..) then ../.. else ''),$pere) and not(@doSpace='false')][position() &lt; $nb+1]"/>
	<!--<xsl:message select="('prec',string(.),':',$lesPrec[position()&gt;count($lesPrec)-$nb], every $m in $lesPrec[position()&gt;count($lesPrec)-$nb] satisfies $m/@mev=('1','2','3'))"/>-->
	<xsl:choose>
		<xsl:when test="count($lesPrec)=$nb and (every $m in $lesPrec satisfies $m/@mev=('1','2','3'))">
			<xsl:value-of select="1"/>
		</xsl:when>
		<xsl:otherwise>			
			<xsl:value-of select="0"/>
		</xsl:otherwise>
	</xsl:choose>
	<!--<xsl:message select="string-join((.,'precedents :',for $i in 1 to count($lesPrec) return string($lesPrec[$i]),'pere',local-name($pere)),' ')" />-->
</xsl:template>
</xsl:stylesheet>
