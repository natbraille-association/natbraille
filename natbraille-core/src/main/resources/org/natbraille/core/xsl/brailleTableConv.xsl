<?xml version='1.0' encoding="UTF-8" ?>
<!DOCTYPE xsl:stylesheet SYSTEM "nat://system/xsl/mmlents/windob.dtd" 
[
<!ENTITY % table_braille PUBLIC "table braille entree" "nat://temp/xsl/inputBrailleTable.ent">
%table_braille;
<!ENTITY % table_conversion PUBLIC "table braille sortie" "nat://temp/xsl/outputBrailleTable.ent">
%table_conversion;
]>
<xsl:stylesheet version="2.0" 
	xmlns:javaNat="java:nat.saxFuncts.SaxFuncts"
	xmlns:xsl='http://www.w3.org/1999/XSL/Transform' 
	xmlns:xs='http://www.w3.org/2001/XMLSchema'
	xmlns:functx='http://www.functx.com'>
	<xsl:output method="text" encoding="UTF-8" indent="no" />
	<xsl:variable name="ptBraille" as="xs:string">
		<xsl:text>&pt;&pt1;&pt12;&pt123;&pt1234;&pt12345;&pt123456;&pt12346;&pt1235;&pt12356;&pt1236;&pt124;&pt1245;&pt12456;&pt1246;&pt125;&pt1256;&pt126;&pt13;&pt134;&pt1345;&pt13456;&pt1346;&pt135;&pt1356;&pt136;&pt14;&pt145;&pt1456;&pt146;&pt15;&pt156;&pt16;&pt2;&pt23;&pt234;&pt2345;&pt23456;&pt2346;&pt235;&pt2356;&pt236;&pt24;&pt245;&pt2456;&pt246;&pt25;&pt256;&pt26;&pt3;&pt34;&pt345;&pt3456;&pt346;&pt35;&pt356;&pt36;&pt4;&pt45;&pt456;&pt46;&pt5;&pt56;&pt6;</xsl:text>
	</xsl:variable>
	<xsl:variable name="ptEmbos" as="xs:string">
		<xsl:text>&pte;&pte1;&pte12;&pte123;&pte1234;&pte12345;&pte123456;&pte12346;&pte1235;&pte12356;&pte1236;&pte124;&pte1245;&pte12456;&pte1246;&pte125;&pte1256;&pte126;&pte13;&pte134;&pte1345;&pte13456;&pte1346;&pte135;&pte1356;&pte136;&pte14;&pte145;&pte1456;&pte146;&pte15;&pte156;&pte16;&pte2;&pte23;&pte234;&pte2345;&pte23456;&pte2346;&pte235;&pte2356;&pte236;&pte24;&pte245;&pte2456;&pte246;&pte25;&pte256;&pte26;&pte3;&pte34;&pte345;&pte3456;&pte346;&pte35;&pte356;&pte36;&pte4;&pte45;&pte456;&pte46;&pte5;&pte56;&pte6;</xsl:text>
	</xsl:variable>
	<xsl:template match="/">
		<xsl:value-of select="translate(string(.),$ptBraille,$ptEmbos)" />
	</xsl:template>
</xsl:stylesheet>