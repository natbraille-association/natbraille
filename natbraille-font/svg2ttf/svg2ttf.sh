#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
fontforge -script "$DIR/script.pe"  $1 $2
