/*
 * NatBraille - An universal Translator - Braille font generator
 * Copyright (C) 2014 Bruno Mascret, Vivien Guillet
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.font;

import java.util.Date;
import org.natbraille.brailletable.BrailleTable;
import org.natbraille.brailletable.BrailleTableException;
import org.natbraille.brailletable.BrailleTables;
import org.natbraille.brailletable.system.SystemBrailleTables;

/**
 *
 * @author vivien
 */
public class BrailleFontDefinition {

    /**
     * True for 8 dots font, false for 6 dots font
     */
    private boolean eightDots = false;
    /**
     * Add small circle showing empty dot location
     */
    private boolean showEmpty = false;
    /**
     * The {@link org.natbraille.brailletable.BrailleTable} for use with this
     * font
     */
    private BrailleTable brailleTable;
    /**
     * The radius of a point
     */
    private Integer radius = 100;
    /**
     * the horizontal distance between two dots of a Braille
     */
    private Integer xInterPoint = 300;
    /**
     * the vertical distance between two dots of a Braille
     */
    private Integer yInterPoint = 300;
    /**
     * xInterCell the horizontal distance between two Braille cells
     */
    private Integer xInterCell = 700;
    /**
     * the vertical distance between two Braille cells
     */
    private Integer yInterCell = 1024;

    /**
     * the font ascent
     */
    private Integer ascent;

    /**
     * the font descent
     */
    private Integer descent;

    /**
     * the font family
     */
    public String fontFamily = "Braille";

    /**
     * font description
     */
    private String comment;

    /**
     * font name
     */
    private String name; 

    public void setName(String name) {
        this.name = name;
    }
    public String getName(){
        if (name != null){
            return name;
        } else {
            return getId();
        }
    }
    /**
     * Get the font ascent.
     * <p>
     * get the font ascent set by {@link setAscent#forceAscent}, or compute the value if not
     * set
     * <p>
     * @return the set or computed value.
     */
    public Integer getAscent() {
        Integer rAscent;
        if (ascent == null) {
//            if (isEightDots()) {
//                rAscent = (radius * 2) + (yInterPoint * 3);
//            } else {
                rAscent = (radius * 2) + (yInterPoint * 2);
  //          }
        } else {
            rAscent = ascent;
        }
        return rAscent;
    }

    public Integer getLeading(){
        return yInterCell;
    }
    /**
     * Get the font descent.
     * <p>
     * get the font descent set by {@link setDescent#forceDescent}, or compute the value
     * (from
     * the font ascent) if not set.
     * <p>
     * @return the set or computed value.
     */
    public Integer getDescent() {
        Integer rDescent;
        if (descent == null) {
            rDescent = -1 * (yInterPoint);
        } else {
            rDescent = descent;
        }
        return rDescent;
    }

    /**
     * Get the font descent.
     * <p>
     * get the font comment set by {@link setComment}, or generate a comment if
     * not set.
     * <p>
     * @return the set or computed value.
     */
    public String getComment() {

        if (comment == null) {
            String btName = "unknown";
            try {
                btName = getBrailleTable().getName();
            } catch (Exception e) {
            }
            return "built for '" + btName + "' braille table on "+(new Date()).toString();
        } else {
            return comment;
        }
    }

    public String getHorizAdvX() {
        return getxInterCell().toString();

    }

    public String getHorizAdvY() {
        return "";
    }

    public Integer getUnitPerEm() {
        return 1024;
    }

    public String getFontStretch() {
        return "normal";
    }

    /**
     * Get the font id
     * <p>
     * get the font id set by {@link setId}, or generate a value
     * if not set.
     * <p>
     * @return the set or computed value.
     */
    public String getId() {
        StringBuilder sb = new StringBuilder();
        sb.append("Braille-");
        try {
            sb.append(getBrailleTable().getName());
        } catch (BrailleTableException e) {
        }

        if (!showEmpty) {
            sb.append("-thermo");
        }
        return sb.toString();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        try {
            sb.append(getBrailleTable().getName());
        } catch (BrailleTableException e) {
        }
        sb
                .append(" r: ").append(radius)
                .append(" x: ").append(xInterPoint).append(" ").append(xInterCell)
                .append(" y: ").append(yInterPoint).append(" ").append(yInterCell);
        return sb.toString();
    }

    public boolean isEightDots() {
        return eightDots;
    }

    public boolean isShowEmpty() {
        return showEmpty;
    }

    /**
     * get the @{link org.natbraille.brailletable.BrailleTable}
     * <p>
     * @return
     * @throws BrailleTableException
     */
    public BrailleTable getBrailleTable() throws BrailleTableException {
        if (brailleTable == null) {
            this.setBrailleTable(BrailleTables.forName(SystemBrailleTables.BrailleUTF8));
        }
        return brailleTable;
    }

    public Integer getRadius() {
        return radius;
    }

    public Integer getxInterPoint() {
        return xInterPoint;
    }

    public Integer getyInterPoint() {
        return yInterPoint;
    }

    public Integer getxInterCell() {
        return xInterCell;
    }

    public Integer getyInterCell() {
        return yInterCell;
    }

    public String getFontFamily() {
        return fontFamily;
    }

    public void setEightDots(boolean eightDots) {
        this.eightDots = eightDots;
    }

    public void setShowEmpty(boolean showEmpty) {
        this.showEmpty = showEmpty;
    }

    public void setBrailleTable(BrailleTable brailleTable) {
        this.brailleTable = brailleTable;
    }

    public void setRadius(Integer radius) {
        this.radius = radius;
    }

    public void setxInterPoint(Integer xInterPoint) {
        this.xInterPoint = xInterPoint;
    }

    public void setyInterPoint(Integer yInterPoint) {
        this.yInterPoint = yInterPoint;
    }

    public void setxInterCell(Integer xInterCell) {
        this.xInterCell = xInterCell;
    }

    public void setyInterCell(Integer yInterCell) {
        this.yInterCell = yInterCell;
    }

    public void forceAscent(Integer ascent) {
        this.ascent = ascent;
    }

    public void forceDescent(Integer descent) {
        this.descent = descent;
    }

    public void setFontFamily(String fontFamily) {
        this.fontFamily = fontFamily;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

}
