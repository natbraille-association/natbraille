/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.natbraille.font.ui;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.FontMetrics;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import static javax.swing.JFileChooser.APPROVE_OPTION;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;
import org.natbraille.brailletable.BrailleTable;
import org.natbraille.brailletable.BrailleTableException;
import org.natbraille.brailletable.BrailleTables;
import org.natbraille.brailletable.Converter;
import org.natbraille.brailletable.UnicodeBraille;
import org.natbraille.brailletable.system.SystemBrailleTables;
import org.natbraille.font.BrailleFontDefinition;
import org.natbraille.font.Data.DotStandards;
import org.natbraille.font.Data.DotStandards.DotStandard;
import org.natbraille.font.svg.Bsvgtottf;
import org.natbraille.font.svg.SvgBrailleFont;

/**
 *
 * @author vivien
 */
public class BrailleFontGui extends javax.swing.JFrame {

    public static String getSampleText() {
        StringBuilder sb = new StringBuilder();
        /*sb.append("⠿⠿⠿⠿⠿⠿⠿\n");
         sb.append(" ⠿ ⠿ ⠿\n");
         sb.append("  ⠿⠿⠿\n");
         sb.append("   ⠿\n");
         */
        int count = 0;
        for (char c : UnicodeBraille.allSixDots()) {
            sb.appendCodePoint(c);
            if ((count % 10 == 0) && (count != 0)) {
                sb.append("\n");
            }
            count++;
        }
        return sb.toString();

    }

    public static class BrailleTableCellRenderer extends JLabel implements ListCellRenderer {

        private static final Color HIGHLIGHT_COLOR = new Color(0, 0, 128);

        public BrailleTableCellRenderer() {
            setOpaque(true);
//                setIconTextGap(12);
        }

        @Override
        public Component getListCellRendererComponent(JList list, Object value,
                int index, boolean isSelected, boolean cellHasFocus) {
            BrailleTable entry = (BrailleTable) value;
            setText(entry.getName());
//                setIcon(entry.getImage());
            if (isSelected) {
                setBackground(HIGHLIGHT_COLOR);
                setForeground(Color.white);
            } else {
                setBackground(Color.white);
                setForeground(Color.black);
            }
            return this;
        }
    }

    public static class DotStandardCellRenderer extends JLabel implements ListCellRenderer {

        private static final Color HIGHLIGHT_COLOR = new Color(0, 0, 128);

        public DotStandardCellRenderer() {
            setOpaque(true);
//                setIconTextGap(12);
        }

        @Override
        public Component getListCellRendererComponent(JList list, Object value,
                int index, boolean isSelected, boolean cellHasFocus) {
            DotStandard entry = (DotStandard) value;
            setText(entry.name);
//                setIcon(entry.getImage());
            if (isSelected) {
                setBackground(HIGHLIGHT_COLOR);
                setForeground(Color.white);
            } else {
                setBackground(Color.white);
                setForeground(Color.black);
            }
            return this;
        }
    }

    /**
     * Creates new form BrailleFontGui
     */
    public BrailleFontGui() {

        initComponents();
        jComboBoxBrailleTables.removeAllItems();
        for (String brailleTableName : SystemBrailleTables.getNames()) {
            try {
                BrailleTable brailleTable = BrailleTables.forName(brailleTableName);

                jComboBoxBrailleTables.addItem(brailleTable);
                jComboBoxBrailleTables.setRenderer(new BrailleTableCellRenderer());
            } catch (BrailleTableException ex) {
                Logger.getLogger(BrailleFontGui.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        jComboBoxDotStandards.removeAllItems();
        for (DotStandard dotStandard : DotStandards.knownStandards) {
            try {
                // will throw and exception is not conform
                dotStandard.getBrailleFontDefinition();
                jComboBoxDotStandards.addItem(dotStandard);
                jComboBoxDotStandards.setRenderer(new DotStandardCellRenderer());
            } catch (Exception ex) {
                //       Logger.getLogger(BrailleFontGui.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jComboBoxDotStandards = new javax.swing.JComboBox();
        jTextAreaDemo = new javax.swing.JTextArea();
        jRadioButtonStandard = new javax.swing.JRadioButton();
        jRadioButtonCustom = new javax.swing.JRadioButton();
        jTextFieldRadius = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jTextFieldxInterPoint = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jTextFieldyInterPoint = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jTextFieldxInterCell = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jTextFieldyInterCell = new javax.swing.JTextField();
        jCheckBoxShowEmptyDots = new javax.swing.JCheckBox();
        jTextPaneDemo = new javax.swing.JTextPane();
        jComboBoxBrailleTables = new javax.swing.JComboBox();
        jButtonSaveAs = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jComboBoxDotStandards.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jComboBoxDotStandards.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxDotStandardsActionPerformed(evt);
            }
        });

        jTextAreaDemo.setColumns(20);
        jTextAreaDemo.setRows(5);

        buttonGroup1.add(jRadioButtonStandard);
        jRadioButtonStandard.setSelected(true);
        jRadioButtonStandard.setText("Standards");
        jRadioButtonStandard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonStandardActionPerformed(evt);
            }
        });

        buttonGroup1.add(jRadioButtonCustom);
        jRadioButtonCustom.setText("Custom");

        jTextFieldRadius.setText("jTextField1");
        jTextFieldRadius.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldRadiusActionPerformed(evt);
            }
        });

        jLabel1.setText("radius");

        jLabel2.setText("xinterpoint");

        jTextFieldxInterPoint.setText("jTextField2");
        jTextFieldxInterPoint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldxInterPointActionPerformed(evt);
            }
        });

        jLabel3.setText("yinterpoint");

        jTextFieldyInterPoint.setText("jTextField3");
        jTextFieldyInterPoint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldyInterPointActionPerformed(evt);
            }
        });

        jLabel4.setText("xintercell");

        jTextFieldxInterCell.setText("jTextField4");
        jTextFieldxInterCell.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldxInterCellActionPerformed(evt);
            }
        });

        jLabel5.setText("yintercell");

        jTextFieldyInterCell.setText("jTextField5");
        jTextFieldyInterCell.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldyInterCellActionPerformed(evt);
            }
        });

        jCheckBoxShowEmptyDots.setText("show empty dots");
        jCheckBoxShowEmptyDots.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxShowEmptyDotsActionPerformed(evt);
            }
        });

        jComboBoxBrailleTables.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jComboBoxBrailleTables.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxBrailleTablesActionPerformed(evt);
            }
        });

        jButtonSaveAs.setText("save");
        jButtonSaveAs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSaveAsActionPerformed(evt);
            }
        });

        jLabel6.setText("Braille table");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButtonSaveAs, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jCheckBoxShowEmptyDots, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jComboBoxDotStandards, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel1))
                        .addGap(20, 20, 20)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextFieldxInterPoint, javax.swing.GroupLayout.DEFAULT_SIZE, 168, Short.MAX_VALUE)
                            .addComponent(jTextFieldRadius)))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5))
                        .addGap(19, 19, 19)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextFieldxInterCell)
                            .addComponent(jTextFieldyInterPoint, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)
                            .addComponent(jTextFieldyInterCell)))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jRadioButtonCustom)
                            .addComponent(jRadioButtonStandard))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jComboBoxBrailleTables, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextAreaDemo, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextPaneDemo, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jRadioButtonStandard)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jComboBoxDotStandards, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jRadioButtonCustom)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel1)
                                    .addComponent(jTextFieldRadius, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel2)
                                    .addComponent(jTextFieldxInterPoint, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel3)
                                    .addComponent(jTextFieldyInterPoint, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel4)
                                    .addComponent(jTextFieldxInterCell, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel5)
                                    .addComponent(jTextFieldyInterCell, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jCheckBoxShowEmptyDots)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel6)
                                    .addComponent(jComboBoxBrailleTables, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addComponent(jButtonSaveAs))
                            .addComponent(jTextAreaDemo, javax.swing.GroupLayout.PREFERRED_SIZE, 295, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jTextPaneDemo))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private void dotStandardToFields() {
        DotStandard dotStandard = (DotStandard) jComboBoxDotStandards.getSelectedItem();
        try {
            if (dotStandard != null) {
                jTextFieldRadius.setText((Float.toString(Float.parseFloat(dotStandard.diameter) / 2f)));
                jTextFieldxInterPoint.setText(dotStandard.xInterPoint);
                jTextFieldyInterPoint.setText(dotStandard.yInterPoint);
                jTextFieldxInterCell.setText(dotStandard.xInterCell);
                jTextFieldyInterCell.setText(dotStandard.yInterCell);
            }
            rebuildAll();
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }
    private void jComboBoxDotStandardsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxDotStandardsActionPerformed
        dotStandardToFields();
        jRadioButtonStandard.setSelected(true);
    }//GEN-LAST:event_jComboBoxDotStandardsActionPerformed
    private void rebuildAll() {
        rebuildAll(new File("super.ttf"));
    }

    private int rescale100(String stringValue){
        Float v = Float.parseFloat(stringValue);
        Float v100 = 100f * v;
        return v100.intValue();
    }
    private void rebuildAll(File ttfFile) {
        try {
            BrailleFontDefinition bfd = new BrailleFontDefinition();
            bfd.setBrailleTable((BrailleTable) jComboBoxBrailleTables.getSelectedItem());
            bfd.setShowEmpty(jCheckBoxShowEmptyDots.isSelected());
            bfd.setRadius(rescale100(jTextFieldRadius.getText()));
            bfd.setxInterPoint(rescale100(jTextFieldxInterPoint.getText()));
            bfd.setyInterPoint(rescale100(jTextFieldyInterPoint.getText()));
            bfd.setxInterCell(rescale100(jTextFieldxInterCell.getText()));
            bfd.setyInterCell(rescale100(jTextFieldyInterCell.getText()));
            bfd.setName(bfd.getBrailleTable().getName());
            SvgBrailleFont svgBrailleFont = new SvgBrailleFont(bfd);

            Bsvgtottf.convert(svgBrailleFont, ttfFile, "natbraille", "http://natbraille.free.fr", 1);
            Font font = Font.createFont(Font.TRUETYPE_FONT, ttfFile);
            
            Converter conv = new Converter(BrailleTables.forName(SystemBrailleTables.BrailleUTF8), bfd.getBrailleTable());
            String brlString = conv.convert(getSampleText());

            Font displayFont = font.deriveFont(20f);

            jTextAreaDemo.setFont(displayFont);
            jTextAreaDemo.setText(brlString);

            //////////////////   
            jTextPaneDemo.setFont(displayFont);
            
            String styleId = Double.toString(Math.random());           
            StyleContext sc = new StyleContext();
            final DefaultStyledDocument doque = new DefaultStyledDocument(sc);
            // Create and add the style
            final Style brailleParagraphStyle = sc.addStyle(styleId, null);
            if (Math.random()>0.5){
            brailleParagraphStyle.addAttribute(StyleConstants.Foreground, Color.red);
            } else {
                brailleParagraphStyle.addAttribute(StyleConstants.Foreground, Color.blue);
            }
             //brailleParagraphStyle.addAttribute(StyleConstants.
            brailleParagraphStyle.addAttribute(StyleConstants.FontSize, 16);
            brailleParagraphStyle.addAttribute(StyleConstants.FontFamily, displayFont.getFamily());
            FontMetrics fontMetrics = new Canvas().getFontMetrics(font);
            brailleParagraphStyle.addAttribute(StyleConstants.LineSpacing, (float) fontMetrics.getLeading());
            doque.setParagraphAttributes(0, 1, brailleParagraphStyle, false);
            //doque.insertString(0, "brlString"+font.getFamily(), brailleParagraphStyle);
            doque.insertString(0, brlString, brailleParagraphStyle);
            jTextPaneDemo.setStyledDocument(doque);
            //jTextPaneDemo.setDocument(doque);
            jTextPaneDemo.setFont(font);
        } catch (BrailleTableException ex) {
            Logger.getLogger(BrailleFontGui.class.getName()).log(Level.SEVERE, null, ex);

        } catch (Exception ex) {
            Logger.getLogger(BrailleFontGui.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    private void jButtonSaveAsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSaveAsActionPerformed
        // TODO add your handling code here:
        rebuildAll();
        JFileChooser dialog = new JFileChooser();
        if (dialog.showSaveDialog(this) == APPROVE_OPTION) {
            File savetofile = dialog.getSelectedFile();
            rebuildAll(savetofile);
        }


    }//GEN-LAST:event_jButtonSaveAsActionPerformed

    private void jComboBoxBrailleTablesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxBrailleTablesActionPerformed
        rebuildAll();
    }//GEN-LAST:event_jComboBoxBrailleTablesActionPerformed

    private void jTextFieldRadiusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldRadiusActionPerformed
        jRadioButtonCustom.setSelected(true);
        rebuildAll();
    }//GEN-LAST:event_jTextFieldRadiusActionPerformed

    private void jTextFieldxInterPointActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldxInterPointActionPerformed
        jRadioButtonCustom.setSelected(true);
        rebuildAll();
    }//GEN-LAST:event_jTextFieldxInterPointActionPerformed

    private void jTextFieldyInterPointActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldyInterPointActionPerformed
        jRadioButtonCustom.setSelected(true);
        rebuildAll();
    }//GEN-LAST:event_jTextFieldyInterPointActionPerformed

    private void jTextFieldxInterCellActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldxInterCellActionPerformed
        jRadioButtonCustom.setSelected(true);
        rebuildAll();
    }//GEN-LAST:event_jTextFieldxInterCellActionPerformed

    private void jTextFieldyInterCellActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldyInterCellActionPerformed
        jRadioButtonCustom.setSelected(true);
        rebuildAll();
    }//GEN-LAST:event_jTextFieldyInterCellActionPerformed

    private void jCheckBoxShowEmptyDotsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxShowEmptyDotsActionPerformed
        rebuildAll();
    }//GEN-LAST:event_jCheckBoxShowEmptyDotsActionPerformed

    private void jRadioButtonStandardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonStandardActionPerformed
        dotStandardToFields();
    }//GEN-LAST:event_jRadioButtonStandardActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {


        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new BrailleFontGui().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton jButtonSaveAs;
    private javax.swing.JCheckBox jCheckBoxShowEmptyDots;
    private javax.swing.JComboBox jComboBoxBrailleTables;
    private javax.swing.JComboBox jComboBoxDotStandards;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JRadioButton jRadioButtonCustom;
    private javax.swing.JRadioButton jRadioButtonStandard;
    private javax.swing.JTextArea jTextAreaDemo;
    private javax.swing.JTextField jTextFieldRadius;
    private javax.swing.JTextField jTextFieldxInterCell;
    private javax.swing.JTextField jTextFieldxInterPoint;
    private javax.swing.JTextField jTextFieldyInterCell;
    private javax.swing.JTextField jTextFieldyInterPoint;
    private javax.swing.JTextPane jTextPaneDemo;
    // End of variables declaration//GEN-END:variables
}
