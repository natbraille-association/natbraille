package org.natbraille.font.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.PosixParser;
import org.apache.commons.cli.UnrecognizedOptionException;
import org.natbraille.brailletable.system.SystemBrailleTables;
import org.natbraille.font.BrailleFontDefinition;
import org.natbraille.font.Data.DotStandards;
import org.natbraille.font.svg.SvgBrailleFont;
import org.xnap.commons.i18n.I18n;
import org.xnap.commons.i18n.I18nFactory;

/*
 * @author vivien
 */
public class BrailleFontCli {

    private static final Logger l = Logger.getLogger(BrailleFontCli.class.getName());
    private static final I18n i18n = I18nFactory.getI18n(BrailleFontCli.class);

    private static final String O_SHOW_EMPTY_DOT = "e";
    private static final String O_BRAILLE_TABLE = "t";
    private static final String O_INTER_DOT_W = "w";
    private static final String O_INTER_DOT_H = "h";
    private static final String O_INTER_CELL_W = "W";
    private static final String O_INTER_CELL_H = "H";
    private static final String O_DOT_RADIUS = "r";
    private static final String O_BRAILLE_NORM = "n";
    private static final String O_LIST = "l";
    private static final String O_HELP = "h";

    private static final Options options = new Options()
            .addOption(O_BRAILLE_NORM, "norm", true,
                    i18n.tr("set the Braille cell dimensions norm"))
            .addOption(O_BRAILLE_TABLE, "page-height", true,
                    i18n.tr("set the Braille table"))
            .addOption(O_DOT_RADIUS, "dot-radius", true,
                    i18n.tr("set the dot radius  (overrides --norm)"))
            .addOption(O_INTER_CELL_H, "inter-cell-height", true,
                    i18n.tr("set the horizontal distance between two Braille cells  (overrides --norm)"))
            .addOption(O_INTER_CELL_W, "inter-cell-width", true,
                    i18n.tr("set the vertical distance between two Braille cells  (overrides --norm)"))
            .addOption(O_INTER_DOT_H, "inter-dot-height", true,
                    i18n.tr("set the vertical distance between two Braille dots in a cell(overrides --norm)"))
            .addOption(O_INTER_DOT_W, "inter-dot-width", true,
                    i18n.tr("set the horizontal distance between two Braille dots in a cell(overrides --norm)"))
            .addOption(O_LIST, "list", false,
                    i18n.tr("list braille tables and cell dimensions norms"))
            .addOption(O_SHOW_EMPTY_DOT, "show-empty", false,
                    i18n.tr("display help"))
            .addOption(O_HELP, "help", false,
                    i18n.tr("display help"));

    public static void print(String s) {
        System.out.println(s);
    }

    public static void printerr(String s) {
        System.err.println(s);
    }

    static String convertFileToString(File file, String charset) throws FileNotFoundException {
        InputStream is = new FileInputStream(file);
        java.util.Scanner s = new java.util.Scanner(is, charset).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    public static void main(String[] args) throws Exception {
        //args = new String[]{"--norm", "American Natio","Ma super font !"};
        l.setLevel(Level.CONFIG);
        TheNatbrailleBannerDrawIt();
        try {

            CommandLine cl = new PosixParser().parse(options, args);
            if (cl.hasOption(O_HELP)) {
                HelpFormatter formatter = new HelpFormatter();
                formatter.printHelp(BrailleFontCli.class.getCanonicalName() + " [options] output_filename", options);
            } else if (cl.hasOption(O_LIST)) {
                // output braille tables names
                System.out.println("Braille tables :");
                for (String brailleTableName : SystemBrailleTables.getNames()) {
                    System.out.println("- " + brailleTableName);
                }
                // output norms name
                System.out.println("Braille cell norms :");
                for (DotStandards.DotStandard dotStandard : DotStandards.knownStandards) {
                    System.out.println("- " + dotStandard);
                }
            } else {

                BrailleFontDefinition brailleFontDefinition = new BrailleFontDefinition();

                // there is a norm
                if (cl.hasOption(O_BRAILLE_NORM)) {
                    List<DotStandards.DotStandard> matchingNorms = DotStandards.forName(cl.getOptionValue(O_BRAILLE_NORM));
                    
                    // one found, build font definition
                    if (matchingNorms.size() == 1) {
                        brailleFontDefinition = matchingNorms.get(0).getBrailleFontDefinition();
                    } else {
                        if (matchingNorms.size() > 1) {
                            System.out.println("multiple Braille cell norms : be more precise");
                            for (DotStandards.DotStandard dotStandard : matchingNorms) {
                                System.out.println("- " + dotStandard.name);
                            }
                        } else if (matchingNorms.isEmpty()) {
                            System.out.println("no matching Braille cell norm. Try -" + O_LIST + " option.");

                        }
                        System.exit(1);
                    }

                }

                if (cl.hasOption(O_SHOW_EMPTY_DOT)) {
                    brailleFontDefinition.setShowEmpty(true);
                }                
                if (cl.hasOption(O_DOT_RADIUS)) {
                    brailleFontDefinition.setRadius(Integer.parseInt(cl.getOptionValue(O_DOT_RADIUS)));
                }
                if (cl.hasOption(O_INTER_CELL_H)) {
                    brailleFontDefinition.setyInterCell(Integer.parseInt(cl.getOptionValue(O_INTER_CELL_H)));
                }
                if (cl.hasOption(O_INTER_CELL_W)) {
                    brailleFontDefinition.setxInterCell(Integer.parseInt(cl.getOptionValue(O_INTER_CELL_W)));
                }
                if (cl.hasOption(O_INTER_DOT_H)) {
                    brailleFontDefinition.setyInterPoint(Integer.parseInt(cl.getOptionValue(O_INTER_DOT_H)));
                }
                if (cl.hasOption(O_INTER_DOT_W)) {
                    brailleFontDefinition.setxInterPoint(Integer.parseInt(cl.getOptionValue(O_INTER_DOT_W)));

                }

                if (cl.getArgs().length > 0) {
                    SvgBrailleFont svgBrailleFont = new SvgBrailleFont(brailleFontDefinition);
                    svgBrailleFont.write(new FileOutputStream(new File(cl.getArgs()[0])));
                    System.err.println("Wrote font "+cl.getArgs()[0]);
                } else {
                    HelpFormatter formatter = new HelpFormatter();
                    formatter.printHelp(BrailleFontCli.class.getCanonicalName() + " [options] output_filename", options);
                    System.err.println("Please provide a filename for generated font.");
                }

            }
        } catch (UnrecognizedOptionException e) {
            l.log(Level.SEVERE, e.getMessage());
        }

    }

    public static void TheNatbrailleBannerDrawIt() {

        System.err.println("natbraille-font: generate braille fonts");
    }
}
