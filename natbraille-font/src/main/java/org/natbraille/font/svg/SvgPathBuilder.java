/*
 * NatBraille - An universal Translator - Braille font generator
 * Copyright (C) 2014 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.font.svg;

/**
 *
 * @author vivien
 */
public class SvgPathBuilder {

    StringBuilder sb = new StringBuilder();

    public String toPath() {
        return sb.toString();
    }

    /**
     * http://stackoverflow.com/questions/5737975/circle-drawing-with-svgs-arc-path
     * <p>
     * d="
     * M cx cy
     * m -r, 0
     * a r,r 0 1,1 (r * 2),0
     * a r,r 0 1,1 -(r * 2),0
     * "
     * <p>
     * @param cx
     * @param cy
     * @param r
     */
    public void addDisc(Integer cx, Integer cy, Integer r) {

        String roundPath = String.format("M %d %d m %d, 0 a %d,%d 0 1,1 %d,0 a %d,%d 0 1,1 %d,0 z ",
                cx,
                cy,
                -r,
                r, r, r * 2,
                r, r, -(r * 2)
        );
        sb.append(roundPath);
    }

}
