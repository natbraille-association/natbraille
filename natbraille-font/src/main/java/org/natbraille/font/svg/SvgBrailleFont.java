/*
 * NatBraille - An universal Translator - Braille font generator
 * Copyright (C) 2014 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.font.svg;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import org.natbraille.brailletable.BrailleTableException;
import org.natbraille.brailletable.UnicodeBraille;
import org.natbraille.font.BrailleFontDefinition;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author vivien
 */
public class SvgBrailleFont {

    private final BrailleFontDefinition brailleFontDefinition;

    public SvgBrailleFont(BrailleFontDefinition brailleFontDefinition) {
        this.brailleFontDefinition = brailleFontDefinition;
    }

    public BrailleFontDefinition getBrailleFontDefinition() {
        return brailleFontDefinition;
    }

    /*


     <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd" >
     <!--
     2014-5-5: Created.
     -->
     <svg>
     <metadata>
     Created by FontForge 20110222 at Mon May  5 13:56:54 2014
     By vivien,,,
     Created by vivien,,, with FontForge 2.0 (http://fontforge.sf.net)
     </metadata>
     <defs>
     <font id="Untitled1" horiz-adv-x="1000" >
     <font-face 
     font-family="Untitled1"
     font-weight="500"
     font-stretch="normal"
     units-per-em="1000"
     panose-1="2 0 6 9 0 0 0 0 0 0"
     ascent="800"
     descent="-200"
     cap-height="596.5"
     bbox="69.5 288.5 333.5 596.5"
     underline-thickness="50"
     underline-position="-100"
     unicode-range="U+0041-0041"
     />
     <missing-glyph />
     <glyph glyph-name="A" unicode="A" 
     d="M69.5 596.5h264v-308h-264v308z" />
     </font>
     </defs></svg>


     */
    private class DotPosition {

        private final Integer x;
        private final Integer y;

        public DotPosition(Integer x, Integer y) {
            this.x = x;
            this.y = y;
        }

    }

    private final DotPosition dotPosition[] = new DotPosition[]{
        new DotPosition(0, 0),
        new DotPosition(0, 1),
        new DotPosition(0, 2),
        new DotPosition(1, 0),
        new DotPosition(1, 1),
        new DotPosition(1, 2),
        new DotPosition(0, 3),
        new DotPosition(1, 3),};

    public SvgGlyph getGlyphForUnicode(char unicodeBraille) throws BrailleTableException {
        char charNumber = brailleFontDefinition.getBrailleTable().getChar(unicodeBraille);
        Boolean points[] = UnicodeBraille.toBooleanPoints(unicodeBraille);
        SvgPathBuilder spb = new SvgPathBuilder();
        int maxPoints = (brailleFontDefinition.isEightDots()) ? (8) : (6);
        for (int i = 0; i < maxPoints; i++) {
            int cx = brailleFontDefinition.getRadius() + dotPosition[i].x * brailleFontDefinition.getxInterPoint();
            int cy;
           // if (brailleFontDefinition.isEightDots()) {
             //   cy = brailleFontDefinition.getRadius() + (3 - dotPosition[i].y) * brailleFontDefinition.getyInterPoint();
            //} else {
                cy = brailleFontDefinition.getRadius() + (2 - dotPosition[i].y) * brailleFontDefinition.getyInterPoint();
            //}
            //cy -= brailleFontDefinition.getyInterCell();//-2*brailleFontDefinition.getyInterPoint());
           /* int yoff = brailleFontDefinition.getyInterCell()
                    - (brailleFontDefinition.isEightDots()?3:2)*brailleFontDefinition.getyInterPoint()
                    - 2*brailleFontDefinition.getRadius();
                    
            cy = cy + yoff;
                   */
      //      System.err.println(brailleFontDefinition.toString());
            
            if (points[i]) {
                spb.addDisc(cx, cy, brailleFontDefinition.getRadius());
            } else {
                if (brailleFontDefinition.isShowEmpty()) {
                    spb.addDisc(cx, cy, brailleFontDefinition.getRadius() / 4);
                }
            }
        }
        SvgGlyph glyph = new SvgGlyph(charNumber, brailleFontDefinition.getxInterCell().toString(), spb.toPath());
        return glyph;
    }

    private List<SvgGlyph> getGlyphs() throws BrailleTableException {
        List<SvgGlyph> resu = new ArrayList<>();

        for (Character unicodeBraille : UnicodeBraille.allSixDots()) {
            SvgGlyph glyph = getGlyphForUnicode(unicodeBraille);
            resu.add(glyph);

        }
        return resu;
    }

    private Document getDocument() throws ParserConfigurationException, TransformerConfigurationException, TransformerException, BrailleTableException {

        Document document = XmlUtils.newSvgDocument();
        Element racine = (Element) document.getFirstChild();

        // A. metadata
        Element metadata = document.createElement("metadata");
        metadata.appendChild(document.createTextNode(brailleFontDefinition.getComment()));
        racine.appendChild(metadata);

        // B. defs 
        Element defs = document.createElement("defs");

        // defs.font
        Element font = document.createElement("font");
        font.setAttribute("id", brailleFontDefinition.getId());
        font.setAttribute("horiz-adv-x", brailleFontDefinition.getHorizAdvX());
        font.setAttribute("vert-adv-y", brailleFontDefinition.getHorizAdvY());

        // defs.font-face
        Element font_face = document.createElement("font-face");
        font_face.setAttribute("font-weight", "normal");
        font_face.setAttribute("font-family", brailleFontDefinition.getFontFamily());
        font_face.setAttribute("units-per-em", brailleFontDefinition.getUnitPerEm().toString());
        font_face.setAttribute("ascent", brailleFontDefinition.getAscent().toString());
        font_face.setAttribute("descent", brailleFontDefinition.getDescent().toString());
        font_face.setAttribute("font-stretch", brailleFontDefinition.getFontStretch());
        //font_face.setAttribute("panose-1", bFont.panose_1);
        // font_face.setAttribute("x-height", bFont.x_height);
        //font_face.setAttribute("cap-height", bFont.cap_height);
        //  font_face.setAttribute("bbox", bFont.bbox);
        //    font_face.setAttribute("underline-thickness", bFont.underline_thickness);
        //      font_face.setAttribute("underline-position", bFont.underline_position);
//        font_face.setAttribute("unicode-range", bFont.unicode_range);
        font.appendChild(font_face);
        //
        // missing-glyph 
        Element missing_glyph = document.createElement("missing-glyph");
        font.appendChild(missing_glyph);

        // glyph s
        List<SvgGlyph> glyphs = getGlyphs();

        for (SvgGlyph gl : glyphs) {
            Element glyph = document.createElement("glyph");
            //glyph.setAttribute("name", gl.glyph_name);
            glyph.setAttribute("unicode", gl.unicode.toString());
            glyph.setAttribute("horiz-adv-x", gl.horiz_adv_x);
            if (!gl.d.isEmpty()) {
                glyph.setAttribute("d", gl.d);

            }
            font.appendChild(glyph);
        }

        defs.appendChild(font);
        racine.appendChild(defs);
        return document;
    }

    public void write(OutputStream fos) throws ParserConfigurationException, TransformerException, TransformerConfigurationException, BrailleTableException {
        Document fontDoc = getDocument();
        XmlUtils.documentToStream(fontDoc, fos);

    }

}
