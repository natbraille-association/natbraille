/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.natbraille.font.svg;

import doubletype.FontFileWriter;
import doubletype.FontFormatWriter;
import doubletype.TTGlyph;
import doubletype.TTUnicodeRange;
import java.awt.Point;
import java.io.File;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import name.suchanek.svg2ttf.Arc;
import org.natbraille.brailletable.BrailleTableException;
import org.natbraille.brailletable.UnicodeBraille;
import org.natbraille.font.BrailleFontDefinition;

/**
 *
 * @author vivien
 */
public class Bsvgtottf {

    static class BrailleGlyphCreator {

        public static Pattern pathPattern = Pattern.compile("([a-zA-Z])\\s*([-0-9\\. ,]++)");
        public static Pattern numberPattern = Pattern.compile("[-0-9\\.]+");

        /**
         * Returns all numbers in a string
         */
        public static List<Double> getNumbers(String string) {
            List<Double> result = new ArrayList<>();
            Matcher m = numberPattern.matcher(string);
            while (m.find()) {
                result.add(Double.parseDouble(m.group()));
            }
            return (result);
        }

        /**
         * Produces a glyph for an SVG character path
         * M 100 1300 m -25, 0 a 25,25 0 1,1 50,0 a 25,25 0 1,1 -50,0 z M 100 700 m -25, 0 a 25,25 0 1,1 50,0 a 25,25 0 1,1 -50,0 z M 100 100 m -25, 0 a 25,25 0 1,1 50,0 a 25,25 0 1,1 -50,0 z M 700 1300 m -25, 0 a 25,25 0 1,1 50,0 a 25,25 0 1,1 -50,0 z M 700 700 m -25, 0 a 25,25 0 1,1 50,0 a 25,25 0 1,1 -50,0 z M 700 100 m -25, 0 a 25,25 0 1,1 50,0 a 25,25 0 1,1 -50,0 z
         */
        protected static TTGlyph glyph(char c, double width, String svgPath, int scale) {
            TTGlyph glyph = new TTGlyph();
            glyph.setAdvanceWidth(((int) width) * scale);            
            Point point = new Point(0, 0);
            boolean hasContours = false;
            for (String contour : svgPath.split("z")) {
                Matcher m2 = pathPattern.matcher(contour);
                int numPoints = glyph.getNumOfPoints();
                while (m2.find()) {
                    hasContours = true;
                    char command = m2.group(1).charAt(0);
                    List<Double> numbers = getNumbers(m2.group(2));
                    switch (command) {
                        case 'l':
                        case 'L':
                        case 'M':
                        case 'm':
                            if (numbers.size() != 2) {
                                FontFormatWriter.warning("Invalid numbers in glyph", FontFormatWriter.toString(c),
                                        "at command", command, m2.group(2));
                                break;
                            }
                            int x = numbers.get(0).intValue() * scale;
                            int y = numbers.get(1).intValue() * scale;
                            if (Character.isLowerCase(command)) {
                                point.setLocation(x + point.x, y + point.y);
                            } else {
                                point.setLocation(x, y);
                            }
                            // Make a new point because otherwise it's the same
                            glyph.addPoint(new Point(point.x, point.y));
                            glyph.addFlag(1); // on curve
                            break;
                        case 'a':
                        case 'A':
                            if (numbers.size() != 7) {
                                FontFormatWriter.warning("Invalid numbers in glyph", FontFormatWriter.toString(c),
                                        "at command", command, m2.group(2));
                                break;
                            }
                            double x1 = point.x / scale;
                            double y1 = point.y / scale;
                            double rx = numbers.get(0);
                            double ry = numbers.get(1);
                            double rotation = numbers.get(2);
                            double largeflag = numbers.get(3);
                            double sweepflag = numbers.get(4);
                            double x2 = numbers.get(5);
                            double y2 = numbers.get(6);
                            if (Character.isLowerCase(command)) {
                                x2 = x2 + x1;
                                y2 = y2 + y1;
                            }
                            if (rotation != 0) {
                                FontFormatWriter.warning("Unsupported rotation in glyph", FontFormatWriter.toString(c), ":",
                                        m2.group());
                            }
                            if (rx != ry) {
                                FontFormatWriter.warning("Unsupported elliptic curve in glyph", FontFormatWriter.toString(c),
                                        ":", m2.group());
                            }
                            Arc arc = new Arc(x1, y1, x2, y2, rx, ry, sweepflag, largeflag);
                            if (!arc.isOK()) {
                                FontFormatWriter.warning("Pathological arc in glyph", FontFormatWriter.toString(c), ":",
                                        m2.group(), arc);
                            } else {
                                for (double[] bezier : arc.asBezier()) {
                                    glyph.addPoint(new Point(((int) bezier[0]) * scale, ((int) bezier[1])
                                            * scale));
                                    glyph.addFlag((int) bezier[2]);
                                }
                            }
                            // Update final point
                            point.setLocation(x2 * scale, y2 * scale);
                            break;
                        default:
                            FontFormatWriter.warning("Unsupported path command in glyph", c, (int) (c), ":", m2.group());
                            break;
                    }
                }
                if (numPoints < glyph.getNumOfPoints()) {
                    glyph.addEndPoint(glyph.getNumOfPoints() - 1);
                }
            }
            // If they tried to declare something, but nothing came out of it, fail
            if (hasContours && glyph.getNumOfContours() == 0) {
                return (null);
            }
            // Otherwise return the glyph
            return (glyph);
        }


        /**
         * Creates a glyph
         */
        public static TTGlyph makeGlyph(char unicodeChar, SvgBrailleFont svgBrailleFont, int scale) throws BrailleTableException {
            BrailleFontDefinition brailleFontDefinition = svgBrailleFont.getBrailleFontDefinition();
            char mappedChar = brailleFontDefinition.getBrailleTable().getChar(unicodeChar);
            String svgPath = svgBrailleFont.getGlyphForUnicode(unicodeChar).d;            
            if (svgPath == null) {
                FontFormatWriter.warning("No glyph defined for", FontFormatWriter.toString(unicodeChar));
                return (null);
            }
            double width = Double.parseDouble(brailleFontDefinition.getHorizAdvX());
            TTGlyph glyph = glyph(mappedChar, width, svgPath, scale);
            return (glyph);
        }
        
        
        /**
         * Adds a glyph
         */
        protected static boolean addGlyph(char unicodeChar, SvgBrailleFont svgBrailleFont, FontFileWriter writer, int scale) throws BrailleTableException {
            int mappedChar = svgBrailleFont.getBrailleFontDefinition().getBrailleTable().getChar(unicodeChar);

            TTUnicodeRange range = TTUnicodeRange.of(mappedChar);
            if (range != null) {
                writer.addUnicodeRange(range);
            }
            TTGlyph glyph = BrailleGlyphCreator.makeGlyph(unicodeChar, svgBrailleFont, scale);
            if (glyph == null) {
                return (false);
            }
            int index = writer.addGlyph(glyph);
            writer.addCharacterMapping(mappedChar, index);
//            System.err.println("wrote "+unicodeChar+" to ("+mappedChar+")");
            return (true);
        }
    }

    
    /** Returns the undefined glyph */
    public static TTGlyph undef(BrailleFontDefinition brailleFontDefinition) {
        int advx = brailleFontDefinition.getxInterCell();
        int advy = brailleFontDefinition.getAscent();

        int mx = brailleFontDefinition.getxInterPoint()+brailleFontDefinition.getRadius()*2;


        TTGlyph g = new TTGlyph();
        g.setAdvanceWidth(advx);
        g.addPoint(new Point(0, advy));
        g.addFlag(1);
        g.addPoint(new Point(mx,advy));
        g.addFlag(1);
        g.addPoint(new Point(mx,0));
        g.addFlag(1);
        g.addPoint(new Point(0, 0));
        g.addFlag(1);
        g.addEndPoint(3);
        return (g);
    }

    /**
     * Converts an SVG font file to TTF
     * @param svgBrailleFont
     * @param ttfFile
     * @param designer
     * @param scale
     * @param designerUrl
     * @throws java.lang.Exception
     */
    public static void convert(SvgBrailleFont svgBrailleFont, File ttfFile, String designer, String designerUrl,int scale) throws Exception {

        BrailleFontDefinition brailleFontDefinition = svgBrailleFont.getBrailleFontDefinition();
        //File ttfFile = new File("fonts/"+brailleFontDefinition.getId() + ".ttf");

        try (RandomAccessFile out = new RandomAccessFile(ttfFile, "rw")) {
            FontFileWriter writer = new FontFileWriter(out);
            
            
            // writer.setDescent((int) (brailleFontDefinition.getDescent() * scale));
            // writer.setDescent((int) ((brailleFontDefinition.getLeading() - brailleFontDefinition.getDescent() )* scale));
            writer.setAscent((int) (brailleFontDefinition.getAscent() * scale));
            writer.setDescent(-(int) (brailleFontDefinition.getDescent() * scale));
            writer.setLineGap((int) ((brailleFontDefinition.getLeading())));//+brailleFontDefinition.getDescent()-brailleFontDefinition.getAscent())* scale));
            
           // writer.setLineGap(brailleFontDefinition.getLeading() * scale);
            System.err.println("generate ttf "+brailleFontDefinition.getAscent()+" /  "+brailleFontDefinition.getDescent()+" / "+brailleFontDefinition.getLeading());
            //writer.setOffset(0);
           writer.setXHeight((int) (brailleFontDefinition.getAscent() * scale));
            
            /*writer.setCodeRangeFlag(TTCodePage.forName("US-ASCII").getOsTwoFlag());
            writer.setCodeRangeFlag(TTCodePage.forName("windows-1252").getOsTwoFlag());
            writer.setCodeRangeFlag(TTCodePage.forName("windows-1252").getOsTwoFlag());
            */
            for (int i=0;i<123;i++){
                writer.setCodeRangeFlag(i);
            }
            //writer.addUnicodeRange(l);
            writer.setNames(brailleFontDefinition.getName().replaceAll("_", " "), designer, designerUrl);

            // We put the characters here in a specific order to comply
            // with the POST 1 table convention. However, we will finally use
            // POST 3.
            // At position 0, there must be a special character "undefined"
            writer.addCharacterMapping(TTUnicodeRange.k_notDef, writer.addGlyph(undef(brailleFontDefinition)));
            // At position 1, we want the NULL character
         //   writer.addCharacterMapping(TTUnicodeRange.k_null, writer.addGlyph(DefaultGlyphs.nullGlyph()));
            // At position 2, we want the CR character, which we render like
            // space
            /*
            TTGlyph space = BrailleGlyphCreator.makeGlyph(UnicodeBraille.FIRST, svgBrailleFont);
            if (space == null) {
                space = DefaultGlyphs.spaceGlyph();
            }
            */
            //      writer.addCharacterMapping(TTUnicodeRange.k_cr, writer.addGlyph(space));
            // At position 3, we want the SPACE character
            //        writer.addCharacterMapping(TTUnicodeRange.k_space, writer.addGlyph(space));
            // Now put the basic Latin ones
            for (char c = 0x20; c < 0x80; c++) {
                //         BrailleGlyphCreator.addGlyph(c, svgBrailleFont, writer);
            }
            
            // Now put all the other characters in order
            for (char unicodeChar : UnicodeBraille.allSixDots()) {
                // Basic Latin is already there
//                if (c >= 0x20 && c < 0x80) {
                //continue;
                //              }
                BrailleGlyphCreator.addGlyph(unicodeChar, svgBrailleFont, writer,scale);
            }
            /* TODO
             // Add all the kernings
             for (char c1 : brailleFontDefinition.kernings()) {
             for (char c2 : brailleFontDefinition.kerningsOf(c1)) {
             // Watch out, these have to be negative!
             writer.addKern(c1, c2, brailleFontDefinition.kerning(c1, c2).intValue() * -scale);
             }
             }
             */
            writer.write();
        }
     
    }

}
