/*
 * NatBraille - An universal Translator - Braille font generator
 * Copyright (C) 2014 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.font;

import java.io.File;
import java.io.FileOutputStream;
import org.natbraille.braillepdf.LowagiePdfFormatter;
import org.natbraille.braillepdf.PageConfig;
import org.natbraille.braillepdf.PdfBoxPdfFormatter;
import org.natbraille.brailletable.BrailleTable;
import org.natbraille.brailletable.BrailleTables;
import org.natbraille.brailletable.Converter;
import org.natbraille.brailletable.UnicodeBraille;
import org.natbraille.brailletable.system.SystemBrailleTables;
import org.natbraille.font.Data.DotStandards;
import org.natbraille.font.Data.DotStandards.DotStandard;
import org.natbraille.font.svg.Bsvgtottf;
import org.natbraille.font.svg.SvgBrailleFont;

/**
 *
 * @author vivien
 */
public class Test {

    public static String getSampleText() {
        StringBuilder sb = new StringBuilder();
        sb.append("⠿⠿⠿⠿⠿⠿⠿\n");
        sb.append(" ⠿ ⠿ ⠿\n");
        sb.append("  ⠿⠿⠿\n");
        sb.append("   ⠿\n");

        int count = 0;
        for (char c : UnicodeBraille.allSixDots()) {
            sb.appendCodePoint(c);
            if (count % 10 == 0) {
                sb.append("\n");
            }
            count++;
        }
        return sb.toString();

    }

    /**
     * @param args the command line arguments
     * @throws java.lang.Exception
     */
    public static void main(String[] args) throws Exception {
        String testDoc = getSampleText();

        for (String brailleTableName : SystemBrailleTables.getNames()) {
            BrailleTable brailleTable = BrailleTables.forName(brailleTableName);
            System.err.println("========= "+brailleTable.getName());

            for (DotStandard dotStandard : DotStandards.knownStandards) {
                try {
                    

                    BrailleFontDefinition def = dotStandard.getBrailleFontDefinition();
                    
                    
                    def.setShowEmpty(true);
                    String standardId = dotStandard.name.replaceAll(" ", "_");
                    System.err.println(standardId);

                    String genName = brailleTableName+"_"+standardId;
                    File pdfFile = new File("fonts/" + genName + ".pdf");
                    File svgFile = new File("fonts/" + genName + ".svg");
                    File ttfFile = new File("fonts/" + genName + ".ttf");
                    def.setName("LouisLouis "+genName);
                    def.setBrailleTable(brailleTable);
                    
                    SvgBrailleFont svgBrailleFont = new SvgBrailleFont(def);
                    
                    if (new File("fonts/").isDirectory()) {
                        svgBrailleFont.write(new FileOutputStream(svgFile));

                        // convert to ttf
                        Bsvgtottf.convert(svgBrailleFont,ttfFile, "Natbraille", "http://natbraille.free.fr", 1);

                        // new pdf configuration
                        PageConfig pageConfig = new PageConfig();
                        pageConfig.fontFileName = ttfFile.getAbsolutePath();
                    //    pageConfig.fontFileName = "/var/lib/tomcat7/webapps/natbraille-http/natbraille/fonts/LouisLouis.ttf";
                        pageConfig.fontSize = 30.0f;

                        //PdfBoxPdfFormatter pdfFormatter = new PdfBoxPdfFormatter(pageConfig);
                        LowagiePdfFormatter pdfFormatter = new LowagiePdfFormatter(pageConfig);

                        // create pdf
                        Converter conv = new Converter(BrailleTables.forName(SystemBrailleTables.BrailleUTF8), brailleTable);
                        String brlString = conv.convert(testDoc);
                        pdfFormatter.brailleToPdf(brlString, pdfFile.getAbsolutePath());

                    } else {
                        System.err.println("please create fonts/ directory");
                    }

                } catch (java.lang.NumberFormatException e){
               //     System.err.println("standard error");
                } catch (Exception e) {
                    e.printStackTrace();
                    System.exit(1);
                }
            }
        }
    }
}
