# natbraille-font: generate braille fonts

## Usage

    $ java -jar build/libs/natbraille-font-1.0-SNAPSHOT-all.jar 

    usage: org.natbraille.font.ui.BrailleFontCli [options] output_filename
        -e,--show-empty                affiche l'aide
        -H,--inter-cell-height <arg>   fixe la distance horizontale entre deux
                                        cellules Braille (écrase --norm)
        -h,--help                      affiche l'aide
        -l,--list                      liste les tables brailles et les normes de
                                        dimension de cellules
        -n,--norm <arg>                fixe la norme de dimension de la cellule
        -r,--dot-radius <arg>          fixe le rayon des points braille
        -t,--page-height <arg>         fixe la table braille
        -W,--inter-cell-width <arg>    fixe la distance verticale entre deux
                                        cellules Braille  (écrase --norm)
        -w,--inter-dot-width <arg>     fixe la distance horizontale entre deux
                                        points Braille dans une cellule  (écrase
                                        --norm)

## Example

Produces a `SVG` braille font covering unicode braille cells using "American Library of Congress" dots dimensions: 

    java -jar build/libs/natbraille-font-1.0-SNAPSHOT-all.jar \
        -n 'American Library of Congress' \
        -t brailleUTF8 \
        braille-alc-utf8.svg 

## TTF

Produces a `TTF` font from a `SVG`  font using the `fontforge` script:

    apt install fontforge 

    ./svg2ttf/svg2ttf.sh braille-alc-utf8.svg braille-alc-utf8.ttf
