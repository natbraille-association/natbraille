/*
 * NatBraille - An universal Translator - soffice interface 
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret, Frédéric Schwebel
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.soffice.configurators;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.exec.CommandLine;
import org.natbraille.soffice.SofficeConfig;
import org.natbraille.soffice.executor.NatExecutor;
import org.natbraille.soffice.executor.SofficeCliTools;

/**
 * retrieves a {@link SofficeConfig} according to "soffice -help" .
 *
 * @author vivien
 */
public class MacConfigurator {

    /**
     * Mac search directories
     */
    private final static String macSearchDirectories[] = {
        "/Applications/OpenOffice.app/Contents/program",
        //        "/Applications/OpenOffice.app/Contents/MacOS",
        "/Applications/LibreOffice.app/Contents/program",};

    public final Long timeout = 60 * 1000L;
    private final SofficeCliTools sofficeCliTools;

    public MacConfigurator() throws IOException {
        sofficeCliTools = new SofficeCliTools();
        sofficeCliTools.createUserInstallationDirectory();
    }

    /**
     * get office configuration according to the "soffice" in Appplications/ by
     * running soffice -help for openoffice and soffice --version for libreoffice
     * <p>
     * @return the generated configuration
     */
    public List<SofficeConfig> getSofficeConfig() {

        List<SofficeConfig> confs = new ArrayList<>();
        for (String variantPath : macSearchDirectories) {
            // build initial config for mac
            SofficeConfig testConf = new SofficeConfig();
            NatExecutor executor = null;
            try {

                testConf.setExec("./soffice");
                if (variantPath.toLowerCase().contains("openoffice")) {
                    testConf.setBrand("OpenOffice.org");
                } else {
                    testConf.setBrand("LibreOffice.org");
                }
                // build the command line
                CommandLine cmdLine = sofficeCliTools.getGenericCommandLine(testConf);

                if (testConf.isOpenOffice()) {
                    cmdLine.addArgument("-help");
                } else {
                    cmdLine.addArgument("--version");
                }

                executor = new NatExecutor(timeout);

                File workingDirectory = new File(variantPath);//.getParentFile();
                executor.setWorkingDirectory(workingDirectory);
                executor.execute(cmdLine);

                // get  SofficeConfig
                List<String> log = executor.getStdOut();
                if (log.size() > 0) {
                    //OpenOffice 4.1.1  411m6(Build:9775)
                    String versionString = log.get(0);                    
                    String vss[] = versionString.split(" ");
                    // reset brand
                    testConf.setBrand(vss[0]);
                    // reset version
                    testConf.setVersion(vss[1]);                    
                    testConf.setPath(variantPath);
                    confs.add(testConf);
                }
            } catch (IOException e) {
                //safely ignore : if there is an error, this office should not be listed anyway
            }

        }
        sofficeCliTools.cleanUpUserInstallationDirectory();

        return confs;
    }
}
