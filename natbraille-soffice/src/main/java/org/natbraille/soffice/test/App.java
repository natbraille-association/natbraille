/*
 * NatBraille - An universal Translator - soffice interface 
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret, Frédéric Schwebel
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.soffice.test;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import org.apache.commons.io.FileUtils;
import org.natbraille.soffice.AutoConfigurator;
import org.natbraille.soffice.SofficeConfig;

/**
 * test class.
 * <p>
 * java -jar natbraille-soffice.jar [mydocument.doc [n_thread n_repeat]]
 * <p>
 * will run a specified number of czif (that will convert the same document a
 * specified number of times)
 * and output usefull debug messages
 * <p>
 * @author vivien
 */
public class App {

    private static final String defaultDocName = "TEST.DOC";

//    private static void makeWordFile() throws IOException {
//        URL inputUrl = App.class.getResource("a.doc");
//        File dest = new File(defaultDocName);
//        FileUtils.copyURLToFile(inputUrl, dest);
//    }

    public static void main(String[] args) throws IOException, InterruptedException {

//        makeWordFile();
        /// get command line argument
        System.out.println("usage is : java -jar natbraille-soffice.jar mydocument.doc [n_thread n_repeat]");
        System.out.println("           java -jar natbraille-soffice.jar a.doc 3 10");
        System.out.println("           java -jar natbraille-soffice.jar a.doc");
        File sourceFile;
        if (args.length > 0) {
            sourceFile = new File(args[0]);
        } else {
            System.out.println("using default test document " + defaultDocName);
            sourceFile = new File(defaultDocName);
        }

        Integer nThreads;
        if (args.length > 1) {
            nThreads = Integer.parseInt(args[1]);
        } else {
            nThreads = 1;
        }

        Integer nRepeats;
        if (args.length > 2) {
            nRepeats = Integer.parseInt(args[2]);
        } else {
            nRepeats = 1;
        }

        // configure soffice
        SofficeConfig sofficeConfig = new AutoConfigurator().getSofficeConfig();
        if (sofficeConfig == null) {
            System.out.println("[configuration done] no suitable soffice found");
            System.exit(1);
        } else {
            System.out.println("[configuration done] using " + sofficeConfig);
        }

        // run threads
        System.out.println("[testing] with " + nThreads + " thread(s), with " + nRepeats + " conv each");
        for (int i = 0; i < nThreads; i++) {
            Thread t = new Thread(new Czif(sofficeConfig, sourceFile, "thread" + (i + 1), nRepeats));
            t.start();
        }

    }
}
