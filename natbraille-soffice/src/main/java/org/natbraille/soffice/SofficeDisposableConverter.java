/*
 * NatBraille - An universal Translator - soffice interface 
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret, Frédéric Schwebel
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.soffice;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Map;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.ExecuteException;
import org.natbraille.soffice.executor.NatExecutor;
import org.natbraille.soffice.executor.SofficeCliTools;
import org.natbraille.soffice.executor.TimeoutException;

/**
 * one time use converter, using a {@link  SofficeConfig}.
 * <p>
 * Calls soffice from the command line using --convert-to parameter
 * <p>
 * see {@link org.natbraille.soffice.test.App} and
 * {@link org.natbraille.soffice.test.Czif} source files for usage
 * <p>
 * @author vivien
 */
public class SofficeDisposableConverter {

    private final SofficeConfig sofficeConfig;
    private final File sourceFile;
    private final File destFile;

    private final SofficeCliTools sofficeCliTools;

    /**
     * creates a new disposable soffice converter using a soffice configuration.
     * this converter cannot be reused once executed.
     * <p>
     * @param sofficeConfig
     * @param sourceFile
     * @param destFile
     * @throws IOException
     */
    public SofficeDisposableConverter(SofficeConfig sofficeConfig, File sourceFile, File destFile) throws IOException {
        sofficeCliTools = new SofficeCliTools();
        sofficeCliTools.createUserInstallationDirectory();
        sofficeCliTools.createOutputDirectory();

        this.sofficeConfig = sofficeConfig;
        this.sourceFile = sourceFile;
        this.destFile = destFile;
        //  this.LO_OUTPUT_DIR = Files.createTempDirectory("natbraille_soffice_outputdir").toFile().getAbsolutePath();

        if (!Files.exists(sourceFile.toPath())) {
            throw new FileNotFoundException("source file '" + sourceFile + "' does not exist");
        }

    }

    /**
     * guess the soffice converted name from original filename using substring
     * magic
     * <p>
     * @return the guessed outputfilename
     */
    private File guessOutputFile() {
        String sourceFileName = sourceFile.toPath().getFileName().toString();
        String extensionLessFileName = sourceFileName.substring(0, sourceFileName.lastIndexOf("."));
        Path p = Paths.get(sofficeCliTools.getOutputDirectory(), extensionLessFileName + ".odt");
        return p.toFile();
    }


    private CommandLine buildCommandLine() {
        CommandLine cl = sofficeCliTools.getGenericCommandLine(sofficeConfig);

        // direct conversion
        cl.addArgument("--convert-to");
        cl.addArgument("${destformat}");
        cl.addArgument("--outdir");
        cl.addArgument("${outdir}");
        cl.addArgument("${sourcefile}");

        Map map = cl.getSubstitutionMap();        
        // (2014) http://ask.libreoffice.org/en/question/2641/convert-to-command-line-parameter/  
        // (2014) http://cgit.freedesktop.org/libreoffice/core/tree/filter/source/config/fragments/filters/writer8.xcu 
        map.put("destformat", "odt:writer8");
        map.put("outdir", sofficeCliTools.getOutputDirectory());
        map.put("sourcefile", sourceFile);

        cl.setSubstitutionMap(map);

        return cl;

    }

    private NatExecutor buildExecutor() {
        NatExecutor executor = new NatExecutor(sofficeConfig.getTimeout());
        executor.setExitValue(0);
        if (sofficeConfig.getPath() != null) {
            executor.setWorkingDirectory(new File(sofficeConfig.getPath()));
        }
        return executor;
    }

    /**
     * converts the file with soffice command line
     * <p>
     * @throws ExecuteException bad return value of executable
     * @throws IOException execption while running executable
     * @throws org.natbraille.soffice.executor.TimeoutException
     * @throws org.natbraille.oolo.SofficeDisposableConverter.TimeoutException
     */
    public void execute() throws ExecuteException, IOException, TimeoutException {

        CommandLine cmdLine = buildCommandLine();
        NatExecutor executor = buildExecutor();

        try {
            executor.execute(cmdLine);

            if (Files.exists(guessOutputFile().toPath())) {
                Files.move(guessOutputFile().toPath(), destFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
            } else {
                throw new FileNotFoundException("conversion result '" + guessOutputFile() + "' was empty");
            }

        } catch (ExecuteException ee) {
            if (executor.timedOut()) {
                throw new TimeoutException("timed out after " + sofficeConfig.getTimeout() + " ms", ee);
            } else {
                throw ee;
            }
        }
        //System.out.println("warning not cleaning up");
        sofficeCliTools.cleanUpUserInstallationDirectory();
        sofficeCliTools.cleanUpOutputDirectory();

    }

}
