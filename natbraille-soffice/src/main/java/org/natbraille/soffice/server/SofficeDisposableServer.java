/*
 * NatBraille - An universal Translator - soffice interface 
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret, Frédéric Schwebel
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.soffice.server;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.ExecuteException;
import org.apache.commons.exec.ExecuteResultHandler;
import static org.apache.commons.exec.ExecuteWatchdog.INFINITE_TIMEOUT;
import org.apache.commons.exec.Executor;
import org.natbraille.soffice.AutoConfigurator;
import org.natbraille.soffice.SofficeConfig;
import org.natbraille.soffice.executor.NatExecutor;
import org.natbraille.soffice.executor.SofficeCliTools;

/**
 * NOT USED.
 * TODO : use the server !
 * netstat -anp | grep LISTEN | grep soffice
 *
 * @author vivien
 */
public class SofficeDisposableServer {

    private final SofficeCliTools sofficeCliTools;
    private final String host;
    private final String port;
    public final SofficeConfig sofficeConfig;
    private final Executor executor;

    public String getHost() {
        return host;
    }

    public String getPort() {
        return port;
    }

    public SofficeDisposableServer(SofficeConfig sofficeConfig) throws IOException {
        this.sofficeConfig = sofficeConfig;
        sofficeCliTools = new SofficeCliTools();
        sofficeCliTools.createUserInstallationDirectory();
        port = "50112";
        host = "192.168.1.1";
        this.executor = new NatExecutor(INFINITE_TIMEOUT);
    }

    private class ServerStopHandler implements ExecuteResultHandler {

        @Override
        public void onProcessComplete(int exitValue) {
            System.out.println("[server happy to stop with " + exitValue + "] Should never happen ");
        }

        @Override
        public void onProcessFailed(ExecuteException e) {
            if (executor.getWatchdog().killedProcess()) {
                System.out.println("[killed]");
            } else {
                System.out.println("[failed] " + e.getLocalizedMessage());
            }
        }
    }

    public String getConnectionString() {
        return "socket,"
                + "host=" + host + ","
                + "port=" + port + ","
                + "tcpNoDelay=1" + ";"
                + "urp;"
                + "StarOffice.ServiceManager";
    }

    public void start() {

        // build the command line
        CommandLine cl = sofficeCliTools.getGenericCommandLine(sofficeConfig);

        // server launch command line
        cl.addArgument("--accept=" + getConnectionString());
        /*+ "socket,"
         + "host=" + host + ","
         + "port=" + port + ","
         + "tcpNoDelay=1" + ";"
         + "urp;"
         + "StarOffice.ServiceManager");*/

        try {
            executor.execute(cl, new ServerStopHandler());

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void kill() {
        if (executor != null) {
            executor.getWatchdog().destroyProcess();
        }
        sofficeCliTools.cleanUpUserInstallationDirectory();
    }

    public static void main(String[] args) {
        try {
            SofficeConfig loConf = new AutoConfigurator().getSofficeConfig();
            if (loConf == null){
                System.out.println("cannot find an usable libre/openoffice");
                System.exit(0);
            }
            System.out.println(loConf);
            SofficeDisposableServer loServer = new SofficeDisposableServer(loConf);
            loServer.start();
            try {
                System.out.println("sleep 5");
                Thread.sleep(5000L);
            } catch (InterruptedException ex) {
                Logger.getLogger(SofficeDisposableServer.class.getName()).log(Level.SEVERE, null, ex);
            }
            loServer.kill();
            System.out.println("stop");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "host : " + getHost() + " port : " + getPort() + "\n" + sofficeConfig;
    }

}
