/*
 * NatBraille - An universal Translator - soffice interface 
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret, Frédéric Schwebel
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.soffice;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.natbraille.soffice.configurators.MacConfigurator;
import org.natbraille.soffice.configurators.UnixConfigurator;
import org.natbraille.soffice.configurators.WindowsRegConfigurator;
import org.natbraille.tools.OsChecker;

/**
 * retrieve the best {@link SofficeConfig} of detected soffice installations
 * using multiple methods.
 * <p>
 * // TODO favor higher version number
 * <p>
 * @author vivien
 */
public class AutoConfigurator {

    private final static boolean DEBUGPRINT = false;

    /**
     * get a fonctional @{link SofficeConfig} on any os for both LibreOffice and
     * OpenOffice.org. TODO : make it work on any os. TODO : make it work wit
     * OpenOffice.org
     * <p>
     * @return the prefered SofficeConfig, null if none
     */
    public SofficeConfig getSofficeConfig() {
        if (DEBUGPRINT) {
            System.err.println("[os check] " + OsChecker.osName);
        }
        List<SofficeConfig> foundConfigurations = new ArrayList<>();

        if (OsChecker.isWindows()) {
            if (DEBUGPRINT) {
                System.err.println("[soffice check] checking from windows registry entry])");
            }

            List<SofficeConfig> regConfs = new WindowsRegConfigurator().getSofficeConfig();
            if (!regConfs.isEmpty()) {
                foundConfigurations.addAll(regConfs);
            }
        }
        if (OsChecker.isMac()) {
            if (DEBUGPRINT) {
                System.err.println("[soffice check] checking from mac common paths])");
            }
            try {
                List<SofficeConfig> regConfs = new MacConfigurator().getSofficeConfig();
                if (!regConfs.isEmpty()) {
                    foundConfigurations.addAll(regConfs);
                }
            } catch (IOException ex) {
                // ignore.
            }
        } else {
            if (DEBUGPRINT) {
                System.err.println("[soffice check] guess from soffice --version in system PATH");
            }
            try {
                SofficeConfig unixConf = new UnixConfigurator().getSofficeConfig();
                if (unixConf != null) {
                    foundConfigurations.add(unixConf);
                }
            } catch (IOException ex) {
                // ignore.
            }
        }
        for (SofficeConfig c : foundConfigurations) {
            if (DEBUGPRINT) {
                System.err.println("[found] " + c);
            }
        }
        if (foundConfigurations.size() > 0) {
            // sort using SofficeConfig compareTo
            Collections.sort(foundConfigurations);
            SofficeConfig best = foundConfigurations.get(0);
            return best;
        } else {
            return null;
        }

    }
}
