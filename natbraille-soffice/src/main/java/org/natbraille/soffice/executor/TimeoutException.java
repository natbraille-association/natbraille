/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.natbraille.soffice.executor;

/**
 * Exception thrown when an nat executor times out.
 * <p>
 * @author vivien
 */
public class TimeoutException extends Exception {

    public TimeoutException(String message, Exception cause) {
        super(message);
        setStackTrace(cause.getStackTrace());
    }
}

