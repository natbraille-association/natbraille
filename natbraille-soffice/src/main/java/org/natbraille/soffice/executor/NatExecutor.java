/*
 * NatBraille - An universal Translator - soffice interface 
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret, Frédéric Schwebel
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.soffice.executor;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.apache.commons.exec.ExecuteResultHandler;
import org.apache.commons.exec.ExecuteWatchdog;
import org.apache.commons.exec.PumpStreamHandler;

/**
 * org.apache.commons.exec.DefaultExecutor configured with :
 * <ul>
 * <li> StdOutLog as a stream handler</li>
 * <li> a default Execute watchdog;</li>
 * </ul>
 * <p>
 * see http://commons.apache.org/proper/commons-exec/
 * <p>
 * see
 * http://stackoverflow.com/questions/7340452/process-output-from-apache-commons-exec
 * <p>
 * @author vivien
 */
public class NatExecutor extends DefaultExecutor {

    private final static boolean DEBUGPRINT = false;

    private final StdOutLog stdOutLog = new StdOutLog();

    /**
     * NatExecutor constructor
     * <p>
     * @param timeout the watchdog timeout
     */
    public NatExecutor(Long timeout) {
        super();
        this.setStreamHandler(new PumpStreamHandler(stdOutLog));
        this.setWatchdog(new ExecuteWatchdog(timeout));
    }

    /**
     * Did the executable time out ?
     * <p>
     * @return true if there has been a timeout while running executor
     */
    public boolean timedOut() {
        return getWatchdog().killedProcess();
    }

    /**
     * Get the standard output line issued by the process
     * <p>
     * @return the list of lines
     */
    public List<String> getStdOut() {
        return stdOutLog.getLines();
    }

    @Override
    public int execute(CommandLine command) throws ExecuteException, IOException {
        if (DEBUGPRINT) {
            System.out.println("[starting] " + command);
        }
        return super.execute(command);
    }

    @Override
    public void execute(CommandLine command, ExecuteResultHandler handler) throws ExecuteException, IOException {
        if (DEBUGPRINT) {
            System.out.println("[starting] " + command);
        }
        super.execute(command, handler);
    }

    @Override
    public void execute(CommandLine command, Map environment, ExecuteResultHandler handler) throws ExecuteException, IOException {
        if (DEBUGPRINT) {
            System.out.println("[starting] " + command);
        }
        super.execute(command, environment, handler);
    }

}
