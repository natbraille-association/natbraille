Natbraille-soffice
==================

Multiplatform (unix / window / macos) document converter based on libreoffice 

Converts any document format known by libreoffice, such as doc, docx, txt, rtf or pdf to odt.

Autoconfigures from windows registry, common paths, system path or configuration file.

API Usage (exceptions not shown for clarity) :
			
	File inputFile = new File( ... );
	File destFile = new File( ... );
	
	// get configuration 
	SofficeConfig sofficeConfig = new AutoConfigurator().getSofficeConfig();
	
	// convert 
	SofficeDisposableConverter loDc = new SofficeDisposableConverter(sofficeConfig, inputFile, destFile);

	// execute the converter
	loDc.execute();

	
		
	
