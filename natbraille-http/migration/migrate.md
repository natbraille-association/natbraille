# gradle

javax -> jakarta
update tomcat, etc -> tomcat 10
update mongodriver, etc.

# tomcat 10 and javax.servlet / javax replacement by jakarta

- javax.servlet.ServletContext -> jakarta.servlet.ServletContext
- javax.servlet.ServletContextEvent -> jakarta.servlet.ServletContextEvent
- javax.servlet.ServletContextListener -> -> jakarta.servlet.ServletContextListener
- javax.ws.* -> jakarta.ws.*

# ServletContextListener 

remove from web.xml ; annotate org.natbraille.http.NatdServerContextListener

    <!--    @WebListener-->
    <!--    <listener>-->
    <!--        <listener-class>org.natbraille.http.NatdServerContextListener</listener-class>-->
    <!--    </listener>-->


#  remove web-app full def

keep only <web-app>

# TODO : 

replace

    @Context
    ServletContext context;
  
    @Context
    SecurityContext securityContext;

by

    @Inject
    private ServletContext context;

    @Inject
    private SecurityContext securityContext;

# Changing deps and imports

## deps

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonMethod;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

// https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-core
implementation("com.fasterxml.jackson.core:jackson-core:2.14.1")

// https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-databind
implementation("com.fasterxml.jackson.core:jackson-databind:2.14.1")

## imports

import org.codehaus.jackson.annotate.JsonIgnoreProperties; ->  import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonAutoDetect; -> import com.fasterxml.jackson.annotation.JsonAutoDetect;


import org.codehaus.jackson.JsonParseException -> import com.fasterxml.jackson.core.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException -> import com.fasterxml.jackson.databind.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper -> import com.fasterxml.jackson.databind.ObjectMapper;


import org.codehaus.jackson.annotate.JsonMethod; -> ????????????
import org.codehaus.jackson.annotate.JsonMethod; -> vire @JsonAutoDetect(JsonMethod.NONE) dans WsSourceDocument

## WEB-INF/web.xml

? remove mappings (annotation)

## META-INF/context.xml

datasource realm + postgre resource

# source de pb
 
 import org.codehaus.jackson.annotate.JsonMethod; -> ????????????








