#!/usr/bin/perl
#
# generate user/password xml fragment of tomcat-users.xml file
#
use strict;
use warnings;
use Encode;

open A, "Lexique3utf8";
my %u;
while (<A>){
    my $l = decode('UTF-8',$_); 
    my ($h,@t) = split("\t",$l);
    if ($h =~ /([a-z]+)/){
	$u{$1}=1;
    }
}
close A;

my @k = keys %u;

foreach (1 .. 100000){
    my $name = join("",map {$k[rand(@k)]} (0 .. (1+rand(1))));
    my $pwd = join(int(1+rand(255)),
		   map {$k[rand(@k)]} (0 .. (1+rand(1))));
    print join("",'<user username="',
	       $name,
	       '" password="',
	       $pwd,
	       '" roles="user" />')."\n";
 
}

