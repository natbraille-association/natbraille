# natbraille REST api

this document defines natbraille http API

# natbraille webservice typical usage

1. PUT a source document (doc, odt, txt) with 
   - (optional charset encoding)
   - (optional doctype) 

2. GET the list of transcription configurations documents (=options)
   
3. PUT a transcription request with 
   - source document ref
   - configuration refs 
   - (optional log level)


4. (optional) GET the transcription status iteratively until the output document is generated

5. GET the transcription document data and metadata


# upload source

http operation
-------------
	
PUT private/~{username}/source

request
-----------

	curl -s -u user:password \
		--form file=@Math_Lycée1.odt \
		--form press=OK \
		http://localhost:8080/natbraille-\
		http/rest/private/~username/source

response content
-----------

	{
	   "brailleTable" : null,
	   "date" : "Tue Sep 10 09:53:18 CEST 2013",
	   "charset" : "UTF-8",
	   "brailleEncoding" : null,
	   "metadata" : {
	      "creationDate" : "Tue Sep 10 09:53:18 CEST 2013",
	      "author" : "vivien"
	   },
	   "id" : "522ecfeee4b00f0ea65dd9b1",
	   "displayName" : "Math_Lycée1.odt",
	   "contentType" : "application/vnd.oasis.opendocument.text"
	}
	
# list all sources

http operation
-------------

GET private/~{username}/source


request
-----------


	curl -s -u user:password \
		http://localhost:8080/natbraille-\
		http/rest/private/~username/source

response content
-----------

	[
	   {
	      "brailleTable" : null,
	      "date" : "Mon Sep 09 15:45:06 CEST 2013",
	      "charset" : "windows-1252",
	      "brailleEncoding" : null,
	      "metadata" : {
	         "creationDate" : "Mon Sep 09 15:45:06 CEST 2013",
	         "author" : "vivien"
	      },
	      "id" : "522dd0e2e4b0ec5bbdf100c6",
	      "displayName" : "testChimie-mml.doc",
	      "contentType" : "application/msword"
	   },
	   {
	      "brailleTable" : null,
	      "date" : "Tue Sep 10 09:53:18 CEST 2013",
	      "charset" : "UTF-8",
	      "brailleEncoding" : null,
	      "metadata" : {
	         "creationDate" : "Tue Sep 10 09:53:18 CEST 2013",
	         "author" : "vivien"
	      },
	      "id" : "522ecfeee4b00f0ea65dd9b1",
	      "displayName" : "Math_Lycée1.odt",
	      "contentType" : "application/vnd.oasis.opendocument.text"
	   }
	]


# list configs



	curl -s -u user:password \
		http://localhost:8080/natbraille-\
		http/rest/private/~username/metaconfig


response content
-----------

	[
	   {
	      "info" : "(codage UTF8 : Esytime...), 32 car. par ligne",
	      "name" : "Plage braille 2",
	      "id" : "51541df0e4b0a51456cb601d",
	      "metadata" : {
	         "creationDate" : "Thu Mar 28 11:39:44 CET 2013",
	         "author" : "natbraille"
	      }
	   },
	   {
	      "info" : "(codage ANSI : Braille Sense, Iris...), 32 car. par ligne",
	      "name" : "Plage braille 1",
	      "id" : "51541df0e4b0a51456cb601e",
	      "metadata" : {
	         "creationDate" : "Thu Mar 28 11:39:44 CET 2013",
	         "author" : "natbraille"
	      }
	   },
	   {
	      "info" : "Pour l'embossage sur une embosseuse en code U.S. (32x28), numerotation des pages ",
	      "name" : "Embossage interligne simple",
	      "id" : "51541df0e4b0a51456cb601f",
	      "metadata" : {
	         "creationDate" : "Thu Mar 28 11:39:44 CET 2013",
	         "author" : "natbraille"
	      }
	   },
	   {
	      "info" : "Pour l'embossage sur une embosseuse en code U.S. (32x28), numerotation des pages ",
	      "name" : "Embossage interligne double",
	      "id" : "51541df0e4b0a51456cb6020",
	      "metadata" : {
	         "creationDate" : "Thu Mar 28 11:39:44 CET 2013",
	         "author" : "natbraille"
	      }
	   }
	]
	


# get one config

http operation
-------------

GET private/~{username}/metaconfig/~{configId}

request
-----------

	curl -s -u user:password \
		http://localhost:8080/natbraille-\
		http/rest/private/~username/metaconfig/51541df0e4b0a51456cb601e

response content
-----------

	{
	   "FORMAT_LINE_LENGTH" : "32",
	   "LAYOUT_NUMBERING_MODE" : "'nn'",
	   "CONFIG_NAME" : "Plage braille 1",
	   "CONFIG_DESCRIPTION" : "(codage ANSI : Braille Sense, Iris...), 32 car. par ligne",
	   "FORMAT_OUTPUT_ENCODING" : "cp1252",
	   "FORMAT_OUTPUT_BRAILLE_TABLE" : "TbFr2007"
	}


# create transcription request document


http operation
-------------

PUT private/~username/transcription/

request
-----------

	curl -s -u user:password -H "Content-Type: application/json" -X POST -d '{"metaConfigurationId":"51541df0e4b0a51456cb6020","logLevel":2,"sourceDocumentId":"522ecfeee4b00f0ea65dd9b1"}' \
		http://localhost:8080/natbraille-\
		http/rest/private/~username/transcription/

response content
-----------

	{
	   "metaConfigurationId" : "51541df0e4b0a51456cb6020",
	   "status" : [
	      {
	         "kind" : "INFO",
	         "logLevel" : 1,
	         "contents" : "la transcription va débuter"
	      }
	   ],
	   "id" : "522ed087e4b00f0ea65dd9b2",
	   "metadata" : {
	      "creationDate" : "Tue Sep 10 09:55:51 CEST 2013",
	      "author" : "vivien"
	   },
	   "destDocumentId" : null,
	   "sourceDocumentId" : "522ecfeee4b00f0ea65dd9b1"
	}
	


# query transcription state document



http operation
-------------

GET private/~username/transcription/{transcriptionRequestId}

request
-----------

	curl -s -u user:password \
		http://localhost:8080/natbraille-\
		http/rest/private/~username/transcription/522ed087e4b00f0ea65dd9b2

response content
-----------

	{
	   "metaConfigurationId" : "51541df0e4b0a51456cb6020",
	   "status" : [
	      {
	         "kind" : "INFO",
	         "logLevel" : 1,
	         "contents" : "la transcription va débuter"
	      },
	      {
	         "kind" : "STEP",
	         "logLevel" : 2,
	         "contents" : "1/6"
	      },
	      {
	         "kind" : "STEP",
	         "logLevel" : 2,
	         "contents" : "2/6"
	      },
	      {
	         "kind" : "STEP",
	         "logLevel" : 2,
	         "contents" : "3/6"
	      },
	      {
	         "kind" : "STEP",
	         "logLevel" : 2,
	         "contents" : "4/6"
	      },
	      {
	         "kind" : "INFO",
	         "logLevel" : 2,
	         "contents" : "double inter"
	      },
	      {
	         "kind" : "INFO",
	         "logLevel" : 2,
	         "contents" : " "
	      },
	      {
	         "kind" : "INFO",
	         "logLevel" : 2,
	         "contents" : " "
	      },
	      {
	         "kind" : "INFO",
	         "logLevel" : 2,
	         "contents" : "taille"
	      },
	      {
	         "kind" : "INFO",
	         "logLevel" : 2,
	         "contents" : " "
	      },
	      {
	         "kind" : "INFO",
	         "logLevel" : 2,
	         "contents" : "2"
	      },
	      {
	         "kind" : "INFO",
	         "logLevel" : 2,
	         "contents" : "\n"
	      },
	      {
	         "kind" : "STEP",
	         "logLevel" : 2,
	         "contents" : "5/6"
	      },
	      {
	         "kind" : "STEP",
	         "logLevel" : 2,
	         "contents" : "6/6"
	      },
	      {
	         "kind" : "INFO",
	         "logLevel" : 2,
	         "contents" : "le filtre TRANSCRIPTOR démarre"
	      },
	      {
	         "kind" : "INFO",
	         "logLevel" : 2,
	         "contents" : "document has content-type application/vnd.oasis.opendocument.text charset UTF-8"
	      },
	      {
	         "kind" : "INFO",
	         "logLevel" : 2,
	         "contents" : "le filtre ODT vers XHTML (writer2Latex) démarre"
	      },
	      {
	         "kind" : "SUCCESS",
	         "logLevel" : 2,
	         "contents" : "filter ODT vers XHTML (writer2Latex) exits after 428 ms"
	      },
	      {
	         "kind" : "INFO",
	         "logLevel" : 2,
	         "contents" : "le filtre XML vers FORMAT INTERNE démarre"
	      },
	      {
	         "kind" : "SUCCESS",
	         "logLevel" : 2,
	         "contents" : "filter XML vers FORMAT INTERNE exits after 4 271 ms"
	      },
	      {
	         "kind" : "INFO",
	         "logLevel" : 2,
	         "contents" : "le filtre TRANSCODAGE : FORMAT INTERNE vers BRAILLE démarre"
	      },
	      {
	         "kind" : "SUCCESS",
	         "logLevel" : 2,
	         "contents" : "filter TRANSCODAGE : FORMAT INTERNE vers BRAILLE exits after 3 643 ms"
	      },
	      {
	         "kind" : "INFO",
	         "logLevel" : 2,
	         "contents" : "le filtre PRESENTATEUR (mise en page) démarre"
	      },
	      {
	         "kind" : "INFO",
	         "logLevel" : 2,
	         "contents" : "⠨⠍⠁⠞⠓⠿⠍⠁⠞⠊⠟⠥⠑⠎ⴴ⠝⠊⠧⠑⠁⠥"
	      },
	      {
	         "kind" : "SUCCESS",
	         "logLevel" : 2,
	         "contents" : "filter PRESENTATEUR (mise en page) exits after 1 037 ms"
	      },
	      {
	         "kind" : "INFO",
	         "logLevel" : 2,
	         "contents" : "le filtre BRAILLE TABLE TRANSLITERATION (java) démarre"
	      },
	      {
	         "kind" : "SUCCESS",
	         "logLevel" : 2,
	         "contents" : "filter BRAILLE TABLE TRANSLITERATION (java) exits after 35 ms"
	      },
	      {
	         "kind" : "SUCCESS",
	         "logLevel" : 2,
	         "contents" : "filter TRANSCRIPTOR exits after 9 430 ms"
	      },
	      {
	         "kind" : "FINAL_SUCCESS",
	         "logLevel" : 0,
	         "contents" : "522ed090e4b00f0ea65dd9cf"
	      }
	   ],
	   "id" : "522ed087e4b00f0ea65dd9b2",
	   "metadata" : {
	      "creationDate" : "Tue Sep 10 09:55:51 CEST 2013",
	      "author" : "vivien"
	   },
	   "destDocumentId" : "522ed090e4b00f0ea65dd9cf",
	   "sourceDocumentId" : "522ecfeee4b00f0ea65dd9b1"
	}
	


# get result

http operation
-------------

GET public/~username/resu/{transcriptionResultId}

request
-----------

	curl -s -u user:password \
		http://localhost:8080/natbraille-\
		http/rest/public/~username/resu/522ed090e4b00f0ea65dd9cf

response content
-----------

	{
	   "metadata":
	   {
	      "creationDate":"Tue Sep 10 09:56:00 CEST 2013",
	      "author":"vivien"
	   },
	   "date":"Tue Sep 10 09:56:00 CEST 2013",
	   "displayName":"Math_Lycée1.odt_Embossage interligne double.natbraille.txt",
	   "contentType":"text/plain",
	   "charset":"US-ASCII",
	   "id":"522ed090e4b00f0ea65dd9cf",
	   "brailleTable":"nat://system/xsl/tablesEmbosseuse/CodeUS.ent",
	   "brailleEncoding":"US-ASCII",
	   "transcriptionId":"522ed087e4b00f0ea65dd9b2"
	}


# get file

http operation
-------------

GET public/~username/resu/{transcriptionResultId}/{desiredFilename}

request
-----------

	curl -s -u user:password \
		http://localhost:8080/natbraille-\
		http/rest/public/~username/resu/522ed090e4b00f0ea65dd9cf/doc

response content
-----------

	                              ,*
	     .MATH=MATIQUES NIVEAU
	
	        LYC=E3 S=RIE ,*
	
	
	  .I4 .=QUATIONS DU SECOND DEGR=
	
	  ,*0 .SOLUTION DE L'=QUATION
	
....