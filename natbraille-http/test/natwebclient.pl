#!/usr/bin/perl
use JSON;
use WWW::Curl::Easy;
use Term::ANSIColor;
use strict;
use warnings;


my ($user,$password) = ("vivien","vivien"); 
my ($host,$port) = ("localhost","8080");
my ($api_rel) = ("natbraille-http/rest");

my $private_base_url = "http://$host:$port/$api_rel/private/~$user";
my $public_base_url  = "http://$host:$port/$api_rel/public/~$user";

my $curl_cmd = "curl -s";
my $curl_auth_cmd = "curl -s -u $user:$password";


#
# presentation
#

sub MDCodeBlock {
    my $string = shift;
    $string =~ s/\n/\n\t/g;
    return "\n\t".$string."\n\n";
}
sub MDCutCommand {
    my $string = shift;
    $string =~ s/--/\\\n\t--/g;
    $string =~ s/http/\\\n\thttp/g;
    return $string;
    
}
sub MDTitle {
    my $content = shift;
    return $content."\n===========\n\n";
}
sub MDSubTitle {
    my $content = shift;
    return $content."\n-----------\n";
}

#
# request
#

sub makeRequestGetJson {
    my $cmd = shift;

    print MDSubTitle("request");
    print MDCodeBlock(MDCutCommand($cmd));

    my $answer = join("",`$cmd`);
    my $vari = 0;
    my $joli;
    eval {
	$vari = JSON->new->decode(join('',$answer));
	$joli = JSON->new->pretty->encode($vari);
	print MDSubTitle("response content");
	print MDCodeBlock($joli)."\n";
    };

    return $vari;
}

sub makeRequest {
    my $cmd = shift;

    print MDSubTitle("request");
    print MDCodeBlock(MDCutCommand($cmd));
    print MDSubTitle("response content");
    my $answer = join("",`$cmd`);
    print MDCodeBlock($answer);
    return $answer;

}


#
# ws calls
#
sub getDestDocuments{
    my $cmd = join(" ",
		   $curl_auth_cmd,
		   join('/',$private_base_url,'resu'));
    return makeRequestGetJson($cmd);
}
sub getSourceDocuments{
    my $cmd = join(" ",
		   $curl_auth_cmd,
		   join('/',$private_base_url,'source'));
    return makeRequestGetJson($cmd);
}

sub getMetaConfigurations{
    my $cmd = join(" ",
		   $curl_auth_cmd,
		   join('/',$private_base_url,'metaconfig'));
    return makeRequestGetJson($cmd);
}

sub getMetaConfiguration{
    my $cfgId = shift;
    my $cmd = join(" ",
		   $curl_auth_cmd,
		   join('/',$private_base_url,'metaconfig',$cfgId));
    return makeRequestGetJson($cmd);
}

sub postSourceDocument{
    my ($filename,$charset,$mimeType) = @_;
    if (!$mimeType){
	$mimeType = 0;
    }
    if (!$charset){
	$charset = 0;
    }
    my $cmd = join(" ",
		   $curl_auth_cmd,
		   '--form',join('','file','=@',$filename),
		   (grep {$charset} ('--form',join('','charset','=',$charset))),
		   (grep {$mimeType} ('--form',join('','mimeType','=',$mimeType))),
		   '--form',join('','press','=','OK'),
		   join('/',$private_base_url,'source'));

    return makeRequestGetJson($cmd);
    
}

sub postTranscriptionUrlEncoded{
    my ($sourceDocumentId,$metaConfigurationId,$logLevel) = @_;
    my $cmd = join(" ",
		   $curl_auth_cmd,
		   "--data-urlencode sourceDocumentId=$sourceDocumentId",
		   "--data-urlencode metaConfigurationId=$metaConfigurationId",
		   "--data-urlencode logLevel=$logLevel",
		   join('/',$private_base_url,'transcription/'));
    return makeRequestGetJson($cmd); 
}


# curl  -H "Content-Type: application/json" 
# -H "Accept: application/json" -X POST -d '{"person":{"name":"bob"}}' http://mysite.com/api.php

sub postTranscription{
    my ($sourceDocumentId,$metaConfigurationId,$logLevel) = @_;
    
    my $postdata = {
	'sourceDocumentId' => $sourceDocumentId,
	'metaConfigurationId'  => $metaConfigurationId,
	'logLevel' => $logLevel,
    };
    
    my $cmd = join(" ",
		   $curl_auth_cmd,
		   '-H "Content-Type: application/json"',
		   '-X POST',
		   "-d '".JSON->new->encode($postdata)."'",
		   join('/',$private_base_url,'transcription/'));

    return makeRequestGetJson($cmd); 
}


sub deleteSourceDocument{
    my ($sourceDocumentId) = @_;
        
    my $cmd = join(" ",
		   $curl_auth_cmd,
		   '-X DELETE',
		   join('/',$private_base_url,'source',$sourceDocumentId));

    return makeRequestGetJson($cmd); 
}

sub getTranscription{
    my $transcriptionId = shift;
    my $cmd = join(" ",
		   $curl_auth_cmd,
#		   join('',$private_base_url,'transcription/',$transcriptionId));
		   join('/',$private_base_url,'transcription',$transcriptionId));
    return makeRequestGetJson($cmd); 
}

sub getTranscriptions{
    my $cmd = join(" ",
		   $curl_auth_cmd,
		   join('/',$private_base_url,'transcription'));
    return makeRequestGetJson($cmd); 
}

sub getDestDocument{
    my $destDocumentId = shift;

    my $cmd = join(" ",
		   $curl_auth_cmd,
		   join('/',$public_base_url,'resu',$destDocumentId));
    return makeRequest($cmd); 

}



#
# command line
#
my %commandes = (
    'config' => [
	':list existing configurations',
	'configurationId : get configuration content' ],
    'transcription' =>  [
	':list user transcriptions',
	'sourceDocumentId metaConfigurationId [logLevel] :starts a new transcription',
	'transcriptionId:query transcription state'],
    'resu' => [
	':get the list of transcribed files',
	'destDocumentId:download transcribed file '],
    'help' => [
	'destDocumentId:this help'],
    'source' => [
	':list source documents',
	'filename [charset [mimeType]]:upload a file to the nat server'],
    'delete source' => [
        'sourceDocumentId:delete this source document']
    );	


sub usage {
    my @categs  = @_;
    print "usage : natwebclient ".join("|",map {color('green').$_.color('reset')} @categs)." \n";
    foreach my $c (@categs){
	foreach (@{$commandes{$c}}){
	    my ($p,$ex) = split(':',$_);
	    print "  ".color('green').$c.color('reset');
	    print " ".color('blue').$p.color('reset')."\n";
	    print "   ".$ex."\n";

	}
    }
}

my $cmd = $ARGV[0]?$ARGV[0]:"";


if ($cmd eq 'config'){
    if($ARGV[1]){
	print MDTitle("getting config ".$ARGV[1]);
	my $mc = getMetaConfiguration($ARGV[1]);
    } else {
	print MDTitle("getting all configs");
	my $mc = getMetaConfigurations();
    }
} elsif ($cmd eq 'source'){
    if ($ARGV[1]){
	print MDTitle(join(" ","posting source file '".$ARGV[1]."'","with options",grep {$_} @ARGV[2..3]));
	my $sd = postSourceDocument($ARGV[1],grep {$_} @ARGV[2..3]);
    } else {
	print MDTitle(join(" ","getting user source files "));
	getSourceDocuments();
    }
} elsif ($cmd eq 'transcription'){
    if (!$ARGV[1]){
	print MDTitle(join(" ","getting user transcription information "));
	my $tri = getTranscriptions();
    } elsif ($ARGV[1] && $ARGV[2]){
	print MDTitle(join(" ","posting transcription "));
	my $loglevel = $ARGV[3];
	if (!$loglevel){
	    $loglevel=2;
	}
	my $ntr = postTranscription($ARGV[1],$ARGV[2],$loglevel);
    } elsif ($ARGV[1]){
	print MDTitle(join(" ","getting transcription "),$ARGV[1]);
	my $tri = getTranscription($ARGV[1]);
    }    
} elsif ($cmd eq 'resu'){
    if ($ARGV[1]){
	print MDTitle(join(" ","getting transcribed files "),$ARGV[1]);
	my $sd = getDestDocument($ARGV[1]);
    } else {
	print MDTitle(join(" ","getting all transcribed files "),$ARGV[1]);
	my $sd = getDestDocuments();
    }
}  elsif ($cmd eq 'delete'){
    if (($ARGV[1] eq 'source')&&($ARGV[2])){
	print MDTitle(join(" ","deleting source "),$ARGV[2]);
	my $sd = deleteSourceDocument($ARGV[2]);
    }
} else {
    print usage('upload','source','config','transcription','resu','help');
}

