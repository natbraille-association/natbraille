//TODO : remove resu

/*
 * Configuration options translation.
 */ 

var I18n = {
    optionsComments : {
	LAYOUT_NUMBERING_MODE : "hs siggnieife ....",
    },
    optionsTranslations : {
	CONFIG_NAME:"nom",
	CONFIG_DESCRIPTION:"description",
	MODE_LIT:"convertir les expressions littéraires",
	MODE_MATH:"convertir les équations mathématiques",
	MODE_MATH_SPECIFIC_NOTATION :"utiliser la notation trigonométrique",
	MODE_MATH_ALWAYS_PREFIX:"toujours ajouter le préfixe mathématique",
	MODE_MUSIC:"traiter la musique",
	MODE_CHEMISTRY:"traiter la chimier",
	MODE_IMAGES:"traiter les images"	,
	MODE_DETRANS  :"transcription inverse (braille vers noir)",
	MODE_LIT_FR_MAJ_DOUBLE  :"double préfixe pour les mots en majuscules",
	MODE_LIT_FR_MAJ_PASSAGE  :"traiter les passages de texte en majuscules",
	MODE_LIT_FR_MAJ_MELANGE  :"traiter le mélange majuscule/minuscules",
	MODE_LIT_EMPH  :"traiter la mise en évidence",
	MODE_LIT_FR_EMPH_IN_WORD  :"traiter la mise en évidence à l'intérieur d'un mot",
	MODE_LIT_FR_EMPH_IN_PASSAGE  :"Process emphasis in a passage",
	MODE_G2  :"convertir en braille abrégé",
	MODE_G2_HEADING_LEVEL:"premier niveau de titre abrégé",
	IvbMajSeule  :"always true",
	MODE_G2_G1_WORDS  :"List,of,words that should be kept in G1 Braille",
	MODE_G2_G1_AMB_WORDS  :"List,of,ambiguous,words that should be kept in G1 Braille",
	MODE_G2_IVB_WORDS  :"List,of,words that require an 'ivb'",
	MODE_G2_IVB_AMB_WORDS  :"List,of,ambiguous,words that that require an 'ivb'",
	MODE_ARRAY_2D_MIN_CELL_NUM :"Minimum table cell number for 2D (non linear) table",
	MODE_ARRAY_LINEARIZE_ALWAYS  :"Always linearize tables",
	MODE_DETRANS_KEEP_HYPHEN  :"Preserve hyphenation found in original braille document",
	LAYOUT  :"Do final layout",
	LAYOUT_LIT_HYPHENATION  :"Activate of literary Braille line breaking",
	LAYOUT_DIRTY_HYPHENATION  :"Activate quirky line breaking",
	LAYOUT_NUMBERING_MODE :"emplacement de la numérotation",
	LAYOUT_NUMBERING_START_PAGE:"première page numérotée",
	LAYOUT_PAGE_EMPTY_LINES_MODE:"traitement des ligne vides",
	LAYOUT_PAGE_EMPTY_LINES_CUSTOM_1:"Number of consecutive empty lines in input outputing one empty braille line",
	LAYOUT_PAGE_EMPTY_LINES_CUSTOM_2:"number of consecutive empty lines in input outputing two empty braille lines",
	LAYOUT_PAGE_EMPTY_LINES_CUSTOM_3:"number of consecutive empty lines in input outputing three empty braille lines",
	LAYOUT_PAGE_BREAKS  :"Generate page breaks",
	LAYOUT_PAGE_BREAKS_MIN_LINES:"Minimum number of line for a page break",
	LAYOUT_PAGE_BREAKS_KEEP_ORIGINAL  :"Keep the page breaks of the original document",
	LAYOUT_PAGE_HEADING_LEVEL  :"Heading,levels,list in output in input document heading level order '1,2,3,4,5,5,5,5,5'",
	LAYOUT_PAGE_TOC  :"Build an table of contents",
	LAYOUT_PAGE_TOC_NAME  :"Build an table of contents title",
	LAYOUT_NOLAYOUT_TR_INPUT  :"String replacement sources",
	LAYOUT_NOLAYOUT_TR_OUTPUT  :"String replacement destinations",
	LAYOUT_NOLAYOUT_OUTPUT_TAGS :  "List,of,string to be added before/after : document, paragraph, line, mathematics, lit, music ",
	FORMAT_LINE_LENGTH:"nombre de caractères par ligne",
	FORMAT_PAGE_LENGTH:"nombre de lignes par page ",
	FORMAT_OUTPUT_ENCODING  :"encodage de sortie",
	FORMAT_OUTPUT_BRAILLE_TABLE :"table braille",
	Stylist:"url of the xml layout style document",
	XSL_g1:"url of Grade 1 Braille conversion xsl stylesheet",
	XSL_g2 :"url of Grade 2 Braille conversion xsl stylesheet",
	XSL_maths:"url of mathematical Braille conversion xsl stylesheet",
	XSL_musique:"url of Braille music conversion xsl stylesheet",
	XSL_chimie:"url of Chemical music conversion xsl stylesheet",
	XSL_g2_Rules:"rules for grade 2",
	XSL_FR_HYPH_DIC:"hyphenation dictionnary",
	XSL_FR_HYPH_RULES  :"hyphenation rules",
	XSL_G2_DICT :"grade 2 dictionnary",
	imageMagickDir  :"image magick dir for image transcription (not used)",
	preserveXhtmlTags  :"keep xml tags?",
	preserveTags  :"keep xml tags?",
	debug_document_write_temp_file  :"write generated documents to temporary file",
	debug_dyn_xsl_write_temp_file  :"write generated xsl transformation stylesheets to temporary file",
	debug_document_write_options_file  :"write transcription options to temporary file",
	debug_document_show  :"show generated documents for debug",
	debug_document_show_max_char:"size of tail of generated documents display for debug",
	debug_dyn_ent_res_show  :"show detail of dynamic entities resolution for debug",
	debug_dyn_res_show  :"show detail of dynamic resolution for debug",
	debug_dyn_xsl_show  :"show generated xsl stylesheets for debug",
	debug_xsl_processing  :"debug the transcodeur (boolean)",
    },

    optionCommentTr : function(optionName){
	var tr = this.optionsComments[optionName];
	if (tr){
	    return tr;
	} else {
	    return 0;
	}
	
    },
    optionTr : function(optionName){
	var tr = this.optionsTranslations[optionName];
	if (tr){
	    return tr;
	} else {
	    return optionName;
	}
    },
};

/*
 * Natbraille scope.
 */

var Natbraille = {};

/*
 * Site configuration.
 */
 function getBasePath(location=document.location){
    const url = new URL(location)
    const basePathname = url.pathname.replace(/index\.html$/,'').replace(/[/]$/,'')
    return basePathname
 }
Natbraille.configuration = {
    // restRoot:" /natbraille-http/rest",
    restRoot : [getBasePath(),'rest'].join('/')
};

/*
 * States.
 * store application state, such as current config, current document, current transcription, current edited conf
 */
Natbraille.states = {};
Natbraille.states.definition = Backbone.Model.extend({
    initialize : function(){
	this.listenTo(Natbraille.models.instances.configCollection,"add",this.selectUpcomingConfig);
	this.listenTo(Natbraille.models.instances.sourceCollection,"add",this.selectUpcomingSource);
    },
    selectUpcomingConfig : function(config){
	this.set('currentConfig',config.id);
    },
    selectUpcomingSource : function(source){
	this.set('currentSource',source.id);
    },
    getConfiguration : function(){
	return  Natbraille.models.instances.configCollection.get(this.get('currentConfig'));
    },
    // in memory
    getEditedConfig : function(){
	return  this.get('editedConfig');
    },
    getEditedOptions : function(){
	return  this.get('editedOptions');
    },
    getSource : function(){
	return  Natbraille.models.instances.sourceCollection.get(this.get('currentSource'));
    },
    getTranscription : function(){
	return  Natbraille.models.instances.transcriptionCollection.get(this.get('currentTranscription'));
    },
});

/*
 * Data Models.
 * Backbone collections and models corresponding to webservices input/output
 */
Natbraille.models = {};
Natbraille.models.definitions = {};
Natbraille.models.definitions.configformat = Backbone.Model.extend({
    urlRoot: function () {
        return Natbraille.configuration.restRoot+'/public/metaconfig/format';
    },
});
Natbraille.models.definitions.user = Backbone.Model.extend({
    urlRoot: function () {
        return Natbraille.configuration.restRoot+'/private/user';
    },
});
Natbraille.models.definitions.config = Backbone.Model.extend({
    urlRoot: function () {
        return Natbraille.configuration.restRoot+'/private/~'+Natbraille.models.instances.user.get('name')+'/metaconfig';
    },
    getNotice : function(){
	return Natbraille.models.instances.configNoticeCollection.get(this.attributes.id);
    }
});
Natbraille.models.definitions.configNoticeCollection = Backbone.Collection.extend({
    model: Natbraille.models.definitions.config,
    url : function(){
	return Natbraille.configuration.restRoot+'/private/~'+Natbraille.models.instances.user.get('name')+'/metaconfig/';
    }, 
});
Natbraille.models.definitions.configCollection = Backbone.Collection.extend({
    comparator : function(item_a,item_b){
	return (item_b.id>item_a.id);
    },
    model: Natbraille.models.definitions.config,
    url : function(){
	return Natbraille.configuration.restRoot+'/private/~'+Natbraille.models.instances.user.get('name')+'/metaconfig/';
    }, 
    initialize : function(){
	// when a configuration notice is added
	this.listenTo(Natbraille.models.instances.configNoticeCollection,"add",this.addNotice);
    },    
    // fetch the configuration from the notice id
    addNotice : function(notice){
	var that = this;
	var full = new Natbraille.models.definitions.config({id:notice.id});
	full.fetch({
	    success : function(full){
		that.add(full);
	    }
	});
    },
    
});


Natbraille.models.definitions.source = Backbone.Model.extend({
    urlRoot :  function() {
	return Natbraille.configuration.restRoot+'/private/~'+Natbraille.models.instances.user.get('name')+'/source';
    },
});
Natbraille.models.definitions.sourceCollection = Backbone.Collection.extend({
    comparator : function(item_a,item_b){
	return (item_b.id>item_a.id);
    },
    model :Natbraille.models.definitions.source,
    url : function(){
	return Natbraille.configuration.restRoot+'/private/~'+Natbraille.models.instances.user.get('name')+'/source/';
    },
});
Natbraille.models.definitions.transcription = Backbone.Model.extend({
    urlRoot :  function() {
	return Natbraille.configuration.restRoot+'/private/~'+Natbraille.models.instances.user.get('name')+'/transcription';
    },
    // get related configuration from config collection
    getRelatedConfig : function(){
	return Natbraille.models.instances.configCollection.get(this.get('metaConfigurationId'));
    },
    // get related configuration from source collection
    getRelatedSource : function(){
	return Natbraille.models.instances.sourceCollection.get(this.get('sourceDocumentId'));
    },

    // build an outputfilename and a url for the result as attachement
    getResuUrl : function(){
	var outputName;
	if (this.getRelatedSource()){
	    outputName = this.getRelatedSource().get('displayName');
	} else {
	    outputName = "translation";
	}
	if (this.getRelatedConfig()){
	    outputName = outputName + "_" + this.getRelatedConfig().get('CONFIG_NAME');
	} 
	outputName = outputName + ".txt";
	if (this.get('destDocumentId')){
	    return Natbraille.configuration.restRoot+'/public/~'+Natbraille.models.instances.user.get('name')
		+'/resu/'+this.get('destDocumentId')
		+'/attachment'
		+"/"+outputName;
	    ;
	}
    },
});

Natbraille.models.definitions.transcriptionCollection = Backbone.Collection.extend({
    comparator : function(item_a,item_b){
	return (item_b.id>item_a.id);
    },
    model : Natbraille.models.definitions.transcription,
    url : function(){
	return Natbraille.configuration.restRoot+'/private/~'+Natbraille.models.instances.user.get('name')+'/transcription/';
    }
});

Natbraille.models.definitions.resu = Backbone.Model.extend({
});

Natbraille.models.definitions.resuCollection = Backbone.Collection.extend({
    model : Natbraille.models.definitions.Resu,
    url : function(){
	return Natbraille.configuration.restRoot+'/private/~'+Natbraille.models.instances.user.get('name')+'/resu/';
    },
});

/*
 * Templates.
 * Underscore.js compiled templates from html content.
 */
Natbraille.templates = {};
Natbraille.templates.user = $('#tmpl_connected_user').html();
Natbraille.templates.configs = $('#tmpl_configs').html();
Natbraille.templates.config = $('#tmpl_config').html();
Natbraille.templates.confirm_config_deletion = $('#tmpl_confirm_config_deletion').html();
Natbraille.templates.config_detail = $('#tmpl_config_detail').html();
Natbraille.templates.file_transcription = $('#tmpl_file_transcription').html();
Natbraille.templates.sources = $('#tmpl_sources').html();
Natbraille.templates.source = $('#tmpl_source').html();
Natbraille.templates.confirm_source_deletion = $('#tmpl_confirm_source_deletion').html();
Natbraille.templates.transcription_detail = $('#tmpl_transcription_detail').html();
Natbraille.templates.transcriptions = $('#tmpl_transcriptions').html();
Natbraille.templates.transcriptions_item = $('#tmpl_transcriptions_item').html();
Natbraille.templates.confirm_transcription_deletion = $('#tmpl_confirm_transcription_deletion').html();
Natbraille.templates.source_upload = $('#tmpl_source_upload').html();
Natbraille.templates.instant_transcription = $('#tmpl_instant_transcription').html();
Natbraille.templates.config_editor = $('#tmpl_config_editor').html();

Natbraille.models.instances = {};
/*
 * Views.
 * Backbone views
 */
Natbraille.views = {};
Natbraille.views.definitions = {};

Natbraille.views.definitions.simpleTemplate = Backbone.View.extend({
    
});

// user
Natbraille.views.definitions.user = Backbone.View.extend({
    el : $(".connected_user"),
    initialize : function(){
	// when user change
	this.listenTo(Natbraille.models.instances.user,"change",this.render);
    },
    template : function(data){
	return _.template(Natbraille.templates.user,data);
    },
    render: function() {
	this.$el.html(this.template(Natbraille.models.instances.user.attributes));	
	return this;
    },
});

// configuration in a list
Natbraille.views.definitions.config = Backbone.View.extend({
    initialize : function(){	
	this.listenTo(this.model,"destroy",this.removeCool);
    },
    events: {
	"click .select":    "select",
	"click .delete":    "delete",
	"click .detail":    "detail",
    },
    select : function(){
	window.history.back();
	Natbraille.states.instance.set("currentConfig",this.model.id);
    },
    removeCool : function(){
	var that = this;
	$( this.$el ).hide( "fast", function() {
	    that.remove();
	});
    },
    detail : function(){
	var that = this;
	this.$el.append(_.template(Natbraille.templates.config_detail,this.model));
	this.$( "#dialog-confirm" ).dialog({
	    modal: false,
	    buttons: {
		Close: function() {
		    $( this ).remove();
		},
	    }

	});

    },
    delete : function(){
	var that = this;
	this.$el.append(_.template(Natbraille.templates.confirm_config_deletion,this.model));
	this.$( "#dialog-confirm" ).dialog({
	    modal: true,
	    buttons: {
		Ok: function() {
		    that.model.destroy({wait: true});
		    $( this ).remove();
		},
		Cancel: function() {
		    $( this ).remove();
		}
	    }

	});
    },
    template : function(data){
	return _.template(Natbraille.templates.config,data);
    },
    render: function() {
	this.$el.html(this.template({model:this.model,buttons:this.options.buttons}));
	return this;
    },
});

// list of configurations
Natbraille.views.definitions.configs = Backbone.View.extend({
    initialize : function(){	
	this.listenTo(Natbraille.models.instances.configCollection,"add",this.render);
	this.listenTo(Natbraille.models.instances.configCollection,"remove",this.render);
    },
    template : function(data){
	return _.template(Natbraille.templates.configs,data);
    },
    buttons : {deleteButton:true,selectButton:true},
    render: function(whate) {
	var that = this;
	this.$el.html(this.template(Natbraille.models.instances.configCollection.models));
	Natbraille.models.instances.configCollection.each(function(config){    
	    new Natbraille.views.definitions.config({model:config,el:that.$("."+config.id),buttons:that.buttons}).render();
	});
	return this;
    },
    setButtons : function(buttons){
	this.buttons = buttons;
	this.render();
    }
});

// source in a list
Natbraille.views.definitions.source = Backbone.View.extend({
    initialize : function(){	
	this.listenTo(this.model,"destroy",this.removeCool);
	this.listenTo(Natbraille.states.instance,"change:currentSource",this.selectedSourceId);

    },
    events: {
	"click .select":    "select",
	"click .delete":    "delete",
    },
    //TODO!!!
    selectedSourceId : function(states){
//	if (states.get('currentSource') === this.model.id){
//	    $($(this.$el)).addClass("selectee");
//	    $(this.$el).addClass("selectee");
//	} else {
//	    $($(this.$el)).removeClass("selectee");
//	    $(this.$el).removeClass("selectee");
//	    console.error(this.model.id);
//	}
    },
    select : function(){
	window.history.back();
	Natbraille.states.instance.set("currentSource",this.model.id);
    },
    removeCool : function(){
	var that = this;
	$( this.$el ).hide( "fast", function() {
	    that.remove();
	});
    },  
    delete : function(){
	var that = this;
	this.$el.append(_.template(Natbraille.templates.confirm_source_deletion,this.model));
	this.$( "#dialog-confirm" ).dialog({
	    modal: true,
	    buttons: {
		Ok: function() {
		    that.model.destroy({wait: true});
		    $( this ).remove();
		},
		Cancel: function() {
		    $( this ).remove();
		}
	    }
	});
    },
    template : function(data){
	return _.template(Natbraille.templates.source,data);
    },
    render: function() {
	this.$el.html(this.template({model:this.model,buttons:this.options.buttons}));
	return this;
    },
});

// list of sources
Natbraille.views.definitions.sources = Backbone.View.extend({
    initialize : function(){	
	this.listenTo(Natbraille.models.instances.sourceCollection,"add",this.render);
    },
    template : function(data){
	return _.template(Natbraille.templates.sources,data);
    },
    render: function(whate) {
	var that = this;
	this.$el.html(this.template(Natbraille.models.instances.sourceCollection.models));
	Natbraille.models.instances.sourceCollection.each(function(source){    
	    new Natbraille.views.definitions.source({model:source,el:that.$("."+source.id),buttons:that.buttons}).render();
	});

	return this;
    },
    buttons : {deleteButton:true,selectButton:true},
    setButtons : function(buttons){
	this.buttons = buttons;
	this.render();
    }
});

// source upload
Natbraille.views.definitions.sourceUpload = Backbone.View.extend({
    events : {
	'click input' : 'newUpload',
	'change input[type=file]' : 'fileChanged',
    },
    initialize : function(){
	this.enableSubmit(false);
    },
    fileChanged : function(event){
	this.enableSubmit(event.currentTarget 
			  && event.currentTarget.files 
			  && (event.currentTarget.files.length > 0));
    },
    enableSubmit : function(enable){
	if (enable == false){
	    $('input[type=submit]', this.el).attr('disabled', 'disabled');
	    $('input[type=file]', this.el).attr('disabled', 'disabled');
	} else {
	    $('input[type=submit]', this.el).removeAttr('disabled');
	    $('input[type=file]', this.el).removeAttr('disabled');
	}
    },

    newUpload : function(){
	this.unsetProgress();
	this.unsetFailedUpload();
    },

    endUpload : function(ok,newInputDocument){
	this.enableSubmit(true);
	this.unsetProgress();
	if (!ok){
	    this.setFailedUpload();
	} else {
	    window.history.back();
	}
    },

    setProgress : function(percentComplete){
	$('.progress',this.el).text("transfert du fichier en cours : "+percentComplete+" % ");
    },
    unsetProgress : function(){
	$('.progress',this.el).text("");
	$('.failed',this.el).text("");
    },
    setFailedUpload : function(){
	$('.failed',this.el).text("l'import du fichier a échoué.");
    },
    unsetFailedUpload : function(){
	$('.failed',this.el).text("");
    },

    render: function(){

 	var template = _.template(Natbraille.templates.source_upload );
	this.$el.html(template);

	var that = this;
	$('form', this.el).ajaxForm({
	    url : Natbraille.models.instances.sourceCollection.url(),
	    beforeSubmit : function(){
		that.enableSubmit(false);
	    },
	    uploadProgress : function(event,position,total,percentComplete){
		that.setProgress(percentComplete);
	    },
	    success : function(json,statusText,xhr) {
		if (xhr.status == 200){
		    var newInput = new Natbraille.models.definitions.source(json);
		    Natbraille.models.instances.sourceCollection.push(newInput);
		    that.endUpload(true,newInput);
		} else {
		    that.endUpload(false);
		}
	    },
	    error : function(what){
		that.endUpload(false);
	    }
	});


	return this;
    },


});



// new transcription view
Natbraille.views.definitions.fileTranscription = Backbone.View.extend({
    initialize : function(){	
	// ?????
	this.listenTo(Natbraille.states.instance.on("change",this.changedParams,this));
    },
    events: {
	"click .select_config":    "selectConfig",
	"click .select_source":    "selectSource",
	"click .upload_source":    "uploadSource",
	"click .start":    "start",
    },
    start : function(){
	var trRequest = new Natbraille.models.definitions.transcription();
	trRequest.set('sourceDocumentId',Natbraille.states.instance.get('currentSource'));
	trRequest.set('metaConfigurationId',Natbraille.states.instance.get('currentConfig'));
	trRequest.set('logLevel',2);
	$.when(
	    trRequest.save()
	).done(function(newTranscription){
	    Natbraille.models.instances.transcriptionCollection.add(newTranscription);
	    Natbraille.states.instance.set("currentTranscription",newTranscription.id);
	    Natbraille.router.instance.navigate("transcription/"+newTranscription.id, {trigger: true});

	});
    },
    selectConfig : function() {
	Natbraille.router.instance.navigate("configs/choose", {trigger: true});
    },
    selectSource : function() {
	Natbraille.router.instance.navigate("sources/choose", {trigger: true});
    },
    uploadSource : function() {
	Natbraille.router.instance.navigate("source/upload", {trigger: true});
    },
    template : function(data){
	return _.template(Natbraille.templates.file_transcription,data);
    },
    changedParams : function(cgg,oo){
	this.render();
    },
    render: function() {
	this.$el.html(this.template(Natbraille.states.instance));
	return this;
    },
    
});

// transcription detail
Natbraille.views.definitions.transcriptionDetail =  Backbone.View.extend({
    initialize : function(){	
	// listen to changes in current transcription
	this.listenTo(Natbraille.states.instance,"change:currentTranscription",this.transcriptionIdChange);
	this.autoUpdate();
    },
    // debug
    events: {
	"click .update":    "update",
    },
    // debug
    update : function(){
	Natbraille.states.instance.getTranscription().fetch()
	this.autoUpdate();
	
    },
    doAutoUpdate : true,

    // autoupdate fetches the transcription to update state and status log
    // until success or error
    autoUpdate : function(){
	var that = this;
	if (Natbraille.states.instance.getTranscription()){
	    var ftr = Natbraille.states.instance.getTranscription().fetch()
	    if (this.doAutoUpdate){
		$.when(
		    ftr
		).done(function(){
		    if  ((Natbraille.states.instance.getTranscription().get('state') !== "success")&&
			 (Natbraille.states.instance.getTranscription().get('state') !== "error")){
			that.autoUpdate();
		    }
		});
	    }
	}
	
    },
    template : function(data){
	return _.template(Natbraille.templates.transcription_detail,data);
    },
    transcriptionIdChange :function(whate){
	this.stopListening();
	if (Natbraille.states.instance.getTranscription()){
	    // reset this listener and transcription change listener
	    this.listenTo(Natbraille.states.instance,"change:currentTranscription",this.transcriptionIdChange);
	    this.listenTo(Natbraille.states.instance.getTranscription(),"change",this.transcriptionChange);
	}
	this.render();
	this.autoUpdate();

    },
    transcriptionChange : function(chat){
	this.render();
    },
    render: function() {

	var that = this;
	this.$el.html(this.template(Natbraille.states.instance));
	// hack : should be set automatically from dialog-message title by ui-dialog
//	this.$el.dialog({title:this.$("#dialog-message").attr("title")});
	/*
	  if (Natbraille.states.instance.getTranscription()
	  && Natbraille.states.instance.getTranscription().get("destDocumentId")){    
	  this.$el.dialog({buttons : 
	  [{
	  text:"voir le résultat",
	  click:function(){
	  Natbraille.router.instance.navigate("#transcriptions", {trigger: true});
	  }
	  }]
	  });
	  
	  } else {

	  this.$el.dialog({buttons : 
	  [{
	  text:"fermer",
	  click:function(){
	  window.history.back();
	  }
	  }]
	  });

	  }
	*/
	return this;
    },
});

// transcription list item
Natbraille.views.definitions.transcriptionsElement = Backbone.View.extend({
    initialize : function(){	
	this.listenTo(this.model,"destroy",this.removeCool);
    },
    events: {
	"click .detail":    "detail",
	"click .delete":    "delete",
    },
    detail : function(){
	Natbraille.router.instance.navigate("transcription/"+this.model.id, {trigger: true});
    },
    removeCool : function(){
	var that = this;
	$( this.$el ).hide( "fast", function() {
	    that.remove();
	});
    },  
    delete : function(){
	var that = this;
	this.$el.append(_.template(Natbraille.templates.confirm_transcription_deletion,this.model));
	this.$( "#dialog-confirm" ).dialog({
	    modal: true,
	    buttons: {
		Ok: function() {
		    that.model.destroy({wait: true});
		    $( this ).remove();
		},
		Cancel: function() {
		    $( this ).remove();
		}
	    }
	});
    },
    template : function(data){
	return _.template(Natbraille.templates.transcriptions_item,data);
    },
    render: function() {
	this.$el.html(this.template({model:this.model}));
	return this;
    },
});

// list of transcriptions
Natbraille.views.definitions.transcriptions = Backbone.View.extend({
    initialize : function(){	
	this.listenTo(Natbraille.models.instances.transcriptionCollection,"add",this.added);
    },
    template : function(data){
	return _.template(Natbraille.templates.transcriptions,data);
    },
    added : function(transcription){
	this.listenTo(transcription,"change",this.render);
	this.render();
    },
    render: function(state) {
	var that = this;
	this.$el.html(this.template({transcriptions:Natbraille.models.instances.transcriptionCollection.models,filter:state}));
	Natbraille.models.instances.transcriptionCollection.each(function(transcription){    
	    new Natbraille.views.definitions.transcriptionsElement({model:transcription,el:that.$("."+transcription.id)}).render();
	});

	return this;
    },
    filterState : function(state) {
	this.render(state);
    }
});
Natbraille.views.instances = {};

/*
 * Application router.
 * (Backbone)
 */
Natbraille.router = {};
Natbraille.router.definition = Backbone.Router.extend({    
    routes: {
	"configs":                 "configs",    // #help
	"configs/choose":                 "configsChoose",    // #help
	"configs/manage":                 "configsManage",    // #help
	"config/editor":                 "configEditor",    // #help
	"transcription":      "transcription",    // #help
	"transcription/:id":      "transcriptionDetail",    // #help
	"transcriptions":      "transcriptions",    // #help
	"transcriptions/:state":      "transcriptionsFiltered",    // #help
	"source/upload":             "sourceUpload",
	"sources":             "sources",
	"sources/choose":             "sourcesChoose",
	"sources/manage":             "sourcesManage",
	"instant": "instant",
	"help":                 "help",    // #help
	"search/:query":        "search",  // #search/kiwis
	"search/:query/p:page": "search",   // #search/kiwis/p7
	"" : "invite",
    },

    shownone : function() {
	Natbraille.views.instances.transcriptionDetail.$el.dialog("close");
	Natbraille.views.instances.sources.$el.hide();
	Natbraille.views.instances.configs.$el.hide();
	Natbraille.views.instances.transcriptions.$el.hide();
	Natbraille.views.instances.sourceUpload.$el.hide();
	Natbraille.views.instances.fileTranscription.$el.hide();
	Natbraille.views.instances.instantTranscription.$el.hide();	
	Natbraille.views.instances.configEditor.$el.hide();
    },
    showAndFocus : function(view){
//	Natbraille..$el.show().focus();
    },
    invite : function() {
	this.transcription();
    },
    transcription : function(){
	this.shownone();
	Natbraille.views.instances.fileTranscription.$el.show();
    },
    sourceUpload : function(){
	this.shownone();
	Natbraille.views.instances.sourceUpload.$el.show().focus();
    },
    sources : function(){
	this.shownone();
	Natbraille.views.instances.sources.$el.show().focus();
    },
    sourcesChoose : function(){
	Natbraille.views.instances.sources.setButtons({selectButton:true,deleteButton:false});
	this.sources();
    },
    sourcesManage : function(){
	Natbraille.views.instances.sources.setButtons({selectButton:false,deleteButton:true});
	this.sources();
    },
    configs : function() {
	this.shownone();
	Natbraille.views.instances.configs.$el.show();
    },
    configsChoose : function(){
	Natbraille.views.instances.configs.setButtons({selectButton:true,deleteButton:false});
	this.configs();
    },
    configsManage : function(){
	Natbraille.views.instances.configs.setButtons({selectButton:false,deleteButton:true});
	this.configs();
    },
    configEditor : function(){
	this.shownone();
	Natbraille.views.instances.configEditor.$el.show().focus();
    },

    transcriptionDetail : function(id){
	Natbraille.states.instance.set('currentTranscription',id);
	Natbraille.views.instances.transcriptionDetail.$el.dialog("open");
    },
    transcriptions : function(){
	this.shownone();
	Natbraille.views.instances.transcriptions.$el.show().focus();
	Natbraille.views.instances.transcriptions.filterState();
    },
    transcriptionsFiltered : function(state){
	this.transcriptions();
	Natbraille.views.instances.transcriptions.filterState(state);
    },
    help: function() {
	//	$(".configs").empty();
	alert("help");
    }, 
    instant : function(){
	this.shownone();
	Natbraille.views.instances.instantTranscription.$el.show().focus();
    },
    
    search: function(query, page) {
	alert("ooooooo"+query);
    }
});

/*
 * Startup.
 */
Natbraille.start = function(){


    // initialize underscore template root variable 
    _.templateSettings.variable = "rc";

    // create models and collections
    Natbraille.models.instances.configformat = new Natbraille.models.definitions.configformat();
    Natbraille.models.instances.user = new Natbraille.models.definitions.user();
    Natbraille.models.instances.configNoticeCollection = new Natbraille.models.definitions.configNoticeCollection();
    Natbraille.models.instances.configCollection = new Natbraille.models.definitions.configCollection();
    Natbraille.models.instances.sourceCollection = new Natbraille.models.definitions.sourceCollection();
    Natbraille.models.instances.transcriptionCollection = new Natbraille.models.definitions.transcriptionCollection();
    Natbraille.models.instances.resuCollection = new Natbraille.models.definitions.resuCollection();
    Natbraille.states.instance = new Natbraille.states.definition({
	currentConfig :0,
	currentSource : 0,
	currentTranscription : 0,
	editedConfig :  new Natbraille.models.definitions.config(),
	editedOptions : 0,
    });    
    Natbraille.views.instances.user = new Natbraille.views.definitions.user();
    
    $.when(
	// fetch static models
	Natbraille.models.instances.configformat.fetch(),
	Natbraille.models.instances.user.fetch()
    ).done(function(){
	$.when(
	    // now "user" is set. get config and sources
	    Natbraille.models.instances.configNoticeCollection.fetch(),
	    Natbraille.models.instances.sourceCollection.fetch()
	).done(function(){
	    $.when(
		// now config and source collection are set, get transcriptions
		Natbraille.models.instances.transcriptionCollection.fetch()
	    ).done(function(){

		// create views
		Natbraille.views.instances.fileTranscription = new Natbraille.views.definitions.fileTranscription({el:$(".transcription")}).render();
		Natbraille.views.instances.configs = new Natbraille.views.definitions.configs({el:$(".configs")}).render();
		Natbraille.views.instances.sources  = new Natbraille.views.definitions.sources({el:$(".sources")}).render();		
		Natbraille.views.instances.transcriptionDetail = new  Natbraille.views.definitions.transcriptionDetail({
		    el:".transcription_detail"
		});
		Natbraille.views.instances.transcriptionDetail.$el.dialog({
		    modal: true,		    
		    buttons: {
			Close: function() {
			    $( this ).dialog("close");
			    window.history.back();
			},
		    }
		});		
		Natbraille.views.instances.transcriptionDetail.render();
		Natbraille.views.instances.transcriptionDetail.$el.dialog("close");

		Natbraille.views.instances.transcriptions = new Natbraille.views.definitions.transcriptions({
		    el:".transcriptions"
		}).render();


		Natbraille.views.instances.sourceUpload  = new Natbraille.views.definitions.sourceUpload({el:".source_upload"}).render();
		Natbraille.views.instances.instantTranscription = new Natbraille.views.definitions.instantTranscription({el:".instant"}).render();
		
		Natbraille.views.instances.configEditor = new Natbraille.views.definitions.configEditor({el:".config_editor"}).render();

		// create router
		Natbraille.router.instance = new Natbraille.router.definition();


		// start application router
		Backbone.history.start(/*{pushState: true}*/);
	    });
	});
    });
}

/*
  Natbraille.models.instances.configformat.on('change:color',function(model,color){
  $("body").append("<p>"+color+"</p>")
  });
  Natbraille.models.instances.configformat.set({color:"Htecolore"});
*/
