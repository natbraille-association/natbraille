Natbraille.templates.tmpl_config_option_ac_item = $("#tmpl_config_option_ac_item").html();
Natbraille.templates.tmpl_config_option_form = $("#tmpl_config_option_form").html();

Natbraille.views.definitions.configEditorAutocomplete = Backbone.View.extend({
    render : function(){
	this.activeAutocomplete();
    },
    activeAutocomplete : function() {
	this.$el.autocomplete({
	    minLength: 0,
	    position : { my: "left top", at: "left bottom", collision: "none", /*of : "body"*/},
	    
	    // get options for given search
	    source: function(search,returncb){
		var lcsearchterm = search.term.toLowerCase();
		var match = [];
		_.each(Natbraille.models.instances.configformat.attributes,function(v,optionName){
		    var hidden = Natbraille.models.instances.configformat.get(optionName).hidden;
		    if (!hidden){
			var setValue = Natbraille.states.instance.getEditedConfig().get(optionName);
			if ((optionName.toLowerCase().search(lcsearchterm)>-1)
			    ||(I18n.optionTr(optionName).toLowerCase().search(lcsearchterm)>-1)
			    ||(setValue && (setValue.toLowerCase().search(lcsearchterm)>-1))){
			    var el = {
				label:I18n.optionTr(optionName),
				value:"value["+optionName+"]",
				name : optionName
			    };
			    if (setValue){
				match.unshift(el);
			    } else {
				match.push(el);
			    }
			}
		    }
		});
		returncb(match);
	    },
	    // when an item is focused in the result list
	    focus: function( event, ui ) {
		$(".config-autocomplete" ).val( ui.item.label );
		return false;
	    },
	    // when an item is selected in the result list
	    select: function( event, ui ) {
		$(".config-autocomplete").val( ui.item.label );
		Natbraille.states.instance.set('editedOptions',ui.item);
		return false;
	    }
	}).autocomplete( "instance" )._renderItem = function( ul, item ) {
	    // render result list item
	    var li = _.template(Natbraille.templates.tmpl_config_option_ac_item,{
		editedConfig : Natbraille.states.instance.getEditedConfig(),
		configformat : Natbraille.models.instances.configformat,
		item : item,
	    });
	    return $(li).appendTo(ul);
	};
    }
});

Natbraille.views.definitions.configEditorOptionForm = Backbone.View.extend({
    initialize : function(){
	this.listenTo(Natbraille.states.instance,"change:editedOptions",this.editedOptionsChange);
    },
    events : {
	"click .set_option" : "setOption",
	"click .delete_option" : "deleteOption",
	"click .cancel_option" : "cancelOption",
    },
    uiValue : function(){
	// is a select option
	if (this.$("select.option_value").length){
	    var itemNum = this.$(".option_value").val();
	    var optionName = this.$("select.option_value").attr("id");
	    var possibleValues = Natbraille.models.instances.configformat.get(optionName).possibleValues;
	    if (possibleValues){
		return possibleValues[itemNum];
	    }
	} else if (this.$("input[type=radio][value=true]").length){
	    return ""+this.$("input[type=radio][value=true]").prop("checked");
	} else {
	    return this.$(".option_value").val();
	}
    },
    setOption :function() {
	var optName = Natbraille.states.instance.getEditedOptions().name;
	Natbraille.states.instance.getEditedConfig().set(optName,this.uiValue());
	Natbraille.states.instance.set('editedOptions',0);
    },
    cancelOption :function() {
	Natbraille.states.instance.set('editedOptions',0);
    },
    deleteOption :function() {
	var optName = Natbraille.states.instance.getEditedOptions().name;
	Natbraille.states.instance.getEditedConfig().unset(optName);
	Natbraille.states.instance.set('editedOptions',0);
    },
    editedOptionsChange : function(editedOptions){
	this.render();
    },
    template : function(data){
	return _.template(Natbraille.templates.tmpl_config_option_form,data);
    },
    render: function() {
	this.$el.html(this.template({
	    configformat : Natbraille.models.instances.configformat,
	    editedConfig : Natbraille.states.instance.getEditedConfig(),
	    editedOptions : Natbraille.states.instance.getEditedOptions(),
	}));
	return this;
    },    
});

Natbraille.views.definitions.configEditor = Backbone.View.extend({
    initialize : function(){
	this.listenTo(Natbraille.states.instance,"change:currentConfig",this.currentConfigChange);
	this.listenTo(Natbraille.states.instance.getEditedConfig(),"change",this.editedConfigChange);
    },
    events : {
	"click .choose_new" : "reset",
	"click .choose_new_from_current" : "resetFromCurrent",
//	"click .savenew" : "savenew",
	"click .replace" : "replace",
	"click .ok" : "ok",
	"click .cancel" : "cancel",
    },
    reset : function(){	
	Natbraille.states.instance.getEditedConfig().clear();
	Natbraille.states.instance.getEditedConfig().set("CONFIG_NAME","***");
    },
    resetFromCurrent : function(){
	this.reset();
	// copy every option from model
	_.each(Natbraille.models.instances.configformat.attributes,function(optModel,name){
	    // defined in base configuration
	    if (Natbraille.states.instance.getConfiguration().get(name)){
		// not having the default value
		if (Natbraille.states.instance.getEditedConfig().get(name) !== optModel.defaultValue){
		    Natbraille.states.instance.getEditedConfig().set(name,Natbraille.states.instance.getConfiguration().get(name));
		}
	    }
	});
	Natbraille.states.instance.getEditedConfig().set("CONFIG_NAME",Natbraille.states.instance.getConfiguration().get("CONFIG_NAME")+"#");
    },
    ok : function(){
	// save
	this.save({replace:true,tell:true, doAfter : function(){
	    // reset edited conf
	    Natbraille.states.instance.getEditedConfig().clear();
	}});
    },
    cancel : function(){
	alert("cancel");
	var lastId = 	Natbraille.states.instance.getEditedConfig().get("id");
	// reset edited conf
	Natbraille.states.instance.getEditedConfig().clear();
	if (lastId){
	    // remove saved conf if so
	    var trashConfig = new Natbraille.models.definitions.config({id:lastId});
	    $.when(
		trashConfig.destroy({wait: true})
	    ).done(function(){
//		console.error("removed");
	    }); 
	}
    },
/*
    savenew : function(){
	// save without replacing
	this.save({replace:false,tell:true});
    },
*/
    replace : function(){
	// save and remove previous version
	this.save({replace:true,tell:false});
    },
    save : function(options){

	if (_.keys(Natbraille.states.instance.getEditedConfig().attributes).length == 0){
	    return;
	}
	var lastId = 	Natbraille.states.instance.getEditedConfig().get("id");
	var newConfig = new Natbraille.models.definitions.config();
	// copy every option from model
	_.each(Natbraille.models.instances.configformat.attributes,function(optModel,name){
	    // defined in edited configuration
	    if (Natbraille.states.instance.getEditedConfig().get(name)){
		// not having the default value
		if (Natbraille.states.instance.getEditedConfig().get(name) !== optModel.defaultValue){
		    newConfig.set(name,Natbraille.states.instance.getEditedConfig().get(name));
		}
	    }
	});
	$.when(
	    // save conf
	    newConfig.save()
	).done(function(serverConf){
//	    console.error("saved",serverConf);
	    Natbraille.states.instance.getEditedConfig().set("id",serverConf.id);
	    if ((options.replace)&& (lastId)){
		    // to remove previous conf
		    var trashConfig = new Natbraille.models.definitions.config({id:lastId});
//		    console.error("remove!",trashConfig);
		    $.when(
			trashConfig.destroy()
		    ).done(function(){
//			console.error("removed");
			if (options.tell){
			    // add new config to others
//			    console.error("tell1");
			    $.when(
				Natbraille.models.instances.configNoticeCollection.fetch()
			    ).done(function(){
//				console.error("told1");
				if (options.doAfter){
//				    console.error("execute next1");

				    // execute next
				    options.doAfter();
				}
			    });
			}
		    });
	    } else {
		if (options.tell){
		    // add new config to others
//		    console.error("tell2");
		    $.when(
			Natbraille.models.instances.configNoticeCollection.fetch()
		    ).done(function(){
//			console.error("told2");
			if (options.doAfter){
			    // execute next
//				    console.error("execute next2");
			    options.doAfter();
			}
		    });
		} else {
		    if (options.doAfter){
		    // execute next
//			console.error("execute next3");
		    options.doAfter();
		    }

		}
	    }
	});
    },
    currentConfigChange : function(what){
	this.render();
    },
    editedConfigChange : function(what){
	//	console.error("diteconf",what);
	//	console.error("diteconf",_.keys(what.attributes).length);
	this.render();
    },
    template : function(data){
	return _.template(Natbraille.templates.config_editor,data);
    },
    render: function() {
	this.$el.html(this.template({
	    baseConfig : Natbraille.states.instance.getConfiguration(),
	    editedConfig : Natbraille.states.instance.getEditedConfig(),
	    editedOptions : Natbraille.states.instance.getEditedOptions(),
	}));
	new Natbraille.views.definitions.configEditorOptionForm({el:this.$(".config-option")}).render();
	new Natbraille.views.definitions.configEditorAutocomplete({el:this.$(".config-autocomplete")}).render();
	return this;
    },
});
