var InstantTranscriptor = {
    
    urlRoot :  function() {
	return Natbraille.configuration.restRoot+'/private/~'+Natbraille.models.instances.user.get('name')+'/instant';
    },


    xhtml : function(){
	return $.wymeditors(0).xhtml();
    },
    
    startEditor : function (){
	//    $('#my-wymeditor').wymeditor();
	var that = this;
	var options = {
	    html : "<h1>Mon Document</h1><h2>Mon titre de niveau 2</h2><p>super, c'est de l'abrégé</p>"
		+ "<p>",
	    lang : 'fr',
            skin: 'compact',
            postInit: function() {
		$("#instant_tobraille").click(function(){
		    that.translate();
		});
            },
	};
	$("#my-wymeditor").wymeditor(options);
//	console.error("editor started",$("#my-wymeditor").wymeditor(options));
    },
    
    pretranslate : function(){
	$("#instant_tobraille").attr('disabled','disabled');
    },
    posttranslate : function(){
	$("#instant_tobraille").removeAttr('disabled');	
    },


    postdata : function(){

	var doctype = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">";

	var docStartElement = "<html><head></head><body>\n";
	var xhtml = this.xhtml()
	var docEndElement = "</body></html>\n";

	var data = JSON.stringify({
	    text : doctype+docStartElement+xhtml+docEndElement
	});
	console.log(data);
	return data;
    },

    autorefresh : function(val){
	if (val === undefined){
	    return $("#instant_autorefresh").prop('checked');
	} else {
	    $("#instant_autorefresh").prop('checked', val);
	}
    },
    brailletohtml : function(braille){
	return "<pre>"+braille+"</pre>";
    },

    translate : function(){
	console.error("translare");
	this.pretranslate()
	var that = this;
	$.ajax({
	    type: "POST",
	    url: that.urlRoot(),
	    data: that.postdata(),
	    contentType: 'application/json',
	    dataType: 'json',
	    success: function(response){
		$("#instant_braille").empty();
		console.log(that.brailletohtml(response.text));
		$("#instant_braille").append(that.brailletohtml(response.text));
		setTimeout(function(){
		    that.posttranslate();
		    if (that.autorefresh()){
			that.translate()
		    }
		},1000);
	    },
	    error: function(response){
		alert("erreur lors de la transcription !");
		console.log(response);
		that.posttranslate();
		that.autorefresh(false);
	    }
	});
    },


}

Natbraille.views.definitions.instantTranscription = Backbone.View.extend({

    template : function(data){
	return _.template(Natbraille.templates.instant_transcription,data);
    },
    render: function() {
	this.$el.html(this.template({}));
	InstantTranscriptor.startEditor();
	return this;
    },
});
