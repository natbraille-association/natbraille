//
// to synchronize end of multiple gets
//
// var myf = new Fetcher({
//     urls : ['url1','utrl2',...'urln'],
//     success : function(content){
// 	// everything is fetched
// 	// content is an associative array, url => urlcontent
//     },
//     fetched : function(url,content){
// 	// one element is fetched
//     }
// }).fetch();

 function Fetcher(params){
    
    var self = new Object({
	urls : params.urls,
	content : new Object(),
	fetched_cb : params.fetched,
	success_cb : params.success,
	done : 0,
    });
    

    self.success = function(){
	self.success_cb(self.content);
    };

    self.oneSuccess = function(Curl,Ccontent){
	self.fetched_cb(Curl,Ccontent);
	self.content[Curl] = Ccontent;
	self.done++;
	if (self.done == self.urls.length){
	    self.success();
	}

    };
    
    self.fetch = function() {

	for (var i=0;i<self.urls.length;i++){
	    // must wrap inside a function just to create a scope
	    // in order for i not to be overriden
	    var a = function iterate(i){
		var oneUrl = self.urls[i];
		var p = i;
		$.get(oneUrl,function(oneUrlContent){
		    return self.oneSuccess(oneUrl,oneUrlContent);
		});
	    }
	    a(i);
	}
    };

    return self;
}

