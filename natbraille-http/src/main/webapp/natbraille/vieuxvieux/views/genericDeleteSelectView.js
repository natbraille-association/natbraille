var SomethingOptionView = Backbone.View.extend({

    templateId : "input_option_template",
    tagName: 'option', // name of tag to be created
  
    initialize: function(){
	_.bindAll(this, 'render', 'unrender');
	this.model.bind('change', this.render);
	this.model.bind('remove', this.unrender);
    },
    templateAttributes : function(){
	return this.model.attributes;
    },
    getHtml : function(){
	var template_html = $('#'+this.templateId).html();
 	var template = _.template( template_html, this.templateAttributes() );
	return template;
    },
    render: function(){
	var template = this.getHtml();
	this.$el.html(template);
	this.$el.attr('value',this.model.get('id'));
	return this;
    },

    unrender: function(){
  	$(this.el).remove();
    },   
});

var SomethingDeleteView =  Backbone.View.extend({

    templateId : "input_delete_template",
    events : {
	"click .delete" : "deleteInput" ,
	"click .deleteAll" : "deleteAllInput" ,
    },
    deleteInput : function(){
	var id = this.selectedId();//$('select option',this.el).filter(":selected").attr("value");
	var item = this.model.get(id);
	var that = this;
	item.destroy({
	    wait: true,
	    success : function(model,response) {
		that.model.remove(item);
	    }
	});
    },
    selectedId : function(){
	return 	$('select option', this.el).filter(":selected").attr("value");
    },
    deleteAllInput : function(){
	var removeList = [];

	_(this.model.models).each(function(item){
	    removeList.push(item);
	}, this);

	var that = this;
	_(removeList).each(function(item){
	    item.destroy({
//		wait: true,
		success : function() {
//		    that.model.remove(item);
		},
		error : function() {
//		    alert('error');
		}
	    });
	}, this);


    },
    initialize: function(){
	_.bindAll(this, 'render',  'appendItem'); // every function that uses 'this' as the current object should be in here
	this.model.bind('add', this.appendItem); // collection event binder
	this.render();
    },
    render: function(){
	var template_html = $('#'+this.templateId).html();
 	var template = _.template( template_html, this.model.attributes );
	this.$el.html(template);
	var self = this;
	_(this.model.models).each(function(item){ // in case collection is not empty
            self.appendItem(item);
	}, this);
	return this;
    },
    newOptionViewOptions : function(item) {
	var options = {
            model: item,
	};
	return options
    },
    appendItem: function(item){
	var somethingOptionView = new this.somethingViewProto(this.newOptionViewOptions(item));
	$('select', this.el).append(somethingOptionView.render().el);
    }  

});

