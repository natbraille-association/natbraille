var ResuTextView = Backbone.View.extend({
    templateId : "resu_text_template",
    events : {
	'click button.toggleView' : 'toggleViewBraille' ,
    },
    el : 'span',

    showBraille : false,
    loaded : false,

    initialize: function(){
	_.bindAll(this, 'load','render'); 
	this.model = new ResuTextModel({resu: this.options.resu});
	this.listenTo(this.model, "change", this.render);
	this.render();
    },

    toggleViewBraille : function(){
	if (this.showBraille == false){
	    this.showBraille = true;
	    if (this.loaded == false){
		this.load();
	    }
	} else {
	    this.showBraille = false;
	    this.render();
	}
    },

    load : function(){
	var that=this;
	this.model.fetch({
	    success:function(){that.render()},
	});
    },

    render : function(){	
	var pass = {};
	pass = {
	    showBraille : this.showBraille,
	    resu : this.options.resu.attributes,
	    resuText : this.model.attributes,
	    downloadUrl : this.model.getDownloadUrl(),
	};
	var template_html = $('#'+this.templateId).html();
 	var template = _.template( template_html, pass );
	this.$el.html(template);
    }
});
