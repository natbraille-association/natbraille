/*
{
    "FORMAT_OUTPUT_BRAILLE_TABLE": "TbFr2007",
    "LAYOUT_NUMBERING_MODE": "'nn'",
    "FORMAT_LINE_LENGTH": "32",
    "LAYOUT_PAGE_EMPTY_LINES_MODE": "5",
    "CONFIG_DESCRIPTION": "(codage UTF8 : Esytime...), 32 car. par ligne",
    "CONFIG_NAME": "Plage braille 2"
}
*/
var Config = Backbone.Model.extend({
    urlRoot: function () {
        return NatbrailleConfig.restRoot+'/private/~'+Natbraille.userName()+'/metaconfig';
    },
});
/*
[{
    "metadata": {
        "creationDate": "Sun Sep 22 19:25:56 CEST 2013",
        "publicUri": null,
        "author": "natbraille"
    },
    "id": "523f2824e4b095b20061c8e5",
    "name": "Plage braille 2",
    "info": "(codage UTF8 : Esytime...), 32 car. par ligne"
}, {
    "metadata": {
        "creationDate": "Sun Sep 22 19:25:56 CEST 2013",
        "publicUri": null,
        "author": "natbraille"
    },
    "id": "523f2824e4b095b20061c8e6",
    "name": "Plage braille 1",
    "info": "(codage ANSI : Braille Sense, Iris...), 32 car. par ligne"
}, {
    "metadata": {
        "creationDate": "Sun Sep 22 19:25:56 CEST 2013",
        "publicUri": null,
        "author": "natbraille"
    },
    "id": "523f2824e4b095b20061c8e7",
    "name": "Embossage interligne simple",
    "info": "Pour l'embossage sur une embosseuse en code U.S. (32x28), numerotation des pages "
}, {
    "metadata": {
        "creationDate": "Sun Sep 22 19:25:56 CEST 2013",
        "publicUri": null,
        "author": "natbraille"
    },
    "id": "523f2824e4b095b20061c8e8",
    "name": "Embossage interligne double",
    "info": "Pour l'embossage sur une embosseuse en code U.S. (32x28), numerotation des pages "
}];
*/

var ConfigColl = Backbone.Collection.extend({
    model: Config,
     url : function(){
	return NatbrailleConfig.restRoot+'/private/~'+Natbraille.userName()+'/metaconfig/';
    }, 
});
