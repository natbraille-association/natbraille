var NewConfig = Backbone.Model.extend({

    hasFormat : false,
    hasConfig : false,

    checkReady : function(){
	if ((this.hasFormat)&&(this.hasConfig)){
	    this.set('ready',true);
	} else {
	    this.set('ready',false);
	}
    },
    defaultValues : function(){
	var defaultValues = {};
	_.each(this.getFormat().attributes,function(infos,name){
	    defaultValues[name] = infos.defaultValue;
	});
	return defaultValues;
	
    },
    incrVersion : function(desc){
	var matched = desc.match(/(.*#)(\d+)/);
	if (matched != undefined){
	    desc = matched[1]+""+(1+matched[2]*1.0);
	} else {
	    desc = desc+'#1'
	}
	return desc; 
    },
    newConfig : function(attributes){
	var newConfig = new Config();
	newConfig.set(this.defaultValues());
	
	if (attributes != undefined){
	    newConfig.set(attributes);
	    newConfig.unset('id');	    
	} else {
	    if (this.getConfig() != undefined){
		var lastAttributes = this.getConfig().attributes;
		newConfig.set(lastAttributes);
		newConfig.unset('id');	    
	    }
	}
	var namenplus1 = this.incrVersion(newConfig.get('CONFIG_NAME'));
	newConfig.set('CONFIG_NAME',namenplus1);
	    
	this.set('config',newConfig);
	this.hasConfig = true;
	this.checkReady();
	
    },
    initialize : function(){
//	this.set('ready',false);

	var configFormat = new ConfigFormat();
	var that = this;

	configFormat.fetch({
	    success : function(){
		that.hasFormat = true;	
		that.checkReady();
		that.newConfig();
	    }
	});
	this.set('format',configFormat);
    },
  
    setTemplateConfig : function(config){
	this.hasConfig = false;
	this.checkReady();

	this.set('templateConfig',config);
	this.newConfig(config.attributes);
	

    },
    getTemplateConfig : function(){
	return this.get('templateConfig');
    },
    getConfig : function(){
	return this.get('config');
    },
    getFormat : function(){
	return this.get('format');
    },
    save : function(options){
	this.hasConfig = false;
	this.checkReady();
	that = this;
	this.getConfig().save(this.getConfig().attributes,{
	    success: function(xhr){
		that.newConfig();
	    	options.success(xhr);
	    },
	    error : function(xhr){
	    	options.error(xhr);
	    }
	    
	});
    },

});
