var Natbraille = {
    user : new User(),
    setUser : function(natUser){
	this.user = natUser;
	return this;
    },
    getUser : function(){
	return this.user;
    },
    userName : function(){
	return this.user.get('name');
    },
  
};


var AppRouter = Backbone.Router.extend({
    
    routes: {
	"convert" : "convert",
	"upload" : "upload",
	"newconf" : "newconf",
	"clean" : "clean",
	"result_:id" : "resultId",
	"result" : "result",
	"about" : "about",
	"menulitt" : "menulitt",
        "*actions": "defaultRoute" // matches http://example.com/#anything-here
    },

    showonly : function(id){
	if (this.removeDomListener != undefined){
	    this.removeDomListener();
	}
//	$('div#menulitt').hide();
	$('div#convert').hide();
	$('div#upload').hide();
	$('div#newconf').hide();
	$('div#result').hide();
	$('div#about').hide();
	$('div#clean').hide();
	$('div#menubar a').removeClass("selectedUrl");
	$('div#tous div#'+id).show("");

	$("div#menubar a[href|='#"+id+"']").addClass("selectedUrl");

    },
    menulitt : function(){
	this.showonly('menulitt');
    },
    upload : function(){
	this.showonly('upload');
    },
    convert : function(){
	this.showonly('convert');
//	$('div#result').show();
    },
    newconf : function(){
	this.showonly('newconf');
    },
    clean : function(){
	this.showonly('clean');
    },
    
    
    resultId : function(id){
	this.showonly('result');

//	$('div#convert').show();
	$('.transcription_summary').hide();

	var elid = "result_"+id;
	$('#'+elid).show();

	var liste = $('#transcription_list');
	var that = this;
	function insertCallBack(event){
	    var insert = $(event.target);
	    if (insert.hasClass('transcription_summary')){
		if (insert.attr('id') == elid){
		    insert.show();
			that.navigate("#"+elid, {trigger: true});
		}  else {
		    insert.hide();
		}
	    }
	}

	liste.get(0).addEventListener("DOMNodeInserted",insertCallBack);
	this.insertCallBack = insertCallBack;
	this.removeDomListener = function(){
//	    console.error("xoxoxoxooxox");
	    $('#transcription_list').get(0).removeEventListener("DOMNodeInserted",this.insertCallBack);
	}

	
    },

    result : function(){
	this.showonly('result');
//	$('div#convert').show();
	$(".transcription_summary").show();
    },
    about : function(){
	this.showonly('about');
    },
    defaultRoute : function(){
	this.convert();
    }
});



function natbrailleroutes(){

    var appRouter = new AppRouter;
    Backbone.history.start();

    $('#clean button').click(function(){
	alert("confirmez la suppression de ...");
    });
    
    $('#convert button').click(function(){
     	appRouter.navigate("#result", {trigger: true});
    });

    
   // var natUser = new User();	    
    var loggedUserView = new ConnectedUserView({model:Natbraille.getUser(), el:$('#connectedBox')});

    var inputColl = new InputColl();
 
    var inputSelectOptionView = new InputSelectView({model:inputColl,el:$("#input_select_option") });
    var inputUploadView = new InputUploadView({model:inputColl,el:$("#input_upload"), router:appRouter });
    var inputDeleteView = new InputDeleteView({model:inputColl,el:$("#input_delete") });

    var configColl = new ConfigColl();
 
    var configSelectOptionView = new ConfigSelectView({model:configColl,el:$("#config_select_option") });
    var configDeleteView = new ConfigDeleteView({model:configColl,el:$("#config_delete") });

    var resuColl = new ResuColl();
    var resuDeleteView = new ResuDeleteView({model:resuColl,el:$("#resu_delete") });
 
    var transcriptionColl = new TranscriptionColl();
    var transcriptionRequestView = new TranscriptionRequestView(
     	{model:transcriptionColl,el:$("#transcription_request"),
    	 inputView:inputSelectOptionView, 
	 configView:configSelectOptionView,
	 router:appRouter
	});

    var transcriptionListView = new TranscriptionListView({
	model:transcriptionColl,
	el:$("#transcription_list"),
    	inputColl:inputColl, 
	configColl:configColl,
	resuColl:resuColl,
    });

    var transcriptionDeleteView = new TranscriptionDeleteView({
	model:transcriptionColl,el:$("#transcription_delete"), 
    	inputColl:inputColl, 
	configColl:configColl,
    });


    Natbraille.getUser().fetch({
	success : function(){
	    transcriptionColl.fetch();
	    inputColl.fetch();	    
	    configColl.fetch();
	    resuColl.fetch();
	    
	    var newConfigView = new NewConfigView({
     		el : $("#newconf_container"),
     		configColl : configColl,
	    });
	}
    });
  
    
 }