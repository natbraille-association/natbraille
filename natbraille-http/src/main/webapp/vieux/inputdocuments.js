/**
  * model of inputdoc
  */
var InputDoc = Backbone.Model.extend({
 /*   url :  function() {
	return '/natbraille-http/rest/private/~vivien/source/'+this.id;
    },
*/
    urlRoot :  function() {
	return '/natbraille-http/rest/private/~vivien/source';
    },

    resume : function(){
	return this.get("displayName");
    }
});

/**
 * collection of inputdoc
 */
var InputDocColl = Backbone.Collection.extend({
    model : InputDoc,
    initialize : function(){
    },
    url :  function() {
	return '/natbraille-http/rest/private/~vivien/source/';
    },
});

var InputDocView = Backbone.View.extend({
    tagName: 'li', // name of tag to be created
    events: {
	'click span.swap':  'swap',
	'click span.delete': 'remove'
    },
    initialize: function(){
	_.bindAll(this, 'render', 'unrender', 'swap', 'remove');
	this.model.bind('change', this.render);
	this.model.bind('remove', this.unrender);
    },
    render: function(){
	
	// Compile the template using underscore
 	var template = _.template( $("#inputdocinfo_template").html(), this.model.attributes );
	
	$(this.el).html(template);
	return this;
    },

    unrender: function(){
	$(this.el).remove();
    },

    swap: function(){
	// var swapped = {
        //     part1: this.model.get('part2'),
        //     part2: this.model.get('part1')
	// };
	// this.model.set(swapped);
    },

    remove: function(){
	this.model.destroy();
    }
});

var InputDocCollView =  Backbone.View.extend({
    events: {
//	'click button#add': 'addItem'
    },
    initialize: function(){
	_.bindAll(this, 'render', 'addItem', 'appendItem'); // every function that uses 'this' as the current object should be in here

	this.model = new InputDocColl();
	this.model.bind('add', this.appendItem); // collection event binder
	this.model.fetch();	
	this.render();
    },
    render: function(){
	var self = this;
	$(this.el).append("<button id='add'>Add list item</button>");
	$(this.el).append("<ul></ul>");
	_(this.model.models).each(function(item){ // in case collection is not empty
            self.appendItem(item);
	}, this);
    },
    addItem: function(){

//	var item = new Item();
//	item.set({
//            part2: item.get('part2') + this.counter // modify item defaults
//	});
//	this.collection.add(item);
    },
    appendItem: function(item){
	var inputDocView = new InputDocView({
            model: item
	});
	$('ul', this.el).append(inputDocView.render().el);
    }  
});

