/*
 * NatBraille Http Server - An universal Translator over http
 * Copyright (C) 2013 Vivien Guillet
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


package org.natbraille.http.ws;


import jakarta.inject.Inject;
import jakarta.servlet.ServletContext;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.SecurityContext;
import org.natbraille.http.ws.formats.out.FoUserDocument;

@Path("/private/user")
public class WsUserTest {

	@Inject
	private ServletContext context;

	@Inject
	private SecurityContext securityContext;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public FoUserDocument testit(){
//		Natd natd = Natd.getFromContext(context);
//		NatLibrary natLibrary = natd.getNatLibrary();		
//		natLibrary.checkUser(securityContext,ppUser);
//		System.err.println("usere principal"+securityContext.getUserPrincipal());
		FoUserDocument resu = new FoUserDocument(securityContext.getUserPrincipal().getName());
		return resu;
	}

}
