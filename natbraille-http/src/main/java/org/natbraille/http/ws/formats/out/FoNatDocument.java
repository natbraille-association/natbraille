/*
 * NatBraille Http Server - An universal Translator over http
 * Copyright (C) 2013 Vivien Guillet
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.natbraille.http.ws.formats.out;

import org.natbraille.http.dbDocument.DbNatDoc;

public class FoNatDocument extends AbstractFoHavingMetadata  {



	String date;
	String displayName;
	String contentType;
	String charset;
	String uri;
	String id;
	String brailleTable;
	String brailleEncoding;
	
	
	/**
	 * @return the brailleTable
	 */
	public String getBrailleTable() {
		return brailleTable;
	}


	/**
	 * @param brailleTable the brailleTable to set
	 */
	public void setBrailleTable(String brailleTable) {
		this.brailleTable = brailleTable;
	}


	/**
	 * @return the brailleEncoding
	 */
	public String getBrailleEncoding() {
		return brailleEncoding;
	}


	/**
	 * @param brailleEncodig the brailleEncoding to set
	 */
	public void setBrailleEncoding(String brailleEncoding) {
		this.brailleEncoding = brailleEncoding;
	}


	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}


	/**
	 * @return the uri
	 */
	public String getUri() {
		return uri;
	}


	/**
	 * @param uri the uri to set
	 */
	public void setUri(String uri) {
		this.uri = uri;
	}


	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}


	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}


	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		return displayName;
	}


	/**
	 * @param displayName the displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}


	/**
	 * @return the contentType
	 */
	public String getContentType() {
		return contentType;
	}


	/**
	 * @param contentType the contentType to set
	 */
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}


	/**
	 * @return the charset
	 */
	public String getCharset() {
		return charset;
	}


	/**
	 * @param charset the charset to set
	 */
	public void setCharset(String charset) {
		this.charset = charset;
	}

	public FoNatDocument(DbNatDoc doc) {
		super(doc);
		setUri(doc.buildURI());
		setDate(doc.getMetadataDocument().getCreationDate().toString());
		setDisplayName(doc.getOriginalFileName());
		setContentType(doc.getMimeType());
		setCharset(doc.getCharset());		
		setId(doc.getId());
		setBrailleEncoding(doc.getBrailleEncoding());
		setBrailleTable(doc.getBrailleTable());
	}

}
