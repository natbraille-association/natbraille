/*
 * NatBraille Http Server - An universal Translator over http
 * Copyright (C) 2013 Vivien Guillet
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.natbraille.http.ws.formats.in;



public class FoTranscriptionRequest {
	String sourceDocumentId;
	String metaConfigurationId; 
	int logLevel;

	/**
	 * @return the sourceDocumentId
	 */
	public String getSourceDocumentId() {
		return sourceDocumentId;
	}
	/**
	 * @param sourceDocumentId the sourceDocumentId to set
	 */
	public void setSourceDocumentId(String sourceDocumentId) {
		this.sourceDocumentId = sourceDocumentId;
	}
	/**
	 * @return the metaConfigurationId
	 */
	public String getMetaConfigurationId() {
		return metaConfigurationId;
	}
	/**
	 * @param metaConfigurationId the metaConfigurationId to set
	 */
	public void setMetaConfigurationId(String metaConfigurationId) {
		this.metaConfigurationId = metaConfigurationId;
	}
	/**
	 * @return the logLevel
	 */
	public int getLogLevel() {
		return logLevel;
	}
	/**
	 * @param logLevel the logLevel to set
	 */
	public void setLogLevel(int logLevel) {
		this.logLevel = logLevel;
	}

}
