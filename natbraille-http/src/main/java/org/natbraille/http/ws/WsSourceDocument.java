/*
 * NatBraille Http Server - An universal Translator over http
 * Copyright (C) 2013 Vivien Guillet
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.http.ws;

//import com.sun.jersey.core.header.FormDataContentDisposition;
//import com.sun.jersey.multipart.FormDataParam;
//import jakarta.ws.rs.core.MediaType;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import jakarta.servlet.ServletContext;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.SecurityContext;
// import com.fasterxml.jackson.annotation.JsonAutoDetect;
// import org.codehaus.jackson.annotate.JsonMethod;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.natbraille.core.document.NatDocument;
import org.natbraille.http.Natd;
import org.natbraille.http.dbDocument.DbInputNatDoc;
import org.natbraille.http.dbDocument.NatLibrary;
import org.natbraille.http.dbDocument.base.DbDoc;
import org.natbraille.http.ws.formats.out.FoInputNatDocument;
import org.natbraille.http.ws.httpexception.Exception400;
import org.natbraille.http.ws.httpexception.Exception404;
import org.natbraille.http.ws.httpexception.Exception503;

/**
 * Web services of nat web server source - document management
 *
 * @author vivien
 *
 */
//@JsonAutoDetect(JsonMethod.NONE)
@Path("/private/~{user}/source")
public class WsSourceDocument {

    @Context
    ServletContext context;
    @Context
    SecurityContext securityContext;
    @PathParam("user")
    String ppUser;

    /**
     * Delete a source document by Id.
     * @param sourceDocumentId 
     */
    @DELETE
    @Path("/{sourceDocumentId}/")
    public void deleteId(
            @PathParam("sourceDocumentId") String sourceDocumentId) {

        NatLibrary natLibrary = Natd.getInstance(context).getNatLibrary();

        natLibrary.checkUser(securityContext, ppUser);

        DbInputNatDoc dbNatDoc = new DbInputNatDoc();
        dbNatDoc.setId(sourceDocumentId);
        dbNatDoc.remove(natLibrary.getDb());
    }

    /**
     * Upload a source document.
     *
     * @param uploadedInputStream file data
     * @param fileDetail file details (size, name...)
     * @param charsetName the charset encoding
     * @param contentType the content type of document
     * @return a newly created @{link DbSourceDocument}
     *
     * @throws Exception400 bad request
     * @throws Exception503 database error
     */
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public FoInputNatDocument upload(
            @FormDataParam("file") InputStream uploadedInputStream,
            @FormDataParam("file") FormDataContentDisposition fileDetail,
            @FormDataParam("charset") String charsetName,
            @FormDataParam("mimeType") String contentType) {

        Natd natd = Natd.getInstance(context);
        NatLibrary natLibrary = natd.getNatLibrary();

        natLibrary.checkUser(securityContext, ppUser);

        FoInputNatDocument resu = null;

        try {

            // create nat document
            NatDocument natDocument = new NatDocument();

            if ((charsetName != null) && (!charsetName.equals("guess"))) {
                try {
                    natDocument.forceCharset(Charset.forName(charsetName));
                } catch (Exception e) {
                }
            }
            if ((contentType != null) && (!contentType.equals("guess"))) {
                try {
                    natDocument.forceContentType(contentType);
                } catch (Exception e) {
                }

            }
            natDocument.setInputstream(uploadedInputStream);
            natDocument.setOriginalFileName(fileDetail.getFileName());

            DbInputNatDoc dbInNatDoc = new DbInputNatDoc(natDocument);

            dbInNatDoc.stamp(ppUser);
            natd.getNatLibrary().add(dbInNatDoc, securityContext);

            resu = new FoInputNatDocument(dbInNatDoc);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return resu;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<FoInputNatDocument> list() {

        Natd natd = Natd.getInstance(context);
        NatLibrary natLibrary = natd.getNatLibrary();

        natLibrary.checkUser(securityContext, ppUser);

        ArrayList<FoInputNatDocument> resu = new ArrayList<>();;

        try {
            DbInputNatDoc request = new DbInputNatDoc();
            request.getMetadataDocument().setAuthor(ppUser);
            List<DbDoc> listeDocs = request.getMatching(natLibrary.getDb());
            for (DbDoc doc : listeDocs) {
                resu.add(new FoInputNatDocument((DbInputNatDoc) doc));
            }
        } catch (Exception e) {
            throw new Exception404("cannot find '" + ppUser + "' input documents");
        }

        return resu;
    }
}
