package org.natbraille.http.ws;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import org.natbraille.http.ws.formats.out.FoNatFilterOptionsDetail;

@Path("/public/metaconfig")
public class WsMetaConfigurationMetaDocument {


	@GET
	@Path("/format/")
	@Produces(MediaType.APPLICATION_JSON)
	public FoNatFilterOptionsDetail get() {
            return new FoNatFilterOptionsDetail();
	}
}