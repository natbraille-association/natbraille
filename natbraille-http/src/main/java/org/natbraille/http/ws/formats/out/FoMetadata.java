/*
 * NatBraille Http Server - An universal Translator over http
 * Copyright (C) 2013 Vivien Guillet
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.natbraille.http.ws.formats.out;

import java.net.URI;
import org.natbraille.http.dbDocument.base.DbMetadataDoc;

public class FoMetadata {
	
	/**
	 * date of creation of the document
	 */
	private String creationDate; 
	
	/**
	 * URI of the document
	 */
	private URI publicUri;

	/**
	 * 
	 */
	private String author;
	
	/**
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * @param author the author to set
	 */
	private void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * @return the creationDate
	 */
	public String getCreationDate() {
		return creationDate;
	}

	/**
	 * @param creationDate the creationDate to set
	 */
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * @return the publicUri
	 */
	public URI getPublicUri() {
		return publicUri;
	}

	/**
	 * @param publicUri the publicUri to set
	 */
	public void setPublicUri(URI publicUri) {
		this.publicUri = publicUri;
	}

	public FoMetadata(DbMetadataDoc udmc){
		setCreationDate(udmc.getCreationDate().toString());
		setAuthor(udmc.getAuthor());
//		setPublicUri(udmc.getI());		
	}
}
