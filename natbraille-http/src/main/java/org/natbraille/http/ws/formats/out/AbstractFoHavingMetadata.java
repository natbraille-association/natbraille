/*
 * NatBraille Http Server - An universal Translator over http
 * Copyright (C) 2013 Vivien Guillet
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.natbraille.http.ws.formats.out;

import org.natbraille.http.dbDocument.base.DbDoc;

public abstract class AbstractFoHavingMetadata {

	FoMetadata metadata;

	/**
	 * @return the metatData
	 */
	public FoMetadata getMetadata() {
		return metadata;
	}

	/**
	 * @param metatData the metatData to set
	 */
	public void setMetadata(FoMetadata metadata) {
		this.metadata = metadata;
	}	
	
	public AbstractFoHavingMetadata(DbDoc und){
		setMetadata(new FoMetadata(und.getMetadataDocument()));
	}
}	
