/**
 * JSON serializable interface formats 
 * for web service output/input.
 * 
 * Introduces redondancy with UniqueDocumentX classes
 * put allows to modify UniqueDoucment classes while
 * maintaining the web services interface.
 * 
 * WARNING : modification (including renaming) of field 
 * name affects the api of the web services.
 */

/**
 * @author vivien
 *
 */
package org.natbraille.http.ws.formats;
