package org.natbraille.http.ws;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import jakarta.servlet.ServletContext;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.ResponseBuilder;
import org.natbraille.core.document.NatDocument;
import org.natbraille.core.document.NatDocumentException;
import org.natbraille.http.Natd;
import org.natbraille.http.dbDocument.DbOutputNatDoc;
import org.natbraille.http.dbDocument.NatLibrary;
import org.natbraille.http.ws.formats.out.FoOutputNatDocument;
import org.natbraille.http.ws.httpexception.Exception404;

@Path("/public/~{user}/resu/{destDocumentId}")
public class WsPublicResuDocument {

    @Context
    ServletContext context;
    @PathParam("destDocumentId")
    String destDocumentId;

    /**
     * Get transcription result description by Id
     *
     * @return
     */
    @Path("/")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public FoOutputNatDocument get() {

        try {
            Natd natd = Natd.getInstance(context);
            NatLibrary natLibrary = natd.getNatLibrary();

            DbOutputNatDoc request = new DbOutputNatDoc();
            request.readId(natLibrary.getDb(), destDocumentId);

            FoOutputNatDocument resu = new FoOutputNatDocument(request);
            return resu;
        } catch (Exception e) {
            throw new Exception404("unable to get the output document '" + destDocumentId + "'");
        }

    }

    /**
     * Get raw transcription result document.
     *
     * @return
     */
    @Path("/bytes/")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response getRaw() {
        return getBytes(false);
    }

    /**
     * Get raw transcription result document as attachement. (should open "save
     * as" dialog in browser).
     *
     * @return
     */
    @Path("/attachment/{docname}")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response getRawAsAttachement() {
        return getBytes(true);
    }

    /**
     * Return document as bytes.
     *
     * @param asAttachement if set, returns as http Content-Disposition :
     * attachement.
     * @return
     */
    private Response getBytes(boolean asAttachement) {

        Natd natd = Natd.getInstance(context);
        NatLibrary natLibrary = natd.getNatLibrary();

        //	natLibrary.checkUser(securityContext,ppUser);
        try {

            ByteBuffer data;
            Charset charset;

            DbOutputNatDoc dbOutputNatDocument = new DbOutputNatDoc();

            dbOutputNatDocument.readId(natLibrary.getDb(), destDocumentId);

            NatDocument outDoc = dbOutputNatDocument.toNatDocument();
            data = outDoc.getBytebuffer();
            charset = outDoc.getOrGuessCharset();

            ResponseBuilder rb = Response
                    .status(200)
                    .entity(data.array())
                    .header(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_PLAIN + "; " + "charset=" + charset.name());

            if (asAttachement) {
                try {
                    rb = rb.header("Content-Disposition", "attachment");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return rb.build();

        } catch (NatDocumentException nde) {
            nde.printStackTrace();
            return Response
                    .status(404)
                    .entity(nde.getMessage())
                    .build();

        } catch (Exception e) {
            e.printStackTrace();
            return Response
                    .status(404)
                    .entity(e.getMessage())
                    .build();

        }

    }

}
