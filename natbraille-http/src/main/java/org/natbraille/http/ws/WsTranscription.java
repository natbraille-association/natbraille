/*
 * NatBraille Http Server - An universal Translator over http
 * Copyright (C) 2013 Vivien Guillet
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.http.ws;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import jakarta.servlet.ServletContext;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.FormParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.SecurityContext;
import org.natbraille.core.gestionnaires.AfficheurConsole;
import org.natbraille.core.gestionnaires.LogLevel;
import org.natbraille.core.gestionnaires.LogMessage;
import org.natbraille.core.gestionnaires.MessageContents;
import org.natbraille.core.gestionnaires.MessageKind;
import org.natbraille.http.AfficheurDb;
import org.natbraille.http.Natd;
import org.natbraille.http.dbDocument.DbTransDoc;
import org.natbraille.http.dbDocument.DbTransStatusDoc;
import org.natbraille.http.dbDocument.NatLibrary;
import org.natbraille.http.dbDocument.base.DbDoc;
import org.natbraille.http.ws.formats.in.FoTranscriptionRequest;
import org.natbraille.http.ws.formats.out.FoExtendedTranscriptionRequestDocument;
import org.natbraille.http.ws.formats.out.FoTranscriptionRequestDocument;
import org.natbraille.http.ws.formats.out.FoTranscriptionStatusDocument;
import org.natbraille.http.ws.httpexception.Exception404;
import org.natbraille.http.ws.httpexception.Exception503;

/**
 * Web services of natbraille web server - transcription management
 *
 * @author vivien
 *
 */
@Path("/private/~{user}/transcription")
public class WsTranscription {

    @Context
    ServletContext context;
    @Context
    SecurityContext securityContext;
    @PathParam("user")
    String ppUser;

    /**
     * Upload a Transcription request.
     *
     * creates a Transcription document and starts the transcription process,
     * which will update the Transcription document
     *
     * @param transcriptionRequest
     * @return
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public FoExtendedTranscriptionRequestDocument upload(FoTranscriptionRequest transcriptionRequest) {
        return newTranscription(
                transcriptionRequest.getSourceDocumentId(),
                transcriptionRequest.getMetaConfigurationId(),
                transcriptionRequest.getLogLevel());
    }

    /**
     * Upload a Transcription request as a Html form.
     *
     * creates a Transcription document and starts the transcription process,
     * which will update the Transcription document
     *
     * @param sourceDocumentId
     * @param logLevel
     * @param metaConfigurationId
     * @return
     */
    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public FoExtendedTranscriptionRequestDocument uploadForm(
            @FormParam("sourceDocumentId") String sourceDocumentId,
            @FormParam("metaConfigurationId") String metaConfigurationId,
            @FormParam("logLevel") int logLevel) {

        Natd natd = Natd.getInstance(context);
        NatLibrary natLibrary = natd.getNatLibrary();

        natLibrary.checkUser(securityContext, ppUser);

        return newTranscription(sourceDocumentId, metaConfigurationId, logLevel);
    }


    /**
     * List user transcription request documents.
     *
     * @return
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<FoTranscriptionRequestDocument> list() {

        Natd natd = Natd.getInstance(context);
        NatLibrary natLibrary = natd.getNatLibrary();

        natLibrary.checkUser(securityContext, ppUser);

        ArrayList<FoTranscriptionRequestDocument> resu = new ArrayList<>();;

        try {
            DbTransDoc request = new DbTransDoc();
            request.getMetadataDocument().setAuthor(ppUser);
            List<DbDoc> listeDocs = request.getMatching(natLibrary.getDb());
            for (DbDoc doc : listeDocs) {
                resu.add(new FoTranscriptionRequestDocument((DbTransDoc) doc));
            }
        } catch (Exception e) {
            throw new Exception404("cannot find '" + ppUser + "' transcriptions");
        }

        return resu;
    }

    /**
     * Get the transcription.
     *
     * @param transcriptionNum
     * @return
     */
    @GET
    @Path("/{transcriptionNum}/")
    @Produces(MediaType.APPLICATION_JSON)
    public FoExtendedTranscriptionRequestDocument get(
            @PathParam("transcriptionNum") String transcriptionNum) {

        Natd natd = Natd.getInstance(context);
        NatLibrary natLibrary = natd.getNatLibrary();

        FoExtendedTranscriptionRequestDocument resu = null;

        try {
            DbTransDoc request = new DbTransDoc();
            request.readId(natLibrary.getDb(), transcriptionNum);
            resu = new FoExtendedTranscriptionRequestDocument(request, natLibrary);
        } catch (Exception e) {
            throw new Exception404("no such transcription");
        }

        return resu;
    }

    /**
     * Get the transcription status
     *
     * @param transcriptionNum
     * @return
     */
    @GET
    @Path("/{transcriptionNum}/status")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<FoTranscriptionStatusDocument> getStatus(
            @PathParam("transcriptionNum") String transcriptionNum) {

        Natd natd = Natd.getInstance(context);
        NatLibrary natLibrary = natd.getNatLibrary();

        natLibrary.checkUser(securityContext, ppUser);

        ArrayList<FoTranscriptionStatusDocument> foStatus = new ArrayList<>();
        try {

            DbTransDoc dbRequestDoc = new DbTransDoc();
            dbRequestDoc.readId(natLibrary.getDb(), transcriptionNum);

            ArrayList<DbTransStatusDoc> dbStatusDocs = dbRequestDoc.getAssociatedDbStatusDocuments(natLibrary);
            for (DbTransStatusDoc dbStatusDoc : dbStatusDocs) {
                foStatus.add(new FoTranscriptionStatusDocument(dbStatusDoc));
            }
        } catch (Exception e) {
            throw new Exception404("no such transcription");
        }

        return foStatus;
    }

    /**
     * Delete a transcription by Id.
     *
     * @param transcriptionId
     */
    @DELETE
    @Path("/{transcriptionNum}/")
    public void deleteId(
            @PathParam("transcriptionNum") String transcriptionId) {

        NatLibrary natLibrary = Natd.getInstance(context).getNatLibrary();

        natLibrary.checkUser(securityContext, ppUser);
        DbTransDoc dbNatDoc = new DbTransDoc();
        dbNatDoc.setId(transcriptionId);
        dbNatDoc.remove(natLibrary.getDb());

    }

    /**
     * transcription startup. called by {@link uploadSerialized#upload} and
     * {@link uploadForm}
     *
     * @param sourceDocumentId
     * @param metaConfigurationId
     * @param loglevel
     * @return
     */
    private FoExtendedTranscriptionRequestDocument newTranscription(String sourceDocumentId, String metaConfigurationId, int logLevel) {
        FoExtendedTranscriptionRequestDocument resu = null;

        Natd natd = Natd.getInstance(context);
        NatLibrary natLibrary = natd.getNatLibrary();

        natLibrary.checkUser(securityContext, ppUser);

        try {

            /**
             * request document creation
             */
            String dbInputNatDocId = sourceDocumentId;
            String dbConfigurationDocId = metaConfigurationId;

            DbTransDoc dbt = new DbTransDoc(dbInputNatDocId, dbConfigurationDocId, logLevel);
            dbt.stamp(ppUser);
            dbt.write(natLibrary.getDb());

            /**
             * create and add afficheurs
             */
            AfficheurDb afficheurDb = new AfficheurDb(dbt, natLibrary);
            afficheurDb.setWhiteList(MessageKind.STEP);

            AfficheurConsole afficheurConsole = new AfficheurConsole(LogLevel.DEBUG);

            dbt.getGestionnaireErreur().addAfficheur(afficheurDb);
            dbt.getGestionnaireErreur().addAfficheur(afficheurConsole);

            /**
             * startup message
             */
            MessageContents mc = MessageContents.Tr("la transcription va débuter");
            LogMessage message = new LogMessage(MessageKind.INFO, mc, LogLevel.SILENT);

            dbt.getGestionnaireErreur().afficheMessage(message);
            resu = new FoExtendedTranscriptionRequestDocument(dbt, natLibrary);

            /**
             * pass transcription request to the request queue
             */
            natd.enqueue(dbt);

        } catch (URISyntaxException e) {
            e.printStackTrace();
            throw new Exception503("cannot start transcription");

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception503("cannot start transcription");

        }
        return resu;
    }

}
