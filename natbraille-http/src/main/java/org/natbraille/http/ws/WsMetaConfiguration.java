/*
 * NatBraille Http Server - An universal Translator over http
 * Copyright (C) 2013 Vivien Guillet
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.http.ws;

import com.mongodb.DB;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import jakarta.servlet.ServletContext;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.SecurityContext;
import org.natbraille.core.filter.NatFilterException;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.http.Natd;
import org.natbraille.http.dbDocument.DbConfDoc;
import org.natbraille.http.dbDocument.NatLibrary;
import org.natbraille.http.dbDocument.base.DbDoc;
import org.natbraille.http.ws.formats.out.FoMetaConfigResponse;
import org.natbraille.http.ws.formats.out.FoNatFilterOptionsDetail;
import org.natbraille.http.ws.httpexception.Exception403;
import org.natbraille.http.ws.httpexception.Exception404;
import org.natbraille.http.ws.httpexception.Exception503;

/**
 * Web services of natbraille web server - FoMetaConfigResponseuration
 * management
 *
 * @author vivien
 *
 */
@Path("/private/~{user}/metaconfig")
public class WsMetaConfiguration {

    @Context
    ServletContext context;
    @Context
    SecurityContext securityContext;
    @PathParam("user")
    String ppUser;

    @GET
    @Path("/format/")
    @Produces(MediaType.APPLICATION_JSON)
    public FoNatFilterOptionsDetail metaInfo() {
        System.out.println("HERE");
        return new FoNatFilterOptionsDetail();
    }

    /**
     * Delete a configuration.
     *
     * @param metaConfigurationId
     */
    @DELETE
    @Path("/{metaConfigurationId}/")
    public void deleteId(@PathParam("metaConfigurationId") String metaConfigurationId) {

        NatLibrary natLibrary = Natd.getInstance(context).getNatLibrary();

        natLibrary.checkUser(securityContext, ppUser);

        DbConfDoc dbNatDoc = new DbConfDoc();
        dbNatDoc.readId(natLibrary.getDb(), metaConfigurationId);
        String author = dbNatDoc.getMetadataDocument().getAuthor();

        if ((author != null) && (author.equals(ppUser))) {
            DbConfDoc doc = new DbConfDoc();
            doc.setId(metaConfigurationId);
            doc.remove(natLibrary.getDb());
        } else {
            throw new Exception403("you don't own the document");
        }

    }

    /**
     * Get the list of configurations available to the user.
     *
     * @return the list of user and system configurations
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<FoMetaConfigResponse> list() {

        List<FoMetaConfigResponse> l = new ArrayList<>();

        Natd natd = Natd.getInstance(context);
        NatLibrary natLibrary = natd.getNatLibrary();

        natLibrary.checkUser(securityContext, ppUser);

        try {
            // add configs by authors ppUser and natbraille
            for (String author : new String[]{"natbraille", ppUser}) {

                DbConfDoc dbConfDocModel = new DbConfDoc();
                dbConfDocModel.getMetadataDocument().setAuthor(author);
                List<DbDoc> userDbConfDocs = dbConfDocModel.getMatching(natLibrary.getDb());

                for (DbDoc dbDoc : userDbConfDocs) {
                    DbConfDoc dbConfDoc = (DbConfDoc) dbDoc;
                    try {
                        l.add(new FoMetaConfigResponse(dbConfDoc));
                    } catch (NatFilterException e) {
                        System.err.println("passing bad conf" + e.getMessage());

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception503("cannot get configuration documents");
        }
        return l;
    }

    /**
     * get the content of the configuration of id
     *
     * @param configurationNum
     * @return the content of the user configuration
     */
    @GET
    @Path("/{configurationNum}/")
    @Produces(MediaType.APPLICATION_JSON)
    public Properties getId(@PathParam("configurationNum") String configurationNum) {
        Natd natd = Natd.getInstance(context);
        NatLibrary natLibrary = natd.getNatLibrary();

        natLibrary.checkUser(securityContext, ppUser);

        try {

            // URI uri = new URI(UniqueConfigurationDocument.authority+ "/" +
            // configurationNum);
            // UniqueConfigurationDocument docConf =
            // (UniqueConfigurationDocument) natLibrary.get(uri);
            DbConfDoc docConf = new DbConfDoc();
            docConf.readId(natLibrary.getDb(), configurationNum);
            return docConf.toNatFilterOptions().getAsProperties();
        } catch (Exception e) {
            throw new Exception404("no such configuration id '" + configurationNum + "'");
        }
    }

    /**
     * Upload a metaconfiguration as a Json Properties list.
     *
     * @param properties
     * @return
     * @throws Exception
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public FoMetaConfigResponse upload(Properties properties) throws Exception {
        Natd natd = Natd.getInstance(context);
        NatLibrary natLibrary = natd.getNatLibrary();

        natLibrary.checkUser(securityContext, ppUser);

        Properties configProperties = new Properties();

        for (NatFilterOption o : NatFilterOption.values()) {
            String value = properties.getProperty(o.name());
            if (value != null) {
                configProperties.setProperty(o.name(), value);
            }
        }

        // create filter options
        NatFilterOptions nfo = new NatFilterOptions(configProperties);

        // write
        // UniqueConfigurationDocument ucd = new
        // UniqueConfigurationDocument(nfo);
        DbConfDoc ucd = new DbConfDoc(nfo);

        ucd.stamp(ppUser);
        ucd.write(natLibrary.getDb());

        // create return object
        FoMetaConfigResponse mc = new FoMetaConfigResponse(ucd);
        return mc;

    }

    private static final String META_CONFIGURATION_URL_PARAM_NAME = "metaConfigurationUrl";

    /**
     * Upload a Metaconfiguration as a html form value map.
     *
     * @param inFormParams
     * @return @throws Exception
     */
    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public FoMetaConfigResponse uploadForm(MultivaluedMap<String, String> inFormParams) throws Exception {
        Natd natd = Natd.getInstance(context);
        NatLibrary natLibrary = natd.getNatLibrary();

        natLibrary.checkUser(securityContext, ppUser);

        try {
            DB db = natLibrary.getDb();

            String baseConfId = inFormParams.getFirst(META_CONFIGURATION_URL_PARAM_NAME);
            DbConfDoc docConf = new DbConfDoc();
            docConf.readId(db, baseConfId);

            // get base config properties
            Properties newOptions = docConf.toNatFilterOptions().getAsProperties();

            // set form data
            for (String k : inFormParams.keySet()) {
                newOptions.setProperty(k, inFormParams.getFirst(k));
            }

            // discard META_CONFIGURATION_URL_PARAM_NAME because it is not a
            // filter parameter
            newOptions.remove(META_CONFIGURATION_URL_PARAM_NAME);

            // create filter options
            NatFilterOptions nfo = new NatFilterOptions(newOptions);

            // write
            DbConfDoc ucd = new DbConfDoc(nfo);
            ucd.stamp(ppUser);
            ucd.write(db);

            FoMetaConfigResponse mc = new FoMetaConfigResponse(ucd);
            return mc;
        } catch (Exception e) {
            throw new Exception("cannot create new configuration", e);
        }
    }
}
