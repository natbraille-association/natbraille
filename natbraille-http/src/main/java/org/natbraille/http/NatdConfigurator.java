/*
 * NatBraille Http Server - An universal Translator over http
 * Copyright (C) 2013 Vivien Guillet
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.http;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import jakarta.servlet.ServletContext;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.natbraille.core.filter.NatFilterException;
import org.natbraille.core.gestionnaires.GestionnaireErreur;
import org.natbraille.core.gestionnaires.LogLevel;
import org.natbraille.core.gestionnaires.MessageContents;
import org.natbraille.core.gestionnaires.MessageKind;

/**
 * Configuration managment for {@link Natd}
 * @author vivien
 */
public class NatdConfigurator {

    private final ServletContext context;
    private final GestionnaireErreur gestionnaireErreur;

    /**
     * hard coded paths where configuration is looked for
     */
    private static final String HARD_CODED_CONFIG_PATH[]
            = new String[]{
                "./natbrailled",
                "~/.natbrailled",
                "/etc/natbrailled",};

    /**
     * natd configuration file location parameter name as found in web.xml
     *
     */
    private static final String WEB_XML_PARAM_NAME = "configurationFile";

    /**
     * Load the first natd configuration.
     * <p>
     * File is searched in :
     * <ul>
     * <li> ./natbrailled</li>
     * <li> ~/.natbrailled</li>
     * <li> /etc/natbrailled</li>
     * <li> the parh coded in web.xml</li>
     * <p>
     * web.xml<pre>
     * [...]
     * &lt;context-param&gt;
     * &lt;description&gt;Path of the server configuration file&lt;/description&gt;
     * &lt;param-name&gt;configurationFile&lt;/param-name&gt;
     * &lt;param-value&gt;/home/vivien/src/reecritureweb/natdconfig.json&lt;/param-value&gt;
     * &lt;/context-param&gt;
     * </pre>
     * </ul>
     * [...]
     *
     * @return
     */
    public NatdConfiguration load() {

        ArrayList<String> potentialConfigPath = new ArrayList<>();

        // add files found in hard coded path list
        potentialConfigPath.addAll(Arrays.asList(HARD_CODED_CONFIG_PATH));

        // add file found in WEB-INF/web.xml context-param
        potentialConfigPath.add(context.getInitParameter(WEB_XML_PARAM_NAME));

        return loadFirst(potentialConfigPath);
    }

    /**
     * Load a natd configuration from a json file
     *
     * @param fileName
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     * @throws NatFilterException
     * @throws URISyntaxException
     */
    private static NatdConfiguration loadFromJson(String fileName) throws JsonParseException, JsonMappingException, IOException, NatFilterException, URISyntaxException {
        ObjectMapper mapper = new ObjectMapper();
        NatdConfiguration natdConfiguration = mapper.readValue(new File(fileName), NatdConfiguration.class);
        return natdConfiguration;
    }

    /**
     * loadFromJson first correct configuration file in potential filenames
     *
     * @param potentialFilenames
     * @return
     */
    private NatdConfiguration loadFirst(List<String> potentialFilenames) {
        for (String potentialFilename : potentialFilenames) {
            gestionnaireErreur.afficheMessage(MessageKind.INFO,
                    MessageContents.Tr("Searching for server configuration at location : {0}").setValue(potentialFilename),
                    LogLevel.NORMAL);

            try {
                NatdConfiguration config = loadFromJson(potentialFilename);
                gestionnaireErreur.afficheMessage(MessageKind.INFO,
                        MessageContents.Tr("Loaded server configuration at location  : {0}").setValue(potentialFilename),
                        LogLevel.NORMAL);
                return config;
            }  catch (IOException | NatFilterException | URISyntaxException e) {
                gestionnaireErreur.afficheMessage(MessageKind.INFO,
                        MessageContents.Tr("No or malformed server configuration at location  : {0} : {1}").setValues(potentialFilename,e.getLocalizedMessage()),
                        LogLevel.NORMAL);
            }
        }
        gestionnaireErreur.afficheMessage(MessageKind.ERROR,
                MessageContents.Tr("No server configuration found"),
                LogLevel.SILENT);
        return null;
    }

    NatdConfigurator(GestionnaireErreur ge, ServletContext context) {
        this.gestionnaireErreur = ge;
        this.context = context;
    }

}
