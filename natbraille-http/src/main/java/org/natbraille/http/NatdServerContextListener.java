/*
 * NatBraille Http Server - An universal Translator over http
 * Copyright (C) 2013 Vivien Guillet
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.http;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletContextListener;
import jakarta.servlet.annotation.WebListener;

import jakarta.servlet.ServletContextEvent;

/**
 * Context Listener for the servlet. Starts and stops {@link Natd} server
 */
@WebListener
public class NatdServerContextListener implements ServletContextListener {

    /**
     * get the Natd from to the servlet context for use in web service
     *
     * @param context
     * @return Natd instance
     */
    public static Natd getFromContext(ServletContext context) {
        Natd natd = (Natd) context.getAttribute(org.natbraille.http.Natd.class.getCanonicalName());
        return natd;
    }

    /**
     * links the Natd instance to the servlet context for use in web service
     *
     * @param context the servlet context
     * @param natd the Natd instance
     */
    private static void setToContext(ServletContext context, Natd natd) {
        context.setAttribute(org.natbraille.http.Natd.class.getCanonicalName(), natd);
    }

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {

        ServletContext context = servletContextEvent.getServletContext();

        Natd natd = new Natd();
        try {
            natd.initialize(context);
            setToContext(context, natd);
        } catch (Exception e) {            
            e.printStackTrace();
            natd.stop();
        }

    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        ServletContext context = servletContextEvent.getServletContext();
        Natd natd = getFromContext(context);
        natd.stop();
    }

}
