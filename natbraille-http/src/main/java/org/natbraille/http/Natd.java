/*
 * NatBraille Http Server - An universal Translator over http
 * Copyright (C) 2013 Vivien Guillet
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
/**
 *
 */
package org.natbraille.http;

import com.mongodb.DB;
import com.mongodb.MongoClient;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import jakarta.servlet.ServletContext;
import org.natbraille.core.filter.NatFilterException;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.core.gestionnaires.Afficheur;
import org.natbraille.core.gestionnaires.AfficheurConsole;
import org.natbraille.core.gestionnaires.GestionnaireErreur;
import org.natbraille.core.gestionnaires.LogLevel;
import org.natbraille.core.gestionnaires.MessageContents;
import org.natbraille.core.gestionnaires.MessageKind;
import org.natbraille.http.dbDocument.DbConfDoc;
import org.natbraille.http.dbDocument.DbTransDoc;
import org.natbraille.http.dbDocument.NatLibrary;
import org.natbraille.http.dbDocument.base.DbDoc;

/**
 * Nat web server.
 * <p>
 * started and stopped by a {@link NatdServerContextListener}.
 * <p>
 * This server uses mongodb for data persistence.
 * <p>
 * A common {@link TranscriptionThreadPool} is used for all natbraille
 * transcription opération started from the web services.
 *
 * @author vivien
 *
 */
public class Natd {

    /**
     * Server error handler
     */
    private final GestionnaireErreur ge = new GestionnaireErreur();

    /**
     * Nat web server configuration
     */
    private NatdConfiguration natdConfiguration;

    /**
     * Pool of transcription threads
     */
    private TranscriptionThreadPool threadPool;

    /**
     * library of nat documents the server operates on
     */
    private NatLibrary natLibrary;

    /**
     * database client database (one instance : thread-safe)
     */
    private MongoClient mongoClient;

    /**
     * database client database (one instance : thread-safe)
     */
    private DB db;

    /**
     * adds an unique transcription request document to the web server
     *
     * @param transcriptionRequest
     */
    public void enqueue(DbTransDoc transcriptionRequest) {
        threadPool.enqueue(transcriptionRequest);
    }

    /**
     * Get the {@link Natd} instance from the {@link ServletContext}. {
     *
     * @see NatdServerContextListener.getFromContext}
     * @param context
     * @return
     */
    public static Natd getInstance(ServletContext context) {
        return NatdServerContextListener.getFromContext(context);
    }

    /**
     * initialization of the Natd server
     * <p>
     * look for nat server configuration file in hardcoded locations
     * (/etc/natbrailled and ~/.natbrailled) ; loadFromJson the first available
     * <p>
     * connect to the database
     * <p>
     * construct a NatLibray
     * <p>
     * instanciate the transcrition thread pool
     *
     * @param context
     * @throws Exception
     */
    public void initialize(ServletContext context) throws Exception {

        System.setProperty("javax.xml.parsers.DocumentBuilderFactory", "org.apache.xerces.jaxp.DocumentBuilderFactoryImpl");
        System.setProperty("javax.xml.xpath.XPathFactory:http://java.sun.com/jaxp/xpath/dom", "com.sun.org.apache.xpath.internal.jaxp.XPathFactoryImpl");

        // 1. Create error manager.
        Afficheur aff = new AfficheurConsole(LogLevel.ULTRA);
        ge.addAfficheur(aff);

        ge.afficheMessage(MessageKind.INFO,
                MessageContents.Tr("Starting natbraille server"),
                LogLevel.NORMAL);

        // 2. loadFromJson server configuration        
        NatdConfigurator configurator = new NatdConfigurator(ge, context);
        setNatdConfiguration(configurator.load());

        if (getNatdConfiguration() == null) {
            ge.afficheMessage(MessageKind.UNRECOVERABLE_ERROR, MessageContents.Tr("error while loading server configuration file, cannot start"), LogLevel.SILENT);
            throw new Exception();
        } else {
            ge.afficheMessage(MessageKind.SUCCESS, MessageContents.Tr("server configuration file loaded"), LogLevel.NORMAL);
        }

        // 3. connect to the database
        DB newDb = null;

        try {

            // 3.1 create a db client
            String name = getNatdConfiguration().getDb_name();
            String host = getNatdConfiguration().getDb_host();
            String port = getNatdConfiguration().getDb_port();

            MongoClient newMongoClient = null;

            if (host.isEmpty() && port.isEmpty()) {
                newMongoClient = new MongoClient();
            } else if (!host.isEmpty()) {
                if (port.isEmpty()) {
                    newMongoClient = new MongoClient(host);
                } else {
                    newMongoClient = new MongoClient(host, Integer.parseInt(port));
                }
            }

            if ((name == null) || (name.isEmpty())) {
                ge.afficheMessage(MessageKind.UNRECOVERABLE_ERROR,
                        MessageContents.Tr("No database name found in configuration file"),
                        LogLevel.SILENT);
                throw new Exception();
            }
            if (newMongoClient == null) {
                ge.afficheMessage(MessageKind.UNRECOVERABLE_ERROR,
                        MessageContents.Tr("could not connect to {0}@{1}:{2}").setValues(name, host, port),
                        LogLevel.SILENT);
                throw new Exception();
            } else {
                ge.afficheMessage(MessageKind.INFO,
                        MessageContents.Tr("connected to {0}@{1}:{2}").setValues(name, host, port),
                        LogLevel.NORMAL);
                // 3.2 keep reference to the client for shudown
                setMongoClient(newMongoClient);
            }

            // 3.2 client opens the natbraille database
            newDb = newMongoClient.getDB(name);

            // 3.3 test database
            if ((newDb != null) && Mongotest.test(newDb)) {
                ge.afficheMessage(MessageKind.SUCCESS,
                        MessageContents.Tr("successfully connected and tested database : {0}").setValue(name),
                        LogLevel.NORMAL);
                setDb(newDb);
            } else {
                ge.afficheMessage(MessageKind.UNRECOVERABLE_ERROR,
                        MessageContents.Tr("Failed while opening database : {0}").setValue(name),
                        LogLevel.SILENT);
                throw new Exception();
            }

        } catch (Exception e) {
            throw new Exception("error while creating and testing database", e);
        }

        // 4. create a library
        try {
            this.natLibrary = new NatLibrary(newDb);
        } catch (Exception e) {
            ge.afficheMessage(MessageKind.UNRECOVERABLE_ERROR,
                    MessageContents.Tr("error while creating library"),
                    LogLevel.SILENT);
            throw new Exception();
        }
        ge.afficheMessage(MessageKind.SUCCESS,
                MessageContents.Tr("natbraille library created"),
                LogLevel.NORMAL);

        // 5. load natbraille transcriptor configurations specified in 
        // NatdConfiguration in the database if they do not already exist.
        try {
            DbConfDoc dbConfDocExistingModel = new DbConfDoc();
            dbConfDocExistingModel.getMetadataDocument().setAuthor("natbraille");
            List<DbDoc> dbConfDocList = dbConfDocExistingModel.getMatching(db);

            ArrayList<String> natOptionFilterFiles = getNatdConfiguration().getSystemFilterConfigurationPaths();
            for (DbConfDoc dbConf : loadSystemNatFilterOptions(natOptionFilterFiles)) {
                String name = dbConf.toNatFilterOptions().getValue(NatFilterOption.CONFIG_NAME);
                if (!dbConf.hasEquivalent(dbConfDocList)) {
                    dbConf.stamp("natbraille");
                    dbConf.write(newDb);
                    ge.afficheMessage(MessageKind.INFO,
                            MessageContents.Tr("wrote new filter configuration : {0} ").setValue(name),
                            LogLevel.NORMAL);
                } else {
                    ge.afficheMessage(MessageKind.INFO,
                            MessageContents.Tr("skipping already existing filter conf : {0}").setValue(name),
                            LogLevel.NORMAL);
                }
            }

        } catch (Exception e) {
            throw new Exception("error in default server filter configurations, cannot start", e);
        }
        ge.afficheMessage(MessageKind.SUCCESS, MessageContents.Tr("System filter options configurations loaded"), LogLevel.NORMAL);

        // 6. instanciate the transcription thread pool
        try {
            threadPool = new TranscriptionThreadPool(natLibrary, getNatdConfiguration().getTranscriptionPoolSize());
            new Thread(threadPool).start();
            ge.afficheMessage(MessageKind.SUCCESS,
                    MessageContents.Tr("Transcription thread pool started"),
                    LogLevel.NORMAL);
        } catch (Exception e) {
            throw new Exception("cannot start transcription thread pool, cannot start", e);
        }

    }

    public void stop() {
        ge.afficheMessage(MessageKind.INFO,
                MessageContents.Tr("Stopping natbraille server"),
                LogLevel.NORMAL);

        try {
            ge.afficheMessage(MessageKind.INFO,
                    MessageContents.Tr("Stopping thread pool"),
                    LogLevel.NORMAL);
            threadPool.shutdownAndAwaitTermination();
        } catch (Exception e) {
            ge.afficheMessage(MessageKind.ERROR,
                    MessageContents.Tr("Stopping thread pool failed"),
                    LogLevel.SILENT);
            e.printStackTrace();
        }
        try {
            ge.afficheMessage(MessageKind.INFO,
                    MessageContents.Tr("Stopping db client"),
                    LogLevel.NORMAL);
            getMongoClient().close();
        } catch (Exception e) {
            ge.afficheMessage(MessageKind.ERROR,
                    MessageContents.Tr("Stopping db client failed"),
                    LogLevel.SILENT);
            e.printStackTrace();
        }

    }

    /**
     * @return the db
     */
    /*
     private DB getDb() {        
     return db;
     }
     */
    /**
     * @param db the db to set
     */
    private void setDb(DB db) {
        this.db = db;
    }

    /**
     * loads server configurations from files
     *
     * @param natOptionFilterFiles the filename list
     * @return
     */
    private ArrayList<DbConfDoc> loadSystemNatFilterOptions(ArrayList<String> natOptionFilterFiles) {
        ArrayList<DbConfDoc> confs = new ArrayList<>();

        for (String path : natOptionFilterFiles) {
            Properties p = null;
            NatFilterOptions natFilterOptions = null;
            DbConfDoc ucd = null;
            try {
                p = new Properties();
                InputStream is = new FileInputStream(new File(path));
                p.load(is);
            } catch (IOException e) {
                ge.afficheMessage(MessageKind.ERROR,
                        MessageContents.Tr("does not seem to be a property filter options for file : {0} ").setValue(path),
                        LogLevel.SILENT);
            }
            try {
                natFilterOptions = new NatFilterOptions(p);
            } catch (NatFilterException e) {
                ge.afficheMessage(MessageKind.ERROR,
                        MessageContents.Tr("does not seem to be a filter options for file : {0}: ").setValue(path),
                        LogLevel.SILENT);
                e.printStackTrace();
            }
            try {
                ucd = new DbConfDoc(natFilterOptions);
            } catch (Exception e) {
                ge.afficheMessage(MessageKind.ERROR,
                        MessageContents.Tr("cannot create configuration document for file : {0}").setValue(path),
                        LogLevel.SILENT);
            }
            if (ucd != null) {
                ge.afficheMessage(MessageKind.INFO,
                        MessageContents.Tr("loaded filter options file : {0}").setValue(path),
                        LogLevel.NORMAL);
                confs.add(ucd);
            }
        }
        return confs;
    }

    /**
     * @return the server configuration
     */
    public NatdConfiguration getNatdConfiguration() {
        return natdConfiguration;
    }

    private void setNatdConfiguration(NatdConfiguration natdConfiguration) {
        this.natdConfiguration = natdConfiguration;
    }

    /**
     * Get the {@link NatLibrary}
     *
     * @return
     */
    public NatLibrary getNatLibrary() {
        return natLibrary;
    }

    protected MongoClient getMongoClient() {
        return mongoClient;
    }

    private void setMongoClient(MongoClient mongoClient) {
        this.mongoClient = mongoClient;
    }

}
