/*
 * NatBraille Http Server - An universal Translator over http
 * Copyright (C) 2013 Vivien Guillet
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.http;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;
import org.natbraille.http.dbDocument.DbTransDoc;
import org.natbraille.http.dbDocument.NatLibrary;

/**
 * Pool of {@link TranscriptionThread}.
 * @author vivien
 */
public class TranscriptionThreadPool implements Runnable {

    private final PriorityBlockingQueue<DbTransDoc> transcriptionsRequestsQueue = new PriorityBlockingQueue<>();
    private final ExecutorService pool;
    private final NatLibrary natLibrary;

    public TranscriptionThreadPool(NatLibrary natLibrary, int poolSize) {
        pool = Executors.newFixedThreadPool(poolSize);
        this.natLibrary = natLibrary;
    }

    private void dbg(String msg) {
        System.out.println("socket thread pool :: " + msg);
    }

    public void shutdownAndAwaitTermination() {
        pool.shutdown(); // Disable new tasks from being submitted
        try {
            // Wait a while for existing tasks to terminate
            if (!pool.awaitTermination(60, TimeUnit.SECONDS)) {
                pool.shutdownNow(); // Cancel currently executing tasks
                // Wait a while for tasks to respond to being cancelled
                if (!pool.awaitTermination(60, TimeUnit.SECONDS)) {
                    dbg("Pool did not terminate");
                }
            }
        } catch (InterruptedException ie) {
            // (Re-)Cancel if current thread also interrupted
            pool.shutdownNow();
            // Preserve interrupt status
            Thread.currentThread().interrupt();
        }
    }

    public void enqueue(DbTransDoc utranscriptionRequestDocument) {
        transcriptionsRequestsQueue.add(utranscriptionRequestDocument);
    }

    public void run() { // run the service
        try {
            dbg("new connexions accepted");
            for (;;) {
                DbTransDoc request = transcriptionsRequestsQueue.take();
                pool.execute(new TranscriptionThread(natLibrary, request));
                dbg("new connexion accepted");
            }
        } catch (RejectedExecutionException e) {
            dbg("rejected exceution exception");
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            dbg("shutdown");
            pool.shutdown();
            dbg("shutdown done");
        }
    }

}
