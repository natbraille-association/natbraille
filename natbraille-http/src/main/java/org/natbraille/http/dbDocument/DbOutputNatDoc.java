package org.natbraille.http.dbDocument;

import org.natbraille.core.document.NatDocument;
import org.natbraille.core.document.NatDocumentException;

public class DbOutputNatDoc extends DbNatDoc {
	private final String COLLECTION_NAME = "output documents";
	protected String getCollectionName(){
		return COLLECTION_NAME;
	}

	public DbOutputNatDoc(NatDocument natDoc) throws NatDocumentException {
		super(natDoc);
	}
	
	public DbOutputNatDoc() {
	}

	public class Key {
		public final static String  TRANSCRIPTION_ID = "transcription_id";
	}
	
	public String getTranscriptionId(){
		return getDbo().getString(Key.TRANSCRIPTION_ID);
	}
	
	public void setTranscriptionId(String transcriptionId){
		getDbo().put(Key.TRANSCRIPTION_ID,transcriptionId);
	}
	
	
	
}
