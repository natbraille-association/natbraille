package org.natbraille.http.dbDocument;

import java.net.URISyntaxException;
import java.nio.charset.Charset;
import org.natbraille.brailletable.BrailleTables;
import org.natbraille.core.document.NatDocument;
import org.natbraille.core.document.NatDocumentException;

public class DbInputNatDoc extends DbNatDoc {

	private final String COLLECTION_NAME = "input documents";
	protected String getCollectionName(){
		return COLLECTION_NAME;
	}
	
	public DbInputNatDoc(NatDocument natDoc) throws NatDocumentException {
		setOriginalFileName(natDoc.getOriginalFileName());
		setCharset(natDoc.getOrGuessCharset().name());
		setMimeType(natDoc.getOrGuessContentType());
		try {
			setBrailleTable(natDoc.getBrailletable().getName());
			setBrailleEncoding(natDoc.getOrGuessCharset().toString());
		} catch (Exception e){
			// getBrailleTable can be null;
		}
		setData(natDoc.getBytebuffer().array());
	}

	public DbInputNatDoc() {
	}

	public NatDocument toNatDocument() throws URISyntaxException, NatDocumentException{
		NatDocument natDoc = new NatDocument();
		try {
		//URI uri = new URI(getBrailleTable());
		//String name = null;
		//Charset charset = Charset.forName(getBrailleEncoding());		
		natDoc.setBrailletable(BrailleTables.forName(getBrailleTable()));
		} catch (Exception e){
			// not illegal not to have a braille table
		}
		natDoc.setByteBuffer(getData());
		natDoc.setOriginalFileName(getOriginalFileName());
		natDoc.forceCharset(Charset.forName(getCharset()));
		natDoc.forceContentType(getMimeType());
		return natDoc;
	}
}
