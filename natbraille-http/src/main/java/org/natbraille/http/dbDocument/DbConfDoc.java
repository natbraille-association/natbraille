/*
 * NatBraille Http Server - An universal Translator over http
 * Copyright (C) 2013 Vivien Guillet
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.natbraille.http.dbDocument;

import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.bson.BasicBSONObject;
import org.natbraille.core.filter.NatFilterException;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.http.dbDocument.base.DbDoc;

public class DbConfDoc extends DbDoc {
	private final String COLLECTION_NAME = "configuration documents";
	protected String getCollectionName(){
		return COLLECTION_NAME;
	}

	public class Key {
		public final static String  OPTIONS = "options";
	}

	/* (non-Javadoc)
	 * @see natd.dbDocument.DbConfigurationDocumentInterface#getOptions()
	 */
	public Map<?,?> getOptions(){
		Object o = getDbo().get(Key.OPTIONS);
		BasicBSONObject bsonmap = (BasicBSONObject) o;
		return bsonmap.toMap();
	}

	private void setOptions(Properties map){
		getDbo().remove(Key.OPTIONS);
		getDbo().append(Key.OPTIONS,new BasicBSONObject(map));

	}
	public DbConfDoc(NatFilterOptions nfo){
		//		Map<Object,Object> map = nfo.getAsProperties();
		setOptions(nfo.getAsProperties());

	}

	public DbConfDoc() {
	}


	/* (non-Javadoc)
	 * @see natd.dbDocument.DbConfigurationDocumentInterface#toNatFilterOptions()
	 */
	public NatFilterOptions toNatFilterOptions() throws NatFilterException {

		Map<?,?> options = getOptions();
		Properties p = new Properties();


		for (Object o : options.keySet()){
			String name = (String)o;
			String value = (String) options.get(name);
			p.setProperty(name, value);

		}

		NatFilterOptions nfo = new NatFilterOptions(p);
		return nfo;
	}

	/**
	 * "equivalent" dbConf have the same NatFilterOptions, but
	 * can have different metadata
	 * @param dbConf
	 * @return
	 */
	public boolean equivalent(DbConfDoc dbConf) {
		boolean resu = false;

		try {
			boolean equivalent = true;
			NatFilterOptions nfoA = dbConf.toNatFilterOptions();
			NatFilterOptions nfoB = toNatFilterOptions();
			NatFilterOption allOptions[] = NatFilterOption.values();

			lookup :
				for (NatFilterOption o : allOptions){
					String valueA = nfoA.getValue(o);
					String valueB = nfoB.getValue(o);
					if (!valueA.equals(valueB)){
						equivalent = false;
						break lookup;
					}
				}
			resu = equivalent;
		} catch (NatFilterException e) {
			// if there is an exception, resu is not equivalent
			e.printStackTrace();
		}

		return resu;
	}
	public boolean hasEquivalent(List<DbDoc> dbConfDocList){
		boolean resu = false;

		lookup:
			for (DbDoc dbDoc: dbConfDocList) {
				try {
					DbConfDoc dbConfDoc = (DbConfDoc) dbDoc;
					if (equivalent(dbConfDoc)){
						resu = true;
						break lookup;	
					}
				} catch (Exception e){
					// was probably not a dbConfDoc, so not equivalent : not a problem
				}
			}
		return resu;
	}
}
