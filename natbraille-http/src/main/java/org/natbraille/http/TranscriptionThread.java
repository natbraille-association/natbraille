/*
 * NatBraille Http Server - An universal Translator over http
 * Copyright (C) 2013 Vivien Guillet
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.http;

import com.mongodb.DB;
import org.natbraille.core.document.NatDocument;
import org.natbraille.core.filter.NatFilterConfigurator;
import org.natbraille.core.NatDynamicResolver;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.core.filter.transcriptor.Transcriptor;
import org.natbraille.core.gestionnaires.GestionnaireErreur;
import org.natbraille.core.gestionnaires.LogLevel;
import org.natbraille.core.gestionnaires.LogMessage;
import org.natbraille.core.gestionnaires.MessageContents;
import org.natbraille.core.gestionnaires.MessageKind;
import org.natbraille.http.dbDocument.DbConfDoc;
import org.natbraille.http.dbDocument.DbInputNatDoc;
import org.natbraille.http.dbDocument.DbOutputNatDoc;
import org.natbraille.http.dbDocument.DbTransDoc;
import org.natbraille.http.dbDocument.NatLibrary;

/**
 * {@link Natd} transcription thread using a {link Transcriptor}.
 *
 * @author vivien
 *
 */
public class TranscriptionThread implements Runnable {

    private DbTransDoc transcriptionRequest;
    private NatLibrary natLibrary;

    private DbTransDoc getTranscriptionRequest() {
        return transcriptionRequest;
    }

    private void setTranscriptionRequest(DbTransDoc transcriptionRequest) {
        this.transcriptionRequest = transcriptionRequest;
    }

    private NatLibrary getNatLibrary() {
        return natLibrary;
    }

    private void setNatLibrary(NatLibrary natLibrary) {
        this.natLibrary = natLibrary;
    }

    TranscriptionThread(NatLibrary natLibrary, DbTransDoc transcriptionRequest) {
        setTranscriptionRequest(transcriptionRequest);
        setNatLibrary(natLibrary);
    }

    public void run() {
        LogMessage msg = null;
        try {

            DB db = getNatLibrary().getDb();

            getTranscriptionRequest().setState(TranscriptionState.pending);
            getTranscriptionRequest().write(db);

            // get nat document from id
            DbInputNatDoc dbInputNatDocument = new DbInputNatDoc();
            dbInputNatDocument.readId(natLibrary.getDb(), getTranscriptionRequest().getInputDocId());
            NatDocument natDocument = dbInputNatDocument.toNatDocument();

            // nat filter options from id
            DbConfDoc dbConfigurationDocument = new DbConfDoc();
            dbConfigurationDocument.readId(natLibrary.getDb(), getTranscriptionRequest().getConfigurationDocId());

            NatFilterOptions natFilterOptions = dbConfigurationDocument.toNatFilterOptions();

            // create Transcriptor filter
            GestionnaireErreur ge = getTranscriptionRequest().getGestionnaireErreur();
            NatDynamicResolver ndr = new NatDynamicResolver(natFilterOptions, ge);
            NatFilterConfigurator ff = new NatFilterConfigurator().set(ge).set(ndr).set(natFilterOptions);

            Transcriptor natTranscriptor = new Transcriptor(ff);

            // run filter
            NatDocument resu = natTranscriptor.run(natDocument);
            resu.setOriginalFileName(natDocument.getOriginalFileName()
                    + "_"
                    + natFilterOptions.getValue(NatFilterOption.CONFIG_NAME)
                    + ".natbraille.txt");

            // create output document
            DbOutputNatDoc dbOutputNatDocument = new DbOutputNatDoc(resu);
            dbOutputNatDocument.stamp(getTranscriptionRequest().getMetadataDocument().getAuthor());
            dbOutputNatDocument.setTranscriptionId(getTranscriptionRequest().getId());
            dbOutputNatDocument.write(db);

            getTranscriptionRequest().setState(TranscriptionState.success);
            getTranscriptionRequest().setOutputDocId(dbOutputNatDocument.getId());
            getTranscriptionRequest().write(db);
            MessageContents mc = MessageContents.Tr("{0}").setValue(dbOutputNatDocument.getId());
            msg = new LogMessage(MessageKind.FINAL_SUCCESS, mc, LogLevel.NONE);

        } catch (Exception e) {
            getTranscriptionRequest().setState(TranscriptionState.error);
            DB db = getNatLibrary().getDb();
            getTranscriptionRequest().write(db);
            getTranscriptionRequest().getGestionnaireErreur().afficheMessage(MessageKind.UNRECOVERABLE_ERROR, e);
        }
        if (msg != null) {
            getTranscriptionRequest().getGestionnaireErreur().afficheMessage(msg);
        }
    }
}
