/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.cli;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.MissingArgumentException;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.PosixParser;
import org.apache.commons.cli.UnrecognizedOptionException;
import org.natbraille.core.document.NatDocument;
import org.natbraille.core.filter.NatFilterConfigurator;
import org.natbraille.core.NatDynamicResolver;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.core.filter.transcriptor.Transcriptor;
import org.natbraille.core.gestionnaires.AfficheurAnsiConsole;
import org.natbraille.core.gestionnaires.GestionnaireErreur;
import org.natbraille.core.gestionnaires.LogLevel;
import org.natbraille.core.gestionnaires.MessageContents;
import org.natbraille.core.gestionnaires.MessageKind;

public class NatCommandLine {

    /**
     * global cli options
     */
    public final static String CL_LOG_LEVEL = "log";
    public final static String CL_FILTER_OPTIONS_FILENAME = "filter";
    public final static String CL_WRITE_CONF = "writefilter";
    public final static String CL_HELP = "help";
    public final static String CL_SOURCE_FILENAME = "from";
    public final static String CL_DEST_FILENAME = "to";
    public final static String CL_SET = "set";
    public final static String CL_REVERSE_TRANS = "reverse";
    public final static String CL_INTERACTIVE = "interactive";
    public final static String CL_LIST = "list";

    public final static Options cliOptions = new Options()
            .addOption(CL_LOG_LEVEL, true, "log level, integer ranging from 0 (silent) to 4 (debug)")
            .addOption(CL_FILTER_OPTIONS_FILENAME, true, "filter options filename")
            .addOption(CL_SOURCE_FILENAME, true, "source filename")
            .addOption(CL_DEST_FILENAME, true, "destination filename")
            .addOption(CL_HELP, false, "display help message")
            .addOption(CL_WRITE_CONF, true, "write filter to file")
            .addOption(CL_REVERSE_TRANS, false, "from braille to black")
            .addOption(CL_INTERACTIVE, false, "line by line translation")
            .addOption(CL_SET, true, "set filter option --set help for option")
            .addOption(CL_LIST, false, "list filter options");

    private static void displayHelpAndExit(int returnStatus) {
        HelpFormatter formatter = new CliHelp();
        formatter.printHelp("natbraille", cliOptions);
        System.exit(returnStatus);
    }

    /**
     *
     * @param args command line arguments
     */
    public static void main(String[] args) {

        GestionnaireErreur ge = new GestionnaireErreur();

        AfficheurAnsiConsole afficheurConsole = new AfficheurAnsiConsole(LogLevel.NORMAL);

        System.setProperty("javax.xml.xpath.XPathFactory:http://java.sun.com/jaxp/xpath/dom", "com.sun.org.apache.xpath.internal.jaxp.XPathFactoryImpl");

        
        // TODO uncomment next line
        // afficheurConsole.setBlackList(MessageKind.STEP);
        ge.addAfficheur(afficheurConsole);

        CommandLineParser parser = new PosixParser();
        try {

            CommandLine cmd = parser.parse(cliOptions, args);

            if (cmd.hasOption(CL_HELP)) {
                displayHelpAndExit(0);
            }
            if (cmd.hasOption(CL_LIST)) {

                if ((cmd.getOptionValue(CL_LIST) == null) || cmd.getOptionValue(CL_LIST).isEmpty()) {
                    CliHelp.helpOptions();
                } else {
                    CliHelp.helpOptions(cmd.getOptionValue(CL_LIST));
                }
                System.exit(0);
            }
            /*
             * try to set the log level from command line
             */
            try {
                for (String s : cmd.getOptionValues(CL_LOG_LEVEL)) {
                    afficheurConsole.setLogLevel(Integer.parseInt(s));
                    MessageContents mc = MessageContents.Tr("log level set to ''{0}''").setValue(s);
                    ge.afficheMessage(MessageKind.INFO, mc, LogLevel.DEBUG);
                }
                ;
            } catch (Exception e) {
                MessageContents mc = MessageContents.Tr("log level set to ''{0}''").setValue(afficheurConsole.getLogLevel());
                ge.afficheMessage(MessageKind.INFO, mc, LogLevel.VERBOSE);
            }

            /*
             * gather nat filter options from filter files and -set options=value
             */
            NatFilterOptions nfo = new NatFilterOptions();
            try {
                nfo.setOptions(CliFilterOptionTools.getFileFilterOptions(ge, cmd).getAsProperties());
                nfo.setOptions(CliFilterOptionTools.getDirectFilterOptions(ge, cmd).getAsProperties());
                if (cmd.hasOption(CL_REVERSE_TRANS)) {
                    nfo.setOption(NatFilterOption.MODE_DETRANS, "true");
                }
            } catch (Exception e) {
                throw new Exception("cannot parse filter options from command line", e);
            }


            /*
             * if CL_WRITE_CONF is found write conf to <fileNames>
             * and exit
             */
            if (cmd.hasOption(CL_WRITE_CONF)) {

                for (String fileName : cmd.getOptionValues(CL_WRITE_CONF)) {

                    StringBuilder sb = new StringBuilder(" NatBraille generated configuration\n");
                    if (cmd.hasOption(CL_FILTER_OPTIONS_FILENAME)) {
                        for (String s : cmd.getOptionValues(CL_FILTER_OPTIONS_FILENAME)) {
                            sb.append(" base filter options file : '").append(s).append("' \n");
                        }
                    }
                    OutputStream os = null;
                    if (fileName.equals("-")) {
                        os = System.out;
                    } else {
                        MessageContents mc = MessageContents.Tr("options saved to ''{0}''").setValue(fileName);
                        ge.afficheMessage(MessageKind.INFO, mc, LogLevel.NORMAL);
                        os = new FileOutputStream(new File(fileName));
                    }
                    nfo.getAsProperties().store(os, sb.toString());
                }
                System.exit(0);
            }

            if (cmd.hasOption(CL_INTERACTIVE)) {
                /*
                 * build the transcriptor
                 */
                nfo.setOption(NatFilterOption.FORMAT_LINE_LENGTH, "0");
                NatDynamicResolver ndr = new NatDynamicResolver(nfo, ge);
                NatFilterConfigurator configurator = new NatFilterConfigurator().set(nfo).set(ge).set(ndr);
                Transcriptor transcriptor = new Transcriptor(configurator);

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in), 32 * 1024);
                boolean over = false;
                do {
                    System.out.print("natbraille > ");
                    String line = br.readLine();
                    if ((line == null)) {
                        over = true;
                    } else {
                        boolean isCommand = false;
                        Matcher matcher = Pattern.compile("::set (.*)=(.*)").matcher(line);
                        while (matcher.find()) {
                            try {
                                // hum...
                                String name = CliFilterOptionTools.getNatFilterOptionNameFuzzy(matcher.group(1));
                                String value = matcher.group(2);
                                Properties p = new Properties();
                                p.setProperty(name, value);
                                nfo.setOptions(p);
                                ndr = new NatDynamicResolver(nfo, ge);
                                configurator = new NatFilterConfigurator().set(nfo).set(ge).set(ndr);
                                transcriptor = new Transcriptor(configurator);
                                ge.afficheMessage(MessageKind.INFO, MessageContents.Tr("option {0} set to {1}").setValues(name, value), LogLevel.NORMAL);
                                isCommand = true;
                            } catch (Exception e) {
                            }
                        }

                        if ((!isCommand) && (line.length() > 1)) {
                            try {
                                NatDocument indoc = new NatDocument();
                                indoc.setString(line);
                                indoc.forceCharset(Charset.forName(System.getProperty("file.encoding")));
                                NatDocument outDoc = transcriptor.run(indoc);
                                System.out.print(outDoc.getString().replaceAll("\n\n", "\n"));
                            } catch (Exception e) {
                            }
                        }
                    }
                } while (!over);
                System.exit(0);
            } else {
                /*
                 * build the transcriptor
                 */
                NatDynamicResolver ndr = new NatDynamicResolver(nfo, ge);
                NatFilterConfigurator configurator = new NatFilterConfigurator().set(nfo).set(ge).set(ndr);
                Transcriptor transcriptor = new Transcriptor(configurator);

                /*
                 * extract source and destination filenames;
                 */
                CliSourceDestTools.SourceAndDests sd = CliSourceDestTools.extractSourceAndDestinationFileNames(ge, cmd);
                String[] sourceFileNames = sd.getSources();
                String[] destFileNames = sd.getDests();

                /**
                 * run configured filters on inputs
                 */
                for (int idx = 0; idx < sourceFileNames.length; idx++) {

                    MessageContents mc = MessageContents.Tr("processing document ''{0}''").setValue(sourceFileNames[idx]);
                    ge.afficheMessage(MessageKind.INFO, mc, LogLevel.SILENT);

                    try {
                        try {
                            // build a NatDocument
                            NatDocument indoc = new NatDocument();
                            FilenamePlusInfos fpi = new FilenamePlusInfos(sourceFileNames[idx]);

                            if (fpi.getFilePath().toString().equals("-")) {
                                indoc.setInputstream(System.in);
                            } else {
                                indoc.setPath(fpi.getFilePath());
                            }
                            if (fpi.getCharset() != null) {
                                indoc.forceCharset(fpi.getCharset());
                            }
                            if (fpi.getBrailleTable() != null) {
                                indoc.setBrailletable(fpi.getBrailleTable());
                            }

                            // run transcription
                            NatDocument outDoc = transcriptor.run(indoc);

                            // output result
                            if (destFileNames[idx].equals("-")) {
                                System.out.println(outDoc.getString());
                            } else {
                                outDoc.writeFile(new File(destFileNames[idx]));

                                MessageContents mcw = MessageContents.Tr("wrote document ''{0}''").setValue(destFileNames[idx]);
                                ge.afficheMessage(MessageKind.FINAL_SUCCESS, mcw, LogLevel.SILENT);
                            }
                        } catch (Exception e) {
                            ge.afficheMessage(MessageKind.ERROR, new Exception("error while processing '" + sourceFileNames[idx] + "' ", e));
                        }
                    } catch (Exception e) {
                        // display the exception but try to process next
                        // document
                        ge.afficheMessage(MessageKind.ERROR, e);
                    }
                }
            }
        } catch (Exception e) {
            if (e instanceof MissingArgumentException) {
                ge.afficheMessage(MessageKind.ERROR, e, LogLevel.SILENT);
            } else if (e instanceof UnrecognizedOptionException) {
                ge.afficheMessage(MessageKind.ERROR, e, LogLevel.SILENT);
            } else {
                ge.afficheMessage(MessageKind.ERROR, e, LogLevel.SILENT);
            }
            ge.afficheMessage(MessageKind.INFO, MessageContents.Tr("try --help for help"), LogLevel.SILENT);
        }

    }

}
