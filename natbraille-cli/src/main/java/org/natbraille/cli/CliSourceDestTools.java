/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.natbraille.cli;

import java.util.ArrayList;
import org.apache.commons.cli.CommandLine;
import static org.natbraille.cli.NatCommandLine.CL_DEST_FILENAME;
import static org.natbraille.cli.NatCommandLine.CL_SOURCE_FILENAME;
import org.natbraille.core.gestionnaires.GestionnaireErreur;
import org.natbraille.core.gestionnaires.LogLevel;
import org.natbraille.core.gestionnaires.MessageContents;
import org.natbraille.core.gestionnaires.MessageKind;

/**
 *
 * @author vivien
 */
public class CliSourceDestTools {
    
    public static class SourceAndDests {

        private final String[] sources;
        private final String[] dests;

        public SourceAndDests(String[] sources, String[] dests) {
            this.sources = sources;
            this.dests = dests;

        }

        public String[] getSources() {
            return sources;
        }

        public String[] getDests() {
            return dests;
        }
        
    }

    /**
     * extracts sources and destination from CL_SOURCE_FILENAME and
     * CL_DEST_FILENAME options if none is found, non options
     * parameters are used as source filenames and dest filename are created by
     * appending ".natbraille.txt' to source filename. If no source file is
     * found, standard input will be used ; if no destination file is found,
     * standard output will be used. The number of input/output parameters must
     * match or an exception will be thrown
     *
     * @param ge
     * @param cmd
     * @return
     * @throws Exception
     */
    public static SourceAndDests extractSourceAndDestinationFileNames(GestionnaireErreur ge, CommandLine cmd) throws Exception {

        String[] sourceFileNames = cmd.getOptionValues(CL_SOURCE_FILENAME);
        String[] destFileNames = cmd.getOptionValues(CL_DEST_FILENAME);

        // we use non parameters as from source and auto rename output
        if ((sourceFileNames == null) && (destFileNames == null)) {
            ArrayList<String> source = new ArrayList<>();
            ArrayList<String> dest = new ArrayList<>();
            for (Object o : cmd.getArgList()) {
                // System.out.println("object a:"+a);
                FilenamePlusInfos fnp = new FilenamePlusInfos(o.toString());

                String f = fnp.getFilePath().toString();
                String t = f + ".natbraille.txt";
                MessageContents mc = MessageContents.Tr("adding ''{0}'' as source document and '{1}' as destination document").setValues(f.toString(), t.toString());

                ge.afficheMessage(MessageKind.INFO, mc, LogLevel.DEBUG);
                source.add(o.toString());
                dest.add(t);
            }
            sourceFileNames = source.isEmpty() ? (null) : (source.toArray(new String[]{}));
            destFileNames = dest.isEmpty() ? (null) : (dest.toArray(new String[]{}));
        }

        // we use only standard input if no source filename is given
        if ((sourceFileNames == null)) {
            ge.afficheMessage(MessageKind.INFO, MessageContents.Tr("using standard input"), LogLevel.VERBOSE);
            sourceFileNames = new String[]{"-"};
        }

        // we use only standard output if no source filename is given
        if (destFileNames == null) {
            ge.afficheMessage(MessageKind.INFO, MessageContents.Tr("using standard output"), LogLevel.VERBOSE);
            destFileNames = new String[]{"-"};
        }

        // check source and destination number consistency
        if (sourceFileNames.length != destFileNames.length) {
            throw new Exception("arguments number for '" + CL_SOURCE_FILENAME + "' and '" + CL_DEST_FILENAME + "'' must match");

        }

        SourceAndDests sd = new SourceAndDests(sourceFileNames, destFileNames);

        return sd;
    }
}
