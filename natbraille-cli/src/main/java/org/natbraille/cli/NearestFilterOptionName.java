package org.natbraille.cli;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.tools.LevenshteinDistance;

public class NearestFilterOptionName {

    /**
     * for a given string, get string neighbourhood in the option name space
     * (levenshtein distance)
     *
     * @param mysteryOption the approximate option name
     * @param maxGuesses maximum results (no max if null)
     * @param maxDistance maximum Levenshtein distance (no max if null)
     * @return
     */
    public static List<NatFilterOption> find(String mysteryOption, Integer maxGuesses, Integer maxDistance) {

        mysteryOption = mysteryOption.toLowerCase();
        /*
         * class for option ordering
         */
        class OptionDistance implements Comparable<OptionDistance> {

            NatFilterOption option;
            Integer dist;

            @Override
            public int compareTo(OptionDistance arg0) {
                return dist.compareTo(arg0.dist);

            }

            public OptionDistance(NatFilterOption option, Integer dist) {
                this.option = option;
                this.dist = dist;
            }

        }

        /*
         * calculate all options distance from missing option 
         */
        ArrayList<OptionDistance> scores = new ArrayList<>();

        for (NatFilterOption natFilterOption : NatFilterOption.values()) {
            String lowCaseNatFilterOptionName = natFilterOption.name().toLowerCase();

            int distance = LevenshteinDistance.computeLevenshteinDistance(mysteryOption, lowCaseNatFilterOptionName);
            if ((maxDistance == null) || (distance <= maxDistance)) {
                OptionDistance os = new OptionDistance(natFilterOption, distance);
                scores.add(os);
            }
        }

        /*
         * sort scores 
         */
        OptionDistance scoreArray[] = scores.toArray(new OptionDistance[]{});
        Arrays.sort(scoreArray);

        /*
         * make an array of only options
         */
        ArrayList<NatFilterOption> cutSortedOptions = new ArrayList<>();

        int remaining = maxGuesses;
        for (OptionDistance os : scoreArray) {
            if (remaining > 0) {
                cutSortedOptions.add(os.option);
            }
            remaining--;
        }
        return cutSortedOptions;

    }

}
