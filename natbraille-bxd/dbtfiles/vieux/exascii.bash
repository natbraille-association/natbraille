#!/bin/bash
#http://en.wikipedia.org/wiki/ASCII

FILE=Manuel_abrege2013_Vol1.dxb

# ==================== replace some special chars 
# ==================== by xml autoclosing tags

# before : replace & by &amp;
#          replace < by &lt;
#          replace > by &gt;
#          replace " by &quot;
# for free formatting :
#         replace > by &gt;
# then replace chars by tags
# then autoclose all tags

cat $FILE \
    | sed 's/\&/\&amp;/g' \
    | sed 's/</\&lt;/g' \
    | sed 's/>/\&gt;/g' \
    | sed 's/"/\&quot;/g' \
    | sed 's/\x1c/\n<FILE_SEPARATOR>/g' \
    | sed 's/\x1f/<UNIT_SEPARATOR>/g' \
    | sed 's/\x04/<END_OF_TRANSMISSION>/g' \
    | sed 's/\x0a/<LINE_FEED>/g' \
    | sed 's/\x0d/<CARRIAGE_RETURN>/g' \
    | sed 's/\x00/<NULL>\n/g'\
    | sed 's/\x01/\n<START_OF_HEADER>/g' \
    | sed 's/\x02/\n<START_OF_TEXT>/g' \
    | sed 's/\x03/\n<END_OF_TEXT>/g' \
    | sed 's/\x03/\n<END_OF_TEXT>/g' \
    | sed 's/\x08/\n<BACKSPACE>/g' \
    | sed 's/\x0b/\n<VERTICAL_TAB>/g' \
    | sed 's/\x11/\n<DEVICE_CONTROL>/g' \
    | sed 's/\x12/<DEVICE_CONTROL_2>/g' \
    | sed 's/\x13/<DEVICE_CONTROL_3>/g' \
    | sed 's/\x1d/\n<GROUP_SEPARATOR>/g' \
    | sed 's/\x1e/\n<RECORD_SEPARATOR>/g' \
    | sed 's/\xff/\n<FULL_CHARACTER>/g' \
    | sed 's/\x0c/<FORM_FEED>\n/g' \
    | sed 's/>/ \/>/g' \
    | grep . > $FILE.xml

# ==================== pseudo xml'

cat $FILE \
    | sed 's/\&/\&amp;/g' \
    | sed 's/</\&lt;/g' \
    | sed 's/>/\&gt;/g' \
    | sed 's/"/\&quot;/g' \
    | sed 's/\x1c\x00/\n<FILE_SEPARATOR>\x00/g' \
    | sed 's/\x1c/\n<CMD c="/g' \
    | sed 's/\x1f/">\n/g' \
    | sed 's/\x04/<END_OF_TRANSMISSION>/g' \
    | sed 's/\x0a/<LINE_FEED>/g' \
    | sed 's/\x0d/<CARRIAGE_RETURN>/g' \
    | sed 's/\x00/<NULL>\n/g'\
    | sed 's/\x01/\n<START_OF_HEADER>/g' \
    | sed 's/\x02/\n<START_OF_TEXT>/g' \
    | sed 's/\x03/\n<END_OF_TEXT>/g' \
    | sed 's/\x05/\n<INQUIERY>/g' \
    | sed 's/\x06/\n<ACK>/g' \
    | sed 's/\x07/\n<BELL>/g' \
    | sed 's/\x08/\n<BACKSPACE>/g' \
    | sed 's/\x0b/\n<VERTICAL_TAB>/g' \
    | sed 's/\x0e/\n<SHIFT_OUT>/g' \
    | sed 's/\x0f/\n<SHIFT_IN>/g' \
    | sed 's/\x10/\n<DATA_LINK_ESCAPE>/g' \
    | sed 's/\x11/\n<DEVICE_CONTROL>/g' \
    | sed 's/\x12/<DEVICE_CONTROL_2>/g' \
    | sed 's/\x13/<DEVICE_CONTROL_3>/g' \
    | sed 's/\x14/<DEVICE_CONTROL_4>/g' \
    | sed 's/\x15/<NEGATIVE_ACK>/g' \
    | sed 's/\x16/<SYNCHRONOUS_IDLE>/g' \
    | sed 's/\x17/<END_OF_TRANSMISSION_BLOCK>/g' \
    | sed 's/\x18/<CANCEL>/g' \
    | sed 's/\x19/<END_OF_MEDIUM>/g' \
    | sed 's/\x1a/\n<SUBSTITUTE>/g' \
    | sed 's/\x1b/\n<ESCAPE>/g' \
    | sed 's/\x1d/\n<GROUP_SEPARATOR>/g' \
    | sed 's/\x1e/\n<RECORD_SEPARATOR>/g' \
    | sed 's/\xff/\n<FULL_CHARACTER>/g' \
    | sed 's/\x0c/<FORM_FEED>\n/g' \
    | sed 's/>/ \/>/g' \
    | grep . > $FILE.presque.xml


# ==================== toutes les commandes uniques par nombre '
# ==================== d utilisation'

cat $FILE.presque.xml | grep CMD | sort | uniq -c | sort -n \
    >  $FILE.cmdusage.txt

# ==================== le texte seulement'

cat $FILE.presque.xml \
    | sed 's/<LINE_FEED>/\x0a/g' \
    | sed 's/<CARRIAGE_RETURN>/\x0a/g' \
    | sed 's/<FORM_FEED>/\x0a\x0a/g' \
    | grep -v '>' \
    | sed 's/\&lt;/</g' \
    | sed 's/\&gt;/>/g' \
    | sed 's/\&quot;/"/g' \
    > $FILE.txt
    

# ================= html'

# h1-9
# b i u 
cat $FILE.presque.xml \
    | sed 's/<CMD c="\(.*\)\." \/>/<CMD c="\1" \/>/g'  \
    | sed 's/<CMD c="es~\(h.*\)" \/>/<\1>/g' \
    | sed 's/<CMD c="ee~\(h.*\)" \/>/<\/\1>/g' \
    | sed 's/<CMD c="fts~\(.\)" \/>/<\1>/g' \
    | sed 's/<CMD c="fte~\(.\)" \/>/<\/\1>/g' \
    | sed 's/<CMD c="es~\(.*\)" \/>/<span ouvrant class="\1" \/>/g' \
    | sed 's/<CMD c="ee~\(.*\)" \/>/<span fermant class="\1" \/>/g' \
    | sed 's/<CMD c="hds" \/>/<p class="header">/g' \
    | sed 's/<CMD c="hde" \/>/<\/p>/g' \
    | sed 's/<CMD c="\(hl.\)" \/>/<p class="\1">/g' \
    | sed 's/<CMD c="\&lt;" \/>/<p class="\&lt;" >/g'\
    | sed 's/<CMD c="\&gt;" \/>/<p class="\&gt;" >/g'\
    | sed 's/<CMD c="l" \/>/<p class="l" \/>/g' \
    > $FILE.html


    # | sed 's/<LINE_FEED \/>/<p>/g' \
    # | sed 's/<CARRIAGE_RETURN>/<p>/g' \
    # | sed 's/<FORM_FEED>/<p>/g' \
