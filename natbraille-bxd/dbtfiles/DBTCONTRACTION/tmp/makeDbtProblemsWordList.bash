#!/bin/bash

rm ./prob*html -f;

for letter in a b c d e f g h i j k l m n o p q r s t u v w x y z ;
do 
    echo $letter;
    wget http://www.brl.org/refdesk/problem/prob$letter.html   
done

cat prob*html  | grep "<LI>" | grep -v "<LI><" | sed "s/<LI>//" | sed "s/<UL>//" | sed 's/\s*\(.*\)/\1/' > dbtProblemWordsList.txt

rm ./prob*html ;