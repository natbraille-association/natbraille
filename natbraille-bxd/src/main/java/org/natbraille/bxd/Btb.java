/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.natbraille.bxd;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author vivien
 */
public class Btb {

    public static String tCS(String c) {
        String u = "";
        for (int i = 0; i < c.length(); i++) {
            u += tC(c.substring(i, i + 1));
            u += " ";
        }
        return u;
    }

    public static String tC(String c) {
        if (c.matches("[a-zA-Z0-9(),.!?%\\[\\] +-]")) {
            return "'" + c + "'";
        }/* else if (c.codePointAt(0) == 0x1a) {
            return "<SUBS>";
        } else if (c.codePointAt(0) == 0x17) {
            return "<ETB>";
        } */ else {
            return "0x" + Integer.toHexString(c.codePointAt(0));
            //  return "0x" + Integer.toString(c.codePointAt(0),2);
        }
    }

    public static void main(String[] args) throws FileNotFoundException, IOException {

        String path = "/home/vivien/.wine/drive_c/Program Files/Duxbury/DBT 12.3/";
        String[] fileNames = {
            "enuts35.btb", "romacb.btb", "deutcp.btb", "engun52.btb", "somacb.btb", "enube14.btb", "enuncp.btb", "engun26.btb", "enumcd.btb", "engus22.btb", "idaacb.btb", "qindub.btb", "enul0402.btb", "enbtp06.btb", "dzaacb.btb", "enumdi.btb", "kuracb.btb", "enumac.btb", "enul0703.btb", "engbe14.btb", "amel0907.btb", "amhacp.btb", "engun45.btb", "zulucp.btb", "engun42.btb", "enul1001.btb", "enutp04.btb", "qmtncb.btb", "enumcc.btb", "ptbrcb.btb", "ptbrcp.btb", "ctgacp.btb", "kmaacb.btb", "amets43.btb", "ltzacb.btb", "udmacb.btb", "bgaaub.btb", "engun33.btb", "eweacb.btb", "mnaacb.btb", "enuts14.btb", "srpmcb.btb", "engun1a.btb", "sylacp.btb", "elmocb.btb", "mkdacb.btb", "enube10.btb", "urdacb.btb", "amets13.btb", "engun43.btb", "engun37.btb", "enumea.btb", "oriacb.btb", "qbibcp.btb", "amets03.btb", "romacp.btb", "qqphcp.btb", "tlaacp.btb", "enuts09.btb", "sotucb.btb", "javacb.btb", "enuncb.btb", "enuts33.btb", "engun41.btb", "amets45.btb", "engus13.btb", "enufp13.btb", "amets53.btb", "skaacb.btb", "enumba.btb", "tsnucp.btb", "engus21.btb", "engun24.btb", "danscp.btb", "enul0601.btb", "amets25.btb", "qmtncp.btb", "enuts45.btb", "amel1001.btb", "latiub.btb", "enaucb.btb", "engun17.btb", "qipacb.btb", "enufp12.btb", "enumaa.btb", "hraacb.btb", "engus23.btb", "amets42.btb", "enumcf.btb", "tiracp.btb", "amets51.btb", "farscp.btb", "engun4a.btb", "enbfp04.btb", "swatcb.btb", "tamacp.btb", "britic.btb", "jakncb.btb", "enumab.btb", "enumeo.btb", "dzaacp.btb", "enufp17.btb", "kmaacp.btb", "amel0702.btb", "span.btb", "swahil.btb", "maiacp.btb", "enumdc.btb", "uzaacb.btb", "enufp09.btb", "qhyacb.btb", "enumda.btb", "enuts49.btb", "amets49.btb", "tsoucp.btb", "heisup.btb", "enumdf.btb", "amhacb.btb", "amets12.btb", "enbtp08.btb", "spanr.btb", "enumbh.btb", "enumcp.btb", "engun53.btb", "enuts36.btb", "bgaaup.btb", "enufp15.btb", "enbtp04.btb", "engun2a.btb", "amets31.btb", "enumdj.btb", "enul0801.btb", "awaacp.btb", "norscp.btb", "ukracp.btb", "amel0705.btb", "thaicp.btb", "amets20.btb", "engun29.btb", "enube08.btb", "russup.btb", "farscb.btb", "enbfp11.btb", "hyaacb.btb", "kanacp.btb", "amets32.btb", "benbcb.btb", "boaacb.btb", "enbfp14.btb", "enuts38.btb", "amets41.btb", "zhmpcp.btb", "amets28.btb", "amel0706.btb", "engun47.btb", "amerc.btb", "amets10.btb", "urdpcp.btb", "copyb.btb", "amel0402.btb", "enbtp01.btb", "tukacb.btb", "enumbe.btb", "amets34.btb", "enbtp05.btb", "amerf.btb", "mtaacb.btb", "enumcb.btb", "enbfp07.btb", "enumbf.btb", "benbcp.btb", "engbe12.btb", "urdacp.btb", "engun25.btb", "eweacp.btb", "amets14.btb", "hilacp.btb", "enumeb.btb", "enufp11.btb", "vietcp.btb", "enbfp15.btb", "thaicb.btb", "enufp07.btb", "heisub.btb", "enube04.btb", "hauscp.btb", "italup.btb", "enbfp18.btb", "kiracb.btb", "enuts43.btb", "engun23.btb", "belacp.btb", "enutp06.btb", "enumem.btb", "enbfp09.btb", "enuts44.btb", "csaacp.btb", "engbe08.btb", "huaacp.btb", "qcyrub.btb", "enumek.btb", "myaacp.btb", "enumae.btb", "enuts53.btb", "ibaacp.btb", "vietcb.btb", "amets08.btb", "amets11.btb", "korecb.btb", "yorucp.btb", "enuts25.btb", "enumcg.btb", "amets36.btb", "sqiacp.btb", "ibaacb.btb", "benacb.btb", "engun34.btb", "enumee.btb", "enuts32.btb", "nlaacb.btb", "russub.btb", "engun39.btb", "msaacp.btb", "enul0710.btb", "enube05.btb", "enbfp08.btb", "inhacb.btb", "enuts07.btb", "enumec.btb", "amets09.btb", "engun32.btb", "tglacp.btb", "etaacb.btb", "enumeg.btb", "enutp01.btb", "enumdd.btb", "asmacp.btb", "heamup.btb", "kaaacp.btb", "sinacp.btb", "hauscb.btb", "cymrcp.btb", "engun38.btb", "cebucb.btb", "hyaacp.btb", "enmocp.btb", "inhacp.btb", "enmocb.btb", "enutp03.btb", "gae0cp.btb", "franc.btb", "enumdl.btb", "afrucb.btb", "zhyjcp.btb", "elmocp.btb", "enumck.btb", "cym0cp.btb", "nlaacp.btb", "amel0801.btb", "xhoucp.btb", "zhmpcb.btb", "venucb.btb", "enumei.btb", "enbtp10.btb", "loaacb.btb", "ukracb.btb", "sqiacb.btb", "chvacb.btb", "enube07.btb", "engun48.btb", "quebec.btb", "malacp.btb", "enul0705.btb", "amets33.btb", "enumcj.btb", "enuts52.btb", "enbfp01.btb", "amets01.btb", "enuts22.btb", "enumeh.btb", "enuts26.btb", "enumad.btb", "amets24.btb", "amets22.btb", "sotucp.btb", "enutp08.btb", "enufp10.btb", "uzaacp.btb", "qbibcb.btb", "myaacb.btb", "engbe13.btb", "panacp.btb", "finnub.btb", "amets07.btb", "enufp14.btb", "enube06.btb", "enuts18.btb", "ndebcb.btb", "enuts31.btb", "fruncp.btb", "enumbc.btb", "lvaacp.btb", "enuts15.btb", "enufp16.btb", "enuts50.btb", "kkaacp.btb", "sraacb.btb", "amel0601.btb", "enutp05.btb", "nblucp.btb", "engun18.btb", "enumdb.btb", "plaacp.btb", "amel0802.btb", "frabp.btb", "engun27.btb", "snaacb.btb", "engun49.btb", "enuts03.btb", "enuts48.btb", "ltaacb.btb", "enbtp11.btb", "ltzacp.btb", "latiup.btb", "enumen.btb", "kkaacb.btb", "catacp.btb", "enuts40.btb", "enbfp16.btb", "amets52.btb", "enuts11.btb", "engun21.btb", "pionier.btb", "enbrcb.btb", "bhoacp.btb", "enumbd.btb", "isaacp.btb", "svenub.btb", "enuts34.btb", "qhyacp.btb", "mnaacp.btb", "enumed.btb", "maracp.btb", "boaacp.btb", "isaacb.btb", "amets39.btb", "engbe07.btb", "zhtwcp.btb", "amets44.btb", "arabcb.btb", "enumai.btb", "amebpc.btb", "enumcn.btb", "tlaacb.btb", "enuts46.btb", "gujacb.btb", "enuts30.btb", "amel0501.btb", "enumcm.btb", "csaacb.btb", "satacp.btb", "amets46.btb", "hraacp.btb", "enbtp09.btb", "kiracp.btb", "bgcacp.btb", "enbtp02.btb", "kuracp.btb", "telacp.btb", "finnup.btb", "chvacp.btb", "amel0803.btb", "tsoncb.btb", "sanacp.btb", "ttaacp.btb", "enbfp13.btb", "kanacb.btb", "eoaacb.btb", "enuts13.btb", "elcacp.btb", "enube13.btb", "enul1003.btb", "idaacp.btb", "qngucp.btb", "enumef.btb", "slvacp.btb", "catacb.btb", "engun44.btb", "enutp00.btb", "gaelcp.btb", "engbe04.btb", "enbtp07.btb", "araacp.btb", "enul0702.btb", "engbe10.btb", "qqphcb.btb", "urdpcb.btb", "enuts51.btb", "tralp01.btb", "enumbj.btb", "lvaacb.btb", "enumch.btb", "oriacp.btb", "enul0704.btb", "enuts28.btb", "enufp05.btb", "enuts24.btb", "danscb.btb", "sckacp.btb", "mniacp.btb", "msaacb.btb", "afrucp.btb", "enul0802.btb", "amets15.btb", "enuts39.btb", "amets19.btb", "engus33.btb", "amets35.btb", "bosacp.btb", "javacp.btb", "tralp02.btb", "telacb.btb", "prsacp.btb", "engus25.btb", "deutcb.btb", "engun19.btb", "skaacp.btb", "ttaacb.btb", "arab.btb", "etaacp.btb", "magacp.btb", "enumdh.btb", "nepacp.btb", "tralp03.btb", "huaacb.btb", "enumci.btb", "enumej.btb", "korecp.btb", "enbfp17.btb", "amel0710.btb", "engus11.btb", "sraacp.btb", "sinacb.btb", "enufp18.btb", "spancb.btb", "amets50.btb", "azeacp.btb", "azeacb.btb", "enumco.btb", "tamacb.btb", "engus12.btb", "amets30.btb", "traacp.btb", "sswucp.btb", "enumbi.btb", "enuts19.btb", "enul0903.btb", "mtaacp.btb", "amets26.btb", "tgkacp.btb", "engun31.btb", "enumce.btb", "enul0403.btb", "engbpc.btb", "enuts01.btb", "enumel.btb", "amel1002.btb", "enul0901.btb", "enumaf.btb", "engbe11.btb", "enbfp05.btb", "yorucb.btb", "enaucp.btb", "amets18.btb", "amets38.btb", "sndacp.btb", "enube11.btb", "enumbg.btb", "traacb.btb", "amets40.btb", "elcacb.btb", "enufp04.btb", "gaelcb.btb", "engus31.btb", "italub.btb", "amel0403.btb", "enumca.btb", "enube09.btb", "amets29.btb", "loaacp.btb", "enuts17.btb", "enumbb.btb", "engus34.btb", "ltaacp.btb", "enul0503.btb", "sparcb.btb", "enbfp10.btb", "enufp01.btb", "enuts20.btb", "enutp07.btb", "panacb.btb", "norscb.btb", "plaacb.btb", "jakncp.btb", "qipacp.btb", "gujacp.btb", "enumcl.btb", "enuts42.btb", "enumdk.btb", "engun13.btb", "pusacp.btb", "udmacp.btb", "amel0901.btb", "enuts41.btb", "venucp.btb", "enbtp12.btb", "heamub.btb", "snaacp.btb", "eoaacp.btb", "enumde.btb", "kruacp.btb", "hinacb.btb", "enbrcp.btb", "engbe06.btb", "engun11.btb", "tiracb.btb", "enutp12.btb", "zhtwcb.btb", "engus32.btb", "engun51.btb", "fruncb.btb", "enbfp12.btb", "cymrcb.btb", "tukacp.btb", "cebucp.btb", "kaaacb.btb", "tralp04.btb", "africb.btb", "enuts29.btb", "enuts08.btb", "engus24.btb", "enuts12.btb", "enumah.btb", "engbe09.btb", "briafc.btb", "engun15.btb", "svenup.btb", "enbtp03.btb", "zhyjcb.btb", "enutp09.btb", "enumag.btb", "amets48.btb", "enutp11.btb", "amets17.btb", "engun22.btb", "hinacp.btb", "enutp02.btb", "engus14.btb", "engbe05.btb", "enufp06.btb", "enumdg.btb", "enbfp06.btb", "engun46.btb", "qngucb.btb", "enul0707.btb", "somacp.btb", "enuts10.btb", "enutp10.btb", "malacb.btb", "swahcb.btb", "enufp08.btb", "iloacp.btb", "mkdacp.btb", "enul0501.btb", "srpmcp.btb", "enube12.btb", "benacp.btb"

        };
        List<String> strs = new ArrayList();
//        for (int i = 0; i < 16; i++) {
        for (String fn : fileNames) {
            // System.out.println("====================================" + fn);
            String string = IOUtils.toString(new FileInputStream(path + fn), "ascii");
            //System.out.println(string.substring(0,108));

            strs.add(string);
        }
        //      }
        // 108 : G
        if (false) {
            for (int i = 0; i < 18000; i++) {
                int countko = 0;
                Map<String, Integer> poss = new LinkedHashMap<>();

                boolean same = true;
                String last = null;
                int nfiles = 0;
                for (String s : strs) {
                    if (i + 1 < s.length()) {
                        nfiles++;
                        String u = s.substring(i, i + 1);
                        if (last == null) {
                            last = s.substring(i, i + 1);
                        }
                        Integer rr = poss.get(u);
                        poss.put(u, (rr == null) ? 1 : rr + 1);
                    }
                }
                same = (poss.keySet().size() == 1);
                //System.out.print("\n");

                System.out.print("(" + nfiles + " files)");
                System.out.print("@" + i + " ");
                if (same) {
                    System.out.print("⃝" + tC(last));
                } else {
                    for (String x : poss.keySet()) {
                        System.out.print(tC(x));
                        System.out.print("(" + poss.get(x) + ") ");
                    }
                }
                System.out.print("\n");

                //System.out.print("\n");
            }
        }
        /////////
        int gramsize = 4;

        Map<String, Integer> ngrams = new LinkedHashMap<>();
        strs.forEach(s -> {
            for (int i = 0; i + gramsize < s.length(); i++) {
                String ng = s.substring(i, i + gramsize);
                Integer inmap = ngrams.get(ng);

                ngrams.put(ng, 1 + (inmap == null ? 0 : inmap));

            }
        });
        String[] a = ngrams.keySet().toArray(new String[ngrams.size()]);

        //String[] pour = Arrays.asList(a).sort( (y,z) ->(ngrams.get(y) - ngrams.get(z)) );
        List<String> pour = Arrays.asList(a);
        pour.sort((String o1, String o2) -> ngrams.get(o2) - ngrams.get(o1));
        for (String p : pour) {
            int count = ngrams.get(p);
            if (count > 10) {
                System.out.println(tCS(p) + " " + count);
            }
        }

        //}).toArray(new String[ngrams.size()]);
    }
}
