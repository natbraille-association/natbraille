/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.natbraille.bxd;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.io.IOUtils;
import org.natbraille.bxd.BinaryDeEncoder.BxdDocument;

/**
 *
 * @author vivien
 */
public class Typeset {

    //
    // encoding
    //
    String bytesToString(byte[] bytes) {
        try {
            return IOUtils.toString(new ByteArrayInputStream(bytes), "ascii");
        } catch (IOException ex) {
            Logger.getLogger(Typeset.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }

    int bytesToInt(byte[] bytes) {
        return Integer.parseInt(bytesToString(bytes));
    }
    final BxdDocument source;

    //
    // styles
    // 
    Map<String, byte[]> stylesStarts = new HashMap();
    Map<String, byte[]> stylesEnds = new HashMap();

    private void buildStyles() throws IOException {
        int styleCount = source.styleSection.styleStructureHeader.number;
        byte[][] styleNames = source.styleSection.names;
        byte[][] styleDefinitions = source.styleSection.definitions;
        short[] styleOrders = source.styleSection.order;

        for (int i = 0; i < styleCount; i++) {
            String name = bytesToString(styleNames[i]);
            int order = styleOrders[i];
            stylesStarts.put(name, styleDefinitions[2 * order]);
            stylesEnds.put(name, styleDefinitions[2 * order + 1]);
        }
    }

    //
    // output document
    //
    StringBuilder document = new StringBuilder();

    private class Props {

        int paragraphStartOffset = 3;
        int paragraphContinationOffset = 1;
        boolean inParagraph = false;
        int pageWidth = 16;
        int pageHeight = 10;
        int currentLine = 0;
        int currentLineOffset = 0;
        int currentPage = 1;
        int lastWordGroupStart = -1;
        int lastWordGroupStop = -1;
        int defaultCenteredLineSkip = 5;
        int defaultCenteredPageSkip = 2;
        char pagePrefix = 'p';
        boolean NO_NEWLINE_NEWPAGE = true;
        int[] tabStops = {5, 10, 15, 20};

        // TODO
        int alignement = Consts.ALIGN_LEFT;
        // TODO
        String grade = "2";
        String lockedGrade = "";
        // TODO
        int wordBreaking = Consts.WORD_BREAK_NORMAL;
        private float verticalCenteringRatio = -1;
        private int verticalCentringStartOffset = -1;
        //
        boolean DEBUG_COMMAND = false;
    }

    private static class Consts {

        static int ALIGN_LEFT = 1;
        static int ALIGN_CENTER = 2;
        static int ALIGN_RIGHT = 3;

        static int WORD_BREAK_NORMAL = 1;
        static int WORD_BREAK_COMPUTER = 2;
        static int WORD_BREAK_NONE = 3;
    }

    Props props = new Props();

    boolean isOnLineStart() {
        return (props.currentLineOffset == 0);
        // or just tabs ?
    }

    void insChar(char c) {

        document.append(c);
        props.currentLineOffset++;
        if (props.currentLineOffset == props.pageWidth) {
            insNewline();
            if (props.currentLineOffset == 0) {
                if (props.inParagraph) {
                    insPadding('~', props.paragraphContinationOffset);
                }
            }
        }
    }

    void insPadding(char c, int count) {
        int max = props.pageWidth - props.currentLineOffset;
        if (count > max) {
            count = max;
        }
        while (count-- > 0) {
            insChar(c);
        }
    }

    void insNewline() {
        document.append("\n");
        props.currentLine++;
        props.currentLineOffset = 0;
        if (props.currentLine == props.pageHeight) {
            if (props.NO_NEWLINE_NEWPAGE) {
                document.delete(document.length() - 1, document.length());
            }
            insNewPage();
        }

    }

    void insNewPage() {
        document.append("\f");
        props.currentPage++;
        props.currentLine = 0;
        props.currentLineOffset = 0;

        boolean printpagenum = true;
        if (printpagenum) {
            String p = props.pagePrefix + Integer.toString(props.currentPage);
            int padSize = props.pageWidth - p.length();
            while (padSize-- > 0) {
                insChar(' ');
            }
            char[] chars = p.toCharArray();
            for (char c : chars) {
                insChar(c);
            }
        }
    }

    void insParagraph() {
        insNewline();
        props.inParagraph = true;
        insPadding('#', props.paragraphStartOffset);

    }

    private int getNextTabStop() {
        for (int pos : props.tabStops) {
            if (pos > props.currentLineOffset) {
                return pos;
            }
        }
        return -1;
    }

    void gotoNextTabStop() {
        int pos = getNextTabStop();
        if (pos != -1) {
            int offset = (pos - props.currentLineOffset);
            insPadding('→', offset);
        }
    }

    void insContent(char c) {
        insChar(c);
    }

    void conditionalHardReturn() {
        if (!isOnLineStart()) {
            insNewline();
        }
        props.inParagraph = false;
    }

    void hardReturn() {
        insNewline();
        props.inParagraph = false;
    }

    void insCommandDebug(String s) {
        if (props.DEBUG_COMMAND) {

            String meta = String.format("{{%1s}}", s);
            char[] chars = meta.toCharArray();
            for (char c : chars) {
                insChar(c);
            }

        }

    }

    void wordGroupStart() {
        // TODO
        props.lastWordGroupStart = document.length();
    }

    void wordGroupEnd() {
        // TODO
        props.lastWordGroupStop = document.length();
        {
            props.lastWordGroupStart = -1;
            props.lastWordGroupStart = -1;
        }
    }

    void centeredHeadingStart(int minRemainingLines, int lineSkip) {

        if (minRemainingLines == -1) {
            minRemainingLines = props.defaultCenteredPageSkip;
        }
        if (lineSkip == -1) {
            lineSkip = props.defaultCenteredLineSkip;
        }
        props.alignement = Consts.ALIGN_CENTER;
        int remainingLines = props.pageHeight - props.currentLine - 1;
        if (remainingLines < (minRemainingLines + 1)) {
            insNewPage();
        } else {
            for (int i = 0; i < lineSkip; i++) {
                insNewline();
            }
        }

    }

    void centeredHeadingEnd() {
        props.alignement = Consts.ALIGN_LEFT;
    }

    private void lockGrade(String grade) {
        if (props.lockedGrade.isEmpty()) {
            props.lockedGrade = grade;
        }
    }

    private void unlockGrade(String grade) {
        if (props.lockedGrade.equals(grade)) {
            props.lockedGrade = "";
        }
    }

    private void setGrade(String grade) {
        if (props.lockedGrade.isEmpty()) {
            props.grade = grade;
        }
    }

    private void normalWordBreaking() {
        props.wordBreaking = Consts.WORD_BREAK_NORMAL;
    }

    private void computerBrailleWordBreaking() {
        props.wordBreaking = Consts.WORD_BREAK_COMPUTER;
    }

    private void noWordBreaking() {
        props.wordBreaking = Consts.WORD_BREAK_NONE;
    }

    private void verticalCenteringStart(int M, int N) {
        props.verticalCenteringRatio = ((float) N / (float) M);
        props.verticalCentringStartOffset = document.length();

    }

    private int findPageStartOffset(int offset) {
        int idx = document.substring(0, offset).lastIndexOf("\f");
        return idx == -1 ? 0 : idx;
    }

    private int findPageEndOffset(int offset) {
        int idx = document.indexOf("\f", offset);
        return idx == -1 ? document.length() : idx;
    }

    private int findLineCount(int startOffset, int endOffset) {
        int count = 0;
        boolean stop = false;
        int idx;
        do {
            idx = document.indexOf("\n", startOffset);
            if ((idx != -1) && (idx < endOffset)) {
                count++;
                startOffset = idx + 1;
            } else {
                stop = true;
            }
        } while (!stop);
        return count;
    }

    private void verticalCenteringEnd() {
        if ((props.verticalCenteringRatio != -1) && (props.verticalCentringStartOffset != -1)) {
            // first page ;
            int coffset = props.verticalCentringStartOffset;
            do {
                int psi = findPageStartOffset(coffset);
                int pei = findPageEndOffset(coffset);
                int nlines = findLineCount(psi, pei);
                int nEmptyLines = props.pageHeight - nlines;
                int nTopEmptyLines = (int) Math.floor(nEmptyLines * props.verticalCenteringRatio);
                int dc = nTopEmptyLines;
                while (dc > 0) {
                    document.insert(psi, "\n");
                    dc--;
                }
                coffset = pei + nTopEmptyLines + 1;
            } while (coffset < document.length());
        }
        props.verticalCenteringRatio = -1;
        props.verticalCentringStartOffset = -1;
    }

    private void newPage(int newPageNumber, String pagePrefix) {
        if (pagePrefix != null) {
            props.pagePrefix = pagePrefix.charAt(0);
        }
        if (newPageNumber >= 0) {
            props.currentPage = newPageNumber - 1;
        }
        insNewPage();

    }

    private void conditionalNewPage() {
        boolean numberingOnTop = true;
        int firstContentLine = numberingOnTop ? 1 : 0;
        if ((props.currentLine == firstContentLine)
                && (props.currentLineOffset == 0)) {
            insNewPage();
        }

    }

    private void setHierarchy(int L, int V, int R, int I, int J, int K) {
    }

    private void setHierarchy(int L, int V, int R, int I, int J, int K, int M, int N, int O, int P) {
    }

    //
    // commands
    //
    @FunctionalInterface
    public interface CommandExecutor {

        void execute(Matcher matcher);
    }

    //  linked for entryset order consistency;
    Map< Pattern, CommandExecutor> patterns = new LinkedHashMap< Pattern, CommandExecutor>() {
        {
            put(Pattern.compile("^es~(?<name>(.*))$"), matcher -> {
                byte[] commands = stylesStarts.get(matcher.group("name"));
                if (commands != null) {
                    parse(commands);
                }
            });
            put(Pattern.compile("^ee~(?<name>(.*))$"), matcher -> {
                byte[] commands = stylesEnds.get(matcher.group("name"));
                if (commands != null) {
                    parse(commands);
                }
            });
            put(Pattern.compile("^l$"), matcher -> {
                conditionalHardReturn();
            });
            put(Pattern.compile("^<$"), matcher -> {
                hardReturn();
            });
            put(Pattern.compile("^p$"), matcher -> {
                insParagraph();
            });
            put(Pattern.compile("^>$"), matcher -> {
                // hard newline
                gotoNextTabStop();
            });
            put(Pattern.compile("^:$"), matcher -> {
                wordGroupStart();
            });
            put(Pattern.compile("^;$"), matcher -> {
                wordGroupEnd();
            });
            put(Pattern.compile("^[hH][dD][sS](?<minRemainingLine>(\\d+))?(?<lineSkip>(\\d+))?$"), matcher -> {
                centeredHeadingStart(
                        matcher.group("minRemainingLine") == null ? -1 : Integer.parseInt(matcher.group("minRemainingLine")),
                        matcher.group("lineSkip") == null ? -1 : Integer.parseInt(matcher.group("lineSkip"))
                );
            });
            put(Pattern.compile("^[hH][dD][eE]$"), matcher -> {
                centeredHeadingEnd();
            });
            put(Pattern.compile("^[g](?<N>(1|1.5|2))?(?<L>[lu])?$"), matcher -> {
                if (matcher.group("L") != null) {
                    if (matcher.group("L").equals("l")) {
                        lockGrade(matcher.group("N"));
                    } else {
                        unlockGrade(matcher.group("N"));
                    }
                } else {
                    setGrade(matcher.group("N"));
                }
            });
            put(Pattern.compile("^((vp)|(wb)|(mf))(-(?<type>(cb|no)))?$"), matcher -> {
                if (matcher.group("type") == null) {
                    normalWordBreaking();
                } else {
                    if ("cb".equals(matcher.group("type"))) {
                        computerBrailleWordBreaking();

                    } else {
                        noWordBreaking();
                    }

                }
            });
            put(Pattern.compile("^[v][c][s]((?<M>(\\d+))(?<N>(\\d+))?)?$"), matcher -> {
                String M = matcher.group("M");
                String N = matcher.group("N");
                verticalCenteringStart(
                        M == null ? 2 : Integer.parseInt(M),
                        N == null ? 1 : Integer.parseInt(N)
                );
            });
            put(Pattern.compile("^[v][c][e]$"), matcher -> {
                verticalCenteringEnd();
            });
            put(Pattern.compile("^[p][g](?<N>(\\d+))?(~(?<X>(.)))?$"), matcher -> {
                String N = matcher.group("N");
                String X = matcher.group("X");
                newPage(
                        N == null ? -1 : Integer.parseInt(N),
                        X
                );
            });

            put(Pattern.compile("^[h][i]"
                    + "(?<L>(\\d+))" // 1 left margin
                    + "(?<V>(\\d+))" // 1 overflow (runnover) margin
                    + "(?<R>(\\d+))" // 0 right margin
                    + "(?<I>(-?\\d+))" // 0 left margin increment 
                    + "(?<J>(-?\\d+))" // 0 overflow (runnover) margin increment
                    + "(?<K>(-?\\d+))" // 0 right margin increment
                    + "("
                    + "(?<M>(-?\\d+))" //  -1 min (if<0) or max indent level
                    + "(?<N>(-?\\d+))" //  -1 min (if<0) or max left margin
                    + "(?<O>(-?\\d+))" //  -1 min (if<0) or max runnerver margin
                    + "(?<P>(-?\\d+))" //  -1 min (if<0) or max right margin
                    + ")?"
                    + "$"), matcher -> {
                        int L = Integer.parseInt(matcher.group("L"));
                        int V = Integer.parseInt(matcher.group("V"));
                        int R = Integer.parseInt(matcher.group("R"));

                        int I = Integer.parseInt(matcher.group("I"));
                        int J = Integer.parseInt(matcher.group("J"));
                        int K = Integer.parseInt(matcher.group("K"));

                        if (matcher.group("M") == null) {
                            setHierarchy(L, V, R, I, J, K);
                        } else {
                            int M = Integer.parseInt(matcher.group("M"));
                            int N = Integer.parseInt(matcher.group("N"));
                            int O = Integer.parseInt(matcher.group("O"));
                            int P = Integer.parseInt(matcher.group("P"));
                            setHierarchy(L, V, R, I, J, K, M, N, O, P);
                        }

                    }
            );
            put(Pattern.compile("^top$"), matcher -> {
                conditionalNewPage();
            });
            put(Pattern.compile("^(.+)$"), matcher -> {
                System.out.println("<??PASSED??>" + matcher.group(0));
            });

        }

    };

    public Typeset(BxdDocument source) throws IOException {
        this.source = source;
        buildStyles();
    }

    //
    // Parsing
    //
    public void applyCommand(byte[] cmd) {
        String cmdString = bytesToString(cmd);
        applyCommand(cmdString);
    }

    public void applyCommand(String cmdString) {
        for (Map.Entry<Pattern, CommandExecutor> ks : patterns.entrySet()) {
            Pattern pattern = (Pattern) ks.getKey();
            Matcher matcher = pattern.matcher(cmdString);
            if (matcher.find()) {
                // System.out.println("<command>:" + matcher.group(0));
                insCommandDebug(matcher.group(0));
                CommandExecutor executor = ks.getValue();
                executor.execute(matcher);
                return;
            }
        }
    }

    public void applyText(byte character) {
        char c = (char) character;
        insContent(c);
    }

    public void parse(byte[] a) {

        int p = 0;
        while (a.length > p) {
            if (a[p] == 0x1c) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                p++;
                while ((a.length > p) && (a[p] != 0x1f)) {
                    baos.write(a[p]);
                    p++;
                }
                byte[] cmd = baos.toByteArray();
                this.applyCommand(cmd);
            } else {
                this.applyText(a[p]);
            }
            p++;
        }
    }

    static byte[] commandBytes(String command) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            baos.write(0x1c);
            baos.write(textBytes(command));
            baos.write(0x1f);
        } catch (IOException ex) {
            Logger.getLogger(Typeset.class.getName()).log(Level.SEVERE, null, ex);
        }
        return baos.toByteArray();
    }

    static byte[] textBytes(String command) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        char[] chars = command.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            baos.write(chars[i]);
        }
        return baos.toByteArray();
    }

    //
    // Main
    //
    public static void main(String[] args) throws FileNotFoundException, Exception {

        // BinaryDeEncoder.BxdDocument bxdDocument = BinaryDeEncoder.checkAndDecode(new FileInputStream(file));
        //Document doc = new Document(XmlEncoder.toXML(bxdDocument, "UTF-8")); // "extended ascii !!!"....
        //doc.get
        File file = new File(
                "dbtfiles/perso/Manuel_abrege2013_Vol1.dxb"
        // "dbtfiles/hyph/hyphentable_dashesandhyphen.dxp"
        // "dbtfiles/inja/Envoi Vivien/Physique Term. - Le centre d'inertie.dxb"
        );
        BinaryDeEncoder.BxdDocument bxdDocument = BinaryDeEncoder.decode(new FileInputStream(file));
        int v = bxdDocument.headerSection.documentMajorVersion;
        System.out.println("version "
                + bxdDocument.headerSection.documentMajorVersion
                + "."
                + bxdDocument.headerSection.documentMinorVersion
        );

        Typeset ts = new Typeset(bxdDocument);
        System.out.println("================================ unknwown tags");

        if (false) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            for (int i = 1; i < 21; i++) {
                baos.write(textBytes(Integer.toString(i)));
                baos.write(commandBytes("<"));
            }
            baos.write(textBytes("0123456789abcdef"));
            baos.write(commandBytes("hi5:"));
            baos.write(commandBytes("<"));
            baos.write(textBytes("ABCD"));
            baos.write(commandBytes("vcs"));
            baos.write(textBytes("ABCD"));
            baos.write(commandBytes("pg1"));
            baos.write(commandBytes("vce"));
            baos.write(commandBytes("pg32"));
            baos.write(commandBytes("p"));
            baos.write(textBytes("SAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"));
            baos.write(textBytes("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"));
            baos.write(textBytes("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"));
            baos.write(textBytes("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"));
            baos.write(textBytes("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"));
            baos.write(textBytes("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"));
            baos.write(textBytes("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAM"));
            baos.write(commandBytes("p"));
            baos.write(textBytes("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"));
            baos.write(textBytes("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAM"));
            ts.parse(baos.toByteArray());
        } else {
            ts.parse(bxdDocument.contentsSection.text);
        }
        System.out.println("================================ doc start");

        String pp = "";
        for (int i = 0; i < ts.props.pageWidth; i++) {
            //   pp += Integer.toString(i%10);
            pp += "-";
        }
        System.out.println(ts.document.toString()
                .replace("\f", "\n" + pp + "\n"));
        System.out.println("================================ doc end");

    }

}
