/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.bxd;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import org.apache.commons.io.IOUtils;

/**
 * Binary decoding and encoding of "the Duxbury Braille Translator (DBT)" files
 * used up to version 11.1.
 * <p>
 * Use {@link decode} and {@link checkAndDecode} static methods to build a
 * structured in-memory representation. Section can be modified, and the written
 * back to a binary DBT file.
 * <p>
 * Further transformations can be made in between on the xml representation
 * build by {@link XmlEncoder} and imported back as a in-memory representation.
 * <p>
 * The byte data is divided in following main sections (See the source code for
 * detailed format)
 * <ol>
 * <li>
 * <p>
 * The header section includes :
 * <ul>
 * <li>version number and subnumber</li>
 * <li>Braille or print format</li>
 * <li>page numbering configuration</li>
 * <li>translation tables</li>
 * <li>hyphenations infos</li>
 * <li>...</li>
 * </ul>
 *
 * <li>
 * <p>
 * The contents section is the document formated text. Text format is as follows
 * <ul>
 * <li>byte 0x0c opens a command</li>
 * <li>byte 0x0f closes a command</li>
 * <li>text between command open and close is a command name</li>
 * <li>text ouside open and close bound is raw text</li>
 * <li>command names starting with es~ are user style start</li>
 * <li>command names starting with ee~ are user style end</li>
 * </ul>
 *
 * <li>
 * <p>
 * The styles sections defines user styles. Each user style has a name and two
 * definitions, one for user style start commands, the other for user style end
 * commands. Both of start and end definition defines a sequence of system
 * commands.
 * <p>
 * System commands include the leading (byte) 0x0c and ending (byte) 0x0f.
 * <p>
 * The style section is divided in
 * <ul>
 * <li>A section block header #1</li>
 * <li>the order of styles (nb of styles found in #1)</li>
 * <li>the list of style names (size of list found in #1)</li>
 * <li>A section block header #2</li>
 * <li>the list of style definition (num and size found in #2</li>
 * </ul>
 * <li><p>
 * The printer section define printer configurations. No extraction.
 * </ol>
 *
 * @author vivien
 */
public class BinaryDeEncoder {

    protected static int getLengthToZero(ByteBuffer bb, int startOffset) {
        for (int i = startOffset; i < bb.capacity(); i++) {
            if (bb.get(i) == 0x00) {
                return (i - startOffset);
            }
        }
        return 0;
    }

    /**
     * HeaderSection Section.
     */
    public static class HeaderSection {

        // magic number
        public static final byte[] BXD_MAGIC = new byte[]{(byte) 0xFF, (byte) 0x44, (byte) 0x53, (byte) 0x49};

        private static byte[] getMagic(ByteBuffer bb) {
            byte foundMagic[] = new byte[BXD_MAGIC.length];
            for (int i = 0; i < foundMagic.length; i++) {
                foundMagic[i] = bb.get(i);
            }
            return foundMagic;
        }

        public static boolean checkMagic(ByteBuffer bb) {
            byte found[] = getMagic(bb);
            if (found.length != BXD_MAGIC.length) {
                return false;
            }
            for (int i = 0; i < BXD_MAGIC.length; i++) {
                if (BXD_MAGIC[i] != found[i]) {
                    return false;
                }
            }
            return true;
        }

        // document major version 
        private static final int OFF_DOCUMENT_MAJOR_VERSION = 0x04;
        public static final short BXD_DOCUMENT_MAJOR_VERSION_2 = 0x02;

        private static short getDocumentMajorVersion(ByteBuffer bb) {
            return bb.getShort(OFF_DOCUMENT_MAJOR_VERSION);
        }
        // document type
        private static final int OFF_DOCUMENT_TYPE = 0x06;
        public static final short BXD_DOCUMENT_TYPE_BRAILLE = (short) 0x00;
        public static final short BXD_DOCUMENT_TYPE_PRINT = (short) 0x01;

        private static short getDocumentType(ByteBuffer bb) {
            return bb.getShort(OFF_DOCUMENT_TYPE);
        }

        // document minor version
        private static final int OFF_DOCUMENT_MINOR_VERSION = 0x08;
        public static final short BXD_DOCUMENT_MINOR_VERSION_0 = (short) 0x00;
        public static final short BXD_DOCUMENT_MINOR_VERSION_1 = (short) 0x01;

        private static short getDocumentMinorVersion(ByteBuffer bb) {
            return bb.getShort(OFF_DOCUMENT_MINOR_VERSION);
        }

        /*
         * configuration
         */
        //  numbering type
        private static final int OFF_NUMBERING_EVEN = 0x0a;
        private static final int OFF_NUMBERING_ODD = 0x0c;
        private static final int OFF_NUMBERING_REF_EVEN = 0x0e;
        private static final int OFF_NUMBERING_REF_ODD = 0x10;
        private static final int OFF_NUMBERING_FIRST = 0x12;

        public static final short BXD_NUMBERING_NONE = 0x00;
        public static final short BXD_NUMBERING_UPPER_LEFT = 0x01;
        public static final short BXD_NUMBERING_UPPER_RIGHT = 0x02;
        public static final short BXD_NUMBERING_LOWER_LEFT = 0x03;
        public static final short BXD_NUMBERING_LOWER_RIGHT = 0x04;

        private static short getNumbering(ByteBuffer bb, int numberingType) {
            return bb.getShort(numberingType);
        }

        // maybe structure header. Always 0x00 0x08
        private static final int OFF_PADx14x17 = 0x14;
        public static final short[] BXD_PAD_0x14_0x17_ALWAYS = {(short) 0x00, (short) 0x08};

        private static short[] getPadShort_0x14_0x17(ByteBuffer bb) {
            short[] pad = new short[2];
            for (int offset = 0; offset < 2; offset++) {
                pad[offset] = bb.getShort(OFF_PADx14x17 + offset * 2);
            }
            return pad;
        }

        /* unknown... related to conversion ?
         * the value found is most often (short)0x00.
         * the value is (short)0x00 only for document where minorVersion=1 and
         * and documenttype=1, that seem not to have been translated by dbt. In
         * corpus, when dxb and dxp versions exists, the dxp version has value 0x01
         * and the dxb version 0x00.
         */
        private static final int OFF_PAD_0x18_0x19 = 0x18;
        public static final short BXD_PAD_0x19_variant0 = (short) 0x00;
        public static final short BXD_PAD_0x19_variant1 = (short) 0x01;

        private static short getPadShort_0x18_0x19(ByteBuffer bb) {
            return bb.getShort(OFF_PAD_0x18_0x19);
        }

        // hyphen table
        private static final int OFF_HYPHEN_TABLE = 0x1a;
        public static final short BXD_HYPHEN_TABLE_NONE = 0x00;
        public static final short BXD_HYPHEN_TABLE_DASHES = 0x01;
        public static final short BXD_HYPHEN_TABLE_DASHES_AND_HYPHENS = 0x02;
        public static final short BXD_HYPHEN_TABLE_FRENCH = 0x03;

        private static short getHyphenTable(ByteBuffer bb) {
            return bb.getShort(OFF_HYPHEN_TABLE);
        }
        // contents section size
        private static final int OFF_CONTENTS_SECTION_SIZE = 0x1c;

        private static int getContentsSectionSize(ByteBuffer bb) {
            return bb.getInt(OFF_CONTENTS_SECTION_SIZE);
        }
        // probablesize ??
        private static final int OFF_PAD_0x20 = 0x20;

        private static int getPadInt_0x20(ByteBuffer bb) {
            return bb.getInt(OFF_PAD_0x20);
        }
        // unknown
        // values may depend on minor version ?
        private static final int OFF_PAD_0x24_0x27 = 0x24;
        public static final short[] BXD_PAD_0x24_0x27_ZEROS = new short[2];

        private static short[] getPadShort_0x24_0x27(ByteBuffer bb) {
            short[] pad = new short[2];
            for (int i = 0; i < pad.length; i++) {
                pad[i] = bb.getShort((int) OFF_PAD_0x24_0x27 + i*2);
            }
            return pad;
        }

        // dictionary ref number ?
        private static final int OFF_PAD_0x28 = 0x28;
        public static final byte BXD_PAD_0x28 = 0x04;

        private static byte getPadByte_0x28(ByteBuffer bb) {
            return bb.get(OFF_PAD_0x28);
        }

        // dictionnary references
        private static final int OFF_DICTIONNARY_REFS = 0x29;
        public static final byte[][] BXD_DICTIONNARY_REFS_FRENCH = {
            new byte[]{(byte) 0xb1, 'F', 'R', 'A', 'N', 'C', (byte) 0x00, (byte) 0x00},
            new byte[]{(byte) 0x00, 'F', 'R', 'A', 'D', 'X', 'P', (byte) 0x00},
            new byte[]{(byte) 0x00, 'F', 'R', 'A', 'B', 'P', (byte) 0x00, (byte) 0x00},
            new byte[]{(byte) 0x00, 'G', 'E', 'N', 'D', 'X', 'P', (byte) 0x00}
        };

        private static byte[][] getDictionaryRefs(ByteBuffer bb) {
            int startOffset = OFF_DICTIONNARY_REFS;
            byte dictionaries[][] = new byte[4][8];
            for (int dicoNum = 0; dicoNum < dictionaries.length; dicoNum++) {
                byte[] dictionary = dictionaries[dicoNum];
                for (int i = 0; i < dictionary.length; i++) {
                    dictionary[i] = bb.get(startOffset);
                    startOffset += 1;
                }
                dictionaries[dicoNum] = dictionary;
            }
            return dictionaries;
        }

        // unknown
        private final static int OFF_PADx49xea = 0x49;
        public final static byte[] BXD_PAD_0x49_0xea_ZEROS = new byte[ContentsSection.OFF_TEXT_CONTENT - OFF_PADx49xea];

        private static byte[] getPadByte_0x49_0xe9(ByteBuffer bb) {
            int startOffset = OFF_PADx49xea;
            if (bb.capacity() <= startOffset) {
                return new byte[]{};
            }
            byte pad[] = new byte[ContentsSection.OFF_TEXT_CONTENT - startOffset];
            for (int i = 0; i < pad.length; i++) {
                pad[i] = bb.get(startOffset + i);
            }
            return pad;
        }

        public short documentMajorVersion = BXD_DOCUMENT_MAJOR_VERSION_2;
        public short documentType = BXD_DOCUMENT_TYPE_PRINT;
        public short documentMinorVersion = BXD_DOCUMENT_MINOR_VERSION_0;
        public short numberingEven = BXD_NUMBERING_NONE;
        public short numberingOdd = BXD_NUMBERING_NONE;
        public short numberingRefEven = BXD_NUMBERING_NONE;
        public short numberingRefOdd = BXD_NUMBERING_NONE;
        public short numberingFirstPage = (short) 1;
        public short pad_short_0x14_0x17[] = BXD_PAD_0x14_0x17_ALWAYS;
        public short pad_short_0x19 = BXD_PAD_0x19_variant0;
        public short hyphenTable = BXD_HYPHEN_TABLE_NONE;
        public int contentsSectionSize = 0; // recomputed before writing (TODO)
        public int pad_int_0x20 = 0; // should probably be recomputed... unknown        
        public short[] pad_short_0x24_0x27 = BXD_PAD_0x24_0x27_ZEROS;
        public byte pad_byte_0x28 = BXD_PAD_0x28; // dic ref num ?
        public byte[][] dictionaryRefs = BXD_DICTIONNARY_REFS_FRENCH;
        public byte[] pad_byte_0x49_0xe9 = BXD_PAD_0x49_0xea_ZEROS;

        public HeaderSection() {
        }

        public HeaderSection(ByteBuffer b) throws Exception {
            if (!checkMagic(b)) {
                throw new Exception("bad magic");
            }
            this.documentMajorVersion = getDocumentMajorVersion(b);
            this.documentType = getDocumentType(b);
            this.documentMinorVersion = getDocumentMinorVersion(b);
            this.numberingEven = getNumbering(b, OFF_NUMBERING_EVEN);
            this.numberingOdd = getNumbering(b, OFF_NUMBERING_ODD);
            this.numberingRefEven = getNumbering(b, OFF_NUMBERING_REF_EVEN);
            this.numberingRefOdd = getNumbering(b, OFF_NUMBERING_REF_ODD);
            this.numberingFirstPage = getNumbering(b, OFF_NUMBERING_FIRST);
            this.pad_short_0x14_0x17 = getPadShort_0x14_0x17(b);
            this.pad_short_0x19 = getPadShort_0x18_0x19(b);
            this.hyphenTable = getHyphenTable(b);
            this.contentsSectionSize = getContentsSectionSize(b);
            this.pad_int_0x20 = getPadInt_0x20(b);
            this.pad_short_0x24_0x27 = getPadShort_0x24_0x27(b);
            this.pad_byte_0x28 = getPadByte_0x28(b);
            this.dictionaryRefs = getDictionaryRefs(b);
            this.pad_byte_0x49_0xe9 = getPadByte_0x49_0xe9(b);
        }

        public int write(OutputStream outputStream) throws IOException {
            int written = 0;

            try (DataOutputStream dos = new DataOutputStream(outputStream)) {
                dos.write(BXD_MAGIC);
                dos.writeShort(documentMajorVersion);
                dos.writeShort(documentType);
                dos.writeShort(documentMinorVersion);
                dos.writeShort(numberingEven);
                dos.writeShort(numberingOdd);
                dos.writeShort(numberingRefEven);
                dos.writeShort(numberingRefOdd);
                dos.writeShort(numberingFirstPage);
                for (short b : pad_short_0x14_0x17) {
                    dos.writeShort(b);
                }
                dos.writeShort(pad_short_0x19);
                dos.writeShort(hyphenTable);
                // TODO recompute size int@0x1c (size of text contents block)
                dos.writeInt(contentsSectionSize);
                // TODO recompute size int@0x20 (size of text contents block)                
                dos.writeInt(pad_int_0x20);
                for (short b : pad_short_0x24_0x27) {
                    dos.writeShort(b);
                }
                dos.write(pad_byte_0x28);
                for (byte[] bs : dictionaryRefs) {
                    dos.write(bs);
                }
                dos.write(pad_byte_0x49_0xe9);

                written = dos.size();
            }

            return written;
        }
    }

    /**
     * Contents section.
     */
    public static class ContentsSection {

        public static final int OFF_TEXT_CONTENT = 0xea;

        private static int getTextContentLength(ByteBuffer bb) {
            return getLengthToZero(bb, OFF_TEXT_CONTENT);
        }

        private static byte[] getTextContent(ByteBuffer bb) {
            int len = getTextContentLength(bb);
            byte textContent[] = new byte[len];
            for (int off = 0; off < len; off++) {
                textContent[off] = bb.get(OFF_TEXT_CONTENT + off);
            }
            return textContent;
        }

        private static int getTextEndOffset(ByteBuffer bb) {
            return OFF_TEXT_CONTENT + getTextContentLength(bb);
        }

        public byte[] text;

        public ContentsSection(ByteBuffer b) {
            this.text = getTextContent(b);
        }

        public ContentsSection() {
            this.text = new byte[]{};
        }

        public int write(OutputStream outputStream) throws IOException {
            outputStream.write(text);
            return text.length;
        }
    }

    /**
     * Structure header.
     */
    public static class StructureHeader {

        public short number;
        public int size;
        public byte val1;
        public byte val2;

        public StructureHeader(ByteBuffer bb, int offset) {
            this.number = bb.getShort(offset);
            this.size = bb.getInt(offset + 2);
            this.val1 = bb.get(offset + 6);
            this.val2 = bb.get(offset + 7);
        }

        public int write(OutputStream outputStream) throws IOException {
            int written = 0;
            try (DataOutputStream dos = new DataOutputStream(outputStream)) {
                dos.writeShort(number);
                dos.writeInt(size);
                dos.write(val1);
                dos.write(val2);
                written = dos.size();
            }
            return written;
        }

        @Override
        public String toString() {
            return "num:" + (int) number + " size:" + size + " vals:" + String.format("0x%02X 0x%02X", val1, val2);
        }
    }

    /**
     * Styles section.
     */
    public static class StyleSection {

        private static int getStyleSectionOffset(ByteBuffer bb) {
            return ContentsSection.getTextEndOffset(bb);
        }

        // styles header
        private static int OFF_STYLE_BLOCSIZE = 0x00;

        /**
         * short value : size of style order (= number of styles) int value :
         * size of the style names (without this block, and without the order
         * block)
         *
         * @param bb
         * @return
         */
        private static int getStyleStructureHeaderOffset(ByteBuffer bb) {
            return (getStyleSectionOffset(bb) + OFF_STYLE_BLOCSIZE);
        }

        private static StructureHeader getStyleStructureHeader(ByteBuffer bb) {
            return new StructureHeader(bb, getStyleStructureHeaderOffset(bb));
        }

        // styles order
        private static int OFF_STYLE_ORDER = 0x08;

        private static int getStyleOrderOffset(ByteBuffer bb) {
            return getStyleSectionOffset(bb) + OFF_STYLE_ORDER;
        }

        private static short[] getStyleOrder(ByteBuffer bb) {
            int startOffset = getStyleOrderOffset(bb);
            short orders[] = new short[getStyleStructureHeader(bb).number];
            for (int styleNum = 0; styleNum < orders.length; styleNum++) {
                orders[styleNum] = bb.getShort(startOffset + 2 * styleNum); // short = 2 bytes
            }
            return orders;
        }

        // styles names
        private static int getStyleNamesOffset(ByteBuffer bb) {
            int startOffset = getStyleSectionOffset(bb)
                    + OFF_STYLE_ORDER
                    + (2 * getStyleStructureHeader(bb).number);
            return startOffset;
        }

        private static byte[][] getStyleNames(ByteBuffer bb) {
            int startOffset = getStyleNamesOffset(bb);
            byte names[][] = new byte[getStyleStructureHeader(bb).number][];
            for (int styleNum = 0; styleNum < names.length; styleNum++) {
                int styleNameSize = getLengthToZero(bb, startOffset);
                byte[] styleName = new byte[styleNameSize];
                for (int i = 0; i < styleNameSize; i++) {
                    styleName[i] = bb.get(startOffset);
                    startOffset += 1;
                }
                names[styleNum] = styleName;
                startOffset += 1;
            }
            return names;
        }

        /**
         * short value : number of style definitions (= 2x number of styles) int
         * value : size of the definition block (without this block)
         *
         * @param bb
         * @return
         */
        private static int getStyleDefinitionStructureHeaderOffset(ByteBuffer bb) {
            int styleNameOffset = getStyleNamesOffset(bb);
            byte[][] names = getStyleNames(bb);
            int namesSize = 0;
            for (byte[] name : names) {
                namesSize += name.length + 1;
            }
            return styleNameOffset + namesSize;
        }

        private static StructureHeader getStyleDefinitionStructureHeader(ByteBuffer bb) {
            return new StructureHeader(bb, getStyleDefinitionStructureHeaderOffset(bb));
        }

        // style definitions
        private static int getStyleDefinitionsOffset(ByteBuffer bb) {
            int startOffsetName = getStyleNamesOffset(bb);
            byte[][] names = getStyleNames(bb);
            int namesSize = 0;
            for (byte[] name : names) {
                namesSize += name.length + 1;
            }
            int startOffset = startOffsetName + namesSize;
            startOffset += 8;
            return startOffset;
        }

        private static byte[][] getStyleDefinitions(ByteBuffer bb) {
            int startOffset = getStyleDefinitionsOffset(bb);
            byte definitions[][] = new byte[getStyleDefinitionStructureHeader(bb).number][];
            for (int styleNum = 0; styleNum < (definitions.length / 2); styleNum++) {
                for (int beforeafter = 0; beforeafter <= 1; beforeafter++) {
                    int styleDefinitionSize = getLengthToZero(bb, startOffset);
                    byte[] styleDefinition = new byte[styleDefinitionSize];
                    for (int i = 0; i < styleDefinitionSize; i++) {
                        styleDefinition[i] = bb.get(startOffset);
                        startOffset += 1;
                    }
                    definitions[styleNum * 2 + beforeafter] = styleDefinition;
                    startOffset += 1; // end \0
                }
            }
            return definitions;
        }

        public StructureHeader styleStructureHeader;
        public short[] order;
        public byte[][] names;
        public StructureHeader styleDefinitionStructureHeader;
        public byte[][] definitions;

        public StyleSection(ByteBuffer b) {
            this.styleStructureHeader = getStyleStructureHeader(b);
            this.order = getStyleOrder(b);
            this.names = getStyleNames(b);
            this.styleDefinitionStructureHeader = getStyleDefinitionStructureHeader(b);
            this.definitions = getStyleDefinitions(b);
        }

        private StyleSection() {
            order = new short[]{0};
            names = new byte[][]{new byte[]{'D', 'E', 'F', 'A', 'U', 'L', 'T'}};
            definitions = new byte[][]{new byte[]{}, new byte[]{}};
        }

        public int write(OutputStream outputStream) throws IOException {
            int written = 0;

            try (DataOutputStream dos = new DataOutputStream(outputStream)) {

                //  update style structure header
                ByteArrayOutputStream namesBaos = new ByteArrayOutputStream();
                for (byte s[] : names) {
                    namesBaos.write(s);
                    namesBaos.write((byte) 0x00);
                }
                namesBaos.close();
                styleStructureHeader.number = (short) order.length;
                styleStructureHeader.size = (int) namesBaos.size();

                //  update style definition structure header
                ByteArrayOutputStream definitionsBaos = new ByteArrayOutputStream();
                for (byte s[] : definitions) {
                    definitionsBaos.write(s);
                    definitionsBaos.write((byte) 0x00);
                }
                definitionsBaos.close();
                styleDefinitionStructureHeader.number = (short) definitions.length;
                styleDefinitionStructureHeader.size = (short) definitionsBaos.size();

                // write to stream
                styleStructureHeader.write(dos);
                for (short s : order) {
                    dos.writeShort(s);
                }
                dos.write(namesBaos.toByteArray());

                styleDefinitionStructureHeader.write(dos);
                dos.write(definitionsBaos.toByteArray());

                written = dos.size();
            }
            return written;
        }

    }

    /**
     * Printer section.
     */
    public static class PrinterSection {

        private static int getPrinterListStart(ByteBuffer bb) {
            int startDefinitionOffset = StyleSection.getStyleDefinitionsOffset(bb);
            byte[][] definitions = StyleSection.getStyleDefinitions(bb);
            int definitionsSize = 0;
            for (byte[] definition : definitions) {
                definitionsSize += definition.length + 1;
            }
            int end = startDefinitionOffset + definitionsSize;
            return end;
        }

        private static byte[] getPrinterList(ByteBuffer bb) {
            int startPrinterListOffset = getPrinterListStart(bb);
            if (!(startPrinterListOffset < bb.capacity())) {
                return new byte[]{};
            }
            byte[] printerList = new byte[bb.capacity() - startPrinterListOffset];
            for (int offset = 0; offset < printerList.length; offset++) {
                printerList[offset] = bb.get(startPrinterListOffset + offset);
            }
            return printerList;
        }

        public byte[] data;

        public PrinterSection(ByteBuffer bb) {
            this.data = getPrinterList(bb);
        }

        private PrinterSection() {
            data = new byte[]{};
        }

        public int write(OutputStream os) throws IOException {
            os.write(data);
            return data.length;
        }
    }

    /**
     * BxdDocument section.
     */
    public static class BxdDocument {

        public HeaderSection headerSection;
        public ContentsSection contentsSection;
        public StyleSection styleSection;
        public PrinterSection printerSection;

        public int write(OutputStream os) throws IOException {
            int written = 0;
            try (DataOutputStream dos = new DataOutputStream(os)) {
                headerSection.write(dos);
                contentsSection.write(dos);
                styleSection.write(dos);
                printerSection.write(dos);
                written = dos.size();
            }
            return written;
        }

        public BxdDocument(ByteBuffer b) throws Exception {
            this.headerSection = new HeaderSection(b);
            this.contentsSection = new ContentsSection(b);
            this.styleSection = new StyleSection(b);
            this.printerSection = new PrinterSection(b);
        }

        public BxdDocument() throws Exception {
            this.headerSection = new HeaderSection();
            this.contentsSection = new ContentsSection();
            this.styleSection = new StyleSection();
            this.printerSection = new PrinterSection();
        }
    }

    private static void checkDocumentReadWrite(ByteBuffer bb) throws Exception {
        checkHeaderReadWrite(bb);
        checkContentsReadWrite(bb);
        checkStyleReadWrite(bb);
        checkPrinterReadWrite(bb);
    }

    private static void checkHeaderReadWrite(ByteBuffer bb) throws Exception {
        HeaderSection headerSection = new HeaderSection(bb);
        byte[] copied;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        headerSection.write(baos);
        baos.close();
        copied = baos.toByteArray();
        for (int i = 0; i < copied.length; i++) {
            if (copied[i] != bb.get(i)) {
                System.err.println("!diff in header " + i + " " + String.format("0x%02X", i) + " " + bb.get(i) + " " + copied[i]);
                throw new Exception();
            } else {
          //      System.err.println("                " + i + " " + String.format("0x%02X", i) + " " + bb.get(i) + " " + copied[i]);
            }
        }
//        System.err.println("NO diff in Header");
    }

    private static void checkContentsReadWrite(ByteBuffer bb) throws Exception {
        ContentsSection contentsSection = new ContentsSection(bb);
        byte[] copied;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        contentsSection.write(baos);
        baos.close();
        copied = baos.toByteArray();
        for (int i = 0; i < copied.length; i++) {
            if (copied[i] != bb.get(i + ContentsSection.OFF_TEXT_CONTENT)) {
                System.err.println("!diff in Contents " + i + " " + String.format("0x%02X", i) + " " + bb.get(i) + " " + copied[i]);
                throw new Exception();
            }
        }
//        System.err.println("NO diff in Contents");

    }

    private static void checkStyleReadWrite(ByteBuffer bb) throws Exception {
        StyleSection stylesSection = new StyleSection(bb);
        byte[] copied;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        stylesSection.write(baos);
        baos.close();
        copied = baos.toByteArray();
        for (int i = 0; i < copied.length; i++) {
            int absadd = i + ContentsSection.getTextEndOffset(bb);
            byte model = bb.get(absadd);
            if (copied[i] != model) {
                System.err.println("!diff in Styles @" + absadd + " " + model + " -> " + copied[i]);
                throw new Exception();
            } else {
                //System.err.println("          Syles @"+absadd + " " + model + " -> " + copied[i]);
            }
        }
//        System.err.println("NO diff in Styles");

    }

    private static void checkPrinterReadWrite(ByteBuffer bb) throws Exception {
        PrinterSection PrintersSection = new PrinterSection(bb);
        byte[] copied;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintersSection.write(baos);
        baos.close();
        copied = baos.toByteArray();
        for (int i = 0; i < copied.length; i++) {
            int absadd = i + PrinterSection.getPrinterListStart(bb);
            byte model = bb.get(absadd);
            if (copied[i] != model) {
                System.err.println("!diff in Printers @" + absadd + " " + model + " -> " + copied[i]);
                throw new Exception();
            } else {
                //System.err.println("          Syles @"+absadd + " " + model + " -> " + copied[i]);
            }
        }
//        System.err.println("NO diff in Printers");

    }

    /**
     * Returns the decoded content of the DBT binary stream.
     *
     * @param is flat binary document stream
     * @return
     * @throws IOException
     * @throws Exception
     */
    public static BxdDocument decode(InputStream is) throws IOException, Exception {
        byte[] buffer = IOUtils.toByteArray(is);
        ByteBuffer bb = ByteBuffer.wrap(buffer);
        return new BxdDocument(bb);
    }

    /**
     * Performs read/write check on the document, then returns the decoded
     * content of the DBT binary stream.
     * <p>
     * The test reads, interprets, writes back the result and compares with the
     * orginal. An exception is thrown if the bytes does not match.
     *
     * @param is flat binary document stream
     * @return
     * @throws IOException or
     * @throws Exception if the check fails.
     */
    public static BxdDocument checkAndDecode(InputStream is) throws IOException, Exception {
        byte[] buffer = IOUtils.toByteArray(is);
        ByteBuffer bb = ByteBuffer.wrap(buffer);
        checkDocumentReadWrite(bb);
        return new BxdDocument(bb);
    }
}
