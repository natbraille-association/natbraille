/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.natbraille.bxd;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.io.IOUtils;

/// scanner (lexer) control table


/**
 *
 * @author vivien
 */
public class Sct {
    
    
    /*
    37 0|   binmod 0 = normal for print->brl & brl->print (text, scanning)
      1 0|   binmod 1 = transparent (text mode)
      1 0|   binmod 2 = transparent (binary)
     */
    //     38 0 // 0 
    //      1 0 // 1
    //      1 0 // 2

    /*
     38 1| eliminate trailing blanks on line (0=no, 1=yes)
      2 1| eliminate trailing blanks on line (0=no, 1=yes) (moot here)
     */
    //     40 1 // 1
    /*
      7 2| flag char for scan/format controls (note || = ONE vertical bar)
     31 2| flag char (input) for scan/format controls
      2 2| flag char (input) for scan/format controls (moot here)
     */
    //      7 2 // |
    //     13 2 // $
    //     20 2 // |028

    /*
     38 3| fullstop char for 'sentence' mode
      2 3| fullstop char for 'sentence' mode (moot here)
     */
    //     33 3 // .
    //      7 3 // 4

    /*
      2 4| 1 = newline treated as space (moot here)
     37 4|   1=newline treated as space (spacing) (TEX & old TRANSL)
     */
    //     40 4 // 1

    /*
     31 5| ff is error
      6 5| ff is ignored without warning
      2 5| ff subst. string (|021=error) (moot here)
     */
    //      6 5 // 
    //     34 5 // |021

    /*
      2 6| auto-par insertion (moot here)
     37 6| par insert
     */
    //     21 6 // p
    //     19 6 // P
    /*
     20 7| $no-par in effect initially
     16 7| $par (on either) in effect initially
      2 7| par on either ($par) (moot here)
      1 7| $par-skp (on 2 newlines) in effect initially
     */
    //     20 7 // 0
    //      1 7 // 1
    //     19 7 // 3

    /*
     22 8| 0 = no rejoining at eol hyphens
      2 8| 0=no rejoining hyphens (moot here)
     15 8| 1 = rejoin at all line-ending hyphens
     */
    //     24 8 // 0
    //     16 8 // 1

    /*
     37 9| 0 = compress spaces
      2 9| 0=compress spaces (moot here)
     */
    //     40 9 // 0

    /*    
     38 10| number of spaces to expand tabs to (-1 = tabs are errors)(<= 9999)
      2 10| number of spaces to expand tabs to (-1 = tabs are errors)(<= 9999) (moot here)
     */
    //     20 10 // 1
    //      3 10 // -1
    //     17 10 // 2

    /*
     37 11|   ninmod 0 = normal (word scanning) for prt->brl & brl->prt
      2 11| ninmod: 1= binary pass-through; rest are moot here:
     */
    //     38 11 // 0
    //      2 11 // 1

    /*
     34 12|   1 = yes flatten controls, downcase
      2 12|   1 = yes flatten controls, dwncase
      1 12| 2 = don't flatten case of controls
      2 12| leave chars alone (moot)
     */
    //     37 12 // 1
    //      3 12 // 2

    /*
     31 13|   1 = yes flatten ord'y words, downcase
      2 13|   1 = yes flatten ord'y words, dwncase
      1 13|   2 = don't flatten case of ordinary words
      3 13| 2 = don't flatten case of ordinary words
      2 13| leave chars in ctl words (moot)
     */
    //     34 13 // 1
    //      6 13 // 2

    /*
     17 14|   0 = normal cease u.c. (only end of word)
      2 14| (nsxli: 0 = cessation of uppercase at end of word (eow) (i.e. at whitespace)
      1 14| (nsxli: 0 = cessation of uppercase at eow only; see nsxls blw)
     18 14| (nsxli: 1 = cessation of uppercase at any nonalpha; see nsxls blw)
      2 14| "upper case" cessation normal (moot)
     */
    //     22 14 // 0
    //     18 14 // 1

    /*
      2 15| 0 = DON'T map special chars when upcasing (moot)
     30 15| 0 = `{|}~ DON'T map to @[\]^ when upcasing
      7 15| 1 = `{|}~ DO map to @[\]^ (e.g. for VB1) when upcasing
      1 15| 1 = `{|}~ map to @[\]^ (e.g. for VB1), 0 = DON'T map those chars
     */
    //     33 15 // 0
    //      7 15 // 1
    public final static String[] Names16 = {
        // "ninit",
        //
        "binmod",
        "eliminateTrailingBlanksOnline",
        "flagChar",
        "fullstopChar",
        "treatNewlineAsSpace",
        "formfeedSubstitution",
        "autoParInsertion",
        "parInitially",
        "rejoinLineEndingHyphen",
        "compressSpace",
        "nSpaceForYab",
        "ninmod",
        "flattenControls",
        "flattenOrdinaryWords",
        "uppercaseCessation",
        "dontMapSpecialCharsWhenUpcasing"
    };
    /*
      1 0| (ninfby: 3 = issue both BOF and EOF explicitly)
     38 0| (spare)
     */
    //      1 0 // 0
    //      1 0 // 3

    /*
      2 1| nsnqt - input flag for quoted word
      7 1| nsnqt - input flag for quoted word (nil = none)
      9 1| nsnqt - input flag for quoted word (nil: no quoting)
     22 1| (spare - was nsnqt)
     */
    //     40 1 // 
    ////////// ?
    /*
      7 2| nsoqt - output flag for quoted word
      2 2| nsoqt - output flag for quoted word (moot)
     10 2| nsoqt - output flag for quoted word (moot -- no longer used)
      3 2| (nsxls: 0 = don't change uppercase cess'n within $cb ... $tx; see nsxli abv)
     13 2| (nxsls: 0 = don't change uppercase cess'n within $cb ... $tx; see nsxli abv)
      5 2| (nxsls: 1 = switch uppercase cess'n to eow in $cb ... $tx; see nsxli abv)
     */
    //     16 2 // 0
    //     10 2 // |027
    //      9 2 // |028
    //      5 2 // 1

    /*
     38 3| nsofm - output flag for whole-word format word
      2 3| nsofm - output flag for whole-word format word (moot)
     */
    //     31 3 // |028
    //      9 3 // |029

    /*
     38 4| nsof2 - output flag for 2-char format word
      2 4| nsof2 - output flag for 2-char format word (moot)
     */
    //     31 4 // |027
    //      9 4 // |030

    /*
     33 5| nsocp - capitalization flag output
      2 5| nsocp - capitalization flag output (moot)
      5 5| nsocp - capitalization flag output (nil => don't represent caps)
     */
    //     19 5 // 
    //     21 5 // |001

    /*
     33 6| nsotn - cap termination flag output
      2 6| nsotn - cap termination flag output (moot)
      5 6| nsotn - cap termination flag output (none)
     */
    //     19 6 // 
    //     21 6 // |002

    /*
      9 7| icb - code to insert to toggle treatment as computer braille (0 disables)
     31 7| (spare)
     */
    //     31 7 // 
    //      9 7 // 0

    /*
      2 8| RUNNING (moot)
     */
    //      2 8 // 
    //      2 8 // correr
    //      4 8 // rien
    //     31 8 // running
    //      1 8 // RUNNING

    /*
      2 9| SPACING (moot)
     */
    //      2 9 // 
    //      4 9 // espace
    //      2 9 // espaciar
    //     31 9 // spacing
    //      1 9 // SPACING

    /*
      2 10| MARKING (moot)
     */
    //      2 10 // 
    //      2 10 // marcar
    //     31 10 // marking
    //      1 10 // MARKING
    //      4 10 // marque

    /*
      2 11| AS-IS (moot)
     */
    //      2 11 // 
    //     31 11 // as-is
    //      1 11 // AS-IS
    //      4 11 // conserve
    //      2 11 // literal

    /*
      2 12| COMPRESS (moot)
     */
    //      2 12 // 
    //     35 12 // compress
    //      1 12 // COMPRESS
    //      2 12 // comprim

    /*
      2 13| SENTENCE (moot)
     */
    //      2 13 // 
    //      2 13 // frase
    //      4 13 // phrase
    //     31 13 // sentence
    //      1 13 // SENTENCE

    /*
      2 14| AUTO-TAB (moot)
     */
    //      2 14 // 
    //     35 14 // auto-tab
    //      1 14 // AUTO-TAB
    //      2 14 // tabla

    /*
      2 15| PAR (moot)
     */
    //      2 15 // 
    //     37 15 // par
    //      1 15 // PAR

    /*
      2 16| NO-PAR (moot)
     */
    //      2 16 // 
    //     37 16 // no-par
    //      1 16 // NO-PAR

    /*
      2 17| PAR-SKP (moot)
     */
    //      2 17 // 
    //      2 17 // par-psl
    //     31 17 // par-skp
    //      1 17 // PAR-SKP
    //      4 17 // par-stl

    /*
      2 18| PAR-IND (moot)
     */
    //      2 18 // 
    //      4 18 // par-aln
    //     33 18 // par-ind
    //      1 18 // PAR-IND

    /*
      6 19| issue triple caps if more than 2 uppercase words (0 disables)
      5 19| issue triple caps if more than 3 uppercase words (0 disables)
     29 19| (spare)
     */
    //     29 19 // 
    //      4 19 // 0
    //      6 19 // 2
    //      1 19 // 3

    /*
      2 20| code to issue in front of doubled adjectives (0 disables)
      6 20| code to issue in front of doubled adjectives (0 disables) (+1 = code after dbl adj)
      3 20| code to issue in front of doubled wds (0 disables) (+1 = code afr dbl wd)
     29 20| (spare)
     */
    //     29 20 // 
    //      9 20 // 0
    //      2 20 // 6

    /*
     40 21| required table terminator
     */
    //     40 21 // ***
    public final static String[] MoreNames22 = {
        "infby",
        "snqt",
        "nsoqt/ndxls",
        "nsofm",
        "nsof2",
        "nsocp",
        "nsotn",
        "icb",
        // trans
        "running",
        "spacing",
        "marking",
        "as-is",
        "compress",
        "sentence",
        "auto-tab",
        "par",
        "no-par",
        "par-skp",
        "par-ind",
        //
        "minUppercaseWordForTripleCaps",
        "codePrefixForDoubleAdjectives",
        "tableterminator"
    };

    private final String[] options16 = {"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""};
    private final String[] comment16 = {"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""};
    private final String[] options22 = {"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""};
    private final String[] comment22 = {"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""};
    // fmt of blw: start end max_words_before_double 
    private final List<String> fonts = new ArrayList<>();

    /*
     * string parsing
     */
    private static final Pattern pattern = Pattern.compile(
            "(?<value>"
            + "("
            + "(\\|\\|)"
            + "|"
            + "(\\|\\d+)"
            + "|"
            + "([^|])"
            + ")+"
            + ")?"
            + "("
            + "(?<comment>"
            + "(\\|)"
            + "(.+)"
            + ")"
            + ")*"
    );

    static Sct parseAsciiString(String string) {

        Sct parsed = new Sct();

        String[] lines = string.split("\r\n");
        int state = 0;
        // 1 : params16
        // 2 : font
        // 3 : fontlist
        // 4 : params22
        int remainingFonts = 0;
        int counter = 0;
        int moreCounter = 0;
        for (String line : lines) {
            Matcher matcher = pattern.matcher(line);
            if (matcher.find()) {
                if (matcher.group("value") != null) {
                    String value = matcher.group("value").trim().replace("||", "|");
                    String comment = matcher.group("comment");
                    if (comment == null) {
                        comment = "";
                    }
                    if (state == 0) {
                        if (value.equals("-ninit 6")) {
                            state = 1;
                            counter = 0;
                        }
                    } else if (state == 1) {
                        if (counter < 16) {
                            parsed.options16[counter] = value;
                            parsed.comment16[counter] = comment;
                            counter++;
                        }
                        if (counter == 16) {
                            state = 2;
                        }
                    } else if (state == 2) {
                        try {
                            remainingFonts = Integer.parseInt(value.trim());
                        } catch (NumberFormatException e) {

                        }
                        if (remainingFonts > 0) {
                            state = 3;
                        } else {
                            state = 4;
                        }

                    } else if (state == 3) {
                        if (remainingFonts > 0) {
                            parsed.fonts.add(value);
                            remainingFonts--;
                        }

                        if (remainingFonts == 0) {
                            state = 4;
                        }
                    } else if (state == 4) {
                        parsed.options22[moreCounter] = value;
                        parsed.comment22[moreCounter] = comment;
                        moreCounter++;
                        if (moreCounter == 22) {
                            state = 5;
                        }
                    } else if (state == 4) {
                        System.out.println("extraneous line");
                    }
                }
                //System.out.print("⃤");
                //System.out.print("⃝");
            }
        }
        return parsed;
    }

    public static void main(String[] args) throws FileNotFoundException, IOException {

        String path = "/home/vivien/.wine/drive_c/Program Files/Duxbury/DBT 12.3/";
        String[] fileNames = {
            "gendxb.sct", "text.sct", "enxtxt.sct",
            "frabux.sct", "amebru.sct",
            "engtnc.sct", "gendxp.sct", "spattx.sct",
            "gentxt.sct", "engdxb.sct", "model.sct",
            "enutxt.sct", "ametxt.sct", "idndxp.sct",
            "engbru.sct", "spadxp.sct", "frudxp.sct",
            "engbux.sct", "amidxp.sct", "gebdxb.sct",
            "mlydxp.sct", "engtxf.sct", "binary.sct",
            "engttx.sct", "eoaacp.sct", "genbru.sct",
            // uncorrect font number but missing following param...
            // "heidxp.sct", 
            "enxbru.sct", "nladxp.sct",
            "fradxp.sct", "engdxp.sct", "amedxb.sct",
            "amedxp.sct", "frattx.sct", "engtxt.sct",
            "enudxp.sct", "qqphcp.sct", "spabux.sct",
            "ipadxp.sct", "qqphcb.sct", "enbdxp.sct"
        };
//        for (int i = 0; i < 16; i++) {
            for (String fn : fileNames) {
                // System.out.println("====================================" + fn);
                String string = IOUtils.toString(new FileInputStream(path + fn), "ascii");
                Sct options = Sct.parseAsciiString(string);
                // options.fonts.forEach( x -> System.out.println(""+x));            
                //   System.out.println(""+options.comment22[21]);
                /*if (!options.comment16[i].isEmpty()) {
                    System.out.println(i + "" + options.comment16[i]);
                }*/
                 System.out.println(options.comment16[0]);
            }
  //      }
    }
}
