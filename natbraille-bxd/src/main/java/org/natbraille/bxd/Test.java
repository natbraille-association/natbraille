/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.bxd;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import nu.xom.Document;
import nu.xom.Serializer;
import org.apache.tika.parser.txt.CharsetDetector;
import org.apache.tika.parser.txt.CharsetMatch;
import org.natbraille.bxd.TestFiles;

/**
 * http://www.theasciicode.com.ar/extended-ascii-code/capital-letter-e-acute-accent-e-acute-uppercase-ascii-code-144.html
 *
 *
 * @author vivien
 */
public class Test {

    /**
     * remove 0x1c and 0x1f command delimiters from text data for encoding
     * detection purpose.
     *
     * @param textContentsData
     * @return
     */
    public static byte[] filterTextContents(byte[] textContentsData) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        for (byte b : textContentsData) {
            if (b == (byte) 0x1c) {
                continue;
            }
            if (b == (byte) 0x1f) {
                continue;
            }
            if (b == (byte) 0x1d) {
                continue;
            }
            if (b == (byte) 0x1e) {
                continue;
            }
            baos.write(b);
        }
        return baos.toByteArray();
    }

    /**
     * guess the text contents charset name using Tika.
     *
     * @param bxdDocument
     * @return
     */
    public static CharsetMatch guessTextContentsCharset(BinaryDeEncoder.BxdDocument bxdDocument) {
        byte[] data = filterTextContents(bxdDocument.contentsSection.text);
        CharsetDetector cdetector = new CharsetDetector();
        cdetector.setText(filterTextContents(data));
        return cdetector.detect();
    }

    public static void explorePrinterSection(BinaryDeEncoder.BxdDocument bxdDocument, String filename) throws IOException {
        System.err.print(" " + bxdDocument.headerSection.documentType + " " + bxdDocument.headerSection.documentMinorVersion
                + " text:" + bxdDocument.contentsSection.text.length
                + " priner:" + bxdDocument.printerSection.data.length
                + " "
        );

        {
            byte pad[] = bxdDocument.headerSection.pad_byte_0x49_0xe9;

            DataInputStream dis = new DataInputStream(new ByteArrayInputStream(pad));
            dis.skipBytes(0x76 - 0x49);
            short linePerPage = dis.readShort();           // short@x076
            System.err.print("line per page:" + linePerPage);
            short nextPadding = dis.readShort();           // short@x078
            System.err.print(" pad1:" + nextPadding);
            short pagePerLine = dis.readShort();           // short@x07a
            System.err.print(" pagePerLine:" + pagePerLine);
            short nextPadding1 = dis.readShort();          // short@x07c
            System.err.print(" pad2:" + nextPadding1);
            short nextPadding2 = dis.readShort();          // short@x07e
            System.err.print(" pad3:" + nextPadding2);
            short nextPadding3 = dis.readShort();          // short@x080
            System.err.print(" pad4:" + nextPadding3);
            byte paperSize[] = new byte[4];
            dis.read(paperSize);          // short@x080
            System.err.print(" papersize:");
            for (byte b : paperSize) {
                System.err.print((char) b);
            }
            System.err.println();

//                System.err.print(pad.length+" ");
            for (byte b : pad) {

                if ((0xff & b) != 0) {
                    System.err.print((char) (0xff & b));
                } else {
                    System.err.print(".");
                }

            }
        }
        System.err.print("\t" + filename + " \n");
        byte pad[] = bxdDocument.printerSection.data;
        {
            for (byte b : pad) {
                if ((0xff & b) != 0) {
                    System.err.print((char) (0xff & b));
                } else {
                    System.err.print(".");
                }
            }
        }
        System.err.println(" " + filename);
    }

    public static void main(String[] args) {

        List<File> allFiles = TestFiles.all();
        
        // only one
        allFiles = Arrays.asList( new File[]{ new File("dbtfiles/perso/Manuel_abrege2013_Vol1.dxb") });
        
        for (File file : allFiles) {

            System.err.print("* file : " + file.getName());

            try {
                BinaryDeEncoder.BxdDocument bxdDocument = BinaryDeEncoder.checkAndDecode(new FileInputStream(file));
                Document doc = new Document(XmlEncoder.toXML(bxdDocument, "UTF-8")); // "extended ascii !!!"....
                if (true) {
                    Serializer serializer;
                    serializer = new Serializer(System.err);
                    serializer.setIndent(4);
                    serializer.write(doc);
                    serializer.flush();
                }
            } catch (Exception ex) {
                Logger.getLogger(Test.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
//            System.exit(0);
        }

    }

}
