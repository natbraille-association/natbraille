/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * Author : Vivien Guillet <vivien.guillet@laposte.net>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.starmath.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.natbraille.starmath.DomUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author vivien
 */
public class Extractions {

    public static void main(String[] args) throws ParserConfigurationException, IOException {
        XPath xPath = XPathFactory.newInstance().newXPath();

        HashMap<String, List<String>> occurences = new HashMap();

        List<Validation.Example> exemples = Validation.buildExamples();
        exemples.stream().forEach(e -> {
            Document doc = e.mathMLDocument;
            try {
                NodeList nodes = (NodeList) xPath.evaluate("//mo", doc, XPathConstants.NODESET);

                for (int i = 0; i < nodes.getLength(); i++) {
                    Node node = nodes.item(i);
                    // System.err.println();
                    String text = node.getTextContent();
                    String outer = DomUtils.outerXml(node).replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n", "");
                    List<String> array;

                    List<String> cur = occurences.get(text);
                    if (cur == null) {
                        array = new ArrayList();
                        occurences.put(text, array);
                    } else {
                        array = cur;
                    }
                    if (!array.stream().anyMatch(x -> x.equals(outer))) { 
                        array.add(outer);
                    }

//                    System.err.println(outer);
                }
            } catch (XPathExpressionException ex) {
                Logger.getLogger(Extractions.class.getName()).log(Level.SEVERE, null, ex);
            }
            occurences.keySet().stream().forEach(k -> {
                System.err.println("=" + k);
                occurences.get(k).stream().forEach(o -> {
                    System.err.println(o);
                });
            });
        });

    }
}
