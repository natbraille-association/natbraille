/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * Author : Vivien Guillet <vivien.guillet@laposte.net>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.starmath.test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Builds a flat odf file from starmath equations
 * where the mathml part is missing. 
 * Using libreoffice to open this document will add 
 * missing mathml part.
 * This can be used to get reference starmath -> mathml conversion
 * @author vivien
 */
public class OpenDocumentWithStarMathEquationsAndNoMathML {
    
    private final static String flatodttemplate = "flatodttemplate.xml";
    private final static String flatodttemplate_variable = "{{ equations }}";
    private final static String starmathtemplate = "starmathtemplate.xml";
    private final static String starmathtemplate_variable = "{{ equation }}";
    
    private static String loadResourceAsString( String resourceName ) throws IOException, URISyntaxException{
        Path p = Paths.get(OpenDocumentWithStarMathEquationsAndNoMathML.class.getResource(resourceName).toURI() );        
        return new String(Files.readAllBytes(p));        
    }
    
    public static String build(List<String> starmathEquations) throws URISyntaxException, IOException{
        String outerTemplate = loadResourceAsString(flatodttemplate);
        String innerTemplate = loadResourceAsString(starmathtemplate);
        
        StringBuilder sb = new StringBuilder();
        for (String starmathEquation : starmathEquations){            
           String inner = innerTemplate.replace(starmathtemplate_variable,starmathEquation);           
           sb.append(inner);
        }
        return outerTemplate.replace(flatodttemplate_variable, sb.toString());        
    }
    
    
    public static void main(String argv[]) throws URISyntaxException, IOException{
        
        List<String> expressions = new ArrayList();
        expressions.add("a+b");
        expressions.add("c+d");
        
        String resu = build(expressions);
        
        File outputFile = new File("flatodtwithstarmaththingsandnomathmlconversion.fodt");
        try (OutputStream outputStream = new FileOutputStream(outputFile)) {
            outputStream.write(resu.getBytes("UTF-8"));
        }
    }
    
}
