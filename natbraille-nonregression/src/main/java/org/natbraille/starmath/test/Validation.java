/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * Author : Vivien Guillet <vivien.guillet@laposte.net>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.starmath.test;

import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.apache.tika.sax.TextContentHandler;
import org.natbraille.core.filter.NatFilterException;
import org.natbraille.starmath.DomUtils;
import org.natbraille.starmath.test.brl.ToFromBrl;
import org.natbraille.starmath.toStarMath.StarMathExport;
import org.natbraille.starmath.toMathML.StarMathParser;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author vivien
 */
public class Validation {

    public static class Example {

        public final String starMath;
        public final String mathML;
        public final Document mathMLDocument;

        public Example(String starMath, String mathML, Document mathMLDocument) {
            this.starMath = starMath;
            this.mathML = mathML;
            this.mathMLDocument = mathMLDocument;
        }

    }
    String[] usedDocs = {
        "/home/vivien/src/natbraille/trunk/natbraille-starmath/doc/MG44-MathGuide.odt",};

    public static List<Example> examplesFromDocument(Document doc, XPath xPath) {

        List<Example> examples = new ArrayList();

        try {
            NodeList nodes;
            nodes = (NodeList) xPath.evaluate("math/semantics", doc, XPathConstants.NODESET);
            for (int i = 0; i < nodes.getLength(); ++i) {
                try {
                    Element e = (Element) nodes.item(i);
                    // String outerXml = outerXml(e);
                    String outerXml = DomUtils.innerXml(e);
                    String starMath = (String) xPath.evaluate("annotation/text()", e, XPathConstants.STRING);

                    // remove starmath node from doc
                    Node starMathNode = (Node) xPath.evaluate("annotation", e, XPathConstants.NODE);
                    starMathNode.getParentNode().removeChild(starMathNode);

                    examples.add(new Example(starMath, outerXml, doc));
                } catch (XPathExpressionException | DOMException e) {
                    Logger.getLogger(Validation.class.getName()).log(Level.SEVERE, null, e);
                }
            }
        } catch (XPathExpressionException e) {
            Logger.getLogger(Validation.class.getName()).log(Level.SEVERE, null, e);
        }
        return examples;
    }

    public static List<Example> buildExamples() throws ParserConfigurationException, IOException {

        ///Path path = Paths.get("/home/vivien/src/natbraille/trunk/natbraille-starmath/doc/MG44-MathGuide.odt_dir");
        //     Path path = Paths.get("/home/vivien/src/natbraille/trunk/natbraille-starmath/doctest/st");
        //   Path path = Paths.get("/home/vivien/src/natbraille/trunk/natbraille-starmath/doctest/b");
        // Path path = Paths.get("/home/vivien/src/natbraille/trunk/natbraille-starmath/doctest/");
        Path path = Paths.get("starmath-doctest/testh");
        //Path path = Paths.get("/home/vivien/src/natbraille/trunk/natbraille-starmath/doctest/");

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        XPath xPath = XPathFactory.newInstance().newXPath();

        List<Example> examples = new ArrayList();

        Files.walk(path)
                .filter(Files::isRegularFile)
                .forEach((p) -> {
                    //.forEachOrdered((p) -> {                    
                    System.out.println(p);
                    try {
                        if (p.endsWith("content.xml") || p.toString().endsWith(".mml")) {
                            Document doc;
                            doc = dBuilder.parse(p.toFile());
                            examples.addAll(examplesFromDocument(doc, xPath));
                        } else if (p.toString().endsWith("odt") || p.toString().endsWith(".odf")) {
                            Map<String, String> eqsInZip = OdtMathMLExtractor.extract(p.toFile());
                            eqsInZip.entrySet().forEach(e -> {
                                Document doc;
                                String mathmlString = e.getValue();
                                InputSource dis = new InputSource(new StringReader(mathmlString));
                                try {
                                    doc = dBuilder.parse(dis);
                                    examples.addAll(examplesFromDocument(doc, xPath));
                                } catch (SAXException | IOException ex) {
                                    Logger.getLogger(Validation.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            });
                        } else {
                            System.out.println("ignore" + p);

                        }
                    } catch (SAXException | IOException ex) {
                        Logger.getLogger(Validation.class.getName()).log(Level.SEVERE, null, ex);
                    }

                });
        return examples;
    }

    private static String roughlyCompareStarMathComp(String version) {
        return version.replace("{", "").replace("}", "").replace(" ", "");
    }

    private static boolean roughlyCompareStarMath(String versionA, String versionB) {
        return (versionA != null)
                && (versionB != null)
                && roughlyCompareStarMathComp(versionA).equals(roughlyCompareStarMathComp(versionB));
    }

    public static void main(String[] args) throws ParserConfigurationException, IOException, TransformerException, NatFilterException {

        ToFromBrl toFromBrl = new ToFromBrl();

        Document doc = DomUtils.newDoc();
        Element html = doc.createElement("html");
        doc.appendChild(html);

        Element head = doc.createElement("head");
        html.appendChild(head);
        Element title = doc.createElement("title");
        title.setTextContent("tests");
        head.appendChild(title);

        Element body = doc.createElement("body");
        html.appendChild(body);

        Element table = doc.createElement("table");
        body.appendChild(table);

        {
            Element tableheader = doc.createElement("tr");
            table.appendChild(tableheader);

            String captions[] = new String[]{
                "original starmath",
                "original mathml",
                "mathml from original starmath",
                "starmath from original mathml", //                "starmath from mathml from original starmath",
                //"does starmaths ~ match ?",
                "braille from original starmath",
                "braille from original starmath",
                "mathml from gen. braille",
                "starmath from gen. braille",
            };
            for (String caption : captions) {
                Element th = doc.createElement("th");
                th.setTextContent(caption);
                tableheader.appendChild(th);
            }
        }

        Element cases = table;

        List<Example> examples = buildExamples();
        //examples.sort((o1, o2) -> o1.starMath.compareTo(o2.starMath));
        examples.stream().forEach/*Ordered*/((example) -> {
                    System.out.println("-------------------------> processing:" + example.starMath);
                    Element el = doc.createElement("tr");

                    String starMathVersionA = null;
                    String starMathVersionB = null;

                    cases.appendChild(el);
                    {
                        Element starMath = doc.createElement("td");
                        starMath.setAttribute("class", "starmath");
                        starMath.setTextContent(example.starMath);
                        starMathVersionA = example.starMath;
                        el.appendChild(starMath);
                    }

                    {
                        Element expectedMathML = doc.createElement("td");
                        expectedMathML.setAttribute("class", "expected-mathml");
                        Node mathml = example.mathMLDocument.getFirstChild().cloneNode(true);
                        doc.adoptNode(mathml);
                        expectedMathML.appendChild(mathml);
                        el.appendChild(expectedMathML);
                    }
                    Document destMathML;
                    {
                        Element generatedMathML = doc.createElement("td");
                        generatedMathML.setAttribute("class", "generated-mathml");
                        Document parsedStarMath = StarMathParser.parseString(example.starMath, true);
                        destMathML = parsedStarMath;
                        Node mathml2 = parsedStarMath.getFirstChild().cloneNode(true);
                        doc.adoptNode(mathml2);
                        generatedMathML.appendChild(mathml2);
                        el.appendChild(generatedMathML);

                    }
                    if (true) {
                        try {
                            Element generatedStarMath = doc.createElement("td");
                            generatedStarMath.setAttribute("class", "generated-starmath");
                            el.appendChild(generatedStarMath);
                            String textContent = StarMathExport.mathmlToStarMathAsString(example.mathMLDocument);
                            // generatedStarMath.setTextContent(textContent);
                            starMathVersionB = textContent;
                            Element asElement = StarMathExport.mathmlToStarMathAsElement(example.mathMLDocument);
                            asElement.getParentNode().removeChild(asElement);
                            doc.adoptNode(asElement);
                            generatedStarMath.appendChild(asElement);

                        } catch (SAXException ex) {
                            Logger.getLogger(Validation.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (IOException ex) {
                            Logger.getLogger(Validation.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (ParserConfigurationException ex) {
                            Logger.getLogger(Validation.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (IndexOutOfBoundsException ex) {
                            Logger.getLogger(Validation.class.getName()).log(Level.SEVERE, null, ex);
                        }

                    }

                    if (false) {
                        Element starMathComparation = doc.createElement("td");
                        starMathComparation.setAttribute("class", "rough-starmath-comparation");
                        if (roughlyCompareStarMath(starMathVersionA, starMathVersionB)) {
                            starMathComparation.setTextContent("yes");
                        } else {
                            starMathComparation.setTextContent("no");
                            starMathComparation.setAttribute("style", "color:red;");
                        }
                        el.appendChild(starMathComparation);
                    }
                    if (false) {
                        try {
                            Element generatedMathML = doc.createElement("td");
                            generatedMathML.setAttribute("class", "generated-startmath-from-generated-mathml");
                            el.appendChild(generatedMathML);
                            String textContent = StarMathExport.mathmlToStarMathAsString(destMathML);
                            generatedMathML.setTextContent(textContent);
                        } catch (SAXException ex) {
                            Logger.getLogger(Validation.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (IOException ex) {
                            Logger.getLogger(Validation.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (ParserConfigurationException ex) {
                            Logger.getLogger(Validation.class.getName()).log(Level.SEVERE, null, ex);
                        }

                    }
                    // starmath -> braille
                    String generatedBrailleString = "";
                    if (true) {
                        Element generatedBraille = doc.createElement("td");
                        generatedBraille.setAttribute("class", "generated-codeus-braille-from-starmath");
                        el.appendChild(generatedBraille);
                        String textContent = toFromBrl.starMathToBrl(starMathVersionA, true);
                        generatedBraille.setTextContent(textContent);
                    }
                    if (true) {
                        Element generatedBraille = doc.createElement("td");
                        generatedBraille.setAttribute("class", "generated-braille-from-starmath");
                        el.appendChild(generatedBraille);
                        String textContent = toFromBrl.starMathToBrl(starMathVersionA, false);
                        generatedBrailleString = textContent;
                        generatedBraille.setTextContent(textContent);
                    }

// braille -> mathml
                    if (true) {
                        Element mathmlFromBraille = doc.createElement("td");
                        mathmlFromBraille.setAttribute("class", "generated-mathml-from-braille");
                        el.appendChild(mathmlFromBraille);

                        Document generatedMathmlFromBrailleDoc = toFromBrl.brailleToMathML(generatedBrailleString);
                        String sss = DomUtils.outerXml(generatedMathmlFromBrailleDoc);
                        System.err.println("########################");
                        System.err.println(sss);

                        if (generatedMathmlFromBrailleDoc != null) {
                            Node generatedMathmlFromBraille = generatedMathmlFromBrailleDoc.getFirstChild().cloneNode(true);
                            if ((generatedMathmlFromBraille != null)) {
                                doc.adoptNode(generatedMathmlFromBraille);
                                mathmlFromBraille.appendChild(generatedMathmlFromBraille);
                            }
                        }

                    }
                    if (true) {
                        Element starMathFromBraille = doc.createElement("td");
                        starMathFromBraille.setAttribute("class", "generated-startmath-from-braille");
                        el.appendChild(starMathFromBraille);
                        
                        String textContent = toFromBrl.brailleToStarMath(generatedBrailleString);
                        generatedBrailleString = textContent;
                        starMathFromBraille.setTextContent(textContent);
                    }
                    // parseString

                    // Element mathml = doc.createElement("mathml");
                    // starMath.setTextContent(example.starMath);
/*
            
            System.out.println(example.starMath
                    + "\t"
                    + example.mathML
            
    
    .replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n", "")
                            .replaceAll("<annotation encoding=\"StarMath 5.0\">.*", "")

            );
                     */
                });
        Element styleElement = doc.createElement("link");
        head.appendChild(styleElement);
        styleElement.setAttribute("rel", "stylesheet");
        styleElement.setAttribute("href", "flume.css");

        Element scriptElement = doc.createElement("script");
        body.appendChild(scriptElement);
        scriptElement.setAttribute("src", "flume.js");
        // <link rel="stylesheet" type="text/css" href="theme.css">

//        System.err.println("-----");
        //      System.err.println(DomUtils.convertXMLDocumentToString(doc));
        Files.write(Paths.get("./flume.html"), DomUtils.convertXMLDocumentToString(doc).getBytes());
        // Files.write(Paths.get("c:/output.txt"), content.getBytes());

        //System.err.println(DomUtils.outerXml(doc));
    }

}
