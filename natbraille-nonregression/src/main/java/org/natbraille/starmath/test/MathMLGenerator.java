/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * Author : Vivien Guillet <vivien.guillet@laposte.net>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.starmath.test;

import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author vivien
 */
public class MathMLGenerator {

    /*
    <mi> (Identifier)
    <mn> (Number)
    <mo> (Operator)
    <mtext> (Text)
    <mglyph> (Displaying non-standard symbols)
    <ms> (String literal)
    <mspace> (Space)
    
     */
    int TYPE_MATH = 1 << 0;
    int TYPE_MROW = 1 << 1;
    int TYPE_MN = 1 << 2;
    int TYPE_MO = 1 << 3;
    int TYPE_MI = 1 << 4;
    int TYPE_MTEXT = 1 << 5;
    int TYPE_MS = 1 << 6;
    int TYPE_MSPACE = 1 << 7;

    /*    <mrow> (Grouped sub-expressions)
    */
      int TYPE_ROW = 1 << 8;
    /*
    <msqrt> (Square root without an index)
    <mroot> (Radical with specified index)
    <mfrac> (Fraction)
    <munder> (Underscript)
    <mover> (Overscript)
    <munderover> (Underscript-overscript pair)
    <msub> (Subscript)
    <msup> (Superscript)
    <msubsup> (Subscript-superscript pair)
    <mmultiscripts> (Prescripts and tensor indices)
    <none>
    <mlongdiv> (Long division notation)   
    <mscarries> (Annotations such as carries)
    <mscarry> (Single carry, child element of <mscarries>)
    <mfenced>  (Parentheses)
    <menclose> (Enclosed contents)
*/
    int TYPE_MSQRT = 1 << 9;
    int TYPE_MROOT = 1 << 10;
    int TYPE_MFRAC = 1 << 11;
    int TYPE_MUNDER = 1 << 12;
    int TYPE_MOVER = 1 << 13;
    int TYPE_MUNEROVER = 1 << 14;
    int TYPE_MSUB = 1 << 15;
    int TYPE_MSUP = 1 << 16;
    int TYPE_MSUBSUP = 1 << 17;
    int TYPE_MMULTISCRIPTS = 1 << 18;
    int TYPE_NONE = 1 << 19;    
    int TYPE_MLONGDIV = 1 << 20;
    int TYPE_MSCARRIES = 1 << 21;
    int TYPE_MSCARRY = 1 << 22;
    int TYPE_MFENCED = 1 << 23;
    int TYPE_MENCLOSE = 1 << 24;
    

/*
            // stack;
    <mstack> (Stacked alignment)
    <msrow> (Rows in <mstack> elements)
    <msline> (Horizontal lines inside <mstack> elements)
    */
    int TYPE_MSTACK= 1 << 25 ;
    int TYPE_MSROW = 1 << 26;
    int TYPE_MSLINE = 1 << 27;
    /*

            // array
    <mtable> (Table or matrix)
    <mtd> (Cell in a table or a matrix)
    <mtr> (Row in a table or a matrix)
    <mlabeledtr> (Labeled row in a table or a matrix)
    <maligngroup> (Alignment group)
    <malignmark> (Alignment points)
*/
    
    int TYPE_MTABLE = 1 << 28;
    int TYPE_MTD = 1 << 29;
    int TYPE_MTR = 1 << 30;
    int TYPE_MLABELEDTR = 1 << 31;
    //int TYPE_M = 1 << 32;
    //int TYPE_M = 1 << 33;
    
    /*
    <msgroup> (Grouped rows of <mstack> and <mlongdiv> elements)


    <mpadded> (Space around content)
    <mphantom> (Invisible content with reserved space)


//    <maction> (Binded actions to sub-expressions)
//    <merror> (Enclosed syntax error messages)
    
    <mstyle> (Style change)
    <semantics> (Container for semantic annotations)
    <annotation> (Data annotations)
    <annotation-xml> (XML annotations)
     */
    String[] INSTANCE_LETTER = {"a", "b"};

    public static int randomMinMax(int min, int max) {
        return ThreadLocalRandom.current().nextInt(min, max + 1);
    }

    public static String generate(int typeconstraint) {

        /*
        switch (typeconstraint) {
            case (TYPE_LETTER): {
                break;
            }
        }
         */ return "a";
    }

    public static void main(String[] args) {
        // String mathml = generate(TYPE_MATHML);
        for (int i = 0; i < 64; i++) {
            int a = 1 << i;
            long b = (long)((long)1) << i;
            //BigInteger z = new BigInteger();
            //z = z << i;
     //       System.err.println(i + ":" + a + ":" + ":" + b+":"+z);
        }
    }
/*
    "\\frac{%1$s}{%2$s}"
    
     }*/
}
