/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * Author : Vivien Guillet <vivien.guillet@laposte.net>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.starmath.test;

import java.util.Arrays;

/**
 *
 * @author vivien
 * get combinations/arrangements of symbols for studying
 * what starmath calls a number from digits, commas and dots 
 */
public class AllLists {

    // https://stackoverflow.com/questions/5113707/getting-every-possible-permutation-of-a-string-or-combination-including-repeated?rq=1
    public static String[] getAllLists(String[] elements, int lengthOfList) {
        //initialize our returned list with the number of elements calculated above
        String[] allLists = new String[(int) Math.pow(elements.length, lengthOfList)];

        //lists of length 1 are just the original elements
        if (lengthOfList == 1) {
            return elements;
        } else {
            //the recursion--get all lists of length 3, length 2, all the way up to 1
            String[] allSublists = getAllLists(elements, lengthOfList - 1);

            //append the sublists to each element
            int arrayIndex = 0;

            for (int i = 0; i < elements.length; i++) {
                for (int j = 0; j < allSublists.length; j++) {
                    //add the newly appended combination to the list
                    allLists[arrayIndex] = elements[i] + allSublists[j];
                    arrayIndex++;
                }
            }

            return allLists;
        }
    }
/*
    public static void main(String[] a) {
        System.out.println("");

        String rez[] = getAllLists(new String[]{"1", ",", "."}, 5);
        // System.out.println(rez.length);
        Arrays.stream(rez).forEach(
                x -> System.out.println("\"" + x + "\"" + " " + x + " " + "newline")
        );

    }
*/
}
