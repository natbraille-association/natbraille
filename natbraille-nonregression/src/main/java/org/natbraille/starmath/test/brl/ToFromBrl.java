/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.natbraille.starmath.test.brl;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.dom.DOMSource;
import org.natbraille.core.NatDynamicResolver;
import org.natbraille.core.document.NatDocument;
import org.natbraille.core.document.NatDocumentException;
import org.natbraille.core.filter.NatFilterChain;
import org.natbraille.core.filter.NatFilterConfigurator;
import org.natbraille.core.filter.NatFilterException;
import org.natbraille.core.filter.convertisseur.StarMathToMathMLFilter;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.core.filter.presentateur.BrailleTableModifierJava;
import org.natbraille.core.filter.presentateur.MathMLToStarMathFilter;
import org.natbraille.core.filter.presentateur.PresentateurMEP;
import org.natbraille.core.filter.transcodeur.TranscodeurNormal;
import org.natbraille.core.filter.transcriptor.Transcriptor;
import org.natbraille.core.gestionnaires.AfficheurAnsiConsole;
import org.natbraille.core.gestionnaires.GestionnaireErreur;
import org.natbraille.core.gestionnaires.LogLevel;
import org.natbraille.starmath.DomUtils;
import org.w3c.dom.Document;

/**
 *
 * @author vivien
 */
public class ToFromBrl {

    NatDynamicResolver natDynamicResolver;
    private GestionnaireErreur gestionnaireErreurs;
    //XMLReader xmlreader;
    private NatFilterChain starToCodeUsBrlFilter;
    private NatFilterChain starToUtf8BrlFilter;
    private NatFilterChain brailleToMathMLFilter;
    private NatFilterChain brailleToStarMathFilter;

    public String starMathToBrl(String starMath, boolean codeus) {
        NatFilterChain nfc = codeus ? starToCodeUsBrlFilter : starToUtf8BrlFilter;
        try {
            NatDocument in = new NatDocument();
            in.setString(starMath);
            NatDocument out = nfc.run(in);
            String s = out.getString();
            return s;
        } catch (IOException | TransformerException | NatDocumentException | NatFilterException ex) {
            return ex.getMessage();
        }
    }

    public String brailleToStarMath(String braille) {                
        NatFilterChain nfc = brailleToStarMathFilter;
        try {
            NatDocument in = new NatDocument();
            in.setString(braille);
            NatDocument out = nfc.run(in);
            String s = out.getString();
            Document doc = DomUtils.convertStringToXMLDocument(s);                
            // do not return structure
            return doc.getFirstChild().getTextContent();
        } catch (IOException | TransformerException | NatDocumentException | NatFilterException ex) {
            return ex.getMessage();
        }
    }

    public Document brailleToMathML(String braille) {

        String startTag = "<m:math>";
        String endTag = "</m:math>";

        String startReplacement = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\" display=\"block\">\n"
                + "                  <semantics xmlns=\"\">";
        String endReplacement = " </semantics></math>";

        NatFilterChain nfc = brailleToMathMLFilter;
        try {
            NatDocument in = new NatDocument();
            in.setString(braille);
            NatDocument out = nfc.run(in);
            String s = out.getString();
            String s4;
            {
                int i = s.indexOf(startTag);
                int j = s.indexOf(endTag);
                if ((i >= 0) && (j >= 0)) {
                    String s1 = s.substring(i + startTag.length(), j);
                    String s2 = s1.replaceAll("<m:", "<");
                    String s3 = s2.replaceAll("</m:", "</");
                    s4 = startReplacement + s3 + endReplacement;
                } else {
                    s4 = null;
                }
            }
            if (s4 != null) {
                Document doc = DomUtils.convertStringToXMLDocument(s4);
                return doc;

            }
        } catch (NatDocumentException ex) {
            Logger.getLogger(ToFromBrl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NatFilterException ex) {
            Logger.getLogger(ToFromBrl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ToFromBrl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (TransformerException ex) {
            Logger.getLogger(ToFromBrl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public ToFromBrl() throws NatFilterException {
        this.gestionnaireErreurs = new GestionnaireErreur();
//        AfficheurAnsiConsole aff = new AfficheurAnsiConsole(LogLevel.ULTRA);
        // AfficheurAnsiConsole aff = new AfficheurAnsiConsole(LogLevel.NONE);
        AfficheurAnsiConsole aff = new AfficheurAnsiConsole(LogLevel.NORMAL);
        gestionnaireErreurs.addAfficheur(aff);

        System.setProperty("javax.xml.parsers.DocumentBuilderFactory",
                "org.apache.xerces.jaxp.DocumentBuilderFactoryImpl");
        System.setProperty("javax.xml.xpath.XPathFactory:http://java.sun.com/jaxp/xpath/dom",
                "com.sun.org.apache.xpath.internal.jaxp.XPathFactoryImpl");

        starToCodeUsBrlFilter = mkStarMathToBrl("CodeUS");
        starToUtf8BrlFilter = mkStarMathToBrl("brailleUTF8");

        brailleToMathMLFilter = mkBrlToMathML();
        brailleToStarMathFilter = mkBrlToStarMath();
    }

    private NatFilterChain mkBrlToStarMath() throws NatFilterException {
        /* {
    "filters":[
	"org.natbraille.core.filter.transcriptor.Transcriptor",
        "org.natbraille.core.filter.presentateur.MathMLToStarMathFilter",
    ],
    "options":{
	"FORMAT_OUTPUT_ENCODING":"UTF-8",
        "MODE_DETRANS":"true",
        "debug_dyn_xsl_write_temp_file":"true",
        "debug_document_write_options_file":"true",
        "debug_document_show":"true",
        "debug_document_show_max_char":"1000",
        "debug_dyn_ent_res_show":"true",
        "debug_dyn_res_show":"true",
        "debug_dyn_xsl_show":"true",
        "debug_xsl_processing":"true",
        "debug_document_write_temp_file":"true",

    }
}

         */
        NatFilterOptions nfo = new NatFilterOptions();
        nfo.setOption(NatFilterOption.MODE_DETRANS, "true");

        nfo.setOption(NatFilterOption.CONFIG_DESCRIPTION, "desc");
        nfo.setOption(NatFilterOption.FORMAT_OUTPUT_ENCODING, "UTF-8");
        NatFilterConfigurator filterConfigurator = new NatFilterConfigurator(nfo, gestionnaireErreurs);
        NatFilterChain chain = filterConfigurator.newFilterChain();
        chain.addNewFilter(Transcriptor.class);
        chain.addNewFilter(MathMLToStarMathFilter.class);

        return chain;

    }

    private NatFilterChain mkBrlToMathML() throws NatFilterException {

//        AfficheurAnsiConsole aff = new AfficheurAnsiConsole(LogLevel.ULTRA);
        // AfficheurAnsiConsole aff = new AfficheurAnsiConsole(LogLevel.NONE);
        //      AfficheurAnsiConsole aff = new AfficheurAnsiConsole(LogLevel.NORMAL);
//        gestionnaireErreurs.addAfficheur(aff);
        NatFilterOptions nfo = new NatFilterOptions();
        nfo.setOption(NatFilterOption.MODE_DETRANS, "true");

        nfo.setOption(NatFilterOption.CONFIG_DESCRIPTION, "desc");
        nfo.setOption(NatFilterOption.FORMAT_OUTPUT_ENCODING, "UTF-8");

        /*        //nfo.setOption(NatFilterOption.FORMAT_OUTPUT_ENCODING, "UTF-8");
        // nfo.setOption(NatFilterOption.FORMAT_OUTPUT_BRAILLE_TABLE, brailleTable);
        nfo.setOption(NatFilterOption.MODE_MATH_ALWAYS_PREFIX, "true");
        nfo.setOption(NatFilterOption.MODE_MATH_SPECIFIC_NOTATION, "true");
        nfo.setOption(NatFilterOption.FORMAT_LINE_LENGTH, "10000");
        nfo.setOption(NatFilterOption.MODE_LIT, "false");
        nfo.setOption(NatFilterOption.MODE_G2, "false");
        nfo.setOption(NatFilterOption.LAYOUT, "false");
        nfo.setOption(NatFilterOption.MODE_LIT_FR_MAJ_DOUBLE, "false");
        nfo.setOption(NatFilterOption.MODE_LIT_FR_MAJ_PASSAGE, "false");
        nfo.setOption(NatFilterOption.MODE_LIT_FR_MAJ_MELANGE, "false");
        nfo.setOption(NatFilterOption.MODE_LIT_EMPH, "false");
        nfo.setOption(NatFilterOption.MODE_LIT_FR_EMPH_IN_WORD, "false");
        nfo.setOption(NatFilterOption.MODE_LIT_FR_EMPH_IN_PASSAGE, "false");
        nfo.setOption(NatFilterOption.MODE_CHEMISTRY, "false");
         */ nfo.setOption(NatFilterOption.debug_document_show, "true");
        nfo.setOption(NatFilterOption.debug_document_show_max_char, "1000");

        NatFilterConfigurator filterConfigurator = new NatFilterConfigurator(nfo, gestionnaireErreurs);
        NatFilterChain chain = filterConfigurator.newFilterChain();
        //xmlreader = filterConfigurator.getNatDynamicResolver().getXMLReader();
        natDynamicResolver = filterConfigurator.getNatDynamicResolver();
        chain.addNewFilter(Transcriptor.class);
        return chain;

    }

    private NatFilterChain mkStarMathToBrl(String brailleTable) throws NatFilterException {

        NatFilterOptions nfo = new NatFilterOptions();
        nfo.setOption(NatFilterOption.CONFIG_DESCRIPTION, "desc");

        //nfo.setOption(NatFilterOption.FORMAT_OUTPUT_ENCODING, "UTF-8");
        //nfo.setOption(NatFilterOption.FORMAT_OUTPUT_ENCODING, "UTF-8");
        nfo.setOption(NatFilterOption.FORMAT_OUTPUT_BRAILLE_TABLE, brailleTable);
        nfo.setOption(NatFilterOption.MODE_MATH_ALWAYS_PREFIX, "true");
        nfo.setOption(NatFilterOption.MODE_MATH_SPECIFIC_NOTATION, "true");
        nfo.setOption(NatFilterOption.FORMAT_LINE_LENGTH, "10000");
        nfo.setOption(NatFilterOption.MODE_LIT, "false");
        nfo.setOption(NatFilterOption.MODE_G2, "false");
        nfo.setOption(NatFilterOption.LAYOUT, "false");
        nfo.setOption(NatFilterOption.MODE_LIT_FR_MAJ_DOUBLE, "false");
        nfo.setOption(NatFilterOption.MODE_LIT_FR_MAJ_PASSAGE, "false");
        nfo.setOption(NatFilterOption.MODE_LIT_FR_MAJ_MELANGE, "false");
        nfo.setOption(NatFilterOption.MODE_LIT_EMPH, "false");
        nfo.setOption(NatFilterOption.MODE_LIT_FR_EMPH_IN_WORD, "false");
        nfo.setOption(NatFilterOption.MODE_LIT_FR_EMPH_IN_PASSAGE, "false");
        nfo.setOption(NatFilterOption.MODE_CHEMISTRY, "false");

        nfo.setOption(NatFilterOption.debug_document_show, "true");
        nfo.setOption(NatFilterOption.debug_document_show_max_char, "1000");

        NatFilterConfigurator filterConfigurator = new NatFilterConfigurator(nfo, gestionnaireErreurs);
        NatFilterChain chain = filterConfigurator.newFilterChain();
        chain.addNewFilter(StarMathToMathMLFilter.class);
        chain.addNewFilter(TranscodeurNormal.class);
        chain.addNewFilter(PresentateurMEP.class);
        chain.addNewFilter(BrailleTableModifierJava.class);

        return chain;

    }

}
/*
        nfo.setOption(NatFilterOption.debug_dyn_xsl_write_temp_file, "true");
        nfo.setOption(NatFilterOption.debug_document_write_options_file, "true");
 */
 /*
        nfo.setOption(NatFilterOption.debug_document_write_temp_file, "true");
        nfo.setOption(NatFilterOption.debug_dyn_ent_res_show, "true");
        nfo.setOption(NatFilterOption.debug_dyn_res_show, "true");
        nfo.setOption(NatFilterOption.debug_dyn_xsl_show, "true");
        nfo.setOption(NatFilterOption.debug_xsl_processing, "true");
 */
