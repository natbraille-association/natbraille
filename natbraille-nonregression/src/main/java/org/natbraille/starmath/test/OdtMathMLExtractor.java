/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * Author : Vivien Guillet <vivien.guillet@laposte.net>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.starmath.test;

/**
 *
 * @author vivien
 */
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class OdtMathMLExtractor {

    private static final int BUFFER_SIZE = 4096;

    public static Map<String, String> extract(ZipInputStream zipIn) throws IOException {
        Map<String, String> equations = new HashMap();
        ZipEntry entry = zipIn.getNextEntry();
        while (entry != null) {
            if (!entry.isDirectory()) {
                String mathMLString = getMathMLEntryAsString(zipIn);
                if (mathMLString != null) {
                    equations.put(entry.getName(), mathMLString);
                }
            }
            zipIn.closeEntry();
            entry = zipIn.getNextEntry();
        }
        zipIn.close();
        return equations;
    }

    /*
     * Extract all math equations from a odt zip stream
     * returns <object name/number, equation as String>
     */
    private static String getMathMLEntryAsString(ZipInputStream zipIn) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] bytesIn = new byte[BUFFER_SIZE];
        int read = 0;
        while ((read = zipIn.read(bytesIn)) != -1) {
            baos.write(bytesIn, 0, read);
        }
        String s = baos.toString("utf8");
        if (s.contains("\n<math")) {
            return s;
        }
        return null;
    }

    /*
     * Extract all math equations from a odt zip file
     * returns <object name/number, equation as String>
     */
    public static Map<String, String> extract(File zipFilePath) throws IOException {
        return extract(new ZipInputStream(new FileInputStream(zipFilePath)));
    }

    // usage
    public static void main(String args[]) throws IOException {
        // String zipFilePath = "./doc/NormeMathématique2007b-marc.odt";
        String zipFilePath = "./doc/MG44-MathGuide.odt";

        OdtMathMLExtractor.extract(new File(zipFilePath)).entrySet().forEach(e -> {
            System.out.println("== " + e.getKey());
            System.out.println(e.getValue());

        });
    }

}
