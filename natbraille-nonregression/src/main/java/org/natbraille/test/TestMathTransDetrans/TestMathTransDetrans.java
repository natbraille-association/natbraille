/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.natbraille.test.TestMathTransDetrans;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.transform.TransformerException;
import org.natbraille.brailletable.BrailleTables;
import org.natbraille.core.document.NatDocument;
import org.natbraille.core.document.NatDocumentException;
import org.natbraille.core.filter.NatFilterConfigurator;
import org.natbraille.core.filter.NatFilterException;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.core.filter.transcriptor.Transcriptor;
import org.natbraille.core.gestionnaires.AfficheurAnsiConsole;
import org.natbraille.core.gestionnaires.GestionnaireErreur;
import org.natbraille.core.gestionnaires.LogLevel;
import org.natbraille.starmath.DomUtils;
import org.natbraille.starmath.test.Validation;

/**
 *
 * @author vivien
 */
public class TestMathTransDetrans {

    static String MODE_MATH_SPECIFIC_NOTATION = "false";

    public static Transcriptor getMathMLToBrailleTranscriptor() throws NatFilterException {

        System.setProperty("javax.xml.parsers.DocumentBuilderFactory",
                "org.apache.xerces.jaxp.DocumentBuilderFactoryImpl");
        System.setProperty("javax.xml.xpath.XPathFactory:http://java.sun.com/jaxp/xpath/dom",
                "com.sun.org.apache.xpath.internal.jaxp.XPathFactoryImpl");

        GestionnaireErreur ge = new GestionnaireErreur();

        AfficheurAnsiConsole afficheurConsole = new AfficheurAnsiConsole(LogLevel.NONE);
        ge.addAfficheur(afficheurConsole);

        NatFilterOptions nfo = new NatFilterOptions();
        // nfo.setOption(NatFilterOption.MODE_DETRANS, "true");
        nfo.setOption(NatFilterOption.MODE_MATH_ALWAYS_PREFIX, "true");
        nfo.setOption(NatFilterOption.MODE_MATH_SPECIFIC_NOTATION, MODE_MATH_SPECIFIC_NOTATION);
        nfo.setOption(NatFilterOption.FORMAT_LINE_LENGTH, "10000");
        nfo.setOption(NatFilterOption.MODE_LIT, "false");
        nfo.setOption(NatFilterOption.MODE_G2, "false");
        nfo.setOption(NatFilterOption.LAYOUT, "false");

        NatFilterConfigurator configurator = new NatFilterConfigurator(nfo, ge);
        Transcriptor transcriptor = new Transcriptor(configurator);
        return transcriptor;
    }

    public static Transcriptor getBrailleToMathMLTranscriptor() throws NatFilterException {

        System.setProperty("javax.xml.parsers.DocumentBuilderFactory",
                "org.apache.xerces.jaxp.DocumentBuilderFactoryImpl");

        System.setProperty("javax.xml.xpath.XPathFactory:http://java.sun.com/jaxp/xpath/dom",
                "com.sun.org.apache.xpath.internal.jaxp.XPathFactoryImpl");

        GestionnaireErreur ge = new GestionnaireErreur();

        AfficheurAnsiConsole afficheurConsole = new AfficheurAnsiConsole(LogLevel.NONE);
        ge.addAfficheur(afficheurConsole);

        NatFilterOptions nfo = new NatFilterOptions();
        nfo.setOption(NatFilterOption.MODE_DETRANS, "true");
        nfo.setOption(NatFilterOption.MODE_MATH_SPECIFIC_NOTATION, MODE_MATH_SPECIFIC_NOTATION);
        nfo.setOption(NatFilterOption.MODE_DETRANS_KEEP_SOURCE_OFFSETS, "true");
        nfo.setOption(NatFilterOption.debug_dyn_xsl_show, "false");

        NatFilterConfigurator configurator = new NatFilterConfigurator(nfo, ge);
        Transcriptor transcriptor = new Transcriptor(configurator);
        return transcriptor;
    }

    public static void testMathtrans(String[] args) throws Exception {
        Transcriptor transcriptor = getMathMLToBrailleTranscriptor();

        int i = 0;
        Validation.buildExamples().subList(0, 1).forEach(e -> {
            try {

                String docString = DomUtils.convertXMLDocumentToString(e.mathMLDocument)
                        .replace("<semantics xmlns=\"\">", "")
                        .replace("</semantics>", "");
                System.out.println(docString);
                NatDocument inDoc = new NatDocument();
                inDoc.setString(docString);
                NatDocument outDoc;
                try {
                    outDoc = transcriptor.run(inDoc);
                    System.out.println(outDoc.getString());
                } catch (NatFilterException ex) {
                    Logger.getLogger(TestMathTransDetrans.class.getName()).log(Level.SEVERE, null, ex);
                }

            } catch (NatDocumentException | IOException | TransformerException ex) {
                Logger.getLogger(TestMathTransDetrans.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    public static void testMathDetrans(String[] args) throws Exception {
        Transcriptor transcriptor = getBrailleToMathMLTranscriptor();
        for (String message : new String[]{ 
            // ok 
            // " ⠠⠄⠜⠩⠩⠩⠖⠁⠆"          // sqrt
            // " ⠠⠄⠜⠩⠩⠩⠖⠦⠁⠖⠁⠴⠆"     // parent simple
            // " ⠠⠄⠜⠩⠩⠩⠖⠐⠦⠁⠖⠁⠐⠴⠆"  // parent double
            // " ⠠⠄⠁⠸⠨⠡⠁"             //  op complexe
            // "⠠⠄⠜⠩⠩⠩⠖⠖⠤⠁"          // op sans prefixe
            // "⠠⠄⠁⠌⠃" // fraction simple
             // "⠠⠄⠁⠌⠃⠖⠁" // fraction simple + suite
            // "⠠⠄⠁⠁⠌⠃⠌⠩⠖⠁" // fraction fraction + suite
            // "⠠⠄⠜⠩⠩⠩⠖⠁⠈⠃⠖⠁"    // exposant simple + suite
            //"⠠⠄⠜⠩⠩⠩⠖⠁⠢⠃⠖⠁"    // indice simple + suite    
            // "⠠⠄⠩⠖⠰⠁⠖⠃⠆⠢⠁", // complex sub first part
            // "⠠⠄⠩⠖⠰⠁⠖⠃⠆⠢⠰⠁⠖⠃⠆", // complex sub first part snd aprt
            // "⠠⠄⠩⠖⠁⠢⠃⠈⠰⠁⠖⠃⠆⠖⠙", // sub sup -> msupsub complex sup
            // "⠠⠄⠩⠖⠁⠢⠃⠈⠖⠁⠖⠙", // sub sup -> msupsub +sup 
            //"⠠⠄⠩⠖⠁⠢⠖⠃⠈⠖⠁⠖⠙", // sub sup -> msupsub +sub +sup 
            // "⠠⠄⠩⠖⠁⠢⠖⠃⠖⠩", // +sub 
            // "⠠⠄⠩⠖⠁⠈⠖⠃⠖⠩", // +sup
            // "⠠⠄⠩⠖⠁⠈⠃⠢⠉⠢⠉⠖⠙",    // sup sub  ?? should be the same than sub sup ?
            // "⠠⠄⠩⠖⠁⠢⠢⠉⠖⠁" // souscrit souscrit -> munder
            // "⠠⠄⠩⠖⠖⠁⠢⠢⠖⠉⠖⠁" // souscrit souscrit + // ?? should be a / + / +a ?  , not a / + / + / a ??
            // "⠠⠄⠩⠖⠁⠈⠈⠉⠖⠁" // surscrit surscrit -> mover
            // "⠠⠄⠩⠖⠁⠢⠢⠩⠈⠈⠉⠖⠁" // souscrit surscrit -> munderover
            // "⠠⠄⠁⠨⠖⠃" // sortieOperateur complexe (plus dans rond)
            // "⠠⠄⠨⠁" // sortieOperateur majuscule
            // "⠠⠄⠐⠁" // sortieOperateur ronde minuscule
            // "⠠⠄⠨⠐⠁" // sortieOperateur ronde majuscule
            // "⠠⠄⠨⠨⠁" // sortieOperateur éclairée
            //"⠠⠄⠘⠁" // sortieOperateur grecque minscule
            // "⠠⠄⠨⠘⠁" // sortieOperateur grecque majuscule -> ?? MARCHE PAS ??
            // "⠠⠄⠸⠤⠁" // sortieOperateur lettre barre -> ?? MARCHE PAS ??
            // "⠠⠄⠘⠘⠁" // sortieOperateur lettre hébraique
            // "⠠⠄⠨⠒⠈⠝⠁" // suscrit , tenseur "⠨⠒⠈⠝" // ?? pas compris ?? marche ??
            //"⠠⠄⠨⠒⠈⠝⠁⠶⠁" // suscrit , tenseur "⠨⠒⠈⠝" // ?? pas compris ?? marche ??
            // "⠠⠄⠈⠒⠁⠁⠶⠁" // surscrit arc + suite
            // "⠠⠄⠁⠶⠈⠹⠜⠁⠶⠁"   a=^4sqrta=a ?? MARCHE PAS !!
            // "⠠⠄⠈⠹⠹⠹⠜⠩⠩⠶⠁"  // ^4sqrta MARCHE 
            //"⠠⠄⠈⠹⠹⠹⠜⠩⠩⠶⠁"  // ^4sqrta MARCHE 
            //"⠠⠄⠈⠁"  // special case exponant alone
            // "⠠⠄⠈⠩⠩⠶⠁"  // special case exponant alone + suite
            // "⠠⠄⠢⠁"  // special case indice alone 
            // "⠠⠄⠢⠩⠩⠶⠁"  // special case indice alone  + suite
            
            // ------
            
            // "⠠⠄⠁⠌⠃⠖⠉⠌⠙⠶⠰⠁⠙⠖⠃⠉⠆⠌⠃⠙" // fractions
            // "⠠⠄⠦⠩⠩⠩⠖⠦⠁⠁⠖⠩⠴⠴\n⠁⠁"
            //
            // "⠠⠄⠜⠰⠩⠩⠩⠖⠁⠆"
            // "⠠⠄⠩⠌⠩"             
            // "⠠⠄⠨⠁⠘⠔⠨⠃"            
            "⠠⠄"          
                
        }) {

            NatDocument inBrailleDoc = new NatDocument();
            inBrailleDoc.setBrailletable(BrailleTables.unicodeBrailleTable());
            inBrailleDoc.setString(message);
            System.out.println(inBrailleDoc.getString());
            NatDocument outMathmlDoc = transcriptor.run(inBrailleDoc);
            System.out.println(outMathmlDoc.getString());

        }

        //  LibreOfficeTools.stopServer();
    }
    // problem :  ⠠⠄⠘⠒

    public static void testMathtransDetrans(String[] args) throws Exception {
        Transcriptor transcriptor = getMathMLToBrailleTranscriptor();
        Transcriptor detranscriptor = getBrailleToMathMLTranscriptor();

        List<Validation.Example> examples = Validation.buildExamples();
        int ok = 0;
        int ko = 0;
        int kko = 0;
        for (Validation.Example e : examples) {
            StringBuilder sb = new StringBuilder();
            try {
                sb.append("================================= ");
                sb.append("\n");
                sb.append(e.starMath);
                sb.append("\n");

                // format mathml
                String originalMathMLString = DomUtils.convertXMLDocumentToString(e.mathMLDocument)
                        .replace("<semantics xmlns=\"\">", "")
                        .replace("</semantics>", "")
                        .replace("<math xmlns=\"http://www.w3.org/1998/Math/MathML\" display=\"block\">", "<math>");
                sb.append(originalMathMLString);
                sb.append("\n");

                // mathml -> braille
                NatDocument doc1 = new NatDocument();
                doc1.setString(originalMathMLString);
                NatDocument doc2 = transcriptor.run(doc1);
                sb.append(doc2.getString().trim());
                sb.append("\n");

                // braille -> mathml
                NatDocument doc3 = detranscriptor.run(doc2);
                // sb.append("detrans to " + doc3.getString());

                // format mathml
                NatDocument doc4 = new NatDocument();
                doc4.setString(doc3.getString()
                        .replaceAll(" +", " ")
                        .replaceAll("\n", "")
                        .replaceAll(".*<p xml:space=\"preserve\">", "")
                        .replaceAll("</p>.+", "")
                        .replaceAll("<m:", "<")
                        .replaceAll("</m:", "</")
                        .replaceAll("\\s+xmlns=\"\"", "")
                        .replaceAll(".*<math", "<math")
                        .replaceAll("> <", "><")
                );
                sb.append(doc4.getString());
                sb.append("\n");

                // mathml -> braille
                NatDocument doc5 = transcriptor.run(doc4);
                sb.append(doc5.getString().trim());
                sb.append("\n");
                if (doc2.getString().equals(doc5.getString())) {
                    sb.append("=> satisfaction");
                    sb.append("\n");
                    ok++;
                } else {
                    ko++;

                    if (e.starMath.startsWith("%")) {
                        sb.append("GREEK!");
                        sb.append("\n");
                    }

                    sb.append("=> disatisfaction");
                    sb.append("\n");

                }
                System.out.println(sb.toString());
            } catch (NatFilterException | NatDocumentException | IOException | TransformerException ex) {
                // Logger.getLogger(Nat.class.getName()).log(Level.SEVERE, null, ex);
                kko++;
                System.out.println("=> VERY disatisfaction");
                System.out.println(sb.toString());

            }
            System.out.println("ok:" + ok + " ko:" + ko + " kko:" + kko);
        }

    }

    public static void main(String[] args) throws Exception {
      testMathDetrans(args);
        // testMathtrans(args);
            //testMathtransDetrans(args);
        // welcome(args);
    }

}
