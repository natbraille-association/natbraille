/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.natbraille.nonregression;

/**
 *
 * @author vivien
 */
public class Stats {

    public enum Result {

        ok("ok"),
        no_converter("!c"),
        no_source("!s"),
        no_reference("!r"),
        no_output("!o"),
        different("!="),
        blacklisted("bl"), 
        created("nw");

        private final String s;

        private Result(String s) {
            this.s = s;
        }

    }

    private final String configNames[];
    private final String sourceNames[];
    private final Result testResults[][];

    public void setTestResult(String configName, String sourceName, Result result) {
        testResults[pos(configNames, configName)][pos(sourceNames, sourceName)] = result;
    }

    public Result getTestResult(String configName, String sourceName) {
        return testResults[pos(configNames, configName)][pos(sourceNames, sourceName)];
    }

    public String getConfigName(int i) {
        return configNames[i];
    }

    public String getSourceName(int i) {
        return sourceNames[i];
    }

    public Result[] getConfigResult(String configName) {
        int configIdx = pos(configNames, configName);
        Result rv[] = new Result[sourceNames.length];
        for (int sourceIdx = 0; sourceIdx < rv.length; sourceIdx++) {
            rv[sourceIdx] = testResults[configIdx][sourceIdx];
        }
        return rv;
    }

    public Result[] getSourceResult(String sourceName) {
        int sourceIdx = pos(sourceNames, sourceName);
        Result rv[] = new Result[sourceNames.length];
        for (int configIdx = 0; configIdx < rv.length; configIdx++) {
            rv[configIdx] = testResults[configIdx][sourceIdx];
        }
        return rv;
    }

    public Stats(String configNames[], String sourceNames[]) {
        this.configNames = configNames;
        this.sourceNames = sourceNames;
        this.testResults = new Result[configNames.length][sourceNames.length];
    }

    private static int pos(String[] where, String what) {
        for (int i = 0; i < where.length; i++) {
            if (where[i].equals(what)) {
                return i;
            }
        }
        return -1;
    }
}
