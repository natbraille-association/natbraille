/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.natbraille.nonregression;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.IOUtils;
import org.natbraille.core.document.NatDocument;
import org.natbraille.core.document.NatDocumentException;
import org.natbraille.core.filter.NatFilterException;
import org.natbraille.core.filter.NatFilterConfigurator;
import org.natbraille.core.NatDynamicResolver;
import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.core.filter.transcriptor.Transcriptor;
import org.natbraille.core.gestionnaires.AfficheurConsole;
import org.natbraille.core.gestionnaires.GestionnaireErreur;
import org.natbraille.core.gestionnaires.LogLevel;

/**
 * Natbraille {@link Transcriptor} tests.
 * <p>
 * Test all combinations of {@link NatFilterOptions} and {@link NatDocuments} of
 * this package resources against expected transcription result if existing.
 * <p>
 * Command line uses four options without arguments after a single leading "-" :
 * <ul>
 * <li>w : write all matching transcriptions in a blacklist
 * <li>r : read the transcription blacklist
 * <li>c : delete and create result directory resu/
 * <li>e : transcribe sources having no refs
 * <li>d : ouput transcription debug
 * <li>m : test on maths documents located in srcMaths/
 * </ul>
 * <p>
 * Command line usage:
 * <ul>
 * <li>
 * <p>
 * Always use "c" option.
 * <li>
 * <p>
 * Use the "-rwc" combination (with caution) to re-run only failed tests. With
 * "r", transcriptions listed in "blacklist.txt" will not be run (and reported
 * as blacklisted). With "w", successfull transcriptions names will be written
 * in "blacklist.txt".
 * <li>
 * <p>
 * Use the "-wc" combination to restart with an empty blacklist (identical to
 * removing "blacklist.txt" manually and using "-wcr")
 * <li>
 * <p>
 * Use the "-rc" combination to use a custom "blacklist.txt" that should not be
 * overwritten
 * <li>
 * <p>
 * Use of neither "r" and "w" disables the blacklist system.
 * <li>
 * <p>
 * Use of no option prints help, and let the user manage "resu/" dir cleaning
 * himself.
 *
 * </ul>
 *
 * @author vivien
 */
public class App {

    private static final String RESU_DIR = "resu";
    //private static final String MATH_RESU_DIR = "resuMaths";

    private static final String CONFIG_RESOURCE_PATH = "conf/";
    private static final String REFERENCE_RESOURCE_PATH = "ref/";
    private static final String MISC_SOURCE_RESOURCE_PATH = "src/";
    private static final String MATH_RESOURCE_PATH = "srcMaths/";
    /**
     * list of resources
     */
    private static final String stdConfigNames[] = {
        "1-tranquille-nc",
        "2-stress-3r",
        "3-stress-tt",
        "4-megastress",
        "5-dbt",
        "6-special-maths",
        "7-detranscription",
        "abregeNoMep",
        "abregeDBT",
        "abrege",};
    private static final String abrConfigNames[] = {
        "abregeNoMep",
        "abregeDBT",
        "abrege",
        "progressif1"
    };

    private static final String miscSourceNames[] = {
        "A_MathCollege_styles-mathml.doc",
        "testChimie.doc",
        "testG1-tirets.odt",
        "Math_DiversV8.txt",
        "A_ExtraitEnvers_liste.doc",
        "testMathOpenOffice.odt",
        "erreurs_terres.odt",
        "testTableaux.odt",
        "testMath-trigo.odt",
        "Braille_base_Doc.txt",
        "Math_CollegeV8.txt",
        "testStyles-openoffice.odt",
        "testSauts.odt",
        "Braille_base.odt",
        "testSauts.txt",
        "Math_Lycée1.odt",
        "testG1.odt",
        "testG1-reglesComp.odt",
        "A_ExtraitEnvers_liste.odt",
        "Math_LycéeV8.txt",
        "Math_Lycee1_styles.doc",
        "A_ExtraitGiono_trucs.odt",
        "testChimie.odt",
        "testG1-Annexes.odt",
        "Math_Lycée_2V8.txt", //"testPartition.xml",
    //        "latex/testMathLatex.tex",
    //    "erreur_terres_titres.zob",
    };
    private static final String mathSourceNames[] = {
        "NormeMath2007_Exemples_0IntroductionConvMT.doc",
        "NormeMathématique2007_Exemples.odt"
    };

    private static final String abrSourceNames[] = {
        "etudions_abrege_exercices.txt",
        "SynthèseCorrectionNat.odt",
        "testAbregeProgressif.txt",
        "test_stylist_passages.odt",};

    /*
     public enum TestResult {

     NO_SRC,
     NO_REF,
     NO_DEST,
     DIFFS,
     OK
     }
     public TestResult testResults[][] = new TestResult[configNames.length][sourceNames.length];
     */
    /**
     * Build a transcriptor with given nat filter options and error handler.
     * {@link GestionnaireErreur}
     *
     * @param natFilterOptions
     * @param ge
     * @return
     */
    private static Transcriptor newTranscriptor(NatFilterOptions natFilterOptions, GestionnaireErreur ge) {
        NatDynamicResolver ndr = new NatDynamicResolver(natFilterOptions, ge);
        NatFilterConfigurator natFilterConfigurator = new NatFilterConfigurator().set(natFilterOptions).set(ge).set(ndr);
        Transcriptor transcriptor = new Transcriptor(natFilterConfigurator);
        return transcriptor;
    }

    /**
     * Build a {@link org.natbraille.core.document.NatDocument} from a resource
     * named resourceName
     *
     * @param resourceName
     * @return
     * @throws NatDocumentException
     */
    private static NatDocument geNatDocumentFromResourceName(String resourceName) throws NatDocumentException {
        NatDocument natDocument = new NatDocument();
        InputStream sourceInput = App.class
                .getResourceAsStream(resourceName);
        natDocument.setInputstream(sourceInput);
        return natDocument;
    }

    /**
     * Pad string with space to match given length
     *
     * @param string
     * @param length
     * @return
     */
    private static String fixedLengthString(String string, int length) {
        StringBuilder sb = new StringBuilder();
        sb.append(string);
        for (int i = 0; i < (length - string.length()); i++) {
            sb.append(" ");
        }
        // space before
        //return String.format("%1$" + length + "s", string);
        return sb.toString();
    }

    /**
     * Ensure presence and emptyness of output path.
     *
     * @param force
     * @return
     */
    private static boolean prepareOutputDir(boolean force) {
        boolean outputDirOk = false;
        try {
            if (force) {
                DirectoryCleaner.cleanUp(Paths.get(RESU_DIR));
                Files.createDirectories(Paths.get(RESU_DIR));
                outputDirOk = true;
                System.err.println("=> output dir " + RESU_DIR + " deleted/created");
            } else {
                if (Files.exists(Paths.get(RESU_DIR)) && Files.isDirectory(Paths.get(RESU_DIR))) {
                    File d = new File(RESU_DIR);
                    if (d.list().length == 0) {
                        outputDirOk = true;
                    } else {
                        System.err.println("=> " + RESU_DIR + " is not empty. manually delete contents or rerun with -c to delete/create at startup");
                    }
                } else {
                    System.err.println("=> please manually create a directory named '" + RESU_DIR + "';  or rerun with -c to delete/create at startup");
                }
            }
        } catch (Exception e) {
            System.err.println("=> unkown error related to " + RESU_DIR);
            e.printStackTrace();
        }
        return outputDirOk;
    }

    public static void main(String args[]) throws IOException {
        System.setProperty("javax.xml.parsers.DocumentBuilderFactory",
                "org.apache.xerces.jaxp.DocumentBuilderFactoryImpl");
        System.setProperty("javax.xml.xpath.XPathFactory:http://java.sun.com/jaxp/xpath/dom", 
                "com.sun.org.apache.xpath.internal.jaxp.XPathFactoryImpl");

        boolean optionReadBlackList = (args.length > 0 && args[0].startsWith("-") && args[0].contains("r"));
        boolean optionWriteBlackList = (args.length > 0 && args[0].startsWith("-") && args[0].contains("w"));
        boolean optionCreateResuDir = (args.length > 0 && args[0].startsWith("-") && args[0].contains("c"));
        boolean optionTranscribeIfNoRef = (args.length > 0 && args[0].startsWith("-") && args[0].contains("e"));
        boolean optionDebug = (args.length > 0 && args[0].startsWith("-") && args[0].contains("d"));
        boolean optionMaths = (args.length > 0 && args[0].startsWith("-") && args[0].contains("m"));
        boolean optionAbr = (args.length > 0 && args[0].startsWith("-") && args[0].contains("a"));

        if (args.length != 1) {
            System.err.println("usage : " + App.class
                    .getName() + ".jar  -crw");
            System.err.println("options : [-[crwdema]]*");
            System.err.println("    w : write all matching transcriptions in a blacklist");
            System.err.println("    r : read the transcription blacklist");
            System.err.println("    c : delete and create result directory " + RESU_DIR);
            System.err.println("    e : transcribe sources having no refs ");
            System.err.println("    d : display debug infos ");
            System.err.println("    m : uses sources of srcMaths instead of src");
            System.err.println("    a : uses sources of srcAbr instead of src");

        }

        Stats stats;
        String sources[];
        String sourceDir;
        String configNames[];
        if (optionMaths) {
            stats = new Stats(stdConfigNames, mathSourceNames);
            configNames = stdConfigNames;
            sources = mathSourceNames;
            sourceDir = MATH_RESOURCE_PATH;
        } else if (optionAbr) {
            stats = new Stats(abrConfigNames, abrSourceNames);
            configNames = abrConfigNames;
            sources = abrSourceNames;
            sourceDir = MATH_RESOURCE_PATH;
        } else {
            stats = new Stats(stdConfigNames, miscSourceNames);
            configNames = stdConfigNames;
            sources = miscSourceNames;
            sourceDir = MISC_SOURCE_RESOURCE_PATH;
        }
        /**
         * Init
         */
        // output dir
        if (!prepareOutputDir(optionCreateResuDir)) {
            System.exit(1);
        }

        // read blacklist
        BlackList inputBlackList;
        if (optionReadBlackList) {
            inputBlackList = new BlackList().read();
        } else {
            inputBlackList = new BlackList();
        }
        // create blacklist for potential writing
        BlackList matchingList = new BlackList();
        matchingList.addAll(inputBlackList);

        // find max sourcename length to align display
        int maxSourceLength = 0;
        for (String s : sources) {
            if (s.length() > maxSourceLength) {
                maxSourceLength = s.length();
            };
        }
        // create nat gestionnaire erreur
        GestionnaireErreur ge = new GestionnaireErreur();
        AfficheurConsole afficheurConsole;
        if (optionDebug) {
            afficheurConsole = new AfficheurConsole(LogLevel.DEBUG);
        } else {
            afficheurConsole = new AfficheurConsole(LogLevel.SILENT);
        }
        ge.addAfficheur(afficheurConsole);
        // init statistic counters 
        int nbTotalReferences = 0;
        int nbTotalMatchingReferences = 0;
        int nbTotalAborted = 0;
        int nbTotalMissingReference = 0;

        /**
         * For each config
         */
        for (String configName : configNames) {
            // init statistic counters for a configuration
            int nbConfigTotalReferences = 0;
            int nbConfigTotalMatchingReferences = 0;
            int nbConfigTotalAborted = 0;
            int nbConfigTotalMissingReference = 0;
            // build transcriptor for given config            
            System.out.println("Configuration : " + configName);
            Transcriptor transcriptor = null;
            try {
                // create options
                NatFilterOptions natFilterOptions = new NatFilterOptions();

                if (optionDebug) {
                    // add debug options
                    natFilterOptions.setOptions(App.class
                            .getResourceAsStream(CONFIG_RESOURCE_PATH + "debugoptions.properties"));
                }
                // add nat2 compatibility default option filter
                natFilterOptions
                        .setOptions(App.class
                                .getResourceAsStream(CONFIG_RESOURCE_PATH + "nat2.default.properties"));
                // add/replace by tested configuration
                natFilterOptions.setOptions(App.class
                        .getResourceAsStream(CONFIG_RESOURCE_PATH + configName));
                transcriptor = newTranscriptor(natFilterOptions, ge);
            } catch (NatFilterException | IOException ex) {
                ex.printStackTrace();
            }
            if (transcriptor
                    == null) {
                System.out.println("Transcriptor build error for conf : '" + configName + "'");
                for (String sourceName : sources) {
                    stats.setTestResult(configName, sourceName, Stats.Result.no_converter);
                }

                continue;
            }

            /**
             * For each source
             */
            for (String sourceName : sources) {
                System.out.print("   - " + fixedLengthString(sourceName, maxSourceLength));
                System.out.print(" ");

                // get reference transcription file
                String referenceName = sourceName + "." + configName + ".braille";
                InputStream referenceIs = null;
                try {
                    referenceIs = App.class.getResourceAsStream(REFERENCE_RESOURCE_PATH + referenceName);
                } catch (Exception ex) {
                }
                boolean referenceExists = ((referenceIs != null) && (referenceIs.available() > 0));
                if (!referenceExists) {
                    nbConfigTotalMissingReference++;
                    if (!optionTranscribeIfNoRef) {
                        System.out.println("reference file '" + referenceName + "' does not exist");
                        stats.setTestResult(configName, sourceName, Stats.Result.no_reference);
                        continue;
                    }
                } else {
                    if (inputBlackList.has(referenceName)) {
                        System.out.println("(blacklisted)");
                        stats.setTestResult(configName, sourceName, Stats.Result.blacklisted);
                        continue;
                    }
                    nbConfigTotalReferences++;
                }
                // get input document
                NatDocument source = null;
                try {
                    source = geNatDocumentFromResourceName(sourceDir + sourceName);
                } catch (NatDocumentException e) {
                }
                if (source == null) {
                    System.out.println("source document '" + sourceName + "' does not exist, passing");
                    stats.setTestResult(configName, sourceName, Stats.Result.no_source);
                    continue;
                }

                // transcribe
                NatDocument result = null;
                try {
                    result = transcriptor.run(source);
                } catch (NatFilterException e) {
                };
                if (result == null) {
                    System.out.println("transcription failed");
                    stats.setTestResult(configName, sourceName, Stats.Result.no_output);
                    nbConfigTotalAborted++;
                    continue;
                }
                if (referenceExists) {
                    // test equals
                    try {
                        if (IOUtils.contentEquals(referenceIs, result.getInputstream())) {
                            System.out.println("[ok]");
                            stats.setTestResult(configName, sourceName, Stats.Result.ok);
                            nbConfigTotalMatchingReferences++;
                            matchingList.add(referenceName);
                        } else {
                            System.out.println("[result is different]");
                        }
                        File outputFile = new File("resu/" + referenceName);
                        IOUtils.copy(result.getInputstream(), new FileOutputStream(outputFile));
                    } catch (NatDocumentException ex) {
                        Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    try {
                        File outputFile = new File("resu/" + referenceName);
                        IOUtils.copy(result.getInputstream(), new FileOutputStream(outputFile));
                        System.out.println("[created]");
                        stats.setTestResult(configName, sourceName, Stats.Result.created);
                    } catch (NatDocumentException ex) {
                        Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }

            // configuration summary
            System.out.println();

            System.out.println(
                    "Summary for config " + configName + " :");
            System.out.println(
                    "  match    : " + nbConfigTotalMatchingReferences + "/" + nbConfigTotalReferences);
            System.out.println(
                    "  aborted  : " + nbConfigTotalAborted);
            System.out.println(
                    "  missing references  : " + nbConfigTotalMissingReference);
            System.out.println();
            nbTotalReferences += nbConfigTotalReferences;
            nbTotalMatchingReferences += nbConfigTotalMatchingReferences;
            nbTotalAborted += nbConfigTotalAborted;
            nbTotalMissingReference += nbConfigTotalMissingReference;
        }
// general summary
        System.out.println("General summary ");
        System.out.println("  match    : " + nbTotalMatchingReferences + "/" + nbTotalReferences);
        System.out.println("  aborted  : " + nbTotalAborted);
        System.out.println("  missing references  : " + nbTotalMissingReference);
        if (optionWriteBlackList) {
            matchingList.write();
            if (!matchingList.isEmpty()) {
                System.err.println("=> rerun with -r to bypass matching files tests");
            }
        }
    }
}
