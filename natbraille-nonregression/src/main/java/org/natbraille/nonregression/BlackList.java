/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.natbraille.nonregression;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.commons.io.IOUtils;

/**
 * Transcription blacklist see {@link App}. An ArrayList<String> that can write
 * and read itselfs as a file named "blacklist.txt" in current directory.
 *
 * @author vivien
 */
public class BlackList extends ArrayList<String> {

    private static final String filename = "blacklist.txt";

    /**
     * write Blacklist to "blacklist.txt"
     * @return 
     */
    public BlackList read() {

        try {
            for (Object s : IOUtils.readLines(new FileInputStream(new File(filename)), "UTF-8")) {
                add((String) s);
            }
            System.err.println("=> found blacklist : " + filename + ", " + size() + " element(s)");
        } catch (Exception e) {
            System.err.println("=> no blacklist named " + filename);
        }
        return this;
    }

    /**
     * write Blacklist to "blacklist.txt"
     */
    public void write() {
        try {
            IOUtils.writeLines(this, "\n", new FileOutputStream(new File(filename)), "UTF-8");
            System.err.println("=> wrote blacklist : " + filename);
        } catch (IOException ioe) {
            System.err.println("=> could not write blacklist : " + filename);
        }

    }

    public boolean has(String what) {
        for (String thi : this) {
            if (thi.equals(what)) {
                return true;
            }
        }
        return false;
    }
}
