$tcs $svgan~,*<%?:$]\[ Une cons�quence de l'effet de serre $p
Terres en voie de disparition $p
 $p
Si la plan�te se r�chauffe, le niveau des mers montera. Doucement, mais s�rement. Et si �a d�borde pour de bon? Les �les, les deltas, les estuaires seront aux premi�res loges. Or ces r�gions sont souvent tr�s peupl�es. Que faire? $p
 $p
 $p
Question: au XXI�me (21�me) si�cle, quel sera le point commun entre un Hollandais, un Bangladais et un habitant des �les Marshall dans le Pacifique? $p
 $p
R�sultat. Les zozos qui ont r�pondu A ou B, ont un gage: porter un sombrero avec thermom�tre int�gr�! Les autres ont les f�licitations du jury. Car aux derni�res nouvelles scientifiques, il semble bien que la temp�rature moyenne de l'atmosph�re va augmenter dans les prochaines d�cennies (voir encadr� p. 9). Et une des cons�quences sur laquelle les savants de tout poil tombent d'accord, c'est bien une mont�e des eaux. $p
 $p
 $p
Du monde au bord de l'eau $p
 $p
Le hic, c'est que l'homme s'est install� dans les deltas et � l'embouchure des fleuves, en bord de mer et d'oc�an. Pour jouir du commerce fluvial et maritime, pour �chapper � la surpopulation des r�gions int�rieures (comme en Hollande ou au Bangladesh), pour la bronzette et le bain marin, ou par amour de l'azur bleut�. Alors si l'eau monte... Certes, direz-vous, tout le monde ne sera pas touch� (voir carte p. 8), et pour les intr�pides qui ont construit leur bicoque au ras des flots il n'y a pas le feu au lac. La mont�e des eaux sera lente (en moyenne de 2 � 4 mm chaque ann�e), ils auront le temps de r�agir. Pourtant -imaginons que l'eau soit plus haute de 1 m- ils risquent fort de se mouiller les pantoufles car l'eau d�bordera plus souvent et engendrera plus de d�g�ts. $p
 $p
Pour comprendre, prenons deux exemples. En 1953, le long des c�tes hollandaises, une terrible temp�te fit monter l'eau de 4 m au-dessus de son niveau normal. La mer du Nord brisa des digues, inonda 15'000 ha de polders (ces terres artificiellement gagn�es par l'homme sur la mer) et fit 1'800 morts. Un tel �v�nement est exceptionnel; la probabilit� qu'il se produise n'est que de 1 fois tous les 250 ans. Mais si le niveau marin �tait plus haut de 1 m (et en supposant que l'on n'ait pas augment� la taille des digues), il suffirait alors d'une �l�vation de 3 m pour retrouver les m�mes conditions. Or une "surcote" de 3 m est beaucoup plus probable: elle a lieu environ 1 fois tous les 50 ans! Deuxi�me exemple: en 1982, le cyclone Isaac inonda 23 km� de l'�le de Tongatapu (royaume du Tonga, dans le Pacifique) et toucha 33� de ses 67'000 habitants. Si la mer montait de 1 m, le m�me cyclone inonderait 37 km�, et 45� de la population se retrouverait les pieds dans l'eau. Face � la mont�e des eaux, mieux vaut donc pr�voir que voir venir. Mais que faire? Pour simplifier, disons qu'il existe trois solutions: se prot�ger, fuir ou s'adapter. $p
 $p
 $p
Am�nagements co�teux $p
 $p
La premi�re de ces solutions est celle choisie par des pays industrialis�s. Le long de leurs 353 km de c�tes, les Hollandais luttent depuis des si�cles contre la mer. Un combat vital puisque 27� du territoire se situe en dessous du niveau marin! Les dunes, sur 254 km, forment une barri�re naturelle. Sur ces 254 km, 96 km sont maintenus par des �pis qui retiennent le sable. Des digues d�fendent 34 km c�tiers, et des constructions diverses 27 km. Enfin, l'ensemble de ces c�tes et les 38 km de plages basses restantes sont �galement prot�g�es par des dunes artificielles. Le sable est pr�lev� 2 ou 3 km � l'int�rieur des terres et remplace celui emport� par les vagues. $p
 $p
Les pays riches se sont �galement lanc�s dans des constructions plus complexes et au co�t... tr�s sal�! En 1982, une barri�re a �t� �rig�e sur la Tamise pour prot�ger Londres de la mont�e des eaux provenant de la mer et provoqu�e par les temp�tes. Depuis sa construction, cette barri�re a �t� ferm�e plus d'une trentaine de fois, d�s que l'on pr�voyait que le niveau de l'eau arriverait � moins de 45 cm des structures de d�fense du centre londonien. Mais la Thames Barrier est une folie financi�re puisque sa construction a co�t� 1 milliard d'euros et que son co�t de maintenance et d'utilisation est de 6 millions d'euros par an. Cet investissement s'av�rera-t-il un jour judicieux? Oui, si une temp�te similaire � celle de 1966 qui ravagea le port de Hambourg se reproduisait. Pour la ville de Londres, les d�g�ts occasionn�s par une telle catastrophe -estim�s � 29 milliards d'euros- seraient alors �vit�s. $p
 $p
 $p
D'abord, moins polluer! $p
 $p
En 1997, � Kyoto, une conf�rence internationale a fix� une r�duction moyenne de 5,2� des �missions de gaz � effet de serre d'ici � 2010. Cette mesure ne s'applique qu'aux pays industrialis�s qui, �tats-unis en t�te, sont les principaux responsables puisqu'ils �mettent 90� de ces gaz. Apr�s la conf�rence de La Haye, ce mois-ci, l'Europe et le Japon suivront tr�s certainement leur engagement. Mais les �tats-unis et d'autres nations anglo-saxonnes tra�nent les pieds de peur de ralentir leur croissance �conomique. R�sultat: faute d'argent pour se prot�ger, les pays pauvres risquent de subir les cons�quences de la pollution des pays riches. En d'autres termes, ils pourraient �tre les dindons mouill�s d'une farce -pas vraiment dr�le- que leurs auront faite les pays riches. $p
 $p
Philippe Monges $p
 $p
Remerciements � Roland Paskoff, professeur �m�rite de l'universit� Louis-Lumi�re de Lyon; et Anne de la Vega-Leinert, de l'universit� du Middlesex. $p
 $p
Avis de fi�vre plan�taire $p
 $p
La terre est emmitoufl�e dans un manteau de gaz et c'est tr�s bien ainsi. Sans le gaz carbonique (CO2), la vapeur d'eau, le m�thane et quelques autres gaz naturellement pr�sents dans l'atmosph�re, le thermom�tre afficherait en moyenne -20� C! En effet, � la mani�re d'une serre de jardin, ces gaz laissent passer les rayons solaires et pi�gent en retour la chaleur due aux rayonnements infrarouges �mis par la Terre chauff�e. Gr�ce � cet "effet de serre", il r�gne sur notre plan�te une moyenne tr�s supportable de +15� C. $p
 $p
Mais l'homme a rompu l'�quilibre naturel. En br�lant charbon et p�trole, usines, voitures et habitations crachent toujours plus de CO2. �levages et rizi�res produisent de fortes quantit�s de m�thane, gaz produit en milieux priv�s d'oxyg�ne (intestins d'animaux, mar�cages naturels ou artificiels). Ces gaz, rejet�s chaque ann�e en plus grande quantit� dans l'atmosph�re, augmentent l'effet de serre naturel. Au point d'engendrer un r�chauffement plan�taire? Tr�s probablement, r�pondent les scientifiques, qui restent prudents. Car si la temp�rature moyenne a d�j� augment� de 0,5� C au cours du XX�me si�cle, il est difficile de prouver que cette augmentation est davantage due � l'impact des activit�s humaines qu'� un cycle climatique naturel. Qu'en serait-il d'ici � 2100? Si les �missions de gaz � effet de serre ne sont pas r�duites, le mercure pourrait grimper de +1� C � +3,5� C. Les cons�quences seraient alors d�sastreuses: mont�e du niveau des mers et des oc�ans mais �galement d�r�glement g�n�ral du climat, augmentation du nombre de temp�tes et d'ouragan, d�sertification accrue dans certaines r�gions, inondations dans d'autres� $p
$tce