package org.natbraille.mathinterativetool.Tests;

import org.natbraille.editor.server.unoconvertwrapper.UnoConvert;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class TestCasesOfQuelquesFormules {
    public static List<TestCase> findQuelquesFormulesTestSuiteTestCases() throws Exception {
        Path suitePath = Path.of("natbraille-math-interactive-tool", "tests", "quelques formules");
        String source = "quelques formules.md";
        List<TestCase> testCases = new ArrayList<>();
        String[] content = Files.readString(suitePath.resolve(source)).split("\n");
        String category = "none";
        for (String line : content){
            if (line.matches("\\s*")){
                // pass
            } else if ( line.matches("#\\s*\\d+[.].+")){
                category = line.substring(2);
                // System.out.println(category);
            } else if ( line.matches("\\(\\d+\\)\\s+.+")){
                int sep = line.indexOf(")");
                String index = line.substring(1,sep);
                String starMath = line.substring(sep+1).trim();
                String mathMLString = new String(UnoConvert.starmathToMathML(starMath));
                String mmlFilename = category+"."+index+".mml";
                Path mmlPath = suitePath.resolve(Path.of(mmlFilename));
                Files.writeString(mmlPath,mathMLString);

                testCases.add(new TestCase(suitePath,mmlPath,Path.of(""),starMath));
           //     System.out.println("["+category+"]"+"["+index+"]"+"["+starMath+"]");
             //   System.out.println( mathMLString );
  //              testCases.add(new TestCase(suitePath),)
            } else {
                System.out.println("ignore:"+line);
            }

        }





    return testCases;
    }
}