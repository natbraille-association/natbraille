package org.natbraille.mathinterativetool.Tests;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Objects;

public class TestCasesOfMathMLTestSuite {
    public static List<TestCase> findMathMLTestSuiteTestCases() throws IOException {
        Path suitePath = Path.of("natbraille-math-interactive-tool","tests", "mathml-test-suite", "testsuite", "testsuite");
        List<Path> testSuiteFilePaths = Files.find(suitePath, Integer.MAX_VALUE, (filePath, fileAttr) -> fileAttr.isRegularFile()).toList();
        return testSuiteFilePaths.stream().map(file -> {
            boolean endsWithMml = file.toString().endsWith(".mml");
            if (endsWithMml) {
                boolean isNotContentMathML = !suitePath.relativize(file).toString().startsWith("Content");
                if (isNotContentMathML) {
                    Path conjointImage = Path.of(file.toString().replaceAll("\\.mml$", ".png"));
                    boolean hasConjointImage = testSuiteFilePaths.contains(conjointImage);
                    if (hasConjointImage) {
                        return new TestCase(suitePath, file, conjointImage,"[no starmath]");
                    }
                }
            }
            return null;
        }).filter(Objects::nonNull).toList();
    }
}
