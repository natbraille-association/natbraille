package org.natbraille.mathinterativetool.Tests;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class TestCase {
    public final Path suitePath;
    public final Path mmlPath;
    public final Path imagePath;
    public final String starMath;
    private String brailleString;

    private String transcriptionError;

    public String getCleanBrailleString() {
        /**
         * --><xsl:param as="xs:integer" name="longueur" select="100"/>
         *    <xsl:param as="xs:boolean" name="mise_en_page" select="true()"/>
         *    <xsl:param as="xs:string" name="coupe" select="'ⴰ'"/>
         *    <xsl:param as="xs:string" name="coupeEsth" select="'ⴱ'"/>
         *    <xsl:param as="xs:string" name="debMath" select="'ⴲ'"/>
         *    <xsl:param as="xs:string" name="finMath" select="'ⴳ'"/>
         *    <xsl:param as="xs:string" name="espace" select="'ⴴ'"/>
         *    <xsl:param as="xs:string" name="espaceSecable" select="'ⴵ'"/>
         *    <xsl:param as="xs:string" name="debTable" select="'ⴶ'"/>
         *    <xsl:param as="xs:string" name="finTable" select="'ⴷ'"/>
         *    <xsl:param as="xs:string" name="sautAGenerer" select="'ⴸ'"/>
         *    <xsl:param as="xs:string" name="stopCoup" select="'ⴹ'"/>
         *    <xsl:param as="xs:boolean" name="abrege" select="false()"/>
         *    <xsl:param as="node()*" name="styles"
         *               select="document('/home/vivien/.nat-braille/user_styles.xml')/styles/*"/>
         *    <xsl:param as="xs:boolean" name="dbg" select="false()"/>
         *    <xsl:variable as="xs:string" name="apos">'</xsl:variable>
         *    <xsl:variable as="xs:string" name="quot">"</xsl:variable>
         *    <xsl:variable as="xs:string" name="carcoup"
         *                  select="concat($coupeEsth,$coupe,$debMath,$finMath,$debTable,$finTable)"/>
         *    <xsl:variable as="xs:string*" name="rajouts">
         *       <xsl:sequence select="('','','','','','','','','','','','')"/>
         *    </xsl:variable>
         * </xsl:stylesheet>
         */
        if (brailleString==null){
            return brailleString;
        }
        return brailleString
                .replaceAll("ⴲ","") // deb math
                .replaceAll("ⴳ","") // fin math
                .replaceAll("[ⴰⴱ]","") // cut position
                .replaceAll("[ⴶⴷ]","") // deb table fin table
                .replaceAll("ⴸ","\n") // saut à générer
                .replaceAll("[ⴵⴴ]"," ") // espace secalble / espace
                ;
    }
    public String getBrailleString() {
        return brailleString;
    }

    public void setBrailleString(String brailleString) {
        this.brailleString = brailleString;
    }

    public String getTranscriptionError() {
        return transcriptionError;
    }

    public void setTranscriptionError(String transcriptionError) {
        this.transcriptionError = transcriptionError;
    }

    public String getName() {
        return suitePath.relativize(mmlPath).toString().replaceAll("\\.mml$", "");
    }
    public String getStarMathString() {
        return starMath;
    }
    public String getMathMLString() throws IOException {
        String mmlString = Files.readString(mmlPath);

        // ensure namespace
        String mmlXMLNamespaceString = "xmlns=\"http://www.w3.org/1998/Math/MathML\"";
        if (!mmlString.contains(mmlXMLNamespaceString)) {
            mmlString = mmlString.replaceAll("<math", "<math " + mmlXMLNamespaceString);
        }

        // ensure no header
        String xmlHeader = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
        if (mmlString.startsWith(xmlHeader)) {
            mmlString = mmlString.replaceFirst(".*\n","");
        }
        return mmlString;
    }

    public TestCase(Path suitePath, Path mmlPath, Path imagePath, String starMath) {
        this.suitePath = suitePath;
        this.mmlPath = mmlPath;
        this.imagePath = imagePath;
        this.starMath = starMath;
    }

}