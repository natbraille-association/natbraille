package org.natbraille.mathinterativetool.Tests;

import org.apache.commons.io.FileUtils;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.editor.server.faketranscriptor.math.MathTranslator;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// find ./mathml-test-suite/ -name '*mml' -type f -print0 | xargs -0 grep -h '<math' | sort | uniq -c

public class DoTests {


    public static String buildResultDocument(List<TestCase> testCases) throws IOException {


        StringBuilder html = new StringBuilder();
        html.append("<html><body>");
        html.append("<style>table,th,td{border:1px solid;}</style>");
        html.append("<style>td{word-wrap: break-word;max-width:25vw}</style>");
        html.append("<style>td.error{background-color:red;}</style>");
        html.append("<style>.braille{font-size:20px;}</style>");
        html.append("<h1>Natbraille math transcription/détranscription tests</h1>");
        html.append("<table>");
        html.append("<thead>");
        html.append("<tr>");
        html.append("<td>path-name</td>");
        html.append("<td>to-braille</td>");
        html.append("<td>source image</td>");
        html.append("<td>source mathml</td>");
        html.append("<td>source starmath</td>");
        html.append("</tr>");
        html.append("</thead>");

        html.append("<tbody>");
        String startCase = "<div>";
        String endCase = "</div>";
        for (TestCase testCase : testCases) {

            html.append("<tr>");
            html.append("<td><p>" + testCase.getName() + "</p></td>");
            if (testCase.getTranscriptionError() == null) {
                html.append("<td><pre class=\"braille\">" + testCase.getCleanBrailleString() + "</pre></td>");
            } else {
                html.append("<td class=\"error\">" + testCase.getTranscriptionError() + "</td>");
            }
            if (testCase.imagePath.toString().isEmpty()) {
                html.append("<td>[no img]</td>");
            } else {
                String base64EncodedImage = Base64.getEncoder().encodeToString(FileUtils.readFileToByteArray(testCase.imagePath.toFile()));
                String imageDataSrc = "data:image/png;base64," + base64EncodedImage;
                html.append("<td><img src=\"" + imageDataSrc + "\"/></td>");
            }
            html.append("<td>" + testCase.getMathMLString() + "</td>");
            html.append("<td><pre>" + testCase.getStarMathString() + "<pre></td>");
            html.append("</tr>");
        }
        html.append("</tbody>");
        html.append("</table>");
        html.append("</body></html>");
        return html.toString();


    }

    private static void executeTestCases(MathTranslator.NatbrailleMathTranslator natbrailleMathTranslator, List<TestCase> testCases) {

        for (int i = 0; i < testCases.size(); i++) {

            TestCase testCase = testCases.get(i);
            try {

                Map<String, String> equations = new HashMap<>();
                String key = String.valueOf(i);

                equations.put(key, testCase.getMathMLString());

                Map<String, String> brailleEquationsMap = natbrailleMathTranslator.translate(equations);
                String brailleString = brailleEquationsMap.get(key);

                testCase.setBrailleString(brailleString);
                //System.out.println(testCase.mmlPath+" "+brailleString);
            } catch (Exception e) {
                testCase.setTranscriptionError(e.toString());
            }
        }
    }

    public static void main(String[] args) throws Exception {

        NatFilterOptions nfo = new NatFilterOptions();
        nfo.setOption(NatFilterOption.MODE_MATH_SPECIFIC_NOTATION, "true");
        nfo.setOption(NatFilterOption.MODE_MATH_ALWAYS_PREFIX, "false");
        MathTranslator.NatbrailleMathTranslator natbrailleMathTranslator = MathTranslator.NatbrailleMathTranslator.build(nfo);

        Path resultDir = Path.of("natbraille-math-interactive-tool", "tests");

        {
            List<TestCase> testCases = TestCasesOfQuelquesFormules.findQuelquesFormulesTestSuiteTestCases();
            executeTestCases(natbrailleMathTranslator, testCases);
            Files.writeString(resultDir.resolve("quelques formules.html"), buildResultDocument(testCases));
        }
        {
            List<TestCase> testCases = TestCasesOfMathMLTestSuite.findMathMLTestSuiteTestCases();
            executeTestCases(natbrailleMathTranslator, testCases);
            Files.writeString(resultDir.resolve("mathml-test-suite.html"), buildResultDocument(testCases));
        }

    }


}
