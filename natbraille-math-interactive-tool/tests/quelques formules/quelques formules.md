# quelques formules

tirées de http://turrier.fr/articles/libreoffice-maths/libre-office-ecrire-des-maths.php 

# 1. Dérivées

(1) f'(x)= {dy} over {dx}
(2) {partial f} over {partial x}( x,y)
(3) {partial f} over {partial x}

# 2. Développements

(1) a^{2}-b^{2}=( a+b)(a-b)
(2) (a+b)^{3}= a^{3}+3a^{2}b+3ab^{2}+b^{3}

# 3. Ensembles

(1) E=emptyset
(2) E=setN
(3) E=setZ
(4) E=setQ
(5) E=setR
(6) E=setN^{"*"}
(7) E=setZ^{"*"}
(8) E=setQ^{"*"}
(9) E=setR^{"*"}
(10) E=size 20{setN}
(11) A intersection B = C
(12) A union B = C

# 4. Fonctions

(1) ℝ et ↦ 
(2) ↦ 
(3) x_{1}, x_{2}
(4) ↦ f : x ↦ {1} over {2x+3}
(5) a={f(x_{2})-f(x_{1})} over {x_{2}-x_{1}}
(6) b={x_{2}f(x_{1})-x_{1}f(x_{2})} over {x_{2}-x_{1}}
(7) x_{milieu}={x_{A}+x_{B}} over {2}
(8) AB^{2}=(x_{A}-x_{B})^{2}+(y_{A}-y_{B})^{2}
(9) AB= sqrt{(x_{A}-x_{B})^{2}+(y_{A}-y_{B})^{2}}
(10) f(x)=sin(ωt+{π} over {2})
(11) y=ln(x)
(12) y=log(x)
(13) y=exp(x)
(14) y=func e^{x}
(15) y=func e^{(x+1)}
(16) (e^{u})'=u' e^{u}
(17) y=func e^{(x+1)over(x+2)}
(18) f: x⟶ {1} over {x}
(19) D=\]-infinity ;0\[ union \]+infinity;0\[
(20) D=\]-bold{infinity} ;0\[ union \]+bold{infinity};0\[
(21) D=\]-bold{infinity} ;size 10{0}\[ union \] +bold{infinity};size 10{0}\[

# 5. Intégrales

(1) int from 0 to x f(t) dt
(2) int_0^x f(t) dt
(3) int from Re f(x)dx
(4) int_0^{π over 2} sin^{3}t dt
(5) int from{x=0} to{1} int from{y=0} to{1} f(x,y)dxdy
(6) int_0^1 int_0^1 f(x,y)dxdy
(7) int_{x=0}^1 int_{y=0}^1 f(x,y)dxdy

# 6. Intervalles

(1) -10 leslant x leslant +10
(2) -10 <= x <= +10
(3) -10 < x < +10
(4) A=[-10,+10]
(5) A=\]-10,+10\[
(6) x in [-10,+10]
(7) x notin [-10,+10]
(8) size 15{setN}= lbrace 0;1;2;3;... rbrace
(9) E=bold lbrace nbold{e_{1},e_{2},...,e_{n}} rbrace
(10) E=bold \{ nbold{e_{1},e_{2},...,e_{n}} bold \}

# 7. Lettres grecques

(1) α β γ δ ε ζ η θ λ μ ν ξ π ρ σ τ φ χ ψ ω

# 8. Matrices

(1) left(matrix{a # b ## c # d}right)
(2) left(matrix{a#b#c ## d#e#f}right)
(3) left ( matrix{a#b#c ## d#e#f ## g#h#i } right)
(4) M=left({matrix{a_{11}#a_{12}#dotsaxis#a_{1n} ## a_{21}#a_{22}#dotsaxis#a_{2n} ##dotsvert#dotsvert#dotsdown#dotsvert ##a_{p1}# a_{p2}#dotsaxis#a_{pn}}}right)

# 9. Nombres complexes

(1) overline {z}=x-iy

# 10. Probabilités

(1) P(Ω)=1
(2) P(emptyset)=0
(3) P(A union B)
(4) c^{p}_{n}= {n!} over {p!(n-p)!}

# 11. Puissances

(1) a^{n} times a^{p}= a^{n+p}
(2) {a^{n}} over {a^{p}} = a^{n-p}
(3) (a^n)^{p} = a^{n.p}
(4) (a times b)^{n}= a^{n} times b^{n}
(5) {({a} over {b})}^{n} = {a^{n}} over {b^{n}}
(6) 2^{-10} times {({1} over {5})}^{-10}

# 12. Statistiques

(1) bar x= {n_{1}x_{1}+n_{2}x_{2}+...n_{p}x_{p}} over {n_{1}+n_{2}+...n_{p}}

# 13. Suites

(1) u_{n}=u_{0}+nr drarrow u_{0}+...+u_{n}=(n+1)(u_{0}+u_{n}) over {2}
(2) {1-q^{n+1}} over {1-q^{ phantom {n+1}}}
(3) u_{n}=u_{0}q^{n} drarrow u_{0}+...+u_{n}=u_{0} {1-q^{n+1}} over {1-q^{ phantom {n+1}}}
(4) lim from{n rightarrow size 10{infinity}} (1 over n)=0
(5) S_{N} = sum from {{i=0}} to {N-1} { u_{i}}=u_{0}+u_{1}+...+u_{N-1}
(6) sum from k = 1 to n a_k
(7) sum to infinity 2^{-n}

# 14. Systèmes d'équations

(1) Left lbrace binom{ax+by=p}{cx-dy=q}right none
(2) bold left lbrace nbold {binom{ax+by=p}{cx-dy=q}}right none
(3) left lbrace {binom {y={2}over{3}x-{1}over{3}}{y={3}over{2}x+{1}over{2}}} right none

# 15. Valeur absolue

(1) abs{x+y} <= abs{x}+abs{y}

# 16. Vecteurs

(1) widevec {AB} + widevec {BC} = widevec {AC}
(2) ldline u rdline = sqrt{ x^{2}+y^{2}}
(3) {ldline widevec {u} + widevec {v} rdline}^{2}
(4) widevec u. widevec v
(5) widevec u and widevec v 
