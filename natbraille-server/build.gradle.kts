/*
 * This file was generated by the Gradle 'init' task.
 *
 * This project uses @Incubating APIs which are subject to change.
 */

plugins {
    id("org.natbraille.java-conventions")
    id("com.github.johnrengelman.shadow") version  "7.1.2"
}

dependencies {
    implementation("com.sparkjava:spark-core:2.3")
    implementation("org.json:json:20170516")
    api(project(":natbraille-core"))
    implementation("net.oneandone.reflections8:reflections8:0.11.4")
}

description = "natbraille-server"

tasks.jar {
    manifest.attributes["Main-Class"] = "org.natbraille.server.ApiServer"
}