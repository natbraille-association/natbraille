/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * Author : Vivien Guillet <vivien.guillet@laposte.net>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.server;

import java.nio.file.Path;

import org.natbraille.core.filter.convertisseur.ConvertisseurXHTML;
import org.natbraille.core.filter.options.NatFilterOption;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;
import org.natbraille.core.gestionnaires.LogLevel;
import org.natbraille.server.configurations.ConfigurationsManager;
import org.natbraille.server.io.Printers;
import org.natbraille.server.model.BrailleTables;
import org.natbraille.server.model.Filters;
import org.natbraille.server.model.Options;
import spark.Spark;
import static spark.Spark.before;
import static spark.Spark.delete;
import static spark.Spark.post;
import static spark.Spark.options;
import static spark.Spark.put;
import static spark.Spark.get;

public class ApiServer {

    public final static Path EDITOR_CONFIG_PATH = Paths.get("editor-configs");

    public static void main(String[] args) {
        System.setProperty("javax.xml.parsers.DocumentBuilderFactory",
                "org.apache.xerces.jaxp.DocumentBuilderFactoryImpl");
        System.setProperty("javax.xml.xpath.XPathFactory:http://java.sun.com/jaxp/xpath/dom",
                "com.sun.org.apache.xpath.internal.jaxp.XPathFactoryImpl");

        Spark.ipAddress("localhost");
        Spark.port(4567);

        // CorsFilter.apply(); // Call this before mapping thy routes
        /*
         */
        options("/*",
                (request, response) -> {

                    String accessControlRequestHeaders = request
                            .headers("Access-Control-Request-Headers");
                    if (accessControlRequestHeaders != null) {
                        response.header("Access-Control-Allow-Headers",
                                accessControlRequestHeaders);
                    }

                    String accessControlRequestMethod = request
                            .headers("Access-Control-Request-Method");
                    if (accessControlRequestMethod != null) {
                        response.header("Access-Control-Allow-Methods",
                                accessControlRequestMethod);
                    }

                    return "OK";
                });

        before((request, response) -> response.header("Access-Control-Allow-Origin", "*"));
        Map<String, ApiTranscriptor> cache = new HashMap<>();
        post("/tdf/:filtername", (req, res)
                -> {
            String filtername = req.params(":filtername");

            ApiTranscriptor t;
            if (cache.containsKey(filtername)) {
                t = cache.get(filtername);
            } else {
                t = TranscriptorDescriptionFile
                        // .toApiTranscriptor(Paths.get(".", "tdf", req.params(":filtername")))
                        .toApiTranscriptor(Paths.get(".", "tdf", filtername));
                cache.put(filtername, t);
            }
            return t.run(new ApiDocument(req.body()));
            /*return TranscriptorDescriptionFile
                    // .toApiTranscriptor(Paths.get(".", "tdf", req.params(":filtername")))
                    .toApiTranscriptor(Paths.get(".", "tdf", filtername))
                    .run(new ApiDocument(req.body()));*/
        }
        );
        /*
         */
        ConfigurationsManager configurationsManager = new ConfigurationsManager(EDITOR_CONFIG_PATH);
        get("/configurations", (req, res)
                -> configurationsManager.configurationHandles()
        );
        post("/configurations", (req, res) -> {
            Path id = configurationsManager.addConfiguration(req.body());
            JSONObject o = new JSONObject();
            if (id != null) {
                o.put(ConfigurationsManager.ID_KEY, id.toString());
            }
            return o;
        });
        put("/configurations", (req, res) -> {
            Path id = configurationsManager.replaceConfiguration(req.body());
            JSONObject o = new JSONObject();
            if (id != null) {
                o.put(ConfigurationsManager.ID_KEY, id.toString());
            }
            return o;
        });
        delete("/configurations/:id", (req, res) -> {
            String id = req.params(":id");
            JSONObject o = new JSONObject();
            Path removedId = configurationsManager.removeConfiguration(Paths.get(id));
            if (removedId != null) {
                o.put(ConfigurationsManager.ID_KEY, removedId);
            }
            return o;
        });
        /*
         */

        get("/stylists", (req, res)
                -> configurationsManager.stylistsHandles()
        );

        get("/stylists/xml/:id", (req, res) -> {
            String id = req.params(":id");

            Path p = Paths.get(id);

            System.out.println("looking for id " + id);
            System.out.println("looking for it at path " + p);
            String xmls = configurationsManager.stylistAsXmlString(p);
            System.out.println("ZZZ========================----");
            System.out.println("===========================----");
            System.out.println("===========================----");
            System.out.println("===========================----");
            System.out.println(xmls);
            System.out.println("===========================----");
            System.out.println("===========================----");
            System.out.println("===========================----");
            System.out.println("===========================----");

            return xmls;
            /*String s = defaultStyles();
            return toXMLString(s);
             */

        });
        post("/stylists", (req, res) -> {
            Path id = configurationsManager.addStylist(req.body());
            JSONObject o = new JSONObject();
            if (id != null) {
                o.put(ConfigurationsManager.ID_KEY, id.toString());
            }
            return o;
        });
        put("/stylists", (req, res) -> {
            Path id = configurationsManager.replaceStylist(req.body());
            JSONObject o = new JSONObject();
            if (id != null) {
                o.put(ConfigurationsManager.ID_KEY, id.toString());
            }
            return o;
        });
        delete("/stylists/:id", (req, res) -> {
            String id = req.params(":id");
            JSONObject o = new JSONObject();
            Path removedId = configurationsManager.removeStylist(Paths.get(id));
            if (removedId != null) {
                o.put(ConfigurationsManager.ID_KEY, removedId);
            }
            return o;
        });

        /*
         */
        JSONArray optionsModel = new Options().json;
        get("/model/options", (req, res) -> optionsModel);

        JSONArray filterModel = new Filters().json;
        get("/model/filters", (req, res) -> filterModel);

        JSONArray brailleTables = new BrailleTables().json;
        get("/model/brailleTables", (req, res) -> brailleTables);

        /*
        *
         */
        get("/printers", (req, res) -> {
            return Printers.getPrinterList();
        });
        post("/printers", (req, res) -> {
            JSONObject out = new JSONObject();
            JSONObject o = new JSONObject(req.body());
            if (o.has("text") && o.has("printer")) {
                String text = o.getString("text");
                String printerName = o.getString("printer");
                //System.out.println(printerName+" + "+text);
                int result = Printers.print(printerName, text);
                if (result == Printers.NOERR) {
                    out.put("status", "ok");
                } else {
                    out.put("status", "ko");
                }
            }
            return out;
        });

        /*
         */
        Map<String, ApiTranscriptor> convertCache = new HashMap<>();
        post("/convert/:configurationId", (req, res)
                -> {
            String configurationId = req.params(":configurationId");
            Path p = configurationsManager.enforceDirectory(
                    configurationsManager.configurationsPath(),
                    Paths.get(configurationId)
            );
            ApiTranscriptor t;
            if (convertCache.containsKey(configurationId)) {
                t = convertCache.get(configurationId);
            } else {
                t = TranscriptorDescriptionFile
                        // .toApiTranscriptor(Paths.get(".", "tdf", req.params(":filtername")))
                        .toApiTranscriptor(p);
                convertCache.put(configurationId, t);
            }
            return t.run(new ApiDocument(req.body()));
            /*return TranscriptorDescriptionFile
                    // .toApiTranscriptor(Paths.get(".", "tdf", req.params(":filtername")))
                    .toApiTranscriptor(Paths.get(".", "tdf", filtername))
                    .run(new ApiDocument(req.body()));*/
        }
        );
        /*
         */
        get("/fulld", (req, res)
                -> new ApiTranscriptor(
                        new FilterList(
                                /*org.natbraille.core.filter.convertisseur.Convertisseur2ODT,
                        org.natbraille.core.filter.convertisseur.ConvertisseurExterne,
                        org.natbraille.core.filter.convertisseur.ConvertisseurOpenOffice,
                        org.natbraille.core.filter.convertisseur.ConvertisseurXML.class,
                        org.natbraille.core.filter.convertisseur.ConvertisseurTan,*/
                                org.natbraille.core.filter.convertisseur.ConvertisseurTexte.class,
                                org.natbraille.core.filter.transcodeur.TranscodeurNormal.class,
                                //                        org.natbraille.core.filter.convertisseur.ConvertisseurTexteMixte,
                                org.natbraille.core.filter.transcodeur.AmbiguityFilter.class,
                                org.natbraille.core.filter.presentateur.PresentateurMEP.class,
                                org.natbraille.core.filter.presentateur.BrailleTableModifierJava.class
                        ),
                        new OptionsList(),
                        LogLevel.DEBUG
                //.set(NatFilterOption.FORMAT_OUTPUT_BRAILLE_TABLE,"TbFr2007"))
                )
                        .run(new ApiDocument(req.body()))
        );
        post("/full", (req, res)
                -> new ApiTranscriptor(
                        new FilterList(
                                org.natbraille.core.filter.transcriptor.Transcriptor.class
                        ),
                        new OptionsList()
                                .set(NatFilterOption.FORMAT_OUTPUT_BRAILLE_TABLE, "TbFr2007"),
                        LogLevel.DEBUG
                )
                        .run(new ApiDocument(req.body()))
        );
        post("/xhtml2braille", (req, res)
                -> new ApiTranscriptor(
                        new FilterList(
                                ConvertisseurXHTML.class,
                                org.natbraille.core.filter.transcodeur.TranscodeurNormal.class,
                                //                        org.natbraille.core.filter.convertisseur.ConvertisseurTexteMixte,
                                org.natbraille.core.filter.transcodeur.AmbiguityFilter.class,
                                org.natbraille.core.filter.presentateur.PresentateurMEP.class,
                                org.natbraille.core.filter.presentateur.BrailleTableModifierJava.class
                        ),
                        new OptionsList()
                                .set(NatFilterOption.FORMAT_OUTPUT_BRAILLE_TABLE, "TbFr2007")
                                .set(NatFilterOption.MODE_G2, "true"),
                        LogLevel.NORMAL
                )
                        .run(new ApiDocument(req.body()))
        );
        post("/inv", (req, res)
                -> {
            System.err.println(req.body());

            return new ApiTranscriptor(
                    new FilterList(org.natbraille.core.filter.convertisseur.ConvertisseurTexte.class,
                            org.natbraille.core.filter.transcodeur.TranscodeurNormal.class),
                    new OptionsList()
                            .set(NatFilterOption.FORMAT_OUTPUT_BRAILLE_TABLE, "TbFr2007")
                            .set(NatFilterOption.MODE_DETRANS, "true"),
                    LogLevel.NORMAL
            )
                    .run(new ApiDocument(req.body()));
        }
        );
        post("/void",(req,res) -> {
           return new ApiTranscriptor(
              new FilterList(
                      ConvertisseurXHTML.class
              ),
              new OptionsList(),
              LogLevel.NORMAL
           ) ;
        });
    }
}
