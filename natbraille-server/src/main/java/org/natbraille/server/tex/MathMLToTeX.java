/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * Author : Vivien Guillet <vivien.guillet@laposte.net>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.natbraille.server.tex;

import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.natbraille.core.NatDynamicResolver;
import org.natbraille.core.document.NatDocument;
import org.natbraille.core.filter.NatTransformerFilter;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.gestionnaires.LogLevel;
import org.natbraille.core.gestionnaires.LogMessage;
import org.natbraille.core.gestionnaires.MessageContents;
import org.natbraille.core.gestionnaires.MessageKind;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author vivien
 */
public class MathMLToTeX extends NatTransformerFilter {

    public MathMLToTeX(NatDynamicResolver ndr) {
        super(ndr);
    }

    private static int saxParse() {
        SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
        try {
            // String path = "/home/vivien/src/braille/mathmltestsuite/testsuite/testsuite/Presentation/ScriptsAndLimits/mover/mover1.mml";            
            String path = "./test/test1.mml";
            // String path = "./test/test2.mml";
            String fileContent = new String(Files.readAllBytes(Paths.get(path)));

            SAXParser saxParser = saxParserFactory.newSAXParser();
            MathMLToTexHandler handler = new MathMLToTexHandler();

            saxParser.parse(new InputSource(new StringReader(fileContent)), handler);
            System.err.println("resu:" + handler.getResultDebug());

        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
        return 42;
    }

    @Override
    protected NatDocument process(NatDocument indoc) throws Exception {

        String mathML = indoc.getString();
        SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
        SAXParser saxParser = saxParserFactory.newSAXParser();
        MathMLToTexHandler handler = new MathMLToTexHandler();
        boolean debug = "true".equals(this.getOption(NatFilterOption.debug_xsl_processing));
        handler.setDebug(debug);

        saxParser.parse(new InputSource(new StringReader(mathML)), handler);
        if (debug) {
            handler.getTrace().stream().forEachOrdered( string -> {
                getGestionnaireErreur().afficheMessage(new LogMessage(
                        MessageKind.INFO,
                        MessageContents.Tr(string),
                        LogLevel.DEBUG)
                );
            });
        }
        NatDocument outdoc = new NatDocument();
        outdoc.setString(handler.getResult());
        outdoc.forceContentType("application/x-latex");
        return outdoc;
//        outdoc.setByteBuffer(baos.toByteArray());

        //System.err.println("resu:" + handler.getResult());
//        // in
//        Document doc = indoc.getDocument(getUriResolver().getXMLReader());
//
//        DOMSource in = new DOMSource(doc);
//        in.setSystemId(doc.getDocumentURI());
//
//        // out
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        BufferedWriter fcible = new BufferedWriter(new OutputStreamWriter(baos, "UTF8"));
//        StreamResult out = new StreamResult(fcible);
//
//        // transform
//        String xslUri = "iii";
//        Transformer tr = makeTransformer(new URI(xslUri));
//        tr.transform(in, out);
//
//        // out nat document
//        NatDocument outdoc = new NatDocument();
//        outdoc.setByteBuffer(baos.toByteArray());
//        return outdoc;
    }

    @Override

    protected String getPrettyName() {
        return "MathML2TeX";
    }

    public static void main(String[] argv) {

        System.err.println("qqqq");
        int z = saxParse();
        System.err.println("qqqq" + z);
    }
}
