/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * Author : Vivien Guillet <vivien.guillet@laposte.net>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.natbraille.server.tex;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

//
// \sum_{i=0}^{10} x^{2} -> ∑_{i=0}^{10}x^{2} => caractere replace
//
// \begin{array}{cc} a & b \\ c & d \end{array}
// ->
// \sum_{i=32}^{64} \sqrt{(32+h^{2})}^2
// beaucoup prennet des enfants multiples. Grouper le dernier param ?
//
public class MathMLToTexHandler extends DefaultHandler {

    private static class StackElement {

        private final String s;
        private final boolean isElement;
        private final Integer cursor;

        StackElement(String s, boolean isElement, Integer cursor) {
            this.s = isElement ? s.toLowerCase() : s;
            this.isElement = isElement;
            this.cursor = cursor;
        }

        StackElement(String s) {
            this.s = s;
            this.isElement = false;
            this.cursor = null;
        }

        public StackElement unmarked() {
            int markerIndex = s.indexOf("\0");
            if (markerIndex == -1) {
                return new StackElement(s, this.isElement, null);
            } else {
                return new StackElement(s.substring(0, markerIndex).concat(s.substring(markerIndex + 1)), this.isElement, markerIndex);
            }
        }

        public StackElement marked() {
            if (cursor == null) {
                return this;
            } else {
                return new StackElement(s.substring(0, cursor).concat("\0").concat(s.substring(cursor)), isElement, null);
            }
        }
    }

    private interface StackTransformer {

        public List<StackElement> transformChild(List<StackElement> stackElements);
    }
    //List to hold Employees object
    // private List<Employee> empList = null;
    // private Employee emp = null;
    //getter method for employee list
    /* public List<Employee> getEmpList() {
        return empList;
    }
     */
    private List<StackElement> stack = new ArrayList();
    private List<String> trace = new ArrayList();
    private boolean debug = false;

    public void setDebug(boolean debug) {
        this.debug = debug;
    }

    @Override
    public void startDocument() {
        System.out.println("GO!");
    }

    // https://developer.mozilla.org/en-US/docs/Web/MathML/Element
/*
            <math> (Top-level element)

ASection
    <maction> (Binded actions to sub-expressions)
    <maligngroup> (Alignment group)
    <malignmark> (Alignment points)
ESection
    <menclose> (Enclosed contents)
    <merror> (Enclosed syntax error messages)
FSection
    <mfenced> (Parentheses)
    <mfrac> (Fraction)
GSection
    <mglyph> (Displaying non-standard symbols)
ISection
    <mi> (Identifier)
LSection
    <mlabeledtr> (Labeled row in a table or a matrix)
    <mlongdiv> (Long division notation)
MSection
    <mmultiscripts> (Prescripts and tensor indices)
NSection
    <mn> (Number)
OSection
    <mo> (Operator)
    <mover> (Overscript)
PSection
    <mpadded> (Space around content)
    <mphantom> (Invisible content with reserved space)
RSection
    <mroot> (Radical with specified index)
    <mrow> (Grouped sub-expressions)
SSection
    <ms> (String literal)
    <mscarries> (Annotations such as carries)
    <mscarry> (Single carry, child element of <mscarries>)
    <msgroup> (Grouped rows of <mstack> and <mlongdiv> elements)
    <msline> (Horizontal lines inside <mstack> elements)
    <mspace> (Space)
    <msqrt> (Square root without an index)
    <msrow> (Rows in <mstack> elements)
    <mstack> (Stacked alignment)
    <mstyle> (Style change)
    <msub> (Subscript)
    <msup> (Superscript)
    <msubsup> (Subscript-superscript pair)
TSection
    <mtable> (Table or matrix)
    <mtd> (Cell in a table or a matrix)
    <mtext> (Text)
    <mtr> (Row in a table or a matrix)
USection
    <munder> (Underscript)
    <munderover> (Underscript-overscript pair)
Other elementsSection
    <semantics> (Container for semantic annotations)
    <annotation> (Data annotations)
    <annotation-xml> (XML annotations)

     */
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes)
            throws SAXException {

        String cursor = attributes.getValue("cursor");
        if (cursor != null) {
            stack.add(new StackElement(qName, true, 0));
        } else {
            stack.add(new StackElement(qName, true, null));
        }

        //boolean addit = true;
/*
        if (qName.equalsIgnoreCase("math")) {
            addit = false;
            // System.err.println(" (Top-level element)");
            stack.add("$");
        } else if (qName.equalsIgnoreCase("maction")) {
            System.err.println(" (Binded actions to sub-expressions)");
        } else if (qName.equalsIgnoreCase("maligngroup")) {
            System.err.println(" (Alignment group)");
        } else if (qName.equalsIgnoreCase("malignmark")) {
            System.err.println(" (Alignment points)");
        } else if (qName.equalsIgnoreCase("menclose")) {
            System.err.println(" (Enclosed contents)");
        } else if (qName.equalsIgnoreCase("merror")) {
            System.err.println("Enclosed syntax error messages");
        } else if (qName.equalsIgnoreCase("mfenced")) {
            System.err.println(" (Parentheses)");
        } else if (qName.equalsIgnoreCase("mfrac")) {
            stack.add("\frac");
            System.err.println(" (Fraction)");
        } else if (qName.equalsIgnoreCase("mglyph")) {
            System.err.println("(Displaying non-standard symbols)");
        } else if (qName.equalsIgnoreCase("mi")) {
            System.err.println(" (Identifier)");
        } else if (qName.equalsIgnoreCase("mlabeledtr")) {
            System.err.println(" (Labeled row in a table or a matrix)");
        } else if (qName.equalsIgnoreCase("mlongdiv")) {
            System.err.println(" (Long division notation)");
        } else if (qName.equalsIgnoreCase("mmultiscripts")) {
            System.err.println(" (Prescripts and tensor indices)");
        } else if (qName.equalsIgnoreCase("mn")) {
            System.err.println(" (Number)");
        } else if (qName.equalsIgnoreCase("mo")) {
            addit = false;
            System.err.println(" (Operator)");
        } else if (qName.equalsIgnoreCase("mover")) {
            System.err.println(" (Overscript)");
        } else if (qName.equalsIgnoreCase("mpadded")) {
            System.err.println(" (Space around content)");
        } else if (qName.equalsIgnoreCase("mphantom")) {
            System.err.println(" (Invisible content with reserved space)");
        } else if (qName.equalsIgnoreCase("mroot")) {
            System.err.println("(Radical with specified index)");
        } else if (qName.equalsIgnoreCase("mrow")) {
            System.err.println(" (Grouped sub-expressions)");
        } else if (qName.equalsIgnoreCase("ms")) {
            System.err.println(" (String literal)");
        } else if (qName.equalsIgnoreCase("mscarries")) {
            System.err.println(" (Annotations such as carries)");
        } else if (qName.equalsIgnoreCase("mscarry")) {
            System.err.println(" (Single carry, child element of <mscarries>)");
        } else if (qName.equalsIgnoreCase("msgroup")) {
            System.err.println(" (Grouped rows of <mstack> and <mlongdiv> elements)");
        } else if (qName.equalsIgnoreCase("msline")) {
            System.err.println("(Horizontal lines inside <mstack> elements)");
        } else if (qName.equalsIgnoreCase("mspace")) {
            System.err.println("(Space)");
        } else if (qName.equalsIgnoreCase("msqrt")) {
            System.err.println("(Square root without an index)");
        } else if (qName.equalsIgnoreCase("msrow")) {
            System.err.println("(Rows in <mstack> elements)");
        } else if (qName.equalsIgnoreCase("mstack")) {
            System.err.println("(Stacked alignment)");
        } else if (qName.equalsIgnoreCase("mstyle")) {
            System.err.println(" (Style change)");
        } else if (qName.equalsIgnoreCase("msub")) {
            System.err.println(" (Subscript)");
        } else if (qName.equalsIgnoreCase("msup")) {
            System.err.println(" (Superscript)");
        } else if (qName.equalsIgnoreCase("msubsup")) {
            System.err.println(" (Subscript-superscript pair)");
        } else if (qName.equalsIgnoreCase("mtable")) {
            System.err.println(" (Table or matrix)");
        } else if (qName.equalsIgnoreCase("mtd")) {
            System.err.println(" (Cell in a table or a matrix)");
        } else if (qName.equalsIgnoreCase("mtext")) {
            System.err.println(" (Text)");
        } else if (qName.equalsIgnoreCase("mtr")) {
            System.err.println(" (Row in a table or a matrix)");
        } else if (qName.equalsIgnoreCase("munder")) {
            System.err.println(" (Underscript)");
        } else if (qName.equalsIgnoreCase("munderover")) {
            System.err.println(" (Underscript-overscript pair)");
        } else if (qName.equalsIgnoreCase("semantics")) {
            System.err.println(" (Container for semantic annotations)");
        } else if (qName.equalsIgnoreCase("annotation")) {
            System.err.println(" (Data annotations)");
        } else if (qName.equalsIgnoreCase("annotation-xml")) {
            System.err.println(" (XML annotations)");
        }
        if (addit) {
            stack.add("{");
        }
         */
    }

    StackElement popStack() {
        return stack.remove(stack.size() - 1);
    }

    StackElement peekStack() {
        return peekStack(0);
    }

    StackElement peekStack(int depth) {
        return stack.get(stack.size() - (depth + 1));
    }

    int findStack(String name) {
        for (int i = stack.size() - 1; i >= 0; i--) {
            StackElement candidate = stack.get(i);
            if (candidate.isElement && name.equals(candidate.s)) {
                return i;
            }
        }
        return -1;
    }

    void pushStack(StackElement e) {
        stack.add(e);
    }

    public final static String insertMarker(String s, Integer p) {
        return (p == null) ? s : (s.substring(0, p) + "\0" + s.substring(p));
    }

    public static void assertArguments(int nArguments, int nParameters, String qName) {
        if (nArguments != nParameters) {
            System.err.println(String.format("%s has %s parameters, but received %s arguments",
                    qName,
                    nParameters,
                    nArguments));
        }
    }

    public void mkPattern(String qName, String format, int nParameters, boolean joinArgs) {

        // find opening element in stack
        int index = findStack(qName);
        int nArguments = stack.size() - index - 1;

        // check arguments/parameters number
        if (!joinArgs) {
            assertArguments(nArguments, nParameters, qName);
        }
        // get arguments
        List<StackElement> tail = stack.subList(index + 1, stack.size());

        String[] tTail = tail.stream()
                .map(s -> s.marked())
                .map(s -> s.s)
                // TODO By param
                .map(s -> (s.startsWith("{") && s.endsWith("}")) ? s.substring(1, s.length() - 1) : s)
                .toArray(String[]::new);

        String s;
        if (joinArgs) {
            // gather all params to $1
            String param = String.join("", tTail);
            s = String.format(format, param);
        } else {
            // dispatch params
            s = String.format(format, (Object[]) tTail);
        }
        //
        stack.set(index, new StackElement(s, false, null).unmarked());
        // remove other elements
        for (int i = 0; i < nArguments; i++) {
            stack.remove(index + 1);
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {

        System.out.println("stackBefore:" + getResultDebug());
        if (debug) {
            trace.add("< " + getResultDebug());
        }

        if ("mn".equals(qName)) {
            StackElement terminal = popStack();
            popStack();
            pushStack(terminal);
        } else if ("mi".equals(qName)) {
            StackElement terminal = popStack();
            popStack();
            pushStack(terminal);
        } else if ("mo".equals(qName)) {
            StackElement terminal = popStack();
            popStack();
            pushStack(terminal);
        } else if ("mrow".equals(qName)) {
            mkPattern(qName, "{%1$s}", 1, true);
        } else if ("math".equals(qName)) {
            mkPattern(qName, "%1s", 1, true);
        } else if ("mfrac".equals(qName)) {
            mkPattern(qName, "\\frac{%1$s}{%2$s}", 2, false);
        } else if ("msup".equals(qName)) {
            mkPattern(qName, "%1$s^{%2$s}", 2, false);
        } else if ("msub".equals(qName)) {
            mkPattern(qName, "%1$s_{%2$s}", 2, false);
        } else if ("munder".equals(qName)) {
            mkPattern(qName, "%1$s_{%2$s}", 2, false);
        } /*else if ("mover".equals(qName)) {
            mkPattern(qName, "%1$s_{%2$s}", 2, false);
        } */ else if ("msubsup".equals(qName)) {
            mkPattern(qName, "%1$s_{%2$s}^{%3$s}", 3, false);
        } else if ("munderover".equals(qName)) {
            mkPattern(qName, "%1$s_{%2$s}^{%3$s}", 3, false);
        } else if ("mfenced".equals(qName)) {
            mkPattern(qName, "\\left(%1s\\right)", 1, true);
        } else if ("mroot".equals(qName)) {
            mkPattern(qName, "\\sqrt[%2$s]{%1$s}", 2, false);
        } else if ("msqrt".equals(qName)) {
            mkPattern(qName, "\\sqrt{%1$s}", 1, true);
        } else if ("mtable".equals(qName)) {
            mkPattern(qName, "\\matrix{%1s}", 1, true);
        } else if ("mtr".equals(qName)) {
            mkPattern(qName, "%1s\\cr ", 1, true);
        } else if ("mtd".equals(qName)) {
            mkPattern(qName, "%1s& ", 1, true);
        } /*
        <mtable>				\matrix{%BLOCKS%}
<mtr>					%BLOCKS%\cr
<mtd>					%BLOCK1%&
         */ else {
            System.err.println("tag not handled" + qName);
            trace.add("unhandled tag: " + qName);
        }
        /*
** Tags:
<munder>				%BLOCK1%_{%BLOCK2%}
<mtable>				\matrix{%BLOCKS%}
<mtr>					%BLOCKS%\cr
<mtd>					%BLOCK1%&
         */
        System.out.println("stackAfter:" + getResultDebug() + " ..... ");
        if (debug) {
            trace.add("> " + getResultDebug());
        }

        /*
if (qName.equalsIgnoreCase("math")) {
            // System.err.println(" (Top-level element)");
            addit = false;
            stack.add("$");
        } else if (qName.equalsIgnoreCase("maction")) {
            System.err.println(" (Binded actions to sub-expressions)");
        } else if (qName.equalsIgnoreCase("maligngroup")) {
            System.err.println(" (Alignment group)");
        } else if (qName.equalsIgnoreCase("malignmark")) {
            System.err.println(" (Alignment points)");
        } else if (qName.equalsIgnoreCase("menclose")) {
            System.err.println(" (Enclosed contents)");
        } else if (qName.equalsIgnoreCase("merror")) {
            System.err.println("Enclosed syntax error messages");
        } else if (qName.equalsIgnoreCase("mfenced")) {
            System.err.println(" (Parentheses)");
        } else if (qName.equalsIgnoreCase("mfrac")) {
            System.err.println(" (Fraction)");
        } else if (qName.equalsIgnoreCase("mglyph")) {
            System.err.println("(Displaying non-standard symbols)");
        } else if (qName.equalsIgnoreCase("mi")) {
            System.err.println(" (Identifier)");
        } else if (qName.equalsIgnoreCase("mlabeledtr")) {
            System.err.println(" (Labeled row in a table or a matrix)");
        } else if (qName.equalsIgnoreCase("mlongdiv")) {
            System.err.println(" (Long division notation)");
        } else if (qName.equalsIgnoreCase("mmultiscripts")) {
            System.err.println(" (Prescripts and tensor indices)");
        } else if (qName.equalsIgnoreCase("mn")) {
            System.err.println(" (Number)");
        } else if (qName.equalsIgnoreCase("mo")) {
            addit = false;
            System.err.println(" (Operator)");
        } else if (qName.equalsIgnoreCase("mover")) {
            System.err.println(" (Overscript)");
        } else if (qName.equalsIgnoreCase("mpadded")) {
            System.err.println(" (Space around content)");
        } else if (qName.equalsIgnoreCase("mphantom")) {
            System.err.println(" (Invisible content with reserved space)");
        } else if (qName.equalsIgnoreCase("mroot")) {
            System.err.println("(Radical with specified index)");
        } else if (qName.equalsIgnoreCase("mrow")) {
            System.err.println(" (Grouped sub-expressions)");
        } else if (qName.equalsIgnoreCase("ms")) {
            System.err.println(" (String literal)");
        } else if (qName.equalsIgnoreCase("mscarries")) {
            System.err.println(" (Annotations such as carries)");
        } else if (qName.equalsIgnoreCase("mscarry")) {
            System.err.println(" (Single carry, child element of <mscarries>)");
        } else if (qName.equalsIgnoreCase("msgroup")) {
            System.err.println(" (Grouped rows of <mstack> and <mlongdiv> elements)");
        } else if (qName.equalsIgnoreCase("msline")) {
            System.err.println("(Horizontal lines inside <mstack> elements)");
        } else if (qName.equalsIgnoreCase("mspace")) {
            System.err.println("(Space)");
        } else if (qName.equalsIgnoreCase("msqrt")) {
            System.err.println("(Square root without an index)");
        } else if (qName.equalsIgnoreCase("msrow")) {
            System.err.println("(Rows in <mstack> elements)");
        } else if (qName.equalsIgnoreCase("mstack")) {
            System.err.println("(Stacked alignment)");
        } else if (qName.equalsIgnoreCase("mstyle")) {
            System.err.println(" (Style change)");
        } else if (qName.equalsIgnoreCase("msub")) {
            System.err.println(" (Subscript)");
        } else if (qName.equalsIgnoreCase("msup")) {
            System.err.println(" (Superscript)");
        } else if (qName.equalsIgnoreCase("msubsup")) {
            System.err.println(" (Subscript-superscript pair)");
        } else if (qName.equalsIgnoreCase("mtable")) {
            System.err.println(" (Table or matrix)");
        } else if (qName.equalsIgnoreCase("mtd")) {
            System.err.println(" (Cell in a table or a matrix)");
        } else if (qName.equalsIgnoreCase("mtext")) {
            System.err.println(" (Text)");
        } else if (qName.equalsIgnoreCase("mtr")) {
            System.err.println(" (Row in a table or a matrix)");
        } else if (qName.equalsIgnoreCase("munder")) {
            System.err.println(" (Underscript)");
        } else if (qName.equalsIgnoreCase("munderover")) {
            System.err.println(" (Underscript-overscript pair)");
        } else if (qName.equalsIgnoreCase("semantics")) {
            System.err.println(" (Container for semantic annotations)");
        } else if (qName.equalsIgnoreCase("annotation")) {
            System.err.println(" (Data annotations)");
        } else if (qName.equalsIgnoreCase("annotation-xml")) {
            System.err.println(" (XML annotations)");

        }
        if (addit){
            stack.add("}");
        }
         */
    }

    private static String stripSpaces(String s) {
        Pattern stripSpacesPattern = Pattern.compile("(^(\\s)*|(\\s)*$)", Pattern.DOTALL);
        return stripSpacesPattern.matcher(s).replaceAll("");
    }

    private static boolean isTerminal(StackElement e) {
        return ((e.isElement) && ("mo".equals(e.s) || "mi".equals(e.s) || "mn".equals(e.s)));
    }

    @Override
    public void characters(char ch[], int start, int length) throws SAXException {
        //  System.out.print(Arrays.toString(ch)+" "+start+"("+length")");

        if (length == 0) {
            stack.add(new StackElement("", false, null));
        } else {
            String g = stripSpaces(new String(ch, start, length));

            StackElement head = stack.get(stack.size() - 1);
            if ((g.length() != 0) || isTerminal(head)) {
                if ("∑".equals(g) || "Σ".equals(g)) {
                    g = "\\sum";
                }
                stack.add(new StackElement(g, false, null));
            }
        }
    }

    String getResultDebug() {
        String s = "";
        for (StackElement e : stack) {
            if (e.isElement) {
                s += " | " + e.s;
            } else {
                s += " | '" + e.s + "'";
            }
        }
        return s;
    }

    String getResult() {
        String s = "";
        for (StackElement e : stack) {
            s += e.s;
        }
        return s;
    }

    List<String> getTrace() {
        return trace;
    }

    public static void main(String[] argv) {

        // System.err.println("'"+stripSpaces("\n\r\ta\n\t ")+"'");
        MathMLToTeX.main(argv);
        /*        
        StackElement e = new StackElement("klllllllllllllll", true, null);
        System.err.println("e"+e.s);
        StackElement e2 = new StackElement("kllllllllla\0bllll", true, null);
        System.err.println("e"+e2.s);
        StackElement e3 = e2.unmarked();
        System.err.println("e"+e3.s);
         */
    }
}
