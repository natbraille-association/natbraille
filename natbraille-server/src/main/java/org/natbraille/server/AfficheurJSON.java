/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * Author : Vivien Guillet <vivien.guillet@laposte.net>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.natbraille.server;
import org.json.JSONArray ;
import org.json.JSONObject ;
import org.natbraille.core.gestionnaires.LogMessage;
import org.natbraille.core.gestionnaires.Afficheur;
import org.natbraille.core.gestionnaires.MessageKind;

//   ~/src/natbraille/trunk/natbraille-core/src/main/java/org/natbraille/core/gestionnaires/Afficheur.java
public class AfficheurJSON extends Afficheur {
    public JSONArray messages;
    @Override
    public void afficheMessage(LogMessage logMessage) {
	JSONObject message = new JSONObject();
	message.put("kind",logMessage.getKind().toString());
	message.put("contents",logMessage.getTrContents(getI18n()));
	message.put("level",logMessage.getLevel());
	message.put("date",logMessage.getDate());
	messages.put(message);
    }
    public AfficheurJSON(int logLevelP){
	messages = new JSONArray();	
        setLogLevel( logLevelP );
	//setLogLevel(logLevel);	
	// setLogLevel(LogLevel.NONE);
	/*setWhiteList(MessageKind.SUCCESS,
		     MessageKind.STEP,
		     MessageKind.WARNING,
		     MessageKind.INFO,
		     MessageKind.FINAL_SUCCESS,
		     MessageKind.ERROR,
		     MessageKind.UNRECOVERABLE_ERROR);*/
        setWhiteList(MessageKind.SUCCESS,
		     MessageKind.STEP,		     
		     //MessageKind.INFO,
		     MessageKind.FINAL_SUCCESS,
		     MessageKind.UNRECOVERABLE_ERROR);

    }
    // 
}
