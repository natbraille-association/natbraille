/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * Author : Vivien Guillet <vivien.guillet@laposte.net>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


package org.natbraille.server;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.filter.options.NatFilterOptions;
import java.util.ArrayList;
import org.natbraille.core.filter.NatFilterException;

public class OptionsList extends ArrayList<OptionPair> {

    public OptionsList(OptionPair ...pairs){
	for( OptionPair p : pairs){
	    this.add(p);
	}
    }
    public OptionsList set(NatFilterOption option,String value){
	add( new OptionPair(option,value) );
	return this;

    }
    public OptionsList set(String option, String value) throws NatFilterException {
	add( new OptionPair(option,value) );
	return this;
    }
    public NatFilterOptions asNatFilterOptions() throws NatFilterException { 
	NatFilterOptions options = new NatFilterOptions();
	for (OptionPair p : this){
	    options.setOption(p.option, p.value);
	}
	return options;
    }
}
    
