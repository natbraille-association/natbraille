/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.natbraille.server.io;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.print.*;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;

import org.json.JSONArray;

/**
 *
 * @author vivien
 */
public class Printers {

    public static final int ERR_NO_SUCH_PRINTER = 1;
    public static final int ERR_PRINTING_ERROR = 2;
    public static final int NOERR = 0;

    public static JSONArray getPrinterList() {
        JSONArray ar = new JSONArray();
        PrintService[] printServices = PrintServiceLookup.lookupPrintServices(null, null);
        for (int i = 0; i < printServices.length; i++) {
            ar.put(printServices[i].getName());
        }
        return ar;
    }

    public static PrintService getPrinterByName(String printerName) {
        PrintService[] printServices = PrintServiceLookup.lookupPrintServices(null, null);
        for (int i = 0; i < printServices.length; i++) {
            if (printServices[i].getName().equals(printerName)) {
                return printServices[i];
            }
        }
        return null;
    }

    public static int print(String printer, String string) {

        PrintService printService = getPrinterByName(printer);
        if (printService == null) {
            return ERR_NO_SUCH_PRINTER;
        } else {
            DocPrintJob pj = printService.createPrintJob();
            DocFlavor flavor = DocFlavor.STRING.TEXT_PLAIN;

            //          PrintRequestAttributeSet aset = new HashPrintRequestAttributeSet();
            Doc doc = new SimpleDoc(string, flavor, null);
            PrintRequestAttributeSet pras = new HashPrintRequestAttributeSet();

            try {
                pj.print(doc, pras);
            } catch (PrintException ex) {
                Logger.getLogger(Printers.class.getName()).log(Level.SEVERE, null, ex);
                return ERR_PRINTING_ERROR;
            }
        }
        return NOERR;
    }

    public static void main(String[] args) {
        System.out.println(getPrinterList().toString());
        PrintService[] printServices = PrintServiceLookup.lookupPrintServices(null, null);
        for (int i = 0; i < printServices.length; i++) {
            if (i == 0) {
                String printerName = printServices[i].getName();
                print(printerName, "bonjour");
            }
        }
    }
}
