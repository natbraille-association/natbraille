/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * Author : Vivien Guillet <vivien.guillet@laposte.net>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.server;

import org.natbraille.core.document.NatDocument;
import org.json.JSONObject;
import org.natbraille.core.document.NatDocumentException;
import org.natbraille.core.document.NatDocumentErrorCode;
import org.natbraille.brailletable.BrailleTables;
import org.natbraille.brailletable.BrailleTable;
import java.nio.charset.Charset;
import org.json.JSONException;
import org.natbraille.brailletable.BrailleTableException;

public class ApiDocument {

    private JSONObject jsonDocument;
    private NatDocument natDocument;
    private String jsonStringDocument;

    public ApiDocument(JSONObject document) {
        this.jsonDocument = document;
    }

    public ApiDocument(NatDocument document) {
        this.natDocument = document;
    }

    public ApiDocument(String document) {
        System.err.println("I got doc " + document);
        this.jsonStringDocument = document;
    }

    public JSONObject toJSON() throws NatDocumentException {
        if (this.jsonDocument != null) {
            return this.jsonDocument;
        } else if (this.jsonStringDocument != null) {
            return new JSONObject(this.jsonStringDocument);
        } else {
            return ApiDocument.fromNatDocument(this.natDocument);
        }
    }

    public NatDocument toNatDocument() throws NatDocumentException {

        if (this.natDocument != null) {
            return this.natDocument;
        } else if (this.jsonDocument != null) {
            return ApiDocument.toNatDocument(this.jsonDocument);
        } else {
            return ApiDocument.toNatDocument(new JSONObject(this.jsonStringDocument));
        }
    }

    public static JSONObject fromNatDocument(NatDocument src) throws NatDocumentException {
        try {
            JSONObject dst = new JSONObject();
            dst.put("string", src.getString());
            dst.put("charset", src.getOrGuessCharset());
            dst.put("contentType", src.getOrGuessContentType());
            BrailleTable srcBrailleTable = src.getBrailletable();
            dst.put("brailleTable", (srcBrailleTable == null)
                    ? "" : srcBrailleTable.getName());

            dst.put("filename", src.getOriginalFileName());
            return dst;
        } catch (Exception e) {
            System.err.println("#221" + e);
            throw new NatDocumentException(NatDocumentErrorCode.DOCUMENT_CANNOT_GET_DATA, "", e);
        }
    }

    public static NatDocument toNatDocument(JSONObject src) throws NatDocumentException {
        try {
            NatDocument dst = new NatDocument();
            if (src.has("string")) {
                String v = src.get("string").toString();
                dst.setString(v);
            } else if (src.has("base64")) {
                String v = src.get("base64").toString();
                byte[] b = java.util.Base64.getDecoder().decode(v);
                dst.setByteBuffer(b);
            }
            {
                if (src.has("charset")) {
                    String v = src.get("charset").toString();
                    dst.forceCharset(Charset.forName(v));
                }
            }
            {
                if (src.has("contentType")) {
                    String v = src.get("contentType").toString();
                    dst.forceContentType(v);
                }
            }
            {
                if (src.has("brailleTable")) {
                    String v = src.get("brailleTable").toString();
                    dst.setBrailletable(BrailleTables.forName(v));
                }
            }
            {
                if (src.has("filename")) {
                    String v = src.get("filename").toString();
                    dst.setOriginalFileName(v);
                } else {
                    dst.setOriginalFileName("?");
                }
            }
            return dst;

        } catch (JSONException | BrailleTableException | NatDocumentException e) {
            System.err.println("#220" + e);
            throw new NatDocumentException(NatDocumentErrorCode.DOCUMENT_CANNOT_GET_DATA, "", e);
        }
    }

}
