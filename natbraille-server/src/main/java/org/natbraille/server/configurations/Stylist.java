/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.natbraille.server.configurations;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author vivien
 */
public class Stylist {

    /*
    <?xml version="1.1" encoding="UTF-8"?>

<!-- Fichier de styles par défaut pour NatBraille
vous pouvez l'éditer mais "à vos risques et périls" -->

<styles>
	<style name="standard" mode="3-1" />
	<style name="integral" special="g1" mode="3-1" />
	<style name="poesie" mode="1-3" />
	<style name="noteTr" mode="7-5" prefixe="p6p23" suffixe="p56p3"/>
	<style name="noIndent" mode="1" />
	<style name="heading1" mode="title1" />
	<style name="heading" mode="title" />
	<style name="chemistry" special="chemistry" />
	<style name="chimie" special="chimie" />
	<style name="echap" special="g0" />
	<style name="envers" mode="1" upsidedown="yes" />
</styles>
     */
    public static final String XML_HEADER = "<?xml version=\"1.1\" encoding=\"UTF-8\"?>";
    public static final String STYLES = "styles";
    public static final String[] STYLE_ATTRIBUTES = {"name", "mode", "prefixe", "suffixe", "special", "upsidedown"};

    public static String quote(String s) {
        return "\"" + s + "\"";
    }

    public static String toXMLString(String json) {
        JSONObject o = new JSONObject(json);
        List<String> lines = new ArrayList();
        lines.add(XML_HEADER);
        if (o.has(STYLES)) {
            lines.add("<styles>");
            JSONArray styles = o.getJSONArray(STYLES);
            for (int i = 0; i < styles.length(); i++) {
                List<String> line = new ArrayList();
                line.add("<style");
                JSONObject style = styles.getJSONObject(i);
                for (int j = 0; j < STYLE_ATTRIBUTES.length; j++) {
                    String attribute = STYLE_ATTRIBUTES[j];
                    if (style.has(attribute)) {
                        line.add(attribute + "=" + quote(style.getString(attribute)));
                    }
                }
                line.add("/>");
                lines.add(String.join(" ", line));
            }
        }
        lines.add("</styles>");
        return String.join("\n", lines);
    }

    public static String defaultStyles() {
        return String.join("", new String[]{
            "{",
            "styles:[",
            "{ name:\"standard\", mode:\"3-1\"},",
            "{ name:\"integral\", special:\"g1\", mode:\"3-1\"},",
            "{ name:\"poesie\", mode:\"1-3\"},",
            "{ name:\"noteTr\", mode:\"7-5\", prefixe:\"p6p23\", suffixe:\"p56p3\"},",
            "{ name:\"noIndent\", mode:\"1\"},",
            "{ name:\"heading1\", mode:\"title1\"},",
            "{ name:\"heading\", mode:\"title\"},",
            "{ name:\"chemistry\", special:\"chemistry\"},",
            "{ name:\"chimie\", special:\"chimie\"},",
            "{ name:\"echap\", special:\"g0\"},",
            "{ name:\"envers\", mode:\"1\", upsidedown:\"yes\"},",
            "]",
            "}"
        });
    }

    public static void main(String args[]) {
        String s = defaultStyles();
        System.out.println(s);
        System.out.println(toXMLString(s));
    }

}
