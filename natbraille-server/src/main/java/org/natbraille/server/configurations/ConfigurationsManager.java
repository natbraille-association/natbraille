/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.server.configurations;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Date;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.server.TranscriptorDescriptionFile;

/**
 *
 * @author vivien
 */
public class ConfigurationsManager {

    private final Path directory;
    
    private static String dateString() {
        long ms = new Date().toInstant().toEpochMilli();
        return String.valueOf(ms);
    }

    public Path enforceDirectory(Path subpath, Path p) {
        Path filename = p.getFileName();
        Path path = Paths.get(this.directory.toString(), subpath.toString(), filename.toString());
        return path;
    }

    private Path writeFile(Path subpath, Path p, String s) {
        Path path = enforceDirectory(subpath, p);
        try {
            Files.write(path, s.getBytes(StandardCharsets.UTF_8));
            return path.getFileName();
        } catch (IOException e) {
            return null;
        }
    }

    private Path removeFile(Path subpath, Path p) {
        Path path = enforceDirectory(subpath, p);
        try {
            if (Files.exists(path)) {
                if (!Files.isDirectory(path)) {
                    Files.deleteIfExists(path);
                    return path.getFileName();
                }
            }
        } catch (IOException ex) {
        }
        return null;
    }
    public Path[] listDirectory( Path subPath ) {
        //Path path = enforceDirectory(subpath, p);
        File d = Paths.get(directory.toString(),subPath.toString()).toFile();
        if (d.isDirectory()) {
            return Arrays.stream(d.listFiles())
                    .filter(f -> f.isFile())
                    .map(f -> f.toPath())
                    .map(p -> p.getFileName())
                    .filter(p -> p.toString().endsWith(".json"))
                    .toArray(Path[]::new);
        } else {
            Path p[] = {};
            return p;
        }

    }
    private JSONObject readJsonFile(Path sub, Path p) {
        Path path = enforceDirectory(sub, p);
        try {
            String s = new String(Files.readAllBytes(path), "UTF-8");
            System.out.println("file has content\n"+s+"\n--eof");
            return new JSONObject(new String(Files.readAllBytes(path), "UTF-8"));
        } catch (IOException | JSONException e) {
            return null;
        }
    }
    private static final String NO_NAME = "?";
    /*
    private String configurationDisplayName(Path p) {

        String nameKey = NatFilterOption.CONFIG_NAME.name();

        try {
            JSONObject o = readJsonFile(p);
            if ((o != null) && (o.has("options"))) {
                JSONObject options = o.getJSONObject(TranscriptorDescriptionFile.OPTIONS_KEY);
                if (options.has(nameKey)) {
                    return options.getString(nameKey);
                } else {
                    return NO_NAME;
                }
            }
        } catch (JSONException e) {
        }
        return null;

    }
    */
    private static final Path CONFIGURATIONS_DIRECTORY = Paths.get("configurations");
    private static final Path STYLIST_DIRECTORY = Paths.get("stylists");   
    
    public final Path configurationsPath(){
        return Paths.get(CONFIGURATIONS_DIRECTORY.toString());
    }
    public final Path stylistPath(){
        return Paths.get(STYLIST_DIRECTORY.toString());
    }
       
    public Path[] listConfigurations( ) {
        return listDirectory( configurationsPath() );
    }
    public Path[] listStylists( ) {
        return listDirectory( stylistPath() );
    }
    

    public static final String NAME_KEY = "name";
    public static final String CONFIGURATION_KEY = "configuration";
    public static final String ID_KEY = "id";
    public static final String DATE_KEY = "created";

    public JSONObject configurationHandle(Path p) {

        String nameKey = NatFilterOption.CONFIG_NAME.name();
        try {
            JSONObject o = readJsonFile(configurationsPath(),p);
            if ((o != null) && (o.has(TranscriptorDescriptionFile.OPTIONS_KEY))) {
                JSONObject options = o.getJSONObject(TranscriptorDescriptionFile.OPTIONS_KEY);
                String name;
                if (options.has(nameKey)) {
                    name = options.getString(nameKey);
                } else {
                    name = NO_NAME;
                }
                JSONObject message = new JSONObject();
                message.put(DATE_KEY, dateString());
                message.put(ID_KEY, p.toString());
                message.put(NAME_KEY, name);
                message.put(CONFIGURATION_KEY, o);
                return message;
            }
        } catch (JSONException e) {
        }
        return null;

    }

    public JSONArray configurationHandles() {
        JSONArray message = new JSONArray();
        Arrays.stream(this.listConfigurations()).forEach((Path p) -> {
            JSONObject c = configurationHandle(p);
            if (c != null) {
                message.put(c);
            }
        });
        return message;
    }

    public Path addConfiguration(String json) {
        // TODO set date
        UUID uuid = UUID.randomUUID();
        Path path = Paths.get(uuid.toString() + ".json");
        return writeFile(configurationsPath(), path, json);
    }
    public Path replaceConfiguration(String json){
        JSONObject conf = new JSONObject( json );        
        if ( conf.has(ID_KEY) ){
            Path path = Paths.get( conf.getString(ID_KEY) );
            return writeFile(configurationsPath(), path, json);
        } else {
            return null;
        }
    }
    public Path removeConfiguration(Path p) {
        return removeFile(configurationsPath(), p);
    }

    /*
    public Path addStylist(String json){
        JSONObject o = new JSONObject(json);
        UUID uuid = UUID.randomUUID();
        Path path = Paths.get(uuid.toString() + ".stylist.json");
        return writeFile(path, json);
    }
*/  
    private static final String STYLES_KEY= "styles";
    
    
    public JSONObject stylistsHandle(Path p) {
    
        try {
            JSONObject o = readJsonFile(stylistPath(),p);
            if ((o != null) && (o.has( STYLES_KEY ))) {                                
                o.put(ID_KEY, p.toString());                
                return o;
            }
        } catch (JSONException e) {
        }
        return null;

    }
    public String stylistAsXmlString(Path p) {
        //Path fp = enforceDirectory(stylistPath(), p);
        try {
            JSONObject o = readJsonFile(stylistPath(),p);
            return Stylist.toXMLString( o.toString() );
        } catch (JSONException e) {
        }
        return null;

    }
    


    public JSONArray stylistsHandles() {
        JSONArray message = new JSONArray();
        Arrays.stream(this.listStylists()).forEach((Path p) -> {            
            JSONObject c = stylistsHandle(p);
            if (c != null) {                
                message.put(c);
            }
        });
        return message;
    }
    public Path addStylist(String json) {
        UUID uuid = UUID.randomUUID();
        Path path = Paths.get(uuid.toString() + ".json");
        return writeFile(stylistPath(), path, json);
    }
    public Path replaceStylist(String json){
        JSONObject stylist = new JSONObject( json );        
        if ( stylist.has(ID_KEY) ){
            Path path = Paths.get( stylist.getString(ID_KEY) );
            return writeFile(stylistPath(), path, json);
        } else {
            return null;
        }
    }

    public Path removeStylist(Path p) {
        return removeFile(stylistPath(),p);
    }

    public ConfigurationsManager(Path directory) {
        this.directory = directory;
        this.directory.toFile().mkdirs();
        this.configurationsPath().toFile().mkdirs();
        this.stylistPath().toFile().mkdirs();
        
    }
    
    public static void main(String args[]) {

        ConfigurationsManager cm = new ConfigurationsManager(Paths.get("editor-configs"));
        
        System.out.println("=====");
        Arrays.stream(cm.listConfigurations()).forEach((Path p) -> {
            System.err.println(p);
/*            String a = cm.configurationDisplayName(p);
            if (a != null) {
                System.out.println("+" + p + " : " + a);
            } else {
                System.out.println("-" + p + " is not a valid config file");
            }
            */
        });
        System.out.println("=====");
        Arrays.stream(cm.listStylists()).forEach((Path p) -> {
            System.err.println(p);
/*            String a = cm.configurationDisplayName(p);
            if (a != null) {
                System.out.println("+" + p + " : " + a);
            } else {
                System.out.println("-" + p + " is not a valid config file");
            }
            */
        });
        System.err.println("=====");
        JSONArray a = cm.configurationHandles();
        System.err.println(a.toString(1));
        
        System.err.println("=====");
        JSONArray b = cm.stylistsHandles();
        System.err.println(b.toString(1));
/*
        Path p = cm.addConfiguration("mon conteénuemàà");
        System.out.println("path:" + p.toString());
        System.out.println("=====");
        Arrays.stream(cm.listConfigurations()).forEach(f -> {
        System.out.println("+" + f);
        });
        cm.removeConfiguration(p);
        System.out.println("=====");
        Arrays.stream(cm.listConfigurations()).forEach(f -> {
        System.out.println("+" + f);
        });
         */
    }


   
}
