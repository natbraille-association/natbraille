/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * Author : Vivien Guillet <vivien.guillet@laposte.net>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.server;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;
import org.natbraille.core.document.NatDocument;
import org.natbraille.core.document.NatDocumentException;
import org.natbraille.core.filter.NatFilter;
import org.natbraille.core.filter.NatFilterChain;
import org.natbraille.core.filter.NatFilterConfigurator;
import org.natbraille.core.filter.NatFilterException;
import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.core.gestionnaires.Afficheur;
import org.natbraille.core.gestionnaires.GestionnaireErreur;
import org.natbraille.core.gestionnaires.LogMessage;
import org.natbraille.core.gestionnaires.MessageKind;

public class ApiTranscriptor {

    private final GestionnaireErreur gestionnaireErreurs;
    private final AfficheurJSON afficheur;
    private final NatFilterChain filter;

    private static NatFilterChain buildFilter(FilterList filterClasses,
            OptionsList options,
            GestionnaireErreur gestionnaireErreurs) {
        NatFilterChain filter = null;
        try {
            NatFilterOptions nfo = options.asNatFilterOptions();
            NatFilterConfigurator filterConfigurator = new NatFilterConfigurator(nfo, gestionnaireErreurs);
            NatFilterChain chain = filterConfigurator.newFilterChain();
            for (Class<? extends NatFilter> c : filterClasses) {
                chain.addNewFilter(c);
            }
            filter = chain;
        } catch (NatFilterException e) {
            System.out.println(e);
            gestionnaireErreurs.afficheMessage(MessageKind.UNRECOVERABLE_ERROR, e);
        }
        return filter;
    }    
    public ApiResult run(ApiDocument apiDocument) {

        JSONObject jsonDocument = null;
        try {
            System.err.println("---" + apiDocument);
            System.err.println("---" + apiDocument);
            NatDocument input = apiDocument.toNatDocument();
            NatDocument output = this.filter.run(input);
            ApiDocument outputApiDocument = new ApiDocument(output);
            jsonDocument = outputApiDocument.toJSON();
        } catch (NatDocumentException e) {
            gestionnaireErreurs.afficheMessage(MessageKind.UNRECOVERABLE_ERROR,
                    "invalid input document");
        } catch (NatFilterException e) {
            gestionnaireErreurs.afficheMessage(MessageKind.UNRECOVERABLE_ERROR,
                    "filter exception");
        } catch (Exception e) {
            gestionnaireErreurs.afficheMessage(MessageKind.UNRECOVERABLE_ERROR,
                    "probable invalid json in document");
        }
        return new ApiResult(afficheur.messages, jsonDocument);
    }

    public ApiTranscriptor(FilterList filters, OptionsList options, int logLevel) {

        /*
        this.afficheur = new AfficheurJSON(logLevel);
        this.gestionnaireErreurs = new GestionnaireErreur();
        gestionnaireErreurs.addAfficheur(this.afficheur);
        */
        
        this.afficheur = new VoidAfficheur();
        this.gestionnaireErreurs = new GestionnaireErreur();
        gestionnaireErreurs.addAfficheur(this.afficheur);
        
        this.filter = buildFilter(filters, options, this.gestionnaireErreurs);
    }

    private static class VoidAfficheur extends AfficheurJSON {

        public VoidAfficheur() {
            super(666);
        }

        @Override
        public void afficheMessage(LogMessage logMessage) {
            System.out.println(logMessage.getDate()
                    +" | "+logMessage.getLevel()
                    +" | "+logMessage.getKind().name()
                    +" | "+logMessage.getTrContents(getI18n())
            );
            //void
        }
    }

}
