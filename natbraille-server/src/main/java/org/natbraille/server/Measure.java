/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * Author : Vivien Guillet <vivien.guillet@laposte.net>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.natbraille.server;
import java.util.Arrays;

public class Measure {
    public static void repeat( Runnable task, int repeats ){
	long ns_total = 0;
	long[] times = new long[repeats];
	for (int count = 0 ; count < repeats ; count++){
	    long ns_start = System.nanoTime();
	    task.run();
	    long ns_duration = System.nanoTime() - ns_start;
	    times[ count ] = ns_duration;
	}
	long ns_sum = Arrays.stream( times ).sum();
	long ns_mean  = ns_sum / repeats;
	Arrays.stream( times ).reduce(0,
				      (l,x) -> {
					  long ns_diff = x - ns_mean;
					  double dev_pc = 100.0 * (( 1.0 * ns_diff )/ns_mean);
					  double dev_last_pc = 100.0 * (( 1.0 * ( x - l ))/l);
					  System.out.println("> " 
							     + x/1000000.0 + " "+"ms" + "  "
							     + ns_mean/1000000.0  +" "+"ms" +  " "
							     + (( dev_pc > 0 )?"+":"")
							     + dev_pc +" "+"%"+" "
							     + l/1000000.0  +" "+"ms" +  " "
							     + (( dev_last_pc > 0 )?"+":"")
							     + dev_last_pc +" "+"%"+" "
							     );
					  return x;
				      } );	
	System.err.println( "--------" +  "total:" + (ns_sum/1000000000.0)+"s " + "mean:"+(ns_mean/1000000.0)+"ms");

	
    }
    
}
