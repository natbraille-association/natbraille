/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * Author : Vivien Guillet <vivien.guillet@laposte.net>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.natbraille.server.howto;

import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.filter.NatFilterException;
import org.natbraille.core.gestionnaires.LogLevel;
import org.natbraille.server.ApiTranscriptor;
import org.natbraille.server.FilterList;
import org.natbraille.server.OptionsList;
class HowToCreateAnApiTranscriptor {

    /*
     * example of ApiTranscriptor creation with access to
     * java options and filter classes
     */
    public static ApiTranscriptor example_one_for_java_lang(){
	FilterList filters
	    = new FilterList(
			     org.natbraille.core.filter.convertisseur.ConvertisseurTexte.class,
			     org.natbraille.core.filter.transcodeur.TranscodeurNormal.class
			     // org.natbraille.core.filter.presentateur.PresentateurMEP.class,
			     //org.natbraille.core.filter.transcodeur.TranscodeurNormal.class);	
			     );
	OptionsList options = new OptionsList()
	    .set(NatFilterOption.FORMAT_OUTPUT_BRAILLE_TABLE,"TbFr2007")
	    .set(NatFilterOption.MODE_DETRANS,"true");

	return new ApiTranscriptor(filters,  options, LogLevel.DEBUG );
    }   
       
    /*
     * example of ApiTranscriptor creation from java options and
     * filter classes as String
     */
    public static ApiTranscriptor example_two_for_the_strings(){
	ApiTranscriptor apiTranscriptor = null;
	try {
	    String[] classes = {
		"org.natbraille.core.filter.presentateur.PresentateurMEP" ,
		"org.natbraille.core.filter.transcodeur.TranscodeurNormal"
	    }; 
	    FilterList filters = new FilterList(classes);
	    	OptionsList options = new OptionsList()
		    .set("FORMAT_OUTPUT_BRAILLE_TABLE","TbFr2007")
		    .set("MODE_DETRANS","true");
		apiTranscriptor = new ApiTranscriptor(filters,  options, LogLevel.DEBUG );
	}  catch ( NatFilterException e ) {
	    System.out.println(e);
	}
	return apiTranscriptor;
    }
}
