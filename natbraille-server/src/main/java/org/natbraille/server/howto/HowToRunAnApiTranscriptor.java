/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * Author : Vivien Guillet <vivien.guillet@laposte.net>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.natbraille.server.howto;

import org.json.JSONObject ;
import org.natbraille.core.document.NatDocument;
import org.natbraille.core.document.NatDocumentException;
import org.natbraille.server.ApiResult;
import org.natbraille.server.ApiTranscriptor;
import org.natbraille.server.ApiDocument;
class HowToRunAnApiTranscriptor {
    /*
     * example of ApiDocument creation from NatDocument
     * and ApiTranscriptor usage
     */ 
    public static ApiResult example_from_NatDocument(  ) throws NatDocumentException{

	NatDocument natDocument = new NatDocument();
	natDocument.setString("hello transcriptor");
	
	ApiDocument apiDocument = new ApiDocument( natDocument );
	ApiTranscriptor apiTranscriptor = HowToCreateAnApiTranscriptor.example_one_for_java_lang();
	return apiTranscriptor.run( apiDocument );
	
    }
    
    /*
     * example of ApiDocument creation from JSONObject representation
     * of a nat document, and ApiTranscriptor usage
     */ 
    public static ApiResult example_from_JSONObject( ){

	JSONObject dst = new JSONObject();
	
	dst.put( "string", "hello transcriptor");
	dst.put( "charset", "UTF8" );
	dst.put( "contentType", "text/plain");
	dst.put( "brailleTable", "");
	dst.put( "filename", "");

	ApiDocument apiDocument = new ApiDocument( dst );
	ApiTranscriptor apiTranscriptor = HowToCreateAnApiTranscriptor.example_one_for_java_lang();
	return apiTranscriptor.run( apiDocument );
	
    }
    	

	
}
