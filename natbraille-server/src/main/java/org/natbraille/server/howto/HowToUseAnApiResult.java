/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * Author : Vivien Guillet <vivien.guillet@laposte.net>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.natbraille.server.howto;

import org.natbraille.core.document.NatDocument;
import org.natbraille.core.document.NatDocumentException;
import org.json.JSONArray ;
import org.json.JSONObject ;
import org.natbraille.server.ApiResult;
import org.natbraille.server.ApiDocument;
class HowToUseAnApiResult {
    
    /*
     *  serialized json is aimed at other languages than java
     *  (if java was used, the serialization would be pointless) 
     */
    public static void strange_example_where_deserialization_happens_in_java() throws NatDocumentException {
	ApiResult apiResult = HowToRunAnApiTranscriptor.example_from_NatDocument();
	boolean ok = apiResult.json.getString("status") == "OK";
	if (ok){
	    JSONArray log = apiResult.json.getJSONArray("log");
	    for ( int i=0 ; i< apiResult.json.length() ; i++ ){
		JSONObject logItem = log.getJSONObject(i);
		String date = logItem.getString("date"); 
		String kind = logItem.getString("kind"); // org.natbraille.core.gestionnaires.MessageKind
		int level = logItem.getInt("level");// org.natbraille.core.gestionnaires.LogLevel
		String contents = logItem.getString("contents");
	    }
	    JSONObject document = apiResult.json.getJSONObject("document");
	    NatDocument natDocument = new ApiDocument( document ).toNatDocument();

	    // then :
	    // String text = natDocument.getString();	    
	    // Charset charset = natDocument.getOrGuessCharset() );
	    // String contentType = natDocument.getOrGuessContentType() );
	    // BrailleTable brailleTable = natDocument.getBrailletable();
	}
    }
}
