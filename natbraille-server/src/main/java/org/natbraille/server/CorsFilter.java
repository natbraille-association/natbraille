/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * Author : Vivien Guillet <vivien.guillet@laposte.net>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.server;

/**
 *
 * @author vivien
 */
import java.util.HashMap;
import spark.Filter;
import spark.Request;
import spark.Response;
import spark.Spark;

/**
 * Really simple helper for enabling CORS in a spark application;
 * UNUSED ATM
 */
public final class CorsFilter {

    private static final HashMap<String, String> CORS_HEADERS = new HashMap<String, String>();

    static {
        CORS_HEADERS.put("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS");
        CORS_HEADERS.put("Access-Control-Allow-Origin", "*");
        CORS_HEADERS.put("Access-Control-Allow-Headers", "Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin,");
        CORS_HEADERS.put("Access-Control-Allow-Credentials", "true");
    }

    public final static void apply() {
        Filter filter = (Request request, Response response) -> {
            CORS_HEADERS.forEach((key, value) -> {
                response.header(key, value);
            });
        };
        Spark.after(filter);
    }

    /**
     * Usage
     */
//    public static void main(String[] args) {
//        CorsFilter.apply(); // Call this before mapping thy routes
//        Spark.get("/hello", (request, response) -> {
//            return "Hello";
//        });
//    }
}
