/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * Author : Vivien Guillet <vivien.guillet@laposte.net>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.natbraille.server;

//package set FORMAT_OUTPUT_BRAILLE_TABLE TbFr2007
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import org.json.JSONArray ;
import org.json.JSONException;
import org.json.JSONObject ;
import org.natbraille.core.filter.NatFilterException;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.gestionnaires.LogLevel;
    
public class TranscriptorDescriptionFile {
  
    public static final String OPTIONS_KEY = "options";
    public static final String LOG_LEVEL_KEY = "logLevel";
    public static final String FILTERS_KEY = "filters";

    static String readFile(Path path)
    {
	try {
	    byte[] encoded = Files.readAllBytes(path);
	    return new String(encoded, "UTF-8");
	} catch(IOException e){
	    return "";
	}
    }
    public static ApiTranscriptor toApiTranscriptor(Path path) throws NatFilterException {
	ApiTranscriptor apiTranscriptor = null;

	//	JSONObject o = new JSONObject(readFile("./detrans.json"));
        System.err.println("config has" + path);      
        
        
        String zobe = readFile(path);
        System.err.println("config string is" + zobe);
        
	JSONObject o = new JSONObject(readFile(path));
	
	// collect filters
	JSONArray jfilters =  o.getJSONArray(FILTERS_KEY);
	String names[] = new String[ jfilters.length() ];
	for (int i = 0 ; i < names.length ; i++){
	    names[i] = jfilters.getString(i);
	}    
	FilterList filters = new FilterList( names );
	
	// collect options
	OptionsList options = new OptionsList();
	JSONObject jopts = o.getJSONObject(OPTIONS_KEY);
	java.util.Iterator<java.lang.String> onit = jopts.keys();
	while (onit.hasNext()){
	    String key = onit.next();
            if ( key.equals( NatFilterOption.Stylist.name() ) ){
                // hack...
                String uri = "http://localhost:4567/stylists/xml/"+jopts.getString(key);
                options.set( key,  uri);
            } else {
                options.set( key,  jopts.getString(key) );
            }
	}
	int logLevel;
        try {
            logLevel = Integer.parseUnsignedInt(o.getString(LOG_LEVEL_KEY));
        } catch (NumberFormatException | JSONException e){
             logLevel = LogLevel.DEBUG;
        }
	return new ApiTranscriptor(filters,  options, logLevel );

    }
}
