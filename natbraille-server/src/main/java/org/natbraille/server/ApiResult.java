/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * Author : Vivien Guillet <vivien.guillet@laposte.net>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.natbraille.server;

import org.json.JSONArray ;
import org.json.JSONObject ;

public class ApiResult {
    public JSONObject json ;

    // explore log to find last message kind
    private static String getSuccessStatus( JSONArray log ){
	String status = "KO";
	try {
	    String lastKind = log.getJSONObject( log.length() -1 ).getString("kind");
	    if ( (lastKind == "SUCCESS" ) || (lastKind == "FINAL_SUCCESS" ) ){
		status = "OK";
	    }
	} catch (Exception e){}
	return status;
    }
    public ApiResult(JSONArray log, JSONObject jsonDocument){
	JSONObject json = new JSONObject();
	json.put("log",log);
	if (jsonDocument != null) {
	    json.put("document",jsonDocument);
	}
	json.put("status",getSuccessStatus( log ));
	this.json = json;
    }
    @Override
    public String toString(){
	return json.toString();
    }
}
