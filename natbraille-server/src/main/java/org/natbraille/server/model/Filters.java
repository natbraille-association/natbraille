/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * Author : Vivien Guillet <vivien.guillet@laposte.net>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.server.model;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.natbraille.core.filter.NatFilter;
import org.reflections8.Reflections;
import org.reflections8.util.ConfigurationBuilder;

/**
 *
 * @author vivien
 */
public class Filters {

    public JSONArray json;

    public Filters() {
        json = buildJSON();
    }

    private static JSONArray buildJSON() {
        JSONArray data = new JSONArray();
        // TODO, todo : todo...
        // TODO, todo : todo...
        // TODO, todo : todo...
        // TODO, todo : todo...
        // TODO, todo : todo...
        // TODO, todo : todo...
        File file = new File("/home/vivien/src/natbraille/trunk/natbraille-server/target/natbraille-server-unspecified-jar-with-dependencies.jar");
        // TODO, todo : todo...
        // TODO, todo : todo...
        // TODO, todo : todo...
        // TODO, todo : todo...
        // TODO, todo : todo...
        // TODO, todo : todo...
        try {
            URL url;
            url = new URL("jar:file:" + file.getAbsolutePath() + "!/");
            ConfigurationBuilder conf = new ConfigurationBuilder().addUrls(url);
            ((Set<Class<? extends NatFilter>>) new Reflections(conf)
                    .getSubTypesOf((Class) NatFilter.class))
                    .forEach((c) -> {
                        data.put(c.getCanonicalName());
                    });
        } catch (MalformedURLException ex) {
            Logger.getLogger(Filters.class.getName()).log(Level.SEVERE, null, ex);
        }
        return data;
    }

    public static void main(String[] args) {
        System.out.println(buildJSON().toString());
    }
}
