/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.natbraille.server.model;

import org.json.JSONArray;
import org.natbraille.brailletable.system.SystemBrailleTables;

/**
 *
 * @author vivien
 */
public class BrailleTables {
    
    public JSONArray json;

    public BrailleTables() {
        json = buildJSON();
    }
    private static JSONArray buildJSON(){
        JSONArray json = new JSONArray();
        String[] names = SystemBrailleTables.getNames();
        for ( int i = 0 ; i < names.length ; i++){
            json.put(names[ i ]);
        }
        return json;
    }
}
