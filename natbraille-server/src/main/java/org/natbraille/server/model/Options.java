/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * Author : Vivien Guillet <vivien.guillet@laposte.net>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.natbraille.server.model;

import org.json.JSONArray;
import org.json.JSONObject;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.filter.options.NatFilterOptionCategory;
import org.natbraille.core.filter.options.NatFilterOptionDetail;

/**
 *
 * @author vivien
 */
public final class Options {
    public JSONArray json;
    public Options() {
        json = buildJSON();
    }
    public JSONArray buildJSON() {
        JSONArray data = new JSONArray();
        for (NatFilterOption o : NatFilterOption.values()) {
            JSONObject optDesc = new JSONObject();
            optDesc.put("name", o.name());
            NatFilterOptionDetail detail = o.getDetail();
            JSONArray categories = new JSONArray();
            for (NatFilterOptionCategory c : detail.categories()) {
                categories.put(c.name());
            }
            optDesc.put("categories", categories);
            optDesc.put("comment", detail.comment());
            optDesc.put("default", detail.defaultValue());
            optDesc.put("hidden", detail.hidden());
            JSONArray possibleValues = new JSONArray();
            for (String c : detail.possibleValues()) {
                possibleValues.put(c);
            }
            optDesc.put("possibleValues", possibleValues);
            optDesc.put("type", detail.type().name());
            data.put(optDesc);
        }
        return data;
    }
    /*
    public static void main(String[] args) {
        Options a = new Options();
        System.err.println(a.json);
    }
    */
}
