/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * Author : Vivien Guillet <vivien.guillet@laposte.net>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


package org.natbraille.server;
import java.util.ArrayList;
import org.natbraille.core.filter.NatFilter;
import java.util.Arrays;
import java.util.stream.Collectors ;
import org.natbraille.core.filter.NatFilterException;
import org.natbraille.core.filter.NatFilterErrorCode;

public class FilterList extends ArrayList<Class<? extends NatFilter>>{

    /*
     * Wrong class in param list is detected at compile time
     */
    public FilterList(Class<? extends NatFilter> ...natFilters){
	for( Class<? extends NatFilter> f : natFilters){
	    this.add(f);
	}
    }
    /*
     * Wrong class in String list throws NatFilterException
     */
    public FilterList(String strings[]) throws NatFilterException {
	Class<? extends NatFilter> natFilters[] = Utils.classNamesToClasses(strings);
	for( Class<? extends NatFilter> f : natFilters){
	    this.add(f);
	}
	if (strings.length != this.size())
	    throw new NatFilterException(NatFilterErrorCode.FILTER_INITIALIZATION_ERROR,
					 "bad filter: " + Arrays.stream(strings).collect(Collectors.joining(", ")));					 
    }


	
    /*
     * Errors handling
     */
    private static void test_errors(){
	/*
	 * compiles
	 */
	FilterList fl1 = new FilterList(//java.util.ArrayList.class,
				       org.natbraille.core.filter.presentateur.PresentateurMEP.class,
				       org.natbraille.core.filter.transcodeur.TranscodeurNormal.class);
	/*
	 * compile time error
	 */
	// FilterList fl2 = new FilterList(java.util.ArrayList.class,
	// 				    org.natbraille.core.filter.presentateur.PresentateurMEP.class,
	// 				    org.natbraille.core.filter.transcodeur.TranscodeurNormal.class);


	String classNames1[] = {"org.natbraille.core.filter.presentateur.PresentateurMEP",
			       "org.natbraille.core.filter.transcodeur.TranscodeurNormal"};
	String classNames2[] = {
	    "java.util.ArrayList",
	    "org.natbraille.core.filter.presentateur.PresentateurMEP",
	    "org.natbraille.core.filter.transcodeur.TranscodeurNormal"
	};
	try {
	    /*
	     * ok
	     */
	    FilterList fl3 = new FilterList( classNames1 );
	    /*
	     * throws NatFilterException
	     */
	    FilterList fl4 = new FilterList( classNames2 );
	} catch (NatFilterException e){
	    System.out.println(e);
	}
    }
}
