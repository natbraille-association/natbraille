#!/bin/bash
#
# utilise curl, jq et pygmentize
#

function wscall {
    echo 'url    :' "$2"
#    echo 'string :' "$1"
    #
    # - la table braille doit être modifiée dans la config (répertoire tdf/ nom correspondant à l'url)
    # - 'charset' et 'filename' ne servent à rien non plus
    #
    DATA='{ "base64" : "'$1'",  "charset" : "UTF-8",  "contentType" : "application/msword",  "brailleTable" : "TbFr2007",  "filename" : "myfilename" }'
    #
    #curl -s -H "Content-Type: application/json" -X POST -d "$DATA" "$2" | jq -r '.document.string'
    # no filter
    curl -s -H "Content-Type: application/json" -X POST -d "$DATA" "$2" #| pygmentize
}

BASE64DOC=`base64 -w0 A_ExtraitEnvers_liste.doc`
echo "= StarMath To MathML"
STARMATH=$BASE64DOC
URL3="http://localhost:4567/tdf/standard_transcriptor_debug.json"
wscall $STARMATH $URL3 
