#!/bin/bash
#
# utilise curl, jq et pygmentize
#

function wscall {
    echo 'url    :' "$2"
    echo 'string :' "$1"
    #
    # - la table braille doit être modifiée dans la config (répertoire tdf/ nom correspondant à l'url)
    # - 'charset' et 'filename' ne servent à rien non plus
    #
    DATA='{ "string" : "'$1'",  "charset" : "UTF-8",  "contentType" : "text/plain",  "brailleTable" : "TbFr2007",  "filename" : "myfilename" }'
    #
    # curl -s -H "Content-Type: application/json" -X POST -d "$DATA" "$2" | jq -r '.document.string'
    # no filter
#    curl -s -H "Content-Type: application/json" -X POST -d "$DATA" "$2" #| pygmentize
        RES=`curl -s -H "Content-Type: application/json" -X POST -d "$DATA" "$2"`
    echo  $RES  | jq -r '.status'
    echo  $RES  | jq -r '.document.string' 

}


echo "= StarMath To MathML To Braille"
STARMATH2="a+b+c+x^2"
URL4="http://localhost:4567/tdf/starofficeToMathMLToBraille.json"
wscall $STARMATH2 $URL4
