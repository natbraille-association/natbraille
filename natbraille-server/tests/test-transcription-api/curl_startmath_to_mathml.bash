#!/bin/bash
#
# utilise curl, jq et pygmentize
#

function wscall {
    echo 'url    :' "$2"
    echo 'string :' "$1"
    #
    # - la table braille doit être modifiée dans la config (répertoire tdf/ nom correspondant à l'url)
    # - 'charset' et 'filename' ne servent à rien non plus
    #
    DATA='{ "string" : "'$1'",  "charset" : "UTF-8",  "contentType" : "text/plain",  "brailleTable" : "TbFr2007",  "filename" : "myfilename" }'
    #
    RES=`curl -s -H "Content-Type: application/json" -X POST -d "$DATA" "$2"`
    echo  $RES  | jq -r '.status'
    echo  $RES  | jq -r '.document.string' 
    # no filter
    # curl -s -H "Content-Type: application/json" -X POST -d "$DATA" "$2" #| pygmentize
}


echo "= StarMath To MathML"
# STARMATH="sqrt(8+sqrt3^2)"
STARMATH="$@"
URL3="http://localhost:4567/tdf/starofficeToMathML.json"
wscall $STARMATH $URL3 | pygmentize
