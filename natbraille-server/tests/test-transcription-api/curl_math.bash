#!/bin/bash
#
# utilise curl, jq et pygmentize
#

function wscall {
    echo 'url    :' "$2"
    echo 'string :' "$1"
    #
    # - la table braille doit être modifiée dans la config (répertoire tdf/ nom correspondant à l'url)
    # - 'charset' et 'filename' ne servent à rien non plus
    #
    DATA='{ "string" : "'$1'",  "charset" : "UTF-8",  "contentType" : "text/plain",  "brailleTable" : "TbFr2007",  "filename" : "myfilename" }'
    #
    curl -s -H "Content-Type: application/json" -X POST -d "$DATA" "$2" | jq -r '.document.string'
    # no filter
    # curl -s -H "Content-Type: application/json" -X POST -d "$DATA" "$2" #| pygmentize
}

# echo "= StarMath To MathML"
# STARMATH="sqrt(8+sqrt3^2)"
# URL3="http://localhost:4567/tdf/starofficeToMathML.json"
# wscall $STARMATH $URL3 

# echo "= StarMath To MathML To Braille"
# STARMATH2="a+b+c+x^2"
# URL4="http://localhost:4567/tdf/starofficeToMathMLToBraille.json"
# wscall $STARMATH2 $URL4

# echo "= MathML vers Braille"
# MATHML='<math><mfrac><mn>1</mn><msqrt><mrow><mi>(</mi><mi>a</mi><mo>+</mo><mn>3</mn><mi>)</mi></mrow><mo>+</mo><mn>1</mn></msqrt></mfrac></math>'
# URL1="http://localhost:4567/tdf/mathml2braille.json"
# wscall $MATHML $URL1

# echo "= Braille vers MathML"
# BRAILLE="⠠⠄⠦⠁⠖⠩⠴"
# URL2="http://localhost:4567/tdf/mathbraille2mathml.json"
# wscall $BRAILLE $URL2 | pygmentize

echo "= Braille vers StarMath"
# BRAILLE2="⠠⠄⠦⠁⠖⠩⠴"
BRAILLE2="⠠⠄⠁⠖⠃⠖⠉⠖⠭⠈⠣"
URL3="http://localhost:4567/tdf/mathbraille2starmath.json"
wscall $BRAILLE2 $URL3 | pygmentize

