#!/bin/bash

# get printer list
FIRST_PRINTER=$( curl http://localhost:4567/printers | jq -r '.[0]' )
echo "first printer: $FIRST_PRINTER"

DATA='{ "text" : "printer test",  "printer" : "'$FIRST_PRINTER'" }'

echo "$DATA" | json_pp

#
# uncomment to print
#

# curl -s -H "Content-Type: application/json" -X POST -d "$DATA" http://localhost:4567/printers | json_pp
