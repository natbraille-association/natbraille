#!/bin/bash

# get printer list
curl http://localhost:4567/configurations | json_pp

# add configuration
CFGFILE="@conf.json"
curl -s -H "Content-Type: application/json" -X POST -d "$CFGFILE" http://localhost:4567/configurations

# remove configuration
CFGID="89150d3f-ff36-4de5-8dbc-cc856069253b.json"
curl -X DELETE "http://localhost:4567/configurations/$CFGID"


# get("/stylist/xml/:id", (req,res) -> {
#             String s = defaultStyles();
#             return toXMLString(s);
#         });

curl http://localhost:4567/stylist/xml/666
curl http://localhost:4567/stylist/json/666
