# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-12-02 18:09+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/main/java/org/natbraille/brailletable/ui/BrailleTableCli.java:66
msgid "set input encoding to <arg>"
msgstr ""

#: src/main/java/org/natbraille/brailletable/ui/BrailleTableCli.java:68
msgid "set output encoding to <arg>"
msgstr ""

#: src/main/java/org/natbraille/brailletable/ui/BrailleTableCli.java:70
msgid "set source braille table to <arg>"
msgstr ""

#: src/main/java/org/natbraille/brailletable/ui/BrailleTableCli.java:72
msgid "set destination braille table to <arg>"
msgstr ""

#: src/main/java/org/natbraille/brailletable/ui/BrailleTableCli.java:74
msgid "copy source chars missing in source braille table"
msgstr ""

#: src/main/java/org/natbraille/brailletable/ui/BrailleTableCli.java:76
msgid "copy source chars missing in source or destination braille table"
msgstr ""

#: src/main/java/org/natbraille/brailletable/ui/BrailleTableCli.java:78
msgid "list all known braille table names"
msgstr ""

#: src/main/java/org/natbraille/brailletable/ui/BrailleTableCli.java:80
msgid "name output filenames by appending <arg>"
msgstr ""

#: src/main/java/org/natbraille/brailletable/ui/BrailleTableCli.java:82
msgid "display help"
msgstr ""

#: src/main/java/org/natbraille/brailletable/ui/BrailleTableCli.java:99
#, java-format
msgid "file : ''{0}''"
msgstr ""

#: src/main/java/org/natbraille/brailletable/ui/BrailleTableCli.java:112
#, java-format
msgid "wrote file : ''{0}''"
msgstr ""

#: src/main/java/org/natbraille/brailletable/ui/BrailleTableCli.java:149
msgid "no input encoding provided"
msgstr ""

#: src/main/java/org/natbraille/brailletable/ui/BrailleTableCli.java:156
msgid "no output encoding provided"
msgstr ""

#: src/main/java/org/natbraille/brailletable/ui/BrailleTableCli.java:165
#: src/main/java/org/natbraille/brailletable/ui/BrailleTableCli.java:173
#, java-format
msgid "not a braille table name : ''{0}''"
msgstr ""

#: src/main/java/org/natbraille/brailletable/Converter.java:110
#, java-format
msgid "missing char #{0} in source table"
msgstr ""

#: src/main/java/org/natbraille/brailletable/Converter.java:118
#, java-format
msgid "missing char #{0} (pt{1}) in destination table"
msgstr ""

#: src/main/java/org/natbraille/brailletable/impl/BrailleTableImpl.java:61
#, java-format
msgid "not an unicode braille char : {0}"
msgstr ""
