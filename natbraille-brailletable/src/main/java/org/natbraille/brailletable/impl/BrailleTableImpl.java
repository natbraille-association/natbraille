/*
 * NatBraille - BrailleTable - An universal Translator
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.brailletable.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.natbraille.brailletable.BrailleTable;
import org.natbraille.brailletable.BrailleTableException;
import org.natbraille.brailletable.auto.Compatibility;
import org.natbraille.brailletable.UnicodeBraille;
import org.xnap.commons.i18n.I18n;
import org.xnap.commons.i18n.I18nFactory;

/**
 * Implementation of the {@link BrailleTable} class using a Character array.
 * This array is indexed according to the unicode braille page.
 * <p>
 * Do not use this directly.
 * <p>
 * Instead, use one of the static methods in
 * {@link org.natbraille.brailletable.BrailleTables}.
 * <p>
 * @author vivien
 */
public class BrailleTableImpl implements BrailleTable {

    private I18n i18n = I18nFactory.getI18n(BrailleTable.class);

    private final Character substitutions[] = new Character[UnicodeBraille.LENGTH_8DOTS];

    private static int unicodeBrailleCharToPosition(Character c) {
        return (int) c - UnicodeBraille.FIRST;
    }

    private static char positionToUnicodeBrailleChar(int pos) {
        return (char) (pos + UnicodeBraille.FIRST);
    }

    @Override
    public final void setChar(Character brailleUnicode, Character substitution) throws BrailleTableException {
        if (!UnicodeBraille.isEightDots(brailleUnicode)) {
            throw new BrailleTableException(i18n.tr("not an unicode braille char : {0}", (int) brailleUnicode));
        }
        substitutions[unicodeBrailleCharToPosition(brailleUnicode)] = substitution;
    }

    @Override
    public final Character getChar(Character brailleUnicode) {
        if (!UnicodeBraille.isEightDots(brailleUnicode)) {
            return null;
        }
        return substitutions[unicodeBrailleCharToPosition(brailleUnicode)];
    }

    @Override
    public final Character toUnicodeBraille(Character substitutionCharacter) {
        for (int i = 0; i < substitutions.length; i++) {
            Character c = substitutions[i];
            if ((c != null) && (c.equals(substitutionCharacter))) {
                return positionToUnicodeBrailleChar(i);
            }
        }
        return null;
    }

    private String name;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean isCompatible(List<BrailleTable> otherBrailleTables) {
        List<BrailleTable> bt = new ArrayList();
        try {
            return Compatibility.checkIncompatibilities(bt).isEmpty();
        } catch (BrailleTableException ex) {
            Logger.getLogger(BrailleTableImpl.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

}
