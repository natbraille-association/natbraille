/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.natbraille.brailletable.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import org.apache.commons.io.IOUtils;
import org.natbraille.brailletable.BrailleTable;
import org.natbraille.brailletable.BrailleTableException;
import org.natbraille.brailletable.UnicodeBraille;

/**
 * Read/write a brailleTable from an utf8 stream, utf8 braille ordered.
 * <p>
 * @author vivien
 */
public class BrailleTableFile {

    /**
     * Write a brailleTable as an utf8 stream; utf8 braille ordered
     * <p>
     * @param brailleTable the Braille table to be written
     * @param stringOs the destination stream where the string will be writen
     * @throws Exception
     */
    public static void toOutputStream(BrailleTable brailleTable, OutputStream stringOs) throws Exception {
        try (OutputStreamWriter osr = new OutputStreamWriter(stringOs, "UTF-8")) {
            for (char c = UnicodeBraille.FIRST; c <= UnicodeBraille.LAST_6DOTS; c++) {
                Character destCode = brailleTable.getChar(c);
                if (destCode != null) {
                    osr.write(destCode);
                }
            }
            osr.flush();
        } catch (IOException ex) {
            throw new BrailleTableException("error while writing braille table " + ex.getLocalizedMessage());
        }
    }

    /**
     * Read a brailleTable from an utf8 stream, utf8 braille ordered
     * <p>
     * @param stringIs the stream containing the Braille Table File
     * @return
     * @throws BrailleTableException
     */
    public static BrailleTable fromInputStream(InputStream stringIs) throws BrailleTableException {
        try {
            BrailleTableImpl brailleTable = new BrailleTableImpl();            
            char brailleChars[] = IOUtils.toCharArray(stringIs, "UTF8");
            for (char i = 0; i < UnicodeBraille.LENGTH_6DOTS; i++) {
                brailleTable.setChar((char) (UnicodeBraille.FIRST+i), brailleChars[i]);
            }
            return brailleTable;
        } catch (IOException e) {
            throw new BrailleTableException("error while creating braille table " + e.getLocalizedMessage());
        }
    }

}
