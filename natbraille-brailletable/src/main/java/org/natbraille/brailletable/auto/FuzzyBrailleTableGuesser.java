/*
 * NatBraille - BrailleTable - An universal Translator
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.brailletable.auto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.natbraille.brailletable.BrailleTable;
import org.natbraille.brailletable.BrailleTableException;
import org.natbraille.brailletable.BrailleTables;
import org.natbraille.brailletable.UnicodeBraille;
import org.natbraille.brailletable.system.SystemBrailleTables;

/**
 *
 * @author vivien
 */
public class FuzzyBrailleTableGuesser {

    /**
     * Count matches only.
     * @param unknown
     * @param bt
     * @return 
     */
    private static Integer score(String unknown, BrailleTable bt) {
        Integer score = 0;
        for (int i = 0; i < unknown.length(); i++) {
            if (bt.toUnicodeBraille((char) unknown.codePointAt(i)) != null) {
                score++;
            }
        }
        return score;
    }

    /**
     * Guess the {@link BrailleTable}s used in given Braille String amongst given
     * {@link BrailleTables}.
     *
     * @param unknown the Braille String
     * @param checkedBrailleTables the Braille tables to be checked against.
     * @return the best matching {@link BrailleTable}s (having best score), empty list if none.
     */
    public static List<BrailleTable> guess(String unknown, List<BrailleTable> checkedBrailleTables) {

        Map<BrailleTable,Integer> scores= new HashMap<>();
        
        Integer bestScore = 0;
        for (BrailleTable bt : checkedBrailleTables) {
            Integer score = score(unknown, bt);
            scores.put(bt, score);
            if (score > bestScore) {
                bestScore = score;
            }
        }

        List<BrailleTable> bests = new ArrayList<>();
        for (BrailleTable bt : scores.keySet()){
            if (Objects.equals(scores.get(bt), bestScore)){
                bests.add(bt);
            }
        }
        
        return bests;

    }


    /**
     * Test TableGuesser on all known {@link SystemBrailleTables}.
     *
     * @param args : unused
     * @throws BrailleTableException
     */
    public static void main(String[] args) throws BrailleTableException {
        
        
         List<BrailleTable> checkedBts = new ArrayList<>();

        for (String tbName : SystemBrailleTables.getNames()) {
            BrailleTable checkedBt;
            try {
                checkedBt = BrailleTables.forName(tbName);
                checkedBts.add(checkedBt);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        
        for (String mysteryTableName : SystemBrailleTables.getNames()) {

            BrailleTable mysteryTable = BrailleTables.forName(mysteryTableName);
            StringBuilder sb = new StringBuilder();
            for (Character c : UnicodeBraille.allSixDots()) {
                sb.append(mysteryTable.getChar(c));
            }
            String mysteryString = sb.toString();
            System.err.println("Test table " + mysteryTableName + " : \"" + mysteryString + "\"");
            List<BrailleTable> bestBrailleTables = guess(mysteryString,checkedBts);
            if (bestBrailleTables.size() == 0) {
                System.err.println("[KO] no guess");
            } else if (!mysteryTableName.equals(bestBrailleTables.get(0).getName())) {
                System.err.println("[KO] bad guess : " + bestBrailleTables.get(0).getName());
            } else {
                System.err.println("[OK] good guess : " + bestBrailleTables.get(0).getName());
            }
        }

    }
}
