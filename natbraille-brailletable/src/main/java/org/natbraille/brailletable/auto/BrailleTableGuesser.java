/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.natbraille.brailletable.auto;

import java.util.ArrayList;
import java.util.List;
import org.natbraille.brailletable.BrailleTable;
import org.natbraille.brailletable.BrailleTables;
import org.natbraille.brailletable.system.SystemBrailleTables;

/**
 *
 * @author vivien
 */
public class BrailleTableGuesser {

    public final List<BrailleTable> checkedBrailleTables;
    private final ExactBrailleTableGuesser exactBrailleTableGuesser;

    /**
     * Create a @{link BrailleTableGuesser}.
     *
     * @param checkedBrailleTables the BrailleTables to be checked.
     */
    public BrailleTableGuesser(List<BrailleTable> checkedBrailleTables) {
        this.checkedBrailleTables = checkedBrailleTables;
        this.exactBrailleTableGuesser = new ExactBrailleTableGuesser(checkedBrailleTables);
    }

    /**
     * Create a @{link BrailleTableGuesser} for System Braille Tables.
     */
    public BrailleTableGuesser() {
        this.checkedBrailleTables = new ArrayList<>();

        for (String tbName : SystemBrailleTables.getNames()) {
            BrailleTable checkedBt;
            try {
                checkedBt = BrailleTables.forName(tbName);
                this.checkedBrailleTables.add(checkedBt);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        this.exactBrailleTableGuesser = new ExactBrailleTableGuesser(checkedBrailleTables);
    }

    /**
     * Get the @{link BrailleTable}s fuzzyly matching a given Braille String.
     * <p>
     * Select the BrailleTable whose symbols count is the highest in a given
     * Braille String.
     *
     * @param unknown
     * @return
     */
    public final List<BrailleTable> fuzzyMatch(String unknown) {
        return FuzzyBrailleTableGuesser.guess(unknown, checkedBrailleTables);
    }

    /**
     * Get the @{link BrailleTable}s matching a given Braille String.
     * <p>
     * For a BrailleTable to be matched, every caracter in the Braille String
     * must be part of this BrailleTable, OR be absent in every other checked
     * BrailleTable.
     * <p>
     *
     * @param unknown
     * @param stopWhenUniq if true, stop as soon as there is only one solution.
     * @return
     */
    public final List<BrailleTable> exactMatch(String unknown, boolean stopWhenUniq) {
        return exactBrailleTableGuesser.exactMatches(unknown, stopWhenUniq);

    }

}
