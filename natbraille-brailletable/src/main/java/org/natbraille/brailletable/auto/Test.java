/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.natbraille.brailletable.auto;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.IOUtils;
import org.natbraille.brailletable.BrailleTable;
import org.natbraille.brailletable.BrailleTableException;
import org.natbraille.brailletable.BrailleTables;
import org.natbraille.brailletable.auto.Compatibility;
import org.natbraille.brailletable.Converter;
import org.natbraille.brailletable.StreamConverter;
import org.natbraille.brailletable.UnicodeBraille;
import org.natbraille.brailletable.system.SystemBrailleTables;

/**
 *
 * @author vivien
 */
public class Test {

    public static String randomUnicodeBraille(int size, boolean eightDot) {
        StringBuilder randomUnicodeBraille = new StringBuilder();
        for (int i = 0; i < size; i++) {
            int min = UnicodeBraille.FIRST;
            int len = (eightDot) ? (UnicodeBraille.LENGTH_8DOTS) : (UnicodeBraille.LENGTH_6DOTS);
            int rand = min + (int) Math.floor(Math.random() * (float) len);
            randomUnicodeBraille.appendCodePoint(rand);
        }
        return randomUnicodeBraille.toString();
    }

    private static boolean compare(byte[] model, byte[] resu) {
        boolean error = false;
        if ((model.length) != (resu.length)) {
            System.err.print(" error : different lenghts " + resu.length + " != " + model.length + " ; ");
            //error = true;
        }
        for (int i = 0; i < model.length; i++) {
            if (!(i < resu.length)) {
                System.err.print("X error : different lenghts " + resu.length + " != " + model.length);
                return false;
            }
            if (model[i] != resu[i]) {
                System.err.print(" error (" + i + ":" + resu[i] + "!=" + model[i] + ")");
                error = true;
            }
        }

        return !error;
    }

    private static byte[] conv(byte[] source, String srcEnc, BrailleTable srcTable, String destEnc, BrailleTable destTable) throws IOException, UnsupportedEncodingException, BrailleTableException {
        StreamConverter sc = new StreamConverter(srcEnc, srcTable, destEnc, destTable);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ByteArrayInputStream bais = new ByteArrayInputStream(source);
        sc.convert(bais, baos);
        return baos.toByteArray();
    }

    private static class TestFile {

        String name;
        BrailleTable brailleTable;
        String encoding;
        byte[] data;

        public TestFile(String name, BrailleTable brailleTable, String encoding) throws IOException {
            this.name = name;
            this.brailleTable = brailleTable;
            this.encoding = encoding;
            this.data = IOUtils.toByteArray(Test.class.getResourceAsStream(name));
        }

    }

    public static void printSystemTables() throws BrailleTableException {
        BrailleTable bt_unicode = BrailleTables.forName(SystemBrailleTables.BrailleUTF8);
        System.err.println("System Braille tables:");
        for (String name : SystemBrailleTables.getNames()) {

            BrailleTable bt_codeus = BrailleTables.forName(name);
            Converter converter = new Converter(bt_unicode, bt_codeus);
            for (Character c : UnicodeBraille.allSixDots()) {
                System.err.print(converter.convert(c) + " ");
            }
            System.err.println(" " + name);

        }

    }

    public static boolean testBrailleTableEncoding(BrailleTable bt, String encoding) throws BrailleTableException, IOException {
        BrailleTable bt_unicode = BrailleTables.forName(SystemBrailleTables.BrailleUTF8);
        String e_utf8 = "UTF8";

        TestFile src = new TestFile("brailleUTF8-UTF8.txt", bt_unicode, e_utf8);
        byte toTested[] = conv(src.data, e_utf8, bt_unicode, src.encoding, src.brailleTable);
        byte toUnicode[] = conv(toTested, src.encoding, src.brailleTable, e_utf8, bt_unicode);
        return compare(toUnicode, src.data);

    }

    public static void main(String argv[]) throws BrailleTableException, FileNotFoundException, IOException, Exception {

        printSystemTables();
        BrailleTable bt_unicode = BrailleTables.forName(SystemBrailleTables.BrailleUTF8);
        BrailleTable bt_duxcbfr1252 = BrailleTables.forName(SystemBrailleTables.DuxCBfr1252);
        BrailleTable bt_duxtbfr2007 = BrailleTables.forName(SystemBrailleTables.DuxTbFr2007);
        BrailleTable bt_tbfr2007 = BrailleTables.forName(SystemBrailleTables.TbFr2007);
        BrailleTable bt_cbfr1252 = BrailleTables.forName(SystemBrailleTables.CBFr1252);
        BrailleTable bt_codeus = BrailleTables.forName(SystemBrailleTables.CodeUS);

        String sbtns[] = SystemBrailleTables.getNames();
        for (int i = 0; i < (sbtns.length - 1); i++) {
            for (int j = i + 1; j < (sbtns.length); j++) {
                List<BrailleTable> ct = new ArrayList<>();
                ct.add(BrailleTables.forName(sbtns[i]));
                ct.add(BrailleTables.forName(sbtns[j]));
                Map<Character, String> incomp = Compatibility.checkIncompatibilities(ct);
                StringBuilder sb = new StringBuilder();
                sb.append(sbtns[i]).append(" and ").append(sbtns[j]).append(" are ");
                if (incomp.isEmpty()) {
                    sb.append("compatibles");
                } else {
                    sb.append("incompatibles :");
                    for (Character v : incomp.keySet()) {
                        sb.append(" ").append(v).append(" means ").append(incomp.get(v));
                    }

                }
                sb.insert(0, incomp.isEmpty() ? "[OK] " : "[!!] ");
                System.err.println(sb.toString());
            }
        }

        System.exit(0);

        String e_utf8 = "UTF8";
        String e_cp1252 = "cp1252";
        String e_ascii = "ASCII";
        String e_iso8859_1 = "iso8859-1";

        printSystemTables();

        TestFile testFiles[] = new TestFile[]{
            new TestFile("TbFR2007-UTF8.txt", bt_tbfr2007, e_utf8),
            new TestFile("TbFR2007-cp1252.txt", bt_tbfr2007, e_cp1252),
            new TestFile("TbFR2007-iso8859-1.txt", bt_tbfr2007, e_iso8859_1),
            new TestFile("brailleUTF8-UTF8.txt", bt_unicode, e_utf8),
            new TestFile("codeUS-UTF8.txt", bt_codeus, e_utf8),
            new TestFile("codeUS-cp1252.txt", bt_codeus, e_cp1252),
            new TestFile("codeUS-iso8859-1.txt", bt_codeus, e_iso8859_1),
            new TestFile("duxTbFR2007-UTF8.txt", bt_duxtbfr2007, e_utf8),
            new TestFile("duxTbFR2007-cp1252.txt", bt_duxtbfr2007, e_cp1252),
            new TestFile("duxTbFR2007-iso8859-1.txt", bt_duxtbfr2007, e_iso8859_1)
        };
        for (TestFile src : testFiles) {
            for (TestFile resu : testFiles) {
                System.err.print("conv " + src.name + " to " + resu.name);
                byte err[] = conv(src.data, src.encoding, src.brailleTable, resu.encoding, resu.brailleTable);
                String ns;
                boolean match = compare(err, resu.data);
                if (match) {
                    System.err.println(" YES");
                    ns = "OK";
                } else {
                    System.err.println(" NO");
                    ns = "KO";
                }
                File f = new File("resu/" + ns + "_" + src.name + "_to_" + resu.name);
                try {
                    Files.createDirectory(Paths.get("resu"));
                } catch (Exception e) {
                }
                IOUtils.copy(new ByteArrayInputStream(err), new FileOutputStream(f));
                if (!match) {
                    IOUtils.copy(new ByteArrayInputStream(resu.data), new FileOutputStream(new File(f.getAbsolutePath() + "_attendu")));
                }
            }

        }


        /*
         byte[] b_testfile_CodeUS_ASCII = getExemple("testfile_CodeUS_ASCII");
         byte[] b_testfile_DuxCBfr1252_cp1252 = getExemple("testfile_DuxCBfr1252_cp1252");
         byte[] b_testfile_brailleUTF8_UTF8 = getExemple("testfile_brailleUTF8_UTF8");

         //
         System.err.println(
         "CodeUS_ASCII__DuxCBfr1252_cp1252");
         byte[] b_testfile_CodeUS_ASCII__DuxCBfr1252_cp1252 = conv(b_testfile_CodeUS_ASCII, e_ascii, bt_codeus, e_cp1252, bt_duxcbfr1252);

         System.err.println(compare(
         b_testfile_CodeUS_ASCII__DuxCBfr1252_cp1252,
         b_testfile_DuxCBfr1252_cp1252
         ) ? "ok" : "ko");
         */
    }

}
