/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.natbraille.brailletable.auto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.natbraille.brailletable.BrailleTable;
import org.natbraille.brailletable.BrailleTableException;
import org.natbraille.brailletable.BrailleTables;
import org.natbraille.brailletable.Converter;
import org.natbraille.brailletable.UnicodeBraille;
import org.natbraille.brailletable.system.SystemBrailleTables;

/**
 *
 * @author vivien
 */
public class ExactBrailleTableGuesser {

    /**
     * Get the map of BrailleTable by symbol.
     * @param checkedBrailleTables
     * @return 
     */
    private static Map<Character, List<BrailleTable>> getCharacterBrailleTableMap(List<BrailleTable> checkedBrailleTables) {
        
        
        Map<Character, List<BrailleTable>> ctf = new HashMap();
        for (BrailleTable checkedBt : checkedBrailleTables) {
            for (Character c : UnicodeBraille.allSixDots()) {
                Character btChar = checkedBt.getChar(c);
                if (!ctf.containsKey(btChar)) {
                    ctf.put(btChar, new ArrayList<BrailleTable>());
                }
                ctf.get(btChar).add(checkedBt);
            }
        }
        return ctf;
    }

    
     /**
     * Get the @{link BrailleTable}s matching a given Braille String.
     * <p>
     * For a BrailleTable to be matched, every caracter in the Braille String
     * must be part of this BrailleTable, OR be absent in every other checked
     * BrailleTable.
     * <p>
     *
     * @param unknown
     * @param stopWhenUniq if true, stop as soon as there is only one solution.
     * @return
     */
    private static List<BrailleTable> exactMatches(String unknown, boolean stopWhenUniq, Map<Character, List<BrailleTable>> characterBrailleTableMap) {
        List<BrailleTable> exactMatches = null;
        for (int i = 0; i < unknown.length(); i++) {
            if ((stopWhenUniq) && (exactMatches != null) && (exactMatches.size() == 1)) {
                break;
            }
            int codePoint = unknown.codePointAt(i);
            if (characterBrailleTableMap.containsKey((char) codePoint)) {
                if (exactMatches == null) {
                    exactMatches = characterBrailleTableMap.get((char) codePoint);
                } else {
                    List<BrailleTable> filteredMatches = new ArrayList<>();
                    for (BrailleTable bt : exactMatches) {
                        if (characterBrailleTableMap.get((char) codePoint).contains(bt)) {
                            filteredMatches.add(bt);
                        }
                    }
                    exactMatches = filteredMatches;
                }
            }
        }
        return exactMatches;
    }

    Map<Character, List<BrailleTable>> characterBrailleTableMap;

    public ExactBrailleTableGuesser(List<BrailleTable> checkedBrailleTables) {
        this.characterBrailleTableMap = getCharacterBrailleTableMap(checkedBrailleTables);
    }

    public List<BrailleTable> exactMatches(String unknown, boolean stopWhenUniq) {
        return exactMatches(unknown, stopWhenUniq, characterBrailleTableMap);
    }
    
    
    /**
     * debug
     * @param args
     * @throws BrailleTableException 
     */
    public static void main(String[] args) throws BrailleTableException {
        List<BrailleTable> checkedBts = new ArrayList<>();

        for (String tbName : SystemBrailleTables.getNames()) {
            BrailleTable checkedBt;
            try {
                checkedBt = BrailleTables.forName(tbName);
                checkedBts.add(checkedBt);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        ExactBrailleTableGuesser ebtg = new ExactBrailleTableGuesser(checkedBts);

//        for (Character c : ctf.keySet()) {
//            System.err.print(c);
//            System.err.print(" : ");
//            System.err.print(ctf.get(c).size());
//            System.err.print(" : ");
//            for (BrailleTable bt : ctf.get(c)) {
//                System.err.print(bt.getName());
//                System.err.print(" ");
//            }
//            System.err.println();
//        }
        String unknown = Test.randomUnicodeBraille(30, false);
        System.err.println(unknown);

        for (String tbName : SystemBrailleTables.getNames()) {
            BrailleTable checkedBt;
            try {
                checkedBt = BrailleTables.forName(tbName);
                Converter btconv = new Converter(BrailleTables.unicodeBrailleTable(), checkedBt);
                String otherString = btconv.convert(unknown);

                List<BrailleTable> exactMatches = ebtg.exactMatches(otherString, true);

                System.err.println("matching " + otherString + " : ");
                System.err.print("-> ");
                for (BrailleTable bt : exactMatches) {
                    System.err.print(bt.getName() + " ");
                }
                System.err.println("");

            } catch (BrailleTableException ex) {
                Logger.getLogger(ExactBrailleTableGuesser.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(ExactBrailleTableGuesser.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }
}
