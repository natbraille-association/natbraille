/*
 * NatBraille - BrailleTable - An universal Translator
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.brailletable;

import java.util.HashMap;
import java.util.Map;
import org.xnap.commons.i18n.I18n;
import org.xnap.commons.i18n.I18nFactory;

/**
 * String and Character converter from one {@link BrailleTable} to another.
 * TODO : important javadoc.
 * <p>
 * @author vivien
 */
public class Converter {

    private I18n i18n = I18nFactory.getI18n(BrailleTable.class);

    public static class Config {

        private Boolean copyMissingSource = true;
        private Boolean copyMissingDest = false;
        private Boolean spaceIsEmptyBrailleCell = true;

        public void setCopyMissingDest(Boolean copyMissingDest) {
            this.copyMissingDest = copyMissingDest;
        }

        public void setCopyMissingSource(Boolean copyMissingSource) {
            this.copyMissingSource = copyMissingSource;
        }

        public void setSpaceIsEmptyBrailleCell(Boolean spaceIsEmptyBrailleCell) {
            this.spaceIsEmptyBrailleCell = spaceIsEmptyBrailleCell;
        }

    };

    private final BrailleTable sourceTable;
    private final BrailleTable destTable;
    private final Map<Character, Character> cache = new HashMap<>();
    private final Config config;

    /**
     * get the converter configuration.
     * {@link Converter.Config}
     * <p>
     * @return
     */
    public Config getConfig() {
        return config;
    }

    public Converter(BrailleTable sourceTable, BrailleTable destTable) {
        this.config = new Config();
        this.sourceTable = sourceTable;
        this.destTable = destTable;
    }

    /**
     * Convert a Character from source Braille table to dest Braille table.
     * <p>
     * If copyMissingSource (getConfig().copyMissingSource) is set to false
     * (default), a missing character in source Braille table will throw an
     * exception. Otherwise, the character will be be passed as is to the
     * destination.
     * <p>
     * If copyMissingSource (getConfig().copyMissingSource) is set to false
     * (default) a missing character in destination Braille table will throw an
     * exception. Otherwise, the character will be be copied as is to the
     * destination.
     * <p>
     * If spaceIsEmptyBrailleCell (getConfig().spaceIsEmptyBrailleCell) is set to true,
     * spaces are interpreted as empty braille cells.
     * @param c the braille Character to be converted
     * @return the converted Character
     * @throws BrailleTableException
     */
    public Character convert(Character c) throws BrailleTableException {
        Character destCharacter;
        if (cache.containsKey(c)) {
            destCharacter = cache.get(c);
        } else {
            Character uc = sourceTable.toUnicodeBraille(c);            
            if (uc == null) {
                if (config.spaceIsEmptyBrailleCell && (c == 0x20)){
                    //?
                    destCharacter = destTable.getChar(UnicodeBraille.FIRST); // first is the braille space                
                } else if (config.copyMissingSource) {
                    destCharacter = c;
                } else {
                    throw new BrailleTableException(i18n.tr("missing char #{0} in source table", (int) c));
                }
            }  else {
                Character dc = destTable.getChar(uc);
                if (dc == null) {
                    if (config.copyMissingDest) {
                        destCharacter = uc;
                    } else {
                        throw new BrailleTableException(i18n.tr("missing char #{0} (pt{1}) in destination table", (int) uc,UnicodeBraille.toPoints(uc)));
                    }
                } else {
                    destCharacter = dc;
                }

            }
            cache.put(c, destCharacter);
        }
        return destCharacter;
    }

    /**
     * Convert a String from source Braille table to dest Braille table.
     * <p>
     * @param s the braille string to be converted
     * @return the converted String
     * @throws Exception
     */
    public String convert(String s) throws Exception {
        char sca[] = s.toCharArray();
        char resu[] = new char[sca.length];
        for (int i = 0; i < sca.length; i++) {
            resu[i] = convert(sca[i]);
        }
        return new String(resu);
    }

}
