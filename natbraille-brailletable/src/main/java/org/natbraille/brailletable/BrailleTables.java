/*
 * NatBraille - BrailleTable - An universal Translator
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.brailletable;

import java.io.InputStream;
import org.natbraille.brailletable.impl.BrailleTableFile;
import org.natbraille.brailletable.impl.BrailleTableImpl;
import org.natbraille.brailletable.impl.EntitiesBrailleTableFile;
import org.natbraille.brailletable.system.SystemBrailleTables;

/**
 * Convenience static methods for {@link BrailleTable} creation.
 * <p>
 * Builds from name, input stream to an entity file (i.e. xxx.ent) and empty
 * table.
 * <p>
 * @author vivien
 */
public class BrailleTables {

    /**
     * Create a {@link BrailleTable} from its name.
     * <p>
     * @param name. name of the system braille table (ie.
     * SystemBrailleTables.CodeUS, SystemBrailleTables.TbFr2007)
     * <p>Braille table definition will be looked for in org.natbraille.brailletable.system.
     * @return the Braille table
     * @throws BrailleTableException
     */
    public static BrailleTable forName(String name) throws BrailleTableException {
        return SystemBrailleTables.forName(name);
    }

    /**
     * Get the Unicode {@link BrailleTable}
     * <p>
     * @return the Braille table
     * @throws BrailleTableException
     */
    public static BrailleTable unicodeBrailleTable() throws BrailleTableException {
        return SystemBrailleTables.forName(SystemBrailleTables.BrailleUTF8);        
    }

    /**
     * Get a {@link BrailleTable} from an .ent stream
     * <p>
     * @param entitiesIs the stream denoting the .ent file
     * @return the Braille table
     * @throws BrailleTableException
     */
    public static BrailleTable fromEntitiesStream(InputStream entitiesIs) throws BrailleTableException, Exception {
        return EntitiesBrailleTableFile.fromInputStream(entitiesIs);
    }

    /**
     * Get a {@link BrailleTable} from an .ent stream
     * <p>
     * @param entitiesIs the stream denoting the .ent file
     * @return the Braille table
     * @throws BrailleTableException
     */
    public static BrailleTable fromUnicodeStream(InputStream entitiesIs) throws BrailleTableException, Exception {
        return BrailleTableFile.fromInputStream(entitiesIs);
    }
    /**
     * Get an empty {@link BrailleTable}
     * <p>
     * @return an empty Braille table
     */
    public static BrailleTable empty() {
        return new BrailleTableImpl();
    }

}
