/*
 * NatBraille - BrailleTable - An universal Translator
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.brailletable.system;

import org.natbraille.brailletable.BrailleTable;
import org.natbraille.brailletable.BrailleTableException;
import org.natbraille.brailletable.impl.BrailleTableFile;

/**
 * static method for creating a Braille Table from its name.
 * <p>
 * the "name" the name of the ".ent" file without extension.
 * <p>
 * File is read from the package resources.
 * <p>
 * @author vivien
 */
public class SystemBrailleTables {

    public static final String BrailleUTF8 = "brailleUTF8";
    public static final String CBISF = "CBISF";
    public static final String brailleUTF8web = "brailleUTF8web";
    public static final String CodeUS = "CodeUS";
    public static final String IBM850 = "IBM850";
    public static final String DuxTbFr2007 = "DuxTbFr2007";
    public static final String CBFr1252 = "CBFr1252";
    public static final String TbFr2007 = "TbFr2007";
    public static final String DuxCBfr1252 = "DuxCBfr1252";
    public static final String MixedFrUnicode = "MixedFrUnicode";
    private static final String names[] = {
        SystemBrailleTables.BrailleUTF8,
        SystemBrailleTables.CBFr1252,
        SystemBrailleTables.CBISF,
        SystemBrailleTables.CodeUS,
        SystemBrailleTables.DuxCBfr1252,
        SystemBrailleTables.DuxTbFr2007,
        SystemBrailleTables.IBM850,
        SystemBrailleTables.TbFr2007,
        SystemBrailleTables.brailleUTF8web

    };

    /**
     * Get all system {@link BrailleTable}s names
     *
     * @return array of names
     */
    public static String[] getNames() {
        return names;
    }

    /**
     * Create a Braille Table from its name.
     * <p>
     * @param name name of the system braille table (ie.
     * SystemBrailleTables.CodeUS, SystemBrailleTables.TbFr2007)
     * @return
     * @throws BrailleTableException
     */
    public static BrailleTable forName(String name) throws BrailleTableException {
        BrailleTable brailleTable = BrailleTableFile.fromInputStream(
                SystemBrailleTables.class.getResourceAsStream(name)
        );
        if (brailleTable == null) {            
            throw new BrailleTableException("Cannot find a Braille Table named '"+name+"' in resources");
        }
        brailleTable.setName(name);
        return brailleTable;
    }

}
