/*
 * NatBraille - BrailleTable - An universal Translator
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.brailletable;

/**
 * Static methods for braille character representation
 * other than unicode.
 * <p>number based representation : "136"
 * <p>boolean array representation : new Boolean[]{true,false,false...
 * @author vivien
 */
public class UnicodeBraille {

    /**
     * position of the first (6 dot) braille unicode char
     */
    public final static char FIRST = 0x2800;
    /**
     * position of the last (6 dot) braille unicode char
     */
    public final static char LAST_6DOTS = 0x283F;
    /**
     * position of the last (8 dot) braille unicode char
     */
    public final static char LAST_8DOTS = 0x28FF;

    /**
     * number of six 6 cells
     */
    public final static int LENGTH_6DOTS = 1 + 0x283F - 0x2800;
    /**
     * number of 6/8 dots cells
     */
    public final static int LENGTH_8DOTS = 1 + 0x28FF - 0x2800;

    /**
     * Get the unicode Character matching a ordered number based representation.
     * <p>
     * <ul><li>1 4</li>
     * <li>2 5</li>
     * <li>3 6</li>
     * <li>7 8</li></ul>
     * <p>
     * @param braillePoints the dots numers (i.e. "146" ; "17")
     * @return
     */
    public static Character forPoints(String braillePoints) {
        Boolean pts[] = new Boolean[]{false, false, false,
                                      false, false, false,
                                      false, false};
        for (char c : braillePoints.toCharArray()) {
            switch (c) {
                case '1':
                    pts[0] = true;
                    break;
                case '2':
                    pts[1] = true;
                    break;
                case '3':
                    pts[2] = true;
                    break;
                case '4':
                    pts[3] = true;
                    break;
                case '5':
                    pts[4] = true;
                    break;
                case '6':
                    pts[5] = true;
                    break;
                case '7':
                    pts[6] = true;
                    break;
                case '8':
                    pts[7] = true;
                    break;
            }
        }
        return forPoints(pts);
    }

    /**
     * Get the unicode Character matching a boolean dots array representation.
     * <p>
     * array is organized as follows (dot:position in array)
     * <ul><li>1:0 4:3</li>
     * <li>2:1 5:4</li>
     * <li>3:2 6:5</li>
     * <li>7:6 8:7</li></ul>
     * <p>
     * @param pts an array of 8 booleans, each for one dot
     * @return
     */
    public static Character forPoints(Boolean pts[]) {
        char code = 10240;
        for (int ptnum = 0; ptnum < pts.length; ptnum++) {
            int val = pts[ptnum] ? 1 : 0;
            code += val << ptnum;
        }
        Character c = code;
        return c;
    }

    /**
     * Is this character a 6 dots unicode Characters ?
     * <p>
     * @param c the unicode braille character
     * @return
     */
    public static boolean isSixDots(Character c) {
        return (c >= FIRST) && (c <= LAST_6DOTS);
    }

    /**
     * Is this character a 6 or 8 dots unicode Characters ?
     * <p>
     * @param c the unicode braille character
     * @return
     */
    public static boolean isEightDots(Character c) {
        return (c >= FIRST) && (c <= LAST_6DOTS);
    }

    /**
     * Get the list of all 6 dots unicode Characters
     * <p>
     * @return
     */
    public static Character[] allSixDots() {
        Character chs[] = new Character[LENGTH_6DOTS];
        for (int c = 0; c < UnicodeBraille.LENGTH_6DOTS; c++) {
            chs[c] = (char) (UnicodeBraille.FIRST + c);
        }
        return chs;
    }

    /**
     * Get the list of all 8 dots unicode Characters
     * <p>
     * @return
     */
    public static Character[] allEightDots() {
        Character chs[] = new Character[LENGTH_8DOTS];
        for (int c = 0; c < UnicodeBraille.LENGTH_8DOTS; c++) {
            chs[c] = (char) (UnicodeBraille.FIRST + c);
        }
        return chs;
    }

    public static String toPoints(Character c) {
        StringBuilder sb = new StringBuilder();
        int num = c;
        for (int p = 0; p < 8; p++) {
            if ((num & (1 << p)) != 0) {
                sb.append(p + 1);
            }
        }
        return sb.toString();
    }

    public static Boolean[] toBooleanPoints(Character c) {
        Boolean pts[] = new Boolean[]{false, false, false,
                                      false, false, false,
                                      false, false};
        int num = c;
        for (int p = 0; p < 8; p++) {
            if ((num & (1 << p)) != 0) {
                pts[p] = true;
            }
        }
        return pts;
    }
}
