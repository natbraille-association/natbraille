# Natbraille 

Natbraille is a software for transcribing to and from French Braille.

Natbraille handles Braille codes for abbreviated Braille, mathematics, and chemistry and page layout with a focus on adhering to standards.

Natbraille can process many input formats, among which are OpenDocument (LibreOffice), MSWord, plain text,  RTF

Natbraille can be used as a command line tool, as a server for document batch processing or as a server for realtime editing and braille translation using 
[natbraille-editor](https://framagit.org/natbraille-association/natbraille-editor) as a web interface.

# License

natbraille-editor is free software, distributed under the terms of the GNU GENERAL PUBLIC LICENSE Version 3. 
For more information, see the file COPYING. 

## Source folder organisation

### Subprojects

- `natbraille-core` is the braille translation engine 
  - `natbraille-brailletable` mappings of braille characters sets 
  - `natbraille-writer2latex` OpenDocument to xhtml translation
  - `natbraille-nonregression` is used for non-regression testing
  - `natbraille-soffice` encapsulation of libreoffice for any format to OpenDocument conversions
  - `gettext-common` a fork of [gettext-commons](https://github.com/jgettext/gettext-commons)
- `natbraille-cli` is a cli interface to natbraille-core
- `natbraille-http` exposes a http batch braille translator with its web user-interface
- `natbraille-editor-server` the backend for the interactive web editor using [natbraille-editor](https://framagit.org/natbraille-association/natbraille-editor) as a web interface.

Note : `natbraille-editor-server` does not make use of `natbraille-writer2latex` and `natbraille-soffice`, the latest being replaced by [unoserver](https://github.com/unoconv/unoserver) ; 
those subproject are nevertheless needed for building `natbraille-editor-server` because `natbraille-core` depends on them.    

### Tools

- `natbraille-pdf` produce an accessible pdf from braille plain text.
- `natbraille-font` produces braille font files with configurable dimensions and character mapping
- `natbraille-mail` provides a Braille translation service via email 

#### Other

- `natbraille-starmath` is a starmath5 (math input language used by libreoffice math) parser to/from mathml converter
- `natbraille-server` is a local http server around natbraille-core mainly used to provide `natbraille-starmath` service 

### Exploration 
 
- `natbraille-giacbraille` is an integration of mathematical braille in [giac](https://www-fourier.ujf-grenoble.fr/~parisse/giac_fr.html)
- `natbraille-bxd` is an effort in exploring the Duxburry Braille Translator (*.dxb,*.dxt) format
- `natbraille-ocr` is an attempt at Braille OCR 
- `natbraille-math-interactive-tool` is a specific tool for testing braille to and from math translation

### Building add-on tool

- `buildSrc/` contains gradle building tools, including an encapsulation of the GNU GetText command line

## How to build

requires msgfmt of GNU gettext (on debian `apt install msgfmt`)

`gradle` is used for building. The `settings.gradle.kts` configuration makes gradle build every project by default including
`natbraille-cli`, `natbraille-http` and `natbraille-editor-server` via:

```
gradle build
```

Specific instruction for installing, configuring and running the `natbraille-editor-server` backend for the new (2024) interactive web editor [natbraille-editor](https://framagit.org/natbraille-association/natbraille-editor) as a web interface.  [natbraille-editor](https://framagit.org/natbraille-association/natbraille-editor), can be found in `natbraille-editor-server/`

### Notes

Sources of the previous (stable) java installable desktop GUI can be found in the [Nat-v2.2rc1 repository](https://framagit.org/natbraille-association/Nat-v2.2rc1)   

# debian specific

## unoserver

    # setup dir
    mkdir unoserver 
    cd unoserver

    # python virtual env
    sudo apt install python3-venv 
    python3 -m venv .
    source ./bin/activate

    # pip install uno unoserver
    

--- git clone https://github.com/unoconv/unoserver
--- cd unoserver
