/*
 * NatBraille Mail - mail Braille transcription
 * Copyright (C) 2014 Vivien Guillet
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.mail.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.URLName;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.MissingOptionException;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.Parser;
import org.apache.commons.cli.PosixParser;
import org.natbraille.core.filter.NatFilterException;
import org.natbraille.mail.MailConfig;
import org.natbraille.mail.MessageDaemon;
import org.natbraille.mail.MessagePoller;

/**
 * Fetch mails from a mailbox, translate attachements, reply with transcriptions
 * periodically.
 *
 *
 * @author vivien
 */
public class BrailleMailCli {
    // TODO super.setOptionComparator(comparator); 

    private static final char O_HELP = 'h';
    private static final char O_URL_INPUT = 'l';
    private static final char O_URL_OUTPUT = 'r';
    private static final char O_FROM = 'f';
    private static final char O_MESSAGE_TEMPLATE = 'm';
    private static final char O_IMAP_SSL = 's';
    private static final char O_SMTP_STARTTLS = 't';
    private static final char O_SLEEP_DURATION = 'x';
    private static final char O_NAT_FILTER_OPTIONS = 'o';

    private static final Options options = new Options()
            .addOption(OptionBuilder.isRequired().withLongOpt("input").withArgName("URL").hasArg().withDescription("mail input, e.g. imap://user:pwd@server:port").create(O_URL_INPUT))
            .addOption(OptionBuilder.isRequired().withLongOpt("output").withArgName("URL").hasArg().withDescription("mail output, e.g. smtp://user:pwd@server:port").create(O_URL_OUTPUT))
            .addOption(OptionBuilder.isRequired().withLongOpt("from").withArgName("email").hasArg().withDescription("'From' field in replies").create(O_FROM))
            .addOption(OptionBuilder.withLongOpt("message").withArgName("filename").hasArg().withDescription("reply message template").create(O_MESSAGE_TEMPLATE))
            .addOption(OptionBuilder.withLongOpt("filter").withArgName("filename").hasArg().withDescription("default filter option").create(O_NAT_FILTER_OPTIONS))
            .addOption(OptionBuilder.withLongOpt("sleep").withArgName("seconds").hasArg().withDescription("time of sleep after one mailbox check (default 10s)").create(O_SLEEP_DURATION))
            .addOption(OptionBuilder.withLongOpt("ssl").withDescription("use ssl for imap").create(O_IMAP_SSL))
            .addOption(OptionBuilder.withLongOpt("starttls").withDescription("use starttls for smtp").create(O_SMTP_STARTTLS))
            .addOption(OptionBuilder.withLongOpt("help").withDescription("this help message").create(O_HELP));

    /**
     * option comparator for help options display order.
     */
    static class optionComparator implements Comparator<Option> {

        private static final List<Character> optionsOrder = Arrays.asList(
                O_URL_INPUT,
                O_URL_OUTPUT,
                O_FROM,
                O_MESSAGE_TEMPLATE,
                O_NAT_FILTER_OPTIONS,
                O_IMAP_SSL,
                O_SMTP_STARTTLS,
                O_SLEEP_DURATION,
                O_HELP
        );

        public optionComparator() {
        }

        @Override
        public int compare(Option option1, Option option2) {
            // get option letter
            char oc1 = (char) option1.getId();
            char oc2 = (char) option2.getId();
            // get letter position
            Integer poso1 = optionsOrder.indexOf(oc1);
            Integer poso2 = optionsOrder.indexOf(oc2);
            // compare positions
            return poso1.compareTo(poso2);

        }

    }

    private static boolean hasHelp(String args[]) {
        for (String arg : args) {
            switch (arg) {
                case "-h":
                case "--help":
                    return true;
            }
        }
        return false;

    }

    public static void main(String[] args) {

        Parser parser = new PosixParser();
        // parse the command line arguments
        HelpFormatter formatter = new HelpFormatter();
        formatter.setOptionComparator(new optionComparator());
        if (hasHelp(args)) {
            formatter.printHelp(BrailleMailCli.class.getCanonicalName(), options, true);
            System.exit(0);
        }
        try {
            CommandLine cl = parser.parse(options, args);
            URLName inUrlName = new URLName(cl.getOptionValue(O_URL_INPUT));
            URLName outUrlName = new URLName(cl.getOptionValue(O_URL_INPUT));
            InternetAddress from = new InternetAddress(cl.getOptionValue(O_FROM));
            MailConfig mailConfig = new MailConfig(inUrlName, outUrlName, from);
            if (cl.hasOption(O_IMAP_SSL)) {
                mailConfig.useEncryptedImap();
            }
            if (cl.hasOption(O_SMTP_STARTTLS)) {
                mailConfig.useSmtpStartTLS();
            }
            if (cl.hasOption(O_MESSAGE_TEMPLATE)) {
                mailConfig.setMessageTemplate(new FileInputStream(new File(cl.getOptionValue(O_MESSAGE_TEMPLATE))));
            }
            if (cl.hasOption(O_NAT_FILTER_OPTIONS)) {
                mailConfig.setDefaultNatFilterOptions(new FileInputStream(new File(cl.getOptionValue(O_NAT_FILTER_OPTIONS))));
            }

            long sleepDuration = 10;
            if (cl.hasOption(O_SLEEP_DURATION)) {
                sleepDuration = Integer.parseInt(cl.getOptionValue(O_SLEEP_DURATION));
            }

            MessagePoller messagePoller = new MessagePoller(mailConfig);

            MessageDaemon mailDaemon = new MessageDaemon(messagePoller, sleepDuration);
            mailDaemon.spawn();

        } catch (MissingOptionException ex) {
            formatter.printHelp(BrailleMailCli.class.getCanonicalName(), options, true);

        } catch (ParseException | AddressException | FileNotFoundException ex) {
            formatter.printHelp(BrailleMailCli.class.getCanonicalName(), options, true);
        } catch (NatFilterException | IOException ex) {
            Logger.getLogger(BrailleMailCli.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
