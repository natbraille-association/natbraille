/*
 * NatBraille Mail - mail Braille transcription
 * Copyright (C) 2014 Vivien Guillet
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.mail.test;

import java.io.IOException;
import javax.mail.URLName;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import org.natbraille.core.filter.NatFilterException;
import org.natbraille.mail.MailConfig;
import org.natbraille.mail.MessageDaemon;
import org.natbraille.mail.MessagePoller;
import org.natbraille.mail.ui.BrailleMailCli;

/**
 * Tests
 *
 * @author vivien
 */
public class Test {

    public static void startByMainHelp() {
        BrailleMailCli.main(new String[]{"--help"});
    }

    public static void startByMain() {

        String argz[] = new String[]{
            "--input", "imap://merdikzbsidersfameux:bonnardo@imap.free.fr",
            "--output", "smtp://smtp-auth.sfr.fr",
            "--from", "do-not-reply@noreply.com",
            "--sleep", "10",
            "--ssl",};
        BrailleMailCli.main(argz);

    }

    public static void startByApi() throws AddressException, NatFilterException, IOException {
        MailConfig mailConfig = new MailConfig(
                new URLName("imap://merdikzbsidersfameux:bonnardo@imap.free.fr"),
                new URLName("smtp://smtp-auth.sfr.fr"),
                new InternetAddress("do-not-reply@noreply.com")
        );
        mailConfig.useEncryptedImap();

        // set java properties
        /*
         Properties props = new Properties();
         props.set("mail.imap... ", "value);
         mailConfig.setProperties();
         */
        // set nat filter options
        /*
         NatFilterOptions natFilterOptions = new NatFilterOptions();
         natFilterOptions.setOption(NatFilterOption.LAYOUT, null);        
         mailConfig.setDefaultNatFilterOptions(natFilterOptions);
         */
        int delay = 10;
        MessagePoller messagePoller = new MessagePoller(mailConfig);
        MessageDaemon mailDaemon = new MessageDaemon(messagePoller, delay);
        mailDaemon.spawn();

    }

    /**
     * test command line and api
     *
     * @param args
     * @throws AddressException
     * @throws NatFilterException
     * @throws IOException
     */
    public static void main(String[] args) throws AddressException, NatFilterException, IOException {

        int test = 2;
        switch (test) {
            case 1:
                startByMainHelp();
                break;
            case 2:
                startByMain();
                break;
            case 3:
                startByApi();
                break;
        }

    }

}
