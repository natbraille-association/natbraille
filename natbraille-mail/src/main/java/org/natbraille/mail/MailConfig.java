/*
 * NatBraille Mail - mail Braille transcription
 * Copyright (C) 2014 Vivien Guillet
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.mail;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.Security;
import java.text.MessageFormat;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.URLName;
import javax.mail.internet.InternetAddress;
import org.apache.commons.io.IOUtils;
import org.natbraille.core.filter.NatFilterException;
import org.natbraille.core.filter.options.NatFilterOptions;

/**
 *
 * @author vivien
 */
public class MailConfig {

    private final URLName inURLName;
    private final URLName outURLName;
    private final InternetAddress addressFrom;
    private final Properties props = new Properties(System.getProperties());
    private String messageTemplate = "{0}";
    private final NatFilterOptions natFilterOptions;

    /**
     * New mail configuration.
     *
     * @param inURLName i.e. imap://user:password@server:port
     * @param outURLName i.e. smtp://server
     * @param addressFrom i.e. no-reply@noreply.com
     */
    public MailConfig(URLName inURLName, URLName outURLName, InternetAddress addressFrom) throws NatFilterException {
        setUrl(inURLName);
        setUrl(outURLName);
        this.inURLName = inURLName;
        this.outURLName = outURLName;
        this.addressFrom = addressFrom;
        this.natFilterOptions = new NatFilterOptions();
    }

    private final static String MessageTemplateResourceName = "defaultMessage.txt";

    /**
     * Set the content of the mail message template. template uses {0} for
     * result list.
     *
     * @param messageTemplateStream
     */
    public void setMessageTemplate(InputStream messageTemplateStream) {
        try {
            messageTemplate = IOUtils.toString(messageTemplateStream, "UTF8");
        } catch (IOException ex) {
            Logger.getLogger(MailConfig.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Set the default/base {@link NatFilterOptions} for transcription.
     *
     * @param natFilterOptionsStream
     * @throws org.natbraille.core.filter.NatFilterException
     * @throws java.io.IOException
     */
    public void setDefaultNatFilterOptions(FileInputStream natFilterOptionsStream) throws NatFilterException, IOException {
        natFilterOptions.setOptions(natFilterOptionsStream);
    }

    /**
     * Set the default/base {@link NatFilterOptions} for transcription.
     *
     * @param NatFilterOptions
     * @throws java.io.IOException
     * @throws org.natbraille.core.filter.NatFilterException
     */
    public void setDefaultNatFilterOptions(NatFilterOptions NatFilterOptions) throws IOException, NatFilterException {
        natFilterOptions.setOptions(NatFilterOptions);
    }

    /**
     * Get the content of the mail message using the template.
     *
     * @param objects
     * @return
     */
    public String getMessage(Object[] objects) {
        if (messageTemplate == null) {
            messageTemplate = "{0}";
        }
        if ("{0}".equals(messageTemplate)) {
            // try to set default from MessageTemplateResourceName
            InputStream is = MailConfig.class.getResourceAsStream(MessageTemplateResourceName);
            setMessageTemplate(is);
        }
        return MessageFormat.format(messageTemplate, objects);
    }

    /**
     * use SSL for imap. Set properties and add ssl provider for use of ssl with
     * imap.
     */
    public void useEncryptedImap() {
        // configure the jvm to use the jsse security.

        // Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
        // set this session up to use SSL for IMAP connections
        props.setProperty("mail.imap.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        // don't fallback to normal IMAP connections on failure.
        props.setProperty("mail.imap.socketFactory.fallback", "false");
        // use the simap port for imap/ssl connections.
        if ((inURLName == null) || (inURLName.getPort() == -1)) {
            props.setProperty("mail.imap.socketFactory.port", "993");
        }
    }

    /**
     * use startTLS for smtp.
     */
    public void useSmtpStartTLS() {
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");

    }

    /**
     * see :
     * <p>
     * https://javamail.java.net/nonav/docs/api/com/sun/mail/imap/package-summary.html
     * <p>
     * https://javamail.java.net/nonav/docs/api/com/sun/mail/smtp/package-summary.html
     *
     * @author vivien
     * @param userProps
     */
    public void setProperties(Properties userProps) {
        props.putAll(userProps);

    }

    private void setUrl(URLName urlName) {
        if (null != urlName.getProtocol()) {
            switch (urlName.getProtocol()) {
                case "imap":
                    if (urlName.getHost() != null) {
                        props.setProperty("mail.imap.host", urlName.getHost());
                    }
                    if (urlName.getPort() >= 0) {
                        props.setProperty("mail.imap.port", Integer.toString(urlName.getPort()));
                    }
                    break;
                case "smtp":
                    if (urlName.getHost() != null) {
                        props.setProperty("mail.smtp.host", urlName.getHost());
                    }
                    if (urlName.getPort() >= 0) {
                        props.setProperty("mail.smtp.port", Integer.toString(urlName.getPort()));
                    }
                    break;
                default:
                    Logger.getLogger(MessageReplier.class
                            .getName()).log(Level.SEVERE, "cannot use protocol {0}", urlName.getProtocol());

                    break;
            }
        }
    }

    /**
     * Create a new @{link Session} from this MailConfig.
     *
     * @return
     */
    public Session newPasswordAuthentificationSession() {
        Logger.getLogger(MessageReplier.class
                .getName()).log(Level.FINE, "building session...");
        // Get the Session object.
        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        if (("smtp").equals(getRequestingProtocol())) {
                            return new PasswordAuthentication(outURLName.getUsername(), outURLName.getPassword());
                        } else {
                            return new PasswordAuthentication(inURLName.getUsername(), inURLName.getPassword());
                        }
                    }

                });
        return session;
    }

    public InternetAddress getAddressFrom() {
        return addressFrom;
    }

    public URLName getInURLName() {
        return inURLName;
    }

    public URLName getOutURLName() {
        return outURLName;
    }

    public Properties getProperties() {
        return props;
    }

    public NatFilterOptions getNatFilterOptions() {
        return natFilterOptions;
    }

}
