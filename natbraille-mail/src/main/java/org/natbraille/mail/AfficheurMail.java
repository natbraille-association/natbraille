/*
 * NatBraille Mail - mail Braille transcription
 * Copyright (C) 2014 Vivien Guillet
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.mail;

import java.util.ArrayList;
import java.util.List;
import org.natbraille.core.gestionnaires.Afficheur;
import org.natbraille.core.gestionnaires.LogMessage;

/**
 *
 * @author vivien
 */
public class AfficheurMail extends Afficheur {

    private static final List<LogMessage> logMessages = new ArrayList<>();

    @Override
    public void afficheMessage(LogMessage logMessage) {
        logMessages.add(logMessage);
    }

    public String getMailLog() {
        StringBuilder sb = new StringBuilder();
        for (LogMessage logMessage : logMessages) {
            sb.append("[");
            sb.append(logMessage.getDate());
            sb.append("]");
            sb.append("[");
            sb.append(logMessage.getLevel());
            sb.append("]");
            sb.append("[");
            sb.append(logMessage.getKind());
            sb.append("]");
            sb.append(" ");
            sb.append(logMessage.getTrContents(getI18n()));
            sb.append("\n");
        }
        return sb.toString();
    }

}
