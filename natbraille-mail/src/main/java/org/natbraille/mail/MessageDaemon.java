/*
 * NatBraille Mail - mail Braille transcription
 * Copyright (C) 2014 Vivien Guillet
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.mail;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Periodically run a {@link MessagePoller}.
 * <p>
 * internally uses a {@link ScheduledThreadPoolExecutor}.
 *
 * @author vivien
 */
public class MessageDaemon {

    private final ScheduledThreadPoolExecutor exec;
    private final MessagePoller messagePoller;
    private final long sleepDuration;

    public MessageDaemon(MessagePoller messagePoller, long sleepDuration) {
        exec = new ScheduledThreadPoolExecutor(1);
        this.messagePoller = messagePoller;
        this.sleepDuration = sleepDuration;
    }

    public void spawn() {
        // wait 0 seconds. Then run messagePoller ; wait sleepDuration, loop.
        exec.scheduleWithFixedDelay(messagePoller, 0, sleepDuration, TimeUnit.SECONDS);
    }
}
