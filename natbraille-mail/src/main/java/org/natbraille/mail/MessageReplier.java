/*
 * NatBraille Mail - mail Braille transcription
 * Copyright (C) 2014 Vivien Guillet
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.mail;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.DataHandler;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;
import javax.xml.transform.TransformerException;
import org.natbraille.brailletable.BrailleTableException;
import org.natbraille.brailletable.BrailleTables;
import org.natbraille.brailletable.system.SystemBrailleTables;
import org.natbraille.core.document.NatDocument;
import org.natbraille.core.document.NatDocumentException;
import org.natbraille.core.filter.NatFilterConfigurator;
import org.natbraille.core.filter.NatFilterException;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.core.filter.transcriptor.Transcriptor;
import org.natbraille.core.gestionnaires.AfficheurAnsiConsole;
import org.natbraille.core.gestionnaires.GestionnaireErreur;
import org.natbraille.core.gestionnaires.LogLevel;
import org.natbraille.core.gestionnaires.MessageKind;

/**
 * Reply to a {
 *
 * @Message} with converted attachements.
 *
 * @author vivien
 */
public class MessageReplier implements Runnable {

    private final Message requestMessage;
    private final InternetAddress fromAddress;
    private final MailConfig mailConfig;

    public MessageReplier(MailConfig mailConfig, Message requestMessage, InternetAddress fromAddress) {
        this.mailConfig = mailConfig;
        this.requestMessage = requestMessage;
        this.fromAddress = fromAddress;
    }

    @Override
    public void run() {
        try {
            List<NatDocument> src = getNatDocumentsFromMessage(requestMessage);
            List<NatDocument> resu = transcribeDocuments(mailConfig, src);
            Message reply = buildReplyMessage(mailConfig, requestMessage, resu, fromAddress);
            Transport.send(reply);
        } catch (IOException | MessagingException | NatFilterException e) {
            Logger.getLogger(MessageReplier.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    private static List<NatDocument> getNatDocumentsFromMessage(Message message) throws IOException, MessagingException {

        List<NatDocument> natDocuments = new ArrayList<>();

        Multipart multipart = (Multipart) message.getContent();
        // foreach part of multipart
        for (int i = 0; i < multipart.getCount(); i++) {
            try {
                BodyPart bodyPart = multipart.getBodyPart(i);
                String fileName = bodyPart.getFileName();
                if (!Part.ATTACHMENT.equalsIgnoreCase(bodyPart.getDisposition())
                        && ((fileName == null) || (fileName.isEmpty()))) {
                    continue; // dealing with attachments only
                }
                NatDocument doc = new NatDocument();
                doc.setOriginalFileName(fileName);
                // Do we know better about content-type than the sender ?
                // doc.forceContentType(bodyPart.getContentType());         
                // -> yes, we know what we accept.
                doc.setInputstream(bodyPart.getInputStream());
                Logger.getLogger(MessageReplier.class.getName()).log(Level.INFO, "- got document {0}", fileName);

                // add document to the document list
                natDocuments.add(doc);

            } catch (NatDocumentException ex) {
                Logger.getLogger(MessagePoller.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return natDocuments;
    }

    private static List<NatDocument> transcribeDocuments(MailConfig mailConfig, List<NatDocument> inDocs) throws NatFilterException, IOException, MessagingException {
        //System.exit(0);
        // regle le probleme du parser par défaut merdique.

        // create GestionnaireErreur and Afficheurs
        GestionnaireErreur ge = new GestionnaireErreur();

        AfficheurAnsiConsole afficheurConsole = new AfficheurAnsiConsole(LogLevel.NORMAL);
        afficheurConsole.setLogLevel(LogLevel.SILENT);
        afficheurConsole.setBlackList(MessageKind.STEP);
        ge.addAfficheur(afficheurConsole);

        AfficheurMail afficheurMail = new AfficheurMail();
        afficheurMail.setLogLevel(LogLevel.DEBUG);
        ge.addAfficheur(afficheurMail);

        // create nat filter options (TODO : get from title or mail part configuration)
        NatFilterOptions nfo = new NatFilterOptions();
        nfo.setOption(NatFilterOption.FORMAT_OUTPUT_BRAILLE_TABLE, SystemBrailleTables.TbFr2007);
        nfo.setOption(NatFilterOption.FORMAT_OUTPUT_ENCODING, "cp1252");
        nfo.setOption(NatFilterOption.MODE_LIT_FR_MAJ_DOUBLE, "true");
        nfo.setOption(NatFilterOption.MODE_G2, "true");

        /*	
         nfo.setOption(NatFilterOption.debug_document_write_temp_file,"true");
         nfo.setOption(NatFilterOption.debug_dyn_xsl_write_temp_file,"true");
         nfo.setOption(NatFilterOption.debug_document_show,"true");
         */
        nfo.setOptions(mailConfig.getNatFilterOptions());

        // build the transcritor
        NatFilterConfigurator natFilterConfigurator = new NatFilterConfigurator(nfo, ge);
        Transcriptor transcriptor = new Transcriptor(natFilterConfigurator);

        List<NatDocument> outDocs = new ArrayList<>();
        for (NatDocument inDoc : inDocs) {
            try {
                // transcribe this document
                NatDocument outDoc = transcriptor.run(inDoc);
                outDoc.setOriginalFileName(inDoc.getOriginalFileName() + ".txt");
                try {
                    outDoc.setBrailletable(BrailleTables.forName(nfo.getValue(NatFilterOption.FORMAT_OUTPUT_BRAILLE_TABLE)));
                } catch (BrailleTableException ex) {
                    Logger.getLogger(MessageReplier.class.getName()).log(Level.WARNING, null, ex);
                }
                // set charset from filter option.
                outDoc.forceCharset(Charset.forName(nfo.getValue(NatFilterOption.FORMAT_OUTPUT_ENCODING)));
                Logger.getLogger(MessageReplier.class.getName()).log(Level.INFO, "- translated {0} to {1}", new Object[]{inDoc.getOriginalFileName(), outDoc.getOriginalFileName()});
                // add doc to result list
                outDocs.add(outDoc);
            } catch (NatFilterException e) {
                Logger.getLogger(MessageReplier.class.getName()).log(Level.INFO, "- failed to translate {0}", new Object[]{inDoc.getOriginalFileName()});
            }
        }

        // add lines of log as an attachement.
        try {
            NatDocument logDocument = new NatDocument();
            logDocument.setString(afficheurMail.getMailLog());
            logDocument.forceCharset(Charset.forName("UTF8"));
            logDocument.setOriginalFileName("natbraille-log.txt");
            outDocs.add(logDocument);

        } catch (NatDocumentException ex) {
            Logger.getLogger(MessageReplier.class.getName()).log(Level.SEVERE, null, ex);
        }

        return outDocs;
    }

    private static Message buildReplyMessage(MailConfig mailConfig, Message originalMessage, List<NatDocument> natDocuments, InternetAddress fromAddress) throws MessagingException {

        // reply to sender only
        Message answerMessage = originalMessage.reply(false);

        // set sender adress
        answerMessage.setFrom(fromAddress);

        // multipart content
        Multipart multipart = new MimeMultipart();

        // build toc for attachements
        BodyPart messageBodyPart = new MimeBodyPart();
        StringBuilder sb = new StringBuilder();
        for (NatDocument natDocument : natDocuments) {
            sb.append("- ");
            sb.append(natDocument.getOriginalFileName());
            sb.append(", ");
            sb.append(natDocument.getOrGuessCharset());
            if (natDocument.getBrailletable() != null) {
                sb.append(", ");
                sb.append(natDocument.getBrailletable().getName());
            }
            sb.append("\n");
        }

        // build message
        messageBodyPart.setText(mailConfig.getMessage(new Object[]{sb.toString()}));
        multipart.addBodyPart(messageBodyPart);

        // create attachements
        for (NatDocument natDocument : natDocuments) {
            MimeBodyPart attachementBodyPart = new MimeBodyPart();
            String filename = natDocument.getOriginalFileName();
            try {
                attachementBodyPart.setFileName(filename);
                attachementBodyPart.setDescription("transcription Braille");
                String fullContentType = natDocument.getOrGuessContentType() + "; charset=" + natDocument.getOrGuessCharset();
                DataHandler ds = new DataHandler(natDocument.getString(), fullContentType);
                attachementBodyPart.setDataHandler(ds);
                multipart.addBodyPart(attachementBodyPart);
            } catch (NatDocumentException | IOException | TransformerException ex) {
                Logger.getLogger(MessageReplier.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        for (Address address : answerMessage.getAllRecipients()) {
            Logger.getLogger(MessageReplier.class.getName()).log(Level.INFO, "- sent to {0}", address.toString());
        }

        answerMessage.setContent(multipart);

        return answerMessage;
    }

}
