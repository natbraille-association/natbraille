/*
 * NatBraille Mail - mail Braille transcription
 * Copyright (C) 2014 Vivien Guillet
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.mail;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Address;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.InternetAddress;

/**
 *
 * @author vivien
 */
public class MessagePoller implements Runnable {

    private final InternetAddress addressFrom;
    private final MailConfig mailConfig;

    public MessagePoller(MailConfig mailConfig) {
        this.addressFrom = mailConfig.getAddressFrom();
        // this.mailPasswordAuthentificationSessionFactory = null;
        this.mailConfig = mailConfig;
    }

    private static Folder getDoneFolder(Folder inbox) throws MessagingException {

        String folderName = "INBOX.job-done";
        //Folder defaultFolder = store.getDefaultFolder();
        if (inbox.getFolder(folderName).create(Folder.HOLDS_MESSAGES)) {
            Logger.getLogger(MessageReplier.class.getName()).log(Level.FINE, "done folder created");
        } else {
            Logger.getLogger(MessageReplier.class.getName()).log(Level.FINE, "done folder not created");
        }
        return inbox.getFolder(folderName);

    }

    @Override
    public void run() {

        try {
            // Get the Session object.
            Session session = mailConfig.newPasswordAuthentificationSession();

            Store store = session.getStore("imap");
            Logger.getLogger(MessageReplier.class.getName()).log(Level.FINE, "connecting... ");
            store.connect();

            //open the inbox folder
            Folder inbox = store.getFolder("INBOX");
            inbox.open(Folder.READ_WRITE);

            // create or get done folder
            Folder doneFolder = getDoneFolder(inbox);
            Logger.getLogger(MessageReplier.class.getName()).log(Level.INFO, "checking {0}", inbox.getFullName());
            Message[] messages = inbox.getMessages();

            if (messages.length == 0) {
                return;
            } else {
                Logger.getLogger(MessageReplier.class.getName()).log(Level.INFO, "{0} new mails", new Object[]{messages.length});
            }

            for (int i = 0; i < messages.length; i++) {

                Message msg = messages[i];
                String from = adressesToString(msg.getFrom());

                Logger.getLogger(MessageReplier.class.getName()).log(Level.INFO, "[msg:{0}" + "] " + "{1}", new Object[]{i, from});
                try {
                    // multipart content may contain attachements to be converted
                    if (msg.getContent() instanceof Multipart) {
                        // run transcription on each attachement and send back one mail
                        MessageReplier messageReplier = new MessageReplier(mailConfig, msg, addressFrom);
                        messageReplier.run();
                    }
                } catch (IOException ex) {
                    Logger.getLogger(MessagePoller.class.getName()).log(Level.SEVERE, null, ex);
                }

                // copy in done folder (TODO : does nothing!)
                inbox.copyMessages(messages, doneFolder);

                // mark as deleted
                msg.setFlag(Flags.Flag.DELETED, true);

            }
            //close the inbox folder and expunge
            inbox.close(true);
            store.close();
        } catch (NoSuchProviderException ex) {
            Logger.getLogger(MessagePoller.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MessagingException ex) {
            Logger.getLogger(MessagePoller.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private static String adressesToString(Address addresses[]) throws MessagingException {
        StringBuilder from = new StringBuilder();
        for (Address address : addresses) {
            from.append(address.toString());
        }
        return from.toString();
    }

}
