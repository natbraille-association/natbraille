package org.natbraille.plugin

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.kotlin.dsl.create
import java.io.File
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.StandardCopyOption
import java.util.*

//import kotlin.io.path


/*
../natbraille-brailletable/pom.xml:                    <targetBundle>org.natbraille.brailletable.Messages</targetBundle>
../natbraille-font/pom.xml:                    <targetBundle>org.natbraille.font.Messages</targetBundle>
../natbraille-core/pom.xml:                    <targetBundle>org.natbraille.core.i18n.Messages</targetBundle>
../natbraille-braillepdf/pom.xml:                    <targetBundle>org.natbraille.braillepdf.Messages</targetBundle>
../natbraille-ocr/pom.xml:                    <targetBundle>org.natbraille.ocr.Messages</targetBundle>
 */

/*
../natbraille-brailletable/pom.xml:                    <poDirectory>${basedir}/src/main/po/org/natbraille/brailletable</poDirectory>
../natbraille-font/pom.xml:                    <poDirectory>${basedir}/src/main/po/org/natbraille/font</poDirectory>
../natbraille-core/pom.xml:                    <poDirectory>${project.build.sourceDirectory}/../resources/org/natbraille/core/i18n</poDirectory>
../natbraille-braillepdf/pom.xml:                    <poDirectory>${basedir}/src/main/po/org/natbraille/braillepdf</poDirectory>
../natbraille-ocr/pom.xml:                    <poDirectory>${basedir}/src/main/po/org/natbraille/ocr</poDirectory>
*/
/*
../natbraille-core/pom.xml:                    <keywords>-kTr -kTrc:1,2 -kTrn:1,2 -kTrnc:1,2,3</keywords>
 */
open class GetTextExtension(
    var poDirectory: String = "/src/main/po",
    var targetBundle: String = "",// = "org.natbraille.brailletable.Messages",
    var keywords: String = "-ktrc:1c,2 -ktrnc:1c,2,3 -ktr -kmarktr -ktrn:1,2 -k"
)

//interface GetTextMsgfmtPluginExtension {
//    val message: Property<String>
//}

class MsgfmtCommand constructor(
    val commandLine: String,
    val tmpDir: String,
    val tmpOutputJavaSourceFile: Path,
    val localeString: String
);

// https://discuss.gradle.org/t/right-way-to-generate-sources-in-gradle-7/41141/2

class MessageTargetBundle(val targetBundle: String) {
    // targetBundle : "org.natbraille.brailletable.Messages"

    //  ['org','natbraille','brailletable','Messages']
    val components: List<String> = targetBundle.split(".");

    // -> ['org','natbraille','brailletable']
    fun packagePath(): String {
        val basePackageName = components.subList(0, components.size - 1);
        return basePackageName.joinToString(File.separator);
    }

    // -> Message_<fr_FR>.java
    fun localeJavaClassFilename(localeString: String): String {
        val classBaseName = components.last()
        return classBaseName + '_' + localeString + ".java";
    }
};

class GetTextPlugin : Plugin<Project> {

    val generatedSourcesSubDirectory = Paths.get("generated", "main", "java")

    fun getProjectPortableObjectFiles(project: Project, poDirectory: String): MutableSet<File> {
        return project
            .fileTree(poDirectory) { include("**/*.po") }
            .getFiles()
    }

    fun getMsgfmtCommandLine(project: Project, poFile: File, targetBundle: String): MsgfmtCommand {

        println("[msgfmt plugin] from po file: " + poFile)

        val messageTargetBundle = MessageTargetBundle(targetBundle)

        // .../fr_FR.po -> fr_FR
        val localeString = poFile.nameWithoutExtension

        // -> tmp/gettext-tmpxyz/
        val tmpDir = Files.createTempDirectory("gettext-tmp").toFile().getAbsolutePath();

        // -> /tmp/gettext-tmp9189840871469177239/org/natbraille/brailletable/Messages_fr_FR.java
        val tmpOutputJavaFile = Paths.get(
            tmpDir,
            messageTargetBundle.packagePath(),
            messageTargetBundle.localeJavaClassFilename(localeString)
        )
        // println("@@@@@ tmpDir  <" + tmpDir + ">")
        // println("@@@@@ classLocaleName  <" + classLocaleName + ">")
        //println("@@@@@ classPackageComponents  <" + classPackageComponents + ">")
        // println("@@@@@ tmpOutputJavaFile  <" + tmpOutputJavaFile + ">")

        // create 'tmp/gettext-tmpxyz'
        Files.createDirectories(Paths.get(tmpDir));


        val cmdLine = arrayOf(
            "msgfmt",
            "--verbose",
            "--java2",
            "--source",
            "-d", tmpDir.toString(),
            "-r", targetBundle,
            "-l", localeString,
            poFile.toString()
        ).joinToString(" ")

        //println("=== ------------>" + cmdLine)

        var a = MsgfmtCommand(cmdLine, tmpDir, tmpOutputJavaFile, localeString)//,javaBaseFilename);
        //println("************" + a.commandLine);
        //println("************" + a.tmpOutputJavaSourceFile);
        //println("************" + a);
        //return CliAndOutputFile(cmdLine,packageDirectory)
        return a;//cmdLine
    }

    fun doMsgfmt(project: Project, poDirectory: String, targetBundle: String) {

        val messageTargetBundle = MessageTargetBundle(targetBundle);

        // create path to generated Messages_xx_XX.java
        // -> .../build/generated/main/java/org/natbraille/brailletable
        /*
        var outputJavaSourcePath = Paths.get(
            project.buildDir.toString(),
            generatedSourcesSubDirectory.toString(),
            messageTargetBundle.packagePath(),
        )*/
        /*
        var outputJavaSourcePath = project.layout.buildDirectory.dir(
            Paths.get(
            generatedSourcesSubDirectory.toString(),
            messageTargetBundle.packagePath()
            ).toString()
        )*/
        /*
        var outputJavaSourcePath = project.layout.buildDirectory.dir(
            Paths.get(
                generatedSourcesSubDirectory.toString(),
                messageTargetBundle.packagePath()
            ).toString()
        );
        */
        var outputJavaSourcePath = Paths.get(
            project.layout.buildDirectory.get().toString(),
                generatedSourcesSubDirectory.toString(),
                messageTargetBundle.packagePath()

        );
        Files.createDirectories(outputJavaSourcePath);

        println("[msgfmt plugin] outputJavaSourcePath : " + outputJavaSourcePath)

        // get sources files (.po)
        val poFiles = getProjectPortableObjectFiles(project, poDirectory)

        val commandLines = poFiles.map({ poFile ->
            getMsgfmtCommandLine(project, poFile, targetBundle)
        })

        val builders = commandLines.map({ a ->
            ProcessBuilder()
                .inheritIO()
                .command(a.commandLine.split(" "))
                .directory(project.projectDir)
                //.redirectOutput(File("theoutput.txt"))
                //.redirectError(File("theerror.txt"))
                .redirectOutput(ProcessBuilder.Redirect.INHERIT)
                .redirectError(ProcessBuilder.Redirect.INHERIT)
            //.redirectOutput(File("output.txt"))
        })
        val processes = builders.map({ builder ->
            // System.out.println("commande" + builder.command())
            builder.start()
        })


        //processes.forEach({ process -> process.waitFor(5, TimeUnit.SECONDS) })
        // TODO TImeUnit
        processes.forEach({ process -> process.waitFor() })

        commandLines.forEach({ a ->

            // ${project}/build/generated/main/java/org/natbraille/brailletable/Messages_fr_FR.jav
            val targetOutputJavaSourceFile = Paths.get(
                outputJavaSourcePath.toString(),
                messageTargetBundle.localeJavaClassFilename(a.localeString)
            )

            // copy temp Message_xx.java to target (build/generated/main/java/org...)
            System.out.println("[msgfmt plugin] copy " + a.tmpOutputJavaSourceFile + " to " + targetOutputJavaSourceFile)
            Files.move(
                a.tmpOutputJavaSourceFile,
                targetOutputJavaSourceFile,
                StandardCopyOption.REPLACE_EXISTING
            )

            // remove tmp directory
            Files.walk(Paths.get(a.tmpDir))
                .sorted(Comparator.reverseOrder())
                .map(Path::toFile)
                .forEach(File::delete);
        })

        //val out = processes.map({ process -> process.inputStream.reader().readText() })
    }

    fun doXgettext(project: Project, poDirectory: String, keywords: String) {
//        val finalPoDirectoryString = project.projectDir.toString() + "/src/main/po"
        //val javaSourceFiles = fileTree("${project.projectDir}/src/main/java/org/natbraille/brailletable/") { include("**/*.java") }
        val javaSourceFiles = project.fileTree("${project.projectDir}/src/main/java/") { include("**/*.java") }
            .getFiles()
            .map({ it -> it.relativeTo(project.projectDir) })

        val commandLine = (arrayOf(
            "xgettext",
            "--language=Java",
            "--force-po",
            "--output=${poDirectory}/keys.pot",
            "--from-code=utf-8",
//        //"--join-existing",
        ).toSet() + keywords.split(" ") + javaSourceFiles).joinToString(" ")

        println("xxxx on files" + javaSourceFiles)
        println("xxxx " + commandLine)

        val builder = ProcessBuilder()
            .command(commandLine.split(" "))
            .directory(project.projectDir)

        val process = builder.start()
        //process.waitFor(5, TimeUnit.SECONDS) })
        // TODO TImeUnit
        process.waitFor()

//
    }
//
//    fun getMsgFmtInputFiles(project: Project, poDirectory: String): Set<File> {
//        println("AND I AM HER=============================="+"poDirectory:"+poDirectory)
//        val inputsFiles =         project
//            .fileTree(poDirectory) { include("**/*.po") }
//            .getFiles()
//        println("input files found :"+inputsFiles)
//        return inputsFiles
//    }
//
//    fun getMsgFmtOutputFiles(project: Project, poDirectory: String, targetBundle: String) : Set<File> {
//
//        val inputs = getMsgFmtInputFiles(project, poDirectory)
//
//        val outputBaseDirectory = project.buildDir.toString() + "/classes/java/main"
//        val targetBundleDirectory = targetBundle//.split(".").joinToString(".")
//        val outputBasePath = outputBaseDirectory  + "/" + targetBundleDirectory
//        println("inputs are : "+inputs)
//        println("outputBaseDirectory is : "+outputBaseDirectory)
//        println("targetBundelDirectory is : "+targetBundleDirectory)
//        println("outputBasePath is : "+outputBasePath)
//        return project.fileTree(project.buildDir).getFiles()
//    }

    override fun apply(project: Project) {
//        val extension = project.extensions.create<GetTextMsgfmtPluginExtension>("greeting")
//        extension.message.convention("Hello from GreetingPlugin")

        val extension = project.extensions.create<GetTextExtension>("GetTextExtension")

        project.extensions.add("gettext", extension)
        //project.task()
        project.task("msgfmt") {
//            getMsgFmtInputFiles(project, extension.poDirectory)
            //inputs.files(getMsgFmtInputFiles(project, extension.poDirectory))
            //outputs.files(getMsgFmtOutputFiles(project, extension.poDirectory, extension.targetBundle))
            //  inputs.files(    getMsgFmtInputFiles(project, extension.poDirectory))


            doLast {
                doMsgfmt(project, extension.poDirectory, extension.targetBundle)
            }
        }
        project.task("xgettext") {
            doLast {
                doXgettext(project, extension.poDirectory, extension.keywords)
            }
        }
    }
}
