plugins {
    `kotlin-dsl`
}

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(17))
    }
}

repositories {
    gradlePluginPortal()
    mavenLocal()
    mavenCentral()
}

gradlePlugin {
    plugins {
        register("gettext-plugin") {
            id = "gettext"
            implementationClass = "org.natbraille.plugin.GetTextPlugin"
        }
    }
}


// ./gradlew listDirectDependencies | grep '^  -'  | sort
tasks.register("listDirectDependencies") {
    doLast {
        subprojects.forEach { subproject ->
            println("Dependencies for subproject: ${subproject.name}")
            // Iterate over common configurations
            subproject.configurations.forEach { configuration ->
                if (configuration.name == "implementation" || configuration.name == "api" || configuration.name == "compileOnly" || configuration.name == "antlr" || configuration.name == "testImplementation") {
                    configuration.dependencies.forEach { dependency ->
                        println("  - ${dependency.group}:${dependency.name}:${dependency.version} // ${configuration.name}")
                    }
                }
            }
        }
    }
}